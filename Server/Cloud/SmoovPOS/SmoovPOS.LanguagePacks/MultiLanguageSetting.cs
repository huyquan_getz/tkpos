﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SmoovPOS.LanguagePacks
{
    public class MultiLanguageSetting : IResource
    {
        private static MultiLanguageSetting _multiLanguageSetting;
        protected string _culture = string.Empty;

        #region Constructor

        public MultiLanguageSetting()
            : this("en")
        {
        }

        public MultiLanguageSetting(string culture)
        {
            _culture = culture;

            //Predefine Culture
            _resourceButton = LoadResource("Button");
            _resourceMessage = LoadResource("Message");
        }

        #endregion

        #region Singleton

        public static MultiLanguageSetting GetInstance()
        {
            return _multiLanguageSetting ?? (_multiLanguageSetting = new MultiLanguageSetting());
        }

        public static MultiLanguageSetting GetInstance(string culture)
        {
            return (_multiLanguageSetting = new MultiLanguageSetting(culture));
        }

        #endregion

        protected ResourceManager _resourceButton;
        public string Button(string key)
        {
            OverrideCulture();

            string retVal = string.Empty;

            if (_resourceButton != null)
            {
                retVal = _resourceButton.GetString(key);
            }

            return retVal;
        }

        protected ResourceManager _resourceMessage;
        public string Message(string key)
        {
            OverrideCulture();

            string retVal = string.Empty;

            if (_resourceMessage != null)
            {
                retVal = _resourceMessage.GetString(key);
            }

            return retVal;
        }

        public string Module(string module, string key)
        {
            ResourceManager res = LoadResource(module);
            return res.GetString(key, Thread.CurrentThread.CurrentCulture);
        }

        protected ResourceManager LoadResource(string module)
        {
            //OverrideCulture();

            string baseName = string.Format(typeof(MultiLanguageSetting).Namespace + ".Lang.{0}", module);

            return new ResourceManager(baseName, Assembly.GetExecutingAssembly());
        }

        protected void OverrideCulture()
        {
            CultureInfo cul = CultureInfo.CreateSpecificCulture(_culture);
            Thread.CurrentThread.CurrentCulture = cul;
            Thread.CurrentThread.CurrentUICulture = cul;
        }
    }
}