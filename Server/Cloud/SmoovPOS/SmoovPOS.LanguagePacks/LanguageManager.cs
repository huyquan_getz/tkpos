﻿using SmoovPOS.Common;
using SmoovPOS.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace SmoovPOS.LanguagePacks
{
    public class LanguageManager : ILanguageManager
    {
        #region Declarations



        #endregion

        #region Constructors

        public LanguageManager()
        {
        }

        #endregion

        #region Properties



        #endregion

        #region Methods

        #region ILanguageManager Members

        public string GetButtonText(string name)
        {
            var resourceManager = new ResourceManager(typeof(Objects.Button));

            try
            {
                string returnValue = resourceManager.GetString(name, CultureInfo.CurrentCulture);

                if (!string.IsNullOrEmpty(returnValue))
                {
                    return returnValue;
                }
            }
            catch (Exception)
            {
                return ConstantSmoovs.Attributes.ResourceUndefined;
            }

            return ConstantSmoovs.Attributes.ResourceUndefined;
        }

        public string GetLabelText(string name)
        {
            var resourceManager = new ResourceManager(typeof(Objects.Label));
            try
            {
                string returnValue = resourceManager.GetString(name, CultureInfo.CurrentCulture);

                if (!string.IsNullOrEmpty(returnValue))
                {
                    return returnValue;
                }
            }
            catch (Exception)
            {
                return ConstantSmoovs.Attributes.ResourceUndefined;
            }

            return ConstantSmoovs.Attributes.ResourceUndefined;
        }

        public string GetMessageText(string name)
        {
            var resourceManager = new ResourceManager(typeof(Objects.Message));

            try
            {
                string returnValue = resourceManager.GetString(name, CultureInfo.CurrentCulture);

                if (!string.IsNullOrEmpty(returnValue))
                {
                    return returnValue;
                }
            }
            catch (Exception)
            {
                return ConstantSmoovs.Attributes.ResourceUndefined;
            }

            return ConstantSmoovs.Attributes.ResourceUndefined;
        }

        #endregion

        #endregion

        #region Dispose



        #endregion
    }
}
