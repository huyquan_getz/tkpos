﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmoovPOS.LanguagePacks
{
    public interface IResource
    {
        string Button(string key);
        string Message(string key);
        string Module(string module, string key);
    }
}
