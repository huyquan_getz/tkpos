﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Permission
    {
        public Inventory store { get; set; }
        public Role role { get; set; }

        public bool isMerchant { get; set; }
    }
}