﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Inventory : CBBaseEntity  
    {
        public int index { get; set; }
        public string businessID { get; set; }
        public string description { get; set; }
        //public string contact { get; set; }       //20150408 MaoNguyen DEL
        public string contactMobile { get; set; }         //20150408 MaoNguyen ADD
        public string contact { get; set; }
        public string streetAddress { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }
        public string timeZone { get; set; }
        public string addressCombine { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string table
        {
            get { return "Inventory"; }
        }
        public Dictionary<string, string> listDefaultTimeZones { get; set; }
    }
}