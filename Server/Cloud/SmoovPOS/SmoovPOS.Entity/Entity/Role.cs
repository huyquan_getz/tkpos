﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Role : CBBaseEntity
    {
        public string roleName { get; set; }
        public string keyName { get; set; }
        public string description { get; set; }
        public string table
        {
            get { return "Role"; }
        }
        public string siteId { get; set; }
    }
}