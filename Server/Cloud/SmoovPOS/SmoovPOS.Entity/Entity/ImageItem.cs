﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class ImageItem
    {
        [Key]
        public string imageDetault { get; set; }
        public List<string> arrayImage { get; set; }
    }
}