﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Entity
{
    public class CashDrawerReport
    {
      //  public List<string> arrayActivity { get; set; }
        public DateTime startedDate { get; set; }
        public DateTime modifiedDate { get; set; }
        public DateTime closedDate { get; set; }
        public double startingCash { get; set; }
        public double closingCash { get; set; }

        public bool closed { get; set; }
        public ReportInfo reportInfo { get; set; }

        public string branchTimezone { get; set; }
        public string currency { get; set; }
      
    }

    public class ReportInfo
    {
        public double startingCash { get; set; }
        public double cashTendered { get; set; }
        public double cashChange { get; set; }
        public double cashRefund { get; set; }
        public double cashPaidIn { get; set; }
        public double cashPaidOut { get; set; }
        public double cashExpected { get; set; }
        public double cashTotalActual { get; set; }

    }
}