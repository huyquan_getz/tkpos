﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Entity
{
    public class NotifyOrder
    {
        public string orderID { get; set; }
        public int orderStatus { get; set; }
        public int paymentStatus { get; set; }
        public string stationID { get; set; }
        public List<OrderStatusHistory> arrayStatusHistory { get; set; }
    }
}