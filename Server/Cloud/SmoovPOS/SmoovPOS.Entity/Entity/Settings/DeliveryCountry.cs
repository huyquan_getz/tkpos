﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class DeliveryCountry : CBBaseEntity
    {
        public string table
        {
            get { return "deliverycountry"; }
        }
        public string country { get; set; }
        public string image { get; set; }
        public string countryCode { get; set; }
        public Dictionary<string, string> listDefaultTimeZones { get; set; }
        public List<Area> arrayArea { get; set; }
    }

    public class Area
    {
        public string areaName { get; set; }
        public string zipCode { get; set; }
    }
}