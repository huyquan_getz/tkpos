﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class ProductOrder : CBBaseEntity
    {
        public string table { get { return "ProductOrder"; } }
        public virtual List<VarientItem> arrayVariantOrder { get; set; }
        public virtual List<InventoryItem> arrayInventory { get; set; }
        public string sku { get; set; }
        public string title { get; set; }
        public string categoryId { get; set;}
        public bool discount { get; set; }
        public DiscountProductItem discountProductItem { get; set; }

    } 
}