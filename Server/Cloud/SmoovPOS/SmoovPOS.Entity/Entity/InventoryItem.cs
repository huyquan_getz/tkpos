﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class InventoryItem
    {
        [Key]
        public string name { get; set; }
        public List<string> options { get; set; }
    }
}