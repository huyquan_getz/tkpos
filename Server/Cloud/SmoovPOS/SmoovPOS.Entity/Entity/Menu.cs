﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Menu
{
    public class Menu
    {
        public string id { get; set; }
        public string name { get; set; }
        public string siteID { get; set; }
        public string url { get; set; }
    }
}