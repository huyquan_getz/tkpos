﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class ServiceShipping : CBBaseEntity
    {
        public string table { get { return "ServiceShipping"; } }
        public string deliveryService { get; set; }
        public double shippingFee { get; set; }
        public string timeEstimate { get; set; }
       
    }
}