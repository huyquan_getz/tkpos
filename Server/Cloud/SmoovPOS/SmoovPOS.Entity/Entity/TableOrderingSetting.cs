﻿using Com.SmoovPOS.Entity.CBEntity;
using SmoovPOS.Entity.Entity.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Com.SmoovPOS.Entity
{
    public class TableOrderingSetting : CBBaseEntity
    {
        public string table
        {
            get { return "TableOrderingSetting"; }
        }
        public string BranchId { get; set; }
        public string BranchName { get; set; }
        public int BranchIndex { get; set; }
        public bool AcceptTableOrdering { get; set; }
        public bool AllowAutoAcceptOrder { get; set; }
        public string Instructions { get; set; }
        public bool AllowSound { get; set; }
        public bool AllowPushNotification { get; set; }
        public bool AllowSendEmail { get; set; }
        public bool AllowSendSMS { get; set; }
        public List<DayOfWeekSetting> ListDayOfWeekSetting { get; set; }
    }

    //[PropertiesMustMatch("DayOfWeek", "Option", "Open_Opening", "Open_Closing", "Split_OpeningStart", "Split_OpeningEnd", "Split_ClosingStart", "Split_ClosingEnd", ErrorMessage = "The password and confirmation password do not match.")]
    //public class DayOfWeekSetting
    //{
    //    public int DayOfWeek { get; set; }
    //    public int Option { get; set; }
    //    //[Required(ErrorMessage = "Field required")]
    //    public string Open_Opening { get; set; }
    //    public string Open_Closing { get; set; }
    //    public string Split_OpeningStart { get; set; }
    //    public string Split_OpeningEnd { get; set; }
    //    public string Split_ClosingStart { get; set; }
    //    public string Split_ClosingEnd { get; set; }


    //}

    //[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    
}