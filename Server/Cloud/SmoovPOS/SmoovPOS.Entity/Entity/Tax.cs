﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Tax : CBBaseEntity
    {
        public string table { get { return "Tax"; } }
        public List<TaxItem> arrayTax { get; set; }
        public List<ServiceCharge> arrServiceCharge { get; set; }
    }

    public class ServiceCharge
    {
        public int index { get; set; }
        public Inventory store { get; set; }
        public double serviceChargeValue { get; set; }
        public double serviceChargeRate { get; set; }
    }
}