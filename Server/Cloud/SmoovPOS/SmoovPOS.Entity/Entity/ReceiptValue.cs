﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class ReceiptValue
    {
        public double subTotal { get; set; }
        public double grandTotal  { get; set; }
        public double serviceCharge { get; set; }
        public double serviceChargeRate { get; set; }
        public double totalTax { get; set; }
        public double rountAmt { get; set; }
        public List<TaxValue> listTaxOrder { get; set; }
        public double taxPercent { get; set; }
        public double discountTotal { get; set; }
        public double shippingFree { get; set; }
        public ReceiptValue ()
        {
            this.subTotal = 0;
            this.grandTotal = 0;
            this.serviceCharge = 0;
            this.serviceChargeRate = -1;
            this.totalTax = 0;
            this.rountAmt = 0;
            this.listTaxOrder = new List<TaxValue>();
            this.taxPercent = 0;
            this.discountTotal = 0;
            this.shippingFree = 0;
        }
    }
  
    public class TaxValue
    {
        public double taxValue { get; set; }
        public string taxName { get; set; }
        public double percent  { get; set; }
    }
}