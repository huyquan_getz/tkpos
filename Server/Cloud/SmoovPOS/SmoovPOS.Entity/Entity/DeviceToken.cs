﻿using Com.SmoovPOS.Entity.CBEntity;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.DeviceToken
{
    public class ManageDevice : CBBaseEntity
    {
        public string table
        {
            get { return "ListDevices"; }
        }
        public List<DeviceTokenModel> ArrayDevices;
    }
}