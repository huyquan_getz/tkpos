﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Entity
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Dao.Nguyen"</author>
    /// <datetime>2/12/2015-10:45 AM</datetime>
    public class AdHoc
    {
        public string title { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public int quantity { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime modifiedDate { get; set; }

        public int employeeId { get; set; } 
        public string employeeName { get; set; }
        public DiscountProductItem discountProductItem { get; set; }

        public bool applyDiscount { get; set; }
        public bool discount { get; set; }
    }
}