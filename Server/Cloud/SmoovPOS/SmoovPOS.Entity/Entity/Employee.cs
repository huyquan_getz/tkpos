﻿using Com.SmoovPOS.Entity.CBEntity;
using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Com.SmoovPOS.Entity
{
    public class Employee : CBBaseEntity
    {
        public string firstName  { get; set; } 
        public string lastName   { get; set; }
        public string fullName { get; set; }
        public int EmployeeId { get; set; } 
        public int gender        { get; set; }
        public string birthday { get; set; }
        public string phone      { get; set; }
        public string hiredDate { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string zipCode { get; set; }
        public string email { get; set; }
        public string pinCode { get; set; }
        public string password { get; set; }
        public string passwordConfirm { get; set; }
        public string image { get; set; }
        public virtual List<Permission> arrPermission { get; set; }
        public string table
        {
            get { return "Employee"; }
        }
        public string siteId { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string AddressCombine { get; set; }
    }
}