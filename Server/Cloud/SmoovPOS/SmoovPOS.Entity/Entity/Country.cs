﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Com.SmoovPOS.Entity.CBEntity;

namespace Com.SmoovPOS.Entity
{
    public class Country : CBBaseEntity
    {
        public string table
        {
            get { return "Country"; }
        }
        public virtual List<String> arrayCountry { get; set; }
        //public string currency { get; set; }
        public Dictionary<string, string> listDefaultTimeZones { get; set; }
    }
}