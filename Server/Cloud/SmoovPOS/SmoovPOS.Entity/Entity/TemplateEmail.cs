﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class TemplateEmail : CBBaseEntity
    {
        public string table { get { return "TemplateEmail"; } }
        public int index { get; set; }
        public string name { get; set; }

        public string body { get; set; }
    }
}