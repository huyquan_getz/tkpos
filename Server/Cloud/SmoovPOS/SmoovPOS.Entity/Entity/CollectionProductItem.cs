﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class CollectionProductItem
    {
        public string CollectionID { get; set; }
        public string NameCollection { get; set; }
    }
}