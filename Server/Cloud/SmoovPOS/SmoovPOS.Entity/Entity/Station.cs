﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Station : CBBaseEntity
    {
        public string table
        {
            get { return "Station"; }
        }

        public int branchIndex { get; set; }
        public string stationName { get; set; }
        public int stationSize { get; set; }
        public string stationQRCode { get; set; }

        public int statusOrdering { get; set; }
        public CustomerInfo customerHost { get; set; }
        public string orderId { get; set; }
    }

    public class CustomerInfo
    {
        public string memberID { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullName { get; set; }
        public string phone { get; set; }
        public string phoneRegion { get; set; }
        public string email { get; set; }
    }
}