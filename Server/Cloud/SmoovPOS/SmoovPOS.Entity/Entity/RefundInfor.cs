﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class RefundInfor 
    {

        public string reasonRefund { get; set; }
        public DateTime refundDate { get; set; }
        public string staffID { get; set; }
        public string staffFullName { get; set; }
        public virtual List<PaymentItem> arrayPayment { get; set; }
        public int typeRefund { get; set; }
        public string table
        {
            get { return "RefundInfor"; }
        }
    }
}