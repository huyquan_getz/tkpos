﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Instruction : CBBaseEntity
    {
        public string table
        {
            get { return "Instruction"; }

        }
        public string name { get; set; }
    }
}