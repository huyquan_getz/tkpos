﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class VarientDefault : CBBaseEntity
    {
        [Key]
        public List<string> items { get; set; }
    }
}