﻿using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Model
{
    public class SearchServicePackageModel : EntityPaging
    {
        public string name { get; set; }
        public IEnumerable<ServicePackageModel> ListServicePackages { get; set; }
    }
}