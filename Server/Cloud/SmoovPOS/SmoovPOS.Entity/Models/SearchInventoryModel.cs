﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    public class SearchInventoryModel
    {
        public IEnumerable<Inventory> Inventories { get; set; }
        public NewPaging Paging { get; set; }
        public bool isAddNew { get; set; }
    }
}