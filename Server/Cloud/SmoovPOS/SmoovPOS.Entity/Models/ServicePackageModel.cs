﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Model
{
    [Serializable]
    public class ServicePackageModel
    {
        public System.Guid servicePacketID { get; set; }

        public string servicePacketName { get; set; }

        public string description { get; set; }

        public System.Nullable<System.DateTime> servicePacketStartDate { get; set; }

        public System.Nullable<System.DateTime> servicePacketExpiredDate { get; set; }

        public System.Nullable<bool> status { get; set; }

        public string bussinessPlan { get; set; }

        public string userOwner { get; set; }

        public System.DateTime createdDate { get; set; }

        public string modifyUser { get; set; }

        public System.Nullable<System.DateTime> modifyDate { get; set; }

        public bool display { get; set; }
        public bool isTrial { get; set; }
        public int trialDays { get; set; }

        public string image { get; set; }
        public int NoOfMerchants { get; set; }
        public string dataDisplay { get; set; }
        public string createdDateDisplay
        {
            get
            {
                string display = string.Empty;
                if (createdDate > new DateTime(1970, 1, 1))
                {
                    display = createdDate.ToString("dd/MM/yyyy");
                }
                return display;
            }
        }
        public string monthlyFeeDisplay
        {
            get
            {
                string display = "--";
                if (billingServicePacketModel != null)
                {
                    display = string.Format("{0} {1}", billingServicePacketModel.moneyType, billingServicePacketModel.moneyPerUnit);
                }

                return display;
            }
        }

        public BillingServicePackageModel billingServicePacketModel { get; set; }

        public List<BillingServicePackageModel> BillingServicePackets { get; set; }

        public List<FunctionServicePackageModel> FunctionServicePackets { get; set; }
    }
}