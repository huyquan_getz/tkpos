﻿using SmoovPOS.Entity.Models.Paging;
using System.Collections.Generic;
namespace Com.SmoovPOS.Model
{
    public class SearchMerchantManagementModel : EntityPaging
    {
        public string fullName { get; set; }
        public IEnumerable<MerchantManagementModel> ListMerchantManagements { get; set; }

        //public int PageDefault { get { return 1; } }

        //public int LastOfPage { get; set; }

    }
}