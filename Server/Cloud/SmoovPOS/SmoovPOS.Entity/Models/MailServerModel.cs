﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    public class MailServerModel
    {
        public string Id { get; set; }
        public string IncomingPop { get; set; }
        public int IncomingPort { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string OutgoingSmtp { get; set; }
        //[Required(ErrorMessage = "Field Port is required")]
        // [Range(0, int.MaxValue, ErrorMessage = "Field Port must be a number")]
        //[StringLength(5)]
        public int OutgoingPort { get; set; }
        public bool EnableSsl { get; set; }
        public bool Credentials { get; set; }

        //public string UserName1 { get; set; }
        //public string PassWord1 { get; set; }
        //public string UserName2 { get; set; }
        //public string PassWord2 { get; set; }
        //public string UserName3 { get; set; }
        //public string PassWord3 { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid? merchantID { get; set; }

        public bool isApply { get; set; }
        public int type { get; set; }
    }
    // public class Pop3Client
    // {
    //string hostname { get; set; }
    //int port { get; set; }
    //string username { get; set; }
    //string password { get; set; }
    ////AuthMethod method = AuthMethod.Login { get; set; }
    //bool ssl { get; set; }
    ////RemoteCertificateValidationCallback validate { get; set; }
    //public X509CertificateCollection ClientCertificates { get; }  


    //public bool EnableSsl { get; set; }  


    //public bool UseDefaultCredentials { get; set; }
    //}
}