﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    public class DeviceTokenModel
    {
        public string uuid { get; set; }
        public string token { get; set; }
        public string deviceToken { get; set; }
        public string name { get; set; }
        public string systemName { get; set; }
        public string systemVersion { get; set; }
        public string model { get; set; }
        public string os { get; set; }
        public bool status { get; set; }
        public int badge { get; set; }
        public string appType { get; set; }
        public string userOwner { get; set; }
        public string bucket { get; set; }
        public Guid? merchantID { get; set; }
        public Guid deviceTokenId { get; set; }
        public int branchIndex { get; set; }
    }
}