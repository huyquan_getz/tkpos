﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Com.SmoovPOS.Model
{
    public class MerchantManagementModel
    {
        public System.Guid merchantID { get; set; }
        [Required]
        public string firstName { get; set; }
        [Required]
        public string lastName { get; set; }

        public string fullName { get; set; }

        public System.Nullable<bool> gender { get; set; }
        public System.Nullable<System.DateTime> birthday { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string zipCode { get; set; }
        public string region { get; set; }
        public string country { get; set; }

        public string email { get; set; }

        public string userID { get; set; }
        public string title { get; set; }
        public string businessName { get; set; }
        public System.Nullable<System.Guid> servicePacketID { get; set; }
        public System.Nullable<System.DateTime> servicePacketStartDate { get; set; }

        public System.Nullable<System.DateTime> servicePacketEndDate { get; set; }

        public string verifyLink { get; set; }

        public bool status { get; set; }

        public string bucket { get; set; }

        public string userOwner { get; set; }

        public System.DateTime createdDate { get; set; }

        public string modifyUser { get; set; }

        public System.Nullable<System.DateTime> modifyDate { get; set; }

        public bool display { get; set; }

        //public List<SelectListItem> servicePackets { get; set; }
        //public List<SelectListItem> countrys { get; set; }
        //public List<SelectListItem> titles { get; set; }
        #region servicePacket
        public string servicePacketName { get; set; }
        #endregion

        #region
        public string titleHomePage { get; set; }
        public string Domain { get; set; }
        public string SubDomain { get; set; }
        public string phoneRegion { get; set; }
        public System.Nullable<bool> CheckSubdomain { get; set; }
        public int proxyPort { get; set; }
        #endregion

        public int expiredDate
        {
            get
            {
                int expiredDate = 0;
                if (servicePacketEndDate != null)
                {
                    expiredDate = (int)(servicePacketEndDate.Value - DateTime.Now).TotalDays;
                }

                return expiredDate;
            }
        }
        public bool isExpiredDate
        {
            get
            {
                if (servicePacketEndDate != null)
                {
                    return (expiredDate <= 7);
                }
                return false;
            }
        }
        public bool isVerifyLink
        {
            get
            {
                return (string.IsNullOrEmpty(verifyLink));
            }
        }

        public string BillingCycleName { get; set; }

        public System.Nullable<System.Guid> billingCycleId { get; set; }
    }
}