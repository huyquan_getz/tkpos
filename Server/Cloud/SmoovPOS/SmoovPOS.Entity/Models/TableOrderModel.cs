﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    [Serializable]
    public class TableOrderModel
    { //Station Info
        public string id { get; set; }
        public string branchName { get; set; }
        public int branchIndex { get; set; }
        public string stationName { get; set; }
        public string stationId { get; set; }
        public int stationSize { get; set; }
        public string stationQRCode { get; set; }
        public int statusOrdering { get; set; }
        public string currency { get; set; }
        public bool isHost { get; set; }
        public bool isStartOrder { get; set; }
       
        //Customer Info
        public string memberID { get; set; }
        public string firstName { get; set; }
        //[LocalizedRequired]
        public string lastName { get; set; }
        public string fullName { get; set; }
        public string phoneRegion { get; set; }
        public string phone { get; set; }
        public string email { get; set; }

        public string remark { get; set; }
        //Order Info
        public int countOfItemsInCart { get; set; }
        public string orderId { get; set; }
        public List<ProductItem> listProductItems { get; set; }
        public ReceiptValue receiptValue { get; set; } //Receipt value of member's cart
        public string timeZone { get; set; }
        public string ref_id { get; set; }
    }
}