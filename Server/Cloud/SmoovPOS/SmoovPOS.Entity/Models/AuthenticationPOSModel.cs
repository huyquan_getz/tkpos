﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    [DataContract]
    public class AuthenticationPOSModel
    {
        [DataMember]
        public int code { get; set; }//0 fail: 1 success
        [DataMember]
        public string access_token { get; set; }
        [DataMember]
        public long expires_in { get; set; }
        [DataMember]
        public string serverSync { get; set; }
        [DataMember]
        public string databaseSync { get; set; }
        [DataMember]
        public string userAccessSync { get; set; }
        [DataMember]
        public string passAccessSync { get; set; }
        [DataMember]
        public string[] channels { get; set; }
        [DataMember]
        public List<Store> stores { get; set; }
        [DataMember]
        public string merchantID { get; set; }

    }
    [DataContract]
    public class Store
    {
        [DataMember]
        public string _id { get; set; }
        [DataMember]
        public string businessID { get; set; }
        [DataMember]
        public int index { get; set; }
        [DataMember]
        public string[] channels { get; set; }
        [DataMember]
        public string employeeID { get; set; }
    }
}