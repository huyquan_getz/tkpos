﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Model
{
    public class FunctionServicePackageModel
    {
        public System.Guid functionServiceID { get; set; }

        public System.Guid functionMethodID { get; set; }

        public System.Guid servicePacketID { get; set; }

        public int limit { get; set; }

        public string userOwner { get; set; }

        public System.DateTime createdDate { get; set; }

        public string modifyUser { get; set; }

        public System.Nullable<System.DateTime> modifyDate { get; set; }

        public bool display { get; set; }
        public bool isUnlimit { get; set; }
        public bool isSelect { get; set; }
        public string FunctionName { get; set; }
        public string keyMethod { get; set; }
        public bool isAddOns { get; set; }
        public bool isDatabaseFunction { get; set; }
        public string dataType { get; set; }
    }
}