﻿using Com.SmoovPOS.Model;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.FunctionMethod
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSFunctionMethod" in both code and config file together.
    [ServiceContract]
    public interface IWSFunctionMethod
    {
        //[OperationContract]
        //void DoWork();
        /// <summary>
        /// Wses the create function method.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>
        /// "Hoan.Tran"
        /// </author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        [OperationContract]
        Guid? WSCreateFunctionMethod(FunctionMethodModel result);
        /// <summary>
        /// Wses the update function method.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>
        /// "Hoan.Tran"
        /// </author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        [OperationContract]
        bool WSUpdateFunctionMethod(FunctionMethodModel result);
        /// <summary>
        /// Wses the delete function method.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>
        /// "Hoan.Tran"
        /// </author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        [OperationContract]
        bool WSDeleteFunctionMethod(Guid id);
        /// <summary>
        /// Wses the get function method.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>
        /// "Hoan.Tran"
        /// </author>
        /// <datetime>2/2/2015-3:48 PM</datetime>
        [OperationContract]
        FunctionMethodModel WSGetFunctionMethod(Guid id);
        /// <summary>
        /// Wses the get all function method.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:48 PM</datetime>
        [OperationContract]
        IEnumerable<FunctionMethodModel> WSGetAllFunctionMethod();
        /// <summary>
        /// Wses the name of the exist function method by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:48 PM</datetime>
        [OperationContract]
        bool WSExistFunctionMethodByName(string name, Guid? id);
        /// <summary>
        /// Wses the get function method by filter.
        /// </summary>
        /// <param name="searchModel">The search model.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:48 PM</datetime>
        [OperationContract]
        SearchFunctionMethodModel WSGetFunctionMethodByFilter(SearchFunctionMethodModel searchModel);
    }
}
