﻿using MessagingToolkit.QRCode.Codec;
using System;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace SmoovPOS.Service.QRGenerator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "QRGenerator" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select QRGenerator.svc or QRGenerator.svc.cs at the Solution Explorer and start debugging.
    public class QRGenerator : IQRGenerator
    {
        private const string ExistingBucketName = "smoovturnkey"; //Name of the bucket

        public string QRCodeGenerator(string Url, int width, string filename)
        {
            try
            {
                string qr_url = "";
                string path = HttpContext.Current.Server.MapPath("~/QRGenerator/TempFolder") + filename;
                QRCodeEncoder encoder = new QRCodeEncoder();
                encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H; // 30%
                encoder.QRCodeScale = 10;
                Bitmap img = encoder.Encode(Url);
                img = new Bitmap(img, new Size(width, width));
                //MemoryStream ms = new MemoryStream();
                //img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                img.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                //ms.Position = 0;
                qr_url = UploadToS3Amazon(path);
                return qr_url;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UploadToS3Amazon(string filePath)
        {
            string url = "";
            try
            {
                string fileName = Path.GetFileName(filePath);

                var fileTransferUtility = new
                    TransferUtility(new AmazonS3Client(Amazon.RegionEndpoint.USEast1));

                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = ExistingBucketName,
                    FilePath = filePath,
                    StorageClass = S3StorageClass.ReducedRedundancy,
                    PartSize = 6291456, // 6 MB.
                    CannedACL = S3CannedACL.PublicRead

                };

                fileTransferUtility.Upload(fileTransferUtilityRequest);
                url = "https://s3-ap-southeast-1.amazonaws.com/" + ExistingBucketName + "/" + fileName;

            }
            catch (AmazonS3Exception s3Exception)
            {
                System.Diagnostics.Debug.WriteLine(s3Exception.Message);
                System.Diagnostics.Debug.WriteLine(s3Exception.InnerException);
            }
            finally
            {
                File.Delete(filePath);
            }
            return url;
        }
    }
}
