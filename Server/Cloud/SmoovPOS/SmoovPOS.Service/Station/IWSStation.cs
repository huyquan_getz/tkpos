﻿using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Station
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSStation" in both code and config file together.
    [ServiceContract]
    public interface IWSStation
    {
        [OperationContract]
        int WSCreateStation(Com.SmoovPOS.Entity.Station station, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Station> WSGetAllStations(string designDoc, string viewName, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Station> WSGetListStationsOfBranch(string designDoc, string viewName, string siteID, int BranchIndex);

        [OperationContract]
        Com.SmoovPOS.Entity.Station WSGetDetailStation(string id);

        [OperationContract]
        Com.SmoovPOS.Entity.Station WSGetDetailStationByName(string designDoc, string viewName, string siteID, int BranchIndex, string StationName);

        [OperationContract]
        int WSUpdateStation(Com.SmoovPOS.Entity.Station station, string siteID);

        [OperationContract]
        int WSUpdateStationWithRawData(string designDoc, string viewName, string siteID, int BranchIndex, string StationID, string StationName, int StationSize, string StationQRCode);

        [OperationContract]
        int WSDeleteStation(string _id, string siteID);

        #region station table
        [OperationContract]
        ScanQRCodeModel WSGetScanQRCode(string stationid, string siteID);
        #endregion
    }
}
