﻿using Com.SmoovPOS.Business.Components;
using Com.SmoovPOS.Model;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Station
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSStation" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSStation.svc or WSStation.svc.cs at the Solution Explorer and start debugging.
    public class WSStation : IWSStation
    {
        public int WSCreateStation(Com.SmoovPOS.Entity.Station station, string siteID)
        {
            try
            {
                StationComponent component = new StationComponent();
                int returnValue = component.CreateNewStation(station, siteID);
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Com.SmoovPOS.Entity.Station> WSGetAllStations(string designDoc, string viewName, string siteID)
        {
            try
            {
                StationComponent component = new StationComponent();
                IEnumerable<Com.SmoovPOS.Entity.Station> returnValue = component.GetAllStations(designDoc, viewName, siteID);
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Com.SmoovPOS.Entity.Station> WSGetListStationsOfBranch(string designDoc, string viewName, string siteID, int BranchIndex)
        {
            try
            {
                StationComponent component = new StationComponent();
                IEnumerable<Com.SmoovPOS.Entity.Station> returnValue = component.GetListStationsOfBranch(designDoc, viewName, siteID, BranchIndex);
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Com.SmoovPOS.Entity.Station WSGetDetailStation(string id)
        {
            try
            {
                StationComponent component = new StationComponent();
                Com.SmoovPOS.Entity.Station returnValue = component.GetDetailStation(id);
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Com.SmoovPOS.Entity.Station WSGetDetailStationByName(string designDoc, string viewName, string siteID, int BranchIndex, string StationName)
        {
            try
            {
                StationComponent component = new StationComponent();
                List<Com.SmoovPOS.Entity.Station> list = component.GetListStationsOfBranch(designDoc, viewName, siteID, BranchIndex).ToList();
                Com.SmoovPOS.Entity.Station returnValue = list.FirstOrDefault(i => i.stationName == StationName);
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int WSUpdateStation(Com.SmoovPOS.Entity.Station station, string siteID)
        {
            try
            {
                StationComponent component = new StationComponent();
                int returnValue = component.UpdateStation(station, siteID);
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int WSUpdateStationWithRawData(string designDoc, string viewName, string siteID, int BranchIndex, string StationID, string StationName, int StationSize, string StationQRCode)
        {
            try
            {
                StationComponent component = new StationComponent();
                int returnValue = component.UpdateStationWithRawData(designDoc, viewName, siteID, BranchIndex, StationID, StationName, StationSize, StationQRCode);
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int WSDeleteStation(string _id, string siteID)
        {
            try
            {
                StationComponent component = new StationComponent();
                int returnValue = component.DeleteStation(_id, siteID);
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region station table
        public ScanQRCodeModel WSGetScanQRCode(string stationid, string siteID)
        {
            var model = new ScanQRCodeModel();
            try
            {
                StationComponent component = new StationComponent();
                model.station = component.GetDetailStation(stationid);

                if (model.station != null)
                {
                    TableOrderingSettingComponent tableOrderingSettingComp = new TableOrderingSettingComponent();
                    IEnumerable<Com.SmoovPOS.Entity.TableOrderingSetting> settings = tableOrderingSettingComp.GetAllTableOrderingSetting(ConstantSmoovs.CouchBase.DesigndocTableOrdering, ConstantSmoovs.CouchBase.ViewNameGetAllTableOrdering, siteID);
                    if (settings != null && settings.Count() > 0)
                    {
                        model.tableOrderingSetting = settings.Where(s => s.BranchIndex == model.station.branchIndex).FirstOrDefault();
                    }

                    InventoryComponent inventoryComponent = new InventoryComponent();
                    model.inventory = inventoryComponent.GetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, siteID).Where(i => i.index == model.station.branchIndex && i.status).FirstOrDefault();

                    MerchantGeneralSettingComponent componentSetting = new MerchantGeneralSettingComponent();
                    var merchantGeneralSetting = componentSetting.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID);
                    model.currency = merchantGeneralSetting.Currency;

                    var servicePacket = new SmoovPOS.Business.Components.ServicePackageComponent();
                    ServicePackageModel servicePackage = servicePacket.GetServicePacket(siteID);
                    var functionServicePacket = servicePackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.TABLEORDERING);
                    model.tableOrderingPermission = functionServicePacket != null && functionServicePacket.limit >= 1;

                    var merchantManagement = new SmoovPOS.Business.Components.MerchantManagementComponent();
                    var merchant = merchantManagement.GetMerchantManagementBySiteId(siteID);
                    if (merchant != null)
                    {
                        model.businessName = merchant.businessName;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        #endregion
    }
}