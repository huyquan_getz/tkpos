﻿using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SmoovPOS.Service.AuthenticationPOS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSAuthenticationPOS" in both code and config file together.
    [ServiceContract]
    public interface IWSAuthenticationPOS
    {
        //[OperationContract]
        //void DoWork();

        [WebGet(ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "WSCheckAuthenticationPOS")]
        AuthenticationPOSModel WSCheckAuthenticationPOS(string businessName, string username, string password);
        [WebGet(ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "WSGetAuthenticationPOS")]
        AuthenticationPOSModel WSGetAuthenticationPOS(string token);
    }
}
