﻿using Com.SmoovPOS.Model;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.ServicePacket
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "IWSServicePacket" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select IWSServicePacket.svc or IWSServicePacket.svc.cs at the Solution Explorer and start debugging.
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/13/2015-3:22 PM</datetime>
    public class IWSServicePacket : IIWSServicePacket
    {
        private SmoovPOS.Business.Components.ServicePackageComponent ServicePacket = new SmoovPOS.Business.Components.ServicePackageComponent();
        /// <summary>
        /// Wses the get Service Package.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/13/2015-3:22 PM</datetime>
        /// <exception cref="System.NotImplementedException"></exception>
        public ServicePackageModel WSGetServicePacket(Guid id)
        {
            if (id != null)
            {
                ServicePackageModel merchant = ServicePacket.GetServicePacket(id);
                return merchant;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Wses the get all Service Package.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/13/2015-3:22 PM</datetime>
        /// <exception cref="System.NotImplementedException"></exception>
        public IEnumerable<ServicePackageModel> WSGetAllServicePacket()
        {
            IEnumerable<ServicePackageModel> ServicePacketList = ServicePacket.GetAllServicePacket();

            return ServicePacketList;
        }
        /// <summary>
        /// Wses the get all Service Package display.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/13/2015-3:25 PM</datetime>
        public IEnumerable<ServicePackageModel> WSGetAllServicePacketDisplay()
        {
            IEnumerable<ServicePackageModel> ServicePacketList = ServicePacket.GetAllServicePacket();

            return ServicePacketList.Where(s => s.display).OrderBy(s => s.servicePacketName).ToList();
        }

        public Guid? WSCreateServicePacket(ServicePackageModel result)
        {
            var value = ServicePacket.CreateServicePacket(result);
            return value;
        }

        public bool WSUpdateServicePacket(ServicePackageModel result)
        {
            var value = ServicePacket.UpdateServicePacket(result);
            return value;
        }

        public SearchServicePackageModel WSGetServicePacketByFilter(SearchServicePackageModel searchModel)
        {
            var results = ServicePacket.GetServicePacket(searchModel);
            return results;
        }
        public bool WSExistServicePacketByName(string name, Guid? id)
        {
            var value = ServicePacket.ExistServicePacketByName(name, id);
            return value;
        }
        public ServicePackageModel WSNewServicePacket(string userOwner)
        {
            ServicePackageModel merchant = ServicePacket.NewServicePacket(userOwner);
            return merchant;
        }
        public IEnumerable<BillingCycleModel> WSGetAllBillingCycle()
        {
            IEnumerable<BillingCycleModel> BillingCycleList = ServicePacket.GetAllBillingCycle();

            return BillingCycleList;
        }
        public BillingCycleModel WSGetBillingCycle(Guid id)
        {
            BillingCycleModel billingCycle = ServicePacket.GetBillingCycle(id);

            return billingCycle;
        }
        public bool WSChangeStatus(Guid id, bool status)
        {
            var value = ServicePacket.ChangeStatus(id, status);
            return value;
        }

        public List<string> WSGetMerchantUsingServicePackage(string service_package_id, DateTime dtUTC)
        {
            var value = ServicePacket.GetListMerchantUsingServicePackage(service_package_id, dtUTC);
            return value;
        }
    }
}
