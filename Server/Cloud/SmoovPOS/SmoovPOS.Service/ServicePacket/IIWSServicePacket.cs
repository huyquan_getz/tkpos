﻿using Com.SmoovPOS.Model;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.ServicePacket
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IIWSServicePacket" in both code and config file together.
    [ServiceContract]
    public interface IIWSServicePacket
    {
        [OperationContract]
        Guid? WSCreateServicePacket(ServicePackageModel result);
        [OperationContract]
        bool WSUpdateServicePacket(ServicePackageModel result);
        [OperationContract]
        ServicePackageModel WSGetServicePacket(Guid id);
        [OperationContract]
        IEnumerable<ServicePackageModel> WSGetAllServicePacket();
        [OperationContract]
        IEnumerable<ServicePackageModel> WSGetAllServicePacketDisplay();
        [OperationContract]
        SearchServicePackageModel WSGetServicePacketByFilter(SearchServicePackageModel searchModel);
        [OperationContract]
        bool WSExistServicePacketByName(string name, Guid? id);
        [OperationContract]
        ServicePackageModel WSNewServicePacket(string userOwner);
        [OperationContract]
        IEnumerable<BillingCycleModel> WSGetAllBillingCycle();
        [OperationContract]
        BillingCycleModel WSGetBillingCycle(Guid id);
        [OperationContract]
        bool WSChangeStatus(Guid id, bool status);
        [OperationContract]
        List<string> WSGetMerchantUsingServicePackage(string service_package_id, DateTime dtUTC);
    }
}
