﻿using Com.SmoovPOS.Business;
using Com.SmoovPOS.Model;
using SmoovPOS.Business.Components;
using System.Collections.Generic;

namespace SmoovPOS.Service.Site
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSSite" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSSite.svc or WSSite.svc.cs at the Solution Explorer and start debugging.
    public class WSSite : IWSSite
    {
        public string WSCreateSite(SiteModel siteModel)
        {
            SiteBusiness siteBiz = new SiteBusiness();
            siteBiz.Save(siteModel, true);

            return siteModel.SiteID;
        }

        public string WSCreateSiteAlias(SiteAliasModel siteAliasModel)
        {
            SiteBusiness siteBiz = new SiteBusiness();
            siteBiz.CreateSiteAlias(siteAliasModel);

            return string.Empty;
        }

        public string WSUpdateSite(SiteModel siteModel)
        {
            //SiteComponent siteComp = new SiteComponent();
            //int StatusCode = siteComp.UpdateSite(site);
            //return StatusCode;

            return string.Empty;
        }

        public string WSDeleteSite(string _id)
        {
            //SiteComponent siteComp = new SiteComponent();
            //int StatusCode = siteComp.DeleteSite(_id);
            //return StatusCode;

            return string.Empty;
        }

        public IEnumerable<SiteModel> WSGetAllSite()
        {
            SiteBusiness siteBiz = new SiteBusiness();
            IEnumerable<SiteModel> siteList = siteBiz.GetAllSite();

            return siteList;
        }

        public IEnumerable<SiteAliasModel> WSGetAllSiteAlias()
        {
            SiteBusiness siteBiz = new SiteBusiness();
            IEnumerable<SiteAliasModel> siteAliasList = siteBiz.GetAllSiteAlias();

            return siteAliasList;
        }
        public SiteAliasModel WSGetSiteAliasBySiteID(string siteID)
        {
            SiteComponent site = new SiteComponent();

            return site.GetSiteAlias(siteID);
        }
        public bool WSCreateSiteSQL(SiteModel siteModel)
        {
            SiteComponent site = new SiteComponent();

            return site.CreateSite(siteModel);
        }

        public bool WSCreateSiteAliasSQL(SiteAliasModel siteAliasModel)
        {
            SiteComponent site = new SiteComponent();

            return site.CreateSiteAlias(siteAliasModel);
        }
        public bool WSUpdateSiteSQL(SiteModel siteModel, string siteID)
        {
            SiteComponent site = new SiteComponent();

            return site.UpdateSite(siteModel, siteID);
        }

        public bool WSUpdateSiteAliasSQL(SiteAliasModel siteAliasModel, string siteAliasID)
        {
            SiteComponent site = new SiteComponent();

            return site.UpdateSiteAlias(siteAliasModel, siteAliasID);
        }

        public bool WSCheckExistUrl(string url, bool isSubDomain, string siteID)
        {
            SiteComponent site = new SiteComponent();
            if (string.IsNullOrEmpty(url))
            {
                return false;
            }
            return site.ExistSiteAliasByUrl(url, isSubDomain, siteID);
        }

        public SiteAliasModel WSGetSiteAlias(string bucket)
        {
            SiteComponent site = new SiteComponent();
            if (string.IsNullOrEmpty(bucket))
            {
                return null;
            }
            return site.GetSiteAlias(bucket);
        }
        public SiteAliasModel WSGetSiteAliasByHost(string host)
        {
            SiteComponent site = new SiteComponent();
            if (string.IsNullOrEmpty(host))
            {
                return null;
            }
            return site.GetSiteAliasByHost(host);
        }
    }
}
