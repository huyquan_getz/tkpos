﻿using Com.SmoovPOS.Model;
using System.Collections.Generic;
using System.ServiceModel;

namespace SmoovPOS.Service.Site
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSSite" in both code and config file together.
    [ServiceContract]
    public interface IWSSite
    {
        // TODO: Add your service operations here

        [OperationContract]
        string WSCreateSite(SiteModel site);

        [OperationContract]
        string WSCreateSiteAlias(SiteAliasModel siteAlias);

        [OperationContract]
        string WSUpdateSite(SiteModel site);

        [OperationContract]
        string WSDeleteSite(string _id);

        [OperationContract]
        IEnumerable<SiteModel> WSGetAllSite();

        [OperationContract]
        IEnumerable<SiteAliasModel> WSGetAllSiteAlias();

        [OperationContract]
        bool WSCreateSiteSQL(SiteModel site);

        [OperationContract]
        bool WSCreateSiteAliasSQL(SiteAliasModel siteAlias);

        [OperationContract]
        bool WSUpdateSiteSQL(SiteModel site,string siteID);

        [OperationContract]
        bool WSUpdateSiteAliasSQL(SiteAliasModel siteAlias, string siteAliasID);

        [OperationContract]
        bool WSCheckExistUrl(string url, bool isSubDomain, string siteID);

        [OperationContract]
        SiteAliasModel WSGetSiteAlias(string bucket);

        [OperationContract]
        SiteAliasModel WSGetSiteAliasByHost(string host);

        [OperationContract]
        SiteAliasModel WSGetSiteAliasBySiteID(string siteID);

    }
}
