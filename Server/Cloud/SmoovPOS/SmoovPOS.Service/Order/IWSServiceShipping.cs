﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Order
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSServiceShipping" in both code and config file together.
    [ServiceContract]
    public interface IWSServiceShipping
    {
        [OperationContract]
        int WSCreateServiceShipping(ServiceShipping serviceShipping, string siteID);

        [OperationContract]
        ServiceShipping WSDetailServiceShipping(string _id, string siteID);

        [OperationContract]
        int WSUpdateServiceShipping(ServiceShipping serviceShipping, string siteID);

        [OperationContract]
        int WSDeleteServiceShipping(string _id, string siteID);

        [OperationContract]
        IEnumerable<ServiceShipping> WSGetAllServiceShipping(string designDoc, string viewName, string siteID);
    }
}
