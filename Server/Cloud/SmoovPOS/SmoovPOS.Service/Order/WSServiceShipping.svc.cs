﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Order
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSServiceShipping" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSServiceShipping.svc or WSServiceShipping.svc.cs at the Solution Explorer and start debugging.
    public class WSServiceShipping : IWSServiceShipping
    {

        public int WSCreateServiceShipping(ServiceShipping serviceShipping, string siteID)
        {
            ServiceShippingComponent serviceShippingComponent = new ServiceShippingComponent();
            return serviceShippingComponent.CreateServiceShipping(serviceShipping, siteID);
        }

        public ServiceShipping WSDetailServiceShipping(string _id, string siteID)
        {
            ServiceShippingComponent serviceShippingComponent = new ServiceShippingComponent();
            return serviceShippingComponent.DetailServiceShipping(_id, siteID);
        }

        public int WSUpdateServiceShipping(ServiceShipping serviceShipping, string siteID)
        {
            ServiceShippingComponent serviceShippingComponent = new ServiceShippingComponent();
            return serviceShippingComponent.UpdateServiceShipping(serviceShipping, siteID);
        }

        public int WSDeleteServiceShipping(string _id, string siteID)
        {
            ServiceShippingComponent serviceShippingComponent = new ServiceShippingComponent();
            return serviceShippingComponent.DeleteServiceShipping(_id, siteID);
        }

        public IEnumerable<ServiceShipping> WSGetAllServiceShipping(string designDoc, string viewName, string siteID)
        {
            ServiceShippingComponent serviceShippingComponent = new ServiceShippingComponent();
            return serviceShippingComponent.GetAllServiceShipping(designDoc,viewName, siteID);
        }
    }
}
