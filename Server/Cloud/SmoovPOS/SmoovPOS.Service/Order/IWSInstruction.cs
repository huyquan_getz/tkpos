﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Order
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSInstruction" in both code and config file together.
    [ServiceContract]
    public interface IWSInstruction
    {
        [OperationContract]
        int WSCreateInstruction(Instruction instruction, string siteID);

        [OperationContract]
        Instruction WSDetailInstruction(string _id, string siteID);

        [OperationContract]
        int WSUpdateInstruction(ServiceShipping serviceShipping, string siteID);

        [OperationContract]
        int WSDeleteInstruction(string _id, string siteID);

        [OperationContract]
        IEnumerable<Instruction> WSGetAllInstruction(string designDoc, string viewName, string siteID);
    }
}
