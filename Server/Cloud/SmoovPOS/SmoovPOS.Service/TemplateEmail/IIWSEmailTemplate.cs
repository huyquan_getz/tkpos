﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.TemplateEmail
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IIWSEmailTemplate" in both code and config file together.
    [ServiceContract]
    public interface IIWSEmailTemplate
    {
        [OperationContract]
        int WSCreateTemplateEmail(Com.SmoovPOS.Entity.TemplateEmail templateEmail, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.TemplateEmail WSGetDetailTemplateEmail(string key, string siteID);

        [OperationContract]
        int WSDeleteAllTemplateEmail(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSUpdateTemplateEmail(Com.SmoovPOS.Entity.TemplateEmail templateEmail, string siteID);


        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.TemplateEmail> WSGetAllTemplateEmail(string designDoc, string viewName, string siteID);
    }
}
