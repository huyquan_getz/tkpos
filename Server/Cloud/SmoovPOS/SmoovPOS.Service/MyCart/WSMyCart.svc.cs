﻿using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MyCart
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSMyCart" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSMyCart.svc or WSMyCart.svc.cs at the Solution Explorer and start debugging.
    public class WSMyCart : IWSMyCart
    {
        public Com.SmoovPOS.Entity.MyCart WSGetMyCartByEmail(string designDoc, string viewName, string bucket, string email)
        {
            Com.SmoovPOS.Entity.MyCart myCart = new Com.SmoovPOS.Entity.MyCart();
            MyCartComponent mycartComponent = new MyCartComponent();
            myCart = mycartComponent.GetMyCartByEmail(designDoc, viewName, bucket, email);

            return myCart;
        }

        public int WSCreateMyCart(Com.SmoovPOS.Entity.MyCart myCart, string bucket)
        {
            MyCartComponent mycartComponent = new MyCartComponent();
            int statusCode = mycartComponent.CreateMyCart(myCart, bucket);
            return statusCode;
        }

        public int WSUpdateMyCart(Com.SmoovPOS.Entity.MyCart myCart, string bucket)
        {
            MyCartComponent mycartComponent = new MyCartComponent();
            int statusCode = mycartComponent.UpdateMyCart(myCart, bucket);
            return statusCode;
        }

        public int WSDeleteMyCart(string _id, string bucket)
        {
            MyCartComponent mycartComponent = new MyCartComponent();
            int statusCode = mycartComponent.DeleteMyCart(_id, bucket);
            return statusCode;
        }
    }
}