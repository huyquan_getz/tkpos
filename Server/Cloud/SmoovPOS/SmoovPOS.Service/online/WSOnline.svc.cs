﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Business.Components;
using SmoovPOS.Entity.Menu;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmoovPOS.Service.online
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSOnline" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSOnline.svc or WSOnline.svc.cs at the Solution Explorer and start debugging.
    public class WSOnline : IWSOnline
    {
        public void DoWork()
        {
        }

        public IEnumerable<Menu> GetListNameCategory(string designDoc, string viewName, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            IEnumerable<Menu> categoryList = online.GetListNameCategory(designDoc, viewName, siteID);
            return categoryList;
        }

        public IEnumerable<Menu> GetListNameCollection(string designDoc, string viewName, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            IEnumerable<Menu> categoryList = online.GetListNameCollection(designDoc, viewName, siteID);
            return categoryList;
        }

        public IEnumerable<Com.SmoovPOS.Entity.Category> WSGetAllActiveCategory(string designDoc, string viewName, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            IEnumerable<Com.SmoovPOS.Entity.Category> categoryList = online.GetAllActiveCategory(designDoc, viewName, siteID);
            var list = categoryList.Where(r => r.status).OrderBy(r=>r.title);
            return list;
        }

        public IEnumerable<ProductItem> WSGetAllActiveProductItemByCategory(string designDoc, string viewName, string categoryName, EntityPaging paging, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            IEnumerable<Com.SmoovPOS.Entity.ProductItem> ListProduct = online.GetAllActiveProductItemByCategory(designDoc, viewName, categoryName, paging, siteID);
            return ListProduct;
        }

        public IEnumerable<ProductItem> WSGetAllActiveProductItem(string designDoc, string viewName, EntityPaging paging, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            IEnumerable<Com.SmoovPOS.Entity.ProductItem> ListProduct = online.GetAllActiveProductItem(designDoc, viewName, paging, siteID);
            return ListProduct;
        }

        public IEnumerable<ProductItem> WSGetAllActiveProductItemBestSeller(string designDoc, string viewName, EntityPaging paging, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            IEnumerable<Com.SmoovPOS.Entity.ProductItem> ListProduct = online.GetProductItem(designDoc, viewName, paging, siteID);
            return ListProduct;
        }

        public ProductItem WSGetProductItemDetail(string _id, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            Com.SmoovPOS.Entity.ProductItem productItem = online.GetProductItemDetail(_id, siteID);
            return productItem;
        }

        public int WSCountAllActiveProductItem(string designDoc, string viewName, string siteID)
        {
           OnlineComponent online = new OnlineComponent();
           int count = online.CountAllActiveProductItem(designDoc, viewName, siteID);
           return count;
        }

        public int WSCountAllActiveProductItemByCategory(string designDoc, string viewName, string categoryName, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            int count = online.CountAllActiveProductItemByCategory(designDoc, viewName, categoryName, siteID);
            return count;
        }

        public IEnumerable<ProductItem> WSSearchAllActiveProductItem(string designDoc, string viewName, string searchKey, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            return online.SearchAllActiveProductItem(designDoc, viewName, searchKey, siteID);
        }

        public IEnumerable<ProductItem> WSSearchBestSellerProductItem(string designDoc, string viewName, string siteID, int countBestSeller)
        {
            OnlineComponent online = new OnlineComponent();
            return online.SearchBestSellerProductItem(designDoc, viewName, siteID, countBestSeller);
        }

        public IEnumerable<ProductItem> WSGetAllOnlineProductItem(string designDoc, string viewName, string siteID)
        {
            OnlineComponent online = new OnlineComponent();
            IEnumerable<Com.SmoovPOS.Entity.ProductItem> ListProduct = online.GetAllActiveProductItem(designDoc, viewName, siteID);
            return ListProduct.Where(p => p.status).ToList();
        }
    }
}
