﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Menu;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace SmoovPOS.Service.online
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSOnline" in both code and config file together.
    [ServiceContract]
    public interface IWSOnline
    {
        [OperationContract]
        void DoWork();
        [OperationContract]
        IEnumerable<Menu> GetListNameCategory(string designDoc, string viewName, string siteID);

        [OperationContract]
        IEnumerable<Menu> GetListNameCollection(string designDoc, string viewName, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Category> WSGetAllActiveCategory(string designDoc, string viewName, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllActiveProductItemByCategory(string designDoc, string viewName, string categoryName, EntityPaging paging, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllActiveProductItem(string designDoc, string viewName, EntityPaging paging, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllActiveProductItemBestSeller(string designDoc, string viewName, EntityPaging paging, string siteID);

        [OperationContract]
        ProductItem WSGetProductItemDetail(string _id, string siteID);

        [OperationContract]
        int WSCountAllActiveProductItem(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSCountAllActiveProductItemByCategory(string designDoc, string viewName, string categoryName, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSSearchAllActiveProductItem(string designDoc, string viewName, string searchKey, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSSearchBestSellerProductItem(string designDoc, string viewName, string siteID, int countBestSeller);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllOnlineProductItem(string designDoc, string viewName, string siteID);
    }
}
