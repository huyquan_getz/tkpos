﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.DeveloperSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICurrency" in both code and config file together.
    [ServiceContract]
    public interface IWSCurrency
    {
        [OperationContract]
        List<Com.SmoovPOS.Entity.CurrencyItem> GetAllCurrency(string designDoc, string viewName, string siteID);

        [OperationContract]
        int CreateNewCurrency(string siteID, Com.SmoovPOS.Entity.Currency currency);

        [OperationContract]
        int UpdateCurrency(string siteID, Com.SmoovPOS.Entity.Currency currency);
    }
}
