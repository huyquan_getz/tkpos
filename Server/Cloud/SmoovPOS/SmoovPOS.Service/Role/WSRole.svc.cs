﻿using Com.SmoovPOS.Business.Components;
using Com.SmoovPOS.Service.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Role
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSRole" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSRole.svc or WSRole.svc.cs at the Solution Explorer and start debugging.
    public class WSRole : IWSRole
    {

        public int WSCreateRole(Com.SmoovPOS.Entity.Role role, string siteID)
        {
            RoleComponent roleComponent = new RoleComponent();
            return roleComponent.CreateNewRole(role, siteID);
        }


        public IEnumerable<Com.SmoovPOS.Entity.Role> WSGetAllRole(string designDoc, string viewName, string siteID)
        {
            IEnumerable<Com.SmoovPOS.Entity.Role> listRole = Enumerable.Empty<Com.SmoovPOS.Entity.Role>();

            try
            {
                RoleComponent roleComponent = new RoleComponent();
                listRole = roleComponent.GetAllRole(designDoc, viewName, siteID);
                return listRole;
            }
            catch
            {
                return listRole;
                throw new NotImplementedException();
            }
        }


        public Com.SmoovPOS.Entity.Role WSDetailRole(string _id, string siteID)
        {
            RoleComponent roleComponent = new RoleComponent();
            Com.SmoovPOS.Entity.Role role = roleComponent.DetailRole(_id, siteID);
            return role;
        }
    }
}
