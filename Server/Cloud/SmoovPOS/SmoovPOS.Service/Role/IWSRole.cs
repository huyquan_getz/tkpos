﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Com.SmoovPOS.Service.Role
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSRole" in both code and config file together.
    [ServiceContract]
    public interface IWSRole
    {
        [OperationContract]
        int WSCreateRole(Com.SmoovPOS.Entity.Role role, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Role> WSGetAllRole(string designDoc, string viewName, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Role WSDetailRole(string _id, string siteID);
    }
}
