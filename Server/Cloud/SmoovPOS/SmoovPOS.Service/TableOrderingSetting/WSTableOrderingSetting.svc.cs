﻿using Com.SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.TableOrderingSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSTableOrderingSetting" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSTableOrderingSetting.svc or WSTableOrderingSetting.svc.cs at the Solution Explorer and start debugging.
    public class WSTableOrderingSetting : IWSTableOrderingSetting
    {
        public int WSCreateTableOrderingSetting(Com.SmoovPOS.Entity.TableOrderingSetting tableOrderingSetting, string siteID)
        {
            TableOrderingSettingComponent tableOrderingSettingComp = new TableOrderingSettingComponent();
            int StatusCode = tableOrderingSettingComp.CreateNewTableOrderingSetting(tableOrderingSetting, siteID);
            return StatusCode;
        }

        public Com.SmoovPOS.Entity.TableOrderingSetting WSDetailTableOrderingSetting(string _id, string siteID)
        {
            //
            TableOrderingSettingComponent tableOrderingSettingComp = new TableOrderingSettingComponent();
            Com.SmoovPOS.Entity.TableOrderingSetting tableOrderingSetting = tableOrderingSettingComp.DetailTableOrderingSetting(_id, siteID);
            return tableOrderingSetting;
        }

        public int WSUpdateTableOrderingSetting(Com.SmoovPOS.Entity.TableOrderingSetting tableOrderingSetting, string siteID)
        {
            TableOrderingSettingComponent tableOrderingSettingComp = new TableOrderingSettingComponent();
            int StatusCode = tableOrderingSettingComp.UpdateTableOrderingSetting(tableOrderingSetting, siteID);
            return StatusCode;
        }

        public int WSDeleteTableOrderingSetting(string _id, string siteID)
        {
            TableOrderingSettingComponent tableOrderingSettingComp = new TableOrderingSettingComponent();
            int StatusCode = tableOrderingSettingComp.DeleteTableOrderingSetting(_id, siteID);
            return StatusCode;
        }

        public Com.SmoovPOS.Entity.TableOrderingSetting WSDetailTableOrderingSettingByBranchId(string designDoc, string viewName, string branch_id, string siteID)
        {
            try
            {
                TableOrderingSettingComponent tableOrderingSettingComp = new TableOrderingSettingComponent();
                return tableOrderingSettingComp.DetailTableOrderingSettingByBranchId(designDoc, viewName, branch_id, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Com.SmoovPOS.Entity.TableOrderingSetting> WSGetAllTableOrderingSetting(string designDoc, string viewName, string siteID)
        {
            try
            {
                TableOrderingSettingComponent tableOrderingSettingComp = new TableOrderingSettingComponent();
                return tableOrderingSettingComp.GetAllTableOrderingSetting(designDoc, viewName, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
