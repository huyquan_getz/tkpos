﻿using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.DeliverySetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSDeliverySetting" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSDeliverySetting.svc or WSDeliverySetting.svc.cs at the Solution Explorer and start debugging.
    public class WSDeliverySetting : IWSDeliverySetting
    {
        public bool WSCreateDeliverySetting(Com.SmoovPOS.Entity.DeliverySetting result, string siteID)
        {
            DeliverySettingComponent DeliverySettingComp = new DeliverySettingComponent();
            bool StatusCode = DeliverySettingComp.CreateNewDeliverySetting(result, siteID);
            return StatusCode;
        }

        public bool WSUpdateDeliverySetting(Com.SmoovPOS.Entity.DeliverySetting result, string siteID)
        {
            DeliverySettingComponent DeliverySettingComp = new DeliverySettingComponent();
            bool StatusCode = DeliverySettingComp.UpdateDeliverySetting(result, siteID);
            return StatusCode;
        }

        public bool WSDeleteDeliverySetting(string id, string siteID)
        {
            DeliverySettingComponent DeliverySettingComp = new DeliverySettingComponent();
            bool StatusCode = DeliverySettingComp.DeleteDeliverySetting(id, siteID);
            return StatusCode;
        }

        public Com.SmoovPOS.Entity.DeliverySetting WSGetDeliverySetting(string id, string siteID)
        {
            DeliverySettingComponent DeliverySettingComp = new DeliverySettingComponent();
            var result = DeliverySettingComp.DetailDeliverySetting(id, siteID);
            return result;
        }

        public IEnumerable<Com.SmoovPOS.Entity.DeliverySetting> WSGetAllDeliverySetting(string designDoc, string viewName, string siteID)
        {
            DeliverySettingComponent DeliverySettingComp = new DeliverySettingComponent();
            var results = DeliverySettingComp.GetAllDeliverySetting(designDoc, viewName, siteID);
            return results;
        }
        public Com.SmoovPOS.Entity.DeliverySetting WSGetDeliverySettingByCountry(string designDoc, string viewName, string siteID, string country)
        {
            DeliverySettingComponent DeliverySettingComp = new DeliverySettingComponent();
            var results = DeliverySettingComp.GetDeliverySettingByCountry(designDoc, viewName, siteID, country);
            return results;
        }
    }
}
