﻿using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.DeliverySetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSDeliveryCountry" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSDeliveryCountry.svc or WSDeliveryCountry.svc.cs at the Solution Explorer and start debugging.
    public class WSDeliveryCountry : IWSDeliveryCountry
    {
        //public void DoWork()
        //{
        //}
        public bool WSCreateDeliveryCountry(Com.SmoovPOS.Entity.DeliveryCountry result, string siteID)
        {
            DeliveryCountryComponent DeliveryCountryComp = new DeliveryCountryComponent();
            bool StatusCode = DeliveryCountryComp.CreateNewDeliveryCountry(result, siteID);
            return StatusCode;
        }

        public bool WSUpdateDeliveryCountry(Com.SmoovPOS.Entity.DeliveryCountry result, string siteID)
        {
            DeliveryCountryComponent DeliveryCountryComp = new DeliveryCountryComponent();
            bool StatusCode = DeliveryCountryComp.UpdateDeliveryCountry(result, siteID);
            return StatusCode;
        }

        public bool WSDeleteDeliveryCountry(string id, string siteID)
        {
            DeliveryCountryComponent DeliveryCountryComp = new DeliveryCountryComponent();
            bool StatusCode = DeliveryCountryComp.DeleteDeliveryCountry(id, siteID);
            return StatusCode;
        }

        public Com.SmoovPOS.Entity.DeliveryCountry WSGetDeliveryCountry(string id, string siteID)
        {
            DeliveryCountryComponent DeliveryCountryComp = new DeliveryCountryComponent();
            var result = DeliveryCountryComp.DetailDeliveryCountry(id, siteID);
            return result;
        }

        public IEnumerable<Com.SmoovPOS.Entity.DeliveryCountry> WSGetAllDeliveryCountry(string designDoc, string viewName, string siteID)
        {
            DeliveryCountryComponent DeliveryCountryComp = new DeliveryCountryComponent();
            var results = DeliveryCountryComp.GetAllDeliveryCountry(designDoc, viewName, siteID);
            return results;
        }
    }
}
