﻿using Com.SmoovPOS.Business.Components.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.TakeAwaySetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSTakeAwaySetting" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSTakeAwaySetting.svc or WSTakeAwaySetting.svc.cs at the Solution Explorer and start debugging.
    public class WSTakeAwaySetting : IWSTakeAwaySetting
    {
        public int WSCreateTakeAwaySetting(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID)
        {
            TakeAwaySettingComponent takeAwaySettingComp = new TakeAwaySettingComponent();
            int StatusCode = takeAwaySettingComp.CreateNewTakeAwaySetting(takeAwaySetting, siteID);
            return StatusCode;
        }

        public Com.SmoovPOS.Entity.TakeAwaySetting WSDetailTakeAwaySetting(string _id, string siteID)
        {
            //
            TakeAwaySettingComponent takeAwaySettingComp = new TakeAwaySettingComponent();
            Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting = takeAwaySettingComp.DetailTakeAwaySetting(_id, siteID);
            return takeAwaySetting;
        }

        public int WSUpdateTakeAwaySetting(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID)
        {
            TakeAwaySettingComponent takeAwaySettingComp = new TakeAwaySettingComponent();
            int StatusCode = takeAwaySettingComp.UpdateTakeAwaySetting(takeAwaySetting, siteID);
            return StatusCode;
        }

        public int WSDeleteTakeAwaySetting(string _id, string siteID)
        {
            TakeAwaySettingComponent takeAwaySettingComp = new TakeAwaySettingComponent();
            int StatusCode = takeAwaySettingComp.DeleteTakeAwaySetting(_id, siteID);
            return StatusCode;
        }

        public Com.SmoovPOS.Entity.TakeAwaySetting WSDetailTakeAwaySettingByBranchId(string designDoc, string viewName, string branch_id, string siteID)
        {
            try
            {
                TakeAwaySettingComponent takeAwaySettingComp = new TakeAwaySettingComponent();
                return takeAwaySettingComp.DetailTakeAwaySettingByBranchId(designDoc, viewName, branch_id, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Com.SmoovPOS.Entity.TakeAwaySetting> WSGetAllTakeAwaySetting(string designDoc, string viewName, string siteID)
        {
            try
            {
                TakeAwaySettingComponent takeAwaySettingComp = new TakeAwaySettingComponent();
                return takeAwaySettingComp.GetAllTakeAwaySetting(designDoc, viewName, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
