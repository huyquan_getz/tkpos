﻿using Com.SmoovPOS.Business.Components;
using Com.SmoovPOS.Entity;
using SmoovPOS.Entity;
using SmoovPOS.Entity.Models;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmoovPOS.Service.Product
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSProductItem" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSProductItem.svc or WSProductItem.svc.cs at the Solution Explorer and start debugging.
    public class WSProductItem : IWSProductItem
    {
        public int WSCreateProductItem(ProductItem productItem, string siteID)
        {
            int result = -1;
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                result = productComponent.CreateNewProductItem(productItem, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }


        public int WSUpdateProductItem(ProductItem productItem, string siteID)
        {
            int result = -1;
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                result = productComponent.UpdateProductItem(productItem, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }


        public ProductItem WSDetailProductItem(string _id, string siteID)
        {
            ProductItem product = null;
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                product = productComponent.DetailProductItem(_id, siteID);
                return product;
            }
            catch
            {
                return product;
                throw new NotImplementedException();
            }
        }


        public int WSDeleteProductItem(string _id, string siteID)
        {
            int result = -1;
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                result = productComponent.DeleteProductItem(_id, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }


        public IEnumerable<ProductItem> WSGetAllProductItem(string designDoc, string viewName, string siteID)
        {
            IEnumerable<ProductItem> listProduct = Enumerable.Empty<ProductItem>();
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                listProduct = productComponent.GetAllProductItem(designDoc, viewName, siteID);
                return listProduct;
            }
            catch
            {
                return listProduct;
                throw new NotImplementedException();
            }
        }
        public IEnumerable<ProductItem> WSGetAllProductItemByProductID(string designDoc, string viewName, string productID, string siteID)
        {
            IEnumerable<ProductItem> listProduct = Enumerable.Empty<ProductItem>();
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                listProduct = productComponent.GetAllProductItemByIDProduct(designDoc, viewName, productID, siteID);
                return listProduct;
            }
            catch
            {
                return listProduct;
                throw new NotImplementedException();
            }
        }
        public SearchProductModel WSGetAllProductItemByStore(string designDoc, string viewName, int store, string siteID, NewPaging paging)
        {
            SearchProductModel listProduct = new SearchProductModel();
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                listProduct = productComponent.GetAllProductItemByStore(designDoc, viewName, store, siteID, paging);
                return listProduct;
            }
            catch
            {
                return listProduct;
                throw new NotImplementedException();
            }
        }

        public IEnumerable<ProductItem> WSGetAllProductItemOnStore(string designDoc, string viewName, string siteID)
        {
            IEnumerable<ProductItem> listProduct = Enumerable.Empty<ProductItem>();
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                listProduct = productComponent.GetAllProductItemByStore(designDoc, viewName, siteID);
                return listProduct;
            }
            catch
            {
                return listProduct;
                throw new NotImplementedException();
            }
        }

        public int WSCountVarients(string designDoc, string viewName, string businessID, string productID, string varientID, string siteID)
        {
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                return productComponent.CountVarients(designDoc, viewName, businessID, productID, varientID, siteID);
            }
            catch
            {
                throw new NotImplementedException();
            }
        }
        public int WSCountAllProductItemByStore(string designDoc, string viewName, string businessID, string siteID)
        {
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                return productComponent.CountAllProductItemByStore(designDoc, viewName, businessID, siteID);
            }
            catch
            {
                throw new NotImplementedException();
            }
        }

        public IEnumerable<ProductItem> WSGetAllActiveProductItemByStoreAndCategory(string designDoc, string viewName, string siteID, string branchName, string categoryName)
        {
            ProductItemComponent productComponent = new ProductItemComponent();
            IEnumerable<ProductItem> ListProduct = productComponent.GetAllActiveProductItemByStoreAndCategory(designDoc, viewName, siteID, branchName, categoryName);
            return ListProduct;
        }

        public IEnumerable<ProductItem> WSGetAllProductItemByMaster(string designDoc, string viewName, string startKey, string endKey, int limit, bool allowStale, int pageCurrent, int pageGoTo, string sort, Boolean desc, string siteID)
        {
            IEnumerable<ProductItem> listProduct = Enumerable.Empty<ProductItem>();
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                listProduct = productComponent.GetAllProductItemByMaster(designDoc, viewName, startKey, endKey, limit, allowStale, pageCurrent, pageGoTo, sort, desc, siteID);
                return listProduct;
            }
            catch
            {
                return listProduct;
                throw new NotImplementedException();
            }
        }

        public int WSCountAllProductItemByMaster(string designDoc, string viewName, string siteID)
        {
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                return productComponent.CountAllProductItemByMaster(designDoc, viewName, siteID);
            }
            catch
            {
                throw new NotImplementedException();
            }
        }
        public IEnumerable<ProductItem> WSGetAllProductItemByMasterNoLimit(string designDoc, string viewName, string siteID)
        {
            IEnumerable<ProductItem> listProduct = Enumerable.Empty<ProductItem>();
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                listProduct = productComponent.GetAllProductItemByMasterNoLimit(designDoc, viewName, siteID);
                return listProduct;
            }
            catch
            {
                return listProduct;
                throw new NotImplementedException();
            }
        }

        public IEnumerable<ProductItem> WSGetProductItemByListProductID(string designDoc, string viewName, List<string> productIDs, string siteID)
        {
            IEnumerable<ProductItem> listProduct = Enumerable.Empty<ProductItem>();
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                listProduct = productComponent.GetProductItemByListIDProduct(designDoc, viewName, productIDs, siteID);
                return listProduct;
            }
            catch
            {
                return listProduct;
                throw new NotImplementedException();
            }
        }

        public IEnumerable<ProductItem> WSGetProductItemInDiscount(string siteID)
        {
            IEnumerable<ProductItem> listProduct = Enumerable.Empty<ProductItem>();
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                listProduct = productComponent.GetAllProductItem(Common.ConstantSmoovs.CouchBase.DesignDocProductItem, Common.ConstantSmoovs.CouchBase.ViewAllProductItemByDiscount, siteID);
                listProduct = listProduct.Where(l => l.DiscountProductItem != null && l.DiscountProductItem.beginDate <= DateTime.UtcNow && l.DiscountProductItem.endDate > DateTime.UtcNow && l.discount == true);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return listProduct;
        }

        public SearchProductModel WSSearchProductItem(string designDoc, string viewName, string siteID, NewPaging paging)
        {
            SearchProductModel listProduct = new SearchProductModel();
            try
            {
                ProductItemComponent productComponent = new ProductItemComponent();
                listProduct = productComponent.SearchingProduct(designDoc, viewName, siteID, paging);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return listProduct;

        }
        public int WSUpdateStatus(string[] arrID, Boolean status, string siteID)
        {
            try
            {

                IEnumerable<ProductItem> listProduct = Enumerable.Empty<ProductItem>();
                ProductItemComponent productItemComponent = new ProductItemComponent();             
                foreach (string _id in arrID)
                {
                    productItemComponent.UpdateStatusProductItem(_id, status, siteID);
                }
                return 1;
            }
            catch
            {
                return 0;
                throw new NotImplementedException();
            }
        }
        public int WSUpdateQuantityProductItemByStore(List<ProductItem> listProductItemsMyCart, int store, string siteID)
        {
            try
            {
                ProductItemComponent productItemComponent = new ProductItemComponent();
                productItemComponent.UpdateQuantityProductItem(listProductItemsMyCart,store,siteID);
                return 1;
            }
            catch
            {
                return 0;
                throw new NotImplementedException();
            }
        }
        public int UpdateBusinessNameProductItemByStore(string designDoc, string viewName, int store, string bucket, string businessName)
        {
            try
            {
                ProductItemComponent productItemComponent = new ProductItemComponent();
                var status = productItemComponent.UpdateBusinessNameProductItemByStore(designDoc,viewName,store,bucket,businessName);
                return 1;
            }
            catch
            {
                return 0;
                throw new NotImplementedException();
            }
        }
    }
}
