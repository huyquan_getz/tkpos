﻿using Com.SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Com.SmoovPOS.Product
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSProduct" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSProduct.svc or WSProduct.svc.cs at the Solution Explorer and start debugging.
    public class WSProduct : IWSProduct
    {
        public void DoWork()
        {
        }


        public int WSCreateProduct(Com.SmoovPOS.Entity.Product product, string siteID)
        {
            int result = -1;
            try
            {
                ProductComponent productComponent = new ProductComponent();
                result = productComponent.CreateNewProduct(product, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }

        public Com.SmoovPOS.Entity.Product WSDetailProduct(string _id, string siteID)
        {
            Com.SmoovPOS.Entity.Product product = null;
            try
            {
                ProductComponent productComponent = new ProductComponent();
                product = productComponent.DetailProduct(_id, siteID);
                return product;
            }
            catch
            {
                return product;
                throw new NotImplementedException();
            }
        }

        public int WSUpdateProduct(Com.SmoovPOS.Entity.Product product, string siteID)
        {
            int result = -1;
            try
            {
                ProductComponent productComponent = new ProductComponent();
                result = productComponent.UpdateProduct(product, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }

        }

        public int WSDeleteProduct(string _id, string siteID)
        {
            int result = -1;
            try
            {
                ProductComponent productComponent = new ProductComponent();
                result = productComponent.DeleteProduct(_id, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }

        }

        public int WSCountAllProduct(string designDoc, string viewName, string siteID)
        {
            ProductComponent productComp = new ProductComponent();
            return productComp.CountAllProduct(designDoc, viewName, siteID);
        }


        public string WSCreateProduct_(Entity.Product product, string siteID)
        {
            string _id = "";
            try
            {
                ProductComponent productComponent = new ProductComponent();
                _id = productComponent.CreateNewProduct_(product, siteID);
                return _id;
            }
            catch
            {
                return _id;
                throw new NotImplementedException();
            }
        }

        public int WSCheckSKUExist(string designDoc, string viewName, string sku, string siteID)
        {
            ProductComponent productComp = new ProductComponent();
            return productComp.CheckSKUExist(designDoc, viewName, sku, siteID);
        }

        public IEnumerable<Com.SmoovPOS.Entity.Product> WSGetAllProductActive(string designDoc, string viewName, string siteID)
        {
            ProductComponent productComp = new ProductComponent();
            IEnumerable<Com.SmoovPOS.Entity.Product> ListProduct = productComp.GetAllProductActive(designDoc, viewName, siteID);
            return ListProduct;
        }
    }
}
