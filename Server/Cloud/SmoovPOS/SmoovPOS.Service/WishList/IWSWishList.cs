﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.WishList
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSWishList" in both code and config file together.
    [ServiceContract]
    public interface IWSWishList
    {

        [OperationContract]
        int WSCreateWishList(Com.SmoovPOS.Entity.WishList wishlist, string siteID);
        [OperationContract]
        int WSUpdateWishList(Com.SmoovPOS.Entity.WishList wishlist, string siteID);
        [OperationContract]
        int WSDeleteWishList(string _id, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.WishList WSGetWishListWithEmail(string designDoc, string viewName, string email, string siteID);
    }
}
