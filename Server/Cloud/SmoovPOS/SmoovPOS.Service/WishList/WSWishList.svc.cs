﻿using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.WishList
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSWishList" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSWishList.svc or WSWishList.svc.cs at the Solution Explorer and start debugging.
    public class WSWishList : IWSWishList
    {
        public int WSCreateWishList(Com.SmoovPOS.Entity.WishList wishlist, string siteID)
        {
            WishListComponent wishListComponent = new WishListComponent();
            int statusCode = wishListComponent.CreateWishList(wishlist, siteID);
            return statusCode;
        }
        public int WSUpdateWishList(Com.SmoovPOS.Entity.WishList wishlist, string siteID)
        {
            WishListComponent wishListComponent = new WishListComponent();
            int statusCode = wishListComponent.UpdateWishList(wishlist, siteID);
            return statusCode;
        }
        public int WSDeleteWishList(string _id, string siteID)
        {
            WishListComponent wishListComponent = new WishListComponent();
            int statusCode = wishListComponent.DeleteWishList(_id, siteID);
            return statusCode;
        }
        public Com.SmoovPOS.Entity.WishList WSGetWishListWithEmail(string designDoc, string viewName, string email, string siteID) 
        {
            Com.SmoovPOS.Entity.WishList wishlist = new Com.SmoovPOS.Entity.WishList();
            WishListComponent wishListComponent = new WishListComponent();
            wishlist = wishListComponent.GetWishListWithEmail(designDoc,viewName,email,siteID);

            return wishlist;
        }
    }
}
