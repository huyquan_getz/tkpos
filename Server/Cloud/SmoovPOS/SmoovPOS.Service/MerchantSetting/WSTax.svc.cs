﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MerchantSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSTax" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSTax.svc or WSTax.svc.cs at the Solution Explorer and start debugging.
    public class WSTax : IWSTax
    {

        public int WSCreateTax(Com.SmoovPOS.Entity.Tax tax, string siteId)
        {
            TaxComponent taxComponent = new TaxComponent();
            return taxComponent.CreateNewRole(tax,siteId);

        }

        public IEnumerable<Com.SmoovPOS.Entity.Tax> GetAllTax(string designDoc, string viewName, string siteId)
        {
            TaxComponent taxComponent = new TaxComponent();
            return taxComponent.GetAllTax(designDoc,viewName,siteId);
        }

        public Com.SmoovPOS.Entity.Tax DetailTax(string id, string siteId)
        {
            TaxComponent taxComponent = new TaxComponent();
            return taxComponent.DetailTax(id, siteId);
        }

        public Com.SmoovPOS.Entity.Tax GetDetailTaxByInventory(string designDoc, string viewName, string siteId, int inventoryId)
        {
            TaxComponent taxComponent = new TaxComponent();
            return taxComponent.GetDetailTaxWithInventory(designDoc, viewName, siteId, inventoryId);
        }

        public List<Com.SmoovPOS.Entity.TaxItem> GetTaxWithInventory(string designDoc, string viewName, string siteId, int inventoryId)
        {
            TaxComponent taxComponent = new TaxComponent();
            return taxComponent.GetTaxWithStoreId(designDoc,viewName, siteId,inventoryId);
        }
         public ServiceCharge GetServiceChargeWithInventory (string designDoc, string viewName, string siteId, int inventoryId)
        {
            TaxComponent taxComponent = new TaxComponent();
            return taxComponent.GetServiceChargeWithStoreId(designDoc, viewName, siteId, inventoryId);
        }
        
    }
}
