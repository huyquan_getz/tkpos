﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MerchantSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSMerchantSetting" in both code and config file together.
    [ServiceContract]
    public interface IWSMerchantSetting
    {
       
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.MerchantSetting> GetAllMerchantSetting(string designDoc, string viewName, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.MerchantSetting GetMerchantSetting(string designDoc, string viewName, string siteID);

        [OperationContract]
        int UpdateMerchantSetting(Com.SmoovPOS.Entity.MerchantSetting setting, string siteID);
    }
}
