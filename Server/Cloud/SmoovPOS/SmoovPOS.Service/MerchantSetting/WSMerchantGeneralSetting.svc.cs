﻿using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MerchantSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSMerchantGeneralSetting" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSMerchantGeneralSetting.svc or WSMerchantGeneralSetting.svc.cs at the Solution Explorer and start debugging.
    public class WSMerchantGeneralSetting : IWSMerchantGeneralSetting
    {
        public Com.SmoovPOS.Entity.MerchantGeneralSetting GetDetailMerchantGeneralSetting(string designDoc, string viewName, string siteID)
        {

            Com.SmoovPOS.Entity.MerchantGeneralSetting merchantGeneralSetting = null;
            try
            {
                MerchantGeneralSettingComponent component = new MerchantGeneralSettingComponent();
                merchantGeneralSetting = component.GetDetailMerchantGeneralSetting(designDoc, viewName, siteID);
                return merchantGeneralSetting;
            }
            catch
            {
                return merchantGeneralSetting;
                throw new NotImplementedException();
            }
        }

        public int CreateNewMerchantGeneralSetting(Com.SmoovPOS.Entity.MerchantGeneralSetting merchantGeneralSetting, string siteID)
        {
            MerchantGeneralSettingComponent component = new MerchantGeneralSettingComponent();
            return component.CreateNewMerchantGeneralSetting(merchantGeneralSetting, siteID);
        }

        public int UpdateMerchantGeneralSetting(Com.SmoovPOS.Entity.MerchantGeneralSetting merchantGeneralSetting, string siteID)
        {
            MerchantGeneralSettingComponent component = new MerchantGeneralSettingComponent();
            return component.UpdateMerchantGeneralSetting(merchantGeneralSetting, siteID);
        }
    }
}
