﻿using SmoovPOS.Entity.DeviceToken;
using SmoovPOS.Entity.Models;
using SmoovPOS.Entity.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MerchantSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSCheckoutSetting" in both code and config file together.
    [ServiceContract]
    public interface IWSCheckoutSetting
    {
        [OperationContract]
        CheckoutSetting GetDetailCheckoutSetting(string designDoc, string viewName, string siteID);

        [OperationContract]
        int CreateCheckoutSetting(CheckoutSetting merchantCheckoutSetting, string siteID);

        [OperationContract]
        int UpdateCheckoutSetting(CheckoutSetting merchantCheckoutSetting, string siteID);

        [OperationContract]
        AccountSmoovpay GetDetailAccountSmoovpay(string designDoc, string viewName, string siteID);

        [OperationContract]
        int CreateAccountSmoovpay(string siteID, AccountSmoovpay merchantCheckoutSetting);

        [OperationContract]
        int UpdateAccountSmoovpay(string siteID, AccountSmoovpay merchantCheckoutSetting);

        [OperationContract]
        int WSDeleteAccountSmoovPay(string _id, string siteID);

        [OperationContract]
        ManageDevice DetailDeviceToken(string designDoc, string viewName, string bucket);

        [OperationContract]
        int UpdateDeviceToken(string bucket, DeviceTokenModel device);

        [OperationContract]
        int AddDeviceToken(string bucket, string designDoc, string viewName, DeviceTokenModel device);
        [OperationContract]
        int DeleteDevice(string id, string bucket);
        [OperationContract]
        bool ExistToken(string id, string bucket);

        [OperationContract]
        int UpdateManageDevice(ManageDevice manageDevice, string bucket);

        [OperationContract]
        bool SendNotification(string designDoc, string viewName, string bucket, int branchIndex, string message, string appType);
    }
}
