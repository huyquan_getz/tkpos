﻿using Com.SmoovPOS.Model;
using System;
using System.Collections.Generic;

namespace SmoovPOS.Service.MerchantManagement
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "IWSMerchantManagement" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select IWSMerchantManagement.svc or IWSMerchantManagement.svc.cs at the Solution Explorer and start debugging.
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/9/2015-10:54 AM</datetime>
    public class IWSMerchantManagement : IIWSMerchantManagement
    {
        private SmoovPOS.Business.Components.MerchantManagementComponent merchantManagement = new SmoovPOS.Business.Components.MerchantManagementComponent();

        /// <summary>
        /// Wses the create merchant management.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:54 AM</datetime>
        public Guid? WSCreateMerchantManagement(MerchantManagementModel result)
        {
            var value = merchantManagement.CreateMerchantManagement(result);
            return value;
        }

        /// <summary>
        /// Wses the update merchant management.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:54 AM</datetime>
        public bool WSUpdateMerchantManagement(MerchantManagementModel result)
        {
            var value = merchantManagement.UpdateMerchantManagement(result);
            return value;
        }

        /// <summary>
        /// Wses the delete merchant management.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:54 AM</datetime>
        public bool WSDeleteMerchantManagement(Guid id)
        {
            var value = merchantManagement.DeleteMerchantManagement(id);
            return value;
        }

        /// <summary>
        /// Wses the change status merchant management.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:54 AM</datetime>
        public bool WSChangeStatusMerchantManagement(Guid id, bool status)
        {
            var value = merchantManagement.ChangeStatusMerchantManagement(id, status);
            return value;
        }

        /// <summary>
        /// Wses the get merchant management.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:54 AM</datetime>
        public MerchantManagementModel WSGetMerchantManagement(Guid id)
        {
            if (id != null)
            {
                MerchantManagementModel merchant = merchantManagement.GetMerchantManagement(id);
                return merchant;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Wses the get merchant management by user identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-3:42 PM</datetime>
        public MerchantManagementModel WSGetMerchantManagementByUserId(string id)
        {
            if (id != null)
            {
                MerchantManagementModel merchant = merchantManagement.GetMerchantManagementByUserId(id);
                return merchant;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Wses the get all merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:54 AM</datetime>
        public IEnumerable<MerchantManagementModel> WSGetAllMerchantManagement()
        {
            IEnumerable<MerchantManagementModel> merchantManagementList = merchantManagement.GetAllMerchantManagement();

            return merchantManagementList;
        }

        public int WSGetCountAllMerchantManagement()
        {
            var count = merchantManagement.GetCountAllMerchantManagement();

            return count;
        }
        /// <summary>
        /// Wses the get merchant management.
        /// </summary>
        /// <param name="searchModel">The search model.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/8/2015-4:20 PM</datetime>
        public SearchMerchantManagementModel WSGetMerchantManagementByFilter(SearchMerchantManagementModel searchModel)
        {
            SmoovPOS.Business.Components.MerchantManagementComponent merchantManagement = new SmoovPOS.Business.Components.MerchantManagementComponent();
            var merchantManagementList = merchantManagement.GetMerchantManagement(searchModel);

            return merchantManagementList;
        }
        /// <summary>
        /// Wses the exist merchant management by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public bool WSExistMerchantManagementByEmail(string email, Guid? id)
        {
            SmoovPOS.Business.Components.MerchantManagementComponent merchantManagement = new SmoovPOS.Business.Components.MerchantManagementComponent();
            var exist = merchantManagement.ExistMerchantManagementByEmail(email, id);

            return exist;
        }
        /// <summary>
        /// Wses the name of the exist merchant management by business.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public bool WSExistMerchantManagementByBusinessName(string businessName, Guid? id)
        {
            SmoovPOS.Business.Components.MerchantManagementComponent merchantManagement = new SmoovPOS.Business.Components.MerchantManagementComponent();
            var exist = merchantManagement.ExistMerchantManagementByBusinessName(businessName, id);

            return exist;
        }


        /// <summary>
        /// Wses the get merchant management by site identifier.
        /// </summary>
        /// <param name="siteId">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public MerchantManagementModel WSGetMerchantManagementBySiteId(string siteId)
        {
            if (siteId != null)
            {
                MerchantManagementModel merchant = merchantManagement.GetMerchantManagementBySiteId(siteId);
                return merchant;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Wses the get all country.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public List<string> WSGetAllCountry()
        {
            SmoovPOS.Business.Components.MerchantManagementComponent merchantManagement = new SmoovPOS.Business.Components.MerchantManagementComponent();
            var result = merchantManagement.GetAllCountry();

            return result;
        }
        /// <summary>
        /// Wses the get all site identifier.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public List<string> WSGetAllSiteID()
        {
            SmoovPOS.Business.Components.MerchantManagementComponent merchantManagement = new SmoovPOS.Business.Components.MerchantManagementComponent();
            var result = merchantManagement.GetAllSiteID();

            return result;
        }
        /// <summary>
        /// Wses the get next proxy port.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public int WSGetNextProxyPort()
        {
            SmoovPOS.Business.Components.MerchantManagementComponent merchantManagement = new SmoovPOS.Business.Components.MerchantManagementComponent();
            var result = merchantManagement.GetNextProxyPort();

            return result;
        }

        /// <summary>
        /// Wses the name of the get merchant management by business.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public MerchantManagementModel WSGetMerchantManagementByBusinessName(string businessName)
        {
            SmoovPOS.Business.Components.MerchantManagementComponent merchantManagement = new SmoovPOS.Business.Components.MerchantManagementComponent();
            var result = merchantManagement.GetMerchantManagementByBusinessName(businessName);
            return result;
        }
    }
}
