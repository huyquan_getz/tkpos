﻿using Com.SmoovPOS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MerchantManagement
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IIWSMerchantManagement" in both code and config file together.
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/30/2015-5:26 PM</datetime>
    [ServiceContract]
    public interface IIWSMerchantManagement
    {
        /// <summary>
        /// Wses the create merchant management.
        /// </summary>
        /// <param name="merchantManagement">The merchant management.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        Guid? WSCreateMerchantManagement(MerchantManagementModel merchantManagement);

        /// <summary>
        /// Wses the update merchant management.
        /// </summary>
        /// <param name="merchantManagement">The merchant management.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        bool WSUpdateMerchantManagement(MerchantManagementModel merchantManagement);

        /// <summary>
        /// Wses the delete merchant management.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        bool WSDeleteMerchantManagement(Guid id);

        /// <summary>
        /// Wses the change status merchant management.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        bool WSChangeStatusMerchantManagement(Guid id, bool status);

        /// <summary>
        /// Wses the get merchant management.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        MerchantManagementModel WSGetMerchantManagement(Guid id);

        /// <summary>
        /// Wses the get merchant management by user identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        MerchantManagementModel WSGetMerchantManagementByUserId(string id);

        /// <summary>
        /// Wses the get all merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        IEnumerable<MerchantManagementModel> WSGetAllMerchantManagement();

        /// <summary>
        /// Wses the exist merchant management by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        bool WSExistMerchantManagementByEmail(string email, Guid? id);
        /// <summary>
        /// Wses the name of the exist merchant management by business.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        bool WSExistMerchantManagementByBusinessName(string businessName, Guid? id);

        //search and paging
        /// <summary>
        /// Wses the get merchant management by filter.
        /// </summary>
        /// <param name="searchModel">The search model.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        SearchMerchantManagementModel WSGetMerchantManagementByFilter(SearchMerchantManagementModel searchModel);

        /// <summary>
        /// Wses the get merchant management by site identifier.
        /// </summary>
        /// <param name="siteId">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        MerchantManagementModel WSGetMerchantManagementBySiteId(string siteId);
        /// <summary>
        /// Wses the get count all merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        int WSGetCountAllMerchantManagement();
        /// <summary>
        /// Wses the get all country.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        List<string> WSGetAllCountry();
        /// <summary>
        /// Wses the get all site identifier.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        List<string> WSGetAllSiteID();
        [OperationContract]
        int WSGetNextProxyPort();
        /// <summary>
        /// Wses the name of the get merchant management by business.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        MerchantManagementModel WSGetMerchantManagementByBusinessName(string businessName);
    }
}
