﻿using Com.SmoovPOS.Entity;
using System.Collections.Generic;
using System.ServiceModel;

namespace SmoovPOS.Service.WebContents
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSWebContent" in both code and config file together.
    [ServiceContract]
    public interface IWSWebContent
    {
        [OperationContract]
        int WSCreateWebContent(WebContent webContent, string siteID);

        [OperationContract]
        int WSUpdateWebContent(WebContent webContent, string siteID);

        [OperationContract]
        WebContent WSDetailWebContent(string _id, string siteID);

        [OperationContract]
        int WSDeleteWebContent(string _id, string siteID);

        [OperationContract]
        IEnumerable<WebContent> WSGetAllWebContent(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSCountAllWebContent(string designDoc, string viewName, string siteID);

        [OperationContract]
        string WSGetPrimarySiteAlias(string siteID);
    }
}
