﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IQueue" in both code and config file together.
    [ServiceContract]
    public interface IWSQueue
    {
        [OperationContract]
        int WSCreateQueue(Com.SmoovPOS.Entity.Queue queue, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Queue WSDetailQueue(string _id, string siteID);

        [OperationContract]
        int WSUpdateQueue(Com.SmoovPOS.Entity.Queue queue, string siteID);

        [OperationContract]
        int WSDeleteQueue(Com.SmoovPOS.Entity.Queue queue, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Queue> WSGetAllQueue(string designDoc, string viewName, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Queue WSDetailQueueByBranchId(string designDoc, string viewName, string branch_id, string siteID);
    }
}
