﻿using Com.SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Queue" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Queue.svc or Queue.svc.cs at the Solution Explorer and start debugging.
    public class WSQueue : IWSQueue
    {
        public int WSCreateQueue(Com.SmoovPOS.Entity.Queue queue, string siteID)
        {
            try
            {
                QueueComponent component = new QueueComponent();
                return component.CreateNewQueue(queue, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Com.SmoovPOS.Entity.Queue WSDetailQueue(string _id, string siteID)
        {
            try
            {
                QueueComponent component = new QueueComponent();
                return component.DetailQueue(_id, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int WSUpdateQueue(Com.SmoovPOS.Entity.Queue queue, string siteID)
        {
            try
            {
                QueueComponent component = new QueueComponent();
                return component.UpdateQueue(queue, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int WSDeleteQueue(Com.SmoovPOS.Entity.Queue queue, string siteID)
        {
            try
            {
                QueueComponent component = new QueueComponent();
                queue.display = false;
                return component.UpdateQueue(queue, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Com.SmoovPOS.Entity.Queue> WSGetAllQueue(string designDoc, string viewName, string siteID)
        {
            try
            {
                QueueComponent component = new QueueComponent();
                return component.GetAllQueue(designDoc, viewName, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Com.SmoovPOS.Entity.Queue WSDetailQueueByBranchId(string designDoc, string viewName, string branch_id, string siteID)
        {
            try
            {
                QueueComponent component = new QueueComponent();
                return component.DetailQueueByBranchId(designDoc, viewName, branch_id, siteID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
