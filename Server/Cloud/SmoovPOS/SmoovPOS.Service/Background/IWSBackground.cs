﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Background
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSBackground" in both code and config file together.
    [ServiceContract]
    public interface IWSBackground
    {
        [OperationContract]
        void DoWork();
        [OperationContract]
        void AddBackgroundTask(BackGroundTask task);
        [OperationContract]
        void RemoveBackgroundTask(BackGroundTask task);
    }
}
