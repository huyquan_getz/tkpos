﻿using Com.SmoovPOS.Business.Components;
using Com.SmoovPOS.Entity;
using SmoovPOS.Entity;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Category
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSCategory" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSCategory.svc or WSCategory.svc.cs at the Solution Explorer and start debugging.

    public class WSCategory : IWSCategory
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}
        public int WSCreateCategory(Com.SmoovPOS.Entity.Category category, string siteID)
        {
            CategoryComponent categoryComp = new CategoryComponent();
            int StatusCode = categoryComp.CreateNewCategory(category, siteID);
            return StatusCode;
        }

        public Com.SmoovPOS.Entity.Category WSDetailCategory(string _id, string siteID)
        {
            //
            CategoryComponent categoryComp = new CategoryComponent();
            Com.SmoovPOS.Entity.Category category = categoryComp.DetailCategory(_id, siteID);
            return category;
        }

        public int WSUpdateCategory(Com.SmoovPOS.Entity.Category category, string siteID)
        {
            CategoryComponent categoryComp = new CategoryComponent();
            int StatusCode = categoryComp.UpdateCategory(category, siteID);
            return StatusCode;
        }

        public int WSDeleteCategory(string _id, string siteID)
        {
            CategoryComponent categoryComp = new CategoryComponent();
            int StatusCode = categoryComp.DeleteCategory(_id, siteID);
            return StatusCode;
        }

        public IEnumerable<Com.SmoovPOS.Entity.Category> WSGetAllCategoryNoLimit(string designDoc, string viewName, string siteID)
        {
            CategoryComponent categoryComp = new CategoryComponent();
            IEnumerable<Com.SmoovPOS.Entity.Category> ListCategory = categoryComp.GetAllCategoryActive(designDoc, viewName, siteID);
            return ListCategory;
        }

        public int WSCountAllCategory(string designDoc, string viewName, string siteID)
        {
            CategoryComponent categoryComp = new CategoryComponent();
            int ListCategory = categoryComp.CountAllCategory(designDoc, viewName, siteID);
            return ListCategory;
        }

        public int UpdateProductCategory(String id, CategoryProductItem product, string siteID)
        {   
            CategoryComponent categoryComp = new CategoryComponent();

            return categoryComp.UpdateProductCategory(id, product, siteID); 
        }
        public void UpdateNameCategory(string designDoc, string viewName, string oldName, string newName, string siteID)
        {
            CategoryComponent categoryComp = new CategoryComponent();
            categoryComp.UpdateNameCategory(designDoc, viewName, oldName, newName, siteID);
        }
        public SearchCategoryModel WSSearchCategory(string designDoc, string viewName, string siteID, NewPaging paging)
        {
            SearchCategoryModel ListCategories = new SearchCategoryModel();
            try
            {
                CategoryComponent CategoryComponent = new CategoryComponent();
                ListCategories = CategoryComponent.SearchAllCategory(designDoc, viewName, siteID, paging);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ListCategories;
        }

    }
}
