﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Category
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSCategory" in both code and config file together.
    [ServiceContract]
    public interface IWSCategory
    {

        [OperationContract]
        string GetData(int value);

        //[OperationContract]
        //CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here

        [OperationContract]
        int WSCreateCategory(Com.SmoovPOS.Entity.Category category, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Category WSDetailCategory(string _id, string siteID);

        [OperationContract]
        int WSUpdateCategory(Com.SmoovPOS.Entity.Category category, string siteID);

        [OperationContract]
        int WSDeleteCategory(string _id, string siteID);

        [OperationContract]
        int WSCountAllCategory(string designDoc, string viewName, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Category> WSGetAllCategoryNoLimit(string designDoc, string viewName, string siteID);

        [OperationContract]
        int UpdateProductCategory(String id, CategoryProductItem product, string siteID);

        [OperationContract]
        void UpdateNameCategory(string designDoc, string viewName, string oldName, string newName, string siteID);

        [OperationContract]
        SearchCategoryModel WSSearchCategory(string designDoc, string viewName, string siteID, NewPaging paging);
    }
}
