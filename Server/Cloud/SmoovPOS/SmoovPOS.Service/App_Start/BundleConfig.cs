﻿using System.Web;
using System.Web.Optimization;

namespace Com.Service
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/smoovposLibrary.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/angular.js",
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/jquery-ui.min.js",
                      "~/Scripts/jquery.nicescroll.js"
                     ));

            bundles.Add(new StyleBundle("~/Content/css_theme_1").Include(
                     "~/Content/bootstrap.css",
                      "~/Content/jquery-ui.css",
                     "~/Content/font-awesome-4.1.0/css/font-awesome.min.css"  

                     ));
        }
    }
}
