﻿
using SmoovPOS.Business.Business;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace SmoovPOS.Process.MailServer
{
    public class MailServerUtil
    {
        public Boolean SendMailOfficeSite(string displayName, string to, string subject, string body)
        {
            try
            {
                MailConfigBusiness mailConfigBusiness = new MailConfigBusiness();
                MailServerModel mailServer = mailConfigBusiness.GetConfigMailServer();
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(to);
                mail.From = new MailAddress(mailServer.UserName1, displayName, System.Text.Encoding.UTF8);
                mail.Subject = subject;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = body;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                SmtpClient client = new SmtpClient(mailServer.OutgoingSmtp, mailServer.OutgoingPort);

                //Add the Creddentials- use your own email id and password
                System.Net.NetworkCredential nt =
                new System.Net.NetworkCredential(mailServer.UserName1, mailServer.PassWord1);

                // client.Port = 587; // Gmail works on this port
                client.EnableSsl = mailServer.EnableSsl; //Gmail works on Server Secured Layer
                client.UseDefaultCredentials = false;
                client.Credentials = nt;
                client.Send(mail);
                return true;
            }
            catch (Exception e) {
                e.Message.ToString();
                return false;
            }
        }
       
        /// <summary>
        /// Send mail 
        /// </summary>
        /// <param name="type">int : type : 0 ->OfficeSite, 1 -> Addmin, 2 -> Merchant </param>
        /// <param name="displayName">string</param>
        /// <param name="to">string </param>
        /// <param name="subject">string</param>
        /// <param name="body">string</param>
        /// <returns></returns>
        public Boolean SendMail(int type,string displayName, string to, string subject, string body)
        {
            try
            {
                MailConfigBusiness mailConfigBusiness = new MailConfigBusiness();
                MailServerModel mailServer = mailConfigBusiness.GetConfigMailServer();
                string userName ="";
                string passWord ="";
                if(type == 0){
                    userName = mailServer.UserName1;
                    passWord = mailServer.PassWord1;
                }else if(type == 1){
                    userName = mailServer.UserName2;
                    passWord = mailServer.PassWord2;
                }else if(type == 2){
                    userName = mailServer.UserName3;
                    passWord = mailServer.PassWord3;
                }

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(to);
                mail.From = new MailAddress(userName, displayName, System.Text.Encoding.UTF8);
                mail.Subject = subject;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = body;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                SmtpClient client = new SmtpClient(mailServer.OutgoingSmtp, mailServer.OutgoingPort);

                //Add the Creddentials- use your own email id and password
                System.Net.NetworkCredential nt =
                new System.Net.NetworkCredential(userName, passWord);

                // client.Port = 587; // Gmail works on this port
                client.EnableSsl = mailServer.EnableSsl; //Gmail works on Server Secured Layer
                client.UseDefaultCredentials = true;
                client.Credentials = nt;
                client.Send(mail);
                return true;
            }
            catch (Exception e) {
                e.Message.ToString();
                return false;
            }
        }
        /// <summary>
        /// Test send mail 
        /// </summary>
        /// <param name="type">int : type : 0 ->OfficeSite, 1 -> Addmin, 2 -> Merchant</param>
        /// <param name="displayName">string</param>
        /// <param name="to">string</param>
        /// <param name="subject">string</param>
        /// <param name="objectMail">MailObjectModel</param>
        /// <returns>true or false</returns>
        public Boolean SendMailTest(int type,string displayName, string to, string subject, MailObjectModel objectMail){
            string createDate = objectMail.CreateDate;
            string body = "<div> <span>Dear Test</span><br/>Test send mail from server date {CreateDate} </div>";
           // HttpUtility.HtmlEncode(body).Replace("date", t.ToString());
            StringBuilder builder = new StringBuilder(body);
            builder.Replace("{CreateDate}", createDate.ToString());
            
            try
            {
                SendMail(type, displayName, to, subject, builder.ToString());
                return true;
            }
            catch (Exception e) {
                e.Message.ToString();
                return false;
            }
        }
    }
}