﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.ChatHub
{
    public class ChatHub : Hub
    {
        #region Data Members

        static List<UserDetail> ConnectedUsers = new List<UserDetail>();
        static List<MessageDetail> CurrentMessage = new List<MessageDetail>();

        #endregion

        #region Methods
        public void Send(string name, string message)
        {
            // Call the addNewMessageToPage method to update clients.
            Clients.All.addNewMessageToPage(name, message);
        }

        public void Connect(string userName, string stationId)
        {
            var id = Context.ConnectionId;
            if (ConnectedUsers.Count(x => x.ConnectionId == id) == 0)
            {
                var userDetail = new UserDetail() { ConnectionId = id, StationId = stationId, UserName = userName };
                ConnectedUsers.Add(userDetail);

                // send to caller
                Clients.Caller.onConnected(id, userName, new List<UserDetail>() { userDetail }, CurrentMessage);

                // send to all except caller client
                Clients.AllExcept(id).onNewUserConnected(stationId, id, userName);
            }
        }

        public void SendMessageToAll(string userName, string message)
        {
            // store last 100 messages in cache
            AddMessageinCache(userName, message);   
            //Clients.All.messageReceived(userName, message);
        }

        public void UpdateStatusOrderInMerchantPanel(string merchantID, string message)
        {
            var toUsers = ConnectedUsers.Where(x => x.StationId == merchantID).ToList();
            foreach (var item in toUsers)
            {
                // send to 
                Clients.Client(item.ConnectionId).sendPrivateMessage(item.ConnectionId, item.UserName, message);
            }
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                ConnectedUsers.Remove(item);

                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(id, item.UserName);

            }

            return base.OnDisconnected(stopCalled);
        }


        #endregion

        #region private Messages

        private void AddMessageinCache(string userName, string message)
        {
            CurrentMessage.Add(new MessageDetail { UserName = userName, Message = message });

            if (CurrentMessage.Count > 100)
                CurrentMessage.RemoveAt(0);
        }

        #endregion
    }
    public class UserDetail
    {
        public string ConnectionId { get; set; }
        public string StationId { get; set; }
        public string UserName { get; set; }
    }
    public class MessageDetail
    {

        public string UserName { get; set; }

        public string Message { get; set; }

    }
}