﻿
var isMobile = {
    Android: function () {
        return /Android/i.test(navigator.userAgent);
    },
    BlackBerry: function () {
        return /BlackBerry/i.test(navigator.userAgent);
    },
    iOS: function () {
        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
    },
    Windows: function () {
        return /IEMobile/i.test(navigator.userAgent);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
    }
};


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateSpecialCharactor(idElement) {
    var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-="
    var check = function (string) {
        for (i = 0; i < specialChars.length; i++) {
            if (string.indexOf(specialChars[i]) > -1) {
                return true
            }
        }
        return false;
    }

    $("#" + idElement).keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        var c = String.fromCharCode(e.keyCode);
        //alert(c);
        if (check(c) == true) {
            // Code that needs to execute when none of the above is in the string
            return false;
        }

    });

}
var isDismiss = "False";
//// script : active / inactive function
//jsProductItem = {};
$(function () {

    $("input:text").focus(function () { $(this).select(); });

    $('#modalInactive').on('hidden.bs.modal', function () {
        if (isDismiss != "False") {
            dismissPopup();
        }
    });

    //    $("input[type=radio]").change(function () {
    //        var status = $(this).val();
    //        jsProductItem.status = $(this).val();
    //        jsProductItem.value = this.getAttribute('id').toString();
    //        elementChange = $(this).parent().parent().children();
    //        var result = '';
    //        if (status == 'true') {
    //            $('#modalInactive').find('.modal-body').html(" Do you want to activate this discount?");
    //            $('#modalInactive').modal();
    //        } else {
    //            $('#modalInactive').find('.modal-body').html(" Do you want to deactivate this discount?");
    //            $('#modalInactive').modal();
    //        }
    //    });
});
//function dismissPopup() {
//    elementChange[0].classList.contains('active') ? elementChange[0].classList.remove('active') : elementChange[0].classList.add('active');
//    elementChange[1].classList.contains('active') ? elementChange[1].classList.remove('active') : elementChange[1].classList.add('active');
//    return true;
//}
//function confirmUpdate() {
//    isDismiss = "False";
//    return false;
//}

$('textarea[maxlength]').keyup(function () {
    //get the limit from maxlength attribute  
    var limit = parseInt($(this).attr('maxlength'));
    //get the current text inside the textarea  
    var text = $(this).val();
    //count the number of characters in the text  
    var chars = text.length;

    //check if there are more characters then allowed  
    if (chars > limit) {
        //and if there are use substr to get the text before the limit  
        var new_text = text.substr(0, limit);

        //and change the current text with the new text  
        $(this).val(new_text);
    }
});

function convertCurrrency(obj) {
    var valueCurrency;

    if (obj.indexOf(',') != -1) {

        valueCurrency = parseFloat(obj.replace(/,/g, ""))
            .toFixed(2)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    }
    else {
        valueCurrency = parseFloat(obj)
          .toFixed(2)
          .toString()
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    return valueCurrency;
}
function isEmpty(str) {
    return (!str || 0 === str.length);
}
function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}
function CalculateDimensions(sourceWidth, sourceHeight, maxWidth, maxHeight) {
    var widthPercent = maxWidth / sourceWidth;
    var heightPercent = maxHeight / sourceHeight;

    var percent = widthPercent;
    if (heightPercent < widthPercent) {
        percent = heightPercent;
    }

    var destWidth = (sourceWidth * percent);
    var destHeight = (sourceHeight * percent);

    var size = new Array(2);
    size[0] = destWidth;
    size[1] = destHeight;

    return size;
}
function AutoResizeWidthHeightImage(imageItem,elementResizeId, maxWidth, maxHeight) {
    var sourceWidth = imageItem.width();
    var sourceHeight = imageItem.height();

    //console.log(imageItem.attr('id'));
    //console.log(sourceWidth);
    //console.log(sourceHeight);

    var size = CalculateDimensions(sourceWidth, sourceHeight, maxWidth, maxHeight);
    var destWidth = size[0];
    var destHeight = size[1];

    elementResize = $('#' + elementResizeId);
    elementResize.removeAttr('width');
    elementResize.removeAttr('height');
    elementResize.attr('width', destWidth);
    elementResize.attr('height', destHeight);
    elementResize.css('Width', destWidth.toFixed(0).toString() + 'px');
    elementResize.css('Height', destHeight.toFixed(0).toString() + 'px');
}
function SetAutoResizeWidthHeightImage(elementResizeId, maxWidth, maxHeight) {
    var imageItem=$('#' + elementResizeId);
    var sourceWidth = imageItem.width();
    var sourceHeight = imageItem.height();
    if (sourceWidth > 0 && sourceHeight > 0) {
        AutoResizeWidthHeightImage(imageItem, elementResizeId, maxWidth, maxHeight);
    }
    //else {
    //    setTimeout(function () {
    //        console.log(imageItem.attr('id'));
    //        SetAutoResizeWidthHeightImage(elementResizeId, maxWidth, maxHeight);
    //    }, 1000);
    //}
}