﻿$(document).ready(function () {
    $('.OrderDatePicker').unbind();
    $('.OrderDatePicker').off();
    $('.OrderDatePicker').datetimepicker({
        autoclose: true,
        language: 'pt-BR'
    })
    .on('changeDate', function (e) {
        FilterOrder(e);
        $(this).datetimepicker('hide');
    });

    $('form.SearchForm').unbind();
    $('form.SearchForm').off();
    $('form.SearchForm').submit(function (e) {
        e.preventDefault();
        SearchOrderSubmit(e.currentTarget);
        return false;
    });
});

function FilterOrder(e) {
    //debugger;
    //var table = $('.tab-content .active table').DataTable();
    //if (table.rows().data().length > 0) {
        //table.search($(e.currentTarget).val()).draw();
    //} else {
        var form = $(e.currentTarget).parents("form")[0];
        SearchOrderSubmit(form);
    //}
}

function SearchOrderSubmit(form) {
    var tabElement = currentTab;
    if ($(form).valid()) {
        $.ajax({
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function (result) {
                //$(".tab-content .active").html(result);
                $($("#" + tabElement).find('#pageCurrent')).val('');
                $("#" + tabElement + " li").each(function () {
                    $(this).removeClass("active");
                });
                var div_Main = $('#' + tabElement);
                div_Main.fadeIn();
                div_Main.html(result);
                $('#divpaging').html($('#divpaging2').html());
                $('#DeliveryTab').removeAttr('style');
                $('#SelfCollectTab').removeAttr('style');
                $('#CancelOrderTab').removeAttr('style');
            },
            error: function (error) {
                //debugger;
            },
            beforeSend: function () {
                //$('#' + tabElement + ' .loading').css("display", "block");
                $('.loading').css("display", "block");
            },
            complete: function () {
                //$('#' + tabElement + ' .loading').css("display", "none");
                $('.loading').css("display", "none");
            }
        });
    }

    return false;
}