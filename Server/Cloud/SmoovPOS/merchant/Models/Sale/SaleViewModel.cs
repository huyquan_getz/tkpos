﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class SaleViewModel
    {
        public string ReportType { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsNextDay { get; set; }

        public string Currency { get; set; }

        public SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel listReportOrders { get; set; }

        public double TotalSales { get; set; }

        public double TotalRevenue { get; set; }

        public int TotalNumberOfOrders { get; set; }

        public double AverageOrderSize { get; set; }
    }
}