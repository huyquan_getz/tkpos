﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models.DeviceToken
{
    public class DetailDeviceViewModel
    {      
        //[LocalizedRequired]       
        //[LocalizedRemote("IsExistedToken", "ManageDevice", AdditionalFields = "idDevice", ResourceCode = "TokenExisted")]
        [LocalizedDisplayName("UUID")]
        public string uuid { get; set; }
        [LocalizedDisplayName("DeviceToken")]
        public string token { get; set; }
        [LocalizedDisplayName("Name")]
        public string name { get; set; }
        [LocalizedDisplayName("SystemName")]
        public string systemName { get; set; }
        [LocalizedDisplayName("SystemVersion")]
        public string systemVersion { get; set; }
        [LocalizedDisplayName("Model")]
        public string model { get; set; }
        [LocalizedDisplayName("OperatingSystem")]
        public string os { get; set; }
        public bool status { get; set; }
        public int badge { get; set; }
        public string appType { get; set; }
        public string userOwner { get; set; }
        public string bucket { get; set; }
        public Guid? merchantID { get; set; }
        public Guid deviceTokenId { get; set; }
        public int branchIndex { get; set; }
    }
}