﻿using SmoovPOS.Entity.DeviceToken;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models.DeviceToken
{
    public class DeviceViewModel
    {
        public ManageDevice manageDevice;
        public DetailDeviceViewModel Device;
    }
}