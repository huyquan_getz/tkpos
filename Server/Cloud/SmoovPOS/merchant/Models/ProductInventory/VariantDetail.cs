﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.ProductInventory
{
    public class VariantDetail
    {
        public string idVariant { get; set; }
        public int indexVariant { get; set; }
    }
}