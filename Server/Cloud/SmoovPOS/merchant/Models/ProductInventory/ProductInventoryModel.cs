﻿using SmoovPOS.Utility.CustomAttributes;
using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models
{
    //-Created by MaoNguyen
    public class ProductInventoryModel
    {
        [LocalizedRequired]
        [LocalizedDisplayName("Name")]
        public string name { get; set; }
        public string Category { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("ShortNameOnly3character")]
        public string shortName { get; set; }

        public string description { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("UnitType")]
        public string unitType { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("Brand")]
        public string brand { get; set; }

        public string fileInput { get; set; }

        [LocalizedDisplayName("ChooseFile")]
        public string chooseFile { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("SalingPrice")]
        public string salingPrice { get; set; }

        [LocalizedDisplayName("CompareAtPrice")]
        public string compareAtPrice { get; set; }

        [LocalizedDisplayName("CostPriceOnlyForReport")]
        public string costPriceOnlyForReport { get; set; }

        [LocalizedDisplayName("SKU")]
        public string SKU { get; set; }

        [LocalizedDisplayName("WeightKG")]
        public string weightKG { get; set; }

        [LocalizedDisplayName("OptionName")]
        public string optionName { get; set; }

        [LocalizedDisplayName("Policy")]
        [RegularExpression(@"[0-9]+([,\.][0-9]+)?")]
        public string policy { get; set; }


        [LocalizedDisplayName("Quantity")]
        public string quantity { get; set; }

         [LocalizedDisplayName("OptionValue")]
        public string optionValue { get; set; }

        [LocalizedDisplayName("RemindMeWhenStockReaches")]
        [RegularExpression(@"[0-9]+([,\.][0-9]+)?")]
        public string remindMeWhenStockReaches { get; set; }

        public bool trackWeight { get; set; }
        public bool productOption { get; set; }

        public List<SelectListItem> listCategories{get;set;}
        public List<SelectListItem> listPolicyName { get; set; }
        public List<SelectListItem> listOptionName { get; set; }
    }
}