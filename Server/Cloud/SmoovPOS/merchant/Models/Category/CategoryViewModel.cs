﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class CategoryViewModel
    {
        [LocalizedRequired]
        [LocalizedDisplayName("CategoryName")]
        public string categoryName { get; set; }

         [LocalizedDisplayName("description")]
        public string description { get; set; }
    }
}