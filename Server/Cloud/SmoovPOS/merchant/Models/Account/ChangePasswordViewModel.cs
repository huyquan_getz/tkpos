﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class ChangePasswordViewModel
    {
        public string userID { get; set; }
        [LocalizedRequired]
        [LocalizedStringLength(100, MinimumLength = 6, Name = "NewPasswordLength")]//Your password must be at least 6 characters long.
        [DataType(DataType.Password)]
        [LocalizedDisplayName("NewPassword")]
        //[RegularExpression(@"^(?=.*\d)(?=.*[a-zA-Z]).{8,100}$", ErrorMessage = "Password must be minimum length of 8 and contains numeric and capital character.")]
        public string NewPassword { get; set; }
        //[LocalizedRemote("IsExistedName", "ExactLocation", AdditionalFields = "Id", ResourceCode = "NameExisted")]
        [LocalizedRequired]
        [DataType(DataType.Password)]
        [LocalizedDisplayName("ConfirmPassword")]
        //[LocalizedStringLength(100, MinimumLength = 6, Name = "ConfirmPassword")]
        [LocalizedCompare("NewPassword", Name = "ConfirmPasswordCompare")]//Your passwords do not match.
        public string ConfirmPassword { get; set; }
    }
}