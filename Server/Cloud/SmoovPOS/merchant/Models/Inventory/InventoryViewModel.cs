﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Entity.CBEntity;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models
{
    public class InventoryViewModel : CBBaseEntity
    {
        [LocalizedRequired]
        [LocalizedDisplayName("businessID")]
         public string businessID { get; set; }

        [LocalizedDisplayName("description")]
        public string description { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("Contact")]
         public string contact { get; set; }

        public string contactMobile { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("StreetAddress")]
         public string streetAddress { get; set; }
                
        [LocalizedRequired]
        [LocalizedDisplayName("city")]
        public string city { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("zipCode")]
         public string zipCode { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("state")]
        public string state { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("country")]
        public string country { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("TimeZone")]
        public string timeZone{ get; set; }

        public List<SelectListItem> listCountries { get; set; }

        public Dictionary<string, string> listDefaultTimeZones { get; set; }

        public List<SelectListItem> listContacts { get; set; }
    }
}