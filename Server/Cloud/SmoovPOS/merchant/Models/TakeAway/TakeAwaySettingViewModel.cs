﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using SmoovPOS.UI.Models.CommonModel;
using SmoovPOS.Entity.Entity.Settings;

namespace SmoovPOS.UI.Models
{
    public class TakeAwaySettingViewModel
    {

        //private TableOrderingSetting _TableOrderingSetting;

        public string SelectedBranchId { get; set; }

        public List<Inventory> ListInventory { get; set; }

        //public TableOrderingSetting TableOrderingSetting 
        //{
        //    get { return _TableOrderingSetting; }
        //    set 
        //    {
        //        _TableOrderingSetting = value;
        //    }
        //}

        public string table
        {
            get { return "TakeAway"; }
        }

        //private TableOrderingSetting ValidateSetting(TableOrderingSetting tableOrderingSetting)
        //{
        //    if (tableOrderingSetting != null)
        //    {
        //        if (tableOrderingSetting.ListDayOfWeekSetting != null)
        //        {
        //            foreach (DayOfWeekSetting dows in tableOrderingSetting.ListDayOfWeekSetting)
        //            {
        //                if (dows.Option == 0)
        //                {

        //                    if (string.IsNullOrEmpty(dows.Open_Opening) || string.IsNullOrEmpty(dows.Open_Closing))
        //                        throw new NotSupportedException("Not valid time");
        //                }
        //            }
        //        }
        //    }
            
        //    return tableOrderingSetting;
        //}

        public string BranchId { get; set; }
        public string BranchName { get; set; }
        public int BranchIndex { get; set; }
        public bool AcceptTakeAway { get; set; }
        public string TakeAwayInstructions { get; set; }
        
        [ValidateDayOfWeekSetting("ListDayOfWeekSetting", ErrorMessage = "Wrong input.")]
        public List<DayOfWeekSetting> ListDayOfWeekSetting { get; set; }

        public bool isAddNew { get; set; }
    }
}