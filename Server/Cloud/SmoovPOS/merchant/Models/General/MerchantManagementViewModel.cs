﻿using SmoovPOS.Entity.Models;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models.General
{
    public class MerchantManagementViewModel 
    {
        [LocalizedDisplayName("BusinessDisplayName")]
        public string businessName { get; set; }

        [LocalizedDisplayName("TitleHomePage")]
        public string titleHomePage { get; set; }          
        [LocalizedDisplayName("Title")]
        public string title { get; set; }
        [LocalizedDisplayName("MerchantName")]
        public string firstName { get; set; }
        [LocalizedDisplayName("LastName")]
        public string lastName { get; set; }
        [LocalizedDisplayName("RegionProvineState")]
        public string region { get; set; }
        [LocalizedDisplayName("PhoneNumber")]
        public string phone { get; set; }
        [LocalizedDisplayName("Gender")]
        public System.Nullable<bool> gender { get; set; }
        [LocalizedDisplayName("Birthday")]
        public System.Nullable<System.DateTime> birthday { get; set; }
        [LocalizedDisplayName("Address")]
        public string address { get; set; }
        [LocalizedDisplayName("city")]
        public string city { get; set; }
        [LocalizedDisplayName("zipCode")]
        public string zipCode { get; set; }
        [LocalizedDisplayName("country")]
        public string country { get; set; }
        [LocalizedDisplayName("PhoneNumber")]
        public string phoneRegion { get; set; }

        public System.Nullable<bool> CheckSubdomain { get; set; }
        [LocalizedDisplayName("SubDomain")]
        public string SubDomain { get; set; }
        [LocalizedDisplayName("Domain")]
        public string Domain { get; set; }
        public System.Guid merchantID { get; set; }
    }
}