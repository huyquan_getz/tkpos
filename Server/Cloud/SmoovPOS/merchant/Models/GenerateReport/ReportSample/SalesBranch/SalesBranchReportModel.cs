﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.GenerateReport.ReportSample.SalesBranch
{
    public class SBRGeneralModel
    {
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsNextDay { get; set; }
        public string Currency { get; set; }
        public string BranchName { get; set; }
        public List<Com.SmoovPOS.Entity.Order> ListPaidOrders { set; get; }
        public List<Com.SmoovPOS.Entity.Order> ListRefundedOrders { set; get; }
        
        public string Date { get; set; }
        public string Range { get; set; }
        public string TotalSales { get; set; }
        public string BestSellingItem { get; set; }
        public List<SBRProductDetailModel> ListProductVariants { set; get; }
    }

    public class SBRProductDetailModel
    {
        //Hidden fields
        public string VariantSKU { get; set; }

        //Display fields
        public string Category { get; set; }
        public string Product { get; set; }
        public int QuantitySold { get; set; }
        public int QuantitySoldToday { get; set; }
        public string QuantityLeft { get; set; }
        public double SalesCollected { get; set; }
        public double CostOfGoodsSold { get; set; }
        public double GrossProfit { get; set; }
        public string GrossMargin { get; set; }
        public string PaymentType { get; set; }
    }
}