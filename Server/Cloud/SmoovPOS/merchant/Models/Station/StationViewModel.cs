﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Station
{
    public class StationViewModel
    {
        public List<Com.SmoovPOS.Entity.Inventory> ListBranches { set; get; }
        public string SelectedBranchName { set; get; }
        public List<StationItemViewModel> ListStations { set; get; }
        public StationDetailViewModel StationDetail { set; get; }
        public List<string> ListStationNames { set; get; }
    }
}