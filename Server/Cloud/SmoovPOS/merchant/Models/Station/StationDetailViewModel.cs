﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class StationDetailViewModel
    {
        public string BranchName { get; set; }
        public int BranchIndex { get; set; }
        public string StationID { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("Name")]
        [LocalizedRemote("IsExistedStation", "Station", AdditionalFields = "StationID, BranchIndex", ResourceCode = "StationExisted")]
        public string StationName { get; set; }
        [LocalizedRequired]
        [Range(1, int.MaxValue, ErrorMessage = "Please input a number that is greater than {1}.")]
        [LocalizedDisplayName("StationSize")]
        public int StationSize { get; set; }
    }
}