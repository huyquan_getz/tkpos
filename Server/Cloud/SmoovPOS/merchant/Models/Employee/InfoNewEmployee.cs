﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models
{
    public class InfoNewEmployee
    {
        public List<Permission> arrPermission { get; set; }
        public Country arrCountry { get; set; }
        public Inventory[] inventoryList { get; set; }
        public Role[] roleList { get; set; }
        public Com.SmoovPOS.Entity.Employee Employee { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("FirstName")]
        public string firstName { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("LastName")]
        public string lastName { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("Gender")]
        public string Gender { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("Birthday")]
        public string Birthday { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("PhoneNumber")]
        public string phoneNumber { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("HiredDate")]
        public string hiredDate { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("Address")]
        public string Address { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("city")]
        public string city { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("zipCode")]
        public string zipCode { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("region")]
        public string state { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("country")]
        public string country { get; set; }

        [LocalizedRequired]
        [EmailAddress]
        [LocalizedDisplayName("email")]
        [LocalizedRemote("IsExistedEmployee", "Employee", AdditionalFields = "id", ResourceCode = "EmailExist")]
        public string email { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("pinCode")]
        public string pinCode { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("Password")]
        [StringLength(100, ErrorMessage = "Please enter a password with at least 6 characters", MinimumLength = 6)]
        public string Password { get; set; }

        //[LocalizedRequired]
        [LocalizedDisplayName("confirmPass")]
        [System.Web.Mvc.CompareAttribute("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string confirmPassword { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("account")]
        public string account { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("accountInfor")]
        public string accountInformation { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("permission")]
        public string permission { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("Store")]
        public string Store { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("role")]
        public string Role { get; set; }

        public List<SelectListItem> listMale { get; set; }
        public List<SelectListItem> listCountries { get; set; }
    }
}