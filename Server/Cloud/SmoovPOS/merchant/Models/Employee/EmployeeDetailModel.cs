﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class EmployeeDetailModel
    {
        public Com.SmoovPOS.Entity.Employee Employee { get; set; }
        public IEnumerable<Order> OrderList { get; set; }
        public string CurrencyDisplay
        {
            get
            {
                //Get and set merchant's currency
                WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
                string currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, VariableConfigController.GetBucket()).Currency;
                merchantGeneralSettingClient.Close();
                return currency;
            }
        }
        public IEnumerable<Order> OrderListSelfPOS { get; set; }
        public IEnumerable<Order> OrderListSelfPOSThisMonth { get; set; }
        public IEnumerable<Order> OrderListSelfPOSLastMonth { get; set; }

        public IEnumerable<Order> OrderListSelfOnl { get; set; }
        public IEnumerable<Order> OrderListSelfOnlThisMonth { get; set; }
        public IEnumerable<Order> OrderListSelfOnlLastMonth { get; set; }

        public IEnumerable<Order> OrderListDelivery { get; set; }
        public IEnumerable<Order> OrderListDeliveryThisMonth { get; set; }
        public IEnumerable<Order> OrderListDeliveryLastMonth { get; set; }

        double PointThisMonth { get; set; }
        double PointLastMonth { get; set; }
        double PointAllTime { get; set; }
       
    }
}