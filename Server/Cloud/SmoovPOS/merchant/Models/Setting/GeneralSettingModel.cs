﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Setting
{
    public class GeneralSettingModel
    {
        public Com.SmoovPOS.Entity.MerchantGeneralSetting generalSetting { get; set; }
        public Dictionary<string, string> listDefaultTimeZones { get; set; }
        public List<CurrencyItem> listDefaultCurrencies { get; set; }
    }
}