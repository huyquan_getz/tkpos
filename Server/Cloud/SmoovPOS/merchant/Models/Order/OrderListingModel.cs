﻿using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmoovPOS.UI.Models
{
    public class OrderListingModel
    {
        public IEnumerable<OrderModel> AllOrderList { get; set; }

        private OrderItemListingModel deliveryOrderListing;
        public OrderItemListingModel DeliveryOrderListing
        {
            get
            {
                if (deliveryOrderListing == null && AllOrderList != null)
                {
                    deliveryOrderListing = new OrderItemListingModel();
                    deliveryOrderListing.TableId = ConstantSmoovs.Order.DeliveryTable;
                    deliveryOrderListing.TabId = ConstantSmoovs.Order.DeliveryTab;
                    int deliveryOrderType = (int)Convert.ChangeType(ConstantSmoovs.Enums.OrderType.Delivery, ConstantSmoovs.Enums.OrderType.Delivery.GetTypeCode());
                    deliveryOrderListing.SearchOrder.OrderType = deliveryOrderType;
                    deliveryOrderListing.OrderList = AllOrderList.Where(r => r.orderType == deliveryOrderType);
                }

                return deliveryOrderListing;
            }
            set { deliveryOrderListing = value; }
        }

        private OrderItemListingModel selfCollectOrderListing;
        public OrderItemListingModel SelfCollectOrderListing
        {
            get
            {
                if (selfCollectOrderListing == null && AllOrderList != null)
                {
                    selfCollectOrderListing = new OrderItemListingModel();
                    selfCollectOrderListing.TableId = ConstantSmoovs.Order.SelfCollectTable;
                    selfCollectOrderListing.TabId = ConstantSmoovs.Order.SelfCollectTab;
                    int selfCollectOrderType = (int)Convert.ChangeType(ConstantSmoovs.Enums.OrderType.SelfCollection, ConstantSmoovs.Enums.OrderType.SelfCollection.GetTypeCode());
                    selfCollectOrderListing.SearchOrder.OrderType = selfCollectOrderType;
                    selfCollectOrderListing.OrderList = AllOrderList.Where(r => r.orderType == selfCollectOrderType);
                }

                return selfCollectOrderListing;
            }
            set { selfCollectOrderListing = value; }
        }

    }
}