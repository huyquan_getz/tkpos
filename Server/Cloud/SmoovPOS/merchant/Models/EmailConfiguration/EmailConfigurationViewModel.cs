﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class EmailConfigurationViewModel
    {
        public string Id { get; set; }
        [LocalizedRequired]
        public string IncomingPop { get; set; }
        [LocalizedRequired]
        [RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        [LocalizedStringLength(5, MinimumLength = 1, ErrorMessage = "IncomingPort")]
        public int IncomingPort { get; set; }
        [LocalizedRequired]
        [EmailAddress(ErrorMessage = "Please enter a valid email address.")]
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        [LocalizedRequired]
        public string PassWord { get; set; }
        [LocalizedRequired]
        public string OutgoingSmtp { get; set; }
        //[Required(ErrorMessage = "Field Port is required")]
        // [Range(0, int.MaxValue, ErrorMessage = "Field Port must be a number")]
        //[StringLength(5)]
        [LocalizedRequired]
        [RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        [LocalizedStringLength(5, MinimumLength = 1, ErrorMessage = "OutgoingPort")]
        public int OutgoingPort { get; set; }
        public bool EnableSsl { get; set; }
        public bool Credentials { get; set; }

        //public string UserName1 { get; set; }
        //public string PassWord1 { get; set; }
        //public string UserName2 { get; set; }
        //public string PassWord2 { get; set; }
        //[LocalizedRequired]
        //[EmailAddress(ErrorMessage = "Please enter a valid email address.")]
        //public string UserName3 { get; set; }
        //[DataType(DataType.Password)]
        //[LocalizedRequired]
        //public string PassWord3 { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid? merchantID { get; set; }
        public string defaultEmail { get; set; }
        //public string customerEmail { get; set; }

        public string smtpPlaceHolder { get; set; }
        public string popPlaceHolder { get; set; }

        public string smtpPortPlaceHolder { get; set; }
        public string popPortPlaceHolder { get; set; }

        public bool isApplyMail { get; set; }
    }
}