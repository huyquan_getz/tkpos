﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SmoovPOS.UI.Models
{
    public class CustomersViewModel
    {
        public string _id { get; set; }
        
        public DateTime createdDate { get; set; }
        public DateTime modifiedDate { get; set; }
        public string userID { get; set; }
        public string userOwner { get; set; }
        public Boolean status { get; set; }
        public bool display { get; set; }
        public string[] channels { get; set; }
        public string DateTimeInt { get; set; }
        public string memberID { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("FirstName")]
        public string firstName { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("LastName")]
        public string lastName { get; set; }
        public string fullName { get; set; }
        public int gender { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("DoB")]
        public string birthday { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("PhoneNumber")]
        public string phonenumber { get; set; }
        
        public string hiredDate { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("StreetAddress")]
        public string address { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("city")]
        public string city { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("zipCode")]
        public string zipCode { get; set; }
        public string email { get; set; }
        public string pinCode { get; set; }
        public string password { get; set; }
        public string passwordConfirm { get; set; }
        public string image { get; set; }
        public string country { get; set; }
        public Country listCountry { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("region")]
        public string state { get; set; }

        public string remark { get; set; }
        public bool acceptsNewsletter { get; set; }
        public bool acceptsSms { get; set; }
        public List<Tags> tags { get; set; }
        public DateTime lastLogin { get; set; }
        public DateTime lastOrder { get; set; }
        public int totalOrder { get; set; }
        public double totalSpend { get; set; }
        public List<Orderdetail> listOrder { get; set; }
        public Boolean active { get; set; }
        public string strTags { get; set; }
        public string addressCombine { get; set; }
        public string phoneRegion { get; set; }
        public Boolean verify { get; set; }
        public string timeZoneFullName { get; set; }

        public string tableId;
        public string Amount { get; set; }
      
    }
}