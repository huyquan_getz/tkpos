﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Entity.CBEntity;
using SmoovPOS.Entity.BaseEntity;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models
{
    public class CollectionViewModel : CBBaseEntity
    {
        [LocalizedRequired]
        [LocalizedDisplayName("Name")]
        public string name { get; set; }
        [LocalizedDisplayName("description")]
        public string description { get; set; }
        //[LocalizedRequired]
        //[LocalizedDisplayName("BeginDate")]
        //public System.Nullable<System.DateTime> beginDate { get; set; }
        ////[LocalizedRequired]
        //public System.Nullable<System.DateTime> endDate { get; set; }
        [LocalizedDisplayName("defaultSortProduct")]
        public string defaultSortProduct { get; set; }
        public string image { get; set; }
        public IEnumerable<SmallProductItem> arrayProduct { get; set; }
        [LocalizedDisplayName("isNeverExpires")]
        public bool isNeverExpires { get; set; }
        public List<SelectListItem> defaultSortList { get; set; }

        public List<Com.SmoovPOS.Entity.Category> listCategories { get; set; }

        public List<SmallProductItem> listProductItems { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    // check 1: Start Date should be before End Date
        //    if (beginDate != null && endDate != null)
        //    {
        //        //if (DateTime.Compare(beginDate.Value, endDate.Value) > 0)
        //        if(beginDate.Value > endDate.Value)
        //        {
        //            var msg = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("EnddateVSBegindate");
        //            yield return new ValidationResult(msg);
        //        }
        //    }
        //}

        //public string selectedProducts
        //{
        //    get
        //    {
        //        var strArrayProducts = new StringBuilder();
        //        if (arrayProduct.Count() > 0)
        //        {
        //            foreach (var item in arrayProduct)
        //            {
        //                if (strArrayProducts.Length <= 0)
        //                {
        //                    strArrayProducts.Append(item._id);
        //                }
        //                else
        //                {
        //                    strArrayProducts.Append(",");
        //                    strArrayProducts.Append(item._id);
        //                }
        //            }
        //        }

        //        return strArrayProducts.ToString();
        //    }
        //}

    }
}