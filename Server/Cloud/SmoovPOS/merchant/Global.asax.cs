﻿using log4net;
using SmoovPOS.Common;
using SmoovPOS.UI.App_Start;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSSiteReference;
using SmoovPOS.Utility.WSServicePacketReference;

namespace Com.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MvcApplication));

        void Application_Error(Object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError().GetBaseException();

            log.Error("App_Error", ex);
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoFacConfig.RegisterContainers();
            AutoMapperConfig.RegisterProfiles();

            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
        }
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            if (HttpContext.Current.Session != null && string.IsNullOrEmpty(VariableConfigController.GetBucket()) && !string.IsNullOrEmpty(User.Identity.Name))
            {
                IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                var merchant = client.WSGetMerchantManagementByUserId(User.Identity.GetUserId());
                client.Close();

                if (merchant != null && !string.IsNullOrEmpty(merchant.bucket))
                {
                    HttpContext.Current.Session[ConstantSmoovs.Users.bucket] = merchant.bucket;
                    HttpContext.Current.Session[ConstantSmoovs.Users.businessName] = merchant.businessName;

                    if (!string.IsNullOrEmpty(merchant.bucket))
                    {
                        WSSiteClient clientSite = new WSSiteClient();
                        var siteAlias = clientSite.WSGetSiteAliasBySiteID(merchant.bucket);
                        if (siteAlias != null)
                        {
                            HttpContext.Current.Session[ConstantSmoovs.Users.domain] = siteAlias.HTTP;
                            HttpContext.Current.Session[ConstantSmoovs.Users.subdomain] = siteAlias.HTTPAlias;
                            HttpContext.Current.Session[ConstantSmoovs.Users.checksubdomain] = siteAlias.CheckSubdomain.GetValueOrDefault();
                        }

                        if (merchant.servicePacketID.HasValue)
                        {
                            IWSServicePacketClient clientServicePacket = new IWSServicePacketClient();
                            var servicePacket = clientServicePacket.WSGetServicePacket(merchant.servicePacketID.GetValueOrDefault());
                            clientServicePacket.Close();
                            HttpContext.Current.Session[ConstantSmoovs.Users.servicePackage] = servicePacket;
                        }
                    }
                }
            }

        }
        protected void Session_Start(object sender, EventArgs e)
        {
            //var urlActive = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteMerchantSmoovPOS];
            Response.Redirect("~/Account/Login");
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/Login");
        }
        public void Session_OnEnd()
        {
            Response.Redirect("~/Account/Login");
            // do your desired task when the session expires
            System.Web.Security.FormsAuthentication.SignOut();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        }
        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    if (Context.Session != null)
        //    {
        //        if (Session.IsNewSession)
        //        {
        //            HttpCookie newSessionIdCookie = Request.Cookies["ASP.NET_SessionId"];
        //            if (newSessionIdCookie != null)
        //            {
        //                string newSessionIdCookieValue = newSessionIdCookie.Value;
        //                if (newSessionIdCookieValue != string.Empty)
        //                {
        //                    // This means Session was timed Out and New Session was started
        //                    Response.Redirect("~/Account/Login");
        //                }
        //            }
        //        }
        //    }
        //}
    }
}
