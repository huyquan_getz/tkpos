﻿using SmoovPOS.Business.Components;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmoovPOS.Utility.WSSMSSettingReference;
using SmoovPOS.UI.Models.SMSSetting;
using Com.SmoovPOS.Entity;

namespace SmoovPOS.UI.Controllers
{
    public class SMSSettingController : BaseController
    {
        string SiteID = string.Empty;
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.SiteID = VariableConfigController.GetBucket();
        }
        // GET: SMSSetting
        public ActionResult Index()
        {
            ViewBag.Class = "SMSSetting";
            SMSSettingModel model = new SMSSettingModel();
            WSSMSSettingClient client = new WSSMSSettingClient();
            var smsSettings = client.WSGetAllSMSSetting(this.SiteID);
            if (smsSettings == null || (smsSettings != null && smsSettings.Count() == 0))
            {
                return View(model);
            }
            return RedirectToAction("Detail");
        }

        public ActionResult Edit(SMSSettingModel model)
        {
            WSSMSSettingClient client = new WSSMSSettingClient();
            SMSSetting smssetting = client.WSGetAllSMSSetting(this.SiteID).FirstOrDefault();
            if (smssetting == null)
            {

                smssetting = new Com.SmoovPOS.Entity.SMSSetting()
                {
                    _id = Guid.NewGuid().ToString(),
                    channels = new string[] { "server", "app" },
                    ApiKey = model.ApiKey,
                    SecretCode = model.SecretCode
                };
                client.WSCreateSMSSetting(smssetting, this.SiteID);
            }
            else
            {
                smssetting.ApiKey = model.ApiKey;
                smssetting.SecretCode = model.SecretCode; 
                client.WSUpdateSMSSetting(smssetting, this.SiteID);
            }
            client.Close();
            return RedirectToAction("Detail");
        }

        public ActionResult Detail()
        {
            ViewBag.Class = "SMSSetting";
            return View();
        }

        public ActionResult Remove()
        {
            WSSMSSettingClient client = new WSSMSSettingClient();
            SMSSetting smssetting = client.WSGetAllSMSSetting(this.SiteID).FirstOrDefault();
            if (smssetting != null)
            {
                client.WSDeleteSMSSetting(smssetting._id);
            }
            client.Close();
            return RedirectToAction("Index");
        }
    }
}