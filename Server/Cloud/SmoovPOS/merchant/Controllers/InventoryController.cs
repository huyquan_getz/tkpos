﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Com.SmoovPOS.Entity;
using SmoovPOS.UI.Models;
using SmoovPOS.UI.Models.ProductInventory;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models.Setting;
using SmoovPOS.Common;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSProductItemReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using SmoovPOS.Entity;


namespace SmoovPOS.UI.Controllers
{
    public class InventoryController : BaseController
    {
        [Authorize]
        //
        // GET: /Inventory/
        public ActionResult Index(string sort = "index", string type = "title", string text = "", int pageGoto = 1, int limit = 25, bool desc = false)
        {
            ViewBag.Class = "inventory";

            NewPaging paging = new NewPaging();
            paging.limit = limit;
            paging.pageGoTo = pageGoto;
            paging.text = text;
            paging.sort = sort;
            paging.type = type;
            paging.desc = desc;

            WSInventoryClient client = new WSInventoryClient();
            var listStores = client.WSGetAllInventory(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket(), paging);
            client.Close();
            //ViewBag.NumberInit = CheckInitiation();

            //Get/Set the quantity of all current branches to session
            int count = listStores.Paging.total - 1; //Don't count inventory OnlineShop
            Session[ConstantSmoovs.Stores.BranchCount] = count;
            //Permission module
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            listStores.isAddNew = userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Branch, count);
            ViewBag.NumberInit = CheckInitiation();
            return View(listStores);
        }

        [Authorize]
        public ActionResult Create()
        {
            //Permission with create when limit Stores
            int count = 0;
            if (Session[ConstantSmoovs.Stores.BranchCount] == null)
            {
                NewPaging paging = new NewPaging();
                paging.limit = 25;
                paging.pageGoTo = 1;
                paging.text = "";
                paging.sort = "index";
                paging.type = "title";
                paging.desc = false;

                WSInventoryClient clientIn = new WSInventoryClient();
                var listCollections = clientIn.WSGetAllInventory(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket(), paging);
                clientIn.Close();

                //Get/Set the quantity of all current branches to session
                count = listCollections.Paging.total - 1; //Don't count inventory OnlineShop
                Session[ConstantSmoovs.Stores.BranchCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Stores.BranchCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Branch, count))
            {
                return RedirectToAction("Index");
            }

            SmoovPOS.UI.Models.InventoryViewModel inventoryViewModel = new InventoryViewModel();
            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "inventory";

            WSInventoryClient clientInventory = new WSInventoryClient();
            inventoryViewModel.listCountries = new List<SelectListItem>();

            //--20150401 MaoNguyen ADD ↓
            var resultCountry = clientInventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, VariableConfigController.GetBucket()).Select(r => r.arrayCountry).ToList();
            clientInventory.Close();

            foreach (string country in resultCountry[0])
            {
                inventoryViewModel.listCountries.Add(new SelectListItem() { Text = country, Value = country });
            }
            //-------------------------------------------------------------------------
            inventoryViewModel.listDefaultTimeZones = SmoovPOS.Common.ConstantSmoovs.Enums.ListTimeZone;

            inventoryViewModel.listContacts = new List<SelectListItem>();
            inventoryViewModel.listContacts.Add(new SelectListItem() { Text = "+65", Value = "65" });
            inventoryViewModel.listContacts.Add(new SelectListItem() { Text = "+83", Value = "83" });
            inventoryViewModel.listContacts.Add(new SelectListItem() { Text = "+84", Value = "84" });
            inventoryViewModel.listContacts.Add(new SelectListItem() { Text = "+85", Value = "85" });
            //--20150401 MaoNguyen ADD ↑
            //-------------------------------------------------------------------------
            return View(inventoryViewModel);
        }

        [HttpPost]
        public ActionResult Create(FormCollection fc)
        {
            //Permission with create when limit Stores
            int count = 0;
            if (Session[ConstantSmoovs.Stores.BranchCount] == null)
            {
                NewPaging paging = new NewPaging();
                paging.limit = 25;
                paging.pageGoTo = 1;
                paging.text = "";
                paging.sort = "index";
                paging.type = "title";
                paging.desc = false;

                WSInventoryClient clientIn = new WSInventoryClient();
                var listCollections = clientIn.WSGetAllInventory(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket(), paging);
                clientIn.Close();

                //Get/Set the quantity of all current branches to session
                count = listCollections.Paging.total - 1; //Don't count inventory OnlineShop
                Session[ConstantSmoovs.Stores.BranchCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Stores.BranchCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Branch, count))
            {
                return RedirectToAction("Index");
            }

            Inventory inventory = new Inventory();
            inventory._id = Guid.NewGuid().ToString();
            inventory.createdDate = DateTime.Today;
            inventory.modifiedDate = DateTime.Today;
            inventory.userID = "001";
            inventory.userOwner = "Merchant_001";
            inventory.display = true;
            inventory.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            WSInventoryClient client = new WSInventoryClient();
            try
            {
                bool isActive = true;
                if (!string.IsNullOrWhiteSpace(fc["optionsRadios"]))
                {
                    isActive = Boolean.Parse(fc["optionsRadios"]);
                }
                inventory.status = isActive;
            }
            catch (Exception)
            {
                inventory.status = false;
            }
            if (!string.IsNullOrWhiteSpace(fc["businessId"]))
            {
                var listInventory = client.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket());
                inventory.index = listInventory.Max(r => r.index) + 1;
                inventory.businessID = fc["businessId"];
            }
            else
            {
                inventory.businessID = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["description"]))
            {
                inventory.description = fc["description"];
            }
            else
            {
                inventory.description = "";
            }
            //--if (!string.IsNullOrWhiteSpace(fc["contactCode"]) && !string.IsNullOrWhiteSpace(fc["contact"]))             //--20150408 MaoNguyen DEL
            if (!string.IsNullOrWhiteSpace(fc["contact"]))                                                                                               //--20150408 MaoNguyen ADD
            {
                // inventory.contact = fc["contactCode"] + fc["contact"];                                                                            //--20150408 MaoNguyen DEL
                inventory.contact = fc["contact"];                                                                                                             //--20150408 MaoNguyen ADD
            }
            else
            {
                inventory.contact = "";
            }
            //--20150408 MaoNguyen ADD ↓
            if (!string.IsNullOrWhiteSpace(fc["contactMobile"]))
            {
                inventory.contactMobile = fc["contactMobile"];
            }
            else
            {
                inventory.contactMobile = "";
            }
            //--20150408 MaoNguyen ADD ↑

            if (!string.IsNullOrWhiteSpace(fc["streetAddress"]))
            {
                inventory.streetAddress = fc["streetAddress"];
            }
            else
            {
                inventory.streetAddress = "";
            }
            //city
            if (!string.IsNullOrWhiteSpace(fc["city"]))
            {
                inventory.city = fc["city"];
            }
            else
            {
                inventory.city = "";
            }

            if (!string.IsNullOrWhiteSpace(fc["country"]))
            {
                inventory.country = fc["country"];
            }
            else
            {
                inventory.country = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["state"]))
            {
                inventory.state = fc["state"];
            }
            else
            {
                inventory.state = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["zipcode"]))
            {
                inventory.zipCode = fc["zipcode"];
            }
            else
            {
                inventory.zipCode = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["timezone"]))
            {
                inventory.timeZone = fc["timezone"];
            }
            else
            {
                inventory.timeZone = "";
            }
            inventory.addressCombine = inventory.streetAddress + ", " + inventory.city + " " + inventory.zipCode + ", " + inventory.state + ", " + inventory.country;
            try
            {
                var idMerchant = VariableConfigController.GetIDMerchant();
                inventory.channels = new String[] { idMerchant + "_" + inventory.index };
                int statusCode = client.WSCreateInventory(inventory, VariableConfigController.GetBucket());
                client.Close();
            }
            catch (Exception) { }

            //Auto gen get all product item in  store master into new store
            WSProductItemClient wsProductItemClient = new WSProductItemClient();
            ProductItem[] lists = wsProductItemClient.WSGetAllProductItemByMasterNoLimit(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllProductItem, VariableConfigController.GetBucket());

            foreach (var pi in lists.Where(x => x.status && x.store == ConstantSmoovs.Stores.Master).ToList())
            {

                ProductItem tmp = pi;
                tmp._id = Guid.NewGuid().ToString();
                tmp.store = inventory.businessID;
                tmp.index = inventory.index;
                tmp.status = false;
                tmp.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                foreach (var item in pi.arrayVarient)
                {
                    if (item.inventory_policy == "0")
                    {
                        item.quantity = -1;
                    }
                    else
                    {
                        item.quantity = 0;
                    }

                    item.status = false;
                }

                wsProductItemClient.WSCreateProductItem(tmp, VariableConfigController.GetBucket());
            }

            wsProductItemClient.Close();

            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
            MerchantSetting setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
            setting.createBranch = true;
            int checkStatus = clientMerchantSetting.UpdateMerchantSetting(setting, VariableConfigController.GetBucket());
            clientMerchantSetting.Close();

            //Update count
            count++;
            Session[ConstantSmoovs.Stores.BranchCount] = count;

            if (setting.updateProfile && setting.createBranch && setting.createCategory && setting.addProduct && setting.distribute)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize]
        public ActionResult Detail(string id)
        {
            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "inventory";
            WSInventoryClient client = new WSInventoryClient();
            Inventory inventory = client.WSDetailInventory(id, VariableConfigController.GetBucket());
            inventory.timeZone += " " + ConstantSmoovs.Enums.ListTimeZone.Where(i => i.Key == inventory.timeZone).FirstOrDefault().Value;
            client.Close();
            return View(inventory);
        }

        public ActionResult ListProduct(int store, string sort = "product", string type = "product", string text = "", int pageGoto = 1, int limit = 25, bool desc = false)
        {
            string siteID = VariableConfigController.GetBucket();
            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "inventory";
            NewPaging paging = new NewPaging();
            paging.pageGoTo = pageGoto;
            paging.text = text;
            paging.sort = sort;
            paging.type = type;
            paging.desc = desc;
            paging.store = store;
            paging.limit = 100;

            //------------- edit store name by ZIN
            WSInventoryClient clientInventory = new WSInventoryClient();
            var inventoryList = clientInventory.WSGetAllInventory(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket(), paging);
            clientInventory.Close();
            if (inventoryList != null && inventoryList.Inventories != null && inventoryList.Inventories.Count() > 0)
            {
                var inventory = inventoryList.Inventories.FirstOrDefault(i => i.index == store);
                ViewBag.StoreName = inventory.businessID;
            }
            else
            {
                ViewBag.StoreName = "";
            }
            //------------- END edit store name by ZIN
            paging.limit = limit;
            WSProductItemClient client = new WSProductItemClient();
            var listsAll = client.WSGetAllProductItemByStore(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByStore, store, siteID, paging);
            client.Close();
            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.currency = GetGeneralSetting().Currency;
            return View(listsAll);
        }

        public ActionResult Edit(string id)
        {
            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "inventory";
            WSInventoryClient client = new WSInventoryClient();
            Inventory inventory = client.WSDetailInventory(id, VariableConfigController.GetBucket());
            inventory.listDefaultTimeZones = SmoovPOS.Common.ConstantSmoovs.Enums.ListTimeZone; ;


            Country[] countrys = client.WSGetAllCountry("country", "get_all_country", VariableConfigController.GetBucket());
            Country country = countrys[0];
            ViewBag.Country = country;
            client.Close();
            return View(inventory);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection fc, string id)
        {   
            string siteID = VariableConfigController.GetBucket();
            ViewBag.Class = "inventory";
            WSInventoryClient client = new WSInventoryClient();
            Inventory inventory = client.WSDetailInventory(id, siteID);
            inventory.createdDate = DateTime.Today;
            inventory.modifiedDate = DateTime.Today;
            var businessIDOld = inventory.businessID;
            //try
            //{
            //    inventory.status = Boolean.Parse(fc["status"]);
            //}
            //catch (Exception)
            //{
            //    inventory.status = false;
            //}
            if (!string.IsNullOrWhiteSpace(fc["businessId"]))
            {
                inventory.businessID = fc["businessId"];
            }
            else
            {
                inventory.businessID = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["description"]))
            {
                inventory.description = fc["description"];
            }
            else
            {
                inventory.description = "";
            }
            //if (!string.IsNullOrWhiteSpace(fc["contactCode"]) && !string.IsNullOrWhiteSpace(fc["contact"]))            //20150408 MaoNguyen DEL
            if (!string.IsNullOrWhiteSpace(fc["contact"]))                          //20150408 MaoNguyen ADD
            {
                //inventory.contact = fc["contactCode"] + fc["contact"];       //20150408 MaoNguyen DEL
                inventory.contact = fc["contact"];                                       //20150408 MaoNguyen ADD
            }
            else
            {
                inventory.contact = "";
            }
            //--20150408 MaoNguyen ADD ↓
            if (!string.IsNullOrWhiteSpace(fc["contactMobile"]))              //20150408 MaoNguyen ADD
            {
                //inventory.contact = fc["contactCode"] + fc["contact"];      //20150408 MaoNguyen DEL
                inventory.contactMobile = fc["contactMobile"];                  //20150408 MaoNguyen ADD
            }
            else
            {
                inventory.contactMobile = "";
            }
            //--20150408 MaoNguyen ADD ↑

            if (!string.IsNullOrWhiteSpace(fc["streetAddress"]))
            {
                inventory.streetAddress = fc["streetAddress"];
            }
            else
            {
                inventory.streetAddress = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["country"]))
            {
                inventory.country = fc["country"];
            }
            else
            {
                inventory.country = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["city"]))
            {
                inventory.city = fc["city"];
            }
            else
            {
                inventory.city = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["state"]))
            {
                inventory.state = fc["state"];
            }
            else
            {
                inventory.state = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["zipcode"]))
            {
                inventory.zipCode = fc["zipcode"];
            }
            else
            {
                inventory.zipCode = "";
            }
            if (!string.IsNullOrWhiteSpace(fc["timezone"]))
            {
                inventory.timeZone = fc["timezone"];
            }
            else
            {
                inventory.timeZone = "";
            }
            inventory.addressCombine = inventory.streetAddress + ", " + inventory.city + " " + inventory.zipCode + ", " + inventory.state + ", " + inventory.country;
            try
            {

                int statusCode = client.WSUpdateInventory(inventory, siteID);
                client.Close();
                if (!businessIDOld.Equals(inventory.businessID))
                {
                    WSProductItemClient clientProductItem = new WSProductItemClient();
                    clientProductItem.UpdateBusinessNameProductItemByStore(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByStore, inventory.index, siteID, inventory.businessID);
                    clientProductItem.Close();
                }

            }
            catch (Exception) { }
            return RedirectToAction("Detail", new { id = id });
        }

        public ActionResult Delete(string id)
        {
            WSInventoryClient client = new WSInventoryClient();
            Inventory inventory = client.WSDetailInventory(id, VariableConfigController.GetBucket());
            inventory.display = false;
            client.WSUpdateInventory(inventory, VariableConfigController.GetBucket());
            WSProductItemClient clientProductItem = new WSProductItemClient();


            var listsAll = clientProductItem.WSGetAllProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByStore, VariableConfigController.GetBucket());
            var lists = listsAll.Where(r => r.store == inventory.businessID);
            foreach (ProductItem productItem in lists)
            {
                clientProductItem.WSDeleteProductItem(productItem._id, VariableConfigController.GetBucket());
            }
            client.Close();
            clientProductItem.Close();
            return RedirectToAction("Index");
        }

        public ActionResult Back()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult updateStatusInventory(string id, Boolean status)
        {
            WSInventoryClient client = new WSInventoryClient();
            Inventory inventory = client.WSDetailInventory(id, VariableConfigController.GetBucket());
            inventory.status = status;
            client.WSUpdateInventory(inventory, VariableConfigController.GetBucket());
            client.Close();
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult updateStatus(string[] arrId, Boolean status)
        {
            WSInventoryClient client = new WSInventoryClient();
            foreach (String id in arrId)
            {
                Inventory inventory = client.WSDetailInventory(id, VariableConfigController.GetBucket());
                inventory.status = status;
                client.WSUpdateInventory(inventory, VariableConfigController.GetBucket());
            }
            client.Close();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public int updateProductItemStatus(ProductItemId obj)
        {
            WSProductItemClient client = new WSProductItemClient();
            foreach (ProductVariantId item in obj.productVariantId)
            {
                var productItem = client.WSDetailProductItem(item.productID, VariableConfigController.GetBucket());
                if (item.status)
                {
                    productItem.status = obj.status;
                }
                for (int i = 0; i < item.arrayVariant.Count(); i++)
                {
                    productItem.arrayVarient[item.arrayVariant[i]].status = obj.status;
                }
                client.WSUpdateProductItem(productItem, VariableConfigController.GetBucket());
            }
            client.Close();
            return 1;
            // return RedirectToAction("ListProduct", new { store = obj.store, startKey = obj.startKey, nextKey = obj.nextKey, limit = obj.limit, pageGoTo = obj.pageGoTo, pageCurrent = obj.pageCurrent, sort = obj.sort, desc = obj.desc });
        }

        [HttpPost]
        public Boolean updateProductItemSwitch(string productId, int index, int type)
        {
            WSProductItemClient client = new WSProductItemClient();
            var productItem = client.WSDetailProductItem(productId, VariableConfigController.GetBucket());
            if (type == 0)
            {// update status Product Item
                productItem.status = !productItem.status;
                for (int i = 0; i < productItem.arrayVarient.Count(); i++)
                {
                    productItem.arrayVarient[i].status = productItem.status;
                }
            }
            else
            {// update status variant of Product Item
                productItem.arrayVarient[index].status = !productItem.arrayVarient[index].status;
                productItem.status = productItem.arrayVarient.Any(r => r.status);
            }
            int reponseCode = client.WSUpdateProductItem(productItem, VariableConfigController.GetBucket());
            client.Close();
            if (reponseCode != 0)
            {
                return false;
            }
            return true;
        }


        [HttpPost]
        public Boolean updateStatusProductItem(string productId, Boolean status)
        {
            WSProductItemClient client = new WSProductItemClient();
            var productItem = client.WSDetailProductItem(productId, VariableConfigController.GetBucket());
            productItem.status = status;
            int reponseCode = client.WSUpdateProductItem(productItem, VariableConfigController.GetBucket());
            client.Close();
            if (reponseCode != 0)
            {
                return false;
            }
            return true;
        }

        [HttpPost]
        public int updateStatusVariant(string productId, int indexVariant, Boolean status, Boolean all = false)
        {
            WSProductItemClient client = new WSProductItemClient();
            var productItem = client.WSDetailProductItem(productId, VariableConfigController.GetBucket());
            if (all)
            {
                foreach (var item in productItem.arrayVarient)
                {
                    item.status = status;
                }
                int reponseCode = client.WSUpdateProductItem(productItem, VariableConfigController.GetBucket());
                client.Close();
                return 1;
            }
            else
            {
                var code = 1;
                productItem.arrayVarient[indexVariant].status = status;
                if (checkStatusProductItem(productItem, status))
                {
                    productItem.status = status;
                    code = 2;
                }
                if (status) productItem.status = true;
                client.WSUpdateProductItem(productItem, VariableConfigController.GetBucket());
                client.Close();
                return code;
            }
        }

        public Boolean checkStatusProductItem(ProductItem productItem, Boolean status)
        {
            int count = 0;
            foreach (var item in productItem.arrayVarient)
            {
                if (item.status == status)
                {
                    count++;
                }
            }
            if (count == productItem.arrayVarient.Count())
            {
                if (productItem.status != status)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
    }
}