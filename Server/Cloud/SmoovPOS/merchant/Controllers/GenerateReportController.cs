﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models.GenerateReport;
using SmoovPOS.UI.Models.GenerateReport.ReportSample.SalesBranch;
using SmoovPOS.Utility.WSInventoryReference;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using SmoovPOS.Utility.WSOrderReference;
using SmoovPOS.Utility.WSCategoryReference;
using SmoovPOS.Utility.WSProductItemReference;

namespace SmoovPOS.UI.Controllers
{
    public class GenerateReportController : BaseController
    {
        string SiteID = VariableConfigController.GetBucket();

        // GET: GenerateReport
        public ActionResult Index()
        {
            //Define ViewBag variables
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "generateReport";

            GenerateReportViewModel modelGenerateReportView = new GenerateReportViewModel();
            modelGenerateReportView.ListBranches = new List<Inventory>();

            //Get all active branches and assign into model.ListBranches
            WSInventoryClient clientInventory = new WSInventoryClient();
            List<Inventory> ListBranches = clientInventory.WSGetAllActiveBranchesForTableOrder(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, SiteID).OrderBy(i => i.businessID).ToList();
            clientInventory.Close();

            if (ListBranches != null) modelGenerateReportView.ListBranches = ListBranches;

            return View(modelGenerateReportView);
        }

        //SET: Calc/Assign content to report and export to file
        public FileResult GenerateReport(string selectedReportForm, string selectedReportType, string selectedReportBranch, string strReportPeriod)
        {
            byte[] report = null;
            string filename = "testReport.xls";
            string BranchName = selectedReportBranch.Split('|')[0];
            string BranchIndex = selectedReportBranch.Split('|')[1];

            //Choose format of report depends on ReportForm
            switch (selectedReportForm)
            {
                case "1"://Sales Channel Report
                    break;

                case "2"://Sales Inventory Report
                    break;

                case "3"://Sales Branch Report
                    SBRGeneralModel model = BuildReportModel(BranchIndex, BranchName, selectedReportType, strReportPeriod);
                    report = GenerateSalesBranchReport(model);
                    filename = "Sales_Branch_Report_for_" + model.BranchName.Trim().Replace(" ", "_") + "_in" + model.Range.Substring(model.Range.IndexOf(" ")).Replace(" ", "_").Replace(",", "") + ".xls";
                    break;

                case "4"://Sales Online Report
                    break;

                case "5"://Individual Store Sales Performance
                    break;
            }

            //Export report to download
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", filename));
            Response.ContentType = "application/Excel";

            return File(report, "application/Excel");
        }

        #region Sales Branch Report
        private SBRGeneralModel BuildReportModel(string BranchIndex, string BranchName, string strReportType, string strReportPeriod)
        {
            //Set report's begin/end date-hour-min and report's currency
            SBRGeneralModel returnModel = SetReportPeriodAndCurrency(strReportType, strReportPeriod);

            //Set branch name
            returnModel.BranchName = BranchName;

            //Get list of appropriate orders to report
            //  get all orders from "BranchName" of current merchant
            SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel listPeriodOrders = GetListOrdersToReport(BranchIndex, returnModel.BeginDate, returnModel.EndDate, returnModel.IsNextDay);
            //  and SORT them descending by paidDate/refundDate to assign LATEST SalesCollected/CostOfGoodsSold/GrossProfit/GrossMargin to report
            returnModel.ListPaidOrders = listPeriodOrders.listPaidOrders.OrderByDescending(i => i.paidDate).ToList();
            returnModel.ListRefundedOrders = listPeriodOrders.listRefundedOrders.OrderByDescending(i => i.refundInfo.refundDate).ToList();

            //Calculate values of item(s) of list of product variants
            returnModel.ListProductVariants = CalculateListProductVariants(returnModel);

            //Calculate values of TotalSales/BestSellingItem
            if (returnModel.ListProductVariants != null && returnModel.ListProductVariants.Count > 0)
            {
                returnModel.TotalSales = returnModel.Currency + " " + returnModel.ListProductVariants.Sum(i => i.SalesCollected);
                returnModel.BestSellingItem = returnModel.ListProductVariants[0].Product;
            }
            else
            {
                returnModel.TotalSales = "";
                returnModel.BestSellingItem = "";
            }

            return returnModel;
        }

        //Set report's begin/end date-hour-min and report's currency
        private SBRGeneralModel SetReportPeriodAndCurrency(string strReportType, string strReportPeriod)
        {
            SBRGeneralModel returnModel = new SBRGeneralModel();

            //Get saved general settings from couchbase
            MerchantGeneralSetting generalSetting = GetGeneralSetting();
            returnModel.Currency = generalSetting.Currency;
            returnModel.IsNextDay = generalSetting.isDailyReportPeriodEndAtNextDay;

            //Set start and end date
            switch (strReportType)
            {
                case "1"://ReportType = Daily
                    DateTime dtDate = Convert.ToDateTime(strReportPeriod);
                    returnModel.BeginDate = returnModel.EndDate = dtDate;
                    returnModel.Range = dtDate.ToLongDateString();
                    break;

                case "2"://ReportType = Weekly
                    DateTime dtBeginDate = Convert.ToDateTime(strReportPeriod.Split('-')[0]);
                    returnModel.BeginDate = dtBeginDate;
                    DateTime dtEndDate = Convert.ToDateTime(strReportPeriod.Split('-')[1]);
                    returnModel.EndDate = dtEndDate;
                    returnModel.Range = returnModel.BeginDate.ToLongDateString() + " - " + returnModel.EndDate.ToLongDateString();
                    break;

                case "3"://ReportType = Monthly
                    DateTime dtBeginMonthYear = Convert.ToDateTime(1 + strReportPeriod);
                    returnModel.BeginDate = dtBeginMonthYear;
                    DateTime dtEndMonthYear = dtBeginMonthYear.AddMonths(1).AddDays(-1);
                    returnModel.EndDate = dtEndMonthYear;
                    returnModel.Range = returnModel.BeginDate.ToLongDateString() + " - " + returnModel.EndDate.ToLongDateString();
                    break;

                default:
                    break;
            }

            //Set start Hour/min
            if (string.IsNullOrEmpty(generalSetting.DailyReportPeriodBeginFrom)) generalSetting.DailyReportPeriodBeginFrom = "00:00";
            returnModel.BeginDate = returnModel.BeginDate.AddHours(int.Parse(generalSetting.DailyReportPeriodBeginFrom.Split(':')[0])).AddMinutes(int.Parse(generalSetting.DailyReportPeriodBeginFrom.Split(':')[1]));

            //Set end Hour/Min
            if (string.IsNullOrEmpty(generalSetting.DailyReportPeriodEndAt)) generalSetting.DailyReportPeriodEndAt = "23:59";
            returnModel.EndDate = returnModel.EndDate.AddHours(int.Parse(generalSetting.DailyReportPeriodEndAt.Split(':')[0])).AddMinutes(int.Parse(generalSetting.DailyReportPeriodEndAt.Split(':')[1]));

            //Convert to UTC
            returnModel.BeginDate = TimeZoneInfo.ConvertTimeToUtc(returnModel.BeginDate, TimeZoneInfo.FindSystemTimeZoneById(generalSetting.TimeZoneFullName));
            returnModel.EndDate = TimeZoneInfo.ConvertTimeToUtc(returnModel.EndDate, TimeZoneInfo.FindSystemTimeZoneById(generalSetting.TimeZoneFullName));

            return returnModel;
        }

        //Get data and calculate the list variants to report
        #region Get and set values of list variants to report
        //Calculate the values of item(s) of ListProductVariants
        private List<SBRProductDetailModel> CalculateListProductVariants(SBRGeneralModel returnModel)
        {
            //Initialize report.ListProducts
            List<SBRProductDetailModel> listVariant = new List<SBRProductDetailModel>();

            //Calc and set model.ListProducts and model.TotalSales / model.BestSellingItem
            foreach (var order in returnModel.ListPaidOrders)
            {
                //Calculate on each order's product
                foreach (var product in order.arrayProduct)
                {
                    //Group and Calculate each product variant
                    foreach (var variant in product.arrayVariantOrder)
                    {
                        int index = listVariant.FindIndex(i => i.VariantSKU == variant.sku);
                        if (index != -1)
                        {
                            //-----------------------------------------------------
                            //Case: variant contains in listVariant --> update item
                            //-----------------------------------------------------

                            SBRProductDetailModel tempVariant = UpdateExistedVariantInList(listVariant[index], order, product, variant);
                            listVariant[index] = tempVariant;
                        }
                        else
                        {
                            //-------------------------------------------------------------
                            //Case: variant doesn't contain in listVariant --> add new item
                            //-------------------------------------------------------------

                            SBRProductDetailModel tempVariant = AddNewVariantIntoList(order, product, variant);
                            listVariant.Add(tempVariant);
                        }
                    }
                }
            }

            foreach (var order in returnModel.ListRefundedOrders)
            {
                //Calculate on each order's product
                foreach (var product in order.arrayProduct)
                {
                    //Group and Calculate each product variant
                    foreach (var variant in product.arrayVariantOrder)
                    {
                        int index = listVariant.FindIndex(i => i.VariantSKU == variant.sku);
                        if (index != -1)
                        {
                            //-----------------------------------------------------
                            //Case: variant contains in listVariant --> update item
                            //-----------------------------------------------------

                            SBRProductDetailModel tempVariant = UpdateExistedRefundedVariantInList(listVariant[index], order, product, variant);
                            listVariant[index] = tempVariant;
                        }
                        else
                        {
                            //-------------------------------------------------------------
                            //Case: variant doesn't contain in listVariant --> add new item
                            //-------------------------------------------------------------

                            SBRProductDetailModel tempVariant = AddNewRefundedVariantIntoList(order, product, variant);
                            listVariant.Add(tempVariant);
                        }
                    }
                }
            }
            return listVariant.OrderByDescending(i => i.QuantitySold).ToList();
        }

        //Update existed item in listVariant
        private static SBRProductDetailModel UpdateExistedVariantInList(SBRProductDetailModel ExistedVariant, Order order, ProductOrder product, VarientItem variant)
        {
            SBRProductDetailModel returnVariant = ExistedVariant;

            double DiscountCost = product.discount ? product.discountProductItem.discountValue : 0;

            //Field: Quantity Sold
            returnVariant.QuantitySold += variant.quantity;

            //Field: Quantity Sold Today
            returnVariant.QuantitySoldToday += (order.paidDate.Date == DateTime.Today.Date) ? variant.quantity : 0;

            //Field: Sales Collected
            returnVariant.SalesCollected += variant.price > DiscountCost ? variant.quantity * (variant.price - DiscountCost) : 0;

            //Field: Cost of Goods sold
            returnVariant.CostOfGoodsSold += variant.quantity * variant.priceCost;

            //Field: Gross Profit
            returnVariant.GrossProfit = returnVariant.SalesCollected - returnVariant.CostOfGoodsSold;

            //Field: Gross Margin
            returnVariant.GrossMargin = (returnVariant.SalesCollected == 0) ? "N/A" : String.Format("{0:P2}", returnVariant.GrossProfit / returnVariant.SalesCollected);

            //Field: Payment Type
            foreach (var item in order.arrayPayment)
            {
                if (!returnVariant.PaymentType.Contains(item.paymentType))
                {
                    returnVariant.PaymentType += ", " + item.paymentType;
                }
            }

            return returnVariant;
        }

        //Add new item into listVariant
        private SBRProductDetailModel AddNewVariantIntoList(Order order, ProductOrder product, VarientItem variant)
        {
            SBRProductDetailModel returnVariant = new SBRProductDetailModel();

            double DiscountCost = product.discount ? product.discountProductItem.discountValue : 0;

            //Field: VariantSKU
            returnVariant.VariantSKU = variant.sku;

            //Field: Category
            returnVariant.Category = GetCategoryTitle(product.categoryId);

            //Field: Title (Inventory / Product Type)
            returnVariant.Product = product.title;
            if (!string.IsNullOrEmpty(variant.title)) returnVariant.Product += " " + ((variant.title.IndexOf(" Default") >= 0) ? variant.title.Replace(" Default", "") : variant.title);
            if (!string.IsNullOrEmpty(variant.color)) returnVariant.Product += " " + variant.color;
            if (!string.IsNullOrEmpty(variant.size)) returnVariant.Product += " " + variant.size;

            //Field: Quantity Sold
            returnVariant.QuantitySold = variant.quantity;
            //Field: Quantity Sold Today
            returnVariant.QuantitySoldToday = (order.paidDate.Date == DateTime.Today.Date) ? variant.quantity : 0;

            //Field: Quantity Left
            returnVariant.QuantityLeft = GetQuantityLeft(product._id, variant.sku);

            //Field: Sales Collected
            returnVariant.SalesCollected = variant.price > DiscountCost ? returnVariant.QuantitySold * (variant.price - DiscountCost) : 0;

            //Field: Cost of Goods sold
            returnVariant.CostOfGoodsSold = returnVariant.QuantitySold * variant.priceCost;

            //Field: Gross Profit
            returnVariant.GrossProfit = returnVariant.SalesCollected - returnVariant.CostOfGoodsSold;

            //Field: Gross Margin
            returnVariant.GrossMargin = (returnVariant.SalesCollected == 0) ? "N/A" : String.Format("{0:P2}", returnVariant.GrossProfit / returnVariant.SalesCollected);

            //Field: Payment Type
            foreach (var item in order.arrayPayment)
            {
                if (string.IsNullOrEmpty(returnVariant.PaymentType)) returnVariant.PaymentType = item.paymentType;
                else returnVariant.PaymentType += ", " + item.paymentType;
            }

            return returnVariant;
        }

        //Update existed item in listVariant
        private static SBRProductDetailModel UpdateExistedRefundedVariantInList(SBRProductDetailModel ExistedVariant, Order order, ProductOrder product, VarientItem variant)
        {
            SBRProductDetailModel returnVariant = ExistedVariant;

            double DiscountCost = product.discount ? product.discountProductItem.discountValue : 0;

            //Field: Quantity Sold
            returnVariant.QuantitySold -= variant.quantity;

            //Field: Quantity Sold Today
            returnVariant.QuantitySoldToday -= (order.paidDate.Date == DateTime.Today.Date) ? variant.quantity : 0;

            //Field: Sales Collected
            returnVariant.SalesCollected -= variant.price > DiscountCost ? variant.quantity * (variant.price - DiscountCost) : 0;

            //Field: Cost of Goods sold
            returnVariant.CostOfGoodsSold -= variant.quantity * variant.priceCost;

            //Field: Gross Profit
            returnVariant.GrossProfit = returnVariant.SalesCollected - returnVariant.CostOfGoodsSold;

            //Field: Gross Margin
            returnVariant.GrossMargin = (returnVariant.SalesCollected == 0) ? "N/A" : String.Format("{0:P2}", returnVariant.GrossProfit / returnVariant.SalesCollected);

            //Field: Payment Type
            foreach (var item in order.arrayPayment)
            {
                if (!returnVariant.PaymentType.Contains(item.paymentType))
                {
                    returnVariant.PaymentType += ", " + item.paymentType;
                }
            }

            return returnVariant;
        }

        //Add new item into listVariant
        private SBRProductDetailModel AddNewRefundedVariantIntoList(Order order, ProductOrder product, VarientItem variant)
        {
            SBRProductDetailModel returnVariant = new SBRProductDetailModel();

            double DiscountCost = product.discount ? product.discountProductItem.discountValue : 0;

            //Field: VariantSKU
            returnVariant.VariantSKU = variant.sku;

            //Field: Category
            returnVariant.Category = GetCategoryTitle(product.categoryId);

            //Field: Title (Inventory / Product Type)
            returnVariant.Product = product.title;
            if (!string.IsNullOrEmpty(variant.title)) returnVariant.Product += " " + ((variant.title.IndexOf(" Default") >= 0) ? variant.title.Replace(" Default", "") : variant.title);
            if (!string.IsNullOrEmpty(variant.color)) returnVariant.Product += " " + variant.color;
            if (!string.IsNullOrEmpty(variant.size)) returnVariant.Product += " " + variant.size;

            //Field: Quantity Sold
            returnVariant.QuantitySold = -variant.quantity;

            //Field: Quantity Sold Today
            returnVariant.QuantitySoldToday = (order.paidDate.Date == DateTime.Today.Date) ? -variant.quantity : 0;

            //Field: Quantity Left
            returnVariant.QuantityLeft = GetQuantityLeft(product._id, variant.sku);

            //Field: Sales Collected
            returnVariant.SalesCollected = variant.price > DiscountCost ? returnVariant.QuantitySold * (variant.price - DiscountCost) : 0;

            //Field: Cost of Goods sold
            returnVariant.CostOfGoodsSold = returnVariant.QuantitySold * variant.priceCost;

            //Field: Gross Profit
            returnVariant.GrossProfit = returnVariant.SalesCollected - returnVariant.CostOfGoodsSold;

            //Field: Gross Margin
            returnVariant.GrossMargin = (returnVariant.SalesCollected == 0) ? "N/A" : String.Format("{0:P2}", returnVariant.GrossProfit / returnVariant.SalesCollected);

            //Field: Payment Type
            foreach (var item in order.arrayPayment)
            {
                if (string.IsNullOrEmpty(returnVariant.PaymentType)) returnVariant.PaymentType = item.paymentType;
                else returnVariant.PaymentType += ", " + item.paymentType;
            }

            return returnVariant;
        }


        //Connect to CB and get the name of category that variant belongs to
        private string GetCategoryTitle(string categoryId)
        {
            WSCategoryClient client = new WSCategoryClient();
            Category category = client.WSDetailCategory(categoryId, SiteID);
            client.Close();
            return category.title;
        }

        //Connect to CB and get the remaining quantity of variant
        private string GetQuantityLeft(string productID, string varianSKU)
        {
            WSProductItemClient client = new WSProductItemClient();
            var product = client.WSDetailProductItem(productID, SiteID);
            client.Close();

            var variant = product.arrayVarient.FirstOrDefault(i => i.sku == varianSKU);
            string result = (variant.quantity >= 0) ? variant.quantity.ToString() : "No Limited";
            return result;
        }
        #endregion

        //Get list of appropriate orders to report
        private SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel GetListOrdersToReport(string BranchIndex, DateTime BeginDate, DateTime EndDate, bool IsNextDay)
        {
            //Get all orders from BranchName that payment status = paid or refunded and order.paidDate or order.refundDate in period from beginDate to endDate
            //also apply general setting's daily report period for each day from beginDate to endDate 
            WSOrderClient clientOrder = new WSOrderClient();
            SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel listReportOrders = clientOrder.WSGetListPaidAndRefundedOrdersInPeriod(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAll, SiteID, BeginDate, EndDate, IsNextDay, BranchIndex);
            clientOrder.Close();

            return listReportOrders;
        }

        //Assign report's parameters and Create report's byte array
        #region Generate Sales Branch Report
        private byte[] GenerateSalesBranchReport(SBRGeneralModel model)
        {
            //Variables  
            Warning[] warnings;
            string[] streamIds;
            string OutputFormat = "Excel";
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            //Declare the Excel's config info
            string deviceInfo = "<DeviceInfo>" +
                                "  <OutputFormat>" + OutputFormat + "</OutputFormat>" +
                                "  <PageWidth>" + "11in" + "</PageWidth>" +
                                "  <PageHeight>" + "8.5in" + "</PageHeight>" +
                                "  <MarginTop>0.0in</MarginTop>" +
                                "  <MarginLeft>0.0in</MarginLeft>" +
                                "  <MarginRight>0.0in</MarginRight>" +
                                "  <MarginBottom>0.0in</MarginBottom>" +
                                "</DeviceInfo>";

            //Generate reportDataSource
            ReportDataSource reportDataSource = new ReportDataSource("SBRDataSet", model.ListProductVariants);//SBRDataSet is the name of the dataset inside the report

            //Generate reportParameters
            List<ReportParameter> reportParameters = GenerateSBRParameters(model);

            //Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.DataSources.Add(reportDataSource);//Add datasource here  
            viewer.LocalReport.EnableExternalImages = true;
            viewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Models/GenerateReport/ReportSample/SalesBranch/SalesBranchReport.rdlc");//Map path to default report file
            viewer.LocalReport.SetParameters(reportParameters);
            byte[] reportBytes = viewer.LocalReport.Render(OutputFormat, deviceInfo, out mimeType, out encoding, out extension, out streamIds, out warnings);

            return reportBytes;
        }

        private List<ReportParameter> GenerateSBRParameters(SBRGeneralModel model)
        {
            if (string.IsNullOrEmpty(model.Range)) model.Range = string.Empty;
            if (string.IsNullOrEmpty(model.BranchName)) model.Range = string.Empty;
            if (string.IsNullOrEmpty(model.TotalSales)) model.TotalSales = "0";
            if (string.IsNullOrEmpty(model.BestSellingItem)) model.BestSellingItem = string.Empty;

            List<ReportParameter> reportParameters = new List<ReportParameter>();

            reportParameters.Add(new ReportParameter("Date", DateTime.Today.ToLongDateString()));
            reportParameters.Add(new ReportParameter("Range", model.Range));
            reportParameters.Add(new ReportParameter("Branch", model.BranchName));
            reportParameters.Add(new ReportParameter("TotalSales", model.TotalSales));
            reportParameters.Add(new ReportParameter("BestSellingItem", model.BestSellingItem));

            return reportParameters;
        }
        #endregion
        #endregion
    }
}