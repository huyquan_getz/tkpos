﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using Microsoft.AspNet.Identity;
using SmoovPOS.Entity;
using SmoovPOS.Utility.WSCategoryReference;
using SmoovPOS.Utility.WSCollectionReference;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using SmoovPOS.Utility.WSProductItemReference;
using SmoovPOS.Utility.WSProductReference;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models;

namespace SmoovPOS.UI.Controllers
{

    public class ProductController : BaseController
    {
        public int statusProduct = -1;

        public ActionResult Index(string sort = "product", string type = "product", string text = "", int pageGoto = 1, int limit = 25, bool desc = false)
        {
            ViewBag.Class = "product";

            NewPaging paging = new NewPaging();
            paging.limit = limit;
            paging.pageGoTo = pageGoto;
            paging.text = text;
            paging.sort = sort;
            paging.type = type;
            paging.desc = desc;

            WSProductItemClient client = new WSProductItemClient();
            var ListProductAllStore = client.WSGetAllProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllProductItem, VariableConfigController.GetBucket());
            var model = GetListProductItemsMaster(paging, ListProductAllStore);
            client.Close();

            var productItemGroupByProduct = ListProductAllStore.Where(r => r.store != ConstantSmoovs.Stores.Master).GroupBy(r => r.productID).ToList();
            GetDistributedInStores(model.ListProductItem.ToArray(), productItemGroupByProduct);
            ViewBag.NumberInit = CheckInitiation();

            //Get/Set the quantity of all products to session
            int count = model.Paging.total;
            Session[ConstantSmoovs.Product.ProductCount] = count;
            //Permission module
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            model.isAddNew = userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Product, count);

            return View(model);
        }

        /// <summary>
        /// Gets the distributed in stores.
        /// </summary>
        /// <param name="ListProduct">The list product.</param>
        /// <param name="productItemGroupByProduct">The product item group by product.</param>
        private static void GetDistributedInStores(ProductItem[] ListProduct, List<IGrouping<string, ProductItem>> productItemGroupByProduct)
        {
            foreach (var productItem in ListProduct.Where(r => r.display))
            {
                var group = productItemGroupByProduct.Where(r => r.Key == productItem.productID).FirstOrDefault();
                if (group != null)
                {

                    productItem.inStores = group.All(r => r.arrayVarient.All(v => v.quantity == -1))
                        ? string.Format("N/A in {0} stores", group.GroupBy(r => r.store).Count())
                        : string.Format("{0} in {1} stores", group.Sum(r => r.arrayVarient.Sum(v => v.quantity >= 0 ? v.quantity : 0)), group.GroupBy(r => r.store).Count());
                }
            }
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/30/2015-7:07 PM</datetime>
        [Authorize]
        [HttpGet]
        public ActionResult Create()
        {
            //Permission with create when limit Product
            int count = 0;
            if (Session[ConstantSmoovs.Product.ProductCount] == null)
            {
                NewPaging paging = new NewPaging();
                paging.limit = 25;
                paging.pageGoTo = 1;
                paging.text = "";
                paging.sort = "index";
                paging.type = "title";
                paging.desc = false;

                WSProductItemClient client = new WSProductItemClient();
                var ListProductAllStore = client.WSGetAllProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllProductItem, VariableConfigController.GetBucket());
                var model = GetListProductItemsMaster(paging, ListProductAllStore);
                client.Close();

                //Get/Set the quantity of all current branches to session
                count = model.Paging.total;
                Session[ConstantSmoovs.Product.ProductCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Product.ProductCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Product, count))
            {
                return RedirectToAction("Index");
            }

            //ViewBag.NumberInit = CheckInitiation();
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            ViewBag.Class = "product";
            WSCategoryClient client1 = new WSCategoryClient();
            var ListCategory = client1.WSGetAllCategoryNoLimit(ConstantSmoovs.CouchBase.DesignDocCategory, ConstantSmoovs.CouchBase.GetAllCategory, bucket);
            client1.Close();
            ViewBag.countListCategory = ListCategory.Count();
            if (!string.IsNullOrWhiteSpace(VariableConfigController.limitImage))
            {
                ViewBag.LimitImage = VariableConfigController.limitImage;
            }
            else
            {
                ViewBag.LimitImage = 15;
            }
            return View(ListCategory);
        }

        /*
        *  - Get list category to show list Category in Product Detail 
        *  - Get information detail of Product  
        *   Nguyen Anh Dao (Zin)
        *  - modified :24/11/2014
        */
        /// <summary>
        /// Creates the specified fc.
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(FormCollection fc)
        {
            //Permission with create when limit Product
            int count = 0;
            if (Session[ConstantSmoovs.Product.ProductCount] == null)
            {
                NewPaging paging = new NewPaging();
                paging.limit = 25;
                paging.pageGoTo = 1;
                paging.text = "";
                paging.sort = "index";
                paging.type = "title";
                paging.desc = false;

                WSProductItemClient client = new WSProductItemClient();
                var ListProductAllStore = client.WSGetAllProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllProductItem, VariableConfigController.GetBucket());
                var model = GetListProductItemsMaster(paging, ListProductAllStore);
                client.Close();

                //Get/Set the quantity of all current branches to session
                count = model.Paging.total;
                Session[ConstantSmoovs.Product.ProductCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Product.ProductCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Product, count))
            {
                return RedirectToAction("Index");
            }

            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            WSInventoryClient clientIventory = new WSInventoryClient();
            Country[] countrys = clientIventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, bucket);
            clientIventory.Close();

            //Get and set merchant's currency
            string currency = ViewBag.currency = GetGeneralSetting().Currency;

            Product product = new Product();
            product._id = Guid.NewGuid().ToString();
            //------------- tao doi tuong IMAGE ITEM
            ImageItem image = new ImageItem();
            List<string> listImage = new List<string>();
            image.imageDetault = "0";
            image.arrayImage = new List<string>();
            image.arrayImage = listImage;
            //-------------CREATE  PRODUCT.IMAGE OBJECT ------------
            product.image = new ImageItem();
            product.image = image;
            string patternURL = ",";
            Regex myRegex = new Regex(patternURL);
            if (!string.IsNullOrWhiteSpace(fc["listImageSave"]))
            {
                string listStringImage = fc["listImageSave"];
                string[] resultStatic = myRegex.Split(listStringImage);
                product.image.arrayImage = new List<string>();
                product.image.arrayImage.AddRange(resultStatic.ToList());

            }
            else
            {
                product.image.arrayImage = new List<string>();
            }

            product.image.imageDetault = !string.IsNullOrWhiteSpace(fc["inputCheck"]) ? fc["inputCheck"] : "0";

            //==================

            product.userID = User.Identity.GetUserId();
            product.userOwner = product.userID;

            product.createdDate = DateTime.UtcNow;
            product.modifiedDate = DateTime.UtcNow;
            product.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            product.description = fc["description"];
            product.name = fc["name"];
            product.table = "Product";
            product.nameShort = fc["nameShort"];
            //product.status = fc["cbActiveProduct"].Equals("true") ? true : false;
            //product.status = fc["statusProduct"].Equals("true") ? true : false;
            bool isActive = true;
            if (!string.IsNullOrWhiteSpace(fc["optionsRadios"]))
            {
                isActive = Boolean.Parse(fc["optionsRadios"]);
            }
            product.status = isActive;

            string Category = fc["drlCategory"];
            int indexSymbol = -1;
            if (!string.IsNullOrWhiteSpace(fc["drlCategory"]))
            {
                indexSymbol = Category.IndexOf('~');
                if (indexSymbol > 0)
                {
                    // int lenghtOfName = Category.Length - 37;
                    product.categoryId = Category.Substring(0, indexSymbol);
                    product.categoryName = Category.Substring(indexSymbol + 1, Category.Length - indexSymbol - 1); // tru di vi tri dau ~
                }
                else
                {
                    product.categoryId = product.categoryName = Category;
                }
            }
            else
            {
                product.categoryId = "";
                product.categoryName = "";

            }

            product.productType = "";
            product.vendor = "";

            product.price = !string.IsNullOrWhiteSpace(fc["price"]) ? double.Parse(fc["price"]) : 0.00;
            product.priceCompare = !string.IsNullOrWhiteSpace(fc["priceCompare"]) ? double.Parse(fc["priceCompare"]) : 0.00;
            product.priceCost = !string.IsNullOrWhiteSpace(fc["priceCost"]) ? double.Parse(fc["priceCost"]) : 0.00;
            product.sku = fc["sku"];
            product.requireShipping = string.IsNullOrEmpty(fc["require-ship"]) ? false : fc["require-ship"].Equals("on") ? true : false;
            product.productMultiple = string.IsNullOrEmpty(fc["multi-inventory"]) ? false : fc["multi-inventory"].Equals("on") ? true : false;
            product.weight = !string.IsNullOrWhiteSpace(fc["weight"]) ? double.Parse(fc["weight"]) : 0.00;
            product.inventoryPolicy = fc["inventoryPolicy"];
            product.vendor = fc["vendor"];
            product.productType = fc["productType"];
            // Require of SMOOV
            product.title = fc["name"];
            product.excerpt = "";
            product.editorNote = "";
            product.sizeNFit = "";
            product.measurement = "";
            product.unit = "";
            product.limit = !string.IsNullOrWhiteSpace(fc["limit"]) ? int.Parse(fc["limit"]) : 0;
            product.remindStock = !string.IsNullOrWhiteSpace(fc["remind-stock"]) ? int.Parse(fc["remind-stock"]) : 0;
            product.trackStock = "";
            product.masterInventory = fc["inventoryPolicy"].Equals("100") ? -1 : int.Parse(fc["master-inventory"]);
            product.display = true;
            product.bucket = bucket;


            // init list varient from option
            product.arrayVarient = new List<VarientItem>();

            string policy = fc["inventoryPolicy"];
            string strValueInput = fc["tag"];
            string strValueOption = fc["optionName"];
            string[] arrValueInput = strValueInput.Split(',');
            string[] arrValueOption = strValueOption.Split(',');

            if (arrValueOption.Count() > 0 && product.productMultiple)
            {
                product.arrayInventory = new List<InventoryItem>();
                for (int i = 0; i < arrValueOption.Count(); i++)
                {
                    InventoryItem inventory = new InventoryItem();
                    switch (int.Parse(arrValueOption[i]))
                    {
                        case 1:
                            inventory.name = "title";
                            break;
                        case 2:
                            inventory.name = "color";
                            break;
                        case 3:
                            inventory.name = "size";
                            break;
                        default:
                            break;
                    }
                    List<string> listStr = new List<string>();
                    int index = arrValueInput.Count() / arrValueOption.Count();
                    for (int j = i * index; j < index * (i + 1); j++)
                    {
                        if (!arrValueInput[j].Equals(""))
                        {
                            listStr.Add(arrValueInput[j]);
                        }
                    }

                    inventory.options = listStr;

                    product.arrayInventory.Add(inventory);
                }
            }
            else
            {
                product.arrayInventory = new List<InventoryItem>();
            }

            if (arrValueOption.Count() > 0 && product.productMultiple)
            {// Choose option is inputted
                ArrayList listResult = new ArrayList();
                ArrayList listInput = new ArrayList();
                if (arrValueOption.Count() > 1)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        List<string> _listInput = new List<string>();
                        _listInput.Add("");
                        listInput.Insert(i, _listInput);
                    }
                    for (int i = 0; i < arrValueOption.Count(); i++)
                    {
                        int index = arrValueInput.Count() / arrValueOption.Count();
                        List<string> _listInput = new List<string>();
                        for (int j = i * index; j < index * (i + 1); j++)
                        {
                            if (!arrValueInput[j].Equals(""))
                            {
                                _listInput.Add(arrValueInput[j]);
                            }
                        }
                        listInput[int.Parse(arrValueOption[i]) - 1] = _listInput;
                    }
                    VarientGenerateController varantGenerate = new VarientGenerateController();
                    listResult = varantGenerate.generate(listInput.Count, listInput);
                }
                else
                {
                    for (int i = 0; i < arrValueInput.Count(); i++)
                    {

                        listResult.Add(arrValueInput[i]);
                    }
                }
                for (int i = 0; i < listResult.Count; i++)
                {

                    VarientItem variant = new VarientItem();
                    string strOutputItem = listResult[i].ToString();
                    variant.sku = string.Concat(product.sku, @"-", i.ToString());
                    if (arrValueOption.Count() > 1)
                    {
                        string[] arrOutputItem = (string[])listResult[i];

                        for (int j = 0; j < arrOutputItem.Count(); j++)
                        {

                            switch (j)
                            {
                                case 0:
                                    variant.title = !string.IsNullOrWhiteSpace(arrOutputItem[j]) ? arrOutputItem[j] : "";
                                    break;
                                case 1:
                                    variant.color = !string.IsNullOrWhiteSpace(arrOutputItem[j]) ? arrOutputItem[j] : "";
                                    break;
                                case 2:
                                    variant.size = !string.IsNullOrWhiteSpace(arrOutputItem[j]) ? arrOutputItem[j] : "";
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    else
                    {
                        variant.title = "";
                        variant.color = "";
                        variant.size = "";
                        switch (int.Parse(arrValueOption[0]))
                        {
                            case 1:
                                variant.title = !string.IsNullOrWhiteSpace(strOutputItem) ? strOutputItem : @"";
                                break;
                            case 2:
                                variant.color = !string.IsNullOrWhiteSpace(strOutputItem) ? strOutputItem : @"";
                                break;
                            case 3:
                                variant.size = !string.IsNullOrWhiteSpace(strOutputItem) ? strOutputItem : @"";
                                break;
                            default:
                                break;
                        }
                    }


                    int qty = !string.IsNullOrWhiteSpace(fc["master-inventory"]) ? int.Parse(fc["master-inventory"]) : 0;
                    // if don't track qty, it would set qty = -1
                    variant.inventory_policy = "1";
                    if (fc["inventoryPolicy"].Equals("100"))
                    {
                        qty = -1;
                        variant.inventory_policy = "0";
                    }
                    variant.quantity = qty;
                    variant.image = new ImageItem();
                    variant.image = product.image;
                    if (product.image.arrayImage != null && product.image.arrayImage.Count > 0)
                    {
                        int pIndex = 0, uidV;

                        if (int.TryParse(product.image.imageDetault, out uidV))
                        {
                            pIndex = Convert.ToInt32(product.image.imageDetault);
                            variant.image.imageDetault = product.image.arrayImage[pIndex];
                        }
                        else
                        {
                            Uri uri = null;
                            if (Uri.TryCreate(product.image.imageDetault, UriKind.Absolute, out uri))
                            {
                                variant.image.imageDetault = product.image.imageDetault;
                            }
                            else
                            {
                                variant.image.imageDetault = Url.Content("~/Content/images/imageDefault.png").ToString();
                            }

                        }

                    }
                    else
                    {
                        // variant.image.imageDetault = Url.Content("~/Content/images/imageDefault.png").ToString();
                        variant.image.imageDetault = "0";
                    }
                    variant.price = !string.IsNullOrWhiteSpace(fc["price"]) ? double.Parse(fc["price"]) : 0.00;
                    variant.priceCost = !string.IsNullOrWhiteSpace(fc["priceCost"]) ? double.Parse(fc["priceCost"]) : 0.00;
                    variant.weight = !string.IsNullOrWhiteSpace(fc["weight"]) ? double.Parse(fc["weight"]) : 0.00;
                    variant.requireShipping = product.requireShipping;
                    variant.masterInventory = qty;
                    variant.remindStock = product.remindStock;
                    variant.productID = product._id;
                    variant._id = Guid.NewGuid().ToString();
                    variant.createdDate = product.createdDate;
                    variant.currency = currency;
                    variant.modifiedDate = product.modifiedDate;
                    variant.userID = product.userID;
                    variant.userOwner = product.userOwner;
                    variant.priceCompare = product.priceCompare;
                    variant.status = product.status;
                    product.arrayVarient.Add(variant);
                }
            }
            // no choice option of product
            else
            {
                VarientItem variant = new VarientItem();
                variant.sku = product.sku;
                int qty = !string.IsNullOrWhiteSpace(fc["master-inventory"]) ? int.Parse(fc["master-inventory"]) : 0;
                // if don't track qty, it would set qty = -1
                variant.inventory_policy = "1";
                if (fc["inventoryPolicy"].Equals("100"))
                {
                    qty = -1;
                    variant.inventory_policy = "0";
                }
                variant.title = string.Format("{0} Default", product.name);
                variant.color = "";
                variant.size = "";
                variant.quantity = qty;
                variant.image = new ImageItem();
                if (product.image.arrayImage != null && product.image.arrayImage.Count > 0)
                {
                    int pIndex = 0, uidV;

                    if (int.TryParse(product.image.imageDetault, out uidV))
                    {
                        pIndex = Convert.ToInt32(product.image.imageDetault);
                        variant.image.imageDetault = product.image.arrayImage[pIndex];
                    }
                    else
                    {
                        Uri uri = null;
                        if (Uri.TryCreate(product.image.imageDetault, UriKind.Absolute, out uri))
                        {
                            variant.image.imageDetault = product.image.imageDetault;
                        }
                        else
                        {
                            variant.image.imageDetault = Url.Content("~/Content/images/imageDefault.png").ToString();
                        }

                    }

                }
                else
                {
                    // variant.image.imageDetault = Url.Content("~/Content/images/imageDefault.png").ToString();
                    variant.image.imageDetault = "0";
                }
                variant.price = !string.IsNullOrWhiteSpace(fc["price"]) ? double.Parse(fc["price"]) : 0.00;
                variant.priceCost = !string.IsNullOrWhiteSpace(fc["priceCost"]) ? double.Parse(fc["priceCost"]) : 0.00;
                variant.weight = !string.IsNullOrWhiteSpace(fc["weight"]) ? double.Parse(fc["weight"]) : 0.00;
                variant.requireShipping = product.requireShipping;
                variant.masterInventory = !string.IsNullOrWhiteSpace(fc["master-inventory"]) ? int.Parse(fc["master-inventory"]) : 0;
                variant.remindStock = product.remindStock;
                variant.productID = product._id;
                variant._id = Guid.NewGuid().ToString();
                variant.createdDate = product.createdDate;
                variant.currency = currency;
                variant.modifiedDate = product.modifiedDate;
                variant.userID = product.userID;
                variant.userOwner = product.userOwner;
                variant.priceCompare = product.priceCompare;
                variant.status = product.status;
                product.arrayVarient.Add(variant);
            }
            /*
             * Process to create Product Item with Inventory list
             */
            WSProductClient client1 = new WSProductClient();
            int statusCode = client1.WSCreateProduct(product, VariableConfigController.GetBucket());
            client1.Close();
            //Console.WriteLine("CODE" + statusCode.ToString());
            WSProductItemClient clientProductItem = new WSProductItemClient();
            WSInventoryClient clientInventory = new WSInventoryClient();
            var arrayInventory = clientInventory.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket());
            var productMasterId = string.Empty;
            foreach (var item in arrayInventory)
            {
                ProductItem productItem;
                if (item.businessID.Equals(ConstantSmoovs.Stores.Master))
                {
                    productItem = new ProductItem(product, true);
                    productItem._id = Guid.NewGuid().ToString();
                    productMasterId = productItem._id;
                }
                else
                {
                    productItem = new ProductItem(product, false);
                    productItem._id = Guid.NewGuid().ToString();
                }

                productItem.store = item.businessID;
                productItem.productID = product._id;
                productItem.table = "ProductItem";
                productItem.index = item.index;
                productItem.createdDate = DateTime.UtcNow;
                productItem.modifiedDate = DateTime.UtcNow;
                productItem.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                var idMerchant = VariableConfigController.GetIDMerchant();
                productItem.channels = new String[] { idMerchant + "_" + item.index.ToString() + "_pos" };
                productItem.bucket = bucket;
                int responseCode = clientProductItem.WSCreateProductItem(productItem, VariableConfigController.GetBucket());
            }
            clientProductItem.Close();
            if (!string.IsNullOrWhiteSpace(product.categoryId))
            {
                AddProductIntoCategory(product._id, product.description, product.name, product.categoryId);
            }

            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
            MerchantSetting setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
            setting.addProduct = true;
            int checkStatus = clientMerchantSetting.UpdateMerchantSetting(setting, VariableConfigController.GetBucket());
            clientMerchantSetting.Close();

            if (string.IsNullOrEmpty(productMasterId))
            {
                productMasterId = product._id;
            }

            //Update count
            count++;
            Session[ConstantSmoovs.Product.ProductCount] = count;

            return RedirectToAction("Edit", new { id = productMasterId });
        }
        /*
        *  - Get list category to show list Category in Product Detail 
        *  - Get information detail of Product  
        *  - Nguyen Anh Dao (Zin)
        *  - created:24/11/2014
        *  - modified: 26/11/2014
        */
        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        [Authorize]
        public ActionResult Edit(string id)
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            ViewBag.Class = "product";
            //ViewBag.NumberInit = CheckInitiation();
            WSCategoryClient clientCategory = new WSCategoryClient();
            Category[] listCategory = new Category[1000];
            try
            {
                listCategory = clientCategory.WSGetAllCategoryNoLimit(ConstantSmoovs.CouchBase.DesignDocCategory, ConstantSmoovs.CouchBase.GetAllCategory, bucket);
            }
            catch
            {
                ViewBag.Message = "Cannot connect database.";
            }

            //    var ListCategory = clientCategory.WSGetAllCategoryNoLimit("category", "get_all_category_active");
            clientCategory.Close();


            WSProductItemClient client = new WSProductItemClient();
            var product = client.WSDetailProductItem(id, bucket);
            client.Close();
            ViewBag.listCategory = listCategory.ToList();
            List<VarientItem> ListVariant = new List<VarientItem>();
            try
            {
                if (product.arrayVarient != null)
                    ListVariant = product.arrayVarient;
            }
            catch
            {
                ViewBag.Message = "Error database.";
            }
            ViewBag.listVarient = ListVariant;

            ViewBag.status = product.status;
            ViewBag.listImage = product.image.arrayImage;
            ViewBag.NumPictures = ViewBag.listImage.Count;

            ViewBag.TotalItemVariant = 0;
            ViewBag.CountVariant = 0;
            int totalItemVariant = 0;
            if (product.arrayVarient.Count > 0 && product.arrayVarient != null)
            {
                foreach (VarientItem item in product.arrayVarient)
                {
                    if (item.quantity > 0)
                        totalItemVariant = totalItemVariant + item.quantity;

                }
                ViewBag.CountVariant = product.arrayVarient.Count;
            }
            ViewBag.TotalItemVariant = totalItemVariant;
            if (!string.IsNullOrWhiteSpace(VariableConfigController.limitImage))
            {

                ViewBag.LimitImage = VariableConfigController.limitImage;
            }
            else
            {
                ViewBag.LimitImage = 15;
            }

            //Get and set merchant's currency
            ViewBag.currency = GetGeneralSetting().Currency;

            return View(product);
        }

        public ActionResult ChooseInventory()
        {
            return View();

        }


        /// <summary>
        /// Views the detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/30/2015-7:06 PM</datetime>
        [Authorize]
        public ActionResult ViewDetail(string id)
        {
            //ViewBag.NumberInit = CheckInitiation();
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            ViewBag.Class = "product";
            WSCategoryClient clientCategory = new WSCategoryClient();
            var ListCategory = clientCategory.WSGetAllCategoryNoLimit(ConstantSmoovs.CouchBase.DesignDocCategory, ConstantSmoovs.CouchBase.GetAllCategory, bucket);
            clientCategory.Close();
            WSProductItemClient client = new WSProductItemClient();
            var product = client.WSDetailProductItem(id, bucket);
            client.Close();

            //  ViewBag.idCategory = list.categoryId;
            ViewBag.listCategory = ListCategory.ToList();
            var ListVariant = product.arrayVarient;
            //ViewBag.NumPictures = 0;

            ViewBag.TotalItemVariant = 0;
            ViewBag.CountVariant = 0;
            int totalItemVariant = 0;
            if (ListVariant.Count > 0 && ListVariant != null)
            {
                foreach (VarientItem item in ListVariant)
                {
                    if (item.quantity > 0)
                        totalItemVariant = totalItemVariant + item.quantity;

                }
                ViewBag.CountVariant = ListVariant.Count;
            }
            ViewBag.TotalItemVariant = totalItemVariant;
            GetCountryAndCurrency(bucket);


            return View(product);
            //return View();
        }

        /// <summary>
        /// Gets the country and currency.
        /// </summary>
        /// <param name="bucket">The bucket.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/31/2015-10:42 AM</datetime>
        private void GetCountryAndCurrency(string bucket)
        {
            // Get all country and currency
            WSInventoryClient clientIventory = new WSInventoryClient();
            Country[] countrys = clientIventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, bucket);
            ViewBag.countryModel = countrys[0];
            clientIventory.Close();

            //Get and set merchant's currency
            ViewBag.currency = GetGeneralSetting().Currency;
        }
        /*
        * Update status product on ALL STORE
        * Modified: Nguyen Anh Dao (Zin)
        * 02/12/2014
        */
        /// <summary>
        /// Updates the status product.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult updateStatusProduct(string id, bool status)
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            WSProductItemClient client = new WSProductItemClient();
            UpdateStatusForAllStore(id, status, bucket, client);
            client.Close();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Updates the status for all store.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <param name="bucket">The bucket.</param>
        /// <param name="client">The client.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/31/2015-10:37 AM</datetime>
        private static void UpdateStatusForAllStore(string id, bool status, string bucket, WSProductItemClient client)
        {
            var productItem = client.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, id, bucket);

            WSCollectionClient clientCollection = new WSCollectionClient();
            var modelCollection = clientCollection.WSGetAllCollection(ConstantSmoovs.CouchBase.DesignDocCollection, ConstantSmoovs.CouchBase.CollectionViewAll, VariableConfigController.GetBucket());

            foreach (ProductItem product in productItem)
            {
                product.status = status;
                for (int i = 0; i < product.arrayVarient.Count; i++)
                {
                    product.arrayVarient[i].status = status;
                }
                client.WSUpdateProductItem(product, bucket);

                foreach (var collection in modelCollection)
                {
                    if (collection.arrayProduct != null && collection.arrayProduct.Where(p => p._id == product._id).Any())
                    {
                        var arrayProduct = collection.arrayProduct.ToList();
                        for (int i = 0; i < arrayProduct.Count; i++)
                        {
                            if (arrayProduct[i]._id == product._id)
                            {
                                arrayProduct[i].status = product.status;
                            }
                        }

                        collection.arrayProduct = arrayProduct;
                        clientCollection.WSUpdateCollection(collection, VariableConfigController.GetBucket());
                    }
                }
            }
        }
        /*
     * Update status multi product on ALL STORE
     * Modified: Nguyen Anh Dao (Zin)
     * 02/12/2014
     */
        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="arrID">The arr identifier.</param>
        /// <param name="status">The status.</param>
        [HttpPost]
        public void updateStatus(string[] arrID, Boolean status)
        {
            WSProductItemClient client = new WSProductItemClient();
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            client.WSUpdateStatus(arrID, status, bucket);
            client.Close();
        }

        /*
        * Update product with important information: image, category and other field of product
        * created: Nguyen Anh Dao (Zin)
        * 24/11/2014
        */
        /// <summary>
        /// Updates the specified fc.
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(FormCollection fc)
        {
            //Save form collection into database
            UpdateProduct(fc);

            //Redirect as needed
            if (fc["hfRedirectTo"] == "ViewDetail") return RedirectToAction("ViewDetail", new { id = fc["id"] });
            else return RedirectToAction("distribute", "productitem", new { productId = fc["productID"] });
        }

        private void UpdateProduct(FormCollection fc)
        {
            string bucket = VariableConfigController.GetBucket();

            WSProductItemClient client = new WSProductItemClient();
            var productItems = client.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, fc["productID"], bucket);
            string _idProduct = null, descriptionProduct = null, nameProduct = null, categoryId = null;
            //bool changeStatus = false;

            bool changeStatus = ChangeStatusOfProductMaster(fc, productItems);

            WSCollectionClient clientCollection = new WSCollectionClient();
            Collection[] modelCollection = new List<Collection>() { }.ToArray();
            if (changeStatus)
            {
                modelCollection = clientCollection.WSGetAllCollection(ConstantSmoovs.CouchBase.DesignDocCollection, ConstantSmoovs.CouchBase.CollectionViewAll, VariableConfigController.GetBucket());
            }

            foreach (ProductItem product in productItems)
            {
                if (product != null)
                {
                    //--------------------------- begin DELETE product in arrayProduct of Category
                    if (!string.IsNullOrWhiteSpace(product.categoryId))
                    {
                        DeleteProductInCategory(product.productID, product.categoryId);
                    }
                    //--------------------------- end DELETE product in arrayProduct of Category
                    SetImageProduct(fc, product);
                    try
                    {
                        product.productType = fc["productType"];
                        product.vendor = fc["vendor"];
                        product.modifiedDate = DateTime.UtcNow;
                        product.description = fc["description"];
                        product.name = fc["name"];
                        product.title = fc["name"];
                        product.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                        product.nameShort = fc["nameShort"];
                        //------------------------------- begin split CategoryID and CategoryName
                        UpdateCategoryProduct(fc, bucket, ref _idProduct, ref descriptionProduct, ref nameProduct, ref categoryId, product);

                        ////------------------------------- END UPDATE PRODUCT IN  CATEGORY
                        if (changeStatus) // if change status 
                        {
                            if (product.arrayVarient != null && product.arrayVarient.Count > 0)
                            {
                                for (int i = 0; i < product.arrayVarient.Count; i++)
                                {
                                    product.arrayVarient[i].status = product.status;
                                }
                            }

                            foreach (var collection in modelCollection)
                            {
                                if (collection.arrayProduct != null && collection.arrayProduct.Where(p => p._id == product._id).Any())
                                {
                                    var arrayProduct = collection.arrayProduct.ToList();
                                    for (int i = 0; i < arrayProduct.Count; i++)
                                    {
                                        if (arrayProduct[i]._id == product._id)
                                        {
                                            arrayProduct[i].status = product.status;
                                        }
                                    }

                                    collection.arrayProduct = arrayProduct;

                                    clientCollection.WSUpdateCollection(collection, VariableConfigController.GetBucket());
                                }
                            }
                        }

                        // int statusCode = client.WSUpdateProduct(product);
                        client.WSUpdateProductItem(product, bucket);
                    }
                    catch
                    {

                    }
                }
            }
            // END: Update discount when product change --------------------------------------
            client.Close();

            if (!string.IsNullOrWhiteSpace(categoryId))
            {
                AddProductIntoCategory(_idProduct, descriptionProduct, nameProduct, categoryId);
            }
        }

        private void UpdateCollection(ProductItem[] productItems)
        {
            WSCollectionClient client = new WSCollectionClient();
            IEnumerable<Collection> ListCollection = client.WSGetAllCollection(ConstantSmoovs.CouchBase.DesignDocCollection, ConstantSmoovs.CouchBase.CollectionViewAll, VariableConfigController.GetBucket());
            foreach (ProductItem productitem in productItems)
            {
                var collectoinOfProductItem = ListCollection.Where(d => d.arrayProduct.Select(l => l._id).ToList().Contains(productitem._id));
                foreach (Collection collection in collectoinOfProductItem)
                {
                    foreach (var productItemC in collection.arrayProduct)
                    {
                        if (productItemC._id == productitem._id)
                        {
                            List<SmallProductItem> lisproductItems = new List<SmallProductItem>();
                            SmallProductItem smallProd = GetSmallProduct(productitem);

                            lisproductItems.Add(smallProd);
                            collection.arrayProduct = lisproductItems;
                            client.WSUpdateCollection(collection, VariableConfigController.GetBucket());

                        }
                    }

                }
            }
        }
        private SmallProductItem GetSmallProduct(ProductItem product)
        {
            SmallProductItem smallProd = new SmallProductItem();
            smallProd.name = product.name;
            smallProd._id = product._id;
            smallProd.categoryId = product.categoryId;
            smallProd.categoryName = product.categoryName;
            var imageUrl = Url.Content("~/Content/Images/imageDefault.png");

            if (product.image.arrayImage != null && product.image.arrayImage.Count > 0)
            {
                int pIndex = 0, uidV;

                if (int.TryParse(product.image.imageDetault, out uidV))
                {
                    pIndex = Convert.ToInt32(product.image.imageDetault);
                    imageUrl = product.image.arrayImage[pIndex];
                }
                else
                {
                    Uri uri = null;
                    if (Uri.TryCreate(product.image.imageDetault, UriKind.Absolute, out uri))
                    {
                        imageUrl = product.image.imageDetault;
                    }
                    else
                    {
                        imageUrl = Url.Content("~/Content/images/imageDefault.png").ToString();
                    }

                }

            }

            smallProd.urlImageProduct = imageUrl;
            smallProd.createdDate = DateTime.UtcNow;
            return smallProd;
        }

        /// <summary>
        /// Sets the image product.
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <param name="product">The product.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/30/2015-7:04 PM</datetime>
        private static void SetImageProduct(FormCollection fc, ProductItem product)
        {
            if (product.image == null || product.image.imageDetault == null || product.image.arrayImage == null)
            {
                //------------- tao doi tuong IMAGE ITEM
                ImageItem image = new ImageItem();
                List<string> listImage = new List<string>();
                image.imageDetault = "0";
                image.arrayImage = new List<string>();
                image.arrayImage = listImage;
                //------------
                //------------- tao doi tuong PRODUCT.IMAGE
                product.image = new ImageItem();
                product.image = image;
            }

            string patternURL = ",";
            Regex myRegex = new Regex(patternURL);
            if (!string.IsNullOrWhiteSpace(fc["listImageSave"]))
            {
                string listStringImage = fc["listImageSave"];
                string[] resultStatic = myRegex.Split(listStringImage);
                product.image.arrayImage = new List<string>();
                product.image.arrayImage.AddRange(resultStatic.ToList());
                if (product.arrayVarient != null && product.arrayVarient.Count > 0)
                {
                    // int indexVariant = 0;
                    //   foreach(VarientItem itemVariant in product.arrayVarient)
                    for (int i = 0; i < product.arrayVarient.Count; i++)
                    {
                        VarientItem itemVariant = new VarientItem();
                        itemVariant = product.arrayVarient[i];
                        if (itemVariant.image != null)
                        {
                            itemVariant.image.arrayImage = product.image.arrayImage;
                            product.arrayVarient[i] = itemVariant;
                        }
                        else
                        {

                            itemVariant.image = new ImageItem();
                            itemVariant.image.imageDetault = "0";
                            itemVariant.image.arrayImage = product.image.arrayImage;
                            product.arrayVarient[i] = itemVariant;
                        }

                        //   indexVariant++;
                    }



                }

            }
            else
            {
                product.image.arrayImage = new List<string>();
            }


            product.image.imageDetault = !string.IsNullOrWhiteSpace(fc["inputCheck"]) ? fc["inputCheck"] : "0";
        }

        /// <summary>
        /// Updates the category product.
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <param name="bucket">The bucket.</param>
        /// <param name="_idProduct">The _id product.</param>
        /// <param name="descriptionProduct">The description product.</param>
        /// <param name="nameProduct">The name product.</param>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="product">The product.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/30/2015-6:38 PM</datetime>
        private static void UpdateCategoryProduct(FormCollection fc, string bucket, ref string _idProduct, ref string descriptionProduct, ref string nameProduct, ref string categoryId, ProductItem product)
        {
            string Category = fc["drlCategory"];
            int indexSymbol = -1;
            if (!string.IsNullOrWhiteSpace(fc["drlCategory"]))
            {
                indexSymbol = Category.IndexOf('~');
                if (indexSymbol > 0)
                {
                    int lenghtOfName = Category.Length - 37;
                    product.categoryId = Category.Substring(0, indexSymbol);
                    product.categoryName = Category.Substring(indexSymbol + 1, Category.Length - indexSymbol - 1); // tru di vi tri dau ~
                }
                else
                {
                    product.categoryId = product.categoryName = Category;
                }
            }
            else
            {
                product.categoryId = "";
                product.categoryName = "";

            }
            //------------------------------- end split CategoryID and CategoryName 
            //------------------------------- BEGIN ADD PRODUCT IN  CATEGORY BY ZIN

            if (!string.IsNullOrWhiteSpace(product.categoryId))
            {
                _idProduct = product.productID;
                descriptionProduct = product.description;
                nameProduct = product.name;
                categoryId = product.categoryId;
                // AddProductIntoCategory(product._id, product.description, product.name, product.categoryId);
            }

            ////------------------------------- END ADD PRODUCT IN  CATEGORY
            if (!string.IsNullOrWhiteSpace(product.categoryId))
            {
                Category categoryItem = new Category();
                WSCategoryClient clientCategory = new WSCategoryClient();
                categoryItem = clientCategory.WSDetailCategory(product.categoryId, bucket);
                if (categoryItem != null && categoryItem.arrayProduct != null && categoryItem.arrayProduct.Count > 0)
                {
                    int indexCate = 0;
                    foreach (CategoryProductItem itemProduct in categoryItem.arrayProduct)
                    {
                        // if (categoryItem.arrayProduct[indexCate].idProduct.ToString().Equals(product._id))
                        if (categoryItem.arrayProduct[indexCate].idProduct.ToString().Equals(product.productID))
                        {
                            itemProduct.description = fc["description"];
                            itemProduct.name = fc["name"];
                            categoryItem.arrayProduct[indexCate] = itemProduct;
                            clientCategory.UpdateProductCategory(product.categoryId, itemProduct, bucket);
                            break;
                        }

                        indexCate++;
                    }
                    clientCategory.WSUpdateCategory(categoryItem, bucket);

                    //--------------


                }
                clientCategory.Close();
            }
        }

        /// <summary>
        /// Changes the status of product master.
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <param name="productItems">The product items.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/30/2015-6:39 PM</datetime>
        private static bool ChangeStatusOfProductMaster(FormCollection fc, ProductItem[] productItems)
        {
            bool changeStatus = false;

            bool status = false;
            try
            {
                status = Boolean.Parse(fc["status"]);
            }
            catch (Exception)
            {

            }
            foreach (var item in productItems.Where(p => p.store.Equals(ConstantSmoovs.Stores.Master)))
            {
                if (item.status != status)
                {
                    changeStatus = true;
                    break;
                }
            }
            return changeStatus;
        }
        /*
        * - Call delete product in arrayProduct in Category 
        * - Delete product 
        * - Nguyen Anh Dao (Zin)
        * - created:24/11/2014
        * - modified:26/11/2014
        */
        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>System.Int32.</returns>
        [HttpPost]
        public int Delete(string id)
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            ViewBag.SyncType = "Synchronous";
            ViewBag.Class = "product";
            WSProductItemClient client = new WSProductItemClient();
            var productItem = client.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, id, bucket);

            WSCollectionClient clientCollection = new WSCollectionClient();
            var modelCollection = clientCollection.WSGetAllCollection(ConstantSmoovs.CouchBase.DesignDocCollection, ConstantSmoovs.CouchBase.CollectionViewAll, VariableConfigController.GetBucket());
            foreach (ProductItem product in productItem)
            {

                if (product != null)
                {
                    if (!string.IsNullOrWhiteSpace(product.categoryId))
                    {
                        DeleteProductInCategory(id, product.categoryId);

                    }
                    DeleteProductItem(id);

                    client.WSDetailProductItem(id, bucket);

                    foreach (var collection in modelCollection)
                    {
                        if (collection.arrayProduct != null && collection.arrayProduct.Where(p => p._id == product._id).Any())
                        {
                            var arrayProduct = collection.arrayProduct.Where(p => p._id != product._id).ToList();
                            collection.arrayProduct = arrayProduct;

                            clientCollection.WSUpdateCollection(collection, VariableConfigController.GetBucket());
                        }
                    }
                }
            }
            client.Close();
            return 1;

        }

        /// <summary>
        /// Saves the variant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult SaveVariant(String id)
        {
            ViewBag.Class = "product";
            //WSProductClient client = new WSProductClient();
            //var list = client.WSDetailProduct(id);
            WSProductItemClient client = new WSProductItemClient();
            var list = client.WSDetailProductItem(id, VariableConfigController.GetBucket());
            client.Close();


            return RedirectToAction("Edit");
        }

        /// <summary>
        /// Deletes the variant.
        /// </summary>
        /// <param name="varID">The variable identifier.</param>
        /// <param name="proID">The pro identifier.</param>
        public void DeleteVariant(string varID, string proID)
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            //Com.SmoovPOS.Entity.ProductItem product = new Com.SmoovPOS.Entity.ProductItem();

            //WSProductClient WSProduct = new WSProductClient();
            //product = WSProduct.WSDetailProduct(proID);
            WSProductItemClient client = new WSProductItemClient();
            var productItem = client.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, proID, bucket);
            foreach (ProductItem product in productItem)
            {


                List<VarientItem> arrVar_ = product.arrayVarient;

                arrVar_.RemoveAll(item => item._id == varID);

                product.arrayVarient = arrVar_;
                //WSProduct.WSUpdateProduct(product);
                client.WSUpdateProductItem(product, bucket);
            }
            client.Close();

        }
        /*
         * Function check duplicate titleOption in Variant
         *
         */
        /// <summary>
        /// Checks the duplicate option.
        /// </summary>
        /// <param name="newOption">The new option.</param>
        /// <param name="listOption">The list option.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        private bool checkDuplicateOption(string newOption, List<string> listOption)
        {
            bool isDuplicate = false;
            for (int i = 0; i < listOption.Count(); i++)
            {
                if (listOption[i].Equals(newOption))
                {
                    isDuplicate = true;
                }
            }
            return isDuplicate;
        }

        /// <summary>
        /// Saves the variant add.
        /// </summary>
        /// <param name="variantItem">The variant item.</param>
        /// <returns>System.Int32.</returns>
        public int SaveVariantAdd(VarientItem variantItem)
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            WSProductItemClient client = new WSProductItemClient();
            var productItem = client.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, variantItem.productID, bucket);
            variantItem._id = Guid.NewGuid().ToString();
            foreach (ProductItem product in productItem)
            {

                if (!product.store.Equals(ConstantSmoovs.Stores.Master) && variantItem.inventory_policy.Equals("1"))
                {
                    variantItem.quantity = 0;
                }
                else if (!product.store.Equals(ConstantSmoovs.Stores.Master) && !variantItem.inventory_policy.Equals("1"))
                {
                    variantItem.quantity = -1;
                }
                else if (variantItem.quantity != -1)
                {
                    int qtyVariant = variantItem.masterInventory;
                    variantItem.quantity = qtyVariant;
                }
                variantItem.image = new ImageItem();

                variantItem.image.arrayImage = new List<string>();
                if (product.image.arrayImage.Count > 0)
                {
                    variantItem.image.arrayImage = product.image.arrayImage;
                    variantItem.image.imageDetault = product.image.imageDetault.Length > 4 ? product.image.imageDetault : product.image.arrayImage[int.Parse(product.image.imageDetault)];
                }
                variantItem.userID = product.userID;
                variantItem.userOwner = product.userOwner;
                product.arrayVarient.Add(variantItem);
                foreach (var item in product.arrayInventory)
                {
                    switch (item.name)
                    {
                        case "title":
                            {// no duplicate then updating 
                                if (!this.checkDuplicateOption(variantItem.title, item.options))
                                {
                                    item.options.Add(variantItem.title);
                                }
                            }
                            break;
                        case "color":
                            if (!this.checkDuplicateOption(variantItem.color, item.options))
                            {
                                item.options.Add(variantItem.color);
                            }
                            break;
                        case "size":
                            if (!this.checkDuplicateOption(variantItem.size, item.options))
                            {
                                item.options.Add(variantItem.size);
                            }
                            break;
                        default:
                            break;
                    }
                }
                product.inventoryPolicy = product.arrayVarient.Any(r => r.quantity >= 0) ? "1" : "0";
                client.WSUpdateProductItem(product, bucket);
            }
            client.Close();
            return 1;
            //   return RedirectToAction("Edit", new { id = variantItem.productID });
        }

        /// <summary>
        /// Checks the sku exist.
        /// </summary>
        /// <param name="sku">The sku.</param>
        /// <returns>Boolean.</returns>
        [HttpPost]

        public Boolean checkSKUExist(string sku)
        {

            WSProductClient client = new WSProductClient();
            int count = client.WSCheckSKUExist(ConstantSmoovs.CouchBase.DesignDocProduct, ConstantSmoovs.CouchBase.CheckSKUExists, sku, VariableConfigController.GetBucket());
            if (count == 0) return true;
            else return false;

        }

        /// <summary>
        /// Updates the image variant.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/30/2015-7:03 PM</datetime>
        [HttpPost]
        public int updateImageVariant()
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            NameValueCollection nvc = Request.Form;
            string productIDVariant;
            string patternURL = ",";
            Regex myRegex = new Regex(patternURL);
            if (!string.IsNullOrEmpty(nvc["productIDVariant"]))
            {
                productIDVariant = nvc["productIDVariant"];
                WSProductItemClient client = new WSProductItemClient();
                var productItem = client.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productIDVariant, bucket);
                foreach (ProductItem product in productItem)
                {

                    if (!string.IsNullOrWhiteSpace(nvc["arrayImageVariant"]))
                    {

                        string[] resultStatic = myRegex.Split(nvc["arrayImageVariant"]);
                        if (product.image != null)
                        {
                            product.image.arrayImage = new List<string>();
                            product.image.arrayImage.AddRange(resultStatic.ToList());
                        }
                        else
                        {
                            product.image = new ImageItem();
                            product.image.imageDetault = "0";
                            product.image.arrayImage.AddRange(resultStatic.ToList());
                        }

                    }
                    if (!string.IsNullOrWhiteSpace(nvc["_idVariant"]) && !string.IsNullOrWhiteSpace(nvc["imageDefaultVariant"]))
                    {
                        foreach (VarientItem item in product.arrayVarient.Where(r => r._id == nvc["_idVariant"]))
                        {
                            item.image.imageDetault = nvc["imageDefaultVariant"];
                            client.WSUpdateProductItem(product, bucket);
                        }
                    }
                }
            }


            return 1;


        }
        /// <summary>
        /// Finds the variant in list.
        /// </summary>
        /// <param name="arrayVariant">The array variant.</param>
        /// <param name="variant">The variant.</param>
        /// <returns>System.Int32.</returns>
        public int findVariantInList(List<VarientItem> arrayVariant, VarientItem variant)
        {
            int i = 0;
            foreach (VarientItem item in arrayVariant)
            {
                if (item._id.Equals(variant._id))
                {
                    return i;
                }
                i++;
            }
            return 0;
        }
        /*
        * Update variant of distrubute but not update quantity of Online and quantity of Branch
        * Nguyen Anh Dao (Zin)
        * created: 01/12/2014
        */
        /// <summary>
        /// Updates the variant of distribute.
        /// </summary>
        /// <param name="product">The product.</param>
        [HttpPost]
        public void updateVariantOfDistribute(Com.SmoovPOS.Entity.ProductItem product)
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            WSProductItemClient clientProductItem = new WSProductItemClient();
            var productItem = clientProductItem.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, product.productID, bucket);
            if (productItem != null)
            {
                foreach (ProductItem itemPro in productItem) // Online and Branch
                {
                    // itemPro.arrayVarient.Clear();
                    itemPro.arrayInventory = product.arrayInventory;
                    if (itemPro.arrayVarient != null && itemPro.arrayVarient.Count > 0)
                    {

                        if (itemPro.arrayVarient != null && itemPro.arrayVarient.Count > 0 && !itemPro.store.Equals(ConstantSmoovs.Stores.Master))
                        {
                            int indexVariant = 0;
                            foreach (VarientItem variant in product.arrayVarient)
                            {

                                bool oldStatus = itemPro.arrayVarient[indexVariant].status;
                                int quantityOlder = itemPro.arrayVarient[indexVariant].quantity; // get quantity to save
                                itemPro.arrayVarient[indexVariant] = variant; // update all field 


                                //--------------\
                                if (variant.inventory_policy.Equals("1"))
                                {
                                    //itemPro.arrayVarient[indexVariant].quantity = 0;
                                    if (quantityOlder == -1)
                                    {
                                        itemPro.arrayVarient[indexVariant].quantity = 0;
                                    }
                                    else
                                    {
                                        itemPro.arrayVarient[indexVariant].quantity = quantityOlder;
                                    }

                                }
                                else
                                {
                                    itemPro.arrayVarient[indexVariant].quantity = -1;
                                }
                                itemPro.arrayVarient[indexVariant].status = oldStatus; // keep Old status of variant
                                itemPro.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                                indexVariant++;
                            }
                            clientProductItem.WSUpdateProductItem(itemPro, bucket);
                        }
                    }


                    // clientProductItem.WSUpdateProductItem(itemPro);
                }
            }
            clientProductItem.Close();


        }
        /*
     * Update variant ProductItem Master
     * Nguyen Anh Dao (Zin)
     * created: 02/12/2014
     */
        /// <summary>
        /// Updates the variant.
        /// </summary>
        /// <param name="variant">The variant.</param>
        /// <returns>System.Int32.</returns>
        [HttpPost]
        public int updateVariant(VarientItem variant)
        {
            ProductItem itemProdMaster = new ProductItem();

            WSProductItemClient clientProductItem = new WSProductItemClient();
            var productItem = clientProductItem.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, variant.productID, VariableConfigController.GetBucket());
            foreach (ProductItem itemMaster in productItem)
            {
                if (itemMaster.store.Equals(ConstantSmoovs.Stores.Master))
                {
                    itemProdMaster = itemMaster;
                }
            }


            int index = this.findVariantInList(itemProdMaster.arrayVarient, variant);
            // processing to update list option
            var currVariant = itemProdMaster.arrayVarient[index];
            itemProdMaster.arrayVarient.RemoveAt(index);
            itemProdMaster.arrayVarient.Insert(index, variant);
            for (int i = 0; i < itemProdMaster.arrayInventory.Count(); i++)
            {
                var inventoryItem = itemProdMaster.arrayInventory[i];
                switch (inventoryItem.name)
                {
                    case "title":
                        {
                            if (!variant.title.Equals(currVariant.title))
                            {
                                if (!inventoryItem.options.Any(r => r == variant.title))
                                {
                                    inventoryItem.options.Add(variant.title);
                                }
                                for (int j = 0; j < inventoryItem.options.Count(); j++)
                                {
                                    if (!itemProdMaster.arrayVarient.Any(v => v.title == inventoryItem.options[j]))
                                    {
                                        inventoryItem.options.RemoveAt(j);
                                    }
                                }
                                itemProdMaster.arrayInventory[i] = inventoryItem;
                            }
                        }
                        break;
                    case "color":
                        {
                            if (!variant.color.Equals(currVariant.color))
                            {
                                if (!inventoryItem.options.Any(r => r == variant.color))
                                {
                                    inventoryItem.options.Add(variant.color);

                                }
                                for (int j = 0; j < inventoryItem.options.Count(); j++)
                                {
                                    if (!itemProdMaster.arrayVarient.Any(v => v.color == inventoryItem.options[j]))
                                    {
                                        inventoryItem.options.RemoveAt(j);
                                    }
                                }
                                itemProdMaster.arrayInventory[i] = inventoryItem;
                            }
                        }
                        break;
                    case "size":
                        {
                            if (!variant.size.Equals(currVariant.size))
                            {
                                if (!inventoryItem.options.Any(r => r == variant.size))
                                {
                                    inventoryItem.options.Add(variant.size);

                                }
                                for (int j = 0; j < inventoryItem.options.Count(); j++)
                                {
                                    if (!itemProdMaster.arrayVarient.Any(v => v.size == inventoryItem.options[j]))
                                    {
                                        inventoryItem.options.RemoveAt(j);
                                    }
                                }
                                itemProdMaster.arrayInventory[i] = inventoryItem;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            // End processing to update list option

            itemProdMaster.inventoryPolicy = itemProdMaster.arrayVarient.Any(r => r.quantity >= 0) ? "1" : "0";

            clientProductItem.WSUpdateProductItem(itemProdMaster, VariableConfigController.GetBucket());
            updateVariantOfDistribute(itemProdMaster);
            clientProductItem.Close();
            return 1;
            //    return RedirectToAction("Edit", new { id = variant.productID });
        }



        /// <summary>
        /// Deletes the image variant.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/30/2015-6:43 PM</datetime>
        [HttpPost]
        public int deleteImageVariant()
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            NameValueCollection nvc = Request.Form;
            string _idVariant, productIDVariant;
            string patternURL = ",";
            Regex myRegex = new Regex(patternURL);
            if (!string.IsNullOrEmpty(nvc["productIDVariant"]))
            {
                productIDVariant = nvc["productIDVariant"];
                WSProductItemClient client = new WSProductItemClient();
                var productItem = client.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productIDVariant, bucket);
                foreach (ProductItem product in productItem)
                {

                    if (!string.IsNullOrEmpty(nvc["_idVariant"]))
                    {
                        _idVariant = nvc["_idVariant"];
                        int index = 0;
                        foreach (VarientItem item in product.arrayVarient)
                        {
                            if (item._id.ToString() == _idVariant.ToString())
                            {
                                if (!string.IsNullOrWhiteSpace(nvc["arrayImageVariant"]))
                                {
                                    item.image.imageDetault = "-1";
                                    product.arrayVarient[index] = item;
                                    client.WSUpdateProductItem(product, bucket);
                                    return 1;
                                }
                            }
                            index++;
                        }
                    }
                }
            }
            return 0;
        }


        /// <summary>
        /// Chooses the image default variant.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/30/2015-7:02 PM</datetime>
        [HttpPost]
        public int chooseImageDefaultVariant()
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            NameValueCollection nvc = Request.Form;
            string _idVariant, productIDVariant;
            string patternURL = ",";
            Regex myRegex = new Regex(patternURL);
            if (!string.IsNullOrEmpty(nvc["productIDVariant"]))
            {
                productIDVariant = nvc["productIDVariant"];
                WSProductItemClient client = new WSProductItemClient();
                var productItem = client.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productIDVariant, bucket);
                foreach (ProductItem product in productItem)
                {

                    if (!string.IsNullOrEmpty(nvc["_idVariant"]))
                    {
                        _idVariant = nvc["_idVariant"];
                        int index = 0;
                        foreach (VarientItem item in product.arrayVarient)
                        {
                            if (item._id.ToString() == _idVariant.ToString())
                            {
                                if (!string.IsNullOrWhiteSpace(nvc["arrayImageVariant"]))
                                {
                                    item.image.imageDetault = "-1";
                                    product.arrayVarient[index] = item;
                                    client.WSUpdateProductItem(product, bucket);
                                }
                            }
                            index++;
                        }


                    }



                }
                client.Close();
            }

            return 0;


        }

        /// <summary>
        /// Deletes the product in category.
        /// </summary>
        /// <param name="productID">The product identifier.</param>
        /// <param name="categoryID">The category identifier.</param>
        public void DeleteProductInCategory(string productID, string categoryID)
        {
            // new Thread(() =>
            //{
            Category categoryItem = new Category();
            WSCategoryClient clientCategory = new WSCategoryClient();
            categoryItem = clientCategory.WSDetailCategory(categoryID, VariableConfigController.GetBucket());
            if (categoryItem != null && categoryItem.arrayProduct != null && categoryItem.arrayProduct.Count > 0)
            {
                //int indexCate = 0;
                for (int i = 0; i < categoryItem.arrayProduct.Count; i++)
                {
                    if (categoryItem.arrayProduct[i].idProduct.ToString().Equals(productID.ToString()))
                    {
                        categoryItem.arrayProduct.RemoveAt(i);
                        break;
                    }

                }
                clientCategory.WSUpdateCategory(categoryItem, VariableConfigController.GetBucket());
            }
            clientCategory.Close();
            // }).Start();
        }


        /*
        * Add product in arrayProduct of Category
        * Nguyen Anh Dao (Zin)
        * created: 24/11/2014
        */
        /// <summary>
        /// Adds the product into category.
        /// </summary>
        /// <param name="productID">The product identifier.</param>
        /// <param name="productDescription">The product description.</param>
        /// <param name="productName">Name of the product.</param>
        /// <param name="categoryID">The category identifier.</param>
        public void AddProductIntoCategory(string productID, string productDescription, string productName, string categoryID)
        {
            Category categoryItem = new Category();
            WSCategoryClient clientCategory = new WSCategoryClient();
            categoryItem = clientCategory.WSDetailCategory(categoryID, VariableConfigController.GetBucket());
            if (categoryItem != null && categoryItem.arrayProduct != null)
            {
                CategoryProductItem catePro = new CategoryProductItem();
                catePro.idProduct = productID;
                catePro.name = productName;
                catePro.description = productDescription;
                categoryItem.arrayProduct.Add(catePro);
                int response = clientCategory.WSUpdateCategory(categoryItem, VariableConfigController.GetBucket());
            }
            clientCategory.Close();

        }
        /// <summary>
        /// Deletes the product item.
        /// </summary>
        /// <param name="productID">The product identifier.</param>
        public void DeleteProductItem(String productID)
        {
            WSProductItemClient clientProductItem = new WSProductItemClient();
            var listProductItem = clientProductItem.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productID, VariableConfigController.GetBucket());
            foreach (var productItem in listProductItem)
            {
                clientProductItem.WSDeleteProductItem(productItem._id, VariableConfigController.GetBucket());
            }
            clientProductItem.Close();
        }

        /// <summary>
        /// Creates the variant option.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="selectedOptions">The selected options.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult CreateVariantOption(int index, string[] selectedOptions)
        {
            VariantOption optionItem = new VariantOption() { Index = index };
            optionItem.OptionNameList = new List<SelectListItem>()
            {
                new SelectListItem(){Text = "Title"},
                new SelectListItem(){Text = "Name"},
                new SelectListItem(){Text = "Size"},
                new SelectListItem(){Text = "ABC"},
                new SelectListItem(){Text = "XYZ"},
                new SelectListItem(){Text = ConstantSmoovs.Product.SepVariantOption, Disabled = true},
                new SelectListItem(){Text = ConstantSmoovs.Product.AddNewVariantOption}
            };
            if (selectedOptions != null && selectedOptions.Length > 0)
            {
                SelectListItem item = optionItem.OptionNameList.FirstOrDefault(r => !selectedOptions.Contains(r.Text));
                if (item != null) item.Selected = true;
            }

            return View("~/Views/Product/EditorTemplates/OptionItem.cshtml", optionItem);
        }

        public ActionResult Searching(string sort, string type, string text, int pageGoto = 1, int limit = 25, bool desc = true)
        {
            NewPaging paging = new NewPaging();
            paging.limit = limit;
            paging.pageGoTo = pageGoto;
            paging.text = text;
            paging.sort = sort;
            paging.type = type;
            paging.desc = desc;
            WSProductItemClient client = new WSProductItemClient();

            var ListProductAllStore = client.WSGetAllProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllProductItem, VariableConfigController.GetBucket());
            var model = GetListProductItemsMaster(paging, ListProductAllStore);

            client.Close();
            var productItemGroupByProduct = ListProductAllStore.Where(r => r.store != ConstantSmoovs.Stores.Master).GroupBy(r => r.productID).ToList();
            GetDistributedInStores(model.ListProductItem.ToArray(), productItemGroupByProduct);
            return View(model);
        }

        private static SearchProductModel GetListProductItemsMaster(NewPaging paging, ProductItem[] ListProductAllStore)
        {
            IEnumerable<ProductItem> listSearch = ListProductAllStore.Where(p => p.store == ConstantSmoovs.Stores.Master);
            var model = new SearchProductModel();// client.WSSearchProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.AllProductItemMaster, VariableConfigController.GetBucket(), paging);
            model.Paging = new NewPaging();
            if (listSearch != null)
            {
                if (paging.type == "product")
                {
                    listSearch = listSearch.Where(m => m.title.ToUpper().Contains(paging.text.ToUpper()));
                }
                else if (paging.type == "category")
                {
                    listSearch = listSearch.Where(m => m.categoryName.ToUpper().Contains(paging.text.ToUpper()));
                }

                if (paging.sort == "product")
                {
                    if (paging.desc)
                    {
                        listSearch = listSearch.OrderByDescending(m => m.title);
                    }
                    else
                    {
                        listSearch = listSearch.OrderBy(m => m.title);
                    }
                }
                else if (paging.sort == "category")
                {

                    if (paging.desc)
                    {
                        listSearch = listSearch.OrderByDescending(m => m.categoryName);
                    }
                    else
                    {
                        listSearch = listSearch.OrderBy(m => m.categoryName);
                    }
                }
                model.Paging.total = listSearch.Count();
                model.Paging.pageGoTo = paging.pageGoTo;
                model.Paging.limit = paging.limit;
                model.Paging.text = paging.text;
                model.Paging.sort = paging.sort;
                model.Paging.desc = paging.desc;
                model.Paging.type = paging.type;
                listSearch = listSearch.Skip((paging.pageGoTo - 1) * paging.limit).Take(paging.limit).ToList();
                model.ListProductItem = listSearch;
            }
            return model;
        }

    }
}