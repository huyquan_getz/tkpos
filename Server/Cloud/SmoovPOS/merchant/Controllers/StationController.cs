﻿using Com.SmoovPOS.Entity;
using Newtonsoft.Json.Linq;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models;
using SmoovPOS.UI.Models.Station;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSStationReference;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SmoovPOS.UI.Controllers
{
    public class StationController : BaseController
    {
        string SiteID = VariableConfigController.GetBucket();

        [Authorize]
        public ActionResult Index(string BranchName = "")
        {
            try
            {
                //Define ViewBag variables
                //ViewBag.NumberInit = CheckInitiation();
                ViewBag.Class = "station";

                StationViewModel modelStationView = new StationViewModel();
                modelStationView.ListBranches = new List<Inventory>();
                modelStationView.ListStations = new List<StationItemViewModel>();

                //Get all active branches and assign into model.ListBranches
                WSInventoryClient clientInventory = new WSInventoryClient();
                List<Inventory> ListBranches = clientInventory.WSGetAllActiveBranchesForTableOrder(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, SiteID).OrderBy(i => i.businessID).ToList();
                clientInventory.Close();

                if (ListBranches != null) modelStationView.ListBranches = ListBranches;
                modelStationView.SelectedBranchName = BranchName;

                modelStationView.StationDetail = new Models.StationDetailViewModel();
                return View(modelStationView);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetListStationOfBranch(string id)
        {
            StationViewModel modelStationView = new StationViewModel();

            var lst = id.Split("|".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            if (lst.Length > 1)
            {
                var BranchName = lst[0];
                var BranchIndex = int.Parse(lst[1]);

                List<StationItemViewModel> ListStations = new List<StationItemViewModel>();
                List<string> ListStationNames = new List<string>();

                //Get station by model.SelectedBranchName
                WSStationClient clientStation = new WSStationClient();
                var objStation = clientStation.WSGetListStationsOfBranch(ConstantSmoovs.CouchBase.DesignDocStation, ConstantSmoovs.CouchBase.ViewNameGetAllStationsByBranchIndex, SiteID, BranchIndex);
                clientStation.Close();

                //Had station in CB for this branch --> Define data of objStation.ListStations
                if (objStation != null && objStation.Count() > 0)
                {
                    //Assign items of (list<StationItem>)ListStation into (list<StationItemViewModel>)model.ListStations
                    ListStations = (from i in objStation.OrderBy(r => r.stationName)
                                    select new StationItemViewModel(i, BranchName, BranchIndex)).ToList();

                    ListStationNames = objStation.Select(i => i.stationName).ToList();
                }

                modelStationView.ListStations = ListStations;
                modelStationView.ListStationNames = ListStationNames;
            }

            return PartialView("StationListing", modelStationView);
        }

        public ActionResult StationDetail(StationDetailViewModel model)
        {
            return PartialView(model);
        }

        public FileResult DownloadQRCode(string url, string size, string filename)
        {
            SmoovPOS.Common.CodeGenerator.CodeGenerator cg = new Common.CodeGenerator.CodeGenerator();
            Stream file = cg.QRCodeGeneratorStream(url, Convert.ToInt32(size), filename);
            string contentType = "image/jpg";

            return File(file, contentType, filename);
        }

        public JsonResult IsExistedStation(string StationName, string StationID, int BranchIndex)
        {
            if (string.IsNullOrEmpty(StationName))
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            var isExisted = false;

            WSStationClient clientStation = new WSStationClient();
            Station item = clientStation.WSGetDetailStationByName(ConstantSmoovs.CouchBase.DesignDocStation, ConstantSmoovs.CouchBase.ViewNameGetAllStationsByBranchIndex, SiteID, BranchIndex, StationName);
            clientStation.Close();

            isExisted = (item != null);
            if (isExisted && !string.IsNullOrEmpty(StationID))
            {
                isExisted = (item != null && (item._id != StationID));
            }

            return new JsonResult
            {
                Data = !isExisted,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public ActionResult EditStationItem(StationDetailViewModel model)
        {
            try
            {
                //Define StationID
                if (model.StationID == null) model.StationID = Guid.NewGuid().ToString();

                //Define StationQRCode
                string StationQRCode = VariableConfigController.GetHttpAlias();
                if (StationQRCode.Contains("http://localhost")) StationQRCode = string.Format("{0}/online", StationQRCode);

                //Generate real StationQRCode
                if (StationQRCode.LastIndexOf("/") > 0)
                {
                    StationQRCode = string.Format("{0}TableOrder/ScanQRCode/{1}", StationQRCode, model.StationID);
                }
                else
                {
                    StationQRCode = string.Format("{0}/TableOrder/ScanQRCode/{1}", StationQRCode, model.StationID);
                }
                //Update Station object
                WSStationClient clientStation = new WSStationClient();
                clientStation.WSUpdateStationWithRawData(ConstantSmoovs.CouchBase.DesignDocStation, ConstantSmoovs.CouchBase.ViewNameGetAllStationsByBranchIndex,
                                                            SiteID, model.BranchIndex, model.StationID, model.StationName, model.StationSize, StationQRCode);
                clientStation.Close();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult DeleteStationItem(string StationID)
        {
            try
            {
                //Update Station object
                WSStationClient clientStation = new WSStationClient();
                clientStation.WSDeleteStation(StationID, SiteID);
                clientStation.Close();

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}