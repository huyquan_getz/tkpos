﻿using Com.SmoovPOS.Entity;
using Microsoft.Reporting.WebForms;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models;
using SmoovPOS.UI.Models.Sale.ChartSample.SmoovPayTransaction;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSOrderReference;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class SaleController : BaseController
    {
        static readonly string siteID = VariableConfigController.GetBucket();
        readonly string urlActive = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteMerchantSmoovPOS];

        // GET: Sale
        public ActionResult Index()
        {
            ViewBag.Class = "dashboard";
            ViewBag.Url = urlActive;

            string strReportPeriod = DateTime.Today.ToString("dd MMM yyyy");

            SaleViewModel model = new SaleViewModel();
            model = BuildReportModel(ConstantSmoovs.Sale.DailyReport, strReportPeriod);

            return View(model);
        }

        [HttpPost]
        public ActionResult GenerateReport(string strReportType, string strReportPeriod)
        {
            ViewBag.Url = urlActive;

            SaleViewModel model = new SaleViewModel();
            model = BuildReportModel(strReportType, strReportPeriod);

            return PartialView("SalePartial", model);
        }

        private SaleViewModel BuildReportModel(string strReportType, string strReportPeriod)
        {
            SaleViewModel returnModel = new SaleViewModel();

            //Set begin/end date-hour-min
            returnModel = SetReportPeriodAndCurrency(siteID, strReportType, strReportPeriod);
            returnModel.ReportType = strReportType;

            //Get list of appropriate orders to report
            SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel listReportOrders = returnModel.listReportOrders = GetListOrdersToReport(siteID, returnModel.BeginDate, returnModel.EndDate, returnModel.IsNextDay);

            //Get status index of payment status
            int intPaymenPaid = ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.PaymentStatus.Paid).Key;
            int intPaymenRefunded = ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.PaymentStatus.Refunded).Key;

            #region Get and calculate "totalSales", "totalRevenue", "totalNumberOfOrders" and "averageOrderSize"
            List<string> listOrderIDs = listReportOrders.listPaidOrders.Select(i => i._id).ToList();
            foreach (var item in listReportOrders.listRefundedOrders.Select(i => i._id))
            {
                if (!listOrderIDs.Contains(item)) listOrderIDs.Add(item);
            }
            returnModel.TotalNumberOfOrders = listOrderIDs.Count();


            if (returnModel.TotalNumberOfOrders > 0)
            {
                //EndDate+1 when IsNextDay=true to cover all orders that could be in general setting's daily report period
                if (returnModel.IsNextDay) returnModel.EndDate = returnModel.EndDate.AddDays(1);

                //Sum the total of paid payments and paid-then-refunded payments in the period
                double totalPaidInPeriod = listReportOrders.listPaidOrders.Sum(i => i.grandTotal);
                double totalRefundedInPeriod = listReportOrders.listRefundedOrders.Sum(i => i.grandTotal);

                //Sum the total of paid payments and paid-then-refunded payments in the period
                double totalRevenuePaidInPeriod = listReportOrders.listPaidOrders.Sum(i => i.subTotal);
                double totalRevenueRefundedInPeriod = listReportOrders.listRefundedOrders.Sum(i => i.subTotal);

                //Calculate
                returnModel.TotalSales = Math.Round(totalPaidInPeriod - totalRefundedInPeriod, 2);
                returnModel.TotalRevenue = Math.Round(totalRevenuePaidInPeriod - totalRevenueRefundedInPeriod, 2);
                returnModel.AverageOrderSize = Math.Round(returnModel.TotalSales / returnModel.TotalNumberOfOrders, 2);
            }
            else
            {
                returnModel.TotalSales = 0;
                returnModel.TotalRevenue = 0;
                returnModel.AverageOrderSize = 0;
            }
            #endregion

            Session["SaleViewModel"] = returnModel;

            return returnModel;
        }

        //Set begin/end date-hour-min
        private SaleViewModel SetReportPeriodAndCurrency(string siteID, string strReportType, string strReportPeriod)
        {
            SaleViewModel returnModel = new SaleViewModel();

            //Get saved general settings from couchbase
            MerchantGeneralSetting generalSetting = GetGeneralSetting();
            returnModel.Currency = generalSetting.Currency;
            returnModel.IsNextDay = generalSetting.isDailyReportPeriodEndAtNextDay;

            //Set start and end date
            switch (strReportType)
            {
                case ConstantSmoovs.Sale.DailyReport://ReportType = Daily
                    DateTime dtDate = Convert.ToDateTime(strReportPeriod);
                    returnModel.BeginDate = returnModel.EndDate = dtDate;
                    break;

                case ConstantSmoovs.Sale.WeeklyReport://ReportType = Weekly
                    DateTime dtBeginDate = Convert.ToDateTime(strReportPeriod.Split('-')[0]);
                    returnModel.BeginDate = dtBeginDate;
                    DateTime dtEndDate = Convert.ToDateTime(strReportPeriod.Split('-')[1]);
                    returnModel.EndDate = dtEndDate;
                    break;

                case ConstantSmoovs.Sale.MonthlyReport://ReportType = Monthly
                    DateTime dtBeginMonthYear = Convert.ToDateTime(1 + strReportPeriod);
                    returnModel.BeginDate = dtBeginMonthYear;
                    DateTime dtEndMonthYear = dtBeginMonthYear.AddMonths(1).AddDays(-1);
                    returnModel.EndDate = dtEndMonthYear;
                    break;

                default:
                    break;
            }

            //Set start Hour/min
            if (string.IsNullOrEmpty(generalSetting.DailyReportPeriodBeginFrom)) generalSetting.DailyReportPeriodBeginFrom = "00:00";
            returnModel.BeginDate = returnModel.BeginDate.AddHours(int.Parse(generalSetting.DailyReportPeriodBeginFrom.Split(':')[0])).AddMinutes(int.Parse(generalSetting.DailyReportPeriodBeginFrom.Split(':')[1]));

            //Set end Hour/Min
            if (string.IsNullOrEmpty(generalSetting.DailyReportPeriodEndAt)) generalSetting.DailyReportPeriodEndAt = "23:59";
            returnModel.EndDate = returnModel.EndDate.AddHours(int.Parse(generalSetting.DailyReportPeriodEndAt.Split(':')[0])).AddMinutes(int.Parse(generalSetting.DailyReportPeriodEndAt.Split(':')[1]));

            //Convert to UTC
            returnModel.BeginDate = TimeZoneInfo.ConvertTimeToUtc(returnModel.BeginDate, TimeZoneInfo.FindSystemTimeZoneById(generalSetting.TimeZoneFullName));
            returnModel.EndDate = TimeZoneInfo.ConvertTimeToUtc(returnModel.EndDate, TimeZoneInfo.FindSystemTimeZoneById(generalSetting.TimeZoneFullName));

            return returnModel;
        }

        //Get list of appropriate orders to report
        private SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel GetListOrdersToReport(string siteID, DateTime BeginDate, DateTime EndDate, bool IsNextDay)
        {
            //Get all orders that payment status = paid or refunded and order.paidDate or order.refundDate in period from beginDate to endDate
            //also apply general setting's daily report period for each day from beginDate to endDate 
            WSOrderClient clientOrder = new WSOrderClient();
            SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel listReportOrders = clientOrder.WSGetListPaidAndRefundedOrdersInPeriod(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAll,
                                                                                                                                        siteID, BeginDate, EndDate, IsNextDay, string.Empty);
            clientOrder.Close();

            return listReportOrders;
        }

        //[HttpPost]
        public FileContentResult DisplaySmoovPayTransactionChart()
        {
            SaleViewModel model = (SaleViewModel)Session["SaleViewModel"];
            byte[] renderedBytes = null;
            List<SmoovPayTransactionModel> data = new List<SmoovPayTransactionModel>();

            switch (model.ReportType)
            {
                case ConstantSmoovs.Sale.DailyReport://ReportType = Daily
                    for (int i = 1; i < 7; i++)
                    {
                        SmoovPayTransactionModel row = new SmoovPayTransactionModel();
                        row.Maker = "maker_test_" + i;
                        row.Order = 8 - i;
                        row.Transaction = 33.33 * i;
                        data.Add(row);
                    }
                    renderedBytes = GenerateSmoovPayTransactionChart(data);
                    break;

                case ConstantSmoovs.Sale.WeeklyReport://ReportType = Weekly
                    for (int i = 1; i < 8; i++)
                    {
                        SmoovPayTransactionModel row = new SmoovPayTransactionModel();
                        row.Maker = "maker_test_" + i;
                        row.Order = 8 - i;
                        row.Transaction = 33.33 * i;
                        data.Add(row);
                    }
                    renderedBytes = GenerateSmoovPayTransactionChart(data);
                    break;

                case ConstantSmoovs.Sale.MonthlyReport://ReportType = Monthly
                    for (int i = 1; i < 6; i++)
                    {
                        SmoovPayTransactionModel row = new SmoovPayTransactionModel();
                        row.Maker = "maker_test_" + i;
                        row.Order = 8 - i;
                        row.Transaction = 33.33 * i;
                        data.Add(row);
                    }
                    renderedBytes = GenerateSmoovPayTransactionChart(data);
                    break;
            }

            return File(renderedBytes, "image/jpeg");
        }

        private byte[] GenerateSmoovPayTransactionChart(List<SmoovPayTransactionModel> data)
        {
            //Variables  
            Warning[] warnings;
            string[] streamIds;
            string OutputFormat = "Image";
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            //Declare the Excel's config info
            string deviceInfo = "<DeviceInfo>" +
                                "  <OutputFormat>jpeg</OutputFormat>" +
                                "  <PageWidth>" + "11in" + "</PageWidth>" +
                                "  <PageHeight>" + "8.5in" + "</PageHeight>" +
                                "  <MarginTop>0.0in</MarginTop>" +
                                "  <MarginLeft>0.0in</MarginLeft>" +
                                "  <MarginRight>0.0in</MarginRight>" +
                                "  <MarginBottom>0.0in</MarginBottom>" +
                                "</DeviceInfo>";

            //Generate reportDataSource
            ReportDataSource reportDataSource = new ReportDataSource("SPTDataSet", data);//SBRDataSet is the name of the dataset inside the report

            //Generate reportParameters
            //List<ReportParameter> reportParameters = GenerateSBRParameters(model);

            //Setup the report viewer object and get the array of bytes
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.DataSources.Add(reportDataSource);//Add datasource here  
            viewer.LocalReport.EnableExternalImages = true;
            viewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Models/Sale/ChartSample/SmoovPayTransaction/SPTReport.rdlc");//Map path to default report file
            //viewer.LocalReport.SetParameters(reportParameters);
            byte[] reportBytes = viewer.LocalReport.Render(OutputFormat, deviceInfo, out mimeType, out encoding, out extension, out streamIds, out warnings);

            return reportBytes;
        }
    }
}