﻿using Com.SmoovPOS.Common.Controllers;
using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity;
using SmoovPOS.Utility.WSCategoryReference;
using SmoovPOS.Utility.WSDiscountReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using SmoovPOS.Utility.WSProductItemReference;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class CategoryController : BaseController
    {
        // GET: Category
        [Authorize]
        public ActionResult Index(string sort = "title", string type = "title", string text = "", int pageGoto = 1, int limit = 25, bool desc = false)
        {
            ViewBag.Class = "category";
            ViewBag.NumberInit = CheckInitiation();

            NewPaging paging = new NewPaging();
            paging.limit = limit;
            paging.pageGoTo = pageGoto;
            paging.text = text;
            paging.sort = sort;
            paging.type = type;
            paging.desc = desc;

            WSCategoryClient client = new WSCategoryClient();
            var categoriesList = client.WSSearchCategory(ConstantSmoovs.CouchBase.DesignDocCategory, ConstantSmoovs.CouchBase.GetAllCategory, VariableConfigController.GetBucket(), paging);
            client.Close();

            //Get/Set the quantity of all current branches to session
            int count = categoriesList.Paging.total;
            Session[ConstantSmoovs.Category.CategoryCount] = count;
            //Permission module
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            categoriesList.isAddNew = userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Category, count);

            return View(categoriesList);
        }

        [Authorize]
        public ActionResult Create()
        {
            //Permission with create when limit Category
            int count = 0;
            if (Session[ConstantSmoovs.Category.CategoryCount] == null)
            {
                NewPaging paging = new NewPaging();
                paging.limit = 25;
                paging.pageGoTo = 1;
                paging.text = "";
                paging.sort = "index";
                paging.type = "title";
                paging.desc = false;

                WSCategoryClient clientIn = new WSCategoryClient();
                var categoriesList = clientIn.WSSearchCategory(ConstantSmoovs.CouchBase.DesignDocCategory, ConstantSmoovs.CouchBase.GetAllCategory, VariableConfigController.GetBucket(), paging);
                clientIn.Close();

                //Get/Set the quantity of all current branches to session
                count = categoriesList.Paging.total;
                Session[ConstantSmoovs.Category.CategoryCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Category.CategoryCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Category, count))
            {
                return RedirectToAction("Index");
            }

            ViewBag.Class = "category";
            //ViewBag.NumberInit = CheckInitiation();
            return View();
        }
        /*
        * - Creat Category
        * - Update save link Image to database
        * - Nguyen Anh Dao (Zin)
        * - created: unknow
        * - modified:27/11/2014
        */
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            //Permission with create when limit Category
            int count = 0;
            if (Session[ConstantSmoovs.Category.CategoryCount] == null)
            {
                NewPaging paging = new NewPaging();
                paging.limit = 25;
                paging.pageGoTo = 1;
                paging.text = "";
                paging.sort = "index";
                paging.type = "title";
                paging.desc = false;

                WSCategoryClient client1 = new WSCategoryClient();
                var categoriesList = client1.WSSearchCategory(ConstantSmoovs.CouchBase.DesignDocCategory, ConstantSmoovs.CouchBase.GetAllCategory, VariableConfigController.GetBucket(), paging);
                client1.Close();

                //Get/Set the quantity of all current branches to session
                count = categoriesList.Paging.total;
                Session[ConstantSmoovs.Category.CategoryCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Category.CategoryCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Category, count))
            {
                return RedirectToAction("Index");
            }

            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            var idMerchant = VariableConfigController.GetIDMerchant();
            Category category = new Category();
            category.createdDate = DateTime.Today;
            category.modifiedDate = DateTime.Today;
            category.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            category.title = collection["categoryName"];
            category.description = collection["description"];
            category.arrayProduct = new List<CategoryProductItem>();
            category.channels = new String[] { idMerchant + "_pos" };
            try
            {
                category.status = Boolean.Parse(collection["statusCategory"]);
            }
            catch (Exception)
            {
                category.status = false;
            }

            category.display = true;
            if (!string.IsNullOrWhiteSpace(collection["listImageSave"]))
            {
                category.image = collection["listImageSave"];
            }
            else
            {
                category.image = "";
            }

            category._id = Guid.NewGuid().ToString();
            WSCategoryClient client = new WSCategoryClient();

            int statusCode = client.WSCreateCategory(category, VariableConfigController.GetBucket());
            client.Close();

            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
            MerchantSetting setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, bucket);
            setting.createCategory = true;
            int checkStatus = clientMerchantSetting.UpdateMerchantSetting(setting, bucket);
            clientMerchantSetting.Close();

            //Update count
            count++;
            Session[ConstantSmoovs.Category.CategoryCount] = count;

            return RedirectToAction("Index");
        }

        [Authorize]
        public int Delete(string id)
        {
            ViewBag.Class = "category";
            WSCategoryClient client = new WSCategoryClient();
            int sttCode = client.WSDeleteCategory(id, VariableConfigController.GetBucket());
            client.Close();
            // return RedirectToAction("Index");
            return 1;
            //WSCategoryClient client = new WSCategoryClient();
            //Category category = client.WSDetailCategory(id);
            //category.display = false;
            //category.status = false;
            //client.WSUpdateCategory(category);
            //client.Close();
            //return RedirectToAction("Index");
        }
        [Authorize]
        public ActionResult Detail(string id)
        {
            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "category";
            WSCategoryClient client = new WSCategoryClient();
            Category category = client.WSDetailCategory(id, VariableConfigController.GetBucket());
            client.Close();
            return View(category);
        }
        [Authorize]
        public ActionResult Edit(string id)
        {
            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "category";
            WSCategoryClient client = new WSCategoryClient();
            Category category = client.WSDetailCategory(id, VariableConfigController.GetBucket());
            client.Close();
            return View(category);
        }
        /*
        * - Update Category
        * - Update save link Image to database
        * - Nguyen Anh Dao (Zin)
        * - created: unknow
        * - modified:27/11/2014
        */
        [HttpPost]
        public ActionResult Edit(FormCollection co, string id)
        {
            //string linkImageS3 = "";
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            WSCategoryClient client = new WSCategoryClient();
            Category category = client.WSDetailCategory(id, bucket);
            /*
            string tempImg = category.image;
            HttpPostedFileBase photo = Request.Files["image"];
            int random = Guid.NewGuid().GetHashCode();
            try
            {
                if (photo.FileName != "")
                {     
                    string filename = random.ToString()+"_"+ photo.FileName;
                    string savedFileName = Path.Combine(Server.MapPath("~/Content/Upload/"), filename);
                    photo.SaveAs(savedFileName);
                    String path = savedFileName.Replace("\\", "/").Replace(" ", "");
                    PhotosController ph = new PhotosController();
                    linkImageS3 = ph.UploadToS3Amazon(path);
                    System.IO.File.Delete(path);
                    category.image = linkImageS3;
                }


            }
            catch (Exception) { category.image = tempImg; }            
            */
            if (!string.IsNullOrWhiteSpace(co["listImageSave"]))
            {
                category.image = co["listImageSave"];
            }
            else
            {
                category.image = "";
            }
            category.createdDate = DateTime.Today;
            category.modifiedDate = DateTime.Today;
            category.title = co["categoryName"];
            category.description = co["description"];
            try
            {
                category.status = Boolean.Parse(co["statusCategory"]);
            }
            catch (Exception)
            {
                category.status = false;
            }
            string OldNameCategory = co["NameCategoryOld"];
            client.WSUpdateCategory(category, VariableConfigController.GetBucket());

            if (OldNameCategory != category.title)
            {
                //client.UpdateNameCategory("product", "get_all_by_category", OldNameCategory, category.title);
                WSProductItemClient clientProductItem = new WSProductItemClient();
                var listsAll = clientProductItem.WSGetAllProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllProductItem, bucket);
                var lists = listsAll.Where(r => r.categoryName == OldNameCategory);
                foreach (ProductItem productItem in lists)
                {
                    productItem.categoryName = category.title;
                    clientProductItem.WSUpdateProductItem(productItem, bucket);
                }
                clientProductItem.Close();
            }
            client.Close();
            // START: Update discount when category change --------------------------------------
            //WSDiscountClient discountClient = new WSDiscountClient();
            //IEnumerable<Discount> allDiscounts = discountClient.WSGetAllDiscount(Common.ConstantSmoovs.Discount.DesigndocDiscount, Common.ConstantSmoovs.Discount.ViewAllDiscount, bucket);
            //var discountOfCategory = allDiscounts.Where(d => d.ListProductItems.Select(l => l.categoryId).ToList().Contains(category._id));
            //foreach (Discount discount in discountOfCategory)
            //{
            //    for (int i = 0; i < discount.ListProductItems.Count; i++)
            //    {
            //        if (discount.ListProductItems[i].categoryId == category._id)
            //        {
            //            discount.ListProductItems[i].categoryId = category._id;
            //            discount.ListProductItems[i].categoryName = category.title;
            //        }
            //    }
            //    discountClient.WSUpdateDiscount(discount, null, bucket);
            //}
            // END: Update discount when category change --------------------------------------
            return RedirectToAction("Detail", new { id = id });
        }

        [HttpPost]
        public ActionResult updateStatusCategory(string id, bool status)
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            WSCategoryClient client = new WSCategoryClient();
            Category category = client.WSDetailCategory(id, bucket);
            category.status = status;
            client.WSUpdateCategory(category, bucket);
            client.Close();
            return RedirectToAction("Index");
        }
        [AcceptVerbs(HttpVerbs.Post)]

        [HttpPost]
        public ActionResult updateStatus(string[] arrId, Boolean status)
        {
            WSCategoryClient client = new WSCategoryClient();
            foreach (String id in arrId)
            {
                Category category = client.WSDetailCategory(id, VariableConfigController.GetBucket());
                category.status = status;
                client.WSUpdateCategory(category, VariableConfigController.GetBucket());
            }
           
             client.Close();
          
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult updateStatusMulti(string[] arrId, Boolean status, string sort = "title", string type = "title", string text = "", int pageGoto = 1, int limit = 25, bool desc = false)
        {
            WSCategoryClient client = new WSCategoryClient();
            foreach (String id in arrId)
            {
                Category category = client.WSDetailCategory(id, VariableConfigController.GetBucket());
                category.status = status;
                client.WSUpdateCategory(category, VariableConfigController.GetBucket());
            }

            NewPaging paging = new NewPaging();
            paging.limit = limit;
            paging.pageGoTo = pageGoto;
            paging.text = text;
            paging.sort = sort;
            paging.type = type;
            paging.desc = desc;
            var categoriesList = client.WSSearchCategory(ConstantSmoovs.CouchBase.DesignDocCategory, ConstantSmoovs.CouchBase.GetAllCategory, VariableConfigController.GetBucket(), paging);
            client.Close();
            return PartialView("CategoryListing", categoriesList);
            // return RedirectToAction("Index");
        }
        public ActionResult ReturnImg(String id)
        {
            WSCategoryClient client = new WSCategoryClient();
            Category category = client.WSDetailCategory(id, VariableConfigController.GetBucket());
            client.Close();
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "~/Content/upload", category.image);
            FileStream stream = new FileStream(path, FileMode.Open);
            FileStreamResult result = new FileStreamResult(stream, "image/jpg");
            result.FileDownloadName = category.image;
            return result;

        }


      
       
    }
}