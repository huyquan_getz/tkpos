﻿using AutoMapper;
using Com.SmoovPOS.Entity;
using Microsoft.AspNet.Identity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Models;
using SmoovPOS.Entity.Models.Paging;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.CustomHTMLHelper;
using SmoovPOS.Utility.Models;
using SmoovPOS.Utility.WSCollectionReference;
using SmoovPOS.Utility.WSOnlineReference;
using SmoovPOS.Utility.WSProductItemReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    //enum ActionState { Create, Edit, Delete }
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/30/2015-5:29 PM</datetime>
    public class CollectionController : BaseController
    {
        // GET: Collection
        /// <summary>
        /// Indexes the specified search model.
        /// </summary>
        /// <param name="searchModel">The search model.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:29 PM</datetime>
        public ActionResult Index(SearchCollectionModel searchModel)
        {
            ViewBag.Class = "collection";
            if (searchModel.pageGoTo == 0)
            {
                searchModel.pageGoTo = 1;
                searchModel.sort = string.Empty;
            }
            WSCollectionClient client = new WSCollectionClient();
            var model = client.WSGetAllCollection(ConstantSmoovs.CouchBase.DesignDocCollection, ConstantSmoovs.CouchBase.CollectionViewAll, VariableConfigController.GetBucket());
            var count = model.Count();
            Session[ConstantSmoovs.Collection.CollectionCount] = count;
            SearchCollectionModel sModel = new SearchCollectionModel();
            sModel.ListCollection = model;
            client.Close();
            ViewBag.NumberInit = CheckInitiation();
            //permission module
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            sModel.isAddNew = userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Collection, count);
            return View(sModel);
        }

        /// <summary>
        /// Initializes the data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:29 PM</datetime>
        private void InitData(CollectionViewModel model)
        {
            WSOnlineClient clientOline = new WSOnlineClient();
            var listcategories = clientOline.WSGetAllActiveCategory(Common.ConstantSmoovs.CouchBase.DesignDocCategory, Common.ConstantSmoovs.CouchBase.GetAllCategory, VariableConfigController.GetBucket());
            model.listCategories = listcategories.Count() > 0 ? listcategories.Where(c => c.status).OrderBy(c => c.title).ToList() : new List<Category>();

            WSProductItemClient productItemClient = new WSProductItemClient();
            var listProductItems = productItemClient.WSGetAllProductItemOnStore(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByStore, VariableConfigController.GetBucket());
            //model.listProductItems = listProductItems.Count() > 0 ? listProductItems.OrderBy(x => x.name).ToList() : new List<ProductItem>();
            model.listProductItems = new List<SmallProductItem>();
            var countProduct = listProductItems.Count();
            if (listProductItems != null && countProduct > 0)
            {
                var listCategorieIds = model.listCategories.Select(c => c._id).ToList();
                var listProducts = listProductItems.Where(p => p.status && listCategorieIds.Contains(p.categoryId)).ToList();

                foreach (var itemProduct in listProducts)
                {
                    SmallProductItem cateProd = GetSmallProduct(itemProduct);
                    model.listProductItems.Add(cateProd);
                }
            }
            model.defaultSortList = new List<SelectListItem>() { 
                new SelectListItem(){Text=ConstantSmoovs.Collection.AZ, Value=ConstantSmoovs.Collection.AZ},
                new SelectListItem(){Text=ConstantSmoovs.Collection.ZA, Value=ConstantSmoovs.Collection.ZA},
                new SelectListItem(){Text=ConstantSmoovs.Collection.HighestLowest, Value=ConstantSmoovs.Collection.HighestLowest},
                new SelectListItem(){Text=ConstantSmoovs.Collection.LowestHighest, Value=ConstantSmoovs.Collection.LowestHighest},
                new SelectListItem(){Text=ConstantSmoovs.Collection.NewestOldest, Value=ConstantSmoovs.Collection.NewestOldest},
                new SelectListItem(){Text=ConstantSmoovs.Collection.OldestNewest, Value=ConstantSmoovs.Collection.OldestNewest},
            };
        }

        /// <summary>
        /// Selects the product.
        /// </summary>
        /// <param name="selectProduct">The select product.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:29 PM</datetime>
        /// 
        public ActionResult SelectProduct(string id, string[] selectProduct)
        {
            ViewBag.Class = "collection";
            List<SmallProductItem> lisproducts = new List<SmallProductItem>();
            if (selectProduct != null)
            {
                Collection collection = null;
                if (!string.IsNullOrEmpty(id))
                {
                    WSCollectionClient client = new WSCollectionClient();
                    collection = client.WSDetailCollection(id, VariableConfigController.GetBucket());
                    client.Close();
                }

                WSProductItemClient productItemClient = new WSProductItemClient();
                for (int i = 0; i < selectProduct.Count(); i++)
                {
                    var product = productItemClient.WSDetailProductItem(selectProduct[i], VariableConfigController.GetBucket());
                    SmallProductItem smallProd = GetSmallProduct(product);

                    if (collection != null && collection.arrayProduct != null && collection.arrayProduct.Where(p => p._id.Equals(smallProd._id)).Any())
                    {
                        smallProd.createdDate = collection.arrayProduct.Where(p => p._id.Equals(smallProd._id)).FirstOrDefault().createdDate;
                    }
                    if (!lisproducts.Contains(smallProd))
                    {
                        lisproducts.Add(smallProd);
                    }
                }
                productItemClient.Close();
            }
            lisproducts = lisproducts.OrderByDescending(p => p.createdDate).ToList();
            //BaseController baseController = new BaseController();
            //var generalSetting = baseController.GetGeneralSetting();


            //ViewBag.TimeZone = generalSetting.TimeZone + " " + generalSetting.TimeZoneFullName;
            //ViewBag.TimeZoneFullName = generalSetting.TimeZoneFullName;

            return PartialView(lisproducts);
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:29 PM</datetime>
        [Authorize(Roles = "Merchant")]
        public ActionResult Create()
        {
            ViewBag.Class = "collection";
            //permission with create when limit collection
            var count = 0;
            if (Session[ConstantSmoovs.Collection.CollectionCount] == null)
            {
                WSCollectionClient client = new WSCollectionClient();
                var listCollections = client.WSGetAllCollection(ConstantSmoovs.CouchBase.DesignDocCollection, ConstantSmoovs.CouchBase.CollectionViewAll, VariableConfigController.GetBucket());
                count = listCollections.Count();
                Session[ConstantSmoovs.Collection.CollectionCount] = count;
                client.Close();
            }
            count = Int32.Parse(Session[ConstantSmoovs.Collection.CollectionCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Collection, count))
            {
                return RedirectToAction("Index");
            }

            var model = new CollectionViewModel();

            InitData(model);

            model.status = true;
            model.arrayProduct = new List<SmallProductItem>();
            //InitData(model);
            return View(model);
        }

        /// <summary>
        /// Creates the specified viewmodel.
        /// </summary>
        /// <param name="viewmodel">The viewmodel.</param>
        /// <param name="fc">The fc.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:29 PM</datetime>
        [HttpPost]
        public ActionResult Create(CollectionViewModel viewmodel, FormCollection fc)
        {
            //permission with create when limit collection
            var count = 0;
            WSCollectionClient client = new WSCollectionClient();
            if (Session[ConstantSmoovs.Collection.CollectionCount] == null)
            {
                var listCollections = client.WSGetAllCollection(ConstantSmoovs.CouchBase.DesignDocCollection, ConstantSmoovs.CouchBase.CollectionViewAll, VariableConfigController.GetBucket());
                count = listCollections.Count();
                Session[ConstantSmoovs.Collection.CollectionCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Collection.CollectionCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Collection, count))
            {
                return RedirectToAction("Index");
            }

            StringHelper.Trim(viewmodel);

            string productIDs = fc["product"];
            string[] ListProduct = !string.IsNullOrWhiteSpace(productIDs) ? productIDs.Split(',') : new string[0];
            List<SmallProductItem> lisproductItems = new List<SmallProductItem>();
            WSProductItemClient productItemClient = new WSProductItemClient();
            for (int i = 0; i < ListProduct.Count(); i++)
            {
                var product = productItemClient.WSDetailProductItem(ListProduct[i], VariableConfigController.GetBucket());

                if (product != null)
                {
                    product.arrayVarient = null; // Add to pass error entity too large
                    SmallProductItem smallProd = GetSmallProduct(product);

                    lisproductItems.Add(smallProd);
                }
            }
            productItemClient.Close();

            viewmodel.arrayProduct = lisproductItems;
            viewmodel.image = fc["logoImg"];
            viewmodel._id = Guid.NewGuid().ToString();
            // Check button active or inactive?
            bool isActive = true;
            if (!string.IsNullOrWhiteSpace(fc["optionsRadios"]))
            {
                isActive = Boolean.Parse(fc["optionsRadios"]);
            }
            viewmodel.status = isActive;
            //nerver expired
            bool isNeverExpires = false;
            Boolean.TryParse(fc["neverExpires"], out isNeverExpires);
            viewmodel.isNeverExpires = isNeverExpires;

            viewmodel.createdDate = DateTime.UtcNow;
            viewmodel.userOwner = User.Identity.GetUserId();
            viewmodel.userID = viewmodel.userOwner;
            viewmodel.modifiedDate = DateTime.UtcNow;
            viewmodel.channels = new String[] { "server" };

            var model = Mapper.Map<CollectionViewModel, Collection>(viewmodel);

            var isSuccess = client.WSCreateCollection(model, VariableConfigController.GetBucket());
            client.Close();

            //Update count
            count++;
            Session[ConstantSmoovs.Collection.CollectionCount] = count;

            return RedirectToAction("Index");
            //return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the small product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <returns></returns>
        /// <author>
        /// "Dao.Nguyen"
        /// </author>
        /// <datetime>1/30/2015-4:05 PM</datetime>
        private SmallProductItem GetSmallProduct(ProductItem product)
        {
            SmallProductItem smallProd = new SmallProductItem();
            smallProd.name = product.name;
            smallProd._id = product._id;
            smallProd.categoryId = product.categoryId;
            smallProd.categoryName = product.categoryName;
            smallProd.status = product.status;
            smallProd.productID = product.productID;
            var imageUrl = Url.Content("~/Content/Images/imageDefault.png");

            if (product.image.arrayImage != null && product.image.arrayImage.Count > 0)
            {
                int pIndex = 0, uidV;

                if (int.TryParse(product.image.imageDetault, out uidV))
                {
                    pIndex = Convert.ToInt32(product.image.imageDetault);
                    imageUrl = product.image.arrayImage[pIndex];
                }
                else
                {
                    Uri uri = null;
                    if (Uri.TryCreate(product.image.imageDetault, UriKind.Absolute, out uri))
                    {
                        imageUrl = product.image.imageDetault;
                    }
                    else
                    {
                        imageUrl = Url.Content("~/Content/images/imageDefault.png").ToString();
                    }

                }

            }

            smallProd.urlImageProduct = imageUrl;
            smallProd.createdDate = DateTime.UtcNow;
            return smallProd;
        }

        /// <summary>
        /// Details the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>
        /// "Dao.Nguyen"
        /// </author>
        /// <datetime>1/28/2015-8:27 PM</datetime>
        [Authorize(Roles = "Merchant")]
        public ActionResult Detail(string id)
        {
            ViewBag.Class = "collection";
            WSCollectionClient client = new WSCollectionClient();
            Collection collection = new Collection();
            collection = client.WSDetailCollection(id, VariableConfigController.GetBucket());
            collection = UpdateListProductOfCollection(collection, client);
            //client.WSUpdateCollection(collection, VariableConfigController.GetBucket());

            var model = Mapper.Map<Collection, CollectionViewModel>(collection);
            //  InitData(model);
            client.Close();
            InitData(model);
            model.arrayProduct = model.arrayProduct.OrderByDescending(p => p.createdDate).ToList();

            return View(model);
        }

        /// <summary>
        /// Updates the list product of collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:29 PM</datetime>
        private Collection UpdateListProductOfCollection(Collection collection, WSCollectionClient client)
        {
            bool isChange = false;
            WSProductItemClient clientProduct = new WSProductItemClient();

            var arrayProduct = new List<SmallProductItem>();
            if (collection.arrayProduct != null && collection.arrayProduct.Count() > 0)
            {
                foreach (var item in collection.arrayProduct)
                {
                    var product = clientProduct.WSDetailProductItem(item._id, VariableConfigController.GetBucket());
                    if (product != null)
                    {
                        product.arrayVarient = null; // Add to pass error entity too large
                        SmallProductItem smallProd = GetSmallProduct(product);

                        if (smallProd.name != item.name ||
                            smallProd.categoryId != item.categoryId ||
                            smallProd.categoryName != item.categoryName ||
                            smallProd.urlImageProduct != item.urlImageProduct)
                        {
                            isChange = true;
                        }
                        smallProd.createdDate = item.createdDate;
                        arrayProduct.Add(smallProd);
                        // lisproductItems.Add(smallProd);
                    }
                    else
                    {
                        isChange = true;
                    }
                }
            }
            clientProduct.Close();

            collection.arrayProduct = arrayProduct;
            if (isChange)
            {
                client.WSUpdateCollection(collection, VariableConfigController.GetBucket());
            }
            return collection;
        }

        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>
        /// "Dao.Nguyen"
        /// </author>
        /// <datetime>1/28/2015-8:27 PM</datetime>
        [Authorize(Roles = "Merchant")]
        public ActionResult Edit(string id)
        {
            string bucket = VariableConfigController.GetBucket();
            ViewBag.Class = "collection";

            WSCollectionClient client = new WSCollectionClient();
            Collection collection = client.WSDetailCollection(id, bucket);
            collection = UpdateListProductOfCollection(collection, client);
            //client.WSUpdateCollection(collection, bucket);
            client.Close();

            var model = Mapper.Map<Collection, CollectionViewModel>(collection);
            InitData(model);

            model.arrayProduct = model.arrayProduct.OrderByDescending(p => p.createdDate).ToList();

            return View(model);
            //return View(collection);
        }

        /// <summary>
        /// Edits the specified viewmodel.
        /// </summary>
        /// <param name="viewmodel">The viewmodel.</param>
        /// <param name="fc">The fc.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:29 PM</datetime>
        [HttpPost]
        public ActionResult Edit(CollectionViewModel viewmodel, FormCollection fc)
        {
            StringHelper.Trim(viewmodel);
            var isSuccess = 0;
            string productIDs = fc["product"];
            string[] ListProduct = !string.IsNullOrWhiteSpace(productIDs) ? productIDs.Split(',') : new string[0];
            List<SmallProductItem> lisproductItems = new List<SmallProductItem>();
            WSProductItemClient productItemClient = new WSProductItemClient();

            WSCollectionClient client = new WSCollectionClient();
            Collection collection = client.WSDetailCollection(viewmodel._id, VariableConfigController.GetBucket());
            if (collection != null)
            {
                for (int i = 0; i < ListProduct.Count(); i++)
                {
                    var product = productItemClient.WSDetailProductItem(ListProduct[i], VariableConfigController.GetBucket());
                    //lisproductItems.Add(product);
                    if (product != null)
                    {
                        product.arrayVarient = null; // Add to pass error entity too large
                        SmallProductItem smallProd = GetSmallProduct(product);
                        if (collection.arrayProduct != null && collection.arrayProduct.Where(p => p._id.Equals(smallProd._id)).Any())
                        {
                            smallProd.createdDate = collection.arrayProduct.Where(p => p._id.Equals(smallProd._id)).FirstOrDefault().createdDate;
                        }
                        lisproductItems.Add(smallProd);
                    }

                }

                viewmodel.arrayProduct = lisproductItems;
                viewmodel.image = fc["logoImg"];

                viewmodel.status = collection.status;
                //// Check button active or inactive?
                //bool isActive = true;
                //if (!string.IsNullOrWhiteSpace(fc["optionsRadios"]))
                //{
                //    isActive = Boolean.Parse(fc["optionsRadios"]);
                //}
                //viewmodel.status = isActive;
                //nerver expired
                bool isNeverExpires = false;
                Boolean.TryParse(fc["neverExpires"], out isNeverExpires);
                viewmodel.isNeverExpires = isNeverExpires;

                viewmodel.userID = User.Identity.GetUserId();
                viewmodel.modifiedDate = DateTime.UtcNow;

                var model = Mapper.Map<CollectionViewModel, Collection>(viewmodel);

                isSuccess = client.WSUpdateCollection(model, VariableConfigController.GetBucket());
            }
            productItemClient.Close();
            client.Close();

            // update Collection to productItem
            //AddCollectionToProductItem(viewmodel, ListProduct, ActionState.Edit);

            return isSuccess == 0 ? Json(new { success = true }, JsonRequestBehavior.AllowGet) : Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>
        /// "Dao.Nguyen"
        /// </author>
        /// <datetime>1/28/2015-8:27 PM</datetime>
        [Authorize(Roles = "Merchant")]
        public ActionResult Delete(string id)
        {

            ViewBag.Class = "collection";
            WSCollectionClient client = new WSCollectionClient();

            int statusCode = client.WSDeleteCollection(id, VariableConfigController.GetBucket());

            //Update Collection to productItem
            //deleteCollectioninProductItem(id);
            client.Close();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Updates the status collection.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <returns></returns>
        /// <author>
        /// "Dao.Nguyen"
        /// </author>
        /// <datetime>1/28/2015-8:27 PM</datetime>
        [Authorize(Roles = "Merchant")]
        [HttpPost]
        public int UpdateStatusCollection(string id, bool status)
        {
            string siteID = VariableConfigController.GetBucket();
            WSCollectionClient client = new WSCollectionClient();
            Collection collection = new Collection();
            collection = client.WSDetailCollection(id, siteID);
            collection.status = status;
            collection.modifiedDate = DateTime.UtcNow;
            int statusCode = client.WSUpdateCollection(collection, siteID);

            client.Close();
            return statusCode;
            //   return RedirectToAction("Index");
        }

        /// <summary>
        /// Products the by category.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="startKey">The start key.</param>
        /// <param name="nextKey">The next key.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="pageGoTo">The page go to.</param>
        /// <param name="pageCurrent">The page current.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="desc">if set to <c>true</c> [desc].</param>
        /// <param name="theme">The theme.</param>
        /// <returns></returns>
        /// <author>
        /// "Dao.Nguyen"
        /// </author>
        /// <datetime>1/29/2015-7:22 PM</datetime>
        public ActionResult ProductByCategory(string category, string startKey = null, string nextKey = null, int limit = 12, int pageGoTo = 1, int pageCurrent = 1, string sort = "_id", bool desc = false, string theme = "")
        {
            ViewBag.Class = "product";

            string siteID = VariableConfigController.GetBucket();
            WSOnlineClient clientOline = new WSOnlineClient();
            ProductByCategoryModel productItemByCategoryList = new ProductByCategoryModel();
            productItemByCategoryList.Category = category;
            productItemByCategoryList.Paging = new EntityPaging()
            {
                startKey = startKey,
                nextKey = nextKey,
                limit = limit,
                pageGoTo = pageGoTo,
                pageCurrent = pageGoTo,
                sort = sort,
                desc = desc
            };
            var listsAll = clientOline.WSGetAllActiveProductItemByCategory(ConstantSmoovs.CouchBase.DesignDocOnline, ConstantSmoovs.CouchBase.GetAllProductByCategoryWithDate, category, productItemByCategoryList.Paging, siteID);
            var allProductItemList = listsAll.Where(r => r.categoryName == category);
            clientOline.Close();
            //productItemByCategoryList.ProductItemList = allProductItemList.Take(limit);
            productItemByCategoryList.ProductItemList = allProductItemList;
            ViewBag.category = category;


            return View(productItemByCategoryList);
        }
    }
}