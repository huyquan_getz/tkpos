﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.WSDiscountReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSOnlineReference;
using SmoovPOS.Utility.WSProductItemReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CouchBaseDoc = SmoovPOS.Common.ConstantSmoovs.CouchBase;

namespace SmoovPOS.UI.Controllers
{
    public class DiscountController : BaseController
    {
        private string siteID = string.Empty;
        private string TimeZone = string.Empty;
        private string TimeZoneFullName = string.Empty;
        private string Currency = string.Empty;

        enum ActionState { Create, Edit, Delete }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            GetAllMerchantSetting();
        }

        private void GetAllMerchantSetting()
        {
            this.siteID = VariableConfigController.GetBucket();

            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "discount";

            BaseController baseController = new BaseController();
            var generalSetting = baseController.GetGeneralSetting();
            if (generalSetting != null)
            {
                this.TimeZone = generalSetting.TimeZone + " " + generalSetting.TimeZoneFullName;
                ViewBag.TimeZone = this.TimeZone;

                this.TimeZoneFullName = generalSetting.TimeZoneFullName;
                ViewBag.TimeZoneFullName = this.TimeZoneFullName;

                this.Currency = generalSetting.Currency;
                ViewBag.Currency = this.Currency;
            }
        }

        // GET: Discount
        [Authorize]
        public ActionResult Index()
        {
            DiscountListingModel discountListingModel = new DiscountListingModel();

            WSDiscountClient discountClient = new WSDiscountClient();
            var listDiscountAll = discountClient.WSGetAllDiscount(CouchBaseDoc.DesigndocDiscount, CouchBaseDoc.ViewAllDiscount, this.siteID);
            discountListingModel.AllDiscountList = listDiscountAll == null ? new List<Discount>() : listDiscountAll.ToList();
            discountClient.Close();

            //Get/Set the quantity of all current branches to session
            int count = discountListingModel.OpenDiscountListing.DiscountList.Count();
            Session[ConstantSmoovs.Discount.DiscountCount] = count;
            //Permission module
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            discountListingModel.isAddNew = userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Discount, count);
            ViewBag.NumberInit = CheckInitiation();
            return View(discountListingModel);
        }

        [Authorize]
        public ActionResult Create()
        {
            //Permission with create when limit Discount
            int count = 0;
            if (Session[ConstantSmoovs.Discount.DiscountCount] == null)
            {
                DiscountListingModel discountListingModel = new DiscountListingModel();

                WSDiscountClient discountClient = new WSDiscountClient();
                var listDiscountAll = discountClient.WSGetAllDiscount(CouchBaseDoc.DesigndocDiscount, CouchBaseDoc.ViewAllDiscount, this.siteID);
                discountListingModel.AllDiscountList = listDiscountAll == null ? new List<Discount>() : listDiscountAll.ToList();
                discountClient.Close();

                //Get/Set the quantity of all current branches to session
                count = discountListingModel.OpenDiscountListing.DiscountList.Count();
                Session[ConstantSmoovs.Discount.DiscountCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Discount.DiscountCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Discount, count))
            {
                return RedirectToAction("Index");
            }

            DiscountInformation discountInfo = new DiscountInformation();

            WSOnlineClient onlineClient = new WSOnlineClient();

            var listcategories = onlineClient.WSGetAllActiveCategory(CouchBaseDoc.DesignDocCategory, CouchBaseDoc.GetAllCategory, this.siteID);
            discountInfo.listCategories = listcategories.Count() > 0 ? listcategories.ToList() : new List<Category>();

            onlineClient.Close();

            WSDiscountClient discountClient1 = new WSDiscountClient();

            IEnumerable<Discount> listDiscount = discountClient1.WSGetAllDiscount(ConstantSmoovs.CouchBase.DesigndocDiscount, ConstantSmoovs.CouchBase.ViewAllDiscount, this.siteID);
            discountInfo.listDiscountProductItems = discountClient1.WSGetDiscountProducts(this.siteID, DateTime.Now.ToUniversalTime().ToString(), "", this.TimeZoneFullName).ToList();

            discountClient1.Close();

            WSProductItemClient productItemClient = new SmoovPOS.Utility.WSProductItemReference.WSProductItemClient();

            var listProductItems = productItemClient.WSGetAllProductItem(CouchBaseDoc.DesignDocProductItem, CouchBaseDoc.GetAllProductItem, this.siteID)
                .Where(r => r.index == 1 && r.status == true && discountInfo.listCategories.Select(c => c.id).Contains(r.categoryId));
            discountInfo.listProductItems = listProductItems.Count() > 0 ? listProductItems.ToList() : new List<ProductItem>();

            productItemClient.Close();

            return View(discountInfo);
        }

        [HttpPost]
        public ActionResult Create(FormCollection fc)
        {
            //Permission with create when limit Discount
            int count = 0;
            if (Session[ConstantSmoovs.Discount.DiscountCount] == null)
            {
                DiscountListingModel discountListingModel = new DiscountListingModel();

                WSDiscountClient discountClient = new WSDiscountClient();
                var listDiscountAll = discountClient.WSGetAllDiscount(CouchBaseDoc.DesigndocDiscount, CouchBaseDoc.ViewAllDiscount, this.siteID);
                discountListingModel.AllDiscountList = listDiscountAll == null ? new List<Discount>() : listDiscountAll.ToList();
                discountClient.Close();

                //Get/Set the quantity of all current branches to session
                count = discountListingModel.OpenDiscountListing.DiscountList.Count();
                Session[ConstantSmoovs.Discount.DiscountCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Discount.DiscountCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Discount, count))
            {
                return RedirectToAction("Index");
            }

            SaveDiscount(null, fc, ActionState.Create);

            //Update count
            count++;
            Session[ConstantSmoovs.Discount.DiscountCount] = count;

            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Detail(string id, bool isExpired)
        {
            DiscountInformation discountInfo = new DiscountInformation();

            WSDiscountClient discountClient = new WSDiscountClient();

            if (!string.IsNullOrWhiteSpace(id))
            {
                discountInfo.discount = discountClient.WSDetailDiscount(id, siteID);
            }

            discountClient.Close();

            discountInfo.isExpired = isExpired;
            discountInfo.currency = this.Currency;
            discountInfo.listSelectedProductItems = new List<ProductItem>();

            WSProductItemClient productItemClient = new WSProductItemClient();

            var listProductItems = productItemClient.WSGetAllProductItemByMasterNoLimit(CouchBaseDoc.DesignDocProductItem, CouchBaseDoc.GetAllProductItem, this.siteID).Where(x => x.status && x.store == ConstantSmoovs.Stores.Master).ToList();

            discountInfo.listProductItems = listProductItems.Count() > 0 ? listProductItems.ToList() : new List<ProductItem>();

            foreach (string productid in discountInfo.discount.ListProductItems)
            {
                ProductItem productItem = new ProductItem();
                productItem = productItemClient.WSDetailProductItem(productid, siteID);
                discountInfo.listSelectedProductItems.Add(productItem);
            }

            productItemClient.Close();

            return View(discountInfo);
        }

        public ActionResult Edit(string id)
        {
            DiscountInformation discountInfo = new DiscountInformation();
            discountInfo.listSelectedProductItems = new List<ProductItem>();

            WSOnlineClient onlineClient = new WSOnlineClient();

            var listcategories = onlineClient.WSGetAllActiveCategory(CouchBaseDoc.DesignDocCategory, CouchBaseDoc.GetAllCategory, this.siteID);
            discountInfo.listCategories = listcategories.Count() > 0 ? listcategories.ToList() : new List<Category>();

            onlineClient.Close();

            WSDiscountClient discountClient = new WSDiscountClient();

            IEnumerable<Discount> listDiscount = discountClient.WSGetAllDiscount(ConstantSmoovs.CouchBase.DesigndocDiscount, ConstantSmoovs.CouchBase.ViewAllDiscount, this.siteID);
            discountInfo.listDiscountProductItems = discountClient.WSGetDiscountProducts(this.siteID, DateTime.Now.ToUniversalTime().ToString(), id, this.TimeZoneFullName).Where(d => d != null).ToList();

            if (!string.IsNullOrWhiteSpace(id))
            {
                discountInfo.discount = discountClient.WSDetailDiscount(id, siteID);
            }

            discountClient.Close();

            WSProductItemClient productItemClient = new WSProductItemClient();

            var listProductItems = productItemClient.WSGetAllProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, CouchBaseDoc.GetAllProductItem, this.siteID)
                .Where(r => r.index == 1 && r.status == true && discountInfo.listCategories.Select(c => c.id).Contains(r.categoryId));
            discountInfo.listProductItems = listProductItems.Count() > 0 ? listProductItems.ToList() : new List<ProductItem>();

            if (discountInfo.discount == null)
            {
                discountInfo.discount = new Discount();
                discountInfo.discount.ListProductItems = discountInfo.discount.ListProductItems.ToList();
            }

            if (discountInfo.discount.ListProductItems != null)
            {
                for (int i = 0; i < discountInfo.discount.ListProductItems.Count(); i++)
                {
                    var product = productItemClient.WSDetailProductItem(discountInfo.discount.ListProductItems[i], this.siteID);
                    discountInfo.listSelectedProductItems.Add(product);
                }
            }

            productItemClient.Close();

            return View(discountInfo);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection fc)
        {
            SaveDiscount(fc["discount_id"], fc, ActionState.Edit);
            return RedirectToAction("Detail", new { id = fc["discount_id"], isExpired = true });
        }

        private void SaveDiscount(string discount_id, FormCollection fc, ActionState action)
        {
            try
            {
                Discount discount = new Discount();
                if (!string.IsNullOrEmpty(discount_id))
                {
                    discount._id = discount_id;
                }
                else
                {
                    discount._id = Guid.NewGuid().ToString();
                }

                discount.createdDate = DateTime.UtcNow;
                discount.display = true;
                var idMerchant = VariableConfigController.GetIDMerchant();
                discount.channels = new String[] { idMerchant + "_pos" };
                discount.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                discount.modifiedDate = DateTime.UtcNow;
                discount.name = !string.IsNullOrWhiteSpace(fc["discountName"]) ? fc["discountName"] : "";
                discount.type = Convert.ToInt32(fc["discountType"]);
                discount.discountValue = !string.IsNullOrWhiteSpace(fc["discountValue"]) ? Convert.ToDouble(fc["discountValue"]) : 0;
                discount.beginDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(fc["beginDate"]), TimeZoneInfo.FindSystemTimeZoneById(this.TimeZoneFullName));

                // Check checkbox never expire is checked?
                bool isNeverExpires = false;
                Boolean.TryParse(fc["neverExpires"], out isNeverExpires);
                discount.neverExpired = isNeverExpires;

                if (isNeverExpires)
                {
                    discount.endDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.MaxValue, TimeZoneInfo.FindSystemTimeZoneById(this.TimeZoneFullName));
                }
                else
                {
                    discount.endDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.Parse(fc["endDate"]), TimeZoneInfo.FindSystemTimeZoneById(this.TimeZoneFullName));
                }

                // Check begin - end date is expired?
                if (discount.endDate > DateTime.UtcNow && discount.beginDate <= DateTime.UtcNow)
                {
                    discount.status = true;
                }
                else
                {
                    discount.status = false;
                }

                string productIDs = fc["product"];
                string[] ListProduct = !string.IsNullOrWhiteSpace(productIDs) ? productIDs.Split(',') : new string[0];

                // -------------------Add Discount to ProducItem----------------
                //updateDiscountToProductItem(ListProduct, discount, action);

                WSDiscountClient discountClient = new WSDiscountClient();

                if (!string.IsNullOrEmpty(discount_id))
                {
                    discountClient.WSUpdateDiscount(discount, ListProduct, this.siteID);
                }
                else
                {
                    discountClient.WSCreateDiscount(discount, ListProduct, this.siteID);
                }

                discountClient.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void updateDiscountToProductItem(string[] ListProduct, Discount discount, ActionState action)
        {
            WSProductItemClient productItemClient = new WSProductItemClient();
            List<string> ListProductID = new List<string>(); //Use this list to detect edit productitem
            // Create + edit new discount
            if (action == ActionState.Create || action == ActionState.Edit)
            {
                for (int i = 0; i < ListProduct.Count(); i++)
                {
                    var productItemInfo = productItemClient.WSDetailProductItem(ListProduct[i], siteID);
                    var productItems = productItemClient.WSGetAllProductItemByProductID(CouchBaseDoc.DesignDocProductItem, CouchBaseDoc.GetAllByProductID, productItemInfo.productID, this.siteID);
                    var productItemList = productItems.Where(r => r.index != 0);

                    foreach (var productItem in productItemList)
                    {
                        //productItem.discount = discount.status & ListProduct.Intersect(productItemList.Select(p => p._id)).Any();
                        //DiscountProductItem discountProductItem = new DiscountProductItem();
                        //discountProductItem.beginDate = discount.beginDate;
                        //discountProductItem.endDate = discount.endDate;
                        //discountProductItem.discountLimit = discount.discountLimit;
                        //discountProductItem.discountValue = discount.discountValue;
                        //discountProductItem.name = discount.name;
                        //discountProductItem.discountID = discount._id;
                        //discountProductItem.type = discount.type;
                        //productItem.DiscountProductItem = discountProductItem != null ? discountProductItem : new DiscountProductItem();
                        productItem.discount = false;
                        productItem.DiscountProductItem = new DiscountProductItem();

                        productItemClient.WSUpdateProductItem(productItem, siteID);

                        ListProductID.Add(productItem.productID);
                    }
                }
            }

            if (action == ActionState.Edit)
            {
                var productItemAll = productItemClient.WSGetAllProductItem(CouchBaseDoc.DesignDocProductItem, CouchBaseDoc.GetAllProductItem, this.siteID);
                var productItemAllOB = productItemAll.Where(r => r.index != 0);
                var discountproductitem = productItemAllOB.Where(r => r.DiscountProductItem != null && r.DiscountProductItem.discountID == discount._id);

                foreach (var productItem in discountproductitem)
                {
                    if (!ListProductID.Contains(productItem.productID))
                    {
                        //productItem.discount = !ListProduct.Intersect(discountproductitem.Select(p => p._id)).Any();
                        //productItem.DiscountProductItem = null;
                        productItem.discount = false;
                        productItem.DiscountProductItem = new DiscountProductItem();

                        productItemClient.WSUpdateProductItem(productItem, siteID);
                    }
                }
            }

            productItemClient.Close();
        }

        public ActionResult SelectProduct(string[] selectProduct)
        {
            WSProductItemClient productItemClient = new WSProductItemClient();

            DiscountInformation discountInfo = new DiscountInformation();
            discountInfo.listSelectedProductItems = new List<ProductItem>();

            if (discountInfo.discount == null)
            {
                discountInfo.discount = new Discount();
                discountInfo.discount.ListProductItems = selectProduct.ToList();
            }

            if (selectProduct != null)
            {
                for (int i = 0; i < selectProduct.Count(); i++)
                {
                    var product = productItemClient.WSDetailProductItem(selectProduct[i], this.siteID);
                    if (discountInfo.discount.ListProductItems == null)
                    {
                        discountInfo.discount.ListProductItems = new List<string>();
                    }
                    if (!discountInfo.discount.ListProductItems.Contains(product._id))
                    {
                        discountInfo.discount.ListProductItems.Add(product._id);
                        discountInfo.listSelectedProductItems.Add(product);
                    }
                }
            }

            productItemClient.Close();

            return PartialView(discountInfo);
        }

        public ActionResult updateStatus(string id, bool status)
        {
            WSDiscountClient discountClient = new WSDiscountClient();

            discountClient.WSUpdateDiscountStatus(id, status, this.siteID);
            Discount discount = discountClient.WSDetailDiscount(id, this.siteID);

            discountClient.Close();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(string id)
        {
            WSDiscountClient discountClient = new WSDiscountClient();

            Discount discount = discountClient.WSDetailDiscount(id, this.siteID);
            discountClient.WSDeleteDiscount(discount._id, this.siteID);

            discountClient.Close();

            return RedirectToAction("Index");
        }
    }
}