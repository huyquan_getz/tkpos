﻿using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmoovPOS.Business.Business;
using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Utility.MailServer;
using SmoovPOS.UI.Models;
using AutoMapper;
using System.ServiceModel.Channels;
using System.Net.Security;
using SmoovPOS.Utility.ConfigMail;
using Com.SmoovPOS.Model;

namespace SmoovPOS.UI.Controllers
{

    public class EmailConfigurationController : Controller
    {
        //private Pop3Client pop3Client;
        //private Dictionary<int, Message> messages;
        // GET: EmailConfiguration
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-11:23 AM</datetime>

        [Authorize(Roles = "Merchant")]
        public ActionResult Index()
        {
            ViewBag.Class = "emailConfig";
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            MailServerModel mail = mailConfig.GetConfigMailServer(null);

            var merchantInfor = GetMerchant();
            MailServerModel mailMerchant = new MailServerModel();
            if (merchantInfor != null && !string.IsNullOrWhiteSpace(merchantInfor.merchantID.ToString()))
            {
                mailMerchant = mailConfig.GetConfigMailServer(merchantInfor.merchantID);
            }


            if (mail != null && mailMerchant != null)
            {
                var model = Mapper.Map<MailServerModel, EmailConfigurationViewModel>(mailMerchant);
                model.PassWord = !string.IsNullOrWhiteSpace(model.PassWord) ? model.PassWord : "";
                if(mailMerchant!=null && mailMerchant.isApply)
                {
                    model.isApplyMail = mailMerchant.isApply;
                }
                else
                {
                    model.isApplyMail = false;
                }
             //   model.defaultEmail = mail.UserName;
                model.defaultEmail = "noreply@smoovpos.com";
                return View(model);
            }
            else
            {
                EmailConfigurationViewModel modelView = new EmailConfigurationViewModel();
                modelView.PassWord = "";
                //   model.defaultEmail = mail.UserName;
                modelView.defaultEmail = "noreply@smoovpos.com";
                modelView.isApplyMail = false;
                return View(modelView);

            }
            
        }

        /// <summary>
        /// Gets the configuration of merchant.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-11:53 AM</datetime>
        public ActionResult GetConfigOfMerchant()
        {
            ViewBag.Class = "emailConfig";
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            MailServerModel mailDefault = mailConfig.GetConfigMailServer(null);
            var merchantInfor = GetMerchant();
            if (merchantInfor != null)
            {
                MailServerModel mail = mailConfig.GetConfigMailServer(merchantInfor.merchantID);
                if(mail!=null)
                {
                    var model = Mapper.Map<MailServerModel, EmailConfigurationViewModel>(mail);
                    model.PassWord = !string.IsNullOrWhiteSpace(model.PassWord) ? model.PassWord : "";
                    //   model.defaultEmail = mail.UserName;
                    model.defaultEmail = "noreply@smoovpos.com";
                    if (model != null && mail.isApply)
                    {
                        model.isApplyMail = mail.isApply;
                    }
                    else
                    {
                        model.isApplyMail = false;
                    }
                    return View("~/Views/EmailConfiguration/Private.cshtml", model);
                }
                else
                {
                    EmailConfigurationViewModel modelView = new EmailConfigurationViewModel();
                    modelView.PassWord =  "";
                    //   model.defaultEmail = mail.UserName;
                    modelView.defaultEmail = "noreply@smoovpos.com";
                    modelView.isApplyMail = false;
                    // return View(modelView);
                    return PartialView("~/Views/EmailConfiguration/Private.cshtml", modelView);
                }
            }
            else
            {
                EmailConfigurationViewModel modelView = new EmailConfigurationViewModel();
                modelView.PassWord = "";
                //   model.defaultEmail = mail.UserName;
                modelView.defaultEmail = "noreply@smoovpos.com";
                modelView.isApplyMail = false;
               // return View(modelView);
                return PartialView("~/Views/EmailConfiguration/Private.cshtml", modelView);
            }
           // var modalView = new EmailConfigurationController();
           
        }

        /// <summary>
        /// Saves the configuration private.
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-11:23 AM</datetime>
        public ActionResult SaveConfigPrivate(FormCollection fc)
        {
            ViewBag.Class = "emailConfig";

            try
            {
                // TODO: Add update logic here
                MailConfigBusiness mailConfig = new MailConfigBusiness();
                MailServerModel mail = new MailServerModel();
                string id = fc["Id"];
                if (string.IsNullOrWhiteSpace(id))
                {
                    mail.Id = Guid.NewGuid().ToString();

                }
                mail.IncomingPop = fc["IncomingPop"];
              //  string popPort = fc["IncomingPort"].ToString();
                mail.IncomingPort = Int32.Parse(fc["IncomingPort"].ToString());
                mail.UserName = fc["UserName"];
                mail.PassWord = fc["PassWord"];
                mail.OutgoingSmtp = fc["OutgoingSmtp"];
                mail.OutgoingPort = Int32.Parse(fc["OutgoingPort"].ToString());
                mail.Credentials = false;
                mail.EnableSsl = true;
                mail.CreateDate = DateTime.UtcNow;
                var merchantInfor = GetMerchant();
                Guid idMerchant = merchantInfor.merchantID != null ? merchantInfor.merchantID : new Guid();
                mail.merchantID = idMerchant;
                mail.ModifiedDate = DateTime.UtcNow;

                mailConfig.AddOrUpdateMailServer(id, mail, idMerchant);
                // return RedirectToAction("Index");
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                // return View("Index");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }


        }


        /// <summary>
        /// Gets the merchant.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-11:34 AM</datetime>
        public MerchantManagementModel GetMerchant()
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            string siteID = VariableConfigController.GetBusinessName();
            var model = client.WSGetMerchantManagementByBusinessName(siteID);
            client.Close();
            return model;
        }

        /// <summary>
        /// Tests the send email.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-1:24 PM</datetime>
        public bool TestSendEmail(string emailReceive)
        {
            int type = ConstantSmoovs.ConfigMail.Merchant;
            string merchantName = "";

            string titleEmail = "Test Config Email";
            string body = "This is a test e-mail sent from SmoovPos.";
            MailServerUtil sendMail = new MailServerUtil();
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            var merchantInfor = GetMerchant();
            merchantName = merchantInfor != null ? merchantInfor.businessName : "";
            bool isSend = false;
            if (merchantInfor != null)
            {
                isSend = !string.IsNullOrWhiteSpace(emailReceive) ? sendMail.SendMail(type, merchantName, emailReceive, titleEmail, body, merchantInfor.merchantID) : false;
                // bool isSend = !string.IsNullOrWhiteSpace(emailReceive) ? sendMail.SendMail(type, merchantName, "zinhumgato@gmail.com", titleEmail, body, merchantInfor.merchantID) : false;
                return isSend;
            }
            //if (isSend == false)
            //{
            //    isSend = sendMail.SendMail(type, merchantName, emailReceive, titleEmail, body);
            //    return false;
            //}
            else
            {
                return false;
            }


        }

        /// <summary>
        /// Gets the bussiness model.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-2:45 PM</datetime>
        public MerchantManagementModel GetBussinessModel()
        {

            IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
            var modelMerchant = clientMerchant.WSGetMerchantManagementByUserId(VariableConfigController.GetUserID());
            clientMerchant.Close();
            return modelMerchant;

        }

        /// <summary>
        /// Validations the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-3:13 PM</datetime>
        [HttpPost]
        public Boolean ValidationEmail(string email)
        {

            RegexUtilites regex = new RegexUtilites();
            Boolean isValid = regex.IsValidEmail(email);
            return isValid;
        }

        /// <summary>
        /// Tests the configuration server SMTP.
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="portConfig">The port configuration.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/4/2015-11:54 AM</datetime>
        [HttpPost]
        public bool TestConfigServerSMTP(string domain, int portConfig)
        {
            MailServerUtil sendMail = new MailServerUtil();
            bool isSuccess = false;
            sendMail.TestConfigServerSMTP(domain, portConfig);
            return isSuccess;
        }

        /// <summary>
        /// Tests the connection.
        /// </summary>
        /// <param name="smtpServerAddress">The SMTP server address.</param>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/4/2015-11:54 AM</datetime>
        [HttpPost]
        public bool TestConnection(string hostName, int port)
        {
            MailServerUtil mailUtil = new MailServerUtil();
            bool isSuccess = mailUtil.TestConnection(hostName, port);
            return isSuccess;
        }


        /// <summary>
        /// Valids the SMTP.
        /// </summary>
        /// <param name="hostName">Name of the host.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/4/2015-4:52 PM</datetime>
        public bool ValidSMTP(string hostName, int port)
        {
            MailServerUtil mailUtil = new MailServerUtil();
            //TimeSpan timeSpan = new TimeSpan(0, 0, 0, 60, 0);
            //bool isSuccess = mailUtil.ValidSMTP(hostName, port, timeSpan);
            bool isSuccess = mailUtil.ValidSMTP(hostName, port);
            return isSuccess;
        }


        /// <summary>
        /// Valids the pop.
        /// </summary>
        /// <param name="hostName">Name of the host.</param>
        /// <param name="port">The port.</param>
        /// <param name="user">The user.</param>
        /// <param name="pass">The pass.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/6/2015-3:12 PM</datetime>
        public bool ValidPOP(string hostName, int port, string user, string pass)
        {

            MailServerUtil mailUtil = new MailServerUtil();
            bool isValid = mailUtil.ValidPOP(hostName, port, user, pass);
            //TimeSpan timeSpan = new TimeSpan(0, 0, 0,60, 0);
            //bool isSuccess = mailUtil.ValidPOP(hostName, port, timeSpan);
            //return isSuccess;
            return isValid;
            //Pop3MailClient DemoClient = new Pop3MailClient(hostName, port, true, user, pass);
            //try
            //{
            //    //prepare pop client            

            //    DemoClient.IsAutoReconnect = true;
            //    //remove the following line if no tracing is needed
            //    DemoClient.Trace += new SmoovPOS.Utility.ConfigMail.TraceHandler(Console.WriteLine);
            //    DemoClient.ReadTimeout = 60000; //give pop server 60 seconds to answer
            //    //establish connection
            //    DemoClient.Connect();
            //    DemoClient.Disconnect();
            //    return true;
            //}
            //catch
            //{
            //    DemoClient.Disconnect();
            //    return false;
            //}


        }

        /// <summary>
        /// Applies the private.
        /// </summary>
        /// <param name="indexChoose">The index choose.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/6/2015-3:12 PM</datetime>
        public bool ApplyPrivate(int indexChoose)
        {
            ViewBag.Class = "emailConfig";

            try
            {
                // TODO: Add update logic here
                MailConfigBusiness mailConfig = new MailConfigBusiness();
                var merchantInfor = GetMerchant();
                if (merchantInfor != null && !string.IsNullOrWhiteSpace(merchantInfor.merchantID.ToString()))
                {

                    if (indexChoose == 1)
                    {
                        mailConfig.ApplyPrivateMailServer(merchantInfor.merchantID, true);
                    }
                    else
                    {
                        mailConfig.ApplyPrivateMailServer(merchantInfor.merchantID, false);
                    }
                    return true;
                }
                else
                {
                    return false;
                }



                // return RedirectToAction("Index");

            }
            catch
            {
                return false;
            }

        }
    }
}