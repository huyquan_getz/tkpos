﻿using AutoMapper;
using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Entity.Settings;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSTableOrderingSettingReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CouchBaseCommon = SmoovPOS.Common.ConstantSmoovs.CouchBase;

namespace SmoovPOS.UI.Controllers
{
    public class TableOrderingController : Controller
    {
        string SiteID = "";

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.SiteID = VariableConfigController.GetBucket();
        }

        // GET: TableOrder
        public ActionResult Index(string id)
        {
            ViewBag.Class = "tableordering";
            string branchId = id;
            TableOrderingViewModel tableOrderingViewModel = new TableOrderingViewModel();
            tableOrderingViewModel.ListInventory = new System.Collections.Generic.List<Inventory>();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            var inventorys = inventoryClient.WSGetAll(CouchBaseCommon.DesigndocInventory, CouchBaseCommon.GetAllInventories, this.SiteID).Where(i => i.status == true && i.display == true);

            foreach (var inventory in inventorys)
            {
                if (inventory.index != Common.ConstantSmoovs.Stores.OnlineStore && inventory.businessID != Common.ConstantSmoovs.Stores.Master)
                {
                    tableOrderingViewModel.ListInventory.Add(inventory);
                }
            }

            tableOrderingViewModel.SelectedBranchId = branchId;

            inventoryClient.Close();

            var servicePacket = new SmoovPOS.Business.Components.ServicePackageComponent();
            ServicePackageModel servicePackage = servicePacket.GetServicePacket(SiteID);
            var functionServicePacket = servicePackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.TABLEORDERING);
            tableOrderingViewModel.isAddNew = functionServicePacket != null && functionServicePacket.limit >= 1;

            return View(tableOrderingViewModel);
        }

        private TableOrderingSetting InitFirstSetting(string branchId,int branchIndex)
        {
            TableOrderingSetting setting = new TableOrderingSetting();
            setting._id = Guid.NewGuid().ToString();
            setting.AcceptTableOrdering = false;
            setting.AllowAutoAcceptOrder = false;
            setting.AllowPushNotification = false;
            setting.AllowSendEmail = false;
            setting.AllowSendSMS = false;
            setting.AllowSound = false;
            setting.BranchId = branchId;
            setting.BranchIndex = branchIndex;
            var idMerchant = VariableConfigController.GetIDMerchant();
            setting.channels = new String[] { idMerchant + "_" + branchIndex.ToString() + "_table" };
            setting.display = true;
            setting.Instructions = string.Empty;
            setting.status = true;
            
            setting.ListDayOfWeekSetting = new List<SmoovPOS.Entity.Entity.Settings.DayOfWeekSetting>();
            for (int i = 0; i < 7; i++)
            {
                setting.ListDayOfWeekSetting.Add(new SmoovPOS.Entity.Entity.Settings.DayOfWeekSetting() { DayOfWeek = i, Option = 2 });
            }

            WSTableOrderingSettingClient tableOSClient = new WSTableOrderingSettingClient();
            tableOSClient.WSCreateTableOrderingSetting(setting, this.SiteID);
            setting = tableOSClient.WSDetailTableOrderingSetting(branchId, this.SiteID);
            return setting;
        }

        public ActionResult Detail(string id)
        {
            ViewBag.Class = "tableordering";
            string branchId = id;

            TableOrderingViewModel tableOrderingViewModel = new TableOrderingViewModel();
            Inventory inventory = new Inventory();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            inventory = inventoryClient.WSDetailInventory(branchId, this.SiteID);

            inventoryClient.Close();

            WSTableOrderingSettingClient tableOSClient = new WSTableOrderingSettingClient();

            TableOrderingSetting tableOrderingSetting = tableOSClient.WSDetailTableOrderingSettingByBranchId(CouchBaseCommon.DesigndocTableOrdering, CouchBaseCommon.ViewNameGetAllTableOrderingByBranch, branchId, this.SiteID);
            tableOrderingViewModel.SelectedBranchId = branchId;

            if (tableOrderingSetting == null)
            {
                tableOrderingViewModel = Mapper.Map<TableOrderingSetting, TableOrderingViewModel>(InitFirstSetting(branchId, inventory.index));
            }
            else
            {
                tableOrderingViewModel = Mapper.Map<TableOrderingSetting, TableOrderingViewModel>(tableOrderingSetting);
            }


            tableOSClient.Close();

            bool isEdit = HttpContext.Request.UrlReferrer.AbsoluteUri.IndexOf("Edit") > -1;
            if (isEdit)
            {
                ViewBag.IsDisabled = false;
            }
            else
            {
                ViewBag.IsDisabled = true;
            }

            return PartialView("Detail", tableOrderingViewModel);
        }

        public ActionResult Edit(string id)
        {
            var servicePacket = new SmoovPOS.Business.Components.ServicePackageComponent();
            ServicePackageModel servicePackage = servicePacket.GetServicePacket(SiteID);
            var functionServicePacket = servicePackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.TABLEORDERING);
            if (!(functionServicePacket != null && functionServicePacket.limit >= 1))
            {
                return RedirectToAction("Index");
            }

            ViewBag.Class = "tableordering";
            string branchId = id;
            TableOrderingViewModel tableOrderingViewModel = new TableOrderingViewModel();
            
            WSTableOrderingSettingClient tableOSClient = new WSTableOrderingSettingClient();
            TableOrderingSetting tableOrderingSetting = tableOSClient.WSDetailTableOrderingSettingByBranchId(CouchBaseCommon.DesigndocTableOrdering, CouchBaseCommon.ViewNameGetAllTableOrderingByBranch, branchId, this.SiteID);
            tableOSClient.Close();

            tableOrderingViewModel = Mapper.Map<TableOrderingSetting, TableOrderingViewModel>(tableOrderingSetting);
            
            tableOrderingViewModel.ListInventory = new System.Collections.Generic.List<Inventory>();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            var inventory = inventoryClient.WSDetailInventory(branchId, this.SiteID);

            inventoryClient.Close();
            
            tableOrderingViewModel.ListInventory.Add(inventory);
            

            tableOrderingViewModel.SelectedBranchId = branchId;

            

            return View(tableOrderingViewModel);
        }

        [HttpPost]
        public ActionResult Edit(TableOrderingViewModel tableOrderingViewModel)
        {
            var servicePacket = new SmoovPOS.Business.Components.ServicePackageComponent();
            ServicePackageModel servicePackage = servicePacket.GetServicePacket(SiteID);
            var functionServicePacket = servicePackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.TABLEORDERING);
            if (!(functionServicePacket != null && functionServicePacket.limit >= 1))
            {
                return RedirectToAction("Index");
            }

            string branchId = tableOrderingViewModel.SelectedBranchId;

            bool isCreateNew = false;

            tableOrderingViewModel.ListInventory = new System.Collections.Generic.List<Inventory>();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            var inventory = inventoryClient.WSDetailInventory(branchId, this.SiteID);

            tableOrderingViewModel.ListInventory.Add(inventory);

            inventoryClient.Close();

            if (ModelState.IsValid)
            {
                TableOrderingSetting tableOrderingSetting = new TableOrderingSetting();

                WSTableOrderingSettingClient tableOSClient = new WSTableOrderingSettingClient();

                tableOrderingSetting = tableOSClient.WSDetailTableOrderingSettingByBranchId(CouchBaseCommon.DesigndocTableOrdering, CouchBaseCommon.ViewNameGetAllTableOrderingByBranch, branchId, this.SiteID);

                if (tableOrderingSetting == null)
                {
                    isCreateNew = true;
                    tableOrderingSetting = new TableOrderingSetting();
                    tableOrderingSetting._id = Guid.NewGuid().ToString();
                    tableOrderingSetting.BranchId = tableOrderingViewModel.SelectedBranchId;
                }
                tableOrderingSetting.BranchIndex = inventory.index;
                tableOrderingSetting.Instructions = tableOrderingViewModel.Instructions;
                tableOrderingSetting.AcceptTableOrdering = tableOrderingViewModel.AcceptTableOrdering;

                tableOrderingSetting.AllowAutoAcceptOrder = tableOrderingViewModel.AllowAutoAcceptOrder;

                tableOrderingSetting.AllowPushNotification = tableOrderingViewModel.AllowPushNotification;
                tableOrderingSetting.AllowSound = tableOrderingViewModel.AllowSound;

                tableOrderingSetting.AllowSendEmail = tableOrderingViewModel.AllowSendEmail;
                tableOrderingSetting.AllowSendSMS = tableOrderingViewModel.AllowSendSMS;

                if (tableOrderingSetting.ListDayOfWeekSetting == null) tableOrderingSetting.ListDayOfWeekSetting = new List<DayOfWeekSetting>();

                tableOrderingSetting.ListDayOfWeekSetting = new List<DayOfWeekSetting>();

                foreach (DayOfWeekSetting dws in tableOrderingViewModel.ListDayOfWeekSetting)
                {
                    dws.DayOfWeek = tableOrderingViewModel.ListDayOfWeekSetting.IndexOf(dws);
                    if (dws.Option == 0)
                    {
                        dws.Split_OpeningStart = string.Empty;
                        dws.Split_OpeningEnd = string.Empty;
                        dws.Split_ClosingStart = string.Empty;
                        dws.Split_ClosingEnd = string.Empty;
                    }

                    if (dws.Option == 1)
                    {
                        dws.Open_Opening = string.Empty;
                        dws.Open_Closing = string.Empty;
                    }

                    if (dws.Option == 2)
                    {
                        dws.Split_OpeningStart = string.Empty;
                        dws.Split_OpeningEnd = string.Empty;
                        dws.Split_ClosingStart = string.Empty;
                        dws.Split_ClosingEnd = string.Empty;
                        dws.Open_Opening = string.Empty;
                        dws.Open_Closing = string.Empty;
                    }

                    tableOrderingSetting.ListDayOfWeekSetting.Add(dws);
                }

                if (isCreateNew)
                {
                    tableOSClient.WSCreateTableOrderingSetting(tableOrderingSetting, this.SiteID);
                }
                else
                {
                    tableOSClient.WSUpdateTableOrderingSetting(tableOrderingSetting, this.SiteID);
                }

                tableOSClient.Close();
            }
            else
            {
                string errorMsg  = ModelState.Values.SelectMany(e=>e.Errors).Select(gh=>gh.ErrorMessage).FirstOrDefault().ToString();
                ModelState.AddModelError("DayOfWeekSetting", errorMsg);//"Invalid username or password."
                return View(tableOrderingViewModel);
            }
            return RedirectToAction("Index", new { id = tableOrderingViewModel.SelectedBranchId });
        }
    }
}