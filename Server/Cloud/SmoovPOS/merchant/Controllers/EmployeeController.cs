﻿using Com.SmoovPOS.Common.Controllers;
using Com.SmoovPOS.Entity;
using Com.SmoovPOS.UI.Models;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.WSEmployeeReference;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using SmoovPOS.Utility.WSRoleReference;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmoovPOS.Utility.WSMerchantManagementReference;
using System.Threading.Tasks;
using SmoovPOS.Utility.WSOrderReference;
using SmoovPOS.UI.Models.Employee;
using SmoovPOS.Utility.WSStationReference;
using SmoovPOS.Utility.WSCustomerReference;

namespace SmoovPOS.UI.Controllers
{
    public class EmployeeController : BaseController
    {
        public EmployeeController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="MerchantManagementController"/> class.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        public EmployeeController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        // private ApplicationUserManager _userManager;

        /// <summary>
        /// Gets the user manager.
        /// </summary>
        /// <value>
        /// The user manager.
        /// </value>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        public UserManager<ApplicationUser> UserManager { get; private set; }


        // GET: Employee
        [Authorize(Roles = "Merchant")]
        public ActionResult Index()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "employee";

            WSEmployeeClient client = new WSEmployeeClient();
            var listEmployee = client.WSGetAllEmployee(ConstantSmoovs.CouchBase.DesignDocEmployee, ConstantSmoovs.CouchBase.EmployeeViewNameAll, VariableConfigController.GetBucket());
            client.Close();

            EmployeeViewModel model = new EmployeeViewModel();
            model.listEmployees = listEmployee;

            //Get/Set the quantity of all employees to session
            int count = listEmployee.Count() - 1; //Don't count the merchant
            Session[ConstantSmoovs.Employee.EmployeeCount] = count;
            //Permission module
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            model.isAddNew = userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Employee, count);

            return View(model);
        }

        [Authorize(Roles = "Merchant")]
        public ActionResult Create()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "employee";

            //Permission with create when limit Employee
            int count = 0;
            if (Session[ConstantSmoovs.Employee.EmployeeCount] == null)
            {
                WSEmployeeClient client = new WSEmployeeClient();
                var listEmployee = client.WSGetAllEmployee(ConstantSmoovs.CouchBase.DesignDocEmployee, ConstantSmoovs.CouchBase.EmployeeViewNameAll, VariableConfigController.GetBucket());
                client.Close();

                //Get/Set the quantity of all employees to session
                count = listEmployee.Count() - 1; //Don't count the merchant
                Session[ConstantSmoovs.Employee.EmployeeCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Employee.EmployeeCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Employee, count))
            {
                return RedirectToAction("Index");
            }

            InfoNewEmployee infoNewEmployee = new InfoNewEmployee();
            WSRoleClient clientRole = new WSRoleClient();
            WSInventoryClient clientInventory = new WSInventoryClient();
            Country[] countrys = clientInventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, VariableConfigController.GetBucket());
            infoNewEmployee.arrCountry = countrys[0];
            Inventory[] ListInventory = clientInventory.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket());
            infoNewEmployee.inventoryList = ListInventory;
            Role[] roleList = clientRole.WSGetAllRole(ConstantSmoovs.CouchBase.DesigndocRole, ConstantSmoovs.CouchBase.GetAllRole, VariableConfigController.GetBucket());
            infoNewEmployee.roleList = roleList;
            //infoNewEmployee.arrPermission = clientInventory.WSGetAll("inventory", "get_all_inventory");
            //infoNewEmployee.arrPermission = clientRole.WSGetAllRole("role", "get_all_role");

            infoNewEmployee.listCountries = new List<SelectListItem>();
            var resultCountry = clientInventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, VariableConfigController.GetBucket()).Select(r => r.arrayCountry).ToList();
            foreach (string country in resultCountry[0])
            {
                infoNewEmployee.listCountries.Add(new SelectListItem() { Text = country, Value = country });
            }
            clientInventory.Close();
            infoNewEmployee.listMale = new List<SelectListItem>();
            infoNewEmployee.listMale.Add(new SelectListItem() { Text = "Male", Value = "0" });
            infoNewEmployee.listMale.Add(new SelectListItem() { Text = "Female", Value = "1" });
            infoNewEmployee.listMale.Add(new SelectListItem() { Text = "Others", Value = "2" });

            clientRole.Close();
            return View(infoNewEmployee);
        }

        [HttpPost]
        public Boolean ValidationEmail(string email)
        {
            RegexUtilites regex = new RegexUtilites();
            Boolean isValid = regex.IsValidEmail(email);
            return isValid;
        }

        [HttpPost]
        public async Task<ActionResult> Create(FormCollection collection)
        {
            //Permission with create when limit Employee
            int count = 0;
            if (Session[ConstantSmoovs.Employee.EmployeeCount] == null)
            {
                WSEmployeeClient client1 = new WSEmployeeClient();
                var listEmployee = client1.WSGetAllEmployee(ConstantSmoovs.CouchBase.DesignDocEmployee, ConstantSmoovs.CouchBase.EmployeeViewNameAll, VariableConfigController.GetBucket());
                client1.Close();

                //Get/Set the quantity of all employees to session
                count = listEmployee.Count() - 1; //Don't count the merchant
                Session[ConstantSmoovs.Employee.EmployeeCount] = count;
            }
            count = Int32.Parse(Session[ConstantSmoovs.Employee.EmployeeCount].ToString());
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (!userSession.CheckPermissionLimitAddNew(ConstantSmoovs.FunctionMethod.Employee, count))
            {
                return RedirectToAction("Index");
            }

            WSEmployeeClient client = new WSEmployeeClient();
            WSInventoryClient clientInventory = new WSInventoryClient();
            WSRoleClient clientRole = new WSRoleClient();
            Employee employee = new Employee();
            employee._id = Guid.NewGuid().ToString();
            employee.createdDate = DateTime.Today;
            employee.modifiedDate = DateTime.Today;

            employee.userID = User.Identity.GetUserId();
            employee.userOwner = employee.userID;
            if (string.IsNullOrEmpty(employee.userOwner))
            {
                employee.userOwner = VariableConfigController.GetUserID();
            }
            employee.siteId = VariableConfigController.GetBucket();

            employee.display = true;
            employee.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            employee.arrPermission = new List<Permission>();
            var idMerchant = VariableConfigController.GetIDMerchant();
            employee.channels = new String[] { idMerchant };
            string listStore = collection["listStore"];
            string listRole = collection["listRole"];
            if (!string.IsNullOrWhiteSpace(collection["listStore"]))
            {
                string[] arrStore = listStore.Split(',');
                string[] arrRole = listRole.Split(',');
                for (int i = 0; i < arrStore.Count(); i++)
                {
                    Permission permission = new Permission();
                    permission.role = clientRole.WSDetailRole(arrRole[i], VariableConfigController.GetBucket());
                    permission.store = clientInventory.WSDetailInventory(arrStore[i], VariableConfigController.GetBucket());
                    employee.arrPermission.Add(permission);
                }
            }

            try
            {
                bool isActive = true;
                if (!string.IsNullOrWhiteSpace(collection["optionsRadios"]))
                {
                    isActive = Boolean.Parse(collection["optionsRadios"]);
                }
                employee.status = isActive;
            }
            catch (Exception)
            {
                employee.status = false;
            }
            if (!string.IsNullOrWhiteSpace(collection["firstName"]))
            {
                employee.firstName = collection["firstName"];
            }
            else
            {
                employee.firstName = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["lastName"]))
            {
                employee.lastName = collection["lastName"];
            }
            else
            {
                employee.lastName = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["gender"]))
            {
                employee.gender = Convert.ToInt32(collection["gender"]);
            }
            else
            {
                employee.gender = 2;
            }
            if (!string.IsNullOrWhiteSpace(collection["birthday"]))
            {
                employee.birthday = collection["birthday"];
            }
            else
            {
                employee.birthday = DateTime.Today.ToString();
            }
            if (!string.IsNullOrWhiteSpace(collection["phonenumber"]))
            {
                employee.phone = collection["phonenumber"];
            }
            else
            {
                employee.phone = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["hiredate"]))
            {
                employee.hiredDate = collection["hiredate"];
            }
            else
            {
                employee.hiredDate = DateTime.Today.ToString();
            }
            if (!string.IsNullOrWhiteSpace(collection["address"]))
            {
                employee.address = collection["address"];
            }
            else
            {
                employee.address = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["city"]))
            {
                employee.city = collection["city"];
            }
            else
            {
                employee.city = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["zipcode"]))
            {
                employee.zipCode = collection["zipcode"];
            }
            else
            {
                employee.zipCode = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["state"]))
            {
                employee.state = collection["state"];
            }
            else
            {
                employee.state = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["country"]))
            {
                employee.country = collection["country"];
            }
            else
            {
                employee.country = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["email"]))
            {
                employee.email = collection["email"];
            }
            else
            {
                employee.email = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["pincode"]))
            {
                employee.pinCode = collection["pincode"];
            }
            else
            {
                employee.pinCode = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["password"]))
            {
                employee.password = VariableConfigController.MD5Hash(collection["password"]);
            }
            if (!string.IsNullOrWhiteSpace(collection["passwordConfirm"]))
            {
                employee.passwordConfirm = VariableConfigController.MD5Hash(collection["passwordConfirm"]);
            }

            employee.fullName = employee.firstName + " " + employee.lastName;
            employee.AddressCombine = employee.address + ", " + employee.city + " " + employee.zipCode + ", " + employee.state + ", " + employee.country;
            employee.image = collection["logoImg"];
            try
            {
                int employeeId = client.WSGetNextEmployeeId(ConstantSmoovs.CouchBase.DesignDocEmployee, ConstantSmoovs.CouchBase.EmployeeViewNameAll, VariableConfigController.GetBucket());
                employee.EmployeeId = employeeId;

                #region create asp user
                IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
                var modelMerchant = clientMerchant.WSGetMerchantManagementBySiteId(VariableConfigController.GetBucket());

                string userName = employee.email;
                if (modelMerchant != null)
                {
                    employee.userOwner = modelMerchant.merchantID.ToString();
                    userName = string.Format("{0}_{1}", modelMerchant.bucket.Replace("-", ""), employee.email);
                }
                ApplicationDbContext _db = new ApplicationDbContext();
                var user = new ApplicationUser() { UserName = userName, Email = employee.email };

                var resultStatus = await UserManager.CreateAsync(user, collection["password"].ToString());

                if (resultStatus.Succeeded)
                {
                    //assign user to role merchant
                    ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));

                    _db.AddUserToRole(userManager, user.Id, ConstantSmoovs.RoleNames.UserMerchant);

                    employee._id = user.Id;
                }
                #endregion

                int result = client.WSCreateEmployee(employee, VariableConfigController.GetBucket());
                client.Close();
                clientInventory.Close();
                clientRole.Close();
            }
            catch (Exception) { }

            //Update count
            count++;
            Session[ConstantSmoovs.Employee.EmployeeCount] = count;

            return RedirectToAction("Index");
        }
        [Authorize(Roles = "Merchant")]
        public ActionResult Edit(string id)
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "employee";
            WSEmployeeClient client = new WSEmployeeClient();
            InfoNewEmployee infoNewEmployee = new InfoNewEmployee();
            WSRoleClient clientRole = new WSRoleClient();
            WSInventoryClient clientInventory = new WSInventoryClient();
            Country[] countrys = clientInventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, VariableConfigController.GetBucket());
            infoNewEmployee.arrCountry = countrys[0];
            infoNewEmployee.Employee = client.WSDetailEmployee(id, VariableConfigController.GetBucket());
            Inventory[] ListInventory = clientInventory.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket());
            infoNewEmployee.inventoryList = ListInventory;
            Role[] roleList = clientRole.WSGetAllRole(ConstantSmoovs.CouchBase.DesigndocRole, ConstantSmoovs.CouchBase.GetAllRole, VariableConfigController.GetBucket());
            infoNewEmployee.roleList = roleList;
            clientInventory.Close();
            clientRole.Close();
            client.Close();
            return View(infoNewEmployee);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, string id)
        {
            WSEmployeeClient client = new WSEmployeeClient();
            Employee employee = client.WSDetailEmployee(id, VariableConfigController.GetBucket());
            WSInventoryClient clientInventory = new WSInventoryClient();
            WSRoleClient clientRole = new WSRoleClient();
            string listStore = collection["listStore"];
            string listRole = collection["listRole"];

            if (!employee.arrPermission.Where(r => r.isMerchant).Any())
            {
                employee.arrPermission.Clear();
                if (!string.IsNullOrWhiteSpace(collection["listStore"]))
                {
                    string[] arrStore = listStore.Split(',');
                    string[] arrRole = listRole.Split(',');
                    for (int i = 0; i < arrStore.Count(); i++)
                    {
                        Permission permission = new Permission();
                        permission.role = clientRole.WSDetailRole(arrRole[i], VariableConfigController.GetBucket());
                        permission.store = clientInventory.WSDetailInventory(arrStore[i], VariableConfigController.GetBucket());
                        employee.arrPermission.Add(permission);
                    }
                }
            }

            //try
            //{
            //    employee.status = Boolean.Parse(collection["status"]);
            //}
            //catch (Exception)
            //{
            //    employee.status = false;
            //}
            if (!string.IsNullOrWhiteSpace(collection["firstName"]))
            {
                employee.firstName = collection["firstName"];
            }
            else
            {
                employee.firstName = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["lastName"]))
            {
                employee.lastName = collection["lastName"];
            }
            else
            {
                employee.lastName = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["gender"]))
            {
                employee.gender = Convert.ToInt32(collection["gender"]);
            }
            else
            {
                employee.gender = 2;
            }
            if (!string.IsNullOrWhiteSpace(collection["birthday"]))
            {
                employee.birthday = collection["birthday"];
            }
            else
            {
                employee.birthday = DateTime.Today.ToString();
            }
            if (!string.IsNullOrWhiteSpace(collection["phonenumber"]))
            {
                employee.phone = collection["phonenumber"];
            }
            else
            {
                employee.phone = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["hiredate"]))
            {
                employee.hiredDate = collection["hiredate"];
            }
            else
            {
                employee.hiredDate = DateTime.Today.ToString();
            }
            if (!string.IsNullOrWhiteSpace(collection["address"]))
            {
                employee.address = collection["address"];
            }
            else
            {
                employee.address = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["city"]))
            {
                employee.city = collection["city"];
            }
            else
            {
                employee.city = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["zipcode"]))
            {
                employee.zipCode = collection["zipcode"];
            }
            else
            {
                employee.zipCode = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["state"]))
            {
                employee.state = collection["state"];
            }
            else
            {
                employee.state = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["country"]))
            {
                employee.country = collection["country"];
            }
            else
            {
                employee.country = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["pincode"]))
            {
                employee.pinCode = collection["pincode"];
            }
            else
            {
                employee.pinCode = "";
            }
            if (!string.IsNullOrWhiteSpace(collection["password"]) && collection["password"] != employee.password)
            {
                employee.password = VariableConfigController.MD5Hash(collection["password"]);
            }
            //if (!string.IsNullOrWhiteSpace(collection["passwordConfirm"]))
            //{
            //    employee.passwordConfirm = VariableConfigController.MD5Hash(collection["passwordConfirm"]); ;
            //}

            employee.AddressCombine = employee.address + ", " + employee.city + " " + employee.zipCode + ", " + employee.state + ", " + employee.country;
            employee.fullName = employee.firstName + " " + employee.lastName;
            employee.image = collection["logoImg"];
            try
            {
                if (!string.IsNullOrWhiteSpace(collection["password"]) && collection["password"] != employee.password)
                {
                    var user = UserManager.FindById(employee._id);
                    if (user != null)
                    {
                        UserManager.RemovePassword(user.Id);

                        UserManager.AddPassword(user.Id, collection["password"].ToString());
                    }
                    else
                    {
                        #region create asp user
                        IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
                        var modelMerchant = clientMerchant.WSGetMerchantManagementBySiteId(VariableConfigController.GetBucket());

                        string userName = employee.email;
                        if (modelMerchant != null)
                        {
                            employee.userOwner = modelMerchant.merchantID.ToString();
                            userName = string.Format("{0}_{1}", modelMerchant.bucket.Replace("-", ""), employee.email);
                        }
                        ApplicationDbContext _db = new ApplicationDbContext();
                        user = new ApplicationUser() { UserName = userName, Email = employee.email, Id = employee._id };

                        var resultStatus = UserManager.Create(user, collection["password"].ToString());

                        if (resultStatus.Succeeded)
                        {
                            //assign user to role merchant
                            ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));

                            _db.AddUserToRole(userManager, user.Id, ConstantSmoovs.RoleNames.UserMerchant);

                            employee._id = user.Id;
                        }
                        #endregion
                    }
                }
                int result = client.WSUpdateEmployee(employee, VariableConfigController.GetBucket());
                client.Close();
                clientRole.Close();
                clientInventory.Close();
            }
            catch (Exception) { }
            return RedirectToAction("Detail", new { id = id });
        }
        [Authorize(Roles = "Merchant")]
        public ActionResult Delete(string id)
        {
            ViewBag.Class = "employee";
            WSEmployeeClient client = new WSEmployeeClient();
            Employee employee = client.WSDetailEmployee(id, VariableConfigController.GetBucket());
            employee.display = false;
            int result = client.WSUpdateEmployee(employee, VariableConfigController.GetBucket());
            client.Close();
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "Merchant")]
        [HttpPost]
        public ActionResult updateStatusEmployee(string id, bool status)
        {
            WSEmployeeClient client = new WSEmployeeClient();
            var employee = client.WSDetailEmployee(id, VariableConfigController.GetBucket());
            employee.status = status;
            client.WSUpdateEmployee(employee, VariableConfigController.GetBucket());
            client.Close();
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "Merchant")]
        public ActionResult Detail(string id)
        {
            ViewBag.Class = "Employee";
            ViewBag.Class = "employee";
            WSEmployeeClient client = new WSEmployeeClient();

            Employee employee = client.WSDetailEmployee(id, VariableConfigController.GetBucket());
            client.Close();
            EmployeeDetailModel employeeModel = new EmployeeDetailModel();
            employeeModel = GetSaleHistory(id);

            employeeModel.Employee = employee;

            return View(employeeModel);
        }

        /// <summary>
        /// Gets the sale history.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/13/2015-8:03 PM</datetime>
        private EmployeeDetailModel GetSaleHistory(string employeeId)
        {
            EmployeeDetailModel employeeModel = new EmployeeDetailModel();
            WSOrderClient client = new WSOrderClient();
            IEnumerable<Order> ListOrderAll = client.WSGetAllOrder(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAll, VariableConfigController.GetBucket());
            client.Close();

            if (ListOrderAll != null && ListOrderAll.Count() > 0)
            {
                ListOrderAll = ListOrderAll.Where(r => r._id != null && r.staff != null && r.staff._id.Equals(employeeId));


                employeeModel.OrderListSelfPOS = ListOrderAll.Count() > 0 ? ListOrderAll.Where(r => r._id != null && r.orderType == 1 && r.isPOS == 1 && (r.orderStatus == 8 || r.orderStatus == 9)).Distinct() : new List<Order>();
                employeeModel.OrderListSelfPOSThisMonth = employeeModel.OrderListSelfPOS.Where(r => r._id != null && r.completedDate != null &&
                    r.completedDate.Month == DateTime.UtcNow.Month).Count() > 0 ?
                    employeeModel.OrderListSelfPOS.Where(r => r.completedDate != null && r.completedDate.Month == DateTime.UtcNow.Month).Distinct() : new List<Order>();
                employeeModel.OrderListSelfPOSLastMonth = employeeModel.OrderListSelfPOS.Where(r => r._id != null && r.completedDate != null
                    && r.completedDate.Month == DateTime.UtcNow.AddMonths(-1).Month).Count() > 0 ?
                    employeeModel.OrderListSelfPOS.Where(r => r.completedDate.Month == DateTime.UtcNow.AddMonths(-1).Month).Distinct() : new List<Order>();


                employeeModel.OrderListSelfOnl = ListOrderAll.Count() > 0 ? ListOrderAll.Where(r => r._id != null && r.orderType == 1 && r.isPOS == 0 && (r.orderStatus == 8 || r.orderStatus == 9)).Distinct() : new List<Order>();
                employeeModel.OrderListSelfOnlThisMonth = employeeModel.OrderListSelfOnl.Where(r => r._id != null && r.completedDate != null && r.completedDate.Month == DateTime.UtcNow.Month).Count() > 0 ? employeeModel.OrderListSelfOnl.Where(r => r.completedDate != null && r.completedDate.Month == DateTime.UtcNow.Month).Distinct() : new List<Order>();
                employeeModel.OrderListSelfOnlLastMonth = employeeModel.OrderListSelfOnl.Where(r => r._id != null && r.completedDate != null && r.completedDate.Month == DateTime.UtcNow.AddMonths(-1).Month).Count() > 0 ? employeeModel.OrderListSelfOnl.Where(r => r.completedDate != null && r.completedDate.Month == DateTime.UtcNow.AddMonths(-1).Month).Distinct() : new List<Order>();


                employeeModel.OrderListDelivery = ListOrderAll.Count() > 0 ? ListOrderAll.Where(r => r._id != null && r.orderType == 0 && (r.orderStatus == 6 || r.orderStatus == 9)).Distinct() : new List<Order>();
                employeeModel.OrderListDeliveryThisMonth = employeeModel.OrderListDelivery.Where(r => r._id != null && r.completedDate != null && r.completedDate.Month == DateTime.UtcNow.Month).Count() > 0 ? employeeModel.OrderListDelivery.Where(r => r.completedDate != null && r.completedDate.Month == DateTime.UtcNow.Month).Distinct() : new List<Order>();
                employeeModel.OrderListDeliveryLastMonth = employeeModel.OrderListDelivery.Where(r => r._id != null && r.completedDate != null && r.completedDate.Month == DateTime.UtcNow.AddMonths(-1).Month).Count() > 0 ? employeeModel.OrderListDelivery.Where(r => r.completedDate != null && r.completedDate.Month == DateTime.UtcNow.AddMonths(-1).Month).Distinct() : new List<Order>();

            }
            else
            {
                ListOrderAll = new List<Order>();
            }

            employeeModel.OrderList = ListOrderAll;
            return employeeModel;
        }
        /*
         * 
         */
        public JsonResult IsExistedEmployee(string Email, string id)
        {
            if (string.IsNullOrEmpty(Email))
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            var isExisted = false;
            WSEmployeeClient client = new WSEmployeeClient();
            int count = client.WSCheckEmailExist(ConstantSmoovs.CouchBase.DesignDocEmployee, ConstantSmoovs.CouchBase.CheckMailExist, Email, VariableConfigController.GetBucket());
            if (count == 0)
            {
                return new JsonResult
                {
                    Data = !isExisted,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return new JsonResult
            {
                Data = isExisted,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };          
        }

        [HttpPost]
        public Boolean checkEmailExist(string email)
        {
            WSEmployeeClient client = new WSEmployeeClient();
            int count = client.WSCheckEmailExist(ConstantSmoovs.CouchBase.DesignDocEmployee, ConstantSmoovs.CouchBase.CheckMailExist, email, VariableConfigController.GetBucket());
            if (count == 0) return true;
            else return false;
        }
    }
}