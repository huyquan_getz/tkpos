﻿using AutoMapper;
using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using Microsoft.AspNet.Identity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models;
using SmoovPOS.UI.Models.General;
using SmoovPOS.UI.Models.Setting;
using SmoovPOS.Utility.WSCurrencyReference;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using SmoovPOS.Utility.WSSiteReference;
using SmoovPOS.Utility.WSWebContentReference;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class GeneralController : BaseController
    {
        [Authorize]
        // GET: GeneralSetting
        public ActionResult Index()
        {
            //ViewBag.NumberInit = CheckInitiation();
            var urlActive = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteMerchantSmoovPOS];
            ViewBag.Url = urlActive;

            //Initalize model
            GeneralSettingModel model = new GeneralSettingModel();

            //Get saved general settings from couchbase
            model.generalSetting = GetGeneralSetting();

            if (model.generalSetting != null)
            {
                //Set default for General settings: currency="Singapore Dollar", timezone="Singapore Standard Time", default GST="", default BestSeller="", DailyReportPeriod=00:00-23:59
                if (string.IsNullOrEmpty(model.generalSetting.CurrencyFullName) || string.IsNullOrWhiteSpace(model.generalSetting.CurrencyFullName)) model.generalSetting.CurrencyFullName = "Singapore Dollar";
                if (string.IsNullOrEmpty(model.generalSetting.TimeZoneFullName) || string.IsNullOrWhiteSpace(model.generalSetting.TimeZoneFullName)) model.generalSetting.TimeZoneFullName = "Singapore Standard Time";
                if (string.IsNullOrEmpty(model.generalSetting.DailyReportPeriodBeginFrom) || string.IsNullOrWhiteSpace(model.generalSetting.DailyReportPeriodBeginFrom)) model.generalSetting.DailyReportPeriodBeginFrom = "00:00";
                if (string.IsNullOrEmpty(model.generalSetting.DailyReportPeriodEndAt) || string.IsNullOrWhiteSpace(model.generalSetting.DailyReportPeriodEndAt)) model.generalSetting.DailyReportPeriodEndAt = "23:59";
            }
            else
            {
                model.generalSetting = new MerchantGeneralSetting();
            }

            //Get default list of timezones from Enums
            model.listDefaultTimeZones = SmoovPOS.Common.ConstantSmoovs.Enums.ListTimeZone;

            //Get default list of currencies from couchbase and display on View()
            WSCurrencyClient currencyClient = new WSCurrencyClient();
            model.listDefaultCurrencies = currencyClient.GetAllCurrency(ConstantSmoovs.CouchBase.DesignDocDeveloperSetting, ConstantSmoovs.CouchBase.CurrencyViewAll, VariableConfigController.GetBucket()).OrderBy(i => i.currencyFullName).ToList();
            currencyClient.Close();
            ViewBag.Class = "general";
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection form)
        {
            var urlActive = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteMerchantSmoovPOS];
            ViewBag.Url = urlActive;
            string siteID = VariableConfigController.GetBucket();
            //Get the CB record
            WSMerchantGeneralSettingClient client = new WSMerchantGeneralSettingClient();
            MerchantGeneralSetting generalSetting = client.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID);
            bool isCreate = generalSetting == null;
            if (isCreate)
            {
                generalSetting = new MerchantGeneralSetting();
                generalSetting._id = Guid.NewGuid().ToString();

                generalSetting.createdDate = DateTime.UtcNow;
                generalSetting.userOwner = User.Identity.GetUserId();
                generalSetting.userID = generalSetting.userOwner;
                generalSetting.modifiedDate = DateTime.UtcNow;
                var idMerchant = VariableConfigController.GetIDMerchant();
                generalSetting.channels = new String[] { idMerchant };
                generalSetting.status = true;
                generalSetting.display = true;
                generalSetting.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            }
            //Set the new values
            if (!string.IsNullOrWhiteSpace(form["selectedTimeZone"]))
            {
                generalSetting.TimeZoneFullName = form["selectedTimeZone"].Split('~')[0];
                generalSetting.TimeZone = form["selectedTimeZone"].Split('~')[1];
            }
            if (!string.IsNullOrWhiteSpace(form["selectedCurrency"]))
            {
                generalSetting.CurrencyFullName = form["selectedCurrency"].Split('~')[0];
                generalSetting.Currency = form["selectedCurrency"].Split('~')[1];
            }
            generalSetting.GST = form["stringGST"];
            generalSetting.CountOfBestSellerProduct = form["strBestSeller"];
            generalSetting.DailyReportPeriodBeginFrom = form["dtpFrom"];
            generalSetting.DailyReportPeriodEndAt = form["dtpTo"];
            generalSetting.isDailyReportPeriodEndAtNextDay = Convert.ToBoolean(form["DailyReportPeriodEndAtNextDay"]);

            //Call WS to save the update info
            try
            {
                int result = 0;
                if (isCreate)
                {
                    result = client.CreateNewMerchantGeneralSetting(generalSetting, siteID);
                }
                else
                {
                    result = client.UpdateMerchantGeneralSetting(generalSetting, siteID);
                }
                client.Close();
            }
            catch (Exception ex)
            {
                client.Close();

                throw ex;
            }

            //Reload this page after done instead of redirecting to other page
            return RedirectToAction("Index", "General");
        }

        public ActionResult UpdateProfile()
        {
            ViewBag.NumberInit = CheckInitiation();
            WSInventoryClient clientInventory = new WSInventoryClient();
            Country[] countrys = clientInventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, VariableConfigController.GetBucket());
            ViewBag.countries = countrys;
            ViewBag.Class = "dashboard";
            clientInventory.Close();
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var result = client.WSGetMerchantManagementByUserId(VariableConfigController.GetUserID());
            client.Close();
            if (result == null)
            {
                return RedirectToAction("Index");
            }
            var modelSiteAlias = new SiteAliasModel();
            WSSiteClient clientSite = new WSSiteClient();
            modelSiteAlias = clientSite.WSGetSiteAlias(VariableConfigController.GetBucket());
            clientSite.Close();
            result.CheckSubdomain = modelSiteAlias.CheckSubdomain == null ? true : modelSiteAlias.CheckSubdomain;
            result.SubDomain = modelSiteAlias.HTTPAlias;
            result.Domain = modelSiteAlias.HTTP;
            var model = Mapper.Map<MerchantManagementModel, MerchantManagementViewModel>(result);
            ViewBag.urlSite = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteOnlineDomainSmoovPOS];
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateProfile(FormCollection form)
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            MerchantManagementModel merchantInfo = client.WSGetMerchantManagementByUserId(VariableConfigController.GetUserID());
            merchantInfo.businessName = form["businessName"];
            merchantInfo.titleHomePage = form["titleHomePage"];
            merchantInfo.title = form["TitleAccount"];
            merchantInfo.firstName = form["FirstName"];
            merchantInfo.lastName = form["LastName"];
            merchantInfo.fullName = form["firstName"] + " " + form["lastName"];
            merchantInfo.phoneRegion = form["phoneRegion"];
            merchantInfo.phone = form["phone"];
            merchantInfo.gender = form["Gender"] == "Male" ? true : false;
            merchantInfo.birthday = DateTime.ParseExact(form["birthday"], "M/dd/yyyy", null);
            merchantInfo.address = form["address"];
            merchantInfo.city = form["city"];
            merchantInfo.region = form["region"];
            merchantInfo.zipCode = form["zipCode"];
            merchantInfo.country = form["Country"];
            client.WSUpdateMerchantManagement(merchantInfo);
            client.Close();

            WSSiteClient clientSite = new WSSiteClient();
            var modelSiteAlias = clientSite.WSGetSiteAlias(VariableConfigController.GetBucket());
            modelSiteAlias.CheckSubdomain = form["CheckSubdomain"] == "true" ? true : false;

            var siteOnline = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteOnline];
            //if (modelSiteAlias.CheckSubdomain==true)
            //{
            //    modelSiteAlias.HTTPAlias = string.Format(siteOnline.ToString(), form["SubDomain"]);
            //    modelSiteAlias.HTTP = string.IsNullOrEmpty(form["Domain"]) ? "" : string.Format("http://{0}", form["Domain"]);
            //}
            //else
            //{
            //    modelSiteAlias.HTTP = string.IsNullOrEmpty(form["Domain"]) ? "" : string.Format("http://{0}", form["Domain"]);
            //    modelSiteAlias.HTTPAlias = string.IsNullOrEmpty(form["Domain"]) ? "" : string.Format("http://{0}", form["Domain"]);
            //}

            modelSiteAlias.HTTPAlias = string.Format(siteOnline.ToString(), form["SubDomain"]);
            modelSiteAlias.HTTP = string.IsNullOrEmpty(form["Domain"]) ? "" : string.Format("http://{0}/", form["Domain"]);

            clientSite.WSUpdateSiteAliasSQL(modelSiteAlias, VariableConfigController.GetBucket());
            clientSite.Close();
            UpdateSetting("UpdateProfile", "1");

            WSWebContentClient clientContent = new WSWebContentClient();
            WebContent webContent = new WebContent();
            webContent = clientContent.WSGetAllWebContent("webcontent", "get_all_webcontent", VariableConfigController.GetBucket()).FirstOrDefault();
            webContent.titleHomePage = form["titleHomePage"];
            int update = clientContent.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
            clientContent.Close();
            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
            // MerchantSetting setting = new MerchantSetting();
            var setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
            clientMerchantSetting.Close();

            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            userSession.Domain = modelSiteAlias.HTTP;
            userSession.SubDomain = modelSiteAlias.HTTPAlias;
            userSession.CheckSubDomain = modelSiteAlias.CheckSubdomain.GetValueOrDefault();
            if (setting.updateProfile && setting.createBranch && setting.createCategory && setting.addProduct && setting.distribute)
            {
                return RedirectToAction("ViewProfile");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        [HttpPost]
        public int UpdateSetting(string field, string value)
        {
            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
            MerchantSetting setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
            if (field == "UpdateProfile")
            {
                setting.updateProfile = true;
            }
            if (field == "CreateBranch")
            {
                setting.createBranch = true;
            }
            if (field == "CreateCategory")
            {
                setting.createCategory = true;
            }
            if (field == "CreateProduct")
            {
                setting.addProduct = true;
            }
            if (field == "Distribute")
            {
                setting.distribute = true;
            }
            int checkStatus = clientMerchantSetting.UpdateMerchantSetting(setting, VariableConfigController.GetBucket());
            clientMerchantSetting.Close();
            return checkStatus;
        }

        [HttpPost]
        public int CountStepCompleted()
        {
            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
            MerchantSetting setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
            clientMerchantSetting.Close();
            int count = 0;
            if (setting.updateProfile == true)
            {
                count++;
            }
            if (setting.createBranch == true)
            {
                count++;
            }
            if (setting.createCategory == true)
            {
                count++;
            }
            if (setting.addProduct == true)
            {
                count++;
            }
            if (setting.distribute == true)
            {
                count++;
            }
            return count;
        }

        [HttpPost]
        public int CheckUrlExist(string url, bool isSubDomain)
        {
            var siteOnline = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteOnline];
            WSSiteClient clientSite = new WSSiteClient();
            if (isSubDomain)
            {
                url = string.Format(siteOnline.ToString(), url);
            }
            else
            {
                url = "http://" + url + "/";
            }
            var checkStatus = clientSite.WSCheckExistUrl(url, isSubDomain,VariableConfigController.GetUserID());
            clientSite.Close();
            if (checkStatus) return 1;
            else return 0;
        }

        public ActionResult ViewProfile()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "dashboard";
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var result = client.WSGetMerchantManagementByUserId(VariableConfigController.GetUserID());
            client.Close();
            if (result == null)
            {
                return RedirectToAction("Index");
            }
            // var model = Mapper.Map<MerchantManagementModel, MerchantManagementViewModel>(result);
            return View(result);
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewBag.Class = "dashboard";
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var result = client.WSGetMerchantManagementByUserId(VariableConfigController.GetUserID());
            client.Close();
            if (result == null)
            {
                return RedirectToAction("Index");
            }
            var model = new ChangePasswordMerchantManagementViewModel();
            model = Mapper.Map<MerchantManagementModel, ChangePasswordMerchantManagementViewModel>(result);
            return View(model);
        }

        public JsonResult IsMatchPassword(string currentPassword, string newPassword)
        {

            if (string.IsNullOrEmpty(currentPassword))
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            AccountController accountController = new AccountController();

            return accountController.CheckPasswordExist(VariableConfigController.GetUserName(), currentPassword, newPassword);
        }
    }
}