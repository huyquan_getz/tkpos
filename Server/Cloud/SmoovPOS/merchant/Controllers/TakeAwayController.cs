﻿using AutoMapper;
using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Entity.Settings;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSTakeAwaySettingReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CouchBaseCommon = SmoovPOS.Common.ConstantSmoovs.CouchBase;

namespace SmoovPOS.UI.Controllers
{
    public class TakeAwayController : Controller
    {
        string SiteID = "";

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.SiteID = VariableConfigController.GetBucket();
        }

        // GET: TableOrder
        public ActionResult Index(string id)
        {
            ViewBag.Class = "takeaway";
            string branchId = id;
            TakeAwaySettingViewModel takeAwayViewModel = new TakeAwaySettingViewModel();
            takeAwayViewModel.ListInventory = new System.Collections.Generic.List<Inventory>();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            var inventorys = inventoryClient.WSGetAll(CouchBaseCommon.DesigndocInventory, CouchBaseCommon.GetAllInventories, this.SiteID).Where(i => i.status == true && i.display == true);

            foreach (var inventory in inventorys)
            {
                if (inventory.index != Common.ConstantSmoovs.Stores.OnlineStore && inventory.businessID != Common.ConstantSmoovs.Stores.Master)
                {
                    takeAwayViewModel.ListInventory.Add(inventory);
                }
            }

            takeAwayViewModel.SelectedBranchId = branchId;

            inventoryClient.Close();

            var servicePacket = new SmoovPOS.Business.Components.ServicePackageComponent();
            ServicePackageModel servicePackage = servicePacket.GetServicePacket(SiteID);
            var functionServicePacket = servicePackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.ONLINESHOP);
            takeAwayViewModel.isAddNew = functionServicePacket != null && functionServicePacket.limit >= 1;

            return View(takeAwayViewModel);
        }

        private TakeAwaySetting InitFirstSetting(string branchId, int branchIndex)
        {
            TakeAwaySetting setting = new TakeAwaySetting();
            setting._id = Guid.NewGuid().ToString();
            setting.AllowAutoAcceptOrder = false;
            setting.BranchId = branchId;
            setting.BranchIndex = branchIndex;
            var idMerchant = VariableConfigController.GetIDMerchant();
            setting.channels = new String[] { idMerchant + "_" + branchIndex.ToString() + "_table" };
            setting.display = true;
            setting.TakeAwayInstructions = string.Empty;
            setting.status = true;

            setting.ListDayOfWeekSetting = new List<SmoovPOS.Entity.Entity.Settings.DayOfWeekSetting>();
            for (int i = 0; i < 7; i++)
            {
                setting.ListDayOfWeekSetting.Add(new SmoovPOS.Entity.Entity.Settings.DayOfWeekSetting() { DayOfWeek = i, Option = 2 });
            }

            WSTakeAwaySettingClient tableOSClient = new WSTakeAwaySettingClient();
            tableOSClient.WSCreateTakeAwaySetting(setting, this.SiteID);
            setting = tableOSClient.WSDetailTakeAwaySetting(branchId, this.SiteID);
            return setting;
        }

        public ActionResult Detail(string id)
        {
            ViewBag.Class = "tableordering";
            string branchId = id;

            TakeAwaySettingViewModel takeAwayViewModel = new TakeAwaySettingViewModel();
            Inventory inventory = new Inventory();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            inventory = inventoryClient.WSDetailInventory(branchId, this.SiteID);

            inventoryClient.Close();

            WSTakeAwaySettingClient tableOSClient = new WSTakeAwaySettingClient();

            TakeAwaySetting takeAwaySetting = tableOSClient.WSDetailTakeAwaySettingByBranchId(CouchBaseCommon.DesigndocTakeAway, CouchBaseCommon.ViewNameGetAllTakeAwayByBranch, branchId, this.SiteID);
            takeAwayViewModel.SelectedBranchId = branchId;

            if (takeAwaySetting == null)
            {
                takeAwayViewModel = Mapper.Map<TakeAwaySetting, TakeAwaySettingViewModel>(InitFirstSetting(branchId, inventory.index));
            }
            else
            {
                takeAwayViewModel = Mapper.Map<TakeAwaySetting, TakeAwaySettingViewModel>(takeAwaySetting);
            }


            tableOSClient.Close();

            bool isEdit = HttpContext.Request.UrlReferrer.AbsoluteUri.IndexOf("Edit") > -1;
            if (isEdit)
            {
                ViewBag.IsDisabled = false;
            }
            else
            {
                ViewBag.IsDisabled = true;
            }

            return PartialView("Detail", takeAwayViewModel);
        }

        public ActionResult Edit(string id)
        {
            var servicepacket = new SmoovPOS.Business.Components.ServicePackageComponent();
            ServicePackageModel servicepackage = servicepacket.GetServicePacket(SiteID);
            var functionservicepacket = servicepackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.ONLINESHOP);
            if (!(functionservicepacket != null && functionservicepacket.limit >= 1))
            {
                return RedirectToAction("index");
            }

            ViewBag.Class = "tableordering";
            string branchId = id;
            TakeAwaySettingViewModel takeAwayViewModel = new TakeAwaySettingViewModel();

            WSTakeAwaySettingClient tableOSClient = new WSTakeAwaySettingClient();
            TakeAwaySetting takeAwaySetting = tableOSClient.WSDetailTakeAwaySettingByBranchId(CouchBaseCommon.DesigndocTakeAway, CouchBaseCommon.ViewNameGetAllTakeAwayByBranch, branchId, this.SiteID);
            tableOSClient.Close();

            takeAwayViewModel = Mapper.Map<TakeAwaySetting, TakeAwaySettingViewModel>(takeAwaySetting);

            takeAwayViewModel.ListInventory = new System.Collections.Generic.List<Inventory>();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            var inventory = inventoryClient.WSDetailInventory(branchId, this.SiteID);

            inventoryClient.Close();

            takeAwayViewModel.ListInventory.Add(inventory);


            takeAwayViewModel.SelectedBranchId = branchId;



            return View(takeAwayViewModel);
        }

        [HttpPost]
        public ActionResult Edit(TakeAwaySettingViewModel takeAwayViewModel)
        {
            var servicePacket = new SmoovPOS.Business.Components.ServicePackageComponent();
            ServicePackageModel servicePackage = servicePacket.GetServicePacket(SiteID);
            var functionServicePacket = servicePackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.ONLINESHOP);
            if (!(functionServicePacket != null && functionServicePacket.limit >= 1))
            {
                return RedirectToAction("Index");
            }

            string branchId = takeAwayViewModel.SelectedBranchId;

            bool isCreateNew = false;

            takeAwayViewModel.ListInventory = new System.Collections.Generic.List<Inventory>();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            var inventory = inventoryClient.WSDetailInventory(branchId, this.SiteID);

            takeAwayViewModel.ListInventory.Add(inventory);

            inventoryClient.Close();

            if (ModelState.IsValid)
            {
                TakeAwaySetting takeAwaySetting = new TakeAwaySetting();

                WSTakeAwaySettingClient tableOSClient = new WSTakeAwaySettingClient();

                takeAwaySetting = tableOSClient.WSDetailTakeAwaySettingByBranchId(CouchBaseCommon.DesigndocTakeAway, CouchBaseCommon.ViewNameGetAllTakeAwayByBranch, branchId, this.SiteID);

                if (takeAwaySetting == null)
                {
                    isCreateNew = true;
                    takeAwaySetting = new TakeAwaySetting();
                    takeAwaySetting._id = Guid.NewGuid().ToString();
                    takeAwaySetting.BranchId = takeAwayViewModel.SelectedBranchId;
                }

                takeAwaySetting.BranchIndex = inventory.index;
                takeAwaySetting.AcceptTakeAway = takeAwayViewModel.AcceptTakeAway;
                takeAwaySetting.TakeAwayInstructions = takeAwayViewModel.TakeAwayInstructions;

                if (takeAwaySetting.ListDayOfWeekSetting == null) takeAwaySetting.ListDayOfWeekSetting = new List<DayOfWeekSetting>();

                takeAwaySetting.ListDayOfWeekSetting = new List<DayOfWeekSetting>();

                foreach (DayOfWeekSetting dws in takeAwayViewModel.ListDayOfWeekSetting)
                {
                    dws.DayOfWeek = takeAwayViewModel.ListDayOfWeekSetting.IndexOf(dws);
                    if (dws.Option == 0)
                    {
                        dws.Split_OpeningStart = string.Empty;
                        dws.Split_OpeningEnd = string.Empty;
                        dws.Split_ClosingStart = string.Empty;
                        dws.Split_ClosingEnd = string.Empty;
                    }

                    if (dws.Option == 1)
                    {
                        dws.Open_Opening = string.Empty;
                        dws.Open_Closing = string.Empty;
                    }

                    if (dws.Option == 2)
                    {
                        dws.Split_OpeningStart = string.Empty;
                        dws.Split_OpeningEnd = string.Empty;
                        dws.Split_ClosingStart = string.Empty;
                        dws.Split_ClosingEnd = string.Empty;
                        dws.Open_Opening = string.Empty;
                        dws.Open_Closing = string.Empty;
                    }

                    takeAwaySetting.ListDayOfWeekSetting.Add(dws);
                }

                if (isCreateNew)
                {
                    tableOSClient.WSCreateTakeAwaySetting(takeAwaySetting, this.SiteID);
                }
                else
                {
                    tableOSClient.WSUpdateTakeAwaySetting(takeAwaySetting, this.SiteID);
                }

                tableOSClient.Close();
            }
            else
            {
                string errorMsg = ModelState.Values.SelectMany(e => e.Errors).Select(gh => gh.ErrorMessage).FirstOrDefault().ToString();
                ModelState.AddModelError("DayOfWeekSetting", errorMsg);//"Invalid username or password."
                return View(takeAwayViewModel);
            }
            return RedirectToAction("Index", new { id = takeAwayViewModel.SelectedBranchId });
        }
    }
}