﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using SmoovPOS.Common;
using SmoovPOS.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SmoovPOS.Utility.MailServer;
using System.Text;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSOrderReference;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.IWSEmailTemplateReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using SmoovPOS.Utility.WSCustomerReference;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Entity.Models.Paging;
using SmoovPOS.Entity.Models;
using SmoovPOS.Utility.WSEmployeeReference;
using Com.SmoovPOS.TimeZone;
using SmoovPOS.Business.Business;
using AutoMapper;
using Microsoft.AspNet.Identity;
using SmoovPOS.Utility.WSCheckoutSettingReference;
namespace SmoovPOS.UI.Controllers
{
    public class OrderController : BaseController
    {
        /// <summary>
        /// Get order listing.
        /// </summary>
        /// <returns></returns>
        /// <createdby>nhu.dang</createdby>
        /// <createddate>12/11/2014-4:32 PM</createddate>
        public ActionResult Index(SearchOrderModel searchModel)
        {   
            ViewBag.NumberInit = CheckInitiation();
            //OrderListingModel orderListing = new OrderListingModel();
            //var result = GetOrderPaging();
            //orderListing.AllOrderList = GetOrderBySearchCondition(new SearchOrderModel());
            if (searchModel.pageGoTo == 0)
            {
                searchModel.pageGoTo = 1;
                searchModel.sort = SmoovPOS.Common.ConstantSmoovs.OrderSort.completedDate;
                searchModel.desc = true;
                searchModel.OrderType = ConstantSmoovs.OrderType.OrderTypeTableOrdering;
                searchModel.TableId = ConstantSmoovs.Order.TableOrderingTable;
                searchModel.TabId = ConstantSmoovs.Order.TableOrderingTab;
            }
            searchModel.siteID = VariableConfigController.GetBucket();
            WSOrderClient client = new WSOrderClient();
            var model = client.WSGetOrderByFilter(searchModel);
            //orderListing.AllOrderList = GetOrderPaging(searchModel);
            ViewBag.Class = "order";

            //Init search data for delivery, self-collect, cancel order
            InitSearchData(model);
            //InitSearchData(model);
            //InitSearchData(model);

            //----------------------
            //var results = orderListing.AllOrderList.OrderBy(r => r.completedDate).ToList();
            //int i = 0;
            //foreach (var item in orderListing.AllOrderList)
            //{
            //    item.indexDateSort = i;
            //    i++;
            //}

            //orderListing.AllOrderList = results;
            //-----------------
            var merchantGeneralSetting = GetGeneralSetting();
            if (merchantGeneralSetting != null)
            {
                model.Currency = merchantGeneralSetting.Currency;
            }

            return View(model);
        }

        private void InitSearchData(SearchOrderModel searchModel)
        {
            searchModel.FilterList = new List<SelectListItem>() { 
                new SelectListItem(){Text=ConstantSmoovs.FilterFieldList.FilterFieldCustomer, Value=ConstantSmoovs.FilterFieldList.FilterFieldCustomer, Selected = true},
                new SelectListItem(){Text=ConstantSmoovs.FilterFieldList.FilterFieldOrderCode, Value=ConstantSmoovs.FilterFieldList.FilterFieldOrderCode},
                new SelectListItem(){Text=ConstantSmoovs.FilterFieldList.FilterFieldTransactionID, Value=ConstantSmoovs.FilterFieldList.FilterFieldTransactionID}
                //new SelectListItem(){Text=ConstantSmoovs.FilterFieldList.FilterFieldStore, Value=ConstantSmoovs.FilterFieldList.FilterFieldStore}
            };
            searchModel.PaymentStatusList = new List<SelectListItem>() { 
                new SelectListItem(){Text="All Payment Status", Value="", Selected = true},
                new SelectListItem(){Text=ConstantSmoovs.PaymentStatus.Failed, Value=ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.PaymentStatus.Failed).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.PaymentStatus.Paid, Value=ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.PaymentStatus.Paid).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.PaymentStatus.Pending, Value=ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.PaymentStatus.Pending).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.PaymentStatus.Refunded, Value=ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.PaymentStatus.Refunded).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.PaymentStatus.Waiting, Value=ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.PaymentStatus.Waiting).Key.ToString()}
            };
            searchModel.OrderStatusList = new List<SelectListItem>() { 
                new SelectListItem(){Text="All Order Status", Value="", Selected = true},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.Canceled, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.Canceled).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.Collected, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.Collected).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.Delivered, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.Delivered).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.NewOrder, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.NewOrder).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.OnDelivery, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.OnDelivery).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.OnPreRequest, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.OnPreRequest).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.ReadyForCollection, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.ReadyForCollection).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.ReadyForDelivery, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.ReadyForDelivery).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.DineInConfirm, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.DineInConfirm).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.DineInWaiting, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.DineInWaiting).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.DineInReject, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.DineInReject).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.DineInServing, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.DineInServing).Key.ToString()},
                new SelectListItem(){Text=ConstantSmoovs.OrderStatus.DineInDone, Value=ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r=>r.Value == ConstantSmoovs.OrderStatus.DineInDone).Key.ToString()}
            };
            //searchModel.StoresList = new List<SelectListItem>(){ 
            //    new SelectListItem(){Text=ConstantSmoovs.Stores.AllStores, Value="", Selected = true},
            //    new SelectListItem(){Text=ConstantSmoovs.Stores.OnlineShop, Value=ConstantSmoovs.Stores.OnlineShop},
            //    new SelectListItem(){Text=ConstantSmoovs.Stores.Branch1, Value=ConstantSmoovs.Stores.Branch1}
            //};

            WSInventoryClient client = new WSInventoryClient();
            Inventory[] ListInventory = client.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket());
            searchModel.StoresList = new List<SelectListItem>(){ 
                new SelectListItem(){Text=ConstantSmoovs.Stores.AllStores, Value="", Selected = true}
            };
            ListInventory = ListInventory.OrderBy(i => i.businessID).ToArray();
            foreach (var item in ListInventory)
            {
                searchModel.StoresList.Add(new SelectListItem() { Text = item.businessID, Value = item.index.ToString() });
            }
        }
        [HttpPost]
        public ActionResult SearchOrder(SearchOrderModel searchModel)//(int orderType, int pageGoTo, string sort, bool desc, string search)
        {
            //var searchModel = new SearchOrderModel() { pageGoTo = pageGoTo, sort = sort, desc = desc, FilterField = search.Trim() };
            searchModel.siteID = VariableConfigController.GetBucket();

            if (string.IsNullOrEmpty(searchModel.sort) || searchModel.sort == "title")
            {
                searchModel.sort = SmoovPOS.Common.ConstantSmoovs.OrderSort.completedDate;
                searchModel.desc = true;
            }
            WSOrderClient client = new WSOrderClient();
            //searchModel.OrderType = searchModel.OrderType;
            var model = client.WSGetOrderByFilter(searchModel);
            InitSearchData(model);
            client.Close();
            switch (searchModel.OrderType)
            {
                case ConstantSmoovs.OrderType.OrderTypeDelivery:
                    model.TabId = ConstantSmoovs.Order.DeliveryTab;
                    model.TableId = ConstantSmoovs.Order.DeliveryTable;
                    break;
                case ConstantSmoovs.OrderType.OrderTypeSelfCollect:
                    model.TabId = ConstantSmoovs.Order.SelfCollectTab;
                    model.TableId = ConstantSmoovs.Order.SelfCollectTable;
                    break;
                case ConstantSmoovs.OrderType.OrderTypeTableOrdering:
                    model.TabId = ConstantSmoovs.Order.TableOrderingTab;
                    model.TableId = ConstantSmoovs.Order.TableOrderingTable;
                    break;
                case ConstantSmoovs.OrderType.OrderTypeCancel:
                    model.TabId = ConstantSmoovs.Order.CancelOrderTab;
                    model.TableId = ConstantSmoovs.Order.CancelOrderTable;
                    break;
                default:
                    break;
            }
            model.Currency = searchModel.Currency;
            if (string.IsNullOrEmpty(model.Currency))
            {
                var merchantGeneralSetting = GetGeneralSetting();
                if (merchantGeneralSetting != null)
                {
                    model.Currency = merchantGeneralSetting.Currency;
                }
            }
            if (model.ListOrders == null)
            {
                model.ListOrders = new List<Order>() { };
            }
            else
            {
                foreach (var item in model.ListOrders)
                {
                    var modifiedDate = item.modifiedDate;
                    if (item.inventory != null)
                    {
                        modifiedDate = TimeZoneUtil.ConvertUTCToTimeZone(modifiedDate, item.inventory.timeZone, VariableConfigController.GetBucket());
                    }
                    else
                    {
                        modifiedDate = TimeZoneUtil.ConvertUTCToTimeZone(modifiedDate);
                    }

                    item.modifiedDate = modifiedDate;
                }
            }
            return PartialView("OrderListing", model);
            //InitSearchData(searchOrderModel);

            //OrderItemListingModel itemOrderListing = new OrderItemListingModel();
            //itemOrderListing.SearchOrder = searchOrderModel;

            //int deliveryOrder = (int)Convert.ChangeType(ConstantSmoovs.Enums.OrderType.Delivery, ConstantSmoovs.Enums.OrderType.Delivery.GetTypeCode());
            //if (searchOrderModel.OrderType == (int)Convert.ChangeType(ConstantSmoovs.Enums.OrderType.Delivery, ConstantSmoovs.Enums.OrderType.Delivery.GetTypeCode()))
            //{
            //    itemOrderListing.TableId = ConstantSmoovs.Order.DeliveryTable;
            //}
            //else if (searchOrderModel.OrderType == (int)Convert.ChangeType(ConstantSmoovs.Enums.OrderType.SelfCollection, ConstantSmoovs.Enums.OrderType.SelfCollection.GetTypeCode()))
            //{
            //    itemOrderListing.TableId = ConstantSmoovs.Order.SelfCollectTable;
            //}
            //else if (searchOrderModel.OrderType == (int)Convert.ChangeType(ConstantSmoovs.Enums.OrderType.Cancel, ConstantSmoovs.Enums.OrderType.Cancel.GetTypeCode()))
            //{
            //    itemOrderListing.TableId = ConstantSmoovs.Order.CancelOrderTable;
            //}

            ////Call search
            //itemOrderListing.OrderList = GetOrderBySearchCondition(searchOrderModel);

            //return PartialView("~/Views/Order/OrderListing.cshtml", itemOrderListing);
        }

        /// <summary>
        /// Gets the order paging.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/24/2015-3:38 PM</datetime>
        private IEnumerable<OrderModel> GetOrderPaging()
        {
            string siteID = VariableConfigController.GetBucket();
            string timeZoneFullName = string.Empty;
            try
            {
                timeZoneFullName = GetGeneralSetting().TimeZoneFullName;
            }
            catch
            {
                timeZoneFullName = "Singapore Standard Time";
            }

            SearchOrderModel searchOrderModel = new SearchOrderModel();
            WSOrderClient client = new WSOrderClient();
            var sdfd = new EntityPaging();
            sdfd.pageGoTo = 1;
            sdfd.sort = string.Empty;
            var listOrder = client.WSGetAllOrderOrderPaging(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAllDelivery, searchOrderModel, sdfd, VariableConfigController.GetBucket());
            IEnumerable<OrderModel> orderList = listOrder.Select(r => new OrderModel(r, VariableConfigController.GetBucket(), timeZoneFullName));
            client.Close();

            //SearchOrderModel searchOrderModel = new SearchOrderModel();
            //WSOrderClient client = new WSOrderClient();
            //var sdfd=new EntityPaging();
            //var listOrder = client.WSGetAllOrderOrderPaging(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAllDelivery, searchOrderModel, sdfd, siteID);
            //IEnumerable<OrderModel> orderList = listOrder.ToList().Where(r => ConstantSmoovs.Enums.PaymentStatus.ContainsKey(r.paymentStatus) && ConstantSmoovs.Enums.PaymentStatus[r.paymentStatus] != ConstantSmoovs.PaymentStatus.Pending)
            // .Select(r => new OrderModel(r, siteID));
            //client.Close();

            return orderList;
            //return null;
        }

        /// <summary>
        /// Gets the order by search condition.
        /// </summary>
        /// <param name="searchOrderModel">The search order model.</param>
        /// <returns></returns>
        /// <createdby>nhu.dang</createdby>
        /// <createddate>12/20/2014-10:05 AM</createddate>
        private IEnumerable<OrderModel> GetOrderBySearchCondition(SearchOrderModel searchOrderModel)
        {
            string siteID = VariableConfigController.GetBucket();

            string timeZoneFullName = string.Empty;
            try
            {
                timeZoneFullName = GetGeneralSetting().TimeZoneFullName;
            }
            catch
            {
                timeZoneFullName = "Singapore Standard Time";
            }

            WSOrderClient client = new WSOrderClient();
            var listOrder = client.WSGetOrderBySearchCondition(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAll, searchOrderModel, siteID);
            IEnumerable<OrderModel> orderList = listOrder.Where(r => ConstantSmoovs.Enums.PaymentStatus.ContainsKey(r.paymentStatus) && ConstantSmoovs.Enums.PaymentStatus[r.paymentStatus] != ConstantSmoovs.PaymentStatus.Pending).Select(r => new OrderModel(r, siteID, timeZoneFullName)).ToList();
            client.Close();

            return orderList;
        }

        /// <summary>
        /// Details the specified order identifier.
        /// </summary>
        /// <param name="id">The order identifier.</param>
        /// <returns></returns>
        /// <createdby>zin</createdby>
        /// <createddate>12/16/2014-2:42 PM</createddate>
        public ActionResult Detail(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                string siteID = VariableConfigController.GetBucket();

                WSOrderClient client = new WSOrderClient();
                Order order = client.WSGetOrder(id, siteID);
                // get and update inventory;
                if (order.inventory != null)
                {
                    if (!string.IsNullOrWhiteSpace(order.inventory._id))
                    {
                        WSInventoryClient clientInventory = new WSInventoryClient();
                        Inventory inventoryNew = clientInventory.WSDetailInventory(order.inventory._id, siteID);
                        order.inventory = inventoryNew;
                        clientInventory.Close();
                    }
                }
                else
                {
                    order.inventory = new Inventory() { index = 0 };
                }
                //------------end get and update inventory;

                //Get and set merchant's general settings
                MerchantGeneralSetting merchantGeneralSetting = GetGeneralSetting();

                if (order.inventory != null && order.inventory.index == 1) order.inventory.timeZone = merchantGeneralSetting.TimeZoneFullName;
                order.currency = merchantGeneralSetting.Currency;
                client.WSUpdateOrder(order, siteID);

                ViewBag.Class = "order";
                OrderModel orderModel = new OrderModel(order, siteID, merchantGeneralSetting.TimeZoneFullName);
                client.Close();
                return View(orderModel);
            }
            else
            {

                return View();
            }

        }

        /// <summary>
        /// Prints the specified order identifier.
        /// </summary>
        /// <param name="id">The order identifier.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/24/2015-3:37 PM</datetime>
        public ActionResult Print(string id)
        {
            string siteID = VariableConfigController.GetBucket();
            string timeZoneFullName = string.Empty;
            try
            {
                timeZoneFullName = GetGeneralSetting().TimeZoneFullName;
            }
            catch
            {
                timeZoneFullName = "Singapore Standard Time";
            }

            WSOrderClient client = new WSOrderClient();
            Order order = client.WSGetOrder(id, siteID);
            OrderModel orderModel = new OrderModel(order, siteID, timeZoneFullName);
            client.Close();
            ViewBag.Class = "order";

            return View(orderModel);
        }

        /// <summary>
        /// Updates the status order.
        /// </summary>
        /// <param name="orderID">The order identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/24/2015-3:37 PM</datetime>
        [HttpPost]
        public ActionResult UpdateStatusOrder(string orderID, int status)
        {
            string siteID = VariableConfigController.GetBucket();

            string timeZoneFullName = string.Empty;
            try
            {
                timeZoneFullName = GetGeneralSetting().TimeZoneFullName;
            }
            catch
            {
                timeZoneFullName = "Singapore Standard Time";
            }

            IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
            TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, siteID);
            MailServerUtil mailsvr = new MailServerUtil();
            WSOrderClient wsOrder = new WSOrderClient();
            Order order = wsOrder.WSGetOrder(orderID, siteID);
            OrderModel orderModel = new OrderModel(order, siteID, timeZoneFullName);
            string bodyMail = "";
            string listProductItem = "";
            string stBody = "";
            string emailCustomer = "";
            StringBuilder resultBuild = new StringBuilder(); ;
            string titleMail = "";

            //------------------
            string storeUrl = "";
            try
            {
                WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
                MerchantSetting[] listmerchants = new MerchantSetting[1];
                listmerchants = clientMerchantSetting.GetAllMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, siteID);
                storeUrl = listmerchants[0].UrlHostOnline;
            }
            catch
            {

            }

            //order.staff = new EmployeeInfor();
            //var staff = GetStaff(VariableConfigController.GetUserID());
            //if (staff == null)
            //{
            //    staff = GetStaff(User.Identity.GetUserId());
            //}
            //if (staff != null)
            //{
            //    order.staff = staff;
            //}
            //else
            //{
            //    var merchant = GetMerchantManagement();
            //    order.staff._id = merchant != null ? (!string.IsNullOrWhiteSpace(merchant.merchantID.ToString()) ? merchant.merchantID.ToString() : "") : "";
            //    order.staff.fullName = merchant != null ? (!string.IsNullOrWhiteSpace(merchant.fullName) ? merchant.fullName : "") : "";
            //}
            order = GetStaffProcessOrder(order);
            //orderModel.onlineShopLink = (!string.IsNullOrWhiteSpace(storeUrl) ? storeUrl : "");
            //------------------
            if (order.orderType == ConstantSmoovs.OrderType.OrderTypeDelivery)
            {
                switch (status)
                {
                    case 1:
                        order.orderStatus = ConstantSmoovs.OrderStatusValueInt.ReadyForCollection;
                        //---------------------------- Update time complete order in Customer

                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        if (order.customerSender != null)
                        {
                            if ((!string.IsNullOrWhiteSpace(order.customerSender._id)) && (!string.IsNullOrWhiteSpace(order.orderCode)))
                            {
                                UpdateCompleteDateOrderInCustomerTable(order.customerSender._id, order.orderCode, order.completedDate);
                            }

                        }

                        //---------------------------- Update time complete order in Customer
                        wsOrder.WSUpdateOrder(order, VariableConfigController.GetBucket());
                        //  orderModel.onlineShopLink = (!string.IsNullOrWhiteSpace(storeUrl) ? storeUrl : "");
                        // orderModel.businessName = GetBusinessNameToSendMail();
                        var bussinesModel = GetMerchantManagement();                            //GetBussinessModel();
                        //orderModel.businessName = bussinesModel.businessName;
                        orderModel.businessName = bussinesModel != null ? (!string.IsNullOrWhiteSpace(bussinesModel.businessName) ? bussinesModel.businessName : "") : "";
                        //-------------------------
                        TemplateEmail emailItem = listEmail.First(r => r.index == ConstantSmoovs.BodyEmail.CustomerConfirmDeliveryOrder);
                        bodyMail = (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "");

                        listProductItem = RenderRazorViewToString("~/Views/Order/ProductItemSendMail.cshtml", orderModel);
                        resultBuild = new StringBuilder(bodyMail);
                        stBody = resultBuild.ToString();
                        resultBuild = ReplaceInformationOfOrder(stBody, orderModel);
                        resultBuild.Replace(ConstantSmoovs.EmailKeyword.ProductOrderDetails, listProductItem);
                        bodyMail = resultBuild.ToString();

                        titleMail = ConstantSmoovs.TitleEmail.ConfirmSelfCollectOrder + orderModel.orderCode;
                        emailCustomer = order.customerSender != null ? (!string.IsNullOrWhiteSpace(order.customerSender.email) ? order.customerSender.email : "") : "";

                        sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, "Merchant1", emailCustomer, titleMail, bodyMail);
                        break;
                    case 5:
                        order.orderStatus = ConstantSmoovs.OrderStatusValueInt.Collected;

                        //---------------------------- Update time complete order in Customer

                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        if (order.customerSender != null && (!string.IsNullOrWhiteSpace(order.customerSender._id)) && (!string.IsNullOrWhiteSpace(order.orderCode)))
                        {
                            UpdateCompleteDateOrderInCustomerTable(order.customerSender._id, order.orderCode, order.completedDate);
                        }
                        //---------------------------- Update time complete order in Customer
                        wsOrder.WSUpdateOrder(order, VariableConfigController.GetBucket());
                        break;

                }
            }
            else if (order.orderType == ConstantSmoovs.OrderType.OrderTypeSelfCollect)
            {
                switch (status)
                {
                    case 1:
                        order.orderStatus = ConstantSmoovs.OrderStatusValueInt.ReadyForDelivery;
                        //---------------------------- Update time complete order in Customer

                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        if (order.customerSender != null)
                        {
                            if ((!string.IsNullOrWhiteSpace(order.customerSender._id)) && (!string.IsNullOrWhiteSpace(order.orderCode)))
                            {
                                UpdateCompleteDateOrderInCustomerTable(order.customerSender._id, order.orderCode, order.completedDate);
                            }

                        }
                        //---------------------------- Update time complete order in Customer
                        wsOrder.WSUpdateOrder(order, VariableConfigController.GetBucket());
                        //  orderModel.onlineShopLink = (!string.IsNullOrWhiteSpace(storeUrl) ? storeUrl : "");
                        //orderModel.businessName = GetBusinessNameToSendMail();
                        var bussinesModel = GetMerchantManagement(); // GetBussinessModel();
                        // orderModel.businessName = bussinesModel.businessName;
                        orderModel.businessName = bussinesModel != null ? (!string.IsNullOrWhiteSpace(bussinesModel.businessName) ? bussinesModel.businessName : "") : "";
                        TemplateEmail emailItem = listEmail.First(r => r.index == ConstantSmoovs.BodyEmail.CustomerConfirmDeliveryOrder);
                        bodyMail = emailItem.body;

                        listProductItem = RenderRazorViewToString("~/Views/Order/ProductItemSendMail.cshtml", orderModel);
                        resultBuild = new StringBuilder(bodyMail);
                        stBody = resultBuild.ToString();
                        resultBuild = ReplaceInformationOfOrder(stBody, orderModel);
                        resultBuild.Replace(ConstantSmoovs.EmailKeyword.ProductOrderDetails, listProductItem);
                        bodyMail = resultBuild.ToString();


                        titleMail = ConstantSmoovs.TitleEmail.ConfirmDeliveryOrder + orderModel.orderCode;
                        emailCustomer = order.customerSender != null ? (!string.IsNullOrWhiteSpace(order.customerSender.email) ? order.customerSender.email : "") : "";
                        sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, "Merchant1", emailCustomer, titleMail, bodyMail);
                        break;
                    case 2:
                        order.orderStatus = ConstantSmoovs.OrderStatusValueInt.OnDelivery;
                        //---------------------------- Update time complete order in Customer

                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        if (order.customerSender != null && (!string.IsNullOrWhiteSpace(order.customerSender._id)) && (!string.IsNullOrWhiteSpace(order.orderCode)))
                        {
                            UpdateCompleteDateOrderInCustomerTable(order.customerSender._id, order.orderCode, order.completedDate);
                        }
                        //---------------------------- Update time complete order in Customer
                        wsOrder.WSUpdateOrder(order, VariableConfigController.GetBucket());
                        // orderModel.onlineShopLink = (!string.IsNullOrWhiteSpace(storeUrl) ? storeUrl : "");
                        orderModel.businessName = GetBusinessNameToSendMail();
                        TemplateEmail emailItem1 = listEmail.First(r => r.index == ConstantSmoovs.BodyEmail.UpdateOrderStatus);
                        bodyMail = emailItem1.body;
                        // bodyMail = RenderRazorViewToStringNoModel("~/Views/Shared/SendMailChangeStatusOrder.cshtml");
                        listProductItem = RenderRazorViewToString("~/Views/Order/ProductItemSendMail.cshtml", orderModel);
                        resultBuild = new StringBuilder(bodyMail);
                        stBody = resultBuild.ToString();
                        resultBuild = ReplaceInformationOfOrder(stBody, orderModel);
                        resultBuild.Replace(ConstantSmoovs.EmailKeyword.ProductOrderDetails, listProductItem);
                        bodyMail = resultBuild.ToString();
                        titleMail = ConstantSmoovs.TitleEmail.UpdateOrderStatus + orderModel.orderCode;

                        emailCustomer = order.customerSender != null ? (!string.IsNullOrWhiteSpace(order.customerSender.email) ? order.customerSender.email : "") : "";
                        sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, "Merchant1", emailCustomer, titleMail, bodyMail);
                        break;
                    case 3:
                        order.orderStatus = ConstantSmoovs.OrderStatusValueInt.Delivered;
                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        if (order.customerSender != null && (!string.IsNullOrWhiteSpace(order.customerSender._id)) && (!string.IsNullOrWhiteSpace(order.orderCode)))
                        {
                            UpdateCompleteDateOrderInCustomerTable(order.customerSender._id, order.orderCode, order.completedDate);
                        }
                        wsOrder.WSUpdateOrder(order, VariableConfigController.GetBucket());
                        break;
                    case 4:
                        order.orderStatus = ConstantSmoovs.OrderStatusValueInt.Received;
                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        if (order.customerSender != null && (!string.IsNullOrWhiteSpace(order.customerSender._id)) && (!string.IsNullOrWhiteSpace(order.orderCode)))
                        {
                            UpdateCompleteDateOrderInCustomerTable(order.customerSender._id, order.orderCode, order.completedDate);
                        }
                        wsOrder.WSUpdateOrder(order, VariableConfigController.GetBucket());
                        break;
                }
            }
            else if (order.orderType == ConstantSmoovs.OrderType.OrderTypeTableOrdering)
            {
                switch (status)
                {
                    case (int)ConstantSmoovs.Enums.TableOrderingStatus.DineInConfirm: // Dine in confirm
                        order.orderStatus = ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.OrderStatus.DineInConfirm).Key;
                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        if (order.customerSender != null && (!string.IsNullOrWhiteSpace(order.customerSender._id)) && (!string.IsNullOrWhiteSpace(order.orderCode)))
                        {
                            UpdateCompleteDateOrderInCustomerTable(order.customerSender._id, order.orderCode, order.completedDate);
                        }
                        wsOrder.WSUpdateOrder(order, VariableConfigController.GetBucket());
                        // ---- SEND MAIL ---- //
                        //orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        //mailsvr.SendMail(2, orderModel.businessName, "anhhuydng@gmail.com", "Order Confirmation", "<p>Hi " + orderModel.customerFirstNameSender + ",</p><p>Your order for " + order.station.stationName + " has been confirmed.</p><p>Sincerely,</p><p>" + orderModel.businessName + "</p>");
                        SendMailNotifyTableOrdering(orderModel.businessName, orderModel.CustomerReceiveFirstName, orderModel.CustomerReceiveEmail, ConstantSmoovs.MailTypeOrdering.OrderingConfirm, order.station.stationName, "");

                        break;
                    case (int)ConstantSmoovs.Enums.TableOrderingStatus.DineInServing: // Dine in serving
                        order.orderStatus = ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.OrderStatus.DineInServing).Key;
                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        if (order.customerSender != null && (!string.IsNullOrWhiteSpace(order.customerSender._id)) && (!string.IsNullOrWhiteSpace(order.orderCode)))
                        {
                            UpdateCompleteDateOrderInCustomerTable(order.customerSender._id, order.orderCode, order.completedDate);
                        }
                        wsOrder.WSUpdateOrder(order, VariableConfigController.GetBucket());
                        // ---- SEND MAIL ---- //
                        //orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        //mailsvr.SendMail(2, orderModel.businessName, "anhhuydng@gmail.com", "Order ready", "<span>Hi " + orderModel.customerFirstNameSender + ",</span><br /><br /><span>Your order for " + order.station.stationName + " is ready.</span><br /><br /><span>Sincerely,</span><br /><span>" + orderModel.businessName + "</span>");
                        SendMailNotifyTableOrdering(orderModel.businessName, orderModel.CustomerReceiveFirstName, orderModel.CustomerReceiveEmail, ConstantSmoovs.MailTypeOrdering.OrderingReady, order.station.stationName, "");
                        break;
                    case (int)ConstantSmoovs.Enums.TableOrderingStatus.DineInDone: // Dine in done
                        order.orderStatus = ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.OrderStatus.DineInDone).Key;
                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        orderModel = new OrderModel(order, siteID, timeZoneFullName);
                        if (order.customerSender != null && (!string.IsNullOrWhiteSpace(order.customerSender._id)) && (!string.IsNullOrWhiteSpace(order.orderCode)))
                        {
                            UpdateCompleteDateOrderInCustomerTable(order.customerSender._id, order.orderCode, order.completedDate);
                        }
                        wsOrder.WSUpdateOrder(order, VariableConfigController.GetBucket());
                        break;
                }
            }
            else
            {

            }

            clientEmail.Close();
            wsOrder.Close();
            return PartialView("~/Views/Order/ViewPartialAll.cshtml", orderModel);
            //return 1;


        }
        /// <summary>
        /// Sends the email order.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="merchantName">Name of the merchant.</param>
        /// <param name="email">The email.</param>
        /// <param name="titleEmail">The title email.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/24/2015-3:37 PM</datetime>
        public bool sendEmailOrder(int type, string merchantName, string email, string titleEmail, string body)
        {
            MailServerUtil sendMail = new MailServerUtil();
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            var merchantInfor = GetMerchantManagement();
            merchantName = merchantInfor != null ? merchantInfor.businessName : "";
            bool isSend = false;
            if (merchantInfor != null)
            {
                MailServerModel mailMerchant = new MailServerModel();
                mailMerchant = mailConfig.GetConfigMailServer(merchantInfor.merchantID);
                if (mailMerchant != null)
                {
                    isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body, merchantInfor.merchantID) : false;
                }
                else
                {
                    isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
                }
            }
            else
            {
                isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
            }

            return isSend;


        }

        /// <summary>
        /// Replaces the information of order.
        /// </summary>
        /// <param name="bodyMail">The body mail.</param>
        /// <param name="orderModel">The order model.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/24/2015-3:37 PM</datetime>
        public StringBuilder ReplaceInformationOfOrder(string bodyMail, OrderModel orderModel)
        {

            StringBuilder result = new StringBuilder(bodyMail.ToString());
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, orderModel.customerFirstNameSender);
            var bussinesModel = GetMerchantManagement(); //GetBussinessModel();
            string businessName = bussinesModel != null ? (!string.IsNullOrWhiteSpace(bussinesModel.businessName) ? bussinesModel.businessName : "") : "";
            result.Replace(ConstantSmoovs.EmailKeyword.BusinessName, businessName);
            result.Replace(ConstantSmoovs.EmailKeyword.SupportEmailOfMerchant, orderModel.emailMerchantSupport);
            result.Replace(ConstantSmoovs.EmailKeyword.OrderNumber, orderModel.orderCode);
            result.Replace(ConstantSmoovs.EmailKeyword.OrderDay, orderModel.createDateOrderShow);
            result.Replace(ConstantSmoovs.EmailKeyword.OrderStatusOfCustomer, orderModel.OrderStatusDisplayForCustomer);

            result.Replace(ConstantSmoovs.EmailKeyword.OrderTypeInformation, orderModel.orderType == 1 ? ConstantSmoovs.EmailKeyword.CollectionTo : ConstantSmoovs.EmailKeyword.ShipTo);
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameLastNameBill, orderModel.CustomerSenderFullName);
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameLastNameReceive, orderModel.CustomerReceiverFullName);

            result.Replace(ConstantSmoovs.EmailKeyword.AddressBill, orderModel.CustomerSenderAddress);
            result.Replace(ConstantSmoovs.EmailKeyword.AddressReceive, orderModel.addressShipping);
            result.Replace(ConstantSmoovs.EmailKeyword.PhoneBill, orderModel.customerSender != null ? (!string.IsNullOrWhiteSpace(orderModel.customerSender.phone) ? orderModel.customerSender.phone : "") : "");
            result.Replace(ConstantSmoovs.EmailKeyword.PhoneReceive, orderModel.CustomerReceivePhone);
            //result.Replace(ConstantSmoovs.EmailKeyword.PhoneReceive,orderModel.CustomerReceivePhone);
            //result.Replace(Constants.EmailKeyword.PhoneReceive, orderModel.customerReceiver != null ? (!string.IsNullOrWhiteSpace(orderModel.customerReceiver.phone) ? orderModel.customerReceiver.phone : "") : "");
            result.Replace(ConstantSmoovs.EmailKeyword.EmailBill, orderModel.customerSender != null ? (!string.IsNullOrWhiteSpace(orderModel.customerSender.email) ? orderModel.customerSender.email : "") : "");
            result.Replace(ConstantSmoovs.EmailKeyword.EmailReceive, orderModel.CustomerReceiveEmail);
            // result.Replace(Constants.EmailKeyword.EmailReceive, orderModel.customerReceiver != null ? (!string.IsNullOrWhiteSpace(orderModel.customerReceiver.email) ? orderModel.customerReceiver.email : "") : "");

            result.Replace(ConstantSmoovs.EmailKeyword.OnlineshopLink, VariableConfigController.GetHttpAlias());
            //result.Replace(ConstantSmoovs.EmailKeyword.OnlineshopLink, orderModel.onlineShopLink);

            result.Replace(ConstantSmoovs.EmailKeyword.OrderCode, orderModel.orderCode);
            result.Replace(ConstantSmoovs.EmailKeyword.OrderStatusDisplayForCustomer, orderModel.OrderStatusDisplayForCustomer);
            return result;
        }

        public void UpdateCompleteDateOrderInCustomerTable(string idCustomer, string orderCode, DateTime dateCompletd)
        {
            WSCustomerClient clientCustomer = new WSCustomerClient();
            Customer customerItem = clientCustomer.WSDetailCustomer(idCustomer, "smoov");
            if (customerItem != null && customerItem.listOrder != null)
            {

                foreach (var itemOrder in customerItem.listOrder.Where(r => r.orderCode == orderCode))
                {
                    itemOrder.dateOrder = dateCompletd;
                }
                clientCustomer.WSUpdateCustomer(customerItem, "smoov");
            }


            clientCustomer.Close();
        }
        /// <summary>
        /// Gets the bussiness name to send mail.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/24/2015-3:37 PM</datetime>
        public string GetBusinessNameToSendMail()
        {

            IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
            var modelMerchant = clientMerchant.WSGetMerchantManagementByUserId(VariableConfigController.GetUserID());
            clientMerchant.Close();
            return modelMerchant != null ? (!string.IsNullOrWhiteSpace(modelMerchant.businessName) ? modelMerchant.businessName : "") : "";

        }


        /// <summary>
        /// Cancels the order.
        /// </summary>
        /// <param name="orderID">The order identifier.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/24/2015-3:49 PM</datetime>
        /// 
        [HttpPost]
        public ActionResult CancelOrder(string orderID)
        {
            ViewBag.Class = "order";
            string siteID = "";
            MerchantSetting merchantSetting = GetMerchantSetting();

            string timeZoneFullName = string.Empty;
            try
            {
                timeZoneFullName = GetGeneralSetting().TimeZoneFullName;
            }
            catch
            {
                timeZoneFullName = "Singapore Standard Time";
            }

            siteID = VariableConfigController.GetBucket();
            WSOrderClient client = new WSOrderClient();
            Order order = client.WSGetOrder(orderID, siteID);
            order.orderStatus = ConstantSmoovs.OrderStatusValueInt.Canceled;
            order.isCancelRequest = false;
            order.completedDate = DateTime.UtcNow;
            order.modifiedDate = order.completedDate;
            order = GetStaffProcessOrder(order);

            client.WSUpdateOrder(order, siteID);
            //send mail cancel
            string viewToRender = "~/Views/MailTemplate/CancelOrder.cshtml";
            SendMailCancelRejectOrder(merchantSetting, order, viewToRender, ConstantSmoovs.BodyEmail.CustomerCancelOrder, ConstantSmoovs.TitleEmail.CancelOrderNo);
            //

            OrderModel orderModel = new OrderModel(order, siteID, timeZoneFullName);
            client.Close();

            return PartialView("~/Views/Order/ViewPartialAll.cshtml", orderModel);
        }

        private Order GetStaffProcessOrder(Order order)
        {
            order.staff = new EmployeeInfor();
            var staff = GetStaff(User.Identity.GetUserId());
            if (staff == null)
            {
                staff = GetStaff(User.Identity.GetUserId());
            }
            if (staff != null)
            {
                order.staff = staff;
            }
            else
            {
                var merchant = GetMerchantManagement();
                order.staff._id = merchant != null ? (!string.IsNullOrWhiteSpace(merchant.merchantID.ToString()) ? merchant.merchantID.ToString() : "") : "";
                order.staff.fullName = merchant != null ? (!string.IsNullOrWhiteSpace(merchant.fullName) ? merchant.fullName : "") : "";
            }
            return order;
        }

        private void SendMailCancelRejectOrder(MerchantSetting merchantSetting, Order order, string viewToRender, int indexEmail, string titleEmail)
        {
            string bodyMail = "";
            StringBuilder resultBuild;
            //TemplateEmail emailItem = GetTemplateEmail(indexEmail);
            //bodyMail = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
            //if (string.IsNullOrWhiteSpace(bodyMail))
            //{

            bodyMail = RenderRazorViewToStringNoModel(viewToRender);
            //}

            resultBuild = new StringBuilder(bodyMail);
            string storeUrl = "";
            var bussinesModel = GetMerchantManagement(); //GetBussinessModel();
            storeUrl = merchantSetting.UrlHostOnline;
            string firstNameOfCustomer = FirstNameOfCustomerSender(order);
            string businessName = "";
            businessName = bussinesModel != null ? (!string.IsNullOrWhiteSpace(bussinesModel.businessName) ? bussinesModel.businessName : "") : "";
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, firstNameOfCustomer);
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.OrderNumber, !string.IsNullOrWhiteSpace(order.orderCode) ? order.orderCode : "");
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.OnlineshopLink, !string.IsNullOrWhiteSpace(VariableConfigController.GetHttpAlias()) ? VariableConfigController.GetHttpAlias() : "");
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.BusinessName, businessName);
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.ReasonOrder, order.refundInfo != null ? (!string.IsNullOrWhiteSpace(order.refundInfo.reasonRefund) ? order.refundInfo.reasonRefund : "") : "");
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.Table, order.station.stationName);
            //resultBuild.Replace(ConstantSmoovs.EmailKeyword.ReasonOrder, "");
            bodyMail = resultBuild.ToString();

            //bool isSend = sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, businessName, order.customerSender.email, titleEmail + order.orderCode, bodyMail);
            bool isSend = sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, businessName, order.customerSender.email, titleEmail , bodyMail);
        }

        private static string FirstNameOfCustomerSender(Order order)
        {
            string firstNameOfCustomer = "";
            firstNameOfCustomer = order.customerSender != null ? (!string.IsNullOrWhiteSpace(order.customerSender.firstName) ? order.customerSender.firstName
                    : (!string.IsNullOrWhiteSpace(order.customerSender.fullName) ? order.customerSender.fullName
                    : (!string.IsNullOrWhiteSpace(order.customerSender.lastName) ? order.customerSender.lastName
                    : "")))
                    : "";
            return firstNameOfCustomer;
        }

        /// <summary>
        /// Refunds the order.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/24/2015-3:47 PM</datetime>
        [HttpPost]
        public ActionResult RefundOrder(string orderID, string reason, string type)
        {

            ViewBag.Class = "order";
            string siteID = "";
            var merchantSetting = GetMerchantSetting();

            string timeZoneFullName = string.Empty;
            try
            {
                timeZoneFullName = GetGeneralSetting().TimeZoneFullName;
            }
            catch
            {
                timeZoneFullName = "Singapore Standard Time";
            }

            siteID = VariableConfigController.GetBucket();
            WSCheckoutSettingClient clientCheckout = new WSCheckoutSettingClient();
            var smoovpay = clientCheckout.GetDetailAccountSmoovpay(Common.ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAccountSmoovPay, siteID);
            clientCheckout.Close();

            WSOrderClient client = new WSOrderClient();
            Order order = client.WSGetOrder(orderID, siteID);

            if (!string.IsNullOrEmpty(order.referenceCode))
            {
                var authoration = CommonActionSmoovPay.GetAuthorization(siteID);
                if (!string.IsNullOrEmpty(authoration))
                {
                    var statusRefund = CommonActionSmoovPay.CallApiRefundOrder(authoration, order.transactionID, reason, order.grandTotal.ToString());
                    if (statusRefund.status == ConstantSmoovs.Enums.API_REFUND_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_REFUND_STATUS.Refunded).Key)
                    {
                        order.orderStatus = ConstantSmoovs.OrderStatusValueInt.Refunded;
                        order.paymentStatus = ConstantSmoovs.PaymentStatusIndex.Refunded;
                        order.isCancelRequest = false;
                        order.completedDate = DateTime.UtcNow;
                        order.modifiedDate = order.completedDate;
                        order = GetStaffProcessOrder(order);
                        order.refundInfo = new RefundInfor();
                        order.refundInfo.reasonRefund = reason;
                        order.refundInfo.refundDate = DateTime.UtcNow;

                        if (order.staff != null)
                        {
                            order.refundInfo.staffID = !string.IsNullOrWhiteSpace(order.staff._id) ? order.staff._id : "";
                            order.refundInfo.staffFullName = !string.IsNullOrWhiteSpace(order.staff.fullName) ? order.staff.fullName : "";
                        }
                        else
                        {
                            var merchant = GetMerchantManagement();
                            order.refundInfo.staffID = merchant != null ? (!string.IsNullOrWhiteSpace(merchant.merchantID.ToString()) ? merchant.merchantID.ToString() : "") : "";
                            order.refundInfo.staffFullName = merchant != null ? (!string.IsNullOrWhiteSpace(merchant.fullName) ? merchant.fullName : "") : "";
                        }
                        order.refundInfo.arrayPayment = order.arrayPayment;
                        order.refundInfo.typeRefund = 0; // not define

                        client.WSUpdateOrder(order, siteID);
                        if (type == "refund")
                        {
                            string viewToRender = "~/Views/MailTemplate/RefundOrder.cshtml";
                            //SendMailCancelRejectOrder(merchantSetting, order, viewToRender, ConstantSmoovs.BodyEmail.CustomerRefundOrder, ConstantSmoovs.TitleEmail.RefundOrderNo);
                            SendMailCancelRejectOrder(merchantSetting, order, viewToRender, ConstantSmoovs.BodyEmail.CustomerRefundOrder, ConstantSmoovs.TitleEmail.RefundOrder);
                        }
                        else
                        {
                            string viewToRender = "~/Views/MailTemplate/RejectOrder.cshtml";
                            SendMailCancelRejectOrder(merchantSetting, order, viewToRender, ConstantSmoovs.BodyEmail.CustomerRejectOrder, ConstantSmoovs.TitleEmail.OrderReject);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", statusRefund.message);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Please login account smoovpay in merchant panel");
                }
            }
            OrderModel orderModel = new OrderModel(order, siteID, timeZoneFullName);
            client.Close();
            return PartialView("~/Views/Order/ViewPartialAll.cshtml", orderModel);
        }

        //private Employee GetInformationStaffProcessOrder(Order order)
        //{
        //    order.staff = new Employee();
        //    var staff = GetStaff(VariableConfigController.GetUserID());
        //    if (staff != null)
        //    {
        //        order.staff = staff;
        //    }
        //    else
        //    {
        //        var merchant = GetMerchantManagement();
        //        order.staff._id = merchant != null ? (!string.IsNullOrWhiteSpace(merchant.merchantID.ToString()) ? merchant.merchantID.ToString() : "") : "";
        //        order.staff.fullName = merchant != null ? (!string.IsNullOrWhiteSpace(merchant.fullName) ? merchant.fullName : "") : "";
        //    }

        //    return staff;
        //}

        /// <summary>
        /// Approves the cancel order.
        /// </summary>
        /// <param name="orderID">The order identifier.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/24/2015-5:13 PM</datetime>
        public ActionResult ApproveCancelOrder(string orderID)
        {
            ViewBag.Class = "order";
            string siteID = "";
            var merchantSetting = GetMerchantSetting();

            string timeZoneFullName = string.Empty;
            try
            {
                timeZoneFullName = GetGeneralSetting().TimeZoneFullName;
            }
            catch
            {
                timeZoneFullName = "Singapore Standard Time";
            }

            siteID = VariableConfigController.GetBucket();
            WSOrderClient client = new WSOrderClient();
            Order order = client.WSGetOrder(orderID, siteID);
            order.orderStatus = ConstantSmoovs.OrderStatusValueInt.Canceled;

            order.completedDate = DateTime.UtcNow;
            order.modifiedDate = order.completedDate;
            order = GetStaffProcessOrder(order);

            client.WSUpdateOrder(order, siteID);
            //send mail cancel
            string viewToRender = "~/Views/MailTemplate/CancelOrder.cshtml";
            SendMailCancelRejectOrder(merchantSetting, order, viewToRender, ConstantSmoovs.BodyEmail.CustomerCancelOrder, ConstantSmoovs.TitleEmail.CancelOrderNo);
            //

            OrderModel orderModel = new OrderModel(order, siteID, timeZoneFullName);
            client.Close();

            return PartialView("~/Views/Order/ViewPartialAll.cshtml", orderModel);
        }

        /// <summary>
        /// Rejects the cancel order.
        /// </summary>
        /// <param name="orderID">The order identifier.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/24/2015-5:27 PM</datetime>
        public ActionResult RejectCancelOrder(string orderID)
        {
            ViewBag.Class = "order";

            string siteID = "";
            var merchantSetting = GetMerchantSetting();
            siteID = VariableConfigController.GetBucket();

            string timeZoneFullName = string.Empty;
            try
            {
                timeZoneFullName = GetGeneralSetting().TimeZoneFullName;
            }
            catch
            {
                timeZoneFullName = "Singapore Standard Time";
            }

            WSOrderClient client = new WSOrderClient();
            Order order = client.WSGetOrder(orderID, siteID);
            order.isCancelRequest = false;
            order.completedDate = DateTime.UtcNow;
            order.modifiedDate = order.completedDate;
            order = GetStaffProcessOrder(order);
            client.WSUpdateOrder(order, siteID);

            OrderModel orderModel = new OrderModel(order, siteID, timeZoneFullName);
            client.Close();
            string viewToRender = "~/Views/MailTemplate/RejectOrder.cshtml";
            SendMailCancelRejectOrder(merchantSetting, order, viewToRender, ConstantSmoovs.BodyEmail.CustomerRejectOrder, ConstantSmoovs.TitleEmail.RejectCancelOrderNo);
            return PartialView("~/Views/Order/ViewPartialAll.cshtml", orderModel);
        }

        /// <summary>
        /// Gets the bussiness model.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-11:22 AM</datetime>
        //public MerchantManagementModel GetBussinessModel()
        //{

        //    IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
        //    var modelMerchant = clientMerchant.WSGetMerchantManagementBySiteId(VariableConfigController.GetBucket());
        //    clientMerchant.Close();
        //    return modelMerchant;

        //}

        /// <summary>
        /// Gets the merchant setting.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-11:22 AM</datetime>
        public MerchantSetting GetMerchantSetting()
        {

            MerchantSetting merchantSetting = new MerchantSetting();
            try
            {
                WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
                merchantSetting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
                clientMerchantSetting.Close();
            }
            catch
            {

            }

            return merchantSetting;
        }

        /// <summary>
        /// Gets the template email.
        /// </summary>
        /// <param name="indexEmail">The index email.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-11:22 AM</datetime>
        public TemplateEmail GetTemplateEmail(int indexEmail)
        {
            try
            {
                IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
                TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, VariableConfigController.GetBucket());
                TemplateEmail emailItem = listEmail.FirstOrDefault(r => r.index == indexEmail);
                clientEmail.Close();

                return emailItem;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the staff.
        /// </summary>
        /// <param name="idStaff">The identifier staff.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-11:22 AM</datetime>
        public EmployeeInfor GetStaff(string idStaff)
        {
            WSEmployeeClient client = new WSEmployeeClient();
            var employee = client.WSDetailEmployee(idStaff, VariableConfigController.GetBucket());
            client.Close();
            if (employee != null)
            {
                var employeeInfor = Mapper.Map<Employee, EmployeeInfor>(employee);
                return employeeInfor;
            }

            return null;
        }
        /// <summary>
        /// Gets the merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-2:48 PM</datetime>
        public MerchantManagementModel GetMerchantManagement()
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            string siteID = VariableConfigController.GetBusinessName();
            var model = client.WSGetMerchantManagementByBusinessName(siteID);
            client.Close();
            return model;
        }

        /// <summary>
        /// Sends the mail notify table ordering.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="customerName">Name of the customer.</param>
        /// <param name="emailCustomer">The email customer.</param>
        /// <param name="typeEmail">The type email.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="message">The message.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>4/7/2015-6:40 PM</datetime>
        public void SendMailNotifyTableOrdering(string businessName, string customerName, string emailCustomer, string typeEmail, string tableName, string message)
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var merchant = client.WSGetMerchantManagementByBusinessName(businessName);
            client.Close();
            string bodyMail = "";
            string titleMail = "";
            switch (typeEmail)
            {
                case ConstantSmoovs.MailTypeOrdering.OrderingConfirm:
                    titleMail = ConstantSmoovs.TitleEmail.OrderConfirmation;
                    bodyMail = RenderRazorViewToStringNoModel("~/Views/MailTemplate/TableOrderingConfirm.cshtml");
                    break;
                case ConstantSmoovs.MailTypeOrdering.OrderingReject:
                    titleMail = ConstantSmoovs.TitleEmail.OrderReject;
                    bodyMail = RenderRazorViewToStringNoModel("~/Views/MailTemplate/TableOrderingReject.cshtml");
                    break;
                case ConstantSmoovs.MailTypeOrdering.OrderingCancel:
                    titleMail = ConstantSmoovs.TitleEmail.OrderCancel;
                    bodyMail = RenderRazorViewToStringNoModel("~/Views/MailTemplate/TableOrderingCancel.cshtml");
                    break;
                case ConstantSmoovs.MailTypeOrdering.OrderingReady:
                    titleMail = ConstantSmoovs.TitleEmail.OrderReady;
                    bodyMail = RenderRazorViewToStringNoModel("~/Views/MailTemplate/TableOrderingReady.cshtml");
                    break;
                case ConstantSmoovs.MailTypeOrdering.OrderingAmend:
                    titleMail = ConstantSmoovs.TitleEmail.OrderAmend;
                    bodyMail = RenderRazorViewToStringNoModel("~/Views/MailTemplate/TableOrderingAmend.cshtml");
                    break;
                default:
                    break;
            }
            StringBuilder result = new StringBuilder(bodyMail.ToString());
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, customerName);
            result.Replace(ConstantSmoovs.EmailKeyword.TableName, tableName);
            result.Replace(ConstantSmoovs.EmailKeyword.BusinessName, businessName);
            result.Replace(ConstantSmoovs.EmailKeyword.OrderAmendDetail, message);
            bodyMail = result.ToString();
            sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, businessName, emailCustomer, titleMail, bodyMail);
        }

    }
}