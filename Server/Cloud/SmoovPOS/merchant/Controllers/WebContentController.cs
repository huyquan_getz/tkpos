﻿using Com.SmoovPOS.Common.Controllers;
using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Controllers;
using SmoovPOS.Utility.WSMerchantSettingReference;
using SmoovPOS.Utility.WSWebContentReference;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Com.UI.Controllers
{
    public class WebContentController : BaseController
    {
        [Authorize]
        //
        // GET: /WebContent/
        public ActionResult AboutUs()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "content";
            ViewBag.ClassSub = "aboutus";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();

            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent,ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.channels = new String[] { "server" };
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.slideImage = slideImage;

                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }


            client.Close();

            return View(webContent);
        }

        [HttpPost]

        [ValidateInput(false)]

        public ActionResult AboutUs(WebContent model)
        {   
            ViewBag.ClassSub = "aboutus";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                webContent.aboutUs = model.aboutUs;
                webContent.createdDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.modifiedDate = DateTime.Today;
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();

                return View(webContent);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [Authorize]
        // ContactUs
        public ActionResult ContactUs()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.ClassSub = "contactus";
            ViewBag.Class = "content";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            //webContent._id = Guid.NewGuid().ToString();
            //webContent.aboutUs = "About Us";
            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.slideImage = slideImage;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };

                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }

            client.Close();
            return View(webContent);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ContactUs(WebContent model)
        {
            ViewBag.ClassSub = "contactus";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                webContent.contactUs = model.contactUs;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();

                return View(webContent);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [Authorize]
        public ActionResult FAQs()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.ClassSub = "faqs";
            ViewBag.Class = "content";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            //webContent._id = Guid.NewGuid().ToString();
            //webContent.aboutUs = "About Us";
            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.slideImage = slideImage;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };

                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }

            client.Close();
            return View(webContent);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FAQs(WebContent model)
        {
            ViewBag.ClassSub = "faqs";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                webContent.faqs = model.faqs;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();

                return View(webContent);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [Authorize]
        public ActionResult ShippingInfo()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.ClassSub = "shippinginfo";
            ViewBag.Class = "content";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.slideImage = slideImage;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }

            client.Close();

            return View(webContent);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ShippingInfo(WebContent model)
        {
            ViewBag.ClassSub = "shippinginfo";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                webContent.shippingInformation = model.shippingInformation;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();

                return View(webContent);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [Authorize]
        // Return & Exchange
        public ActionResult ReturnExchange()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.ClassSub = "returnexchange";
            ViewBag.Class = "content";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.slideImage = slideImage;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }
            client.Close();

            return View(webContent);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ReturnExchange(WebContent model)
        {
            ViewBag.ClassSub = "returnexchange";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                webContent.returnAndExchange = model.returnAndExchange;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();

                return View(webContent);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [Authorize]
        // Privacy Policy
        public ActionResult PrivacyPolicy()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.ClassSub = "privacypolicy";
            ViewBag.Class = "content";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.slideImage = slideImage;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }

            client.Close();

            return View(webContent);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PrivacyPolicy(WebContent model)
        {
            ViewBag.ClassSub = "privacypolicy";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                webContent.privacyPolicy = model.privacyPolicy;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();

                return View(webContent);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [Authorize]
        // TermConditions
        public ActionResult TermConditions()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.ClassSub = "termconditions";
            ViewBag.Class = "content";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.slideImage = slideImage;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }

            client.Close();

            return View(webContent);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TermConditions(WebContent model)
        {
            ViewBag.ClassSub = "termconditions";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                webContent.termAndCondition = model.termAndCondition;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();

                return View(webContent);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [Authorize]
        // Social Network
        public ActionResult SocialNetwork()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.ClassSub = "socialnetwork";
            ViewBag.Class = "content";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.slideImage = slideImage;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }

            client.Close();

            return View(webContent);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialNetwork(WebContent model, FormCollection collection)
        {
            ViewBag.ClassSub = "socialnetwork";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                SocialNetwork socialNetwork = new SocialNetwork();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                socialNetwork.facebook = collection["facebook"];
                socialNetwork.googlePlus = collection["google"];
                socialNetwork.twitter = collection["twitter"];
                socialNetwork.youtube = collection["youtube"];
                webContent.socialNetworkLink = socialNetwork;
                //webContent.aboutUs = model.termAndCondition;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();

                return View(webContent);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        // LogoInageSlide
        public ActionResult LogoImageSlide()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.ClassSub = "logoimageslide";
            ViewBag.Class = "content";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.slideImage = slideImage;
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }

            client.Close();
            return View(webContent);

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LogoImageSlide(WebContent model, FormCollection collection)
        {
            ViewBag.ClassSub = "logoimageslide";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                //webContent.logo = collection["logoImg"];
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();
                return View(webContent);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }
        // Theme
        public ActionResult Theme()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.ClassSub = "theme";
            ViewBag.Class = "content";
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            IEnumerable<WebContent> list = client.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, VariableConfigController.GetBucket());
            if (list.Count() == 0)
            {
                webContent._id = Guid.NewGuid().ToString();
                SocialNetwork socialNetwork = new SocialNetwork();

                socialNetwork.facebook = "";
                socialNetwork.googlePlus = "";
                socialNetwork.twitter = "";
                socialNetwork.youtube = "";
                webContent.socialNetworkLink = socialNetwork;
                List<SlideImage> slideImage = new List<SlideImage>();
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.slideImage = slideImage;
                webContent.channels = new String[] { "server" };

                int statusCode = client.WSCreateWebContent(webContent, VariableConfigController.GetBucket());

            }
            else
            {
                var webC = list.FirstOrDefault();
                webContent = client.WSDetailWebContent(webC._id, VariableConfigController.GetBucket());
            }

            webContent.PrimarySiteAlias = VariableConfigController.GetHttpAlias();

            client.Close();

            return View(webContent);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Theme(FormCollection collection, WebContent model)
        {
            ViewBag.ClassSub = "theme";
            ViewBag.Class = "content";
            if (Request.IsAuthenticated)
            {
                WSWebContentClient client = new WSWebContentClient();
                WebContent webContent = new WebContent();
                webContent = client.WSDetailWebContent(model._id, VariableConfigController.GetBucket());
                webContent.theme = collection["theme"];
                webContent.createdDate = DateTime.Today;
                webContent.modifiedDate = DateTime.Today;
                webContent.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                webContent.channels = new String[] { "server" };
                int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                client.Close();
                return RedirectToAction("Theme", "WebContent");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        // menu left

        public string DeleteLogoImage()
        {
            NameValueCollection nvc = Request.Form;
            //string deleIcon = "https://s3-ap-southeast-1.amazonaws.com/smoovturnkey/delete-ico.png";

            ViewBag.Id = nvc["_id"];
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            webContent = client.WSDetailWebContent(nvc["_id"], VariableConfigController.GetBucket());
            webContent.logo = "";
            webContent.createdDate = DateTime.Today;
            webContent.modifiedDate = DateTime.Today;
            webContent.channels = new String[] { "server" };
            int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
            client.Close();
            return "success";

        }
       
        public string UploadSlideImage()
        {
            NameValueCollection nvc = Request.Form;
            string priority = "u1";
            StringBuilder builder = new StringBuilder();
            string savedFileName = "";
            string linkImageS3 = "";
            int random = Guid.NewGuid().GetHashCode();
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase photo = Request.Files[i] as HttpPostedFileBase;

                savedFileName = Path.Combine(Server.MapPath("~/Content/Upload/"), random.ToString() + "_" + Path.GetFileName(photo.FileName));
                photo.SaveAs(savedFileName);

                String path = savedFileName.Replace("\\", "/").Replace(" ", "");
                PhotosController ph = new PhotosController();
                linkImageS3 = ph.UploadToS3Amazon(path);
                System.IO.File.Delete(path);


                string html1 = "", html2 = "", html3 = "";
                html1 = "<div id=\"" + "group-image_" + priority + "\">";
                html2 = "<img id=\"thumbnil\" style=\"width:400px;height:200px;margin-top:10px;\" src='" + linkImageS3 + "' class=\"img-rounded-slide\"/>";
                html3 = "<input id=\"urlimage\" type=\"hidden\" value='" + linkImageS3 + "' />";

                builder.Append(html1).Append(html2).Append(html3);
            }

            return builder.ToString();

        }
        public string UploadSlideImageEdit()
        {
            NameValueCollection nvc = Request.Form;
            StringBuilder builder = new StringBuilder();
            string savedFileName = "";
            string linkImageS3 = "";
            int random = Guid.NewGuid().GetHashCode();
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase photo = Request.Files[i] as HttpPostedFileBase;

                savedFileName = Path.Combine(Server.MapPath("~/Content/Upload/"), random.ToString() + "_" + Path.GetFileName(photo.FileName));
                photo.SaveAs(savedFileName);

                String path = savedFileName.Replace("\\", "/").Replace(" ", "");
                PhotosController ph = new PhotosController();
                linkImageS3 = ph.UploadToS3Amazon(path);
                System.IO.File.Delete(path);


                string html1 = "", html2 = "", html3 = "";
                html1 = "<div id=\"group-image_u1\"/>";
                html2 = "<img id=\"thumbnil_u1\" style=\"width:400px;height:200px;margin-top:10px;\" src='" + linkImageS3 + "' class=\"img-rounded-slide\"/>";
                html3 = "<input id=\"urlimage_Edit\" type=\"hidden\" value='" + linkImageS3 + "' />";

                builder.Append(html1).Append(html2).Append(html3);
            }

            return builder.ToString();

        }

        public string SaveImageSlide()
        {
            StringBuilder builder = new StringBuilder();
            NameValueCollection nvc = Request.Form;
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            webContent = client.WSDetailWebContent(nvc["_id"], VariableConfigController.GetBucket());
            int priority = 0;
            if (!string.IsNullOrWhiteSpace(nvc["_id"]))
            {
                try
                {
                    priority = Int32.Parse(nvc["priority"]);
                }
                catch { }
            }

            List<SlideImage> listSlideImage = new List<SlideImage>();
            listSlideImage = webContent.slideImage;
            if (priority != 0)
            {
                foreach (var slide in listSlideImage)
                {
                    if (slide.priority == priority)
                    {

                        slide.urlImage = nvc["urlLink"];
                        slide.targetLink = nvc["targetLink"];
                        break;
                    }
                }
            }
            else
            {
                SlideImage slideImage = new SlideImage();
                slideImage.urlImage = nvc["urlLink"];
                slideImage.priority = webContent.slideImage.Count() + 1;
                slideImage.targetLink = nvc["targetLink"];
                listSlideImage.Add(slideImage);
                webContent.slideImage = listSlideImage;
            }
            int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());

            foreach (var slide in listSlideImage)
            {
                StringBuilder builderSub = new StringBuilder();
                string html1 = "", html2 = "", html3 = "", html4 = "";
                html1 = "<div id=\"" + "group-image_" + slide.priority + "> </div>";
                html4 = "<label > " + slide.priority + " </label>";
                html2 = "<img id=\"thumbnil\" style=\"width:200px;height:100px;margin-top:10px;\" src='" + slide.urlImage + "' class=\"img-rounded-slide\"/>";
                html3 = "<label style=\"width:500px;\">" + slide.targetLink + " </label><div style=\"display:inline-block;\"><button class=\"editImage\" onclick=\"editImageSlide(" + slide.priority + ")\">Edit </button>" +
               " <button class=\"tnDeleteImage\" onclick=\"deleteImageSlide(" + slide.priority + ")\"> <img src=\"https://s3-ap-southeast-1.amazonaws.com/smoovturnkey/delete-ico.png\" width=\"15\" height=\"15\" /></button></div>";
                builderSub.Append(html1).Append(html4).Append(html2).Append(html3);
                builder.Append(builderSub);
            }
            client.Close();

            return builder.ToString();

        }

        public int DeleteImageSlide(string id, string priority)
        {
            // NameValueCollection nvc = Request.Form;            
            WSWebContentClient client = new WSWebContentClient();
            WebContent webContent = new WebContent();
            webContent = client.WSDetailWebContent(id, VariableConfigController.GetBucket());
            if (webContent != null)
            {
                //List<SlideImage> listSlideImage = new List<SlideImage>();
                //listSlideImage = webContent.slideImage;
                foreach (SlideImage slide in webContent.slideImage)
                {
                    if (slide.priority == Int32.Parse(priority))
                    {
                        // ViewBag.Id = slide;
                        webContent.slideImage.Remove(slide);
                        // ViewBag.webContent = webContent.slideImage;
                        int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                        client.Close();
                        return 1;
                    }
                }


            }
            client.Close();
            return 0;

        }

    }
}