﻿using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class InstructionComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateInstruction(Instruction instruction, string bucket)
        {
            instruction.bucket = bucket;
            InstructionDAC serviceShippingDAC = new InstructionDAC(bucketSmoovPOS);
            int statusCode = serviceShippingDAC.CreateDAC(instruction);
            return statusCode;
        }
        public int UpdateInstruction(Instruction instruction, string bucket)
        {
            instruction.bucket = bucket;
            InstructionDAC instructionDAC = new InstructionDAC(bucketSmoovPOS);
            int statusCode = instructionDAC.updateDAC(instruction);
            return statusCode;
        }

        public int DeleteInstruction(string id, string bucket)
        {
            InstructionDAC instructionDAC = new InstructionDAC(bucketSmoovPOS);
            int statusCode = instructionDAC.deleteDAC(id);
            return statusCode;
        }
        public Instruction DetailInstruction(string id, string bucket)
        {
            InstructionDAC instructionDAC = new InstructionDAC(bucketSmoovPOS);
            Instruction customer = instructionDAC.detailDAC(id);
            return customer;
        }
        public IView<Instruction> GetAllServiceShipping(string designDoc, string viewName, string bucket)
        {
            InstructionDAC instructionDAC = new InstructionDAC(bucketSmoovPOS);
            IView<Instruction> listServiceShipping = null;
            listServiceShipping = instructionDAC.GetAll(designDoc, ConstantSmoovs.CouchBase.GetAllInstruction).Key(bucket);
            return listServiceShipping;
        }
    }
}