﻿using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class ServiceShippingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateServiceShipping(ServiceShipping serviceShipping, string bucket)
        {
            serviceShipping.bucket = bucket;
            ServiceShippingDAC serviceShippingDAC = new ServiceShippingDAC(bucketSmoovPOS);
            int statusCode = serviceShippingDAC.CreateDAC(serviceShipping);
            return statusCode;
        }
        public int UpdateServiceShipping(ServiceShipping serviceShipping, string bucket)
        {
            serviceShipping.bucket = bucket;
            ServiceShippingDAC serviceShippingDAC = new ServiceShippingDAC(bucketSmoovPOS);
            int statusCode = serviceShippingDAC.updateDAC(serviceShipping);
            return statusCode;
        }

        public int DeleteServiceShipping(string id, string bucket)
        {
            ServiceShippingDAC serviceShippingDAC = new ServiceShippingDAC(bucketSmoovPOS);
            int statusCode = serviceShippingDAC.deleteDAC(id);
            return statusCode;
        }
        public ServiceShipping DetailServiceShipping(string id, string bucket)
        {
            ServiceShippingDAC serviceShippingDAC = new ServiceShippingDAC(bucketSmoovPOS);
            ServiceShipping customer = serviceShippingDAC.detailDAC(id);
            return customer;
        }
        public IView<ServiceShipping> GetAllServiceShipping(string designDoc, string viewName, string bucket)
        {
            ServiceShippingDAC serviceShippingDAC = new ServiceShippingDAC(bucketSmoovPOS);
            IView<ServiceShipping> listServiceShipping = null;
            listServiceShipping = serviceShippingDAC.GetAll(designDoc, ConstantSmoovs.CouchBase.GetAllServiceShipping).Key(bucket);
            return listServiceShipping;
        }
    }
}