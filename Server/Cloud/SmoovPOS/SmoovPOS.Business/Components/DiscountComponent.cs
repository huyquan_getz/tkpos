﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Business.Components
{
    public class DiscountComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        static DiscountComponent() { }
        public int CreateNewDiscount(Discount discount, string bucket)
        {
            discount.bucket = bucket;
            DiscountDAC discountDAC = new DiscountDAC(bucketSmoovPOS);
            int statusCode = discountDAC.CreateDAC(discount);
            return statusCode;
        }
        public int UpdateDiscount(Discount discount, string bucket)
        {
            discount.bucket = bucket;
            DiscountDAC discountDAC = new DiscountDAC(bucketSmoovPOS);
            int statusCode = discountDAC.UpdateDAC(discount);
            return statusCode;
        }

        public int DeleteDiscount(string id, string bucket)
        {
            DiscountDAC discountDAC = new DiscountDAC(bucketSmoovPOS);
            int statusCode = discountDAC.DeleteDAC(id);
            return statusCode;
        }

        public Discount DetailDiscount(string id, string bucket)
        {
            DiscountDAC discountDAC = new DiscountDAC(bucketSmoovPOS);
            Discount Discount = discountDAC.DetailDAC(id);
            return Discount;
        }
        public IView<Discount> GetAllDiscount(string designDoc, string viewName, string bucket)
        {
            DiscountDAC discountDAC = new DiscountDAC(bucketSmoovPOS);
            IView<Discount> ListDiscount = null;
            ListDiscount = discountDAC.GetAll(designDoc, viewName).Key(bucket);
            return ListDiscount;
        }
    }
}