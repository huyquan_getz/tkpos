﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Business.Components
{
    public class QueueComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateNewQueue(Queue queue, string bucket)
        {
            queue.bucket = bucket;
            QueueDAC queueDAC = new QueueDAC(bucketSmoovPOS);
            int statusCode = queueDAC.CreateDAC(queue);
            return statusCode;
        }
        public int UpdateQueue(Queue queue, string bucket)
        {
            queue.bucket = bucket;
            QueueDAC queueDAC = new QueueDAC(bucketSmoovPOS);
            int statusCode = queueDAC.UpdateDAC(queue);
            return statusCode;
        }

        public int DeleteQueue(string id, string bucket)
        {
            QueueDAC queueDAC = new QueueDAC(bucketSmoovPOS);
            int statusCode = queueDAC.DeleteDAC(id);
            return statusCode;
        }

        public Queue DetailQueue(string id, string bucket)
        {
            QueueDAC queueDAC = new QueueDAC(bucketSmoovPOS);
            Queue queue = queueDAC.DetailDAC(id);
            return queue;
        }

        public Queue DetailQueueByBranchId(string designDoc, string viewName, string branch_id, string bucket)
        {
            QueueDAC queueDAC = new QueueDAC(bucketSmoovPOS);
            IView<Queue> ListQueue = null;
            ListQueue = queueDAC.GetAll(designDoc, viewName, bucket);
            ListQueue.Key(new string[]{bucket, branch_id});
            return ListQueue.FirstOrDefault();
        }

        public IView<Queue> GetAllQueue(string designDoc, string viewName, string bucket)
        {
            QueueDAC queueDAC = new QueueDAC(bucketSmoovPOS);
            IView<Queue> ListQueue = null;
            ListQueue = queueDAC.GetAll(designDoc, viewName, bucket);
            return ListQueue;
        }
    }
}