﻿using Com.SmoovPOS.Model;
using SmoovPOS.DataSql;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>2/2/2015-3:38 PM</datetime>
    public class FunctionMethodComponent
    {
        /// <summary>
        /// Gets all function method.
        /// </summary>
        /// <returns></returns>
        /// <author>
        /// "Hoan.Tran"
        /// </author>
        /// <datetime>2/2/2015-3:38 PM</datetime>
        public IEnumerable<FunctionMethodModel> GetAllFunctionMethod()
        {
            using (var db = new EntitiesDataContext())
            {
                var results = (from m in db.FunctionMethods
                               select new FunctionMethodModel
                               {
                                   functionMethodID = m.functionMethodID,
                                   name = m.name,
                                   keyMethod = m.keyMethod,
                                   isDatabaseFunction = m.isDatabaseFunction,
                                   dataType = m.dataType
                               }).ToList();

                return results;
            }
        }
        /// <summary>
        /// Gets the function method.
        /// </summary>
        /// <param name="searchModel">The search model.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:38 PM</datetime>
        public SearchFunctionMethodModel GetFunctionMethod(SearchFunctionMethodModel searchModel)
        {
            using (var db = new EntitiesDataContext())
            {
                var results = from m in db.FunctionMethods
                              select new FunctionMethodModel
                              {
                                  functionMethodID = m.functionMethodID,
                                  name = m.name,
                                  keyMethod = m.keyMethod,
                                  isDatabaseFunction = m.isDatabaseFunction,
                                  dataType = m.dataType,
                                  userOwner = m.userOwner,
                                  createdDate = m.createdDate,
                                  modifyUser = m.modifyUser,
                                  modifyDate = m.modifyDate,
                                  display = m.display,
                                  isAddOns = m.isAddOns.GetValueOrDefault()
                              };

                //only show record display
                results = results.Where(x => x.display == true && x.isAddOns != true);
                if (!string.IsNullOrEmpty(searchModel.name))
                {
                    var searchValue = searchModel.name.Trim();
                    results = results.Where(x => x.name.Contains(searchValue) || x.keyMethod.Contains(searchValue));
                }
                //get count total
                searchModel.countAll = results.Count();

                searchModel.ListFunctionMethods = new List<FunctionMethodModel>();

                if (searchModel.countAll > 0)
                {
                    //sort
                    if (!string.IsNullOrEmpty(searchModel.sort))
                    {
                        //sort by full name
                        switch (searchModel.sort)
                        {
                            case SmoovPOS.Common.ConstantSmoovs.FunctionMethod.name:
                                if (searchModel.desc)
                                {
                                    results = results.OrderByDescending(x => x.name);
                                }
                                else
                                {
                                    results = results.OrderBy(x => x.name);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        //sort default
                        results = results.OrderByDescending(x => x.createdDate);
                    }

                    //paging
                    searchModel.ListFunctionMethods = results.Skip((searchModel.pageGoTo - 1) * searchModel.limit).Take(searchModel.limit).ToList();

                    searchModel.pageCurrent = searchModel.pageGoTo;//set page current
                }

                return searchModel;
            }
        }
        /// <summary>
        /// Gets the function method.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:40 PM</datetime>
        public FunctionMethodModel GetFunctionMethod(Guid id)
        {
            using (var db = new EntitiesDataContext())
            {
                var result = (from f in db.FunctionMethods
                         where f.functionMethodID.Equals(id)
                         select f).FirstOrDefault();

                if (result != null)
                {
                    var model = new FunctionMethodModel();

                    model.functionMethodID = result.functionMethodID;
                    model.name = result.name;
                    model.keyMethod = result.keyMethod;
                    model.isDatabaseFunction = result.isDatabaseFunction;
                    model.dataType = result.dataType;
                    model.description = result.description;
                    model.userOwner = result.userOwner;
                    model.createdDate = result.createdDate;
                    model.modifyUser = result.modifyUser;
                    model.modifyDate = result.modifyDate;
                    model.display = result.display;

                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Exists the name of the function method by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>
        /// "Hoan.Tran"
        /// </author>
        /// <datetime>2/2/2015-3:40 PM</datetime>
        public bool ExistFunctionMethodByName(string name, Guid? id)
        {
            using (var db = new EntitiesDataContext())
            {
                var isExist = false;
                if (id != null)
                {
                    isExist = (from m in db.FunctionMethods
                               where m.name.ToUpper().Equals(name.Trim().ToUpper()) && (m.functionMethodID != id.Value) && m.display == true
                               select m).Any();

                }
                else
                {
                    isExist = (from m in db.FunctionMethods
                               where m.name.ToUpper().Equals(name.Trim().ToUpper()) && m.display == true
                               select m).Any();
                }
                return isExist;
            }
        }
        /// <summary>
        /// Creates the function method.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public Guid? CreateFunctionMethod(FunctionMethodModel result)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = new FunctionMethod();

                    //general Id
                    if (model.functionMethodID == null || model.functionMethodID == Guid.Empty)
                    {
                        model.functionMethodID = Guid.NewGuid();
                    }
                    else
                    {
                        model.functionMethodID = result.functionMethodID;
                    }
                    model.name = result.name;
                    model.keyMethod = result.keyMethod;
                    model.isDatabaseFunction = result.isDatabaseFunction;
                    model.dataType = result.dataType;
                    model.description = result.description;
                   
                    model.userOwner = result.userOwner;//user create
                    model.createdDate = DateTime.UtcNow;//default
                    model.modifyUser = result.userOwner;
                    model.modifyDate = DateTime.UtcNow;
                    model.display = true;//default

                    db.FunctionMethods.InsertOnSubmit(model);
                    db.SubmitChanges();

                    return model.functionMethodID;
                }

            }
            catch
            {
            }

            return null;
        }


        /// <summary>
        /// Updates the function method.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public bool UpdateFunctionMethod(FunctionMethodModel result)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from m in db.FunctionMethods
                                 where m.functionMethodID.Equals(result.functionMethodID)
                                 select m).FirstOrDefault();

                    if (model != null)
                    {
                        model.name = result.name;
                        model.keyMethod = result.keyMethod;
                        model.isDatabaseFunction = result.isDatabaseFunction;
                        model.dataType = result.dataType;
                        model.description = result.description;

                        model.modifyUser = result.modifyUser;//user update
                        model.modifyDate = DateTime.UtcNow;//default

                        db.SubmitChanges();

                        return true;
                    }
                }

            }
            catch
            {
            }
            return false;
        }
        /// <summary>
        /// Deletes the function method.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public bool DeleteFunctionMethod(Guid id)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from m in db.FunctionMethods
                                 where m.functionMethodID.Equals(id)
                                 select m).FirstOrDefault();

                    if (model != null)
                    {
                        model.display = false;//delete

                        //model.userOwner = (new Common.Controllers.UserSession()).UserId;//user create
                        //model.createdDate = DateTime.Now;//default
                        model.modifyUser = (new Common.Controllers.UserSession()).UserId;//user update
                        model.modifyDate = DateTime.Now;//default
                        //model.display = true;//default

                        var results = (from m in db.FunctionServicePackages
                                       where m.functionMethodID.Equals(id)
                                       select m).ToList();
                        foreach (var item in results)
                        {
                            item.display = false;
                        }

                        db.SubmitChanges();

                        return true;
                    }
                }

            }
            catch
            {
            }
            return false;
        }
    }
}