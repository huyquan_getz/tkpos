﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using SmoovPOS.Entity;
using SmoovPOS.Entity.Models;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Com.SmoovPOS.Business.Components
{
    public class ProductItemComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        static ProductItemComponent() { }
        public int CreateNewProductItem(ProductItem product, string bucket)
        {
            product.bucket = bucket;
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            int StatusCode = productDAC.CreateDAC(product);
            return StatusCode;
        }
        public int UpdateProductItem(ProductItem product, string bucket)
        {
            product.bucket = bucket;
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            int StatusCode = productDAC.UpdateDAC(product);
            return StatusCode;
        }
        public int DeleteProductItem(string id, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            int StatusCode = productDAC.DeleteDAC(id);
            return StatusCode;
        }
        public ProductItem DetailProductItem(string id, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            ProductItem product = productDAC.DetailDAC(id);
            return product;
        }

        public IEnumerable<ProductItem> GetAllProductItem(string designDoc, string viewName, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            var listProduct = productDAC.GetAllProducItem(designDoc, viewName).Key(bucket);
            return listProduct.ToList();
        }

        public IView<ProductItem> GetAllProductItemByIDProduct(string designDoc, string viewName, string productID, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> ListProductItem = null;
            ListProductItem = productDAC.GetAllProducItem(designDoc, viewName);
            ListProductItem.Key(new string[] { bucket, productID });
            return ListProductItem;
        }

        public SearchProductModel GetAllProductItemByStore(string designDoc, string viewName, int store, string bucket, NewPaging paging)
        {

            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IEnumerable<ProductItem> listProductItem = null;
            listProductItem = productDAC.GetAllProducItem(designDoc, viewName).Key(new object[] { bucket, store });
            SearchProductModel model = new SearchProductModel();
            model.Paging = new NewPaging();
            if (listProductItem != null)
            {
                if (paging.type == "product")
                {
                    listProductItem = listProductItem.Where(m => m.title.ToUpper().Contains(paging.text.ToUpper()));
                }
                else if (paging.type == "category")
                {
                    listProductItem = listProductItem.Where(m => m.categoryName.ToUpper().Contains(paging.text.ToUpper()));
                }

                if (paging.sort == "product")
                {
                    if (paging.desc)
                    {
                        listProductItem = listProductItem.OrderByDescending(m => m.title);
                    }
                    else
                    {
                        listProductItem = listProductItem.OrderBy(m => m.title);
                    }
                }
                else if (paging.sort == "category")
                {

                    if (paging.desc)
                    {
                        listProductItem = listProductItem.OrderByDescending(m => m.categoryName);
                    }
                    else
                    {
                        listProductItem = listProductItem.OrderBy(m => m.categoryName);
                    }
                }
                model.Paging.total = listProductItem.Count();
                model.Paging.pageGoTo = paging.pageGoTo;
                model.Paging.limit = paging.limit;
                model.Paging.text = paging.text;
                model.Paging.sort = paging.sort;
                model.Paging.desc = paging.desc;
                model.Paging.type = paging.type;
                model.Paging.store = paging.store;
                listProductItem = listProductItem.Skip((paging.pageGoTo - 1) * paging.limit).Take(paging.limit).ToList();
                model.ListProductItem = listProductItem;
                return model;
            }
            else
            {
                return model;
            }

        }

        public int CountAllProductItemByStore(string designDoc, string viewName, string busineesID, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> ListProductItem = null;
            ListProductItem = productDAC.GetAllProducItem(designDoc, viewName);
            ListProductItem.Key(new string[] { bucket, busineesID });
            return ListProductItem.Count();
        }

        public int CountVarients(string designDoc, string viewName, string businessID, string productID, string varientID, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IEnumerable<ProductItem> listProduct = GetAllProductItemByIDProduct(designDoc, viewName, productID, bucket);

            int countVarient = 0;
            if (listProduct != null && listProduct.Count() > 0)
            {
                var product = listProduct.FirstOrDefault(r => r.store == businessID && (string.IsNullOrWhiteSpace(productID) || r.productID == productID));
                if (product != null)
                {
                    countVarient = product.arrayVarient.Where(r => string.IsNullOrWhiteSpace(varientID) || r._id == varientID).Sum(r => r.quantity);
                }
            }

            return countVarient;
        }

        public IView<ProductItem> GetAllProductItemByMaster(string designDoc, string viewName, string startKey = null, string endKey = null, int limit = 0, bool allowStale = false, int pageCurrent = 0, int pageGoTo = 0, string sort = "title", Boolean desc = false, string bucket = "smoovpos")
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> listProduct = null;
            if (sort == "status")
            {
                listProduct = productDAC.GetAllProducItem(designDoc, viewName);
            }
            else if (sort == "category")
            {
                listProduct = productDAC.GetAllProducItem(designDoc, ConstantSmoovs.CouchBase.GetAllByCategoryMaster);
            }
            else
            {
                listProduct = productDAC.GetAllProducItem(designDoc, ConstantSmoovs.CouchBase.GetAllByTitleMaster);
            }
            if (listProduct.Count() > 0)
            {
                if (!desc)
                {
                    listProduct.Descending(false);
                }
                else
                {
                    listProduct.Descending(true);
                }
                string category = "";
                if (sort == "category")
                {
                    category = listProduct.ElementAt((pageGoTo - 1) * limit).categoryName;
                }

                startKey = sort == "title" ? listProduct.ElementAt((pageGoTo - 1) * limit).title : listProduct.ElementAt((pageGoTo - 1) * limit)._id;
                endKey = null;

                int LastOfPage = listProduct.Count() / limit;
                int percent = listProduct.Count() % limit;
                if (percent > 0)
                {
                    LastOfPage += 1;
                }
                if (pageGoTo == LastOfPage)
                {
                    startKey = sort == "title" ? listProduct.ElementAt((pageGoTo - 1) * limit).title : listProduct.ElementAt((pageGoTo - 1) * limit)._id;
                    endKey = sort == "title" ? listProduct.ElementAt(listProduct.Count() - 1).title : listProduct.ElementAt(listProduct.Count() - 1)._id;
                }

                if (!string.IsNullOrEmpty(startKey))
                {
                    if (!allowStale) listProduct.Stale(StaleMode.False);
                    if (sort == "category")
                    {
                        listProduct.StartKey(new string[] { bucket, category, startKey });
                        if (!string.IsNullOrEmpty(endKey)) listProduct.EndKey(new string[] { bucket, category, endKey });
                    }
                    else if (sort == "status")
                    {
                        listProduct.StartKey(desc);
                        if (!string.IsNullOrEmpty(startKey)) listProduct.EndKey(new string[] { bucket, endKey });
                    }
                    else
                    {
                        listProduct.StartKey(new string[] { bucket, startKey });
                        if (!string.IsNullOrEmpty(endKey)) listProduct.EndKey(new string[] { bucket, endKey });
                    }

                }
                if (limit > 0) listProduct.Limit(limit * 5);
            }

            return listProduct;
        }


        public IView<ProductItem> GetAllProductItemByMasterNoLimit(string designDoc, string viewName, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> listProduct = null;
            listProduct = productDAC.GetAllProducItemWithBucket(designDoc, viewName, bucket);
            return listProduct;
        }

        public int CountAllProductItemByMaster(string designDoc, string viewName, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> listProduct = null;
            listProduct = productDAC.GetAllProducItemWithBucket(designDoc, viewName, bucket);
            return listProduct.Count();
        }

        public IView<ProductItem> GetProductItemByListIDProduct(string designDoc, string viewName, List<string> productIDs, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> ListProductItem = null;
            ListProductItem = productDAC.GetAllProducItemWithBucket(designDoc, viewName, bucket);
            ListProductItem.Where(l => productIDs.Contains(l.productID));
            return ListProductItem;
        }

        public IEnumerable<ProductItem> GetAllActiveProductItemByStoreAndCategory(string designDoc, string viewName, string bucket, string branchName, string categoryName)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            var key = new object[] { bucket, branchName, categoryName };
            IEnumerable<ProductItem> list = productItemDAC.GetAllProducItem(designDoc, viewName).Key(key).OrderBy(i => i.name);
            return list;
        }

        public IView<ProductItem> GetAllProductItemByStore(string designDoc, string viewName, string bucket)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> ListProductItem = null;
            ListProductItem = productDAC.GetAllProducItem(designDoc, viewName).Key(new object[] { bucket, ConstantSmoovs.Stores.OnlineStore });
            return ListProductItem;
        }

        public SearchProductModel SearchingProduct(string designDoc, string viewName, string bucket, NewPaging paging)
        {
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IEnumerable<ProductItem> listSearch = null;
            listSearch = productDAC.GetAllProducItemWithBucket(designDoc, viewName, bucket);
            SearchProductModel model = new SearchProductModel();
            model.Paging = new NewPaging();
            if (listSearch != null)
            {
                if (paging.type == "product")
                {
                    listSearch = listSearch.Where(m => m.title.ToUpper().Contains(paging.text.ToUpper()));
                }
                else if (paging.type == "category")
                {
                    listSearch = listSearch.Where(m => m.categoryName.ToUpper().Contains(paging.text.ToUpper()));
                }

                if (paging.sort == "product")
                {
                    if (paging.desc)
                    {
                        listSearch = listSearch.OrderByDescending(m => m.title);
                    }
                    else
                    {
                        listSearch = listSearch.OrderBy(m => m.title);
                    }
                }
                else if (paging.sort == "category")
                {

                    if (paging.desc)
                    {
                        listSearch = listSearch.OrderByDescending(m => m.categoryName);
                    }
                    else
                    {
                        listSearch = listSearch.OrderBy(m => m.categoryName);
                    }
                }
                model.Paging.total = listSearch.Count();
                model.Paging.pageGoTo = paging.pageGoTo;
                model.Paging.limit = paging.limit;
                model.Paging.text = paging.text;
                model.Paging.sort = paging.sort;
                model.Paging.desc = paging.desc;
                model.Paging.type = paging.type;
                listSearch = listSearch.Skip((paging.pageGoTo - 1) * paging.limit).Take(paging.limit).ToList();
                model.ListProductItem = listSearch;
                return model;
            }
            else
            {
                return model;
            }

        }

        public List<string> UpdateStatusProductItem(string id, bool status, string bucket)
        {
            List<string> listUpdateFailed = new List<string>();
            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            var productItem = this.GetAllProductItemByIDProduct(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, id, bucket);
            CollectionComponent collectionComponent = new CollectionComponent();
            var modelCollection = collectionComponent.GetAllCollection(ConstantSmoovs.CouchBase.DesignDocCollection, ConstantSmoovs.CouchBase.CollectionViewAll, bucket);
            foreach (ProductItem product in productItem)
            {
                product.status = status;
                for (int i = 0; i < product.arrayVarient.Count; i++)
                {
                    product.arrayVarient[i].status = status;
                }
                int reponse = this.UpdateProductItem(product, bucket);
                if (reponse != 0)
                {
                    listUpdateFailed.Add(product._id);
                }
                foreach (var collection in modelCollection)
                {
                    if (collection.arrayProduct != null && collection.arrayProduct.Where(p => p._id == product._id).Any())
                    {
                        var arrayProduct = collection.arrayProduct.ToList();
                        for (int i = 0; i < arrayProduct.Count; i++)
                        {
                            if (arrayProduct[i]._id == product._id)
                            {
                                arrayProduct[i].status = product.status;
                            }
                        }

                        collection.arrayProduct = arrayProduct;
                        collectionComponent.UpdateCollection(collection, bucket);
                    }
                }
            }

            return listUpdateFailed;
        }

        public int UpdateQuantityProductItem(List<ProductItem> listProductItemsMyCart, int store, string bucket)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            ProductItemComponent productComponent = new ProductItemComponent();
            var listProductItems = productItemDAC.GetAllProducItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByStore).Key(new object[] { bucket, store });
            foreach (var productItemPay in listProductItemsMyCart)
            {
                foreach (var productItemStore in listProductItems.Where(r => r._id == productItemPay._id))
                {
                    foreach (var varientPay in productItemPay.arrayVarient.Where(v => v.isCheckedToCheckOut))
                    {
                        foreach (var varientOnl in productItemStore.arrayVarient.Where(k => k.sku == varientPay.sku))
                        {
                            if (varientOnl.quantity > 0) varientOnl.quantity -= varientPay.quantity;

                            productItemStore.countOfSoldItem += varientPay.quantity;
                        }
                    }
                    var result = productComponent.UpdateProductItem(productItemStore, bucket);
                }
            }
            return 1;
        }

        public int UpdateBusinessNameProductItemByStore(string designDoc, string viewName, int store, string bucket, string businessName)
        {

            ProductItemDAC productDAC = new ProductItemDAC(bucketSmoovPOS);
            IEnumerable<ProductItem> listProductItem = null;
            listProductItem = productDAC.GetAllProducItem(designDoc, viewName).Key(new object[] { bucket, store });
            if (listProductItem != null && listProductItem.Count() > 0)
            {
                foreach (var item in listProductItem)
                {
                    item.store = businessName;
                    productDAC.UpdateDAC(item);
                }
                return 1;
            }
            else
            {
                return 0;
            }

        }
    }
        
}