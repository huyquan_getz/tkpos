﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Common.CodeGenerator;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class StationComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateNewStation(Station station, string bucket)
        {
            station.bucket = bucket;
            StationDAC stationDAC = new StationDAC(bucketSmoovPOS);
            int StatusCode = stationDAC.CreateDAC(station);
            return StatusCode;
        }

        public IView<Station> GetAllStations(string designDoc, string viewName, string bucket)
        {
            StationDAC stationDAC = new StationDAC(bucketSmoovPOS);
            IView<Station> list = stationDAC.GetAll(designDoc, viewName);
            return list;
        }

        public IEnumerable<Station> GetListStationsOfBranch(string designDoc, string viewName, string bucket, int BranchIndex)
        {
            StationDAC stationDAC = new StationDAC(bucketSmoovPOS);
            IEnumerable<Station> list = stationDAC.GetAll(designDoc, viewName).Key(new object[] { bucket, BranchIndex });
            return list;
        }

        public Station GetDetailStation(string id)
        {
            StationDAC stationDAC = new StationDAC(bucketSmoovPOS);
            Station station = stationDAC.GetDetailDAC(id);
            return station;
        }

        public int UpdateStation(Station station, string bucket)
        {
            station.bucket = bucket;
            StationDAC stationDAC = new StationDAC(bucketSmoovPOS);
            int StatusCode = stationDAC.UpdateDAC(station);
            return StatusCode;
        }

        public int UpdateStationWithRawData(string designDoc, string viewName, string bucket, int BranchIndex, string StationID, string StationName, int StationSize, string StationQRCode)
        {
            int StatusCode = 0;
            Station objStation = GetDetailStation(StationID);

            if (objStation != null)
            {
                //Case "objStation is existed": Update stationName, stationSize, modifiedDate and userID
                objStation.stationName = StationName;
                objStation.stationSize = StationSize;

                objStation.modifiedDate = DateTime.UtcNow;
                objStation.userID = objStation.userID;

                objStation.stationQRCode = StationQRCode;

                StatusCode = UpdateStation(objStation, bucket);
            }
            else
            {
                //Case "objStation is not existed": Create new station object

                objStation = new Station();

                objStation.branchIndex = BranchIndex;
                objStation._id = objStation.id = StationID;
                objStation.stationName = StationName;
                objStation.stationSize = StationSize;
                objStation.stationQRCode = StationQRCode;
                objStation.statusOrdering = ConstantSmoovs.Enums.StationTableStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.StationTableStatus.Empty).Key;
                objStation.customerHost = new CustomerInfo();
                objStation.orderId = string.Empty;

                objStation.channels = new string[] { bucket + "_" + BranchIndex + "_table" };
                objStation.status = objStation.display = true;
                objStation.createdDate = objStation.modifiedDate = DateTime.UtcNow;
                objStation.userID = objStation.userID;
                objStation.userOwner = objStation.userOwner;
                objStation.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();

                StatusCode = CreateNewStation(objStation, bucket);
            }

            return StatusCode;
        }

        public int DeleteStation(string id, string bucket)
        {
            StationDAC stationDAC = new StationDAC(bucketSmoovPOS);
            int StatusCode = stationDAC.DeleteDAC(id);
            return StatusCode;
        }
    }
}