﻿using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class MerchantGeneralSettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateNewMerchantGeneralSetting(MerchantGeneralSetting merchantGeneralSetting, string bucket)
        {
            merchantGeneralSetting.bucket = bucket;
            MerchantGeneralSettingDAC merchantGeneralSettingDAC = new MerchantGeneralSettingDAC(bucketSmoovPOS);
            int statusCode = merchantGeneralSettingDAC.CreateDAC(merchantGeneralSetting);
            return statusCode;
        }

        public int UpdateMerchantGeneralSetting(MerchantGeneralSetting merchantGeneralSetting, string bucket)
        {
            merchantGeneralSetting.bucket = bucket;
            MerchantGeneralSettingDAC merchantGeneralSettingDAC = new MerchantGeneralSettingDAC(bucketSmoovPOS);
            int statusCode = merchantGeneralSettingDAC.UpdateDAC(merchantGeneralSetting);
            return statusCode;
        }

        public MerchantGeneralSetting GetDetailMerchantGeneralSetting(string designDoc, string viewName, string bucket)
        {
            MerchantGeneralSettingDAC merchantGeneralSettingDAC = new MerchantGeneralSettingDAC(bucketSmoovPOS);
            MerchantGeneralSetting merchantGeneralSetting = merchantGeneralSettingDAC.GetAll(designDoc, viewName).Key(bucket).FirstOrDefault();
            
            return merchantGeneralSetting;
        }
    }
}