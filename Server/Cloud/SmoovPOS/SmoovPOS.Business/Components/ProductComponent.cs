﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Business.Components
{
    public class ProductComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        static ProductComponent() { }
        public int CreateNewProduct(Product product, string bucket)
        {
            product.bucket = bucket;
            ProductDAC productDAC = new ProductDAC(bucketSmoovPOS);
            int StatusCode = productDAC.CreateDAC(product);
            return StatusCode;
        }
        public string CreateNewProduct_(Product product, string bucket)
        {
            product.bucket = bucket;
            ProductDAC productDAC = new ProductDAC(bucketSmoovPOS);
            string _id = productDAC.CreateDAC_(product);
            return _id;
        }
        public int UpdateProduct(Product product, string bucket)
        {
            ProductDAC productDAC = new ProductDAC(bucketSmoovPOS);
            int StatusCode = productDAC.UpdateDAC(product);
            return StatusCode;
        }
        public int DeleteProduct(string id, string bucket)
        {
            ProductDAC productDAC = new ProductDAC(bucketSmoovPOS);
            int StatusCode = productDAC.DeleteDAC(id);
            return StatusCode;
        }
        public Product DetailProduct(string id, string bucket)
        {
            ProductDAC productDAC = new ProductDAC(bucketSmoovPOS);
            Product product = productDAC.DetailDAC(id);
            return product;
        }

        public int CountAllProduct(string designDoc, string viewName, string bucket)
        {
            ProductDAC productDAC = new ProductDAC(bucketSmoovPOS);
            IView<Product> listProduct = null;
            listProduct = productDAC.GetAllDAC(designDoc, ConstantSmoovs.CouchBase.GetAllProduct).Key(bucket);
            return listProduct.Count();
        }

        public int CheckSKUExist(string designDoc, string viewName, string sku, string bucket)
        {
            ProductDAC productDAC = new ProductDAC(bucketSmoovPOS);
            IView<Product> listProduct = null;
            listProduct = productDAC.GetAllDAC(designDoc, viewName);
            listProduct.Key(new string[] { bucket, sku });
            return listProduct.Count();
        }

        public IView<Product> GetAllProductActive(string designDoc, string viewName, string bucket)
        {
            ProductDAC productDAC = new ProductDAC(bucketSmoovPOS);
            IView<Product> listProduct = null;
            listProduct = productDAC.GetAllDAC(designDoc,ConstantSmoovs.CouchBase.GetAllByStatusActive);
            return listProduct;
        }
    }
}