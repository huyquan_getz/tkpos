﻿using Com.SmoovPOS.Model;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC.Settings;
using SmoovPOS.DataSql;
using SmoovPOS.Entity.DeviceToken;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components.Settings
{
    public class DeviceTokenSettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public ManageDevice DetailDeviceToken(string designDoc, string viewName, string bucket)
        {
            //List<ManageDevice> lists = new List<ManageDevice>();
            //DeviceTokenSettingDAC smoovpayDAC = new DeviceTokenSettingDAC(bucketSmoovPOS);
            //lists = smoovpayDAC.GetAll(designDoc, viewName).Key(bucket).ToList();
            //if (lists.Count > 0)
            //{
            //    return lists.FirstOrDefault();
            //}
            //else
            //{
            //    return null;
            //}
            using (var db = new EntitiesDataContext())
            {
                var results = (from m in db.DeviceTokens
                               where m.bucket == bucket
                               select new DeviceTokenModel
                               {
                                   uuid = m.uuid,
                                   token = m.token,
                                   name = m.name,
                                   systemName = m.systemName,
                                   systemVersion = m.systemVersion,
                                   model = m.model,
                                   os = m.os,
                                   status = m.status,
                                   badge = m.badge.GetValueOrDefault(),
                                   appType = m.appType,
                                   bucket = m.bucket,
                                   merchantID = m.merchantID,
                                   deviceTokenId = m.deviceTokenId,
                                   branchIndex = m.branchIndex.GetValueOrDefault()
                               }).ToList();

                var model = new ManageDevice();
                model.ArrayDevices = results;
                return model;
            }
        }

        public int UpdateDeviceToken(DeviceTokenModel device, string bucket)
        {
            //    DeviceTokenSettingDAC DAC = new DeviceTokenSettingDAC(bucketSmoovPOS);
            //    var managedDeviceCurrent = DetailDeviceToken(designDoc, viewName, bucket);
            //    var index = managedDeviceCurrent.ArrayDevices.FindIndex(m => m.uuid == m.uuid);
            //    managedDeviceCurrent.ArrayDevices[index] = device;
            //    int statusCode = DAC.UpdateDAC(managedDeviceCurrent);
            //    return statusCode;

            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from m in db.DeviceTokens
                                 where m.token.Equals(device.token) && m.bucket == bucket
                                 select m).FirstOrDefault();

                    if (model != null)
                    {
                        model.uuid = device.uuid;
                        model.token = device.token;
                        model.badge = device.badge;
                        model.branchIndex = device.branchIndex;

                        model.modifyDate = DateTime.UtcNow;

                        db.SubmitChanges();

                        return 0;
                    }
                }

            }
            catch
            {
            }
            return 1;
        }

        public int CreateDeviceToken(DeviceTokenModel device, string bucket)
        {
            //    DeviceTokenSettingDAC DAC = new DeviceTokenSettingDAC(bucketSmoovPOS);
            //    var managedDeviceCurrent = DetailDeviceToken(designDoc, viewName, bucket);
            //    var index = managedDeviceCurrent.ArrayDevices.FindIndex(m => m.uuid == m.uuid);
            //    managedDeviceCurrent.ArrayDevices[index] = device;
            //    int statusCode = DAC.UpdateDAC(managedDeviceCurrent);
            //    return statusCode;

            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from m in db.DeviceTokens
                                 where m.token.Equals(device.token) && m.bucket == bucket
                                 select m).FirstOrDefault();

                    if (model != null)
                    {
                        model.uuid = device.uuid;
                        model.token = device.token;
                        model.branchIndex = device.branchIndex;

                        model.modifyDate = DateTime.UtcNow;

                        db.SubmitChanges();
                    }
                    else
                    {
                        model = new DeviceToken();

                        model.deviceTokenId = Guid.NewGuid();
                        model.uuid = device.uuid;
                        model.token = device.token;
                        model.name = device.name;
                        model.systemName = device.systemName;
                        model.systemVersion = device.systemVersion;
                        model.model = device.model;
                        model.os = device.os;
                        model.status = device.status;
                        model.badge = device.badge;
                        model.appType = device.appType;

                        model.branchIndex = -1;

                        model.createdDate = DateTime.UtcNow;//default
                        model.modifyDate = DateTime.UtcNow;//default
                        model.userOwner = device.userOwner;

                        var merchant = (from m in db.Merchants
                                        where m.bucket.Equals(bucket)
                                        select m).FirstOrDefault();
                        if (merchant != null)
                        {
                            model.merchantID = merchant.merchantID;
                            model.bucket = merchant.bucket;
                            model.userOwner = merchant.userOwner;
                            model.modifyUser = merchant.userOwner;
                        }

                        db.DeviceTokens.InsertOnSubmit(model);
                        db.SubmitChanges();
                    }
                    return 0;
                }

            }
            catch
            {
            }
            return 1;
        }
        public int AddDeviceToken(string bucket, string designDoc, string viewName, DeviceTokenModel device)
        {
            var servicePacket = new SmoovPOS.Business.Components.ServicePackageComponent();
            ServicePackageModel servicePackage = servicePacket.GetServicePacket(bucket);

            FunctionServicePackageModel checkValidate = null;
            if (device.appType == ConstantSmoovs.AppTypes.pos)
            {
                checkValidate = servicePackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.MAINPOSAPP);
            }
            else
            {
                checkValidate = servicePackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.TABLEORDERING);
            }

            if (checkValidate.limit <= 0)
            {
                return ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.ReturnCodeAppUnavailable).Key;
            }
            //DeviceTokenSettingDAC DAC = new DeviceTokenSettingDAC(bucketSmoovPOS);
            var managedDeviceCurrent = DetailDeviceToken(designDoc, viewName, bucket);
            var isNew = false;
            if (managedDeviceCurrent == null ||
                managedDeviceCurrent.ArrayDevices == null ||
                !managedDeviceCurrent.ArrayDevices.Any(m => m.token == device.token || m.token == device.token))
            {
                ManageDevice newManageDevice = new ManageDevice();
                newManageDevice._id = Guid.NewGuid().ToString();
                newManageDevice.createdDate = DateTime.Today;
                newManageDevice.modifiedDate = DateTime.Today;
                newManageDevice.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                newManageDevice.ArrayDevices = new List<DeviceTokenModel>();
                newManageDevice.channels = new String[] { bucket };
                newManageDevice.display = true;
                newManageDevice.status = true;
                newManageDevice.bucket = bucket;
                managedDeviceCurrent = newManageDevice;
                isNew = true;
            }

            if (managedDeviceCurrent.ArrayDevices.Any(m => m.uuid == device.uuid || m.token == device.token))
            {
                //var index = managedDeviceCurrent.ArrayDevices.FindIndex(m => m.uuid == device.uuid);
                //managedDeviceCurrent.ArrayDevices[index] = device;
                var deviceUpdate = managedDeviceCurrent.ArrayDevices.Where(m => m.uuid == device.uuid || m.token == device.token).FirstOrDefault();
                deviceUpdate.uuid = device.uuid;
                deviceUpdate.token = device.token;
                deviceUpdate.branchIndex = device.branchIndex;

                int statusCode = UpdateDeviceToken(deviceUpdate, bucket);

                return statusCode;
            }
            else
            {
                managedDeviceCurrent.ArrayDevices.Add(device);
            }

            var functionServicePacket = servicePackage.FunctionServicePackets.FirstOrDefault(f => f.keyMethod == ConstantSmoovs.AddOns_Key.POSDEVICE);
            var countCurrent = managedDeviceCurrent.ArrayDevices.Count();
            if (functionServicePacket.isUnlimit || countCurrent <= functionServicePacket.limit)
            {
                int statusCode = -1;
                if (isNew)
                {
                    statusCode = CreateDeviceToken(device, bucket);
                }
                else
                {
                    var deviceUpdate = managedDeviceCurrent.ArrayDevices.Where(m => m.token == device.token || m.token == device.token).FirstOrDefault();
                    deviceUpdate.uuid = device.uuid;
                    deviceUpdate.token = device.token;
                    deviceUpdate.branchIndex = device.branchIndex;

                    statusCode = UpdateDeviceToken(deviceUpdate, bucket);
                }
                return statusCode;
            }

            return ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.ErrorLimitDevicePOS).Key;
        }

        public int DeleteDevice(string id, string bucket)
        {
            //DeviceTokenSettingDAC DAC = new DeviceTokenSettingDAC(bucketSmoovPOS);
            //var managedDeviceCurrent = DetailDeviceToken(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, bucket);
            //managedDeviceCurrent.ArrayDevices.RemoveAll(m => m.uuid == id);
            //int statusCode = DAC.UpdateDAC(managedDeviceCurrent);
            //return statusCode;

            try
            {
                using (var db = new EntitiesDataContext())
                {

                    var model = (from m in db.DeviceTokens
                                 where m.uuid.Equals(id) && m.bucket == bucket
                                 select m).ToList();

                    if (model != null && model.Count > 0)
                    {
                        db.DeviceTokens.DeleteAllOnSubmit(model);

                        db.SubmitChanges();

                        return 0;
                    }
                }

            }
            catch
            {
            }
            return 1;
        }

        public bool ExistsToken(string id, string bucket)
        {
            //DeviceTokenSettingDAC DAC = new DeviceTokenSettingDAC(bucketSmoovPOS);
            var managedDeviceCurrent = DetailDeviceToken(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, bucket);
            if (managedDeviceCurrent == null)
            {
                return false;
            }
            else
            {
                var item = managedDeviceCurrent.ArrayDevices.Where(m => m.uuid == id).FirstOrDefault();
                if (item == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public int UpdateManageDevice(ManageDevice manageDevice, string bucket)
        {
            //DeviceTokenSettingDAC DAC = new DeviceTokenSettingDAC(bucketSmoovPOS);
            //var status = DAC.UpdateDAC(manageDevice);
            foreach (var item in manageDevice.ArrayDevices)
            {
                UpdateDeviceToken(item, bucket);
            }
            return 0;
        }
    }
}