﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class DeliveryCountryComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public bool CreateNewDeliveryCountry(DeliveryCountry deliveryCountry, string bucket)
        {
            deliveryCountry.bucket = bucket;
            DeliveryCountryDAC deliveryCountryDAC = new DeliveryCountryDAC(bucketSmoovPOS);
            int StatusCode = deliveryCountryDAC.CreateDAC(deliveryCountry);
            return (StatusCode == 0);
        }
        public bool UpdateDeliveryCountry(DeliveryCountry deliveryCountry, string bucket)
        {
            deliveryCountry.bucket = bucket;
            DeliveryCountryDAC deliveryCountryDAC = new DeliveryCountryDAC(bucketSmoovPOS);
            int StatusCode = deliveryCountryDAC.UpdateDAC(deliveryCountry);
            return (StatusCode == 0);
        }
        public bool DeleteDeliveryCountry(string id, string bucket)
        {
            DeliveryCountryDAC deliveryCountryDAC = new DeliveryCountryDAC(bucketSmoovPOS);
            int StatusCode = deliveryCountryDAC.DeleteDAC(id);
            return (StatusCode == 0);
        }
        public DeliveryCountry DetailDeliveryCountry(string id, string bucket)
        {
            DeliveryCountryDAC deliveryCountryDAC = new DeliveryCountryDAC(bucketSmoovPOS);
            DeliveryCountry deliveryCountry = deliveryCountryDAC.DetailDAC(id);
            return deliveryCountry;
        }
        public IEnumerable<DeliveryCountry> GetAllDeliveryCountry(string designDoc, string viewName, string bucket)
        {
            DeliveryCountryDAC deliveryCountryDAC = new DeliveryCountryDAC(bucketSmoovPOS);
            IEnumerable<DeliveryCountry> listDeliveryCountry = deliveryCountryDAC.GetAllDeliveryCountryDAC(designDoc, viewName).Key(bucket);// DeliveryCountryDAC.GetAllDeliveryCountryDAC(designDoc, viewName);

            return listDeliveryCountry;
        }
    }
}