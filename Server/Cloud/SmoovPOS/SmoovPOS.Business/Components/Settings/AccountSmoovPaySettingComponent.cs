﻿using SmoovPOS.Common;
using SmoovPOS.Data.DAC.AccountSmoovPaySettings;
using SmoovPOS.Entity.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components.Settings
{
    public class AccountSmoovPaySettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public AccountSmoovpay GetDetailAccountSmoovPay(string designDoc, string viewName, string bucket)
        {
            AccountSmoovPaySettingDAC smoovpayDAC = new AccountSmoovPaySettingDAC(bucketSmoovPOS);
            var listSmoovpay = smoovpayDAC.GetAll(designDoc, viewName).Key(bucket).ToList();
            if (listSmoovpay.Count > 0)
            {
                return listSmoovpay.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public int CreateAccountSmoovPay(string bucket, AccountSmoovpay currency)
        {
            AccountSmoovPaySettingDAC smoovpayDAC = new AccountSmoovPaySettingDAC(bucketSmoovPOS);
            int statusCode = smoovpayDAC.CreateDAC(currency);
            return statusCode;
        }

        public int UpdateAccountSmoovPay(string bucket, AccountSmoovpay currency)
        {
            AccountSmoovPaySettingDAC smoovpayDAC = new AccountSmoovPaySettingDAC(bucketSmoovPOS);
            int statusCode = smoovpayDAC.UpdateDAC(currency);
            return statusCode;
        }

        public int DeleteAccountSmoovPay(string id, string bucket)
        {
            AccountSmoovPaySettingDAC smoovpayDAC = new AccountSmoovPaySettingDAC(bucketSmoovPOS);
            int StatusCode = smoovpayDAC.DeleteDAC(id);
            return StatusCode;
        }
    }
}