﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Business.Components.Settings
{
    public class TakeAwaySettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateNewTakeAwaySetting(TakeAwaySetting takeAwaySetting, string bucket)
        {
            takeAwaySetting.bucket = bucket;
            TakeAwaySettingDAC takeAwaySettingDAC = new TakeAwaySettingDAC(bucketSmoovPOS);
            int statusCode = takeAwaySettingDAC.CreateDAC(takeAwaySetting);
            return statusCode;
        }
        public int UpdateTakeAwaySetting(TakeAwaySetting takeAwaySetting, string bucket)
        {
            takeAwaySetting.bucket = bucket;
            TakeAwaySettingDAC takeAwaySettingDAC = new TakeAwaySettingDAC(bucketSmoovPOS);
            int statusCode = takeAwaySettingDAC.UpdateDAC(takeAwaySetting);
            return statusCode;
        }

        public int DeleteTakeAwaySetting(string id, string bucket)
        {
            TakeAwaySettingDAC takeAwaySettingDAC = new TakeAwaySettingDAC(bucketSmoovPOS);
            int statusCode = takeAwaySettingDAC.DeleteDAC(id);
            return statusCode;
        }

        public TakeAwaySetting DetailTakeAwaySetting(string id, string bucket)
        {
            TakeAwaySettingDAC takeAwaySettingDAC = new TakeAwaySettingDAC(bucketSmoovPOS);
            TakeAwaySetting takeAwaySetting = takeAwaySettingDAC.DetailDAC(id);
            return takeAwaySetting;
        }

        public TakeAwaySetting DetailTakeAwaySettingByBranchId(string designDoc, string viewName, string branch_id, string bucket)
        {
            TakeAwaySettingDAC takeAwaySettingDAC = new TakeAwaySettingDAC(bucketSmoovPOS);
            IView<TakeAwaySetting> ListTakeAwaySetting = null;
            ListTakeAwaySetting = takeAwaySettingDAC.GetAll(designDoc, viewName);
            ListTakeAwaySetting.Key(new string[] { bucket, branch_id });
            return ListTakeAwaySetting.FirstOrDefault();
        }

        public IView<TakeAwaySetting> GetAllTakeAwaySetting(string designDoc, string viewName, string bucket)
        {
            TakeAwaySettingDAC takeAwaySettingDAC = new TakeAwaySettingDAC(bucketSmoovPOS);
            IView<TakeAwaySetting> ListTakeAwaySetting = null;
            ListTakeAwaySetting = takeAwaySettingDAC.GetAll(designDoc, viewName).Key(bucket);
            return ListTakeAwaySetting;
        }
    }
}