﻿using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class DeveloperSettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        //Work on list of developer-defined currencies, no need of bucket
        #region Currency
        public List<CurrencyItem> GetAllCurrency(string designDoc, string viewName, string bucket)
        {
            CurrencyDAC currencyDAC = new CurrencyDAC(bucketSmoovPOS);
            List<CurrencyItem> listCurrency = currencyDAC.GetAllCurrency(designDoc, viewName).Key(bucket).FirstOrDefault().listCurrency;
            return listCurrency;
        }

        public int CreateNewCurrency(string bucket, Currency currency)
        {
            CurrencyDAC currencyDAC = new CurrencyDAC(bucketSmoovPOS);
            int statusCode = currencyDAC.CreateDAC(currency);
            return statusCode;
        }

        public int UpdateCurrency(string bucket, Currency currency)
        {
            CurrencyDAC currencyDAC = new CurrencyDAC(bucketSmoovPOS);
            int statusCode = currencyDAC.UpdateDAC(currency);
            return statusCode;
        }
        #endregion
    }
}