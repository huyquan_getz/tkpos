﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using SmoovPOS.Entity.Menu;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SmoovPOS.Business.Components
{
    public class OnlineComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public IEnumerable<Category> GetAllActiveCategory(string designDoc, string viewName, string bucket)
        {
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            IView<Category> categoryViewList = null;
            categoryViewList = categoryDAC.GetAll(designDoc, viewName).Key(bucket);

            //code to check category active
            return categoryViewList.Where(r => r.status);
        }
        public IEnumerable<Menu> GetListNameCategory(string designDoc, string viewName, string bucket)
        {
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            IView<Category> categoryViewList = null;
            categoryViewList = categoryDAC.GetAll(designDoc, viewName).Key(bucket);
            List<Menu> list = new List<Menu>();
            var listcategory = categoryViewList.ToList();
            if (listcategory != null && listcategory.Count > 0)
            {
                foreach (var cate in categoryViewList.Where(r => r.status))
                {
                    Menu menu = new Menu();
                    menu.id = cate._id;
                    menu.name = cate.title;
                    menu.url = cate.image;
                    menu.siteID = bucket;
                    list.Add(menu);
                }
            }
            
            return list;
        }
        public IEnumerable<Menu> GetListNameCollection(string designDoc, string viewName, string bucket)
        {
            CollectionDAC collectionDAC = new CollectionDAC(bucketSmoovPOS);
            IView<Collection> collectionViewList = null;
            collectionViewList = collectionDAC.GetAllCollectionDAC(designDoc, viewName, bucket);
            List<Menu> list = new List<Menu>();
            foreach (var col in collectionViewList.Where(r => r.status && r.arrayProduct != null && r.arrayProduct.Where(p => p.status).Any()))
            {
                Menu menu = new Menu();
                menu.id = col._id;
                menu.name = col.name;
                menu.url = col.image;
                menu.siteID = bucket;
                list.Add(menu);
            }
            return list;
        }
        public IView<ProductItem> GetProductItem(string designDoc, string viewName, EntityPaging paging, string bucket)
        {
            ProductItemDAC OrderDAC = new ProductItemDAC(bucketSmoovPOS);

            //var countAll = 0;
            var skip = (paging.pageGoTo - 1) * paging.limit;
            var results = OrderDAC.GetProductItemDescendingByLimit(designDoc, viewName, bucket, true, paging.limit + 1, skip);

            return results;
        }
        public IView<ProductItem> GetAllActiveProductItem(string designDoc, string viewName, string bucket)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            return productItemDAC.GetAllProducItem(designDoc, viewName).Key(bucket);
        }

        public IView<ProductItem> GetAllActiveProductItem(string designDoc, string viewName, EntityPaging paging, string bucket)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> productItemList = productItemDAC.GetAllProducItem(designDoc, viewName).Key(bucket);

            if (productItemList.Count() > 0)
            {
                if (!paging.desc)
                {
                    productItemList.Descending(false);
                }
                else
                {
                    productItemList.Descending(true);
                }

                paging.startKey = productItemList.ElementAt((paging.pageGoTo - 1) * paging.limit)._id;
                paging.nextKey = null;

                int LastOfPage = productItemList.Count() / paging.limit;
                int percent = productItemList.Count() % paging.limit;
                if (percent > 0)
                {
                    LastOfPage += 1;
                }
                if (paging.pageGoTo == LastOfPage)
                {
                    paging.startKey = productItemList.ElementAt((paging.pageGoTo - 1) * paging.limit)._id;
                    paging.nextKey = productItemList.ElementAt(productItemList.Count() - 1)._id;
                }

                if (!string.IsNullOrEmpty(paging.startKey))
                {
                    if (!paging.allowStale) productItemList.Stale(StaleMode.False);
                    productItemList.StartKey(paging.startKey);
                    if (!string.IsNullOrEmpty(paging.nextKey)) productItemList.EndKey(paging.nextKey);
                }
                if (paging.limit > 0) productItemList.Limit(paging.limit * 5);
            }

            return productItemList;
        }

        public IView<ProductItem> GetAllActiveProductItemByCategory(string designDoc, string viewName, string categoryName, EntityPaging paging, string bucket)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> productItemList = productItemDAC.GetAllProducItem(designDoc, viewName).StartKey(new string[] { bucket, categoryName, "uefff" }).EndKey(new string[] { bucket, categoryName }).Descending(true);
            if (productItemList.Count() > 0)
            {
                paging.startKey = productItemList.ElementAt((paging.pageGoTo - 1) * paging.limit).DateTimeInt;
                productItemList.StartKey(new string[] { bucket, categoryName, paging.startKey });
                productItemList.Limit(paging.limit * 2);
            }
            return productItemList;
        }

        public ProductItem GetProductItemDetail(string id, string bucket)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            ProductItem productItem = productItemDAC.DetailDAC(id);
            return productItem;
        }

        public int CountAllActiveProductItem(string designDoc, string viewName, string bucket)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> productItemList = null;
            productItemList = productItemDAC.GetAllProducItem(designDoc, viewName);
            int count = productItemList.Count();
            return count;
        }

        public int CountAllActiveProductItemByCategory(string designDoc, string viewName, string categoryName, string bucket)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> productItemList = productItemDAC.GetAllProducItem(designDoc, viewName);
            productItemList.Key(categoryName);
            return productItemList.Count();
        }

        public IEnumerable<ProductItem> SearchAllActiveProductItem(string designDoc, string viewName, string searchKey, string bucket)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> productItemList = productItemDAC.GetAllProducItem(designDoc, viewName).Key(new object[] { bucket, ConstantSmoovs.Stores.OnlineStore });

            return productItemList.Where(r => string.IsNullOrWhiteSpace(searchKey) ||
                (!string.IsNullOrWhiteSpace(r.title) && r.title.ToUpper().Contains(searchKey.ToUpper()) && r.status == true) ||

                (r.countOfSoldItem.Equals(searchKey)));

        }

        public IEnumerable<ProductItem> SearchBestSellerProductItem(string designDoc, string viewName, string bucket, int countBestSeller)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> productItemList = productItemDAC.GetAllProducItem(designDoc, viewName);

            return productItemList.Where(r => r.countOfSoldItem >= countBestSeller);
        }

        public IEnumerable<ProductItem> SearchProductItemHaveBestSeller(string designDoc, string viewName, string searchKey, string bucket, int countBestSeller)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> productItemList = productItemDAC.GetAllProducItem(designDoc, viewName);

            return productItemList.Where(r => r.title.ToUpper().Contains(searchKey.ToUpper()));
        }
    }
}