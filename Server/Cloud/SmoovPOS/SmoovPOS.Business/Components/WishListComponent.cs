﻿using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class WishListComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateWishList(WishList wishlist, string bucket)
        {
            wishlist.bucket = bucket;
            WishListDAC wishListDAC = new WishListDAC(bucketSmoovPOS);
            int returnCode = wishListDAC.CreateDAC(wishlist);
            return returnCode;
        }
        public int UpdateWishList(WishList wishlist, string bucket)
        {
            wishlist.bucket = bucket;
            WishListDAC wishListDAC = new WishListDAC(bucketSmoovPOS);
            int returnCode = wishListDAC.UpdateDAC(wishlist);
            return returnCode;
        }
        public int DeleteWishList(string id, string bucket)
        {
            WishListDAC wishListDAC = new WishListDAC(bucketSmoovPOS);
            int returnCode = wishListDAC.deleteDAC(id);
            return returnCode;
        }

        public WishList GetWishListWithEmail(string designDoc, string viewName, string email, string bucket)
        {
            WishListDAC wishListDAC = new WishListDAC(bucketSmoovPOS);
            IView<WishList> listWishList = null;
            listWishList = wishListDAC.GetAll(designDoc, viewName).Key(new string[] { bucket, email });
            WishList wishlist = null;
            if (listWishList.Count() == 1) {
                wishlist = listWishList.FirstOrDefault();
            }
            return wishlist;
        }
    }
}