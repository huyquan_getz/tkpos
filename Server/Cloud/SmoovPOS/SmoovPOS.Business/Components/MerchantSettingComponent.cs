﻿using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class MerchantSettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public IView<MerchantSetting> GetAllMerchantSetting(string designDoc, string viewName, string bucket)
        {
            MerchantSettingDAC merchantSettingDAC = new MerchantSettingDAC(bucketSmoovPOS);
            IView<MerchantSetting> listmerchantSetting = null;
            listmerchantSetting = merchantSettingDAC.GetAllDAC(designDoc, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll).Key(bucket);
            return listmerchantSetting;
        }

        public int UpdateMerchantSetting(MerchantSetting setting, string bucket)
        {
            setting.bucket = bucket;
            MerchantSettingDAC merchantSettingDAC = new MerchantSettingDAC(bucketSmoovPOS);
            int StatusCode = merchantSettingDAC.UpdateDAC(setting);
            return StatusCode;
        }
    }
}