﻿using Com.SmoovPOS.Model;
using SmoovPOS.DataSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class SiteComponent
    {
        public bool CreateSite(SiteModel result)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = new Site();

                    model.SiteID = result.SiteID;
                    model.Name = result.Name;
                    model.Type = result.Type;
                    model.UserRegistration = result.UserRegistration;
                    model.ExpiryDate = result.ExpiryDate;

                    db.Sites.InsertOnSubmit(model);
                    db.SubmitChanges();

                    return true;
                }

            }
            catch
            {
            }

            return false;
        }
        public bool CreateSiteAlias(SiteAliasModel result)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = new SiteAlia();

                    model.SiteAliasID = result.SiteAliasID;
                    model.SiteID = result.SiteID;
                    model.HTTPAlias = result.HTTPAlias;
                    model.IsPrimary = result.IsPrimary;
                    model.CheckSubdomain = true;

                    db.SiteAlias.InsertOnSubmit(model);
                    db.SubmitChanges();

                    return true;
                }

            }
            catch
            {
            }

            return false;
        }
        public bool UpdateSite(SiteModel result, string siteID)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from s in db.Sites
                                 where s.SiteID.Equals(siteID)
                                 select s).FirstOrDefault();

                    model.SiteID = result.SiteID;
                    model.Name = result.Name;
                    //model.Type = result.Type;
                    //model.UserRegistration = result.UserRegistration;
                    model.ExpiryDate = result.ExpiryDate;

                    //db.Sites.InsertOnSubmit(model);
                    db.SubmitChanges();

                    return true;
                }

            }
            catch
            {
            }

            return false;
        }

        public bool UpdateSiteAlias(SiteAliasModel result, string siteAliasID)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from s in db.SiteAlias
                                 where s.SiteAliasID.Equals(siteAliasID)
                                 select s).FirstOrDefault();

                    //model.SiteAliasID = result.SiteAliasID;
                    //model.SiteID = result.SiteID;
                    model.HTTPAlias = result.HTTPAlias;
                    model.IsPrimary = result.IsPrimary;
                    model.HTTP = result.HTTP;
                    model.CheckSubdomain = result.CheckSubdomain;

                    //db.SiteAlias.InsertOnSubmit(model);
                    db.SubmitChanges();

                    return true;
                }

            }
            catch
            {
            }

            return false;
        }
        public SiteAliasModel UpdateSiteAlias(string siteID)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var result = (from s in db.SiteAlias
                                  where s.SiteAliasID.Equals(siteID)
                                  select s).FirstOrDefault();

                    var model = new SiteAliasModel();
                    model.SiteAliasID = result.SiteAliasID;
                    model.SiteID = result.SiteID;
                    model.HTTPAlias = result.HTTPAlias;
                    model.IsPrimary = result.IsPrimary;
                    model.HTTP = result.HTTP;
                    model.CheckSubdomain = result.CheckSubdomain;

                    return model;
                }

            }
            catch
            {
            }

            return null;
        }
        public bool ExistSiteAliasByUrl(string url, bool isSubDomain, string siteID)
        {
            using (var db = new EntitiesDataContext())
            {
                var isExist = false;
                if (isSubDomain)
                {
                    isExist = (from m in db.SiteAlias
                               where m.HTTPAlias.ToUpper().Contains(url.Trim().ToUpper()) &&
                                     m.SiteID != siteID
                               select m).Any();
                }
                else
                {
                    isExist = (from m in db.SiteAlias
                               where m.HTTP.ToUpper().Contains(url.Trim().ToUpper()) &&
                                     m.SiteID != siteID
                               select m).Any();
                }
                return isExist;
            }
        }

        public SiteAliasModel GetSiteAlias(string bucket)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var result = (from s in db.SiteAlias
                                  where s.SiteAliasID.Equals(bucket)
                                  select s).FirstOrDefault();

                    var model = new SiteAliasModel();

                    model.SiteAliasID = result.SiteAliasID;
                    model.SiteID = result.SiteID;
                    model.HTTPAlias = result.HTTPAlias;
                    model.IsPrimary = result.IsPrimary;
                    model.HTTP = result.HTTP;
                    model.CheckSubdomain = result.CheckSubdomain;

                    return model;
                }

            }
            catch
            {
            }

            return null;
        }

        public SiteAliasModel GetSiteAliasByHost(string host)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var result = (from s in db.SiteAlias
                                  join a in db.Sites on s.SiteID equals a.SiteID
                                  join m in db.Merchants on a.SiteID equals m.bucket
                                  where
                                  m.status == true &&
                                  ((s.CheckSubdomain == true && s.HTTPAlias.Contains(host)) ||
                                  (s.CheckSubdomain != true && s.HTTP.Contains(host)))
                                  select s).FirstOrDefault();
                    if (result != null && !string.IsNullOrEmpty(result.SiteID))
                    {
                        var model = new SiteAliasModel();

                        model.SiteAliasID = result.SiteAliasID;
                        model.SiteID = result.SiteID;
                        model.HTTPAlias = result.HTTPAlias;
                        model.IsPrimary = result.IsPrimary;
                        model.HTTP = result.HTTP;
                        model.CheckSubdomain = result.CheckSubdomain;

                        return model;
                    }
                }

            }
            catch
            {
            }

            return null;
        }
    }
}