﻿
using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Business.Components
{
    public class WebContentComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        static WebContentComponent() { }
        public int CreateNewWebContent(WebContent webContent, string bucket)
        {
            webContent.bucket = bucket;
            WebContentDAC webContentDAC = new WebContentDAC(bucketSmoovPOS);
            int StatusCode = webContentDAC.CreateDAC(webContent);
            return StatusCode;
        }
        public int UpdateWebContent(WebContent webContent, string bucket)
        {
            webContent.bucket = bucket;
            WebContentDAC webContentDAC = new WebContentDAC(bucketSmoovPOS);
            int StatusCode = webContentDAC.UpdateDAC(webContent);
            return StatusCode;
        }
        public int DeleteWebContent(string id, string bucket)
        {
            WebContentDAC webContentDAC = new WebContentDAC(bucketSmoovPOS);
            int StatusCode = webContentDAC.DeleteDAC(id);
            return StatusCode;
        }
        public WebContent DetailWebContent(string id, string bucket)
        {
            WebContentDAC webContentDAC = new WebContentDAC(bucketSmoovPOS);
            WebContent webContent = webContentDAC.DetailDAC(id);
            return webContent;
        }
        public int CountWebContent(string designDoc, string viewName, string bucket)
        {
            WebContentDAC webContentDAC = new WebContentDAC(bucketSmoovPOS);
            IView<WebContent> listWebContent = null;
            listWebContent = webContentDAC.GetAll(designDoc, viewName).Key(bucket);
            return listWebContent.Count();
        }
        public IView<WebContent> GetAllWebContent(string designDoc, string viewName, string bucket)
        {
            WebContentDAC webContentDAC = new WebContentDAC(bucketSmoovPOS);
            IView<WebContent> listWebContent = null;
            listWebContent = webContentDAC.GetAll(designDoc, viewName).Key(bucket);
            return listWebContent;
        }
    }
}