﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmoovPOS.Data.DAC;
using SmoovPOS.Common;
namespace SmoovPOS.Business.Components
{
    public class TemplateEmailComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateTemplateEmail(TemplateEmail templateEmail, string bucket)
        {
            templateEmail.bucket = bucket;
            TemplateEmailDAC templateEmailDAC = new TemplateEmailDAC(bucketSmoovPOS);
            return templateEmailDAC.CreateTemplateEmailDAC(templateEmail);
        }

        public TemplateEmail GetDetailTemplateEmail(string key, string bucket)
        {
            TemplateEmailDAC templateEmailDAC = new TemplateEmailDAC(bucketSmoovPOS);
            return templateEmailDAC.GetTemplateEmailDAC(key);
        }

        public int DeleteAllTemplateEmail(string designDoc, string viewName, string bucket)
        {
            TemplateEmailDAC templateEmailDAC = new TemplateEmailDAC(bucketSmoovPOS);
            return templateEmailDAC.DeleteAllTemplateEmailDAC(designDoc, viewName);
        }
        public int UpdateTemplateEmail(TemplateEmail templateEmail, string bucket)
        {
            templateEmail.bucket = bucket;
            TemplateEmailDAC templateEmailDAC = new TemplateEmailDAC(bucketSmoovPOS);
            int statusCode = templateEmailDAC.UpdateTemplateEmailDAC(templateEmail);
            return statusCode;
        }

        public IEnumerable<TemplateEmail> GetAllTemplateEmail(string designDoc, string viewName, string bucket)
        {
            TemplateEmailDAC templateEmailDAC = new TemplateEmailDAC(bucketSmoovPOS);
            IEnumerable<TemplateEmail> listTemplateEmail = templateEmailDAC.GetAllTemplateEmailDAC(designDoc, viewName, bucket);
            return listTemplateEmail;
        }
    }
}