﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Entity;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Business.Components
{
    public class CategoryComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        static CategoryComponent() { }
        public int CreateNewCategory(Category category, string bucket)
        {
            category.bucket = bucket;
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            int StatusCode = categoryDAC.CreateDAC(category);
            return StatusCode;
        }
        public int UpdateCategory(Category category, string bucket)
        {
            category.bucket = bucket;
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            int StatusCode = categoryDAC.UpdateDAC(category);
            return StatusCode;
        }

        /// <summary>
        /// Updates the product category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="product">The product.</param>
        /// <param name="bucket">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>3/17/2015-2:37 PM</datetime>
        public int UpdateProductCategory(String id, CategoryProductItem product, string bucket)
        {
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            Category category = categoryDAC.DetailDAC(id);
            category.arrayProduct.Add(product);
            int StatusCode = categoryDAC.UpdateDAC(category);
            return StatusCode;
        }

        public int DeleteCategory(string id, string bucket)
        {
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            int StatusCode = categoryDAC.DeleteDAC(id);
            return StatusCode;
        }
        public Category DetailCategory(string id, string bucket)
        {

            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            Category category = categoryDAC.DetailDAC(id);
            return category;
        }

        public void UpdateNameCategory(string designDoc, string viewName, string oldName, string newName, string bucket)
        {
            ProductItemDAC productItemDAC = new ProductItemDAC(bucketSmoovPOS);
            IView<ProductItem> ListProduct = null;
            ListProduct = productItemDAC.GetAllProducItemWithBucket(designDoc, viewName, bucket);
            ListProduct.Key(oldName);
            foreach (ProductItem item in ListProduct)
            {
                item.categoryName = newName;
                productItemDAC.UpdateDAC(item);
            }
        }

        public int CountAllCategory(string designDoc, string viewName, string bucket)
        {
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            IView<Category> ListCategory = null;
            ListCategory = categoryDAC.GetAll(designDoc, ConstantSmoovs.CouchBase.GetAllCategory);
            return ListCategory.Count();
        }
        public Category Get(string id, string bucket)
        {
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            Category category = categoryDAC.DetailDAC(id);
            return category;
        }

        public IView<Category> GetAllCategory(string designDoc, string viewName, string bucket)
        {
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            IView<Category> ListCategory = null;
            ListCategory = categoryDAC.GetAllWithBucket(designDoc, viewName, bucket);
            return ListCategory;
        }

        public IEnumerable<Category> GetAllCategoryActive(string designDoc, string viewName, string bucket)
        {
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            IView<Category> ListCategory = null;
            ListCategory = categoryDAC.GetAllWithBucket(designDoc, viewName, bucket);
            var List = ListCategory.Where(r => r.status);
            return List;
        }

        public SearchCategoryModel SearchAllCategory(string designDoc, string viewName, string bucket, NewPaging paging)
        {
            CategoryDAC categoryDAC = new CategoryDAC(bucketSmoovPOS);
            IEnumerable<Category> listSearch = null;
            listSearch = categoryDAC.GetAllWithBucket(designDoc, viewName, bucket);
            SearchCategoryModel model = new SearchCategoryModel();
            model.Paging = new NewPaging();
            if (listSearch != null)
            {
                if (paging.type == "title")
                {
                    listSearch = listSearch.Where(m => m.title.ToUpper().Contains(paging.text.ToUpper()));
                }

                if (paging.sort == "title")
                {
                    if (paging.desc)
                    {
                        listSearch = listSearch.OrderByDescending(m => m.title);
                    }
                    else
                    {
                        listSearch = listSearch.OrderBy(m => m.title);
                    }
                }

                model.Paging.total = listSearch.Count();
                model.Paging.pageGoTo = paging.pageGoTo;
                model.Paging.limit = paging.limit;
                model.Paging.text = paging.text;
                model.Paging.sort = paging.sort;
                model.Paging.desc = paging.desc;
                model.Paging.type = paging.type;
                listSearch = listSearch.Skip((paging.pageGoTo - 1) * paging.limit).Take(paging.limit).ToList();
                model.Categories = listSearch;
                return model;
            }
            else
            {
                return model;
            }
        }

    }
}