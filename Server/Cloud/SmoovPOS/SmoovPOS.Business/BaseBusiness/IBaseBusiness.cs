﻿using Com.SmoovPOS.Model;

namespace Com.SmoovPOS.Business
{
    /// <summary>
    /// Interface Base
    /// </summary>
    /// <typeparam name="T">T</typeparam>
    public partial interface IBaseBussiness<T> where T : BaseModel
    {
        /// <summary>
        /// Adds a new record to the DB
        /// </summary>
        /// <param name="entity">Object entity to add</param>
        /// <param name="isTransaction">if set to <c>true</c> [is transaction].</param>
        /// <returns>
        /// Object entity return
        /// </returns>
        T Save(T entity, bool isTransaction);

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        void Delete(T entity);

        /// <summary>
        /// Delete the entity by id
        /// </summary>
        /// <param name="id">The id.</param>
        void Delete(int id);

        /// <summary>
        /// Select object with by Id.
        /// Method must used to edit object
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        T Select(int id);
    }
}