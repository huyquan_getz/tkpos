﻿using Com.SmoovPOS.Model;
using SmoovPOS.DataSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SmoovPOS.Business.Business
{
    public class PaymentBusiness
    {
        private MVCDatabase db = new MVCDatabase();
        /// <summary>
        /// Add Or Update paymentgateway 
        /// </summary>
        /// <param name="id">string</param>
        /// <param name="payment">PaymentModel</param>
        /// <returns>true or false</returns>
        public bool AddOrUpdatePaymentGateway(string id,PaymentModel payment) {
            bool result = false;
            try
            { 
                // check paymentgateway exist 
                if (id.Equals(""))
                {
                    payment.Id = System.Guid.NewGuid().ToString();
                    db.Payments.Add(payment);
                    db.SaveChanges();

                }else {
                    PaymentModel paymentgateway = db.Payments.Find(id);
                    paymentgateway.CancelPage = payment.CancelPage;
                    paymentgateway.SuccessPage = payment.SuccessPage;
                    paymentgateway.Status = payment.Status;
                    paymentgateway.SmoovUrl = payment.SmoovUrl;
                    paymentgateway.StrUrl = payment.StrUrl;
                    paymentgateway.SandboxSmoovUrl = payment.SandboxSmoovUrl;
                    paymentgateway.ModifiedDate = payment.ModifiedDate;
                    db.Entry(paymentgateway).State = EntityState.Modified;
                    db.SaveChanges();
                }
                result = true;
            }
            catch (Exception e) {
                e.Message.ToString();
            }
            return result;
        }
        /// <summary>
        /// Get PaymentGateway of server information
        /// </summary>
        /// <returns>PaymentModel</returns>
        public PaymentModel GetPaymentGateway() {
            PaymentModel paymentgateway = new PaymentModel();
           // string site = "http://store1.demo.smoovpos-dev.com/";
            try
            {
                paymentgateway = db.Payments.SingleOrDefault();
              //  paymentgateway.SuccessPage = site + paymentgateway.SuccessPage;
              //  paymentgateway.CancelPage = site + paymentgateway.CancelPage;
              //  paymentgateway.StrUrl = site + paymentgateway.StrUrl;
            }
            catch (Exception e) {
                e.Message.ToString();
                return paymentgateway;
            }
            return paymentgateway;
        }
    }
}