﻿using SmoovPOS.DataSql;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Business
{
    public class MailConfigBusiness
    {
        // connection to database
        //private MVCDatabase db = new MVCDatabase();
        /// <summary>
        /// Add or update config server mail
        /// </summary>
        /// <param name="id">string</param>
        /// <param name="mail">object model</param>
        /// <returns>true or false</returns>
        public bool AddOrUpdateMailServer(string id, MailServerModel mail, Guid? merchantID)
        {
            bool result = false;
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    // check config mail exist 
                    MailConfig mail_ = new MailConfig();

                    if (merchantID != null && merchantID.Value != Guid.Empty)
                    {
                        mail_ = (from m in db.MailConfigs
                                 where (m.merchantID != null && m.merchantID.Value == merchantID.Value)
                                 select m).FirstOrDefault();
                    }
                    else
                    {
                        mail_ = (from m in db.MailConfigs
                                 where (m.Id == id)
                                 select m).FirstOrDefault();
                        //with admin config
                        //if (mails.Count <= 0 && string.IsNullOrEmpty(id))
                        //{
                        //    mails = (from m in db.MailConfigs
                        //             where (m.merchantID == null)
                        //             select m).OrderBy(m => m.CreateDate).ToList();
                        //}
                        //2 mail config
                        //if (mails.Count > type)
                        //{
                        //    mail_ = mails[type];
                        //}
                        //else
                        //{
                        //    mail_ = null;
                        //}
                    }

                    if (mail_ == null)
                    {
                        mail_ = new MailConfig();
                        mail_.Id = Guid.NewGuid().ToString();

                        mail_.IncomingPop = mail.IncomingPop;
                        mail_.IncomingPort = mail.IncomingPort;
                        mail_.UserName = mail.UserName;
                        mail_.PassWord = mail.PassWord;
                        mail_.OutgoingPort = mail.OutgoingPort;
                        mail_.OutgoingSmtp = mail.OutgoingSmtp;
                        mail_.ModifiedDate =DateTime.UtcNow;
                        mail_.Credentials = mail.Credentials;
                        mail_.EnableSsl = mail.EnableSsl;
                        //mail_.UserName1 = mail.UserName1;
                        //mail_.PassWord1 = mail.PassWord1;
                        //mail_.UserName2 = mail.UserName2;
                        //mail_.PassWord2 = mail.PassWord2;
                        //mail_.UserName3 = mail.UserName3;
                        //mail_.PassWord3 = mail.PassWord3;
                        //mail_.isApply = mail.isApply;

                        mail_.CreateDate = DateTime.UtcNow;

                        if (merchantID != null && merchantID.Value != Guid.Empty)
                        {
                            mail_.merchantID = merchantID;
                        }

                        db.MailConfigs.InsertOnSubmit(mail_);
                    }
                    else
                    {
                        mail_.IncomingPop = mail.IncomingPop;
                        mail_.IncomingPort = mail.IncomingPort;
                        mail_.UserName = mail.UserName;
                        mail_.PassWord = mail.PassWord;
                        mail_.OutgoingPort = mail.OutgoingPort;
                        mail_.OutgoingSmtp = mail.OutgoingSmtp;
                        mail_.ModifiedDate = DateTime.UtcNow;
                        mail_.Credentials = mail.Credentials;
                        mail_.EnableSsl = mail.EnableSsl;
                        //mail_.UserName1 = mail.UserName1;
                        //mail_.PassWord1 = mail.PassWord1;
                        //mail_.UserName2 = mail.UserName2;
                        //mail_.PassWord2 = mail.PassWord2;
                        //mail_.UserName3 = mail.UserName3;
                        //mail_.PassWord3 = mail.PassWord3;
                        //mail_.isApply = mail.isApply;

                        if (mail_.CreateDate <= new DateTime(1753, 1, 1, 0, 0, 1))
                        {
                            mail_.CreateDate = DateTime.Now;
                        }
                        if (merchantID != null && merchantID.Value != Guid.Empty)
                        {
                            mail_.merchantID = merchantID;
                        }

                    }

                    db.SubmitChanges();
                }
                //if (id.Equals(""))
                //{
                //    mail.Id = System.Guid.NewGuid().ToString();
                //    db.MailServers.Add(mail);
                //    db.SaveChanges();

                //}
                //else
                //{
                //    MailServerModel mail_ = db.MailServers.Find(id);
                //    mail_.IncomingPop = mail.IncomingPop;
                //    mail_.IncomingPort = mail.IncomingPort;
                //    mail_.UserName = mail.UserName;
                //    mail_.PassWord = mail.PassWord;
                //    mail_.OutgoingPort = mail.OutgoingPort;
                //    mail_.OutgoingSmtp = mail.OutgoingSmtp;
                //    mail_.ModifiedDate = mail.ModifiedDate;
                //    mail_.Credentials = mail.Credentials;
                //    mail_.EnableSsl = mail.EnableSsl;
                //    mail_.UserName1 = mail.UserName1;
                //    mail_.PassWord1 = mail.PassWord1;
                //    mail_.UserName2 = mail.UserName2;
                //    mail_.PassWord2 = mail.PassWord2;
                //    mail_.UserName3 = mail.UserName3;
                //    mail_.PassWord3 = mail.PassWord3;

                //    db.Entry(mail_).State = EntityState.Modified;
                //    db.SaveChanges();
                //}

                result = true;
            }
            catch (Exception e)
            {
                e.Message.ToString();
            }
            return result;
        }

        public bool ApplyPrivateMailServer(Guid merchantID, bool isApply)
        {
            bool result = false;
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    // check config mail exist 
                    MailConfig mail_ = new MailConfig();

                    if (merchantID != null && merchantID != Guid.Empty)
                    {
                        mail_ = (from m in db.MailConfigs
                                 where (m.merchantID != null && m.merchantID.Value == merchantID)
                                 select m).FirstOrDefault();

                        if (mail_ != null)
                        {
                            mail_.isApply = isApply;
                            mail_.ModifiedDate = DateTime.UtcNow;

                            db.SubmitChanges();
                        }
                    }
                }
                result = true;
            }
            catch
            {
            }
            return result;
        }
        /// <summary>
        /// Get config mail server of server information
        /// </summary>
        /// <returns>PaymentModel</returns>
        public MailServerModel GetConfigMailServer(Guid? merchantID, int type = 0)
        {
            using (var db = new EntitiesDataContext())
            {
                // check config mail exist 
                var mail_ = new MailServerModel();
                if (merchantID != null && merchantID.Value != Guid.Empty)
                {
                    mail_ = (from mail in db.MailConfigs
                             where (mail.merchantID.Value == merchantID.Value)
                             select new MailServerModel
                             {
                                 Id = mail.Id,
                                 IncomingPop = mail.IncomingPop,
                                 IncomingPort = mail.IncomingPort,
                                 UserName = mail.UserName,
                                 PassWord = mail.PassWord,
                                 OutgoingPort = mail.OutgoingPort,
                                 OutgoingSmtp = mail.OutgoingSmtp,
                                 ModifiedDate = mail.ModifiedDate,
                                 Credentials = mail.Credentials,
                                 EnableSsl = mail.EnableSsl,
                                 //UserName1 = mail.UserName1,
                                 //PassWord1 = mail.PassWord1,
                                 //UserName2 = mail.UserName2,
                                 //PassWord2 = mail.PassWord2,
                                 //UserName3 = mail.UserName3,
                                 //PassWord3 = mail.PassWord3,
                                 merchantID = mail.merchantID,
                                 isApply = mail.isApply
                             }).FirstOrDefault();
                    if (mail_ == null)
                    {
                        mail_ = GetMailConfigOfAdmin(type, db, mail_);
                    }
                }
                else
                {
                    mail_ = GetMailConfigOfAdmin(type, db, mail_);

                    //2 mail config
                    //if (mails.Count > type)
                    //{
                    //    mail_ = mails[type];
                    //}
                    //else
                    //{
                    //    mail_ = null;
                    //}

                    //mail_ = (from mail in db.MailConfigs
                    //         where (mail.merchantID == null)
                    //         select new MailServerModel
                    //         {
                    //             Id = mail.Id,
                    //             IncomingPop = mail.IncomingPop,
                    //             IncomingPort = mail.IncomingPort,
                    //             UserName = mail.UserName,
                    //             PassWord = mail.PassWord,
                    //             OutgoingPort = mail.OutgoingPort,
                    //             OutgoingSmtp = mail.OutgoingSmtp,
                    //             ModifiedDate = mail.ModifiedDate,
                    //             Credentials = mail.Credentials,
                    //             EnableSsl = mail.EnableSsl,
                    //             //UserName1 = mail.UserName1,
                    //             //PassWord1 = mail.PassWord1,
                    //             //UserName2 = mail.UserName2,
                    //             //PassWord2 = mail.PassWord2,
                    //             //UserName3 = mail.UserName3,
                    //             //PassWord3 = mail.PassWord3,
                    //             merchantID = mail.merchantID,
                    //             isApply = mail.isApply
                    //         }).FirstOrDefault();

                }

                return mail_;
            }
            //MailServerModel mailServer = new MailServerModel();
            //try
            //{
            //    mailServer = db.MailServers.Single();
            //}
            //catch (Exception e)
            //{
            //    e.Message.ToString();
            //}
            //return mailServer;
        }

        private static MailServerModel GetMailConfigOfAdmin(int type, EntitiesDataContext db, MailServerModel mail_)
        {
            mail_ = (from mail in db.MailConfigs
                     where (mail.merchantID == null) && mail.type == type
                     select new MailServerModel
                     {
                         Id = mail.Id,
                         IncomingPop = mail.IncomingPop,
                         IncomingPort = mail.IncomingPort,
                         UserName = mail.UserName,
                         PassWord = mail.PassWord,
                         OutgoingPort = mail.OutgoingPort,
                         OutgoingSmtp = mail.OutgoingSmtp,
                         ModifiedDate = mail.ModifiedDate,
                         Credentials = mail.Credentials,
                         EnableSsl = mail.EnableSsl,
                         merchantID = mail.merchantID,
                         isApply = mail.isApply
                     }).FirstOrDefault();
            return mail_;
        }
    }
}