﻿using Com.SmoovPOS.Business.Components;
using Com.SmoovPOS.Entity;
using Newtonsoft.Json.Linq;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using SmoovPOS.API.Controllers;

namespace SmoovPOS.UI.Controllers.WebServicesPOS
{
    public class CustomerPOSController : BaseController
    {
        // GET: CustomerPOS
        [HttpPost]
        public ActionResult WSCreateCustomer(string Authorization, string result)
        {
            Customer customer = null;
            if (!string.IsNullOrEmpty(result))
            {
                JObject jObject = JObject.Parse(result);

                customer = jObject.ToObject<Customer>();
            }

            if (!string.IsNullOrEmpty(Authorization) && customer != null)
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    if (authen.expires_in > 0)
                    {
                        CustomerComponent customerComponent = new CustomerComponent();
                        if (string.IsNullOrEmpty(customer._id) || customer._id == Guid.Empty.ToString())
                        {
                            customer._id = Guid.NewGuid().ToString();
                        }

                        var returnCode = customerComponent.CreateNewCustomer(customer, authen.databaseSync);
                        if (returnCode == 0)
                        {
                            returnCode = 1;
                        }
                        else
                        {
                            returnCode = 2;
                        }
                        //Success 
                        return Json(new { status = returnCode, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //AccessTokenExpired 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //AccessTokenNotProvided 
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //CommonInvalidRequest 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
            }
            //return 0;
            //CustomerComponent customerComponent = new CustomerComponent();
            //return customerComponent.CreateNewCustomer(customer, siteID);
        }
        [HttpPost]
        public ActionResult WSUpdateCustomer(string Authorization, string result)
        {
            Customer customer = null;
            if (!string.IsNullOrEmpty(result))
            {
                JObject jObject = JObject.Parse(result);

                customer = jObject.ToObject<Customer>();
            }

            if (!string.IsNullOrEmpty(Authorization) && customer != null)
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    if (authen.expires_in > 0)
                    {
                        CustomerComponent customerComponent = new CustomerComponent();
                        var returnCode = customerComponent.UpdateCustomer(customer, authen.databaseSync);
                        if (returnCode == 0)
                        {
                            returnCode = 1;
                        }
                        else
                        {
                            returnCode = 2;
                        }
                        return Json(new { status = returnCode, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //AccessTokenExpired 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //AccessTokenNotProvided 
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //CommonInvalidRequest 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
            }
            //return 0;
            //CustomerComponent customerComponent = new CustomerComponent();
            //return customerComponent.CreateNewCustomer(customer, siteID);
        }
        [HttpPost]
        public ActionResult WSGetCustomer(string Authorization, string id)
        {
            if (!string.IsNullOrEmpty(Authorization) && !string.IsNullOrEmpty(id))
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    if (authen.expires_in > 0)
                    {
                        CustomerComponent customerComponent = new CustomerComponent();
                        Com.SmoovPOS.Entity.Customer customer = customerComponent.DetailCustomer(id, authen.databaseSync);

                        return Json(new { status = 1, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = customer }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //AccessTokenExpired 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //AccessTokenNotProvided 
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //CommonInvalidRequest 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
            }
            //return 0;
            //CustomerComponent customerComponent = new CustomerComponent();
            //return customerComponent.CreateNewCustomer(customer, siteID);
        }
        [HttpPost]
        public ActionResult WSDeleteCustomer(string Authorization, string id)
        {
            if (!string.IsNullOrEmpty(Authorization) && !string.IsNullOrEmpty(id))
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    if (authen.expires_in > 0)
                    {
                        CustomerComponent customerComponent = new CustomerComponent();
                        int returnCode = customerComponent.DeleteCustomer(id, authen.databaseSync);
                        if (returnCode == 0)
                        {
                            returnCode = 1;
                        }
                        else
                        {
                            returnCode = 2;
                        }
                        return Json(new { status = 1, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //AccessTokenExpired 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //AccessTokenNotProvided 
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //CommonInvalidRequest 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
            }
            //return 0;
            //CustomerComponent customerComponent = new CustomerComponent();
            //return customerComponent.CreateNewCustomer(customer, siteID);
        }
        [HttpPost]
        public ActionResult WSGetAllCustomer(string Authorization)
        {
            if (!string.IsNullOrEmpty(Authorization))
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    if (authen.expires_in > 0)
                    {
                        CustomerComponent customerComponent = new CustomerComponent();
                        IEnumerable<Com.SmoovPOS.Entity.Customer> ListCustomer = customerComponent.GetAllCustomer(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.GetAllCustomer, authen.databaseSync);
                        var lst = (from c in ListCustomer
                                   where c.display == true
                                   select new
                                   {
                                       id = c._id,
                                       firstName = c.firstName,
                                       lastName = c.lastName,
                                       fullName = c.fullName,
                                       phoneRegion = c.phoneRegion,
                                       phone = c.phone
                                   }).OrderBy(c => c.fullName).ToList();

                        return Json(new { status = 1, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = lst }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //AccessTokenExpired 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //AccessTokenNotProvided 
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //CommonInvalidRequest 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (Customer)null }, JsonRequestBehavior.AllowGet);
            }
        }

        #region private function

        #endregion
    }
}