﻿using Com.SmoovPOS.Entity;
using Newtonsoft.Json;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using SmoovPOS.Entity.Models;
using SmoovPOS.Utility.Controllers;
using SmoovPOS.Utility.WSOrderReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.MailServer;
using SmoovPOS.Utility.WSCustomerReference;
using SmoovPOS.Utility.WSEmailTemplateReference;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using System.Text;
using System.Web.Routing;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.Models;
using SmoovPOS.UI.Models;
using SmoovPOS.UI.Controllers;
using SmoovPOS.Entity.Entity;
using SmoovPOS.Business.Business;
using Com.SmoovPOS.Model;
using Com.SmoovPOS.TimeZone;
using SmoovPOS.API.Models;
using SmoovPOS.API.Controllers;
using System.Threading.Tasks;

namespace SmoovPOS.Service
{
    public class SendMailPOSController : BaseController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        public async Task<ActionResult> WSSendMailReceipt(string Authorization, string stringOrder, string businessName, string emailCustomer, string typeEmail)
        {
            logger.Info(string.Format("send mail Receipt with token: {0}", Authorization));
            logger.Info(string.Format("string order: {0}", stringOrder));
            WSOrderClient client = new WSOrderClient();

            var order = JsonConvert.DeserializeObject<Order>(stringOrder);
            if (order == null)
            {
                logger.Error("order is null");
            }
            //  order = client.WSGetOrder("00473829-e004-4ec2-bcb2-ed6a79827345", "smoovapp_14219795975512226");
            client.Close();
            if (!string.IsNullOrEmpty(Authorization))
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    if (authen.expires_in > 0)
                    {
                        var returnCode = 0;
                        returnCode = await SendMailReceipt(order, businessName, emailCustomer, typeEmail) == true ? 1 : 2;

                        //return Json(new { returnCode = returnCode }, JsonRequestBehavior.AllowGet);

                        //}
                        //else
                        //{
                        //    logger.Error(string.Format("merchantManagement.GetUserSessionByToken{0} is null", Authorization));
                        //    return Json(new { returnCode = 1000 }, JsonRequestBehavior.AllowGet);
                        //}
                        //Success 
                        return Json(new { status = returnCode, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (string)null }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        logger.Error(string.Format("AccessTokenExpired {0}", Authorization));
                        //AccessTokenExpired 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //AccessTokenNotProvided 
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //CommonInvalidRequest 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Wses the send mail cash drawer.
        /// </summary>
        /// <param name="Authorization">The token.</param>
        /// <param name="data">The data.</param>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="emailCustomer">The email customer.</param>
        /// <param name="typeEmail">The type email.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/12/2015-4:27 PM</datetime>
        [HttpPost]
        public async Task<ActionResult> WSSendMailCashDrawer(string Authorization, string data, string businessName, string emailCustomer, string typeEmail)
        {
            logger.Info(string.Format("send mail Cash Drawer with token: {0}", Authorization));
            if (!string.IsNullOrEmpty(Authorization))
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    if (authen.expires_in > 0)
                    {
                        var returnCode = 2;
                        if (typeEmail.Equals("CashDrawerReport"))
                        {
                            var cashDrawer = JsonConvert.DeserializeObject<CashDrawerReport>(data);

                            returnCode = await SendMailCashDrawer(cashDrawer, businessName, emailCustomer, typeEmail) == true ? 1 : 2;
                            //return Json(new { returnCode = returnCode }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { status = returnCode, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (string)null }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //AccessTokenExpired 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //AccessTokenNotProvided 
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //CommonInvalidRequest 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> WSSendMailOrder(string Authorization, string data, string typeEmail)
        {
            logger.Info(string.Format("send mail Order with token: {0}", Authorization));
            if (!string.IsNullOrEmpty(Authorization))
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    if (authen.expires_in > 0)
                    {
                        IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                        var merchant = client.WSGetMerchantManagementBySiteId(authen.databaseSync);

                        var orderPOS = JsonConvert.DeserializeObject<OrderPOS>(data);
                        var orderSendMail = GetDetailOrder(orderPOS);
                        var returnCode = 2;
                        if (typeEmail == "Cancel")
                        {
                            string viewToRender = "~/View/SendMailPOS/CancelOrder.cshtml";
                            returnCode =await  SendMailCancelRejectOrder(orderSendMail.MerchantSetting, orderSendMail, orderPOS, viewToRender, ConstantSmoovs.BodyEmail.CustomerCancelOrder, ConstantSmoovs.TitleEmail.CancelOrderNo, merchant);
                        }
                        else if (typeEmail == "Reject")
                        {
                            string viewToRender = "~/View/SendMailPOS/RejectOrder.cshtml";
                            returnCode = await SendMailCancelRejectOrder(orderSendMail.MerchantSetting, orderSendMail, orderPOS, viewToRender, ConstantSmoovs.BodyEmail.CustomerRejectOrder, ConstantSmoovs.TitleEmail.RejectCancelOrderNo, merchant);
                        }
                        else if (typeEmail == "Refund")
                        {
                            string viewToRender = "~/View/SendMailPOS/RefundOrder.cshtml";
                            returnCode =await  SendMailCancelRejectOrder(orderSendMail.MerchantSetting, orderSendMail, orderPOS, viewToRender, ConstantSmoovs.BodyEmail.CustomerRefundOrder, ConstantSmoovs.TitleEmail.RefundOrderNo, merchant);
                        }
                        else if (typeEmail == "Ready")
                        {
                            string viewToRender = "~/View/SendMailPOS/ReadyOrder.cshtml";
                            returnCode =await  SendMailCancelRejectOrder(orderSendMail.MerchantSetting, orderSendMail, orderPOS, viewToRender, ConstantSmoovs.BodyEmail.UpdateOrderStatus, ConstantSmoovs.TitleEmail.UpdateOrderStatus, merchant);
                        }

                        return Json(new { status = returnCode, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (string)null }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //AccessTokenExpired 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //AccessTokenNotProvided 
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //CommonInvalidRequest 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<int> SendMailCancelRejectOrder(MerchantSetting merchantSetting, SendMailOrder sendMailOrder, OrderPOS orderPOS, string viewToRender, int indexEmail, string titleEmail, MerchantManagementModel merchantInfor)
        {
            string bodyMail = "";
            StringBuilder resultBuild;
            TemplateEmail emailItem = GetTemplateEmail(indexEmail, sendMailOrder.Bucket);
            bodyMail = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
            if (string.IsNullOrWhiteSpace(bodyMail))
            {
                emailItem = GetTemplateEmail(indexEmail, "smoovpos");
                bodyMail = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
                //bodyMail = RenderRazorViewToStringNoModel(viewToRender);
            }

            resultBuild = new StringBuilder(bodyMail);

            resultBuild.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, orderPOS.FirstName);
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.OrderNumber, orderPOS.OrderNumber);
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.OnlineshopLink, sendMailOrder.MerchantManagementModel.CheckSubdomain == true ? sendMailOrder.MerchantManagementModel.SubDomain : sendMailOrder.MerchantManagementModel.Domain);
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.BusinessName, orderPOS.BusinessName);
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.ReasonOrder, orderPOS.Reason);
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.OrderStatusDisplayForCustomer, "Ready For Collection");

            //resultBuild.Replace(ConstantSmoovs.EmailKeyword.ReasonOrder, "");
            bodyMail = resultBuild.ToString();
          
            bool isSend = await Task.Run(() => sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, orderPOS.BusinessName, orderPOS.EmailCustomer, titleEmail + orderPOS.OrderNumber, bodyMail, merchantInfor));
                
            
            return isSend == true ? 1 : 2;


        }

        public TemplateEmail GetTemplateEmail(int indexEmail, string bucket)
        {
            try
            {
                IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
                TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, bucket);
                TemplateEmail emailItem = listEmail.FirstOrDefault(r => r.index == indexEmail);
                clientEmail.Close();

                return emailItem;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        public SendMailOrder GetDetailOrder(OrderPOS orderMail)
        {
            string siteID = "";
            IWSMerchantManagementClient ManagementClient = new IWSMerchantManagementClient();
            var merchant = ManagementClient.WSGetMerchantManagementByBusinessName(orderMail.BusinessName);
            ManagementClient.Close();
            siteID = merchant.bucket;
            MerchantSetting merchantSetting = new MerchantSetting();
            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
            merchantSetting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, siteID);
            clientMerchantSetting.Close();
            SendMailOrder MailOrder = new SendMailOrder();
            MailOrder.Bucket = merchant.bucket;
            MailOrder.MerchantSetting = merchantSetting;
            MailOrder.MerchantManagementModel = merchant;
            return MailOrder;
        }

        public async Task<Boolean> SendMailReceipt(Order order, string businessName, string emailCustomer, string typeEmail)
        {
            try
            {
                IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                var merchant = client.WSGetMerchantManagementByBusinessName(businessName);
                if (merchant == null)
                {
                    logger.Error(string.Format("get merchant by business {0}: null", businessName));
                }
                client.Close();
                IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
                TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, merchant.bucket);
                OrderModel orderModel = new OrderModel(order, merchant.bucket);
                string bodyMail = "";
                string listProductItem = "";
                string stBody = "";
                //string emailCustomer = "";
                StringBuilder resultBuild = new StringBuilder(); ;
                string titleMail = "";

                //------------------
                string storeUrl = "";
                WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
                MerchantSetting[] listmerchants = new MerchantSetting[1];
                listmerchants = clientMerchantSetting.GetAllMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, merchant.bucket);
                storeUrl = listmerchants[0].UrlHostOnline;
                TemplateEmail emailItem = listEmail.First(r => r.index == ConstantSmoovs.BodyEmail.CustomerConfirmDeliveryOrder);
                bodyMail = (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "");
                WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
                var merchantGeneralSetting = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, merchant.bucket);
                merchantGeneralSettingClient.Close();
                if (merchantGeneralSetting != null)
                {
                    orderModel.currency = merchantGeneralSetting.Currency;
                }
                listProductItem = RenderRazorViewToString("~/Views/SendMailPOS/SendMailReceipt.cshtml", orderModel);
                resultBuild = new StringBuilder(bodyMail);
                stBody = resultBuild.ToString();
                var linkOnlineShop = merchant.CheckSubdomain == true ? merchant.SubDomain : merchant.Domain;
                resultBuild = ReplaceInformationOfOrder(stBody, orderModel, merchant.userID, linkOnlineShop);
                resultBuild.Replace(ConstantSmoovs.EmailKeyword.ProductOrderDetails, listProductItem);
                bodyMail = resultBuild.ToString();

                string title = "";
                if (typeEmail == "Receipt")
                {
                    title = ConstantSmoovs.TitleEmail.ReceiptOrder;
                }
                else if (typeEmail == "ReceiptResend")
                {
                    title = ConstantSmoovs.TitleEmail.ReceiptResendOrder;
                }
                else if (typeEmail == "ReceiptCancelResend")
                {
                    title = ConstantSmoovs.TitleEmail.ReceiptCancelResendOrder;
                }
                else if (typeEmail == "ReceiptRefundResend")
                {
                    title = ConstantSmoovs.TitleEmail.ReceiptRefundResendOrder;
                }

                titleMail = title + orderModel.orderCode;
                // emailCustomer = order.customerSender != null ? (!string.IsNullOrWhiteSpace(order.customerSender.email) ? order.customerSender.email : "") : "";
                bool isSend = await Task.Run(() => sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, merchant.firstName + " " + merchant.lastName, emailCustomer, titleMail, bodyMail, merchant));
                return isSend;
                //return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return false;
            }
        }

        public MerchantManagementModel GetBussinessModel(string UserID)
        {

            IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
            var modelMerchant = clientMerchant.WSGetMerchantManagementByUserId(UserID);
            clientMerchant.Close();
            return modelMerchant;

        }

        public StringBuilder ReplaceInformationOfOrder(string bodyMail, OrderModel orderModel, string userID, string linkOnlineShop)
        {

            StringBuilder result = new StringBuilder(bodyMail.ToString());
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, orderModel.customerFirstNameSender);
            var bussinesModel = GetBussinessModel(userID);
            string businessName = bussinesModel.businessName;
            result.Replace(ConstantSmoovs.EmailKeyword.BusinessName, businessName);
            result.Replace(ConstantSmoovs.EmailKeyword.SupportEmailOfMerchant, orderModel.emailMerchantSupport);
            result.Replace(ConstantSmoovs.EmailKeyword.OrderNumber, orderModel.orderCode);
            result.Replace(ConstantSmoovs.EmailKeyword.OrderDay, orderModel.modifiedDateOrderShow);
            result.Replace(ConstantSmoovs.EmailKeyword.OrderStatusOfCustomer, orderModel.OrderStatusDisplayForCustomer);

            result.Replace(ConstantSmoovs.EmailKeyword.OrderTypeInformation, orderModel.orderType == 1 ? ConstantSmoovs.EmailKeyword.CollectionTo : ConstantSmoovs.EmailKeyword.ShipTo);
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameLastNameBill, orderModel.CustomerSenderFullName);
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameLastNameReceive, orderModel.CustomerReceiverFullName);

            result.Replace(ConstantSmoovs.EmailKeyword.AddressBill, orderModel.CustomerSenderAddress);
            result.Replace(ConstantSmoovs.EmailKeyword.AddressReceive, orderModel.addressShipping);
            result.Replace(ConstantSmoovs.EmailKeyword.PhoneBill, orderModel.customerSender != null ? (!string.IsNullOrWhiteSpace(orderModel.customerSender.phone) ? orderModel.customerSender.phone : "") : "");
            result.Replace(ConstantSmoovs.EmailKeyword.PhoneReceive, orderModel.CustomerReceivePhone);
            //result.Replace(ConstantSmoovs.EmailKeyword.PhoneReceive,orderModel.CustomerReceivePhone);
            //result.Replace(Constants.EmailKeyword.PhoneReceive, orderModel.customerReceiver != null ? (!string.IsNullOrWhiteSpace(orderModel.customerReceiver.phone) ? orderModel.customerReceiver.phone : "") : "");
            result.Replace(ConstantSmoovs.EmailKeyword.EmailBill, orderModel.customerSender != null ? (!string.IsNullOrWhiteSpace(orderModel.customerSender.email) ? orderModel.customerSender.email : "") : "");
            result.Replace(ConstantSmoovs.EmailKeyword.EmailReceive, orderModel.CustomerReceiveEmail);
            // result.Replace(Constants.EmailKeyword.EmailReceive, orderModel.customerReceiver != null ? (!string.IsNullOrWhiteSpace(orderModel.customerReceiver.email) ? orderModel.customerReceiver.email : "") : "");

            result.Replace(ConstantSmoovs.EmailKeyword.OnlineshopLink, linkOnlineShop);
            //result.Replace(ConstantSmoovs.EmailKeyword.OnlineshopLink, orderModel.onlineShopLink);

            result.Replace(ConstantSmoovs.EmailKeyword.OrderCode, orderModel.orderCode);
            result.Replace(ConstantSmoovs.EmailKeyword.OrderStatusDisplayForCustomer, orderModel.OrderStatusDisplayForCustomer);
            return result;
        }

        public bool sendEmailOrder(int type, string merchantName, string email, string titleEmail, string body, MerchantManagementModel merchantInfor)
        {
            bool isSend = false;
            try
            {
                MailServerUtil sendMail = new MailServerUtil();
                MailConfigBusiness mailConfig = new MailConfigBusiness();
                if (merchantInfor == null)
                {
                    merchantInfor = GetMerchantManagement();
                }
                if (merchantInfor == null)
                {
                    logger.Error("merchant is null");
                }
                //var merchantInfor = GetMerchantManagement();
                merchantName = merchantInfor != null ? merchantInfor.businessName : "";
                if (merchantInfor != null)
                {
                    MailServerModel mailMerchant = new MailServerModel();
                    mailMerchant = mailConfig.GetConfigMailServer(merchantInfor.merchantID);
                    if (mailMerchant != null)
                    {
                        isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body, merchantInfor.merchantID) : false;
                    }
                    else
                    {
                        isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
                    }
                }
                else
                {
                    isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return isSend;
        }

        /// <summary>
        /// Sends the mail cash drawer.
        /// </summary>
        /// <param name="cashDrawer">The cash drawer.</param>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="emailCustomer">The email customer.</param>
        /// <param name="typeEmail">The type email.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/12/2015-4:27 PM</datetime>
        public async Task< Boolean> SendMailCashDrawer(CashDrawerReport cashDrawer, string businessName, string emailCustomer, string typeEmail)
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var merchant = client.WSGetMerchantManagementByBusinessName(businessName);
            client.Close();
            string bodyMail = "";
            string titleMail = "";
            try
            {

                bodyMail = RenderRazorViewToString("~/Views/SendMailPOS/CashDrawerReport.cshtml", cashDrawer);
                bodyMail = ReplaceInformationOfCashDrawer(bodyMail, businessName, cashDrawer).ToString();
                titleMail = ConstantSmoovs.TitleEmail.CashDrawerReport;
                bool isSend = await Task.Run(() => sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, merchant.firstName + " " + merchant.lastName, emailCustomer, titleMail, bodyMail, merchant));
                return isSend;
              
                //return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return false;
            }


        }
        /// <summary>
        /// Replaces the information of cash drawer.
        /// </summary>
        /// <param name="bodyMail">The body mail.</param>
        /// <param name="cashDrawer">The cash drawer.</param>
        /// <param name="userID">The user identifier.</param>
        /// <param name="linkOnlineShop">The link online shop.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/11/2015-3:52 PM</datetime>
        public StringBuilder ReplaceInformationOfCashDrawer(string bodyMail, string bussinessName, CashDrawerReport cashDrawer)
        {
            string siteID = "";
            IWSMerchantManagementClient ManagementClient = new IWSMerchantManagementClient();
            var merchant = ManagementClient.WSGetMerchantManagementByBusinessName(bussinessName);
            ManagementClient.Close();
            siteID = merchant != null ? merchant.bucket : "";
            DateTime timeCash = new DateTime();
            if (cashDrawer.closed)
            {
                timeCash = cashDrawer.closedDate;
            }
            else
            {
                timeCash = cashDrawer.startedDate;
            }

            timeCash = cashDrawer != null ? ((timeCash != null) ? TimeZoneUtil.ConvertUTCToTimeZone(timeCash, cashDrawer.branchTimezone, siteID) : timeCash) : timeCash;
            StringBuilder result = new StringBuilder(bodyMail.ToString());
            result.Replace(ConstantSmoovs.EmailKeyword.BusinessName, bussinessName);
            result.Replace(ConstantSmoovs.EmailKeyword.TimeCashDrawer, timeCash.ToString("MM/dd/yyyy - hh:mm:ss"));
            return result;
        }

        /// <summary>
        /// Gets the merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/12/2015-4:26 PM</datetime>
        public MerchantManagementModel GetMerchantManagement()
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            string siteID = VariableConfigController.GetBusinessName();
            var model = client.WSGetMerchantManagementByBusinessName(siteID);
            client.Close();
            return model;
        }


        /// <summary>
        /// Wses the send mail notify table ordering.
        /// </summary>
        /// <param name="Authorization">The authorization.</param>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="customerName">Name of the customer.</param>
        /// <param name="emailCustomer">The email customer.</param>
        /// <param name="typeEmail">The type email.</param>
        /// <param name="statusOrder">The status order.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>3/19/2015-6:23 PM</datetime>
        [HttpPost]
        public async Task<ActionResult> WSSendMailNotifyTableOrdering(string Authorization, string businessName, string customerName, string emailCustomer, string typeEmail, string tableName, string message)
        {
            //var returnCode1 = 2;
            //returnCode1 = await SendMailNotifyTableOrder(businessName, customerName, emailCustomer, typeEmail, tableName, message) == true ? 1 : 2;
            //return Json(new { status = returnCode1, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (string)null }, JsonRequestBehavior.AllowGet);
            if (!string.IsNullOrEmpty(Authorization))
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    if (authen.expires_in > 0)
                    {
                        var returnCode=2;
                        returnCode =await  SendMailNotifyTableOrder(businessName, customerName, emailCustomer, typeEmail, tableName, message) == true ? 1 : 2;
                        return Json(new { status = returnCode, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (string)null }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //AccessTokenExpired 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //AccessTokenNotProvided 
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //CommonInvalidRequest 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (string)null }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Sends the mail notify table order.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="customerName">Name of the customer.</param>
        /// <param name="emailCustomer">The email customer.</param>
        /// <param name="typeEmail">The type email.</param>
        /// <param name="statusOrder">The status order.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>3/19/2015-6:22 PM</datetime>
        public async Task<Boolean> SendMailNotifyTableOrder(string businessName, string customerName, string emailCustomer, string typeEmail, string tableName,string message)
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var merchant = client.WSGetMerchantManagementByBusinessName(businessName);
            client.Close();
            string bodyMail = "";
            string titleMail = "";
            try
            {
                switch (typeEmail)
                {
                    case ConstantSmoovs.MailTypeOrdering.OrderingConfirm:
                        titleMail = ConstantSmoovs.TitleEmail.OrderConfirmation;
                        bodyMail = RenderRazorViewToStringNoModel("~/Views/SendMailPOS/TableOrderingConfirm.cshtml");
                        break;
                    case ConstantSmoovs.MailTypeOrdering.OrderingReject:
                        titleMail = ConstantSmoovs.TitleEmail.OrderReject;
                        bodyMail = RenderRazorViewToStringNoModel("~/Views/SendMailPOS/TableOrderingReject.cshtml");
                        break;
                    case ConstantSmoovs.MailTypeOrdering.OrderingCancel:
                        titleMail = ConstantSmoovs.TitleEmail.OrderCancel;
                        bodyMail = RenderRazorViewToStringNoModel("~/Views/SendMailPOS/TableOrderingCancel.cshtml");
                        break;
                    case ConstantSmoovs.MailTypeOrdering.OrderingReady:
                        titleMail = ConstantSmoovs.TitleEmail.OrderReady;
                        bodyMail = RenderRazorViewToStringNoModel("~/Views/SendMailPOS/TableOrderingReady.cshtml");
                        break;
                    case ConstantSmoovs.MailTypeOrdering.OrderingAmend:
                        titleMail = ConstantSmoovs.TitleEmail.OrderAmend;
                        bodyMail = RenderRazorViewToStringNoModel("~/Views/SendMailPOS/TableOrderingAmend.cshtml");
                        break;
                    default:
                        break;
                }
              
           
              
                StringBuilder result = new StringBuilder(bodyMail.ToString());
                result.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, customerName);
                result.Replace(ConstantSmoovs.EmailKeyword.TableName, tableName);
                result.Replace(ConstantSmoovs.EmailKeyword.BusinessName, businessName);
                result.Replace(ConstantSmoovs.EmailKeyword.OrderAmendDetail, message);
                bodyMail = result.ToString();
              
                bool isSend = await Task.Run(() => sendEmailOrder(ConstantSmoovs.ConfigMail.Merchant, merchant.firstName + " " + merchant.lastName, emailCustomer, titleMail, bodyMail, merchant));
                return isSend;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return false;
            }


        }
    }
}