﻿using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using SmoovPOS.API.Models;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using SmoovPOS.Common.SendNotify;
using SmoovPOS.Entity.DeviceToken;
using SmoovPOS.Entity.Entity;
using SmoovPOS.Entity.Models;
using SmoovPOS.Utility.WSCheckoutSettingReference;
using SmoovPOS.Utility.WSStationReference;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.API.Controllers
{
    public class TableOrderController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //[HttpPost]
        //// GET: TableOrder
        //public JsonResult WSSendMessage(string Authorization, string stationid, string message)
        //{
        //    try
        //    {
        //        WSStationClient clientStation = new WSStationClient();
        //        var model = clientStation.WSGetDetailStation(stationid);
        //        if (model != null)
        //        {
        //            var url = model.stationQRCode;
        //            url = url.Substring(0, url.IndexOf(@"/TableOrder/ScanQRCode"));
        //            var connection = new HubConnection(url);
        //            IHubProxy myHub = connection.CreateHubProxy("ChatHub");
        //            connection.Start().Wait(); // not sure if you need this if you are simply posting to the hub
        //            myHub.Invoke("SendPrivateMessage", new object[] { stationid, message });
        //            connection.Dispose();
        //        }
        //        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key;
        //        return Json(new { status = 1, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (string)null }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("WSSendMessage_Error", ex);
        //        //CommonNotFound 
        //        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonNotFound).Key;
        //        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        private int UpdateStatusTableOrderForConsumer(string orderID, int statusOrder, string stationID)
        {
            try
            {
                WSStationClient clientStation = new WSStationClient();
                var model = clientStation.WSGetDetailStation(stationID);
                var url = model.stationQRCode;
                url = url.Substring(0, url.IndexOf(@"/TableOrder/ScanQRCode"));
                var connection = new HubConnection(url);
                IHubProxy myHub = connection.CreateHubProxy("ChatHub");
                connection.Start().Wait(); // not sure if you need this if you are simply posting to the hub
                myHub.Invoke("SendPrivateMessage", new object[] { stationID, string.Format("{0}~{1}", orderID, statusOrder.ToString()) });
                connection.Dispose();
                return ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key;
            }
            catch
            {
                return ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Fail).Key;
            }

        }
        private int UpdateStatusOrderForMerchant(string merchantID, string message, int typeMessage)
        {
            try
            {
                var url = ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteMerchantSmoovPOS];
                var connection = new HubConnection(url);
                IHubProxy myHub = connection.CreateHubProxy("ChatHub");
                connection.Start().Wait(); // not sure if you need this if you are simply posting to the hub
                var obj = new { typeMessage = typeMessage, data = message };
                myHub.Invoke("UpdateStatusOrderInMerchantPanel", new object[] { merchantID, new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(obj) });
                connection.Dispose();
                return ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key;
            }
            catch (Exception ex)
            {
                logger.Error("WSSendMessage_Error", ex);
                return ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Fail).Key;
            }

        }

        [HttpPost]
        public JsonResult WSUpdateOrder(string Authorization, string merchantID, string result, int typeMessage)
        {
            MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
            var authen = merchantManagement.GetUserSessionByToken(Authorization);
            if (authen == null)
            {
                authen = merchantManagement.GetUserSessionBySiteID(Authorization);
            }
            if (authen != null)
            {
                var checkSentMerchant = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key;
                var checkSentConsumer = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key;
                if (typeMessage == ConstantSmoovs.Enums.TypeMessage.FirstOrDefault(r => r.Value == ConstantSmoovs.TypeMessage.NewOrder).Key)
                {
                    checkSentMerchant = UpdateStatusOrderForMerchant(merchantID, result, typeMessage);
                }
                else if (typeMessage == ConstantSmoovs.Enums.TypeMessage.FirstOrDefault(r => r.Value == ConstantSmoovs.TypeMessage.UpdateTableOrderStatus).Key)
                {
                    var order = JsonConvert.DeserializeObject<NotifyOrder>(result);
                    checkSentConsumer = UpdateStatusTableOrderForConsumer(order.orderID, -1, order.stationID);
                }
                else
                {
                    try
                    {
                        var order = JsonConvert.DeserializeObject<NotifyOrder>(result);
                        checkSentConsumer = UpdateStatusTableOrderForConsumer(order.orderID, order.orderStatus, order.stationID);
                        checkSentMerchant = UpdateStatusOrderForMerchant(merchantID, result, typeMessage);
                    }
                    catch
                    {
                        checkSentMerchant = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Fail).Key;
                        checkSentConsumer = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Fail).Key;
                    }
                }

                if (checkSentConsumer == ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key && checkSentMerchant == ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key)
                {
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (string)null }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Fail).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
                }
            }

            var statusCommonNotFound = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonNotFound).Key;
            return Json(new { status = statusCommonNotFound, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult WSResetBadge(string Authorization, string deviceInfo)
        {
            logger.Info(string.Format("Reset Badge {0}", deviceInfo));
            MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
            var authen = merchantManagement.GetUserSessionByToken(Authorization);
            if (authen == null)
            {
                authen = merchantManagement.GetUserSessionBySiteID(Authorization);
            }
            if (authen != null)
            {
                var jsonInfoDevice = JsonConvert.DeserializeObject<DeviceTokenModel>(deviceInfo);
                jsonInfoDevice.status = true;
                jsonInfoDevice.token = jsonInfoDevice.deviceToken;

                WSCheckoutSettingClient clientCheckoutSetting = new WSCheckoutSettingClient();
                var device = clientCheckoutSetting.DetailDeviceToken(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, authen.databaseSync);
                foreach (var item in device.ArrayDevices.Where(a => a.uuid == jsonInfoDevice.uuid || a.token == jsonInfoDevice.token).ToList())
                {
                    item.badge = 0;
                    clientCheckoutSetting.UpdateDeviceToken(authen.databaseSync, item);
                }
                //clientCheckoutSetting.UpdateManageDevice(device, authen.databaseSync);

                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (string)null }, JsonRequestBehavior.AllowGet);
            }

            var statusCommonNotFound = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonNotFound).Key;
            return Json(new { status = statusCommonNotFound, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
        }
    }
}
