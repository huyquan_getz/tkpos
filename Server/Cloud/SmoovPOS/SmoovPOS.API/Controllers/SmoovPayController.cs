﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSCheckoutSettingReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.API.Controllers
{
    public class SmoovPayController : Controller
    {

        /// <summary>
        /// Wses the refund order.
        /// </summary>
        /// <param name="merchantID">The merchant identifier.</param>
        /// <param name="referenceCode">The reference code.</param>
        /// <param name="refundAmount">The refund amount.</param>
        /// <param name="remarks">The remarks.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>3/26/2015-2:20 PM</datetime>
        [HttpPost]
        public ActionResult WSRefundOrder(string merchantID, string referenceCode, double refundAmount, string remarks = "")
        {
            string Authorization = "";
            Authorization = CommonActionSmoovPay.GetAuthorization(merchantID);
            if (!string.IsNullOrEmpty(Authorization))
            {
                if (!string.IsNullOrEmpty(referenceCode) && !string.IsNullOrEmpty(refundAmount.ToString()))
                {
                    try
                    {
                        var result = CommonActionSmoovPay.CallApiRefundOrder(Authorization, referenceCode, refundAmount.ToString(), remarks);
                        if (result != null)
                        {
                            if (result.status == ConstantSmoovs.ResponseStatusApiSmoovPay.AccessTokenExpired)
                            {
                                var status = CommonActionSmoovPay.UpdateAuthorization(merchantID);
                                if (status)
                                {
                                    return this.WSRefundOrder(merchantID, referenceCode, refundAmount, remarks);
                                }
                                return Json(new { status = ConstantSmoovs.ResponseStatusApiSmoovPay.Timeout, message = ConstantSmoovs.API_RESPONSE_STATUS.Timeout, result = "" }, JsonRequestBehavior.AllowGet);
                            }
                            return Json(new { status = result.status, message = result.message, result = result.result }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { status = ConstantSmoovs.ResponseStatusApiSmoovPay.Timeout, message = ConstantSmoovs.API_RESPONSE_STATUS.Timeout, result = "" }, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                        return Json(new { status = ConstantSmoovs.ResponseStatusApiSmoovPay.ErrorTryCatch, message = ConstantSmoovs.API_RESPONSE_STATUS.Timeout, result = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { status = ConstantSmoovs.ResponseStatusApiSmoovPay.ErrorParams, message = ConstantSmoovs.API_RESPONSE_STATUS.ErrorParams, result = "" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = ConstantSmoovs.ResponseStatusApiSmoovPay.AccountSmoovPayNotExist, message = ConstantSmoovs.API_RESPONSE_STATUS.AccountSmoovPayNotExist, result = "" }, JsonRequestBehavior.AllowGet);

        }
    }
}