﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.API.Models
{
    public class OrderPOS
    {
        public string FirstName { get; set; }
        public string OrderNumber { get; set; }
        public string OnlineShopLink { get; set; }
        public string BusinessName { get; set; }
        public string Reason { get; set; }
        public string TypeEmail { get; set; }
        public string EmailCustomer { get; set; }
    }
}