﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmoovPOS.UI.Models
{
    public class SiteViewModel
    {
        public string SiteID { get; set; }

        public DateTime? ExpiryDate { get; set; }

        [Required]
        public int UserRegistration { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public string NewHTTPAlias { get; set; }
    }

    public class SiteAliasViewModel
    {
        public string SiteAliasID { get; set; }

        public string SiteID { get; set; }

        [Required]
        [Display(Name = "HTTP Alias")]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string HTTPAlias { get; set; }

        public int CreatedByUserID { get; set; }

        public DateTime CreatedOnDate { get; set; }

        public int LastModifiedByUserID { get; set; }

        [Required]
        public DateTime LastModifiedOnDate { get; set; }

        public bool IsPrimary { get; set; }
    }
}