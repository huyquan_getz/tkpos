﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Order
{
    public class OrderHistoryListingModel
    {

        public List<OrderHistoryViewModel> listOrderHistory { get; set; }

        public string CustomerTimezone { get; set; }
    }
}