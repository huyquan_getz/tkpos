﻿using SmoovPOS.Entity.Models.Paging;
using System.Collections.Generic;

namespace online.Models
{
    public class AllProductModel
    {
        private List<ProductByCategoryModel> productByCategoryList;
        public List<ProductByCategoryModel> ProductByCategoryList
        {
            set { productByCategoryList = value; }
            get
            {
                if (productByCategoryList == null)
                {
                    productByCategoryList = new List<ProductByCategoryModel>();
                }

                return productByCategoryList;
            }
        }

        private EntityPaging paging;
        public EntityPaging Paging
        {
            set { paging = value; }
            get
            {
                if (paging == null)
                {
                    paging = new EntityPaging();
                }

                return paging;
            }
        }
    }
}