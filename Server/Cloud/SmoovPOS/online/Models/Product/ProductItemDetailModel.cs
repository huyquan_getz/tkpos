﻿using Com.SmoovPOS.Entity;
namespace online.Models
{
    public class ProductItemDetailModel
    {
        public ProductItem ProductItem { get; set; }
        public Collection Collection { get; set; }
    }
}