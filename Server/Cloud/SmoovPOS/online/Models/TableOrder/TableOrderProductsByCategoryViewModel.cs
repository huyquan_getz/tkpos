﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Models.Paging;
using System.Collections.Generic;

namespace SmoovPOS.UI.Models
{
    public class TableOrderProductsByCategoryViewModel
    {
        public string branchName { get; set; }
        public List<ProductItem> ProductItemList { get; set; }
    }
}