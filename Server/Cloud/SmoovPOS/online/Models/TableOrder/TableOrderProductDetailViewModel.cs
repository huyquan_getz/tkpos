﻿using Com.SmoovPOS.Entity;

namespace SmoovPOS.UI.Models
{
    public class TableOrderProductDetailViewModel
    {
        public string Currency { get; set; }
        public ProductItem ProductItem { get; set; }
    }
}