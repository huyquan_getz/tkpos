﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Collection
{
    public class ProductItemSort
    {
        public Com.SmoovPOS.Entity.ProductItem ProductItem { get; set; }
        public double minPriceDiscount { get; set; }
    }
}