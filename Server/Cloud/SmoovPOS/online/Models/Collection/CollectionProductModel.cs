﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Collection
{
    public class CollectionProductModel
    {
        public Com.SmoovPOS.Entity.Collection Collection { get; set; }
        public Com.SmoovPOS.Entity.ProductItem ProductItem { get; set; }
    }
}