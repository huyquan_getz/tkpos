﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Customer
{
    public class CustomerChangePasswordViewModel
    {
        [LocalizedStringLength(100, MinimumLength = 6, Name = "NewPasswordLength")]
        [DataType(DataType.Password)]
        [LocalizedDisplayName("NewPassword")]
        public string password { get; set; }
        [DataType(DataType.Password)]
        [LocalizedDisplayName("ConfirmPassword")]
        [LocalizedCompare("NewPassword", Name = "ConfirmPasswordCompare")]
        public string passwordConfirm { get; set; }
        [LocalizedDisplayName("OldPassword")]
        public string OldPassword { get; set; }
        public string email { get; set; }
    }
}