﻿using online.Controllers;
using SmoovPOS.Common;
using SmoovPOS.Utility.WSMerchantManagementReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class ConsoleController : BaseController
    {
        // GET: Console
        public ActionResult Index()
        {
            IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
            string business = clientMerchant.WSGetMerchantManagementBySiteId(GetSiteID()).businessName;
            clientMerchant.Close();
            return Redirect(System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteMerchantSmoovPOS] +"?business="+ business);
        }
    }
}