﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using online.Controllers;
using online.Models;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.WSCollectionReference;
using SmoovPOS.Utility.WSProductItemReference;
using SmoovPOS.UI.Models.Collection;

namespace SmoovPOS.UI.Controllers
{
    public class CollectionController : BaseController
    {
        // GET: Collection
        public ActionResult Index()
        {
            CollectionItemListingModel collection = new CollectionItemListingModel();
            ViewBag.Class = "collection";
            GetThemeInfo();
            collection.CollectionList = GetAllCollection();
            return View(collection);
        }
        /// <summary>
        /// Load productItem by collection
        /// </summary>
        /// <param name="id"></param>
        /// <createdby>vu.ho</createdby>
        /// <createddate>04/02/2015-3:07 PM</createddate>
        /// <returns></returns>
        public ActionResult ProductByCollection(string id)
        {
            string siteID = GetSiteID();
            ViewBag.Class = "collection";
            CollectionItemListingModel collection = new CollectionItemListingModel();
            WSCollectionClient CollectionClient = new WSCollectionClient();
            Collection collec = CollectionClient.WSDetailCollection(id, siteID);

            WSProductItemClient productitemClient = new WSProductItemClient();          
             List<ProductItem> productItemListResult = new List<ProductItem>();
             List<ProductItem> productItemListOnl = new List<ProductItem>();
            foreach (var collecItem in collec.arrayProduct)
            {
                var productItemInfo = productitemClient.WSDetailProductItem(collecItem._id, siteID);
                var productItems = productitemClient.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productItemInfo.productID, GetSiteID());
                productItemListOnl = productItems.Where(r => r.index == 1 && r.status).ToList();

                foreach (var productItem in productItemListOnl)
                {
                    if (productItemInfo.productID == productItem.productID)
                    {
                        productItemListResult.Add(productItem);
                    }
                }                
            }

            // Add discount to products
            BaseController baseController = new BaseController();
            productItemListResult = baseController.SetDiscountToListProductItems(productItemListResult, siteID);

            switch (collec.defaultSortProduct)
            {
                case ConstantSmoovs.Collection.AZ:
                    collection.ProductItemList = productItemListResult.OrderBy(r => r.title);
                    break;
                case ConstantSmoovs.Collection.ZA:
                    collection.ProductItemList = productItemListResult.OrderByDescending(r => r.title);
                    break;
                case ConstantSmoovs.Collection.HighestLowest:
                    //collection.ProductItemList = productItemListCo.OrderByDescending(r => r.price);
                    collection.ProductItemList = SortByMinPriceAndDiscount(productItemListResult, true);
                    break;
                case ConstantSmoovs.Collection.LowestHighest:
                    //collection.ProductItemList = productItemListCo.OrderBy(r => r.price);
                    collection.ProductItemList = SortByMinPriceAndDiscount(productItemListResult, false);
                    break;
                case ConstantSmoovs.Collection.NewestOldest:
                    collection.ProductItemList = productItemListResult.OrderByDescending(r => r.modifiedDate);
                    break;
                case ConstantSmoovs.Collection.OldestNewest:
                    collection.ProductItemList = productItemListResult.OrderBy(r => r.modifiedDate);
                    break;
                default:
                    collection.ProductItemList = productItemListResult.OrderBy(r => r.title);
                    break;
            }
            collection.Collection = collec;
            GetThemeInfo();
            return View(collection);
        }
        /// <summary>
        /// sort by minprice of varriant and discount
        /// </summary>
        /// <param name="productItemList"></param>
        /// <param name="p"></param>
        /// <createdby>vu.ho</createdby>
        /// <createddate>06/02/2015-3:07 PM</createddate>
        /// <returns></returns>
        private List<ProductItem> SortByMinPriceAndDiscount(List<ProductItem> productItemList, bool p)
        {
            string siteID = GetSiteID();
            List<ProductItemSort> ProductItemSortList = new List<ProductItemSort>();
            List<ProductItem> ListProductItemResult = new List<ProductItem>();
            foreach (var productItem in productItemList) {
                var discount = productItem.DiscountProductItem;
                // get price min of Variant
                productItem.arrayVarient.Sort((var1, var2) => var1.price.CompareTo(var2.price));
                var smallestProductItem = productItem.arrayVarient[0];
                double minPriceVariant = smallestProductItem.price;
                double minPriceDiscount = 0;
                if (discount != null)
                {
                    minPriceDiscount = discount.type == 0 ? minPriceVariant - minPriceVariant * discount.discountValue / 100 : minPriceVariant - discount.discountValue;
                    minPriceDiscount = minPriceDiscount < 0 ? 0 : minPriceDiscount;
                }
                else
                {
                    minPriceDiscount = smallestProductItem.price;
                }
                ProductItemSort productItemSort = new ProductItemSort();
                productItemSort.ProductItem = productItem;
                productItemSort.minPriceDiscount = minPriceDiscount;                
                ProductItemSortList.Add(productItemSort);
            }

            if (p)
            {
                ProductItemSortList = ProductItemSortList.OrderByDescending(r => r.minPriceDiscount).ToList();
            }
            else {
                ProductItemSortList = ProductItemSortList.OrderBy(r => r.minPriceDiscount).ToList();
            }
            foreach (var item in ProductItemSortList)
            {
                ListProductItemResult.Add(item.ProductItem);
            }

            // Add discount to products
            BaseController baseController = new BaseController();
            ListProductItemResult = baseController.SetDiscountToListProductItems(ListProductItemResult, siteID);

            return ListProductItemResult;
        }
    }    
}