﻿using AutoMapper;
using Com.SmoovPOS.Entity;
using online.Controllers;
using SmoovPOS.Common;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.CustomHTMLHelper;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSProductItemReference;
using SmoovPOS.Utility.WSStationReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SmoovPOS.Utility.WSMyCartReference;
using SmoovPOS.Utility.Cache;
using SmoovPOS.Utility.WSOrderReference;
using SmoovPOS.Utility.WSCustomerReference;
using SmoovPOS.Entity.Models;
using online.Models;
using SmoovPOS.UI.Models.TableOrder;
using SmoovPOS.Utility.WSCheckoutSettingReference;
using SmoovPOS.Entity.BaseEntity;
using System.Text;
using Microsoft.AspNet.SignalR.Client;
using System.Threading.Tasks;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using SmoovPOS.Entity.ApiSmoovPay;
using SmoovPOS.Entity.Entity;
using Com.SmoovPOS.TimeZone;
using SmoovPOS.Entity.Menu;

namespace SmoovPOS.UI.Controllers
{
    public class TableOrderController : BaseController
    {
        private static object locker = new object();

        #region Scan QR Code
        // GET: TableOrder
        [AllowAnonymous]
        public async Task<ActionResult> ScanQRCode(string id)
        {
            string siteID = GetSiteID();

            ViewBag.email = " ";
            ViewBag.TableOrder = new TableOrderViewModel();
            if (!string.IsNullOrEmpty(id))
            {
                TableOrderViewModel viewmodel = null;
                //have not session
                WSStationClient clientStation = new WSStationClient();
                var stationModel = await clientStation.WSGetScanQRCodeAsync(id, siteID);
                clientStation.Close();

                var model = stationModel.station;
                if (model == null)
                {
                    viewmodel = new TableOrderViewModel();
                    viewmodel.isHost = true;
                    viewmodel.isStartOrder = false;
                    return View(viewmodel);
                }

                TableOrderingSetting setting = stationModel.tableOrderingSetting; //GetTableOrderingSetting(siteID, model);

                if (!stationModel.tableOrderingPermission)
                {
                    return RedirectToAction("NotAvailable", "TableOrder");
                }

                Session[ConstantSmoovs.TableOrder.TableOrderingSetting] = setting;
                if (setting == null
                    || (setting != null && !setting.AcceptTableOrdering)
                    || (setting != null && setting.ListDayOfWeekSetting.Count(l => l.Option == (int)ConstantSmoovs.Enums.TableOrderingOption.Closed) == 7))
                {
                    return RedirectToAction("NotAvailable", "TableOrder");
                }

                if (CheckAvailableTime(siteID, setting) == false)
                {
                    TempData[ConstantSmoovs.TableOrder.TableOrderingSetting] = setting;
                    return RedirectToAction("OutOfTime", "TableOrder");
                }

                var statusOccupied = ConstantSmoovs.Enums.StationTableStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.StationTableStatus.Occupied).Key;
                //with have session
                if (Session[ConstantSmoovs.TableOrder.tableCart] != null && model.statusOrdering == statusOccupied)
                {
                    viewmodel = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];

                    if (viewmodel != null && viewmodel.isStartOrder && viewmodel.id == id)
                    {
                        SetCustomerToCache(viewmodel, model, viewmodel.memberID);
                        return RedirectToAction("Index", new { id = viewmodel.id });
                    }
                }

                if (viewmodel != null)
                {
                    var modelMap = Mapper.Map<Station, TableOrderViewModel>(model);
                    viewmodel.id = model._id;
                    viewmodel.stationName = model.stationName;
                    viewmodel.branchIndex = modelMap.branchIndex;
                    viewmodel.stationSize = modelMap.stationSize;
                    viewmodel.stationQRCode = modelMap.stationQRCode;
                    viewmodel.statusOrdering = modelMap.statusOrdering;
                    viewmodel.orderId = modelMap.orderId;
                }
                else
                {
                    viewmodel = Mapper.Map<Station, TableOrderViewModel>(model);
                }

                if (viewmodel == null)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    //WSInventoryClient client = new WSInventoryClient();
                    var inventory = stationModel.inventory; //client.WSDetailInventoryByIndex(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, siteID, viewmodel.branchIndex);
                    Session[ConstantSmoovs.TableOrder.inventory] = stationModel.inventory;
                    //client.Close();
                    if (inventory != null)
                    {
                        viewmodel.branchName = inventory.businessID;
                        viewmodel.timeZone = inventory.timeZone;
                    }
                    else
                    {
                        viewmodel = new TableOrderViewModel();
                        viewmodel.isHost = true;
                        viewmodel.isStartOrder = false;
                        return View(viewmodel);
                    }
                    //WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
                    //string currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
                    //merchantGeneralSettingClient.Close();
                    viewmodel.currency = !string.IsNullOrWhiteSpace(stationModel.currency) ? stationModel.currency : "";

                    viewmodel.isHost = true;
                    viewmodel.isStartOrder = true;
                    if (model.statusOrdering == statusOccupied)
                    {
                        viewmodel.isHost = false;
                        var timeOut = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.OccupiedTimeOut].ToString());
                        ViewBag.email = " ";
                        if (model != null && model.customerHost != null && (model.modifiedDate.AddMinutes(timeOut) > DateTime.UtcNow))
                        {
                            ViewBag.email = model.customerHost.email;
                            TableOrderViewModel modelTableOrder = new TableOrderViewModel();

                            modelTableOrder.firstName = model.customerHost.firstName;
                            modelTableOrder.lastName = model.customerHost.lastName;
                            modelTableOrder.phone = model.customerHost.phone;
                            modelTableOrder.phoneRegion = model.customerHost.phoneRegion;
                            modelTableOrder.email = model.customerHost.email;

                            ViewBag.TableOrder = modelTableOrder;
                        }
                    }
                    //else if (model.statusOrdering != ConstantSmoovs.Enums.StationTableStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.StationTableStatus.Empty).Key)
                    //{
                    //    viewmodel.isHost = true;
                    //    viewmodel.isStartOrder = false;
                    //}

                }

                ViewBag.businessName = stationModel.businessName;
                viewmodel.businessName = stationModel.businessName;
                return View(viewmodel);
            }
            return View();
        }


        [HttpPost]
        public ActionResult StartOrder(TableOrderViewModel viewmodel)
        {
            lock (locker)
            {
                string siteID = GetSiteID();
                StringHelper.Trim(viewmodel);

                WSStationClient clientStation = new WSStationClient();
                var model = clientStation.WSGetDetailStation(viewmodel.id);
                if (model == null)
                {
                    return RedirectToAction("ScanQRCode", new { id = viewmodel.id });
                }

                viewmodel.isHost = (model != null && model.statusOrdering == ConstantSmoovs.Enums.StationTableStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.StationTableStatus.Empty).Key);

                if (!viewmodel.isHost && model != null)
                {
                    var statusOccupied = ConstantSmoovs.Enums.StationTableStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.StationTableStatus.Occupied).Key;
                    if (model.customerHost != null && model.customerHost.email.Trim().ToUpper() == viewmodel.email.Trim().ToUpper())
                    {
                        viewmodel.isHost = true;
                    }
                    else if (model.statusOrdering == ConstantSmoovs.Enums.StationTableStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.StationTableStatus.Empty).Key)
                    {
                        viewmodel.isHost = true;
                    }
                    else if (model.statusOrdering != statusOccupied)
                    {
                        viewmodel.isHost = true;
                    }
                }

                viewmodel.receiptValue = new ReceiptValue();
                if (CacheHelper.TableOrders == null)
                {
                    CacheHelper.TableOrders = new List<TableOrderModel>() { };
                }

                viewmodel.email = viewmodel.email.ToLower();
                //Retrieve member by email
                WSCustomerClient clientCustomer = new WSCustomerClient();
                Customer member = clientCustomer.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, viewmodel.email, siteID);
                viewmodel.fullName = string.Format("{0} {1}", viewmodel.firstName, viewmodel.lastName);
                if (member == null)
                {
                    //Case: email has not used before -> create new guest-role member
                    //member = new Customer();
                    member = InitializeNewCustomer(siteID);

                    member.customerType = 1;
                    var memberId = ConstantSmoovs.StationTable.memberId + (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                    if (memberId.Split(".".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Length > 1)
                    {
                        member.memberID = memberId.Split(".".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];
                    }
                    else
                    {
                        member.memberID = memberId;
                    }

                    member.firstName = viewmodel.firstName;
                    member.lastName = viewmodel.lastName;
                    member.fullName = viewmodel.fullName;
                    member.email = viewmodel.email;
                    member.phone = viewmodel.phone;
                    member.phoneRegion = viewmodel.phoneRegion;

                    member.listOrder = new List<Orderdetail>();

                    member.totalOrder = 0;
                    member.totalSpend = 0;

                    member.createdDate = DateTime.UtcNow;
                    member.modifiedDate = DateTime.UtcNow;
                    //clientCustomer.WSCreateCustomer(member, siteID);
                }
                else
                {
                    //Case: email has used before -> update the existed member (member-role or guest-roles)

                    //if (member.listOrder != null) listOrderDetails = member.listOrder;
                    //listOrderDetails.Add(orderDetail);
                    var memberId = member.memberID;
                    if (memberId.Split(".".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Length > 1)
                    {
                        member.memberID = memberId.Split(".".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];
                    }
                    else
                    {
                        member.memberID = memberId;
                    }
                    //Update member's info
                    member.firstName = viewmodel.firstName;
                    member.lastName = viewmodel.lastName;
                    member.fullName = viewmodel.fullName;
                    member.phone = viewmodel.phone;
                    member.phoneRegion = viewmodel.phoneRegion;

                    member.lastOrder = DateTime.UtcNow;
                    member.modifiedDate = DateTime.UtcNow;
                    //member.listOrder = listOrderDetails;
                    //member.totalOrder += 1;
                    //member.totalSpend += myCartInSession.receiptValue.grandTotal;
                    //clientCustomer.WSUpdateCustomer(member, siteID);
                }
                clientCustomer.Close();
                SetCustomerToCache(viewmodel, model, member.memberID);

                viewmodel.memberID = member.memberID;
                if (viewmodel.listProductItems == null)
                {
                    viewmodel.listProductItems = new List<ProductItem>() { };
                }
                viewmodel.stationQRCode = model.stationQRCode;

                var order = new TableOrderModel();
                if (CacheHelper.TableOrders.Where(c => c != null && c.id == viewmodel.id).Any())
                {
                    order = CacheHelper.TableOrders.Where(c => c != null && c.id == viewmodel.id).FirstOrDefault();
                }

                if (viewmodel.isHost)
                {
                    viewmodel.receiptValue = order.receiptValue;
                    viewmodel.listProductItems = order.listProductItems;
                    if (order == null || string.IsNullOrEmpty(order.orderId) || order.orderId == Guid.Empty.ToString())
                    {
                        viewmodel.orderId = Guid.NewGuid().ToString();
                    }
                    else
                    {
                        viewmodel.orderId = order.orderId;
                    }
                    order = Mapper.Map<TableOrderViewModel, TableOrderModel>(viewmodel);

                    //have not session
                    if (model != null)
                    {
                        model.orderId = order.orderId;
                        var customerHost = Mapper.Map<TableOrderViewModel, CustomerInfo>(viewmodel);
                        customerHost.memberID = member.memberID;
                        model.customerHost = customerHost;

                        model.modifiedDate = DateTime.UtcNow;
                        model.userID = User.Identity.GetUserId();
                    }

                    if (order.listProductItems == null)
                    {
                        order.listProductItems = new List<ProductItem>() { };
                    }
                    SyncOrderToCache(viewmodel.id, order);

                    if (model != null)
                    {
                        model.statusOrdering = ConstantSmoovs.Enums.StationTableStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.StationTableStatus.Occupied).Key;
                        model.modifiedDate = DateTime.UtcNow;
                        model.userID = User.Identity.GetUserId();
                        clientStation.WSUpdateStation(model, siteID);
                    }

                    clientStation.Close();
                }
                else if (order != null)
                {
                    viewmodel.orderId = order.orderId;
                }

                WSInventoryClient clientInventory = new WSInventoryClient();
                var inventory = clientInventory.WSDetailInventoryByIndex(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, siteID, viewmodel.branchIndex);
                viewmodel.timeZone = inventory.timeZone;
                clientInventory.Close();

                Session[ConstantSmoovs.TableOrder.tableCart] = viewmodel;

                return RedirectToAction("Index", new { Id = model._id });
            }
        }

        private static void SetCustomerToCache(TableOrderViewModel viewmodel, Station model, string memberID)
        {
            CustomerInfo customer = null;
            if (CacheHelper.CustomerInfos == null)
            {
                CacheHelper.CustomerInfos = new List<Station>() { };
            }
            if (CacheHelper.CustomerInfos.Any(c => c.customerHost != null && c.customerHost.memberID == memberID))
            {
                customer = CacheHelper.CustomerInfos.FirstOrDefault(c => c.customerHost != null && c.customerHost.memberID == memberID).customerHost;
            }
            if (customer == null)
            {
                customer = new CustomerInfo();
                customer.memberID = memberID;
            }

            customer.email = viewmodel.email;
            customer.firstName = viewmodel.firstName;
            customer.lastName = viewmodel.lastName;
            customer.fullName = viewmodel.fullName;
            customer.email = viewmodel.email;
            customer.phone = viewmodel.phone;
            customer.phoneRegion = viewmodel.phoneRegion;
            if (CacheHelper.CustomerInfos.Any(c => c.customerHost != null && c.customerHost.memberID == memberID && c._id == model._id))
            {
                var stationCache = CacheHelper.CustomerInfos.FirstOrDefault(c => c.customerHost != null && c.customerHost.memberID == memberID && c._id == model._id);
                stationCache.customerHost = customer;
            }
            else
            {
                var stationCache = new Station();
                stationCache._id = model._id;
                stationCache.id = model._id;
                stationCache.branchIndex = model.branchIndex;
                stationCache.stationName = model.stationName;
                stationCache.customerHost = customer;
                CacheHelper.CustomerInfos = CacheHelper.CustomerInfos.Where(c => c.customerHost != null && c.customerHost.memberID != memberID).ToList();
                CacheHelper.CustomerInfos.Add(stationCache);
            }
        }

        [HttpPost]
        public ActionResult RejectJoin(TableOrderViewModel viewmodel)
        {
            viewmodel.isStartOrder = false;
            Session[ConstantSmoovs.TableOrder.tableCart] = viewmodel;
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public int GetCountProductInTableCart()
        {
            int count = 0;
            if (Session[ConstantSmoovs.TableOrder.tableCart] != null)
            {
                TableOrderViewModel myCartInSession = new TableOrderViewModel();
                myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
                if (myCartInSession != null)
                {
                    if (myCartInSession.isHost)
                    {
                        count = GetCountProductInCache(myCartInSession.id);
                    }
                    else
                    {
                        if (myCartInSession.listProductItems != null)
                        {
                            foreach (var product in myCartInSession.listProductItems)
                            {
                                foreach (var item in product.arrayVarient)
                                {
                                    count += item.quantity;
                                }
                            }
                        }
                    }
                }
            }
            return count;
        }

        public int GetCountProductInCache(string id)
        {
            int count = 0;
            var order = new TableOrderModel();
            if (CacheHelper.TableOrders.Where(c => c != null && c.id == id).Any())
            {
                order = CacheHelper.TableOrders.Where(c => c != null && c.id == id).FirstOrDefault();
            }
            if (order != null && order.listProductItems != null)
            {
                foreach (var product in order.listProductItems)
                {
                    foreach (var item in product.arrayVarient)
                    {
                        count += item.quantity;
                    }
                }
            }
            return count;
        }
        [HttpPost]
        public ActionResult removeProductItem(string variantId, string productId)
        {
            TableOrderViewModel myCartInSession = null;
            if ((Object)Session[ConstantSmoovs.TableOrder.tableCart] != null)
            {
                myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            }
            var ishas = false;
            foreach (var productItem in myCartInSession.listProductItems.Where(p => p._id == productId).ToList())
            {
                if (productItem.arrayVarient.Where(v => (v._id + productItem.userOwner) == variantId).Any())
                {
                    ishas = true;
                }
                productItem.arrayVarient = productItem.arrayVarient.Where(v => (v._id + productItem.userOwner) != variantId).ToList();
            }

            myCartInSession.listProductItems = myCartInSession.listProductItems.Where(x => x.arrayVarient.Count > 0).ToList();
            Session[ConstantSmoovs.TableOrder.tableCart] = myCartInSession;
            var isRemoveClient = false;
            if (!ishas && myCartInSession.isHost)
            {
                var order = new TableOrderModel();
                if (CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).Any())
                {
                    order = CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).FirstOrDefault();
                }

                foreach (var productItem in order.listProductItems.Where(p => p._id == productId).ToList())
                {
                    if (productItem.arrayVarient.Where(v => (v._id + productItem.userOwner) == variantId).Any())
                    {
                        if (productItem.arrayVarient.Where(v => productItem.userOwner != myCartInSession.memberID).Any())
                        {
                            isRemoveClient = true;
                        }

                    }
                    productItem.arrayVarient = productItem.arrayVarient.Where(v => (v._id + productItem.userOwner) != variantId).ToList();
                }
                order.listProductItems = order.listProductItems.Where(x => x.arrayVarient.Count > 0).ToList();

                SyncOrderToCache(myCartInSession.id, order);
            }

            if (myCartInSession.isHost)
            {
                SyncToCache(myCartInSession);
            }

            return Json(new { success = true, isRemoveClient = isRemoveClient }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Product
        public ActionResult Index(string Id)
        {
            if (Session[ConstantSmoovs.TableOrder.tableCart] == null)
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    return RedirectToAction("ScanQRCode", new { id = Id });
                }
                return RedirectToAction("ScanQRCode", new { id = Guid.NewGuid().ToString() });
            }

            //GetThemeInfo();
            var siteID = GetSiteID();
            if (CacheHelper.Categories == null || !CacheHelper.Categories.Where(c => c != null && c.siteID == siteID).Any())
            {
                if (CacheHelper.Categories == null)
                {
                    CacheHelper.Categories = new List<Menu>();
                }

                var listC = GetAllCategory();
                if (listC != null && listC.Count() > 0)
                {
                    CacheHelper.Categories.AddRange(listC);
                }
            }

            var listCategory = CacheHelper.Categories.Where(c => c != null && c.siteID == siteID).ToList();
            ViewBag.listCategoryActive = listCategory.OrderBy(c => c.name);

            TableOrderViewModel modelTableOrder = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            ViewBag.TableOrder = modelTableOrder;
            ViewBag.businessName = modelTableOrder.businessName;

            return View();
        }

        public ActionResult ProductList()
        {
            if (Session[ConstantSmoovs.TableOrder.tableCart] == null)
            {
                return RedirectToAction("ScanQRCode", new { id = Guid.NewGuid().ToString() });
            }

            //GetThemeInfo();
            var siteID = GetSiteID();
            if (CacheHelper.Categories == null || !CacheHelper.Categories.Where(c => c != null && c.siteID == siteID).Any())
            {
                if (CacheHelper.Categories == null)
                {
                    CacheHelper.Categories = new List<Menu>();
                }

                var listC = GetAllCategory();
                if (listC != null && listC.Count() > 0)
                {
                    CacheHelper.Categories.AddRange(listC);
                }
            }

            var listCategory = CacheHelper.Categories.Where(c => c != null && c.siteID == siteID).ToList();
            ViewBag.listCategoryActive = listCategory.OrderBy(c => c.name);

            TableOrderViewModel modelTableOrder = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            ViewBag.TableOrder = modelTableOrder;
            ViewBag.businessName = modelTableOrder.businessName;

            return PartialView();
        }

        public ActionResult ProductListByCategory(string CategoryName)
        {
            TableOrderProductsByCategoryViewModel viewmodel = GetListProductsByCategory(CategoryName);

            TableOrderViewModel modelTableOrder = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            ViewBag.TableOrder = modelTableOrder;

            return PartialView(viewmodel.ProductItemList);
        }

        private TableOrderProductsByCategoryViewModel GetListProductsByCategory(string CategoryName)
        {
            string siteID = GetSiteID();

            TableOrderViewModel modelTableOrder = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            string BranchName = modelTableOrder.branchName;

            TableOrderProductsByCategoryViewModel returnModel = new TableOrderProductsByCategoryViewModel();

            WSProductItemClient clientProduct = new WSProductItemClient();
            var listsAll = clientProduct.WSGetAllActiveProductItemByStoreAndCategory(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByCategory, siteID, BranchName, CategoryName);
            clientProduct.Close();

            //Add discount to products
            BaseController baseController = new BaseController();
            returnModel.ProductItemList = baseController.SetDiscountToListProductItems(listsAll.Where(i => i.status).ToList(), siteID);

            return returnModel;
        }

        public ActionResult ProductDetail(string productItemId)
        {
            TableOrderViewModel modelTableOrder = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            ViewBag.TableOrder = modelTableOrder;

            TableOrderProductDetailViewModel viewmodel = GetProductDetail(productItemId);

            return PartialView(viewmodel);
        }

        private TableOrderProductDetailViewModel GetProductDetail(string productItemId)
        {
            string siteID = GetSiteID();

            TableOrderProductDetailViewModel returnModel = new TableOrderProductDetailViewModel();

            WSProductItemClient clientProduct = new WSProductItemClient();
            returnModel.ProductItem = clientProduct.WSDetailProductItem(productItemId, siteID);
            clientProduct.Close();

            var arrayVarient = returnModel.ProductItem.arrayVarient;
            returnModel.ProductItem.arrayVarient = new List<VarientItem>();

            for (var i = 0; i < arrayVarient.Count(); i++)
            {
                if (arrayVarient[i].status)
                {
                    arrayVarient[i].quantityInStock = arrayVarient[i].quantity;
                    returnModel.ProductItem.arrayVarient.Add(arrayVarient[i]);
                }
            }

            //Add discount to product
            BaseController baseController = new BaseController();
            returnModel.ProductItem = baseController.SetDiscountToProductItem(returnModel.ProductItem, siteID);

            return returnModel;
        }
        #endregion

        #region MyCart
        public ActionResult MyCart()
        {
            string siteID = GetSiteID();

            TableOrderViewModel myCartInSession = null;
            if ((Object)Session[ConstantSmoovs.TableOrder.tableCart] != null)
            {
                myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            }
            if (myCartInSession == null)
            {
                return RedirectToAction("ScanQRCode", new { id = Guid.NewGuid().ToString() });
            }
            //MyCart myCartInSession = GetMyCartItems();
            ServiceCharge serviceCharge = new ServiceCharge();
            myCartInSession.receiptValue = CalculateMyCartReceipt(myCartInSession.listProductItems, 0, myCartInSession.branchIndex, serviceCharge);
            Session[ConstantSmoovs.TableOrder.tableCart] = myCartInSession;

            if (myCartInSession.isHost)
            {
                var viewmodel = ObjectHelper.DeepClone<TableOrderViewModel>(myCartInSession);

                var order = new TableOrderModel();
                if (CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).Any())
                {
                    order = CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).FirstOrDefault();
                }
                var orderCopy = ObjectHelper.DeepClone<TableOrderModel>(order);

                List<ProductItem> productItemList = new List<ProductItem>();
                foreach (var item in orderCopy.listProductItems)
                {
                    if (item.userOwner == viewmodel.memberID)
                    {
                        item.userID = ConstantSmoovs.StationTable.Me;
                    }

                    productItemList.Add(item);
                }

                viewmodel.listProductItems = productItemList;
                viewmodel.receiptValue = CalculateMyCartReceipt(viewmodel.listProductItems, 0, myCartInSession.branchIndex, serviceCharge);

                return PartialView(viewmodel);
            }

            return PartialView(myCartInSession);
        }

        [HttpPost]
        public ActionResult AddToCart(ProductItem productItem)
        {
            if (productItem != null)
            {
                //Case: product to add is not null -> update mycart in session and in coughbase

                TableOrderViewModel myCartInSession = null;
                if ((Object)Session[ConstantSmoovs.TableOrder.tableCart] != null)
                {
                    myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
                }

                productItem.userID = myCartInSession.fullName;
                productItem.userOwner = myCartInSession.memberID;

                //else
                //{
                //    Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession = InitializeNewMyCartObject();
                //}
                //Do adding product item into cart
                myCartInSession = AddProductIntoCart(productItem, myCartInSession);
                //Re-assign session cart
                Session[ConstantSmoovs.TableOrder.tableCart] = myCartInSession;
                //If customer logged in -> uppdate coughbase cart
                //StoreMyCartIntoCoughbase(GetSiteID(), myCartInSession);

                if (myCartInSession.isHost)
                {
                    SyncToCache(myCartInSession);
                }
                var count = GetCountProductInTableCart();

                return Json(new { success = true, count = count }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, count = 0 }, JsonRequestBehavior.AllowGet);
        }

        private static void SyncToCache(TableOrderViewModel myCartInSession)
        {
            var order = new TableOrderModel();
            if (CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).Any())
            {
                order = CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).FirstOrDefault();
            }

            order.listProductItems = order.listProductItems.Where(o => o.userOwner != myCartInSession.memberID).ToList();
            order.listProductItems.AddRange(myCartInSession.listProductItems);

            SyncOrderToCache(myCartInSession.id, order);
        }

        private static void SyncOrderToCache(string id, TableOrderModel order)
        {
            //remove order from cache
            CacheHelper.TableOrders = CacheHelper.TableOrders.Where(o => o != null && o.id != id).ToList();
            //add order to cache
            var orderCopy = ObjectHelper.DeepClone<TableOrderModel>(order);
            CacheHelper.TableOrders.Add(orderCopy);
        }

        private TableOrderViewModel AddProductIntoCart(ProductItem productItem, TableOrderViewModel myCartInSession)
        {
            List<ProductItem> listItemsInSession = myCartInSession.listProductItems;

            //Set flag to check this variant to be checked out
            productItem.arrayVarient[0].isCheckedToCheckOut = true;

            //Check current myCart is null or not
            if (listItemsInSession != null && listItemsInSession.Count() > 0)
            {
                //Case: current mycart is not null

                if (listItemsInSession.FindIndex(i => i._id == productItem._id) != -1)
                {
                    //Case: productItem contains in listItemsInSession --> check list variant to update quantity or add new variant

                    List<VarientItem> listVariant = listItemsInSession.FirstOrDefault(i => i._id == productItem._id).arrayVarient;
                    VarientItem productVariantItem = productItem.arrayVarient[0];
                    listItemsInSession.FirstOrDefault(i => i._id == productItem._id).arrayVarient = AddVariantIntoCart(productVariantItem, listVariant);
                }
                else
                {
                    //Case: productItem doesn't contain in listItemsInSession --> add new item
                    listItemsInSession.Add(productItem);
                }
            }
            else
            {
                //Case: current mycart is null

                //Create temp list item
                List<ProductItem> list = new List<ProductItem>();
                list.Add(productItem);
                //Assign temp value into real variable
                listItemsInSession = list;
            }

            //Re-assign value
            //myCartInSession.modifiedDate = DateTime.UtcNow;
            myCartInSession.listProductItems = listItemsInSession;

            return myCartInSession;
        }

        private static List<VarientItem> AddVariantIntoCart(VarientItem productVariantItem, List<VarientItem> listVariant)
        {
            if (listVariant != null && listVariant.Count > 0)
            {
                //Case: listVariant is not null -> add or update listVariant

                if (listVariant.FindIndex(i => i.sku == productVariantItem.sku) != -1)
                {
                    //Case: productItem.variant is existed in listVariant -> update variant's quantity & remark
                    listVariant.FirstOrDefault(v => v.sku == productVariantItem.sku).quantity += productVariantItem.quantity;
                    listVariant.FirstOrDefault(v => v.sku == productVariantItem.sku).variantRemark += productVariantItem.variantRemark + " ";
                    listVariant.FirstOrDefault(v => v.sku == productVariantItem.sku).isCheckedToCheckOut = productVariantItem.isCheckedToCheckOut;
                }
                else
                {
                    //Case: productItem.variant is not existed in listVariant -> add new variant
                    listVariant.Add(productVariantItem);
                }
            }
            else
            {
                //Case: listVariant is null -> create new listVariant

                //Create temp list item
                List<VarientItem> list = new List<VarientItem>();
                list.Add(productVariantItem);
                //Assign temp value into real variable
                listVariant = list;
            }

            return listVariant;
        }

        public void StoreMyCartIntoCoughbase(string siteID, TableOrderViewModel myCart)
        {
            if (Session[ConstantSmoovs.Users.userName] != null && !string.IsNullOrEmpty(Session[ConstantSmoovs.Users.userName].ToString()))
            {
                //Get member's cart from coughbase
                WSMyCartClient client = new WSMyCartClient();
                MyCart myCartInCoughbase = client.WSGetMyCartByEmail(ConstantSmoovs.CouchBase.DesignDocMyCart, ConstantSmoovs.CouchBase.ViewNameGetDetailMyCartByEmail, siteID, myCart.email);

                if (myCart != null && myCartInCoughbase != null)
                {
                    myCartInCoughbase.listProductItems = (myCart.listProductItems != null) ? myCart.listProductItems : new List<ProductItem>();
                    myCartInCoughbase.receiptValue = new ReceiptValue();
                    //myCartInCoughbase.remark = myCart.remark.Trim();
                    myCartInCoughbase.modifiedDate = DateTime.UtcNow;

                    //Store new values into coughbase
                    //client.WSUpdateMyCart(myCartInCoughbase, siteID);
                }

                client.Close();
            }
        }

        [HttpPost]
        public ActionResult changeQuantity(string variantId, string productItemId, string qty)
        {
            TableOrderViewModel myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            bool ishas = false;
            foreach (var itemMycart in myCartInSession.listProductItems.Where(r => r._id == productItemId))
            {
                foreach (var varientCart in itemMycart.arrayVarient.Where(r => (r._id + itemMycart.userOwner) == variantId))
                {
                    string strtotal;
                    string btnPay;
                    StringBuilder builder;
                    double totalItem;
                    ishas = updateQuantity(qty, myCartInSession, ishas, itemMycart, varientCart, out strtotal, out btnPay, out builder, out totalItem);
                    var model = Mapper.Map<TableOrderViewModel, TableOrderModel>(myCartInSession);
                    if (myCartInSession.isHost)
                    {
                        SyncToCache(myCartInSession);
                        model = CacheHelper.TableOrders.Where(o => o != null && o.id == myCartInSession.id).FirstOrDefault();
                    }
                    bool isVariantStock = false;
                    bool isStock = GetIsStock(model, variantId, productItemId, out isVariantStock);

                    return Json(new { success = true, count = GetCountProductInTableCart(), total = strtotal, btnPay = btnPay, tax = builder.ToString(), totalItem = string.Format("{0:0.00}", totalItem), isStock = isStock, isVariantStock = isVariantStock, isChangeClient = false }, JsonRequestBehavior.AllowGet);
                }
            }
            if (!ishas && myCartInSession.isHost)
            {
                var order = new TableOrderModel();
                if (CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).Any())
                {
                    order = CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).FirstOrDefault();
                }

                foreach (var itemMycart in order.listProductItems.Where(p => p._id == productItemId).ToList())
                {
                    foreach (var varientCart in itemMycart.arrayVarient.Where(r => (r._id + itemMycart.userOwner) == variantId))
                    {
                        string strtotal;
                        string btnPay;
                        StringBuilder builder;
                        double totalItem;
                        ishas = updateQuantity(qty, myCartInSession, ishas, itemMycart, varientCart, out strtotal, out btnPay, out builder, out totalItem);

                        SyncOrderToCache(myCartInSession.id, order);

                        bool isVariantStock = false;
                        bool isStock = GetIsStock(order, variantId, productItemId, out isVariantStock);
                        var isChangeClient = false;
                        if (itemMycart.userOwner != myCartInSession.memberID)
                        {
                            isChangeClient = true;
                        }
                        return Json(new { success = true, count = GetCountProductInTableCart(), total = strtotal, btnPay = btnPay, tax = builder.ToString(), totalItem = string.Format("{0:0.00}", totalItem), isStock = isStock, isVariantStock = isVariantStock, isChangeClient = isChangeClient }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        private static bool GetIsStock(TableOrderModel model, string variantId, string productItemId, out bool isVariantStock)
        {
            isVariantStock = false;
            var myCartInSession = Mapper.Map<TableOrderModel, TableOrderViewModel>(model);//clone

            List<ProductItem> listProducts = new List<ProductItem>();
            foreach (var productItem in myCartInSession.listProductItems)
            {
                if (!listProducts.Any(p => p._id == productItem._id))
                {
                    var productN = new ProductItem();
                    productN._id = productItem._id;
                    productN.arrayVarient = new List<VarientItem>();
                    foreach (var variant in productItem.arrayVarient)
                    {
                        var varientItem = new VarientItem();
                        varientItem._id = variant._id;
                        varientItem.quantity = variant.quantity;
                        varientItem.quantityInStock = variant.quantityInStock;
                        productN.arrayVarient.Add(varientItem);
                    }
                    listProducts.Add(productN);
                }
                else
                {
                    for (int p = 0; p < listProducts.Count; p++)
                    {
                        foreach (var variant in productItem.arrayVarient)
                        {
                            if (!listProducts[p].arrayVarient.Any(v => v._id == variant._id))
                            {
                                var varientItem = new VarientItem();
                                varientItem._id = variant._id;
                                varientItem.quantity = variant.quantity;
                                varientItem.quantityInStock = variant.quantityInStock;
                                listProducts[p].arrayVarient.Add(varientItem);
                            }
                            else
                            {
                                var itemVariant = listProducts[p].arrayVarient.Where(v => v._id == variant._id).FirstOrDefault();
                                for (int v = 0; v < listProducts[p].arrayVarient.Count; v++)
                                {
                                    if (listProducts[p].arrayVarient[v]._id == variant._id)
                                    {
                                        listProducts[p].arrayVarient[v].quantity = listProducts[p].arrayVarient[v].quantity + variant.quantity;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

            }
            bool isStock = false;
            variantId = variantId.Substring(0, variantId.IndexOf(ConstantSmoovs.StationTable.memberId));
            foreach (var item in listProducts)
            {
                if (item.arrayVarient.Any(v => v.quantityInStock >= 0 && v.quantity > v.quantityInStock))
                {
                    isStock = true;
                    if (item._id == productItemId && item.arrayVarient.Any(v => v._id == variantId && v.quantityInStock >= 0 && v.quantity > v.quantityInStock))
                    {
                        isVariantStock = true;
                    }
                }
            }
            return isStock;
        }

        private bool updateQuantity(string qty, TableOrderViewModel myCartInSession, bool ishas, ProductItem itemMycart, VarientItem varientCart, out string strtotal, out string btnPay, out StringBuilder builder, out double totalItem)
        {
            varientCart.quantity = Convert.ToInt32(qty);
            ServiceCharge serviceCharge = new ServiceCharge();
            myCartInSession.receiptValue = CalculateMyCartReceipt(myCartInSession.listProductItems, 0, myCartInSession.branchIndex, serviceCharge);
            //myCartInSession.modifiedDate = DateTime.UtcNow;

            Session[ConstantSmoovs.TableOrder.tableCart] = myCartInSession;
            //StoreMyCartIntoCoughbase(siteID, myCartInSession);

            ishas = true;
            //sub total
            var total = (myCartInSession == null || myCartInSession.listProductItems == null || myCartInSession.listProductItems.Count == 0)
                        ? 0
                        : myCartInSession.receiptValue.subTotal;

            strtotal = string.Format("{0:0.00}", total);
            strtotal = string.Format("{0} {1}", myCartInSession.currency, strtotal);

            //grand total
            btnPay = DependencyResolver.Current.GetService<ILanguageManager>().GetButtonText("PAY");
            if (!myCartInSession.isHost)
            {
                btnPay = DependencyResolver.Current.GetService<ILanguageManager>().GetButtonText("SUBMIT");
            }
            btnPay = string.Format("{0} {1} {2}", btnPay, myCartInSession.currency, myCartInSession.receiptValue.grandTotal);

            //tax
            builder = new StringBuilder();

            builder.Append("<table width=\"100%\">");

            if (myCartInSession.receiptValue.serviceChargeRate >= 0)
            {
                var taxtDisplay = string.Format("Service Charge ({0}%)", myCartInSession.receiptValue.serviceChargeRate);
                builder.Append("<tr width=\"100%\" style=\"height:100%!important\">");
                builder.Append("<td><span>" + taxtDisplay + "</span></td>");
                builder.Append("<td style=\"text-align:right\">");
                builder.Append("<span class=\"p-right\">");
                builder.Append(myCartInSession.currency + " " + string.Format("{0:0.00}", myCartInSession.receiptValue.serviceCharge));
                builder.Append("</span></td></tr>");
            }
            foreach (Com.SmoovPOS.Entity.TaxValue item in myCartInSession.receiptValue.listTaxOrder)
            {
                builder.Append("<tr width=\"100%\" style=\"height:100%!important\">");
                builder.Append("<td><span>" + item.taxName + " (" + item.percent + "%)</span></td>");
                builder.Append("<td style=\"text-align:right\">");
                builder.Append("<span class=\"p-right\">");
                builder.Append(myCartInSession.currency + " " + string.Format("{0:0.00}", item.taxValue));
                builder.Append("</span></td></tr>");
            }
            builder.Append("</table>");

            //total item
            totalItem = 0.00;
            if (itemMycart.DiscountProductItem != null && itemMycart.discount)
            {
                if (itemMycart.DiscountProductItem.type == 0)
                {
                    var saveMoney = (double)Math.Round((decimal)Decimal.Parse(String.Format("{0:0.00}", (varientCart.price * varientCart.quantity * itemMycart.DiscountProductItem.discountValue / 100))), 2);
                    totalItem = (double)Math.Round((decimal)Decimal.Parse(String.Format("{0:0.00}", (varientCart.price * varientCart.quantity) - saveMoney)), 2);
                }
                else
                {
                    totalItem = (double)Math.Round((decimal)Decimal.Parse(String.Format("{0:0.00}", (varientCart.price * varientCart.quantity) - itemMycart.DiscountProductItem.discountValue * varientCart.quantity)), 2);
                    totalItem = totalItem > 0 ? totalItem : 0.00;
                }
            }
            else
            {
                totalItem = (double)Math.Round((decimal)Decimal.Parse(String.Format("{0:0.00}", varientCart.price * varientCart.quantity)), 2);
            }
            return ishas;
        }

        public ActionResult OutOfTime()
        {
            TableOrderingSetting setting = (TableOrderingSetting)TempData[ConstantSmoovs.TableOrder.TableOrderingSetting];
            return View(setting);
        }

        public ActionResult ClearHost()
        {
            return View();
        }

        public ActionResult NotAvailable()
        {
            return View();
        }

        #endregion

        #region Payment

        public ActionResult Payment(string remark)
        {
            TableOrderViewModel myCartInSession = null;
            if ((Object)Session[ConstantSmoovs.TableOrder.tableCart] != null)
            {
                myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            }
            else
            {
                return RedirectToAction("ScanQRCode", new { id = Guid.NewGuid().ToString() });
            }
            if (myCartInSession == null || !CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).Any())
            {
                return RedirectToAction("PaymentDone");
            }

            ViewBag.TableOrder = myCartInSession;
            if (!myCartInSession.isHost)
            {
                SyncToCache(myCartInSession);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            var siteID = GetSiteID();

            var order = new TableOrderModel();
            if (CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).Any())
            {
                order = CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).FirstOrDefault();
            }
            TableOrderPaymentViewModel model = new TableOrderPaymentViewModel();
            model.tableOrderModel = order;
            WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
            model.accountSmoovPay = clientSetting.GetDetailAccountSmoovpay(Common.ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAccountSmoovPay, siteID);
            clientSetting.Close();
            model.tableOrderModel.receiptValue = new ReceiptValue();

            #region CreateOrder

            //Get and set merchant's currency
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            ViewBag.currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
            merchantGeneralSettingClient.Close();

            OrderModel orderModel = new OrderModel();
            orderModel.Order = new Order();
            //init buyer's and recipient's information 
            Customer buyer = InitializeNewCustomer(siteID);
            buyer.memberID = myCartInSession.memberID;
            buyer.firstName = myCartInSession.firstName;
            buyer.lastName = myCartInSession.lastName;
            buyer.fullName = myCartInSession.firstName + " " + myCartInSession.lastName;
            buyer.phoneRegion = myCartInSession.phoneRegion;
            buyer.phone = myCartInSession.phone;
            buyer.email = myCartInSession.email;
            buyer.remark = myCartInSession.remark;

            orderModel.Order.customerSender = buyer;
            orderModel.Order.customerReceiver = buyer;
            orderModel.Order._id = order.orderId;
            orderModel.Order.createdDate = DateTime.UtcNow;
            orderModel.Order.modifiedDate = DateTime.UtcNow;
            orderModel.Order.currency = ViewBag.currency;
            orderModel.Order.status = true;
            orderModel.Order.transactionID = "TR" + DateTime.UtcNow.ToString("ddMMyyyyHHmmssffff");
            orderModel.Order.completedDate = DateTime.UtcNow;
            orderModel.Order.orderCode = myCartInSession.branchIndex.ToString() + "-" + DateTime.UtcNow.ToString("ddMMyyyyHHmmss");

            if (Session[ConstantSmoovs.TableOrder.inventory] != null)
            {
                orderModel.Order.inventory = (Inventory)Session[ConstantSmoovs.TableOrder.inventory];
            }
            else
            {
                WSInventoryClient clientInventory = new WSInventoryClient();
                orderModel.Order.inventory = clientInventory.WSDetailInventoryByIndex(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, siteID, myCartInSession.branchIndex);
                clientInventory.Close();
            }
            double shippingFee = 0;
            //if (shippingFee > 0)
            //{
            ServiceCharge serviceCharge = new ServiceCharge();
            order.receiptValue = CalculateMyCartReceipt(order.listProductItems, shippingFee, myCartInSession.branchIndex, serviceCharge);
            //}
            orderModel.Order.subTotal = order.receiptValue.subTotal;
            orderModel.Order.listTaxvalue = order.receiptValue.listTaxOrder;
            orderModel.Order.grandTotal = order.receiptValue.grandTotal;
            orderModel.Order.roundAmount = order.receiptValue.rountAmt;
            orderModel.Order.discount = order.receiptValue.discountTotal;

            var serviceChargePOS = new ServiceChargePOS();
            serviceChargePOS.serviceChargeRate = serviceCharge.serviceChargeRate;
            serviceChargePOS.serviceChargeValue = serviceCharge.serviceChargeValue;
            serviceChargePOS.serviceChargeName = ConstantSmoovs.StationTable.ServiceCharge;

            orderModel.Order.serviceCharge = serviceChargePOS;

            //Initialize list order
            List<Orderdetail> listOrderDetails = new List<Orderdetail>();
            Orderdetail orderDetail = new Orderdetail();
            orderDetail.dateOrder = orderModel.Order.completedDate;
            orderDetail.orderCode = orderModel.Order.orderCode;
            orderDetail.payType = "SmoovPay";
            orderDetail.total = order.receiptValue.grandTotal;
            orderDetail.timeZone = myCartInSession.timeZone;

            //Retrieve member by email
            WSCustomerClient clientCustomer = new WSCustomerClient();
            Customer member = clientCustomer.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, buyer.email, siteID);

            if (member == null)
            {
                //Case: email has not used before -> create new guest-role member

                listOrderDetails.Add(orderDetail);
                //Create new guest-role member
                buyer.listOrder = listOrderDetails;
                buyer.totalOrder = 1;
                buyer.totalSpend = order.receiptValue.grandTotal;
                buyer.modifiedDate = orderModel.Order.completedDate;
                buyer.lastOrder = orderModel.Order.completedDate;
                buyer.customerType = (int)ConstantSmoovs.Enums.CustomerType.Guest;
                clientCustomer.WSCreateCustomer(buyer, siteID);
            }
            else
            {
                //Case: email has used before -> update the existed member (member-role or guest-roles)

                if (member.listOrder != null) listOrderDetails = member.listOrder;
                listOrderDetails.Add(orderDetail);

                //Update member's info
                member.lastOrder = orderModel.Order.completedDate;
                member.modifiedDate = orderModel.Order.completedDate;
                member.listOrder = listOrderDetails;
                member.totalOrder += 1;
                member.totalSpend += order.receiptValue.grandTotal;
                clientCustomer.WSUpdateCustomer(member, siteID);
            }
            clientCustomer.Close();
            orderModel.Order.paymentStatus = ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.PaymentStatus.Waiting).Key;
            orderModel.Order.orderStatus = ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.OrderStatus.OnPreRequest).Key;
            orderModel.Order.status = true;
            orderModel.Order.orderType = (int)ConstantSmoovs.Enums.OrderType.TableOrdering;

            //Update order's list product items
            List<ProductOrder> ProductOrderList = new List<ProductOrder>();
            foreach (var productItem in order.listProductItems)
            {
                ProductOrder productOrder = new ProductOrder();
                productOrder._id = productItem._id;
                productOrder.sku = productItem.sku;
                productOrder.title = productItem.title;
                productOrder.categoryId = productItem.categoryId;
                productOrder.arrayVariantOrder = productItem.arrayVarient;
                productOrder.arrayInventory = productItem.arrayInventory;
                productOrder.discount = productItem.discount;
                productOrder.discountProductItem = productItem.DiscountProductItem;
                ProductOrderList.Add(productOrder);
            }
            orderModel.Order.arrayProduct = ProductOrderList;
            //Update order's remark
            orderModel.Order.remark = remark;
            orderModel.Order.channels = new String[] { siteID + "_" + myCartInSession.branchIndex.ToString() + "_table" };
            var station = Mapper.Map<TableOrderViewModel, Station>(myCartInSession);
            orderModel.Order.station = station;

            orderModel.Order.arrayStatusHistory = new List<OrderStatusHistory>() { };
            //Save order into coughbase
            WSOrderClient client = new WSOrderClient();
            var result = client.WSCreateOrder(orderModel.Order, siteID);
            client.Close();
            #endregion
            var resultRealTime = SendOrderToMerchant(siteID, null, orderModel.Order, ConstantSmoovs.Enums.TypeMessage.FirstOrDefault(r => r.Value == ConstantSmoovs.TypeMessage.NewOrder).Key);
            model.tableOrderModel.receiptValue = order.receiptValue;
            model.transactionId = orderModel.Order.transactionID;

            order.ref_id = model.transactionId;
            order.remark = remark;

            SyncOrderToCache(myCartInSession.id, order);

            return View(model);
        }
        public ApiSmoovPay SendOrderToMerchant(string siteID, NotifyOrder notifyOrder, Order order, int typeMessage)
        {
            using (var client = new WebClient())
            {
                var values = new NameValueCollection { };
                var url = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteApiSmoovPOS] + "/TableOrder/WSUpdateOrder";
                if (typeMessage == ConstantSmoovs.Enums.TypeMessage.FirstOrDefault(r => r.Value == ConstantSmoovs.TypeMessage.NewOrder).Key)
                {
                    values = new NameValueCollection
                {
                    { "Authorization", siteID },
                    { "merchantID", siteID },
                    { "result", new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(order) },
                    { "typeMessage", typeMessage.ToString()}
                };
                }
                else if (typeMessage == ConstantSmoovs.Enums.TypeMessage.FirstOrDefault(r => r.Value == ConstantSmoovs.TypeMessage.UpdateOrderStatus).Key)
                {
                    values = new NameValueCollection
                {
                    { "Authorization", siteID },
                    { "merchantID", siteID },
                    { "result", new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(notifyOrder) },
                    { "typeMessage", typeMessage.ToString()}
                };
                }
                try
                {
                    var responsebytes = client.UploadValues(url, "POST", values);
                    var responsebody = Encoding.UTF8.GetString(responsebytes);
                    var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(responsebody);
                    return JsonResponse;
                }
                catch
                {
                    return null;
                }

            }
        }
        public ActionResult PaymentDone()
        {
            var siteID = GetSiteID();
            TableOrderViewModel myCartInSession = null;
            if (Session[ConstantSmoovs.TableOrder.tableCart] != null)
            {
                if ((Object)Session[ConstantSmoovs.TableOrder.tableCart] != null)
                {
                    myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
                }
                //new Thread(() => {
                //    Thread.Sleep(1200000);          
                //}).Start();
                Session[ConstantSmoovs.TableOrder.tableCart] = null;
                CacheHelper.CustomerInfos = CacheHelper.CustomerInfos.Where(c => c.customerHost != null && c.customerHost.memberID != myCartInSession.memberID).ToList();

                WSOrderClient wsOrder = new WSOrderClient();
                Order orderUpdate = wsOrder.WSGetOrder(myCartInSession.orderId, siteID);
                wsOrder.Close();
                var paymentDoneDate = TimeZoneUtil.ConvertUTCToTimeZone(orderUpdate.createdDate, myCartInSession.timeZone, siteID);
                myCartInSession.paymentDoneDate = paymentDoneDate.ToString(ConstantSmoovs.DefaultFormats.JqueryShortTime);
                var lstHM = myCartInSession.paymentDoneDate.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (lstHM.Length > 1)
                {
                    if (lstHM[0].Length <= 1)
                    {
                        lstHM[0] = "0" + lstHM[0];
                    }
                    if (lstHM[1].Length <= 1)
                    {
                        lstHM[1] = "0" + lstHM[1];
                    }
                    myCartInSession.paymentDoneDate = string.Format("{0}:{1}", lstHM[0], lstHM[1]);
                }

                Session[ConstantSmoovs.TableOrder.tableOrder] = myCartInSession;
            }

            if ((Object)Session[ConstantSmoovs.TableOrder.tableOrder] != null)
            {
                myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableOrder];
            }
            else
            {
                return RedirectToAction("ScanQRCode", new { id = Guid.NewGuid().ToString() });
            }

            ViewBag.TableOrder = myCartInSession;

            if (myCartInSession != null && myCartInSession.isHost)
            {
                var url = myCartInSession.stationQRCode;
                url = url.Substring(0, url.IndexOf(@"/TableOrder/ScanQRCode"));
                var connection = new HubConnection(url);
                IHubProxy myHub = connection.CreateHubProxy("ChatHub");

                connection.Start().Wait(); // not sure if you need this if you are simply posting to the hub

                myHub.Invoke("SendPrivateMessage", new object[] { myCartInSession.id, ConstantSmoovs.StationTable.PaymentTableOrder });

                connection.Dispose();
            }
            ViewBag.businessName = myCartInSession.businessName;
            return View(myCartInSession);
        }
        public ActionResult SuccessPage(string merchant, string ref_id, string reference_code, int response_code, string currency, string total_amount, string signature, string signature_algorithm)
        {
            string siteID = GetSiteID();
            TableOrderViewModel myCartInSession = null;
            if ((Object)Session[ConstantSmoovs.TableOrder.tableCart] != null)
            {
                myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            }

            var order = new TableOrderModel();
            if (CacheHelper.TableOrders.Where(c => c != null && c.ref_id == ref_id).Any())
            {
                order = CacheHelper.TableOrders.Where(c => c != null && c.ref_id == ref_id).FirstOrDefault();
            }
            else
            {
                return RedirectToAction("PaymentDone");
            }

            WSOrderClient wsOrder = new WSOrderClient();
            Order orderUpdate = wsOrder.WSGetOrder(order.orderId, siteID);
            orderUpdate.paymentStatus = ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.PaymentStatus.Paid).Key;
            orderUpdate.paidDate = DateTime.UtcNow;
            orderUpdate.orderStatus = ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.OrderStatus.DineInWaiting).Key;
            orderUpdate.orderType = (int)ConstantSmoovs.Enums.OrderType.TableOrdering;
            orderUpdate.display = true;
            // update array payment
            List<PaymentItem> listPayment = new List<PaymentItem>();
            PaymentItem paymentItem = new PaymentItem();
            paymentItem.paymentType = "SmoovPay";
            paymentItem.display = true;
            paymentItem.createdDate = DateTime.UtcNow;
            paymentItem.charged = orderUpdate.grandTotal;
            listPayment.Add(paymentItem);
            orderUpdate.arrayPayment = listPayment;
            orderUpdate.referenceCode = reference_code;
            for (var i = 0; i < orderUpdate.arrayPayment.Count(); i++)
            {
                orderUpdate.arrayPayment[i].referenceCode = reference_code;
            }

            OrderStatusHistory orderStatusHistory = new OrderStatusHistory();

            orderStatusHistory.orderStatus = ConstantSmoovs.Enums.OrderStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.OrderStatus.DineInWaiting).Key;
            orderStatusHistory.modifiedDate = DateTime.UtcNow;

            if (orderUpdate.arrayStatusHistory == null)
            {
                orderUpdate.arrayStatusHistory = new List<OrderStatusHistory>() { orderStatusHistory };
            }
            else
            {
                orderUpdate.arrayStatusHistory.Add(orderStatusHistory);
            }
            // end
            wsOrder.WSUpdateOrder(orderUpdate, siteID);
            wsOrder.Close();

            //Update status merchant realtime
            var obj = new NotifyOrder();
            obj.orderID = orderUpdate._id;
            obj.orderStatus = orderUpdate.orderStatus;
            obj.paymentStatus = orderUpdate.paymentStatus;
            obj.stationID = orderUpdate.station._id;
            SendOrderToMerchant(siteID, obj, null, ConstantSmoovs.Enums.TypeMessage.FirstOrDefault(r => r.Value == ConstantSmoovs.TypeMessage.UpdateOrderStatus).Key);
            //Update quantity productItem
            ProductItem[] listProducts = new ProductItem[order.listProductItems.Count()];
            listProducts = order.listProductItems.ToArray();
            WSProductItemClient wsProductItem = new WSProductItemClient();
            wsProductItem.WSUpdateQuantityProductItemByStore(listProducts, order.branchIndex, siteID);
            wsProductItem.Close();

            //remove order from cache
            CacheHelper.TableOrders = CacheHelper.TableOrders.Where(o => o != null && o.id != myCartInSession.id).ToList();
            //remove customer from cache
            CacheHelper.CustomerInfos = CacheHelper.CustomerInfos.Where(c => c != null && c._id != myCartInSession.id).ToList();

            ViewBag.TableOrder = myCartInSession;

            WSStationClient clientStation = new WSStationClient();

            var model = clientStation.WSGetDetailStation(myCartInSession.id);
            if (model != null)
            {
                model.statusOrdering = ConstantSmoovs.Enums.StationTableStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.StationTableStatus.Empty).Key;
                model.modifiedDate = DateTime.UtcNow;
                model.userID = User.Identity.GetUserId();
                clientStation.WSUpdateStation(model, GetSiteID());
            }
            TableOrderingSetting setting = null;
            var allowPushNotification = true;
            if ((Object)Session[ConstantSmoovs.TableOrder.TableOrderingSetting] != null)
            {
                setting = (TableOrderingSetting)Session[ConstantSmoovs.TableOrder.TableOrderingSetting];
                if (setting != null)
                {
                    allowPushNotification = setting.AllowPushNotification;
                }
            }

            if (allowPushNotification)
            {
                var message = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText(ConstantSmoovs.Attributes.InfomationMessageNotification);
                var badge = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.AppleNotification].ToString());
                WSCheckoutSettingClient clientCheckoutSetting = new WSCheckoutSettingClient();

                message = string.Format(message, myCartInSession.stationName);
                var sendSuccess = clientCheckoutSetting.SendNotification(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, siteID, myCartInSession.branchIndex, message, ConstantSmoovs.AppTypes.table);
            }

            return RedirectToAction("PaymentDone", myCartInSession);
        }
        [HttpPost]
        public ActionResult SyncSessionCache()
        {
            TableOrderViewModel myCartInSession = null;
            if ((Object)Session[ConstantSmoovs.TableOrder.tableCart] != null)
            {
                myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            }

            var order = new TableOrderModel();
            if (CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).Any())
            {
                order = CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).FirstOrDefault();
            }

            var orderCopy = ObjectHelper.DeepClone<TableOrderModel>(order);
            var productList = orderCopy.listProductItems.Where(p => p.userOwner == myCartInSession.memberID).ToList();
            myCartInSession.listProductItems = productList;
            Session[ConstantSmoovs.TableOrder.tableCart] = myCartInSession;
            var count = GetCountProductInTableCart();
            return Json(new { success = true, count = count }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PaymentHistory(string id, int orderStatus)
        {
            TableOrderViewModel myCartInSession = null;
            if ((Object)Session[ConstantSmoovs.TableOrder.tableOrder] != null)
            {
                myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableOrder];
            }

            string siteID = GetSiteID();
            WSOrderClient wsOrder = new WSOrderClient();
            Order order = wsOrder.WSGetOrder(id, siteID);
            wsOrder.Close();
            if (orderStatus >= 0 && !order.arrayStatusHistory.Any(o => o.orderStatus == orderStatus))
            {
                var statusHistory = new OrderStatusHistory();
                statusHistory.orderStatus = orderStatus;
                statusHistory.modifiedDate = DateTime.UtcNow;

                order.arrayStatusHistory.Add(statusHistory);
            }
            foreach (var item in order.arrayStatusHistory)
            {
                if (item.modifiedDate == null)
                {
                    item.modifiedDate = DateTime.UtcNow;
                }

                var localTime = TimeZoneUtil.ConvertUTCToTimeZone(item.modifiedDate, myCartInSession.timeZone, siteID);

                var dateDisplay = localTime.ToString(ConstantSmoovs.DefaultFormats.JqueryShortTime);
                var lstHM = dateDisplay.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (lstHM.Length > 1)
                {
                    if (lstHM[0].Length <= 1)
                    {
                        lstHM[0] = "0" + lstHM[0];
                    }
                    if (lstHM[1].Length <= 1)
                    {
                        lstHM[1] = "0" + lstHM[1];
                    }

                    dateDisplay = string.Format("{0}:{1}", lstHM[0], lstHM[1]);
                }

                item.modifiedDateDisplay = dateDisplay;
            }

            return Json(new { success = true, arrayStatusHistory = order.arrayStatusHistory }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region  Change Host
        [HttpPost]
        public ActionResult ChangedHost()
        {
            TableOrderViewModel myCartInSession = null;
            if ((Object)Session[ConstantSmoovs.TableOrder.tableCart] != null)
            {
                myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
            }

            var order = new TableOrderModel();
            if (CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).Any())
            {
                order = CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).FirstOrDefault();
            }

            if (order != null && myCartInSession != null && order.email == myCartInSession.email && order.memberID == myCartInSession.memberID)
            {
                myCartInSession.isHost = true;
            }
            else
            {
                myCartInSession.isHost = false;
            }

            Session[ConstantSmoovs.TableOrder.tableCart] = myCartInSession;

            var titleTableOrdering = string.Format("{0} ({1})", myCartInSession.stationName, myCartInSession.TitleHostOrClient);
            var count = GetCountProductInTableCart();
            return Json(new { success = true, titleTableOrdering = titleTableOrdering, count = count, isHost = myCartInSession.isHost }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ChangingHost(string memberID)
        {
            var current_station = CacheHelper.CustomerInfos.FirstOrDefault(c => c.customerHost != null && c.customerHost.memberID == memberID);
            if (current_station != null)
            {
                TableOrderViewModel myCartInSession = null;
                if ((Object)Session[ConstantSmoovs.TableOrder.tableCart] != null)
                {
                    myCartInSession = (TableOrderViewModel)Session[ConstantSmoovs.TableOrder.tableCart];
                    myCartInSession.isHost = false;
                    Session[ConstantSmoovs.TableOrder.tableCart] = myCartInSession;
                }

                var order = new TableOrderModel();
                if (CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).Any())
                {
                    var customer = current_station.customerHost;
                    order = CacheHelper.TableOrders.Where(c => c != null && c.id == myCartInSession.id).FirstOrDefault();

                    order.memberID = customer.memberID;
                    order.firstName = customer.firstName;
                    order.lastName = customer.lastName;
                    order.fullName = customer.fullName;
                    order.phoneRegion = customer.phoneRegion;
                    order.phone = customer.phone;
                    order.email = customer.email;

                    SyncOrderToCache(myCartInSession.id, order);
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadListMember(string id)
        {
            var order = CacheHelper.TableOrders.Where(c => c != null && c.id == id).FirstOrDefault();

            var current_station = CacheHelper.CustomerInfos.Where(c => c._id == id && c.customerHost.email != order.email);
            if (current_station != null)
            {
                var select_distinct = (from c in current_station
                                       select new
                                       {
                                           c.customerHost.email,
                                           c.customerHost.firstName,
                                           c.customerHost.lastName,
                                           c.customerHost.fullName,
                                           c.customerHost.memberID
                                       }).Distinct().ToList();
                for (int i = select_distinct.Count - 1; i >= 0; i--)
                {
                    if (select_distinct.Count(c => c.email == select_distinct[i].email) > 1)
                    {
                        select_distinct.RemoveAt(i);
                    }
                }
                //var select_distinct = current_station.Select(c => new { c.customerHost.email, c.customerHost.firstName, c.customerHost.lastName, c.customerHost.fullName, c.customerHost.memberID }).Distinct();
                return Json(new { success = true, value = select_distinct }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false, value = "" }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}