﻿using Com.SmoovPOS.Entity;
using online.Controllers;
using online.Models;
using SmoovPOS.Common;
using SmoovPOS.Entity.Models.Paging;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSOnlineReference;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class BestSellerController : BaseController
    {
        // GET: BestSeller
        public ActionResult Index(string theme = "")
        {
            ViewBag.Class = "BestSeller";
            GetThemeInfo(theme);
            string siteID = ViewBag.siteID;
            int pageLimit = 20; //Count of displayed item in a page

            //Begin: Get and set merchant's general settings
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            MerchantGeneralSetting generalSettings = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID);
            merchantGeneralSettingClient.Close();
            string currency = !string.IsNullOrWhiteSpace(generalSettings.Currency) ? generalSettings.Currency : "";
            int countOfBestSellerProduct = !string.IsNullOrEmpty(generalSettings.CountOfBestSellerProduct) ? int.Parse(generalSettings.CountOfBestSellerProduct) : 99999;//If generalSettings.CountOfBestSellerProduct==null -> set it to maximum
            //End: Get and set merchant's general settings

            ProductByCategoryModel productItems = new ProductByCategoryModel();
            //Begin: Initialize paging
            productItems.Paging = new EntityPaging()
            {
                startKey = null,
                nextKey = null,
                limit = pageLimit,
                pageGoTo = 1,
                pageCurrent = 1,
                sort = "_id",
                desc = false
            };
            //End: Initialize paging

            //Begin: Get all best seller item
            WSOnlineClient clientOline = new WSOnlineClient();
            var listsAll = clientOline.WSGetAllActiveProductItemBestSeller(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.ProductItemViewNameAllBestSeller, productItems.Paging, siteID);
            productItems.ProductItemList = listsAll.Where(i => i.countOfSoldItem >= countOfBestSellerProduct).OrderByDescending(i => i.countOfSoldItem).Take(pageLimit).ToList();

            // Add discount to products
            BaseController baseController = new BaseController();
            productItems.ProductItemList = baseController.SetDiscountToListProductItems(productItems.ProductItemList, siteID);

            clientOline.Close();
            //End: Get all best seller item
            productItems.Currency = !string.IsNullOrWhiteSpace(currency) ? currency : "";
            productItems.countOfBestSellerProduct = countOfBestSellerProduct;

            return View(productItems);
        }

        [HttpPost]
        public ActionResult LoadMoreBestSellerProduct(int limit, int pageGoTo, int countOfBestSellerProduct, string currency)
        {
            GetThemeInfo();
            string siteID = ViewBag.siteID;

            //Begin: Get and set merchant's general settings
            if (string.IsNullOrEmpty(currency) || countOfBestSellerProduct <= 0)
            {
                WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
                MerchantGeneralSetting generalSettings = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID);
                merchantGeneralSettingClient.Close();

                countOfBestSellerProduct = !string.IsNullOrEmpty(generalSettings.CountOfBestSellerProduct) ? int.Parse(generalSettings.CountOfBestSellerProduct) : 99999;

                currency = !string.IsNullOrWhiteSpace(generalSettings.Currency) ? generalSettings.Currency : "";
            }
            //End: Get and set merchant's general settings

            ProductByCategoryModel productItemByCategoryList = new ProductByCategoryModel();
            //Begin: Initialize paging
            productItemByCategoryList.Paging = new EntityPaging()
            {
                limit = limit,
                pageGoTo = pageGoTo
            };
            //End: Initialize paging

            //Begin: Get all remaining best seller item
            WSOnlineClient clientOline = new WSOnlineClient();
            var listsAll = clientOline.WSGetAllActiveProductItemBestSeller(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.ProductItemViewNameAllBestSeller, productItemByCategoryList.Paging, siteID);
            var allProductItemList = listsAll.Where(i => i.countOfSoldItem >= countOfBestSellerProduct).OrderByDescending(i => i.countOfSoldItem);
            productItemByCategoryList.ProductItemList = allProductItemList.Take(limit).ToList();

            // Add discount to products
            BaseController baseController = new BaseController();
            productItemByCategoryList.ProductItemList = baseController.SetDiscountToListProductItems(productItemByCategoryList.ProductItemList, siteID);

            clientOline.Close();
            //End: Get all remaining best seller item

            productItemByCategoryList.Currency = !string.IsNullOrWhiteSpace(currency) ? currency : "";

            var isLoadMore = (allProductItemList.Count() > limit);
            
            return Json(new { isLoadMore = isLoadMore, Data = productItemByCategoryList });
        }
    }
}