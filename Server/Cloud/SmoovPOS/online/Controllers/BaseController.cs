﻿using Com.SmoovPOS.Entity;
using Microsoft.AspNet.Identity;
using online.Models;
using SmoovPOS.Common;
using SmoovPOS.Entity.Menu;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.Cache;
using SmoovPOS.Utility.WSCollectionReference;
using SmoovPOS.Utility.WSDiscountReference;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Utility.WSMyCartReference;
using SmoovPOS.Utility.WSOnlineReference;
using SmoovPOS.Utility.WSProductItemReference;
using SmoovPOS.Utility.WSSiteReference;
using SmoovPOS.Utility.WSTaxReference;
using SmoovPOS.Utility.WSWebContentReference;
using SmoovPOS.Utility.WSWishListReference;
using SmoovPOS.Utility.WSTableOrderingSettingReference;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SmoovPOS.Utility.WSProductReference;

namespace online.Controllers
{
    public class BaseController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected IEnumerable<Menu> GetAllCategory()
        {
            logger.Info("Call Get All Category");
            ViewBag.siteID = GetSiteIDFromHost();
            WSOnlineClient clientOline = new WSOnlineClient();
            var listCategory = clientOline.GetListNameCategory(ConstantSmoovs.CouchBase.DesignDocCategory, ConstantSmoovs.CouchBase.GetAllCategory, ViewBag.siteID);
            clientOline.Close();

            return listCategory;
        }
        /// <summary>
        /// get all collections have product
        /// </summary>
        /// <returns></returns>
        protected IEnumerable<Menu> GetAllCollection()
        {
            logger.Info("Call Get All Collection");
            ViewBag.siteID = GetSiteIDFromHost();
            WSOnlineClient client = new WSOnlineClient();
            IEnumerable<Menu> listCollection = client.GetListNameCollection(ConstantSmoovs.CouchBase.DesignDocCollection, ConstantSmoovs.CouchBase.CollectionViewAll, ViewBag.siteID);
            client.Close();
            return listCollection;
        }
        protected void SetContentTheme(WebContent webC)
        {
            if (webC != null)
            {
                ViewBag.termAndCondition = webC.termAndCondition;
                ViewBag.aboutUs = webC.aboutUs;
                ViewBag.contactUs = webC.contactUs;
                ViewBag.FAQs = webC.faqs;
                ViewBag.privacyPolicy = webC.privacyPolicy;
                ViewBag.returnAndExchange = webC.returnAndExchange;
                ViewBag.shippingInformation = webC.shippingInformation;
                ViewBag.logo = webC.logo;

                if (webC.slideImage == null)
                {
                    webC.slideImage = new List<SlideImage>() { };
                }
                ViewBag.slideImage = webC.slideImage;

                ViewBag.Social = webC.socialNetworkLink;
                ViewBag.theme = webC.theme;
                ViewBag.titleHomePage = webC.titleHomePage;
                ViewBag.theme = webC.theme;
                ViewBag.businessName = webC.businessName;
                ViewBag.currency = webC.currency;

                if (webC.countries == null)
                {
                    webC.countries = (new List<Country>() { }).ToArray();
                }
                ViewBag.countries = webC.countries;

                ViewBag.siteID = webC.siteID;
            }
        }

        protected void GetThemeInfo(string theme = "", string category = "")
        {
            try
            {
                string siteID = GetSiteIDFromHost();
                if (CacheHelper.Categories == null || !CacheHelper.Categories.Where(c => c != null && c.siteID == siteID).Any())
                {
                    if (CacheHelper.Categories == null)
                    {
                        CacheHelper.Categories = new List<Menu>();
                    }

                    var listC = GetAllCategory();
                    if (listC != null && listC.Count() > 0)
                    {
                        CacheHelper.Categories.AddRange(listC);
                    }
                }

                var listCategory = CacheHelper.Categories.Where(c => c != null && c.siteID == siteID).ToList();
                ViewBag.listCategoryActive = listCategory.OrderBy(c => c.name);

                if (CacheHelper.Collections == null || !CacheHelper.Collections.Where(c => c != null && c.siteID == siteID).Any())
                {
                    if (CacheHelper.Collections == null)
                    {
                        CacheHelper.Collections = new List<Menu>();
                    }

                    var listC = GetAllCollection();
                    if (listC != null && listC.Count() > 0)
                    {
                        CacheHelper.Collections.AddRange(listC);
                    }
                }
                var listCollection = CacheHelper.Collections.Where(c => c != null && c.siteID == siteID).ToList();
                ViewBag.listCollection = listCollection;


                if (CacheHelper.WebContents == null || !CacheHelper.WebContents.Where(r => r != null && r.siteID == siteID).Any())
                {
                    CacheHelper.WebContents = new List<WebContent>();

                    WSWebContentClient clientContent = new WSWebContentClient();
                    IEnumerable<WebContent> listContent = clientContent.WSGetAllWebContent(ConstantSmoovs.CouchBase.DesignDocWebContent, ConstantSmoovs.CouchBase.GetAllWebContent, siteID);
                    clientContent.Close();
                    WebContent web = new WebContent();
                    web = listContent.FirstOrDefault();

                    if (listContent != null && listContent.Count() > 0)
                    {
                        WSInventoryClient clientIventory = new WSInventoryClient();
                        web.countries = clientIventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, siteID);
                        clientIventory.Close();

                        WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
                        web.currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
                        merchantGeneralSettingClient.Close();

                        IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
                        web.businessName = clientMerchant.WSGetMerchantManagementBySiteId(siteID).businessName;
                        clientMerchant.Close();

                        web.siteID = siteID;
                        CacheHelper.WebContents.AddRange(new List<WebContent>() { web });
                    }
                }
                var WebContent = CacheHelper.WebContents.Where(c => c != null && c.siteID == siteID).ToList();
                SetContentTheme(WebContent.FirstOrDefault());

                //Sync mycart data between session and coughbase only one time when member just logged in
                if (Session["isJustLoggedIn"] != null && (bool)Session["isJustLoggedIn"] == true)
                {
                    MyCart myCartInSession = GetMyCartItems(true);
                    myCartInSession.receiptValue = CalculateMyCartReceipt(myCartInSession.listProductItems, 0);

                    Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession;
                    Session["isJustLoggedIn"] = false;
                }
                //Refresh countOfCart number
                ViewBag.countOfCart = GetCountProductInCart();
                ViewBag.countOfWishList = GetCountProductInWishList();

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        #region My Cart
        public MyCart GetMyCartItems(bool isJustLoggedIn = false)
        {
            string siteID = GetSiteID();
            string email = GetEmailMember();

            //Set default myCart items = session cart for the case "customer is guest"
            MyCart returnValue = null;
            if ((Object)Session[ConstantSmoovs.CardOrder.myCart] != null) returnValue = Session[ConstantSmoovs.CardOrder.myCart] as MyCart;
            else Session[ConstantSmoovs.CardOrder.myCart] = returnValue = InitializeNewMyCartObject();

            //Case: customer logged in as member -> set myCart items depending on member's cart in coughbase
            if (Session[ConstantSmoovs.Users.userName] != null && !string.IsNullOrEmpty(Session[ConstantSmoovs.Users.userName].ToString()))
            {
                WSMyCartClient client = new WSMyCartClient();
                //Get member's cart from coughbase
                MyCart myCartFromCoughbase = client.WSGetMyCartByEmail(ConstantSmoovs.CouchBase.DesignDocMyCart, ConstantSmoovs.CouchBase.ViewNameGetDetailMyCartByEmail, siteID, email);

                //Check the doc of member's myCart is created or not
                if (myCartFromCoughbase == null)
                {
                    //Case: member's myCart is not created -> create one

                    //Set email to confirm member
                    returnValue.email = email;
                    //Set modified time
                    returnValue.modifiedDate = DateTime.UtcNow;
                    //Calculate the receipt value
                    returnValue.receiptValue = new ReceiptValue();
                    //
                    myCartFromCoughbase = returnValue;

                    //Create new coughbase myCart
                    client.WSCreateMyCart(myCartFromCoughbase, siteID);
                }
                else
                {
                    //Case: member's myCart is created -> sync data

                    //Sync session myCart with coughbase myCart
                    returnValue = SyncMyCartItems(returnValue, myCartFromCoughbase, isJustLoggedIn);

                    //Update new value into coughbase myCart
                    StoreMyCartIntoCoughbase(siteID, returnValue);
                }
                client.Close();
            }

            return returnValue;
        }

        public MyCart InitializeNewMyCartObject()
        {
            MyCart myCart = new MyCart();
            //Set default value for myCart
            myCart = new MyCart();
            myCart._id = Guid.NewGuid().ToString();
            myCart.email = GetEmailMember();
            myCart.display = true;
            myCart.status = true;
            myCart.remark = string.Empty;
            myCart.listProductItems = new List<ProductItem>();
            myCart.receiptValue = new ReceiptValue();
            myCart.createdDate = DateTime.UtcNow;
            myCart.modifiedDate = DateTime.UtcNow;
            return myCart;
        }

        private MyCart SyncMyCartItems(MyCart myCartInSession, MyCart myCartFromCoughbase, bool isJustLoggedIn)
        {
            //Set return value by member's coughbase cart
            MyCart returnValue = myCartFromCoughbase;

            //Case: session's myCart had items -> update return value = session cart
            if (myCartInSession.listProductItems != null && myCartInSession.listProductItems.Count() > 0)
            {
                //Check member's coughbase cart has value or null
                if (myCartFromCoughbase.listProductItems != null && myCartFromCoughbase.listProductItems.Count() > 0)
                {
                    //Case: member's myCart had items -> mix two carts (session cart and coughbase cart)
                    returnValue.listProductItems = UniteMyCartItemsFromSessionAndCoughbase(myCartInSession.listProductItems, myCartFromCoughbase.listProductItems, isJustLoggedIn);
                }
                else
                {
                    //Case: member's myCart had no items -> set return value = session cart
                    returnValue.listProductItems = myCartInSession.listProductItems;
                }
            }
            //Case: session's myCart had no items -> do nothing because we did set return value by member's coughbase cart before

            //Set the remark
            if (!string.IsNullOrEmpty(myCartInSession.remark) && !string.IsNullOrEmpty(returnValue.remark) && !returnValue.remark.Contains(myCartInSession.remark.Trim()))
                returnValue.remark += " " + myCartInSession.remark.Trim();
            //Set modified time
            returnValue.modifiedDate = DateTime.UtcNow;

            return returnValue;
        }

        //Sum mycart items between session and coughbase right after logging in
        private List<ProductItem> UniteMyCartItemsFromSessionAndCoughbase(List<ProductItem> listMyCartItemsInSession, List<ProductItem> listMyCartItemsFromCoughbase, bool isJustLoggedIn)
        {
            //Set default value by data from coughbase
            List<ProductItem> returnValue = listMyCartItemsFromCoughbase;

            //Reset all variant.isCheckedToCheckOut to be false
            foreach (var item in returnValue)
            {
                foreach (var variant in item.arrayVarient)
                {
                    variant.isCheckedToCheckOut = false;
                }
            }

            foreach (var itemInSession in listMyCartItemsInSession)
            {
                if (returnValue.FindIndex(i => i._id == itemInSession._id) != -1)
                {
                    //Case: itemInSession contains in listMyCartItemsFromCoughbase --> check list variant to update quantity or add new variant
                    foreach (var itemVariantInSession in itemInSession.arrayVarient)
                    {
                        if (returnValue.FirstOrDefault(i => i._id == itemInSession._id).arrayVarient.FindIndex(v => v.sku == itemVariantInSession.sku) != -1)
                        {
                            //Case: variant is existed in returnValue.listProductItem.arrayVariant

                            int quantityOfCoughbaseVariant = returnValue.FirstOrDefault(i => i._id == itemInSession._id)
                                                                        .arrayVarient.FirstOrDefault(v => v.sku == itemVariantInSession.sku).quantity;
                            if (isJustLoggedIn)
                            {
                                returnValue.FirstOrDefault(i => i._id == itemInSession._id).arrayVarient
                                            .FirstOrDefault(v => v.sku == itemVariantInSession.sku).quantity += itemVariantInSession.quantity;
                            }
                            else
                            {
                                returnValue.FirstOrDefault(i => i._id == itemInSession._id).arrayVarient
                                            .FirstOrDefault(v => v.sku == itemVariantInSession.sku).quantity = itemVariantInSession.quantity;
                            }

                            returnValue.FirstOrDefault(i => i._id == itemInSession._id).arrayVarient
                                            .FirstOrDefault(v => v.sku == itemVariantInSession.sku).isCheckedToCheckOut = itemVariantInSession.isCheckedToCheckOut;
                        }
                        else
                        {
                            returnValue.FirstOrDefault(i => i._id == itemInSession._id).arrayVarient.Add(itemVariantInSession);
                        }
                    }
                }
                else
                {
                    //Case: itemInSession doesn't contain in listMyCartItemsFromCoughbase --> add new item into returnValue
                    returnValue.Add(itemInSession);
                }
            }

            return returnValue;
        }

        public void StoreMyCartIntoCoughbase(string siteID, MyCart myCart)
        {
            if (Session[ConstantSmoovs.Users.userName] != null && !string.IsNullOrEmpty(Session[ConstantSmoovs.Users.userName].ToString()))
            {
                //Get member's cart from coughbase
                WSMyCartClient client = new WSMyCartClient();
                MyCart myCartInCoughbase = client.WSGetMyCartByEmail(ConstantSmoovs.CouchBase.DesignDocMyCart, ConstantSmoovs.CouchBase.ViewNameGetDetailMyCartByEmail, siteID, GetEmailMember());

                if (myCart != null && myCartInCoughbase != null)
                {
                    myCartInCoughbase.listProductItems = (myCart.listProductItems != null) ? myCart.listProductItems : new List<ProductItem>();
                    myCartInCoughbase.receiptValue = new ReceiptValue();
                    myCartInCoughbase.remark = myCart.remark.Trim();
                    myCartInCoughbase.modifiedDate = DateTime.UtcNow;

                    //Store new values into coughbase
                    client.WSUpdateMyCart(myCartInCoughbase, siteID);
                }

                client.Close();
            }
        }

        public ReceiptValue CalculateMyCartReceipt(List<ProductItem> listItems, double shippingFree)
        {
            string siteID = GetSiteID();
            ReceiptValue returnValue = new ReceiptValue();

            if (listItems != null && listItems.Count > 0)
            {
                WSTaxClient clientTax = new WSTaxClient();
                var listTax = clientTax.GetTaxWithInventory(ConstantSmoovs.Tax.DesigndocTax, ConstantSmoovs.Tax.ViewNameTax, siteID, 1);// id shop online  = 1
                //  var serviceCharge = clientTax.GetServiceChargeWithInventory(ConstantSmoovs.Tax.DesigndocTax, ConstantSmoovs.Tax.ViewNameTax, siteID, 1);// id shop online  = 1
                clientTax.Close();

                CalculatorReceipt calculatorReceipt = new CalculatorReceipt();
                returnValue = calculatorReceipt.getInformation(listItems, listTax, null, shippingFree);
            }

            return returnValue;
        }

        public ReceiptValue CalculateMyCartReceipt(List<ProductItem> listItems, double shippingFree, int inventoryId, ServiceCharge serviceChargeRef)
        {
            string siteID = GetSiteID();
            ReceiptValue returnValue = new ReceiptValue();

            if (listItems != null && listItems.Count > 0)
            {
                WSTaxClient clientTax = new WSTaxClient();
                var tax = clientTax.GetDetailTaxByInventory(ConstantSmoovs.Tax.DesigndocTax, ConstantSmoovs.Tax.ViewNameTax, siteID, inventoryId);// id shop online  = 1
                clientTax.Close();
                List<TaxItem> listTaxitem = new List<TaxItem>() { };
                CalculatorReceipt calculatorReceipt = new CalculatorReceipt();

            ServiceCharge    serviceCharge = null;

                if (tax != null)
                {
                    if (tax.arrServiceCharge != null && tax.arrServiceCharge.Count > 0)
                    {
                        serviceCharge = tax.arrServiceCharge.FirstOrDefault();
                    }
                    if (tax.arrayTax != null)
                    {
                        listTaxitem = tax.arrayTax;
                    }
                }

                returnValue = calculatorReceipt.getInformation(listItems, listTaxitem.ToArray(), serviceCharge, shippingFree);

                if (serviceCharge != null)
                {
                    serviceChargeRef.serviceChargeValue = returnValue.serviceCharge;
                    serviceChargeRef.serviceChargeRate = returnValue.serviceChargeRate;
                }
            }

            return returnValue;
        }

        public int GetCountProductInCart()
        {
            int count = 0;

            if (Session[ConstantSmoovs.CardOrder.myCart] != null)
            {
                MyCart myCartInSession = new MyCart();
                myCartInSession = Session[ConstantSmoovs.CardOrder.myCart] as MyCart;

                if (myCartInSession != null && myCartInSession.listProductItems != null && myCartInSession.listProductItems.Count > 0)
                {
                    foreach (var product in myCartInSession.listProductItems)
                    {
                        foreach (var item in product.arrayVarient)
                        {
                            count += item.quantity;
                        }
                    }
                }
                else
                {
                    count = 0;
                }
            }
            return count;
        }
        #endregion

        public int GetCountProductInWishList()
        {
            int count = -1;
            if (!string.IsNullOrEmpty(Session[ConstantSmoovs.Users.userName].ToString()))
            {
                count = 0;
                WishList wishlist = new WishList();
                string email = GetEmailMember();
                WSWishListClient clientWishList = new WSWishListClient();
                wishlist = clientWishList.WSGetWishListWithEmail(ConstantSmoovs.CouchBase.DesignDocWishlist, ConstantSmoovs.CouchBase.GetByEmail, email, GetSiteID());
                clientWishList.Close();
                if (wishlist != null)
                {
                    foreach (var product in wishlist.arrayProductItem)
                    {
                        foreach (var item in product.arrayVarient)
                        {
                            count += 1;
                        }
                    }
                }
            }
            return count;
        }

        protected string GetEmailMember()
        {
            if (Session[ConstantSmoovs.Users.userName] != null)
            {
                string email = Session[ConstantSmoovs.Users.userName].ToString();
                if (!string.IsNullOrEmpty(email))
                {
                    return email;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        protected string GetSiteID()
        {
            return GetSiteIDFromHost();
        }

        public string GetSiteIDFromHost()
        {
            string host = Request.Url.Host;
            WSSiteClient client = new WSSiteClient();

            string siteID = string.Empty;

            string subdomain = host;

            if (subdomain.IndexOf("://", StringComparison.Ordinal) != -1)
            {
                subdomain = subdomain.Remove(0, subdomain.IndexOf("://", StringComparison.Ordinal) + 3);
            }
            if (subdomain.IndexOf("\\\\", StringComparison.Ordinal) != -1)
            {
                subdomain = subdomain.Remove(0, subdomain.IndexOf("\\\\", StringComparison.Ordinal) + 2);
            }

            var siteAlias = client.WSGetSiteAliasByHost(subdomain);
            if (siteAlias != null)
            {
                siteID = siteAlias.SiteID;
            }
            else
            {
                var siteAliases = client.WSGetAllSiteAlias();

                //subdomain = subdomain.Split('.')[0];

                foreach (var item in siteAliases)
                {
                    if (item.CheckSubdomain == true)
                    {
                        if (!string.IsNullOrEmpty(item.HTTPAlias) && item.HTTPAlias.Contains(subdomain))
                        {
                            siteID = item.SiteID;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(item.HTTP) && item.HTTP.Contains(subdomain))
                        {
                            siteID = item.SiteID;
                        }
                    }
                }
            }
            client.Close();

            return siteID;
            //return subdomain;
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public string RenderRazorViewToStringNoModel(string viewName)
        {

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public static Customer InitializeNewCustomer(string siteID)
        {
            Customer customer = new Customer();
            customer._id = Guid.NewGuid().ToString();
            customer.firstName = "";
            customer.lastName = "";
            customer.fullName = "";
            customer.email = "";
            customer.image = "";
            customer.memberID = "";
            customer.hiredDate = DateTime.UtcNow.ToString();
            customer.userID = "";
            customer.userOwner = "";
            customer.createdDate = DateTime.UtcNow;
            customer.modifiedDate = DateTime.UtcNow;
            customer.password = "";
            customer.passwordConfirm = "";
            customer.channels = new string[] { "Server" };
            customer.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            customer.display = true;
            customer.status = false;
            customer.active = false;
            customer.tags = null;
            customer.pinCode = Guid.NewGuid().ToString();
            customer.lastLogin = DateTime.UtcNow;
            customer.acceptsNewsletter = false;
            customer.acceptsSms = false;
            customer.gender = 2;
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            try
            {
                customer.timeZoneFullName = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).TimeZoneFullName;
            }
            catch
            {

                customer.timeZoneFullName = "Singapore Standard Time";
            }
            merchantGeneralSettingClient.Close();
            customer.customerType = 0;
            return customer;
        }

        public List<ProductItem> GetAllProductItems(string siteID)
        {
            List<ProductItem> listProductItems = new List<ProductItem>();

            WSDiscountClient discountClient = new WSDiscountClient();
            WSProductItemClient productItemClient = new WSProductItemClient();
            WSProductClient productClient = new WSProductClient();

            var allDiscount = discountClient.WSGetAllDiscount(ConstantSmoovs.CouchBase.DesigndocDiscount, ConstantSmoovs.CouchBase.ViewAllDiscount, siteID)
                .Where(d => d.beginDate <= DateTime.UtcNow && DateTime.UtcNow < d.endDate)
                .OrderBy(d => d.endDate);
            if (allDiscount.Count() > 0)
            {
                foreach (Discount discount in allDiscount)
                {
                    if (discount.ListProductItems != null && discount.ListProductItems.Count > 0)
                    {
                        foreach (string productId in discount.ListProductItems)
                        {
                            Product product = productClient.WSDetailProduct(productId, siteID);
                            
                            ProductItem productItem = productItemClient.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, product._id, siteID).Where(i => i.index == 1).FirstOrDefault();
                            if (productItem != null && productItem.status)
                            {
                                FillDiscountOnProductItem(productItem, discount);
                                listProductItems.Add(productItem);
                            }
                        }
                    }
                }
            }

            productItemClient.Close();
            discountClient.Close();

            return listProductItems;
        }

        public void FillDiscountOnProductItem(ProductItem productItem, Discount discount)
        {
            productItem.discount = true;
            if (productItem.DiscountProductItem == null) productItem.DiscountProductItem = new DiscountProductItem();
            productItem.DiscountProductItem.beginDate = discount.beginDate;
            productItem.DiscountProductItem.endDate = discount.endDate;
            productItem.DiscountProductItem.discountID = discount._id;
            productItem.DiscountProductItem.discountLimit = discount.discountLimit;
            productItem.DiscountProductItem.discountValue = discount.discountValue;
            productItem.DiscountProductItem.name = discount.name;
            productItem.DiscountProductItem.type = discount.type;
        }

        public List<ProductItem> SetDiscountToListProductItems(List<ProductItem> productItemList, string siteID)
        {
            List<ProductItem> productItemListReturn = productItemList;
            // Add discount to products
            List<ProductItem> productItemsDiscount = new List<ProductItem>();
            productItemsDiscount = this.GetAllProductItems(siteID);
            foreach (ProductItem productItemDiscount in productItemsDiscount)
            {
                int productItemIndex = productItemListReturn.IndexOf(productItemListReturn
                    .Where(p => p.productID == productItemDiscount.productID).FirstOrDefault());
                if (productItemIndex > -1)
                {
                    productItemListReturn[productItemIndex] = productItemDiscount;
                }
            }
            return productItemListReturn;
        }

        public ProductItem SetDiscountToProductItem(ProductItem productItem, string siteID)
        {
            ProductItem productItemReturn = productItem;
            // Add discount to products
            List<ProductItem> productItemsDiscount = new List<ProductItem>();
            productItemsDiscount = this.GetAllProductItems(siteID);

            productItemsDiscount = productItemsDiscount.Where(p => p.productID == productItem.productID).ToList();

            if (productItemsDiscount.Count > 0)
            {
                productItemReturn.discount = true;
                productItemReturn.DiscountProductItem = productItemsDiscount
                    .FirstOrDefault().DiscountProductItem;
            }

            return productItemReturn;
        }
        public bool CheckIsAuthentication()
        {
            if (Session[ConstantSmoovs.Users.userName] != null && Session[ConstantSmoovs.Users.firstName] != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public TableOrderingSetting GetTableOrderingSetting(string siteID, Station station)
        {
            TableOrderingSetting _TableOrderingSetting = null;

            WSTableOrderingSettingClient tosclient = new WSTableOrderingSettingClient();

            var settings = tosclient.WSGetAllTableOrderingSetting(ConstantSmoovs.CouchBase.DesigndocTableOrdering, ConstantSmoovs.CouchBase.ViewNameGetAllTableOrdering, siteID);

            if (settings != null && settings.Count() > 0)
            {
                _TableOrderingSetting = settings.Where(s => s.BranchIndex == station.branchIndex).FirstOrDefault();
            }
            return _TableOrderingSetting;
        }

        public bool CheckAvailableTime(string siteID, TableOrderingSetting setting)
        {
            WSInventoryClient invclient = new WSInventoryClient();
            Inventory inv = invclient.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, siteID).Where(i => i.index == setting.BranchIndex).FirstOrDefault();
            invclient.Close();

            DateTime now = Com.SmoovPOS.TimeZone.TimeZoneUtil.ConvertUTCToTimeZone(DateTime.UtcNow, inv.timeZone, siteID);
            int cur_dayofweek = (int)now.DayOfWeek;

            TimeSpan tsNow = new TimeSpan(now.Hour, now.Minute, 0);

            if (setting.ListDayOfWeekSetting[cur_dayofweek].Option != (int)ConstantSmoovs.Enums.TableOrderingOption.Closed)
            {
                var DayOfWeekSetting = setting.ListDayOfWeekSetting[cur_dayofweek];
                if (DayOfWeekSetting.Option == (int)ConstantSmoovs.Enums.TableOrderingOption.Open)
                {
                    if (!string.IsNullOrEmpty(DayOfWeekSetting.Open_Opening) && !string.IsNullOrEmpty(DayOfWeekSetting.Open_Closing))
                    {
                        int setting_open_opening_hour = -1;
                        int setting_open_opening_minute = -1;
                        int setting_open_closing_hour = -1;
                        int setting_open_closing_minute = -1;
                        if (int.TryParse(DayOfWeekSetting.Open_Opening.Split(':')[0], out setting_open_opening_hour)
                            && int.TryParse(DayOfWeekSetting.Open_Opening.Split(':')[1], out setting_open_opening_minute)
                            && int.TryParse(DayOfWeekSetting.Open_Closing.Split(':')[0], out setting_open_closing_hour)
                            && int.TryParse(DayOfWeekSetting.Open_Closing.Split(':')[1], out setting_open_closing_minute))
                        {
                            TimeSpan tsOpen_Opening = new TimeSpan(setting_open_opening_hour, setting_open_opening_minute, 0);
                            TimeSpan tsOpen_Closing = new TimeSpan(setting_open_closing_hour, setting_open_closing_minute, 0);
                            if (tsOpen_Opening <= tsNow && tsNow < tsOpen_Closing) return true;
                            else return false;
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(DayOfWeekSetting.Split_OpeningStart) && !string.IsNullOrEmpty(DayOfWeekSetting.Split_OpeningEnd)
                        && !string.IsNullOrEmpty(DayOfWeekSetting.Split_ClosingStart) && !string.IsNullOrEmpty(DayOfWeekSetting.Split_ClosingEnd))
                    {
                        int setting_split_openingstart_hour = -1;
                        int setting_split_openingstart_minute = -1;
                        int setting_split_openingend_hour = -1;
                        int setting_split_openingend_minute = -1;
                        int setting_split_closingstart_hour = -1;
                        int setting_split_closingstart_minute = -1;
                        int setting_split_closingend_hour = -1;
                        int setting_split_closingend_minute = -1;
                        if (int.TryParse(DayOfWeekSetting.Split_OpeningStart.Split(':')[0], out setting_split_openingstart_hour)
                            && int.TryParse(DayOfWeekSetting.Split_OpeningStart.Split(':')[1], out setting_split_openingstart_minute)
                            && int.TryParse(DayOfWeekSetting.Split_OpeningEnd.Split(':')[0], out setting_split_openingend_hour)
                            && int.TryParse(DayOfWeekSetting.Split_OpeningEnd.Split(':')[1], out setting_split_openingend_minute)
                            && int.TryParse(DayOfWeekSetting.Split_ClosingStart.Split(':')[0], out setting_split_closingstart_hour)
                            && int.TryParse(DayOfWeekSetting.Split_ClosingStart.Split(':')[1], out setting_split_closingstart_minute)
                            && int.TryParse(DayOfWeekSetting.Split_ClosingEnd.Split(':')[0], out setting_split_closingend_hour)
                            && int.TryParse(DayOfWeekSetting.Split_ClosingEnd.Split(':')[1], out setting_split_closingend_minute))
                        {
                            TimeSpan tsSplit_OpeningStart = new TimeSpan(setting_split_openingstart_hour, setting_split_openingstart_minute, 0);
                            TimeSpan tsSplit_OpeningEnd = new TimeSpan(setting_split_openingend_hour, setting_split_openingend_minute, 0);
                            TimeSpan tsSplit_ClosingStart = new TimeSpan(setting_split_closingstart_hour, setting_split_closingstart_minute, 0);
                            TimeSpan tsSplit_ClosingEnd = new TimeSpan(setting_split_closingend_hour, setting_split_closingend_minute, 0);

                            if ((tsSplit_OpeningStart <= tsNow && tsNow < tsSplit_OpeningEnd) || (tsSplit_ClosingStart <= tsNow && tsNow < tsSplit_ClosingEnd)) return true;
                            else return false;
                        }
                    }
                }
            }
            return false;
        }
    }
}