﻿using System.Web;
using System.Web.Optimization;

namespace Com.UI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstraptableorder").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/csstableorder").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/cookie").Include(
                     "~/Scripts/jquery.cookie.js"
                     ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                       "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
           
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                     "~/Scripts/angular.js",
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/jquery-ui.min.js",
                     "~/Scripts/jquery.nicescroll.js",

                     "~/Scripts/jquery.cookie.js",

                     "~/Scripts/bootstrap-tagsinput.js",
                     "~/Scripts/bootstrap-tagsinput-angular.js",
                     "~/Scripts/jquery.maskMoney.js",
                     "~/Scripts/ckeditor/ckeditor.js",
                     "~/Scripts/respond.js",
                     "~/Scripts/switchery.js",
                     "~/Scripts/smoovposLibrary.js",
                     "~/Scripts/jquery.filter_input.js"
                     ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content/bootstrap.css",
                     "~/Content/font-awesome-4.2.0/css/font-awesome.min.css",
                     "~/Content/dashboard.css",
                     "~/Content/bootstrap-tagsinput.css",
                     "~/Content/site.css",
                     "~/Content/Responsive.css",
                     "~/Content/jquery-ui.css",
                     "~/Content/switchery.css"
                     ));

            bundles.Add(new StyleBundle("~/Content/css_theme_1").Include(
                     "~/Content/bootstrap.css",
                      "~/Content/jquery-ui.css",
                     "~/Content/theme_1.css",
                     "~/Content/theme_1_responsive.css",
                     "~/Content/font-awesome-4.2.0/css/font-awesome.min.css"

                     ));
            bundles.Add(new StyleBundle("~/Content/css_theme_2").Include(
                     "~/Content/bootstrap.css",
                     "~/Content/theme_2.css",
                      "~/Content/font-awesome-4.1.0/css/font-awesome.min.css"
                     ));
            bundles.Add(new StyleBundle("~/Content/SignalR").Include(
                     "~/Scripts/jquery.signalR-2.2.0.js"));
        }
    }
}
