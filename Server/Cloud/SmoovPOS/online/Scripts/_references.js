/// <autosync enabled="true" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="bootstrap.js" />
/// <reference path="respond.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="angular.js" />
/// <reference path="angularinit.js" />
/// <reference path="jquery.min.js" />
/// <reference path="jquery-ui.min.js" />
/// <reference path="magnify.js" />
/// <reference path="extend.js" />
/// <reference path="jquery.nicescroll.js" />
/// <reference path="smoovposlibrary.js" />
/// <reference path="bootstrap-datetimepicker.min.js" />
/// <reference path="moment.js" />
/// <reference path="underscore.js" />
/// <reference path="jquery.datatables.js" />
/// <reference path="../content/datatables.bootstrap.js" />
/// <reference path="../content/jquery.datatables.js" />
/// <reference path="jquery.signalr-2.2.0.min.js" />
/// <reference path="jquery.cookie.js" />
/// <reference path="jquery.mobile-1.1.0.min.js" />
/// <reference path="jquery-migrate-1.2.1.min.js" />
