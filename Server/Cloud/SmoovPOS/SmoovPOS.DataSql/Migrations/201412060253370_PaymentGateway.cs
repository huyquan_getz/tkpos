namespace SmoovPOS.DataSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PaymentGateway : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PaymentGateway",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Status = c.Int(nullable: false),
                        SmoovUrl = c.String(),
                        SuccessPage = c.String(),
                        CancelPage = c.String(),
                        StrUrl = c.String(),
                        SandboxSmoovUrl = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PaymentGateway");
        }
    }
}
