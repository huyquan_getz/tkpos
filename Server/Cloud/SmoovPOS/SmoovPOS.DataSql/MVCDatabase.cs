﻿using Com.SmoovPOS.Model;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmoovPOS.DataSql
{
    public class MVCDatabase : DbContext
    {
        public MVCDatabase() : base("DefaultConnection")
        {
            Database.SetInitializer<MVCDatabase>(new CreateDatabaseIfNotExists<MVCDatabase>());
        }
        public DbSet<PaymentModel> Payments { get; set; }
        //public DbSet<MailServerModel> MailServers { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PaymentModel>().ToTable("dbo.PaymentGateway");
            //modelBuilder.Entity<MailServerModel>().ToTable("dbo.MailConfig");
        }
    }
}
