﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Common
{
    public class CalculatorReceipt
    {

        /// <summary>
        /// Gets the information.
        /// </summary>
        /// <param name="listProduct">The list product.</param>
        /// <param name="listTax">The list tax.</param>
        /// <param name="serviceChargeStore">The service charge store.</param>
        /// <returns></returns>
        /// <author>"Quynh.Hoang"</author>
        /// <datetime>1/10/2015-3:36 PM</datetime>
        public ReceiptValue getInformation(List<ProductItem> listProduct, TaxItem[] listTax, ServiceCharge serviceChargeStore, double shippingFree)
        {
            double subtotal = 0;
            double grandTotal = 0;
            double serviceCharge = 0;
            double totalTax = 0;
            double roundAmt = 0;
            double discountTotal = 0;
            ReceiptValue infoReceipt = new ReceiptValue();
            infoReceipt.listTaxOrder = new List<TaxValue>();

            //subTotal before discount
            if (listProduct != null && listProduct.Count() > 0)
            {
                subtotal = listProduct.Sum(r => r.arrayVarient.Where(v => v.isCheckedToCheckOut).Sum(v => v.quantity * v.price));
                foreach (ProductItem item in listProduct)
                {
                    if (item.discount == null) continue;
                    if (item.discount)
                    {
                        if (item.DiscountProductItem.type == 0)
                        {
                            foreach (VarientItem variant in item.arrayVarient)
                            {
                                if (variant.isCheckedToCheckOut)
                                {
                                    double tmp = this.roundPrice(variant.price * variant.quantity * item.DiscountProductItem.discountValue / 100);
                                    discountTotal += tmp;
                                }
                            }
                        }
                        else
                        {
                            foreach (VarientItem variant in item.arrayVarient)
                            {
                                if (variant.isCheckedToCheckOut)
                                {
                                    if (item.DiscountProductItem.discountValue > variant.price)
                                    {
                                        discountTotal += variant.price * variant.quantity;
                                    }
                                    else
                                    {
                                        discountTotal += item.DiscountProductItem.discountValue * variant.quantity;
                                    }

                                }
                            }
                        }
                    }

                }
            }


            if (discountTotal > 0)
            {
                discountTotal = this.roundPrice(discountTotal);
                subtotal = discountTotal > subtotal ? 0.00 : subtotal - discountTotal;
            }

            subtotal = this.roundPrice(subtotal);
            // get list Tax item by storeId
            if (serviceChargeStore != null)
            {
                serviceCharge = (serviceChargeStore.serviceChargeValue * subtotal) / 100;
                serviceCharge = this.roundPrice(serviceCharge);
            }
            double taxPercent = 0;
            foreach (TaxItem item in listTax)
            {
                TaxValue taxValue = new TaxValue();
                taxValue.taxName = item.name;
                taxValue.taxValue = ((subtotal + serviceCharge) * item.valueRate) / 100;
                taxValue.taxValue = this.roundPrice(taxValue.taxValue);
                taxValue.percent = item.valueRate;
                infoReceipt.listTaxOrder.Add(taxValue);
                totalTax += taxValue.taxValue;
                taxPercent += item.valueRate;
            }

            infoReceipt.discountTotal = discountTotal;
            totalTax = this.roundPrice(totalTax);
            infoReceipt.totalTax = totalTax;
            infoReceipt.subTotal = subtotal;
            infoReceipt.shippingFree = this.roundPrice(shippingFree);
            grandTotal = subtotal + totalTax + serviceCharge + shippingFree;
            //taxPercent = (subtotal!=0) ? (totalTax / subtotal) * 100 : 0;
            taxPercent = this.roundPrice(taxPercent);
            infoReceipt.serviceCharge = serviceCharge;
            if (serviceChargeStore != null)
            {
                infoReceipt.serviceChargeRate = serviceChargeStore.serviceChargeRate;
            }
            else
            {
                infoReceipt.serviceChargeRate = -1;
            }
            roundAmt = this.tkRoundCashTotal(grandTotal);
            infoReceipt.rountAmt = roundAmt - grandTotal;
            infoReceipt.grandTotal = roundAmt;
            infoReceipt.taxPercent = taxPercent;

            return infoReceipt;
        }

        public double roundPrice(double value)
        {
            Decimal tmp = (decimal)Decimal.Parse(@String.Format("{0:0.00}", value));
            tmp = Math.Round(tmp, 2);
            return (double)tmp;
        }

        public double tkRoundCashTotal(double amount)
        {
            return Math.Round((amount * 10 * 2)) / (10 * 2);
        }
    }
}