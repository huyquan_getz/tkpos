﻿using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Common.WSMerchantGeneralSettingReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.TimeZone
{
    public class TimeZoneUtil
    {
        public static DateTime ConvertTimeZoneToTimeLocal(string time)
        {

            DateTime convertedDate = DateTime.SpecifyKind(DateTime.Parse(time), DateTimeKind.Utc);
            var kind = convertedDate.Kind; 
            DateTime timeLocal = convertedDate.ToLocalTime(); 

            return timeLocal;
        }

        public static DateTime ConvertGMTToTimeLocal(DateTime time)
        {
            DateTime gmt = time.ToUniversalTime();
            DateTime local = gmt.ToLocalTime();
            return local;
        }

        public static DateTime ConvertUTCToTimeZone(DateTime timeUtc)
        {
            TimeZoneInfo timeInfo = TimeZoneInfo.FindSystemTimeZoneById("Singapore Standard Time");
            DateTime time = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, timeInfo);
            return time;
        }

        public static DateTime ConvertUTCToTimeZoneSource(DateTime timeUtc)
        {
            TimeZoneInfo timeInfo = TimeZoneInfo.FindSystemTimeZoneById("Singapore Standard Time");
            DateTime time = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, timeInfo);
            return time;
        }

        public static DateTime ConvertUTCToTimeZone(DateTime timeUtc, string timeZone, string siteID)
        {
            if (string.IsNullOrEmpty(timeZone))
            {
                timeZone = "Singapore Standard Time";
            }
            //Check timezone is valid or not
            if (ConstantSmoovs.Enums.ListTimeZone.ContainsKey(timeZone) || ConstantSmoovs.Enums.ListTimeZone.ContainsValue(timeZone))
            {
                timeZone = ConstantSmoovs.Enums.ListTimeZone.Where(i => i.Key.Contains(timeZone) || i.Value.Contains(timeZone)).FirstOrDefault().Key;
            }
            else
            {
                //Get saved general settings from couchbase
                WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
                timeZone = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).TimeZoneFullName;
                merchantGeneralSettingClient.Close();
            }

            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            DateTime time = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, timeZoneInfo);
            return time;
        }
    }
}