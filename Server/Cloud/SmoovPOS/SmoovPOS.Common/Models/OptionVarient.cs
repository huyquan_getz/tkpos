﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Common.Models
{
    public class OptionVarient
    {
        public List<string> arrOption { get; set; }
        public int n { get; set; } // number of list
        public int cur { get; set; } // current position
        public int serviced { get; set; } 
    }
}