﻿using Com.SmoovPOS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Common.Controllers
{
    public class UserSession
    {
        /// <summary>
        /// Gets the bucket.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-9:32 AM</datetime>
        public string Bucket
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.bucket];
                if (value != null)
                {
                    return value.ToString();
                }
                return string.Empty;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.bucket] = value;
            }
        }
        public string BusinessName
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.businessName];
                if (value != null)
                {
                    return value.ToString();
                }
                return string.Empty;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.businessName] = value;
            }
        }
        public string Domain
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.domain];
                if (value != null)
                {
                    return value.ToString();
                }
                return string.Empty;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.domain] = value;
            }
        }
        public string SubDomain
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.subdomain];
                if (value != null)
                {
                    return value.ToString();
                }
                return string.Empty;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.subdomain] = value;
            }
        }
        public bool CheckSubDomain
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.checksubdomain];
                if (value != null)
                {
                    return (bool)value;
                }
                return false;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.checksubdomain] = value;
            }
        }
        public string UserId
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.userId];
                if (value != null)
                {
                    return value.ToString();
                }
                return string.Empty;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.userId] = value;
            }
        }

        public string UserName
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.userName];
                if (value != null)
                {
                    return value.ToString();
                }
                return string.Empty;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.userName] = value;
            }
        }

        public string[] Roles
        {
            get
            {
                string[] roles = new string[0] { };
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.roles];
                if (value != null)
                {
                    roles = (string[])value;
                }

                return roles;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.roles] = value;
            }
        }

        public string FirstName
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.firstName];
                if (value != null)
                {
                    return value.ToString();
                }
                return string.Empty;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.firstName] = value;
            }
        }
        public ServicePackageModel ServicePacket
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.servicePackage];
                if (value != null)
                {
                    return (ServicePackageModel)value;
                }
                return null;
            }
            set
            {
                HttpContext.Current.Session[ConstantSmoovs.Users.servicePackage] = value;
            }
        }
        public List<FunctionServicePackageModel> FunctionServicePackets
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.servicePackage];
                if (value != null)
                {
                    var servicePackageModel = (ServicePackageModel)value;
                    if (servicePackageModel != null)
                    {
                        return servicePackageModel.FunctionServicePackets;
                    }
                }
                return null;
            }
        }
        public List<BillingServicePackageModel> BillingServicePackets
        {
            get
            {
                var value = HttpContext.Current.Session[ConstantSmoovs.Users.servicePackage];
                if (value != null)
                {
                    var servicePackageModel = (ServicePackageModel)value;
                    if (servicePackageModel != null)
                    {
                        return servicePackageModel.BillingServicePackets;
                    }
                }
                return null;
            }
        }
        public FunctionServicePackageModel GetFunctionServicePacket(string keyMethod)
        {
            if (FunctionServicePackets != null)
            {
                var functionServicePacket = FunctionServicePackets.FirstOrDefault(f => f.keyMethod == keyMethod);
                if (functionServicePacket != null)
                {
                    return functionServicePacket;
                }
            }

            return null;
        }
        public bool CheckPermissionLimitAddNew(string keyMethod, int countDoc)
        {
            var isPermission = false;
            var functionServicePacket = GetFunctionServicePacket(keyMethod);
            if (functionServicePacket != null)
            {
                if (keyMethod == ConstantSmoovs.AddOns_Key.DATABASE ||
                    keyMethod == ConstantSmoovs.AddOns_Key.MEMBERSHIPFEE ||
                    keyMethod == ConstantSmoovs.AddOns_Key.ONLINESHOP ||
                    keyMethod == ConstantSmoovs.AddOns_Key.POSDEVICE ||
                    keyMethod == ConstantSmoovs.AddOns_Key.QUEUEMANAGEMENT ||
                    keyMethod == ConstantSmoovs.AddOns_Key.TABLEORDERING)
                {
                    isPermission = functionServicePacket.limit >= 1;
                }
                else
                {
                    if (functionServicePacket.isUnlimit)
                    {
                        isPermission = true;
                    }
                    else
                    {
                        isPermission = (functionServicePacket.limit > countDoc);
                    }
                }
            }

            return isPermission;
        }
    }
}