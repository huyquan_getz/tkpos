﻿using MoonAPNS;
using SmoovPOS.Entity.DeviceToken;
using SmoovPOS.Entity.Entity;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Common.Controllers
{
    public static class NotificationsController
    {
        public static List<string> PushNotificationToApple(List<DeviceTokenModel> ListDevices, string message = "Good Morning!")
        {
            List<string> results = new List<string>();
            try
            {
                var notificationList = new List<MoonAPNS.NotificationPayload>();
                //foreach (var item in ListDevices)
                //{
                var payload = new MoonAPNS.NotificationPayload("0937667d46d16575566d498e6b65addf5155d839", message, 1);
                    payload.AddCustom("RegionID", "IDQ10150");
                    notificationList.Add(payload);
                //}

                string certificatePath = HttpContext.Current.Server.MapPath("~/Views/SmoovPOS_Certificates.p12");
                var push = new PushNotification(false, certificatePath, "abcde12345");
                
                string strfilename = push.P12File;
                results = push.SendPushNotifications(notificationList);           
            }
            catch (Exception)
            {

            }
            return results;
        }
    }
}