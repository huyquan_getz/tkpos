﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace SmoovPOS.Common.Controllers
{
    public class VariableConfigController
    {
        public static string limitImage = WebConfigurationManager.AppSettings["limitMaximumImage"];

        /// <summary>
        /// MD5 Hash password
        /// </summary>
        /// <param>string itemToHash</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>23/12/2014-10:07 PM</createddate>
        /// 

        public static string MD5Hash(string itemToHash)
        {
            return string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(itemToHash)).Select(s => s.ToString("x2")));
        }
        /// <summary>
        /// Auto Genarate Password
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>23/12/2014-10:07 PM</createddate>
        /// 

        public static string AutoGenaratePassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }
        public static string GetBucket(){
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            return userSession.Bucket;
        }
        public static string GetUserID()
        {
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            return userSession.UserId;
        }
        public static string GetIDMerchant()
        {
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            return userSession.UserId;
        }

        public static string GetBusinessName()
        {
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            return userSession.BusinessName;
        }
        public static string GetHttpAlias()
        {
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            if (userSession.CheckSubDomain)
            {
                return userSession.SubDomain;
            }
            else
            {
                return userSession.Domain;
            }
           
        }

        public static string GetUserName()
        {
            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            return userSession.UserName;
        }
    }


    
}