﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmoovPOS.Common.Models;
using System.Collections;

namespace SmoovPOS.Common.Controllers
{
    public class VarientGenerateController
    {
        protected OptionVarient initOption(List<string> arr)
        {
            OptionVarient option = new OptionVarient();
            option.arrOption = new List<string>(arr);
            option.n = option.arrOption.Count;
            option.cur = 0;
            option.serviced = 0;
            return option;
        }
        protected string getValue(OptionVarient option)
        {
            string s = option.arrOption[option.cur++];
            if (option.cur == option.n) option.cur = 0;
            if (option.serviced == 0) option.serviced = 1;
            return s;
        }
        protected bool isCircleValue(OptionVarient option)
        {
            return option.cur == 0 && option.serviced == 1;
        }

        protected bool isCircleValueList(OptionVarient[] option, int index)
        {
            int m = option.Length - 1;
            bool isvalue = this.isCircleValue(option[m]);
            for (int i = m - 1; i >= m - index; i--)
                isvalue = isvalue && this.isCircleValue(option[i]);
            return isvalue;
        }

        public ArrayList generate(int n, ArrayList list)
        {
            
            OptionVarient[] option = new OptionVarient[n];
             ArrayList listResult = new ArrayList();
            int m=0;
            foreach (List<string>  listString in list)
            {
                option[m] = this.initOption(listString);
                m++;
            }

            string[] arrStr = new string[n];
            for (int i = 0; i < n; i++)
            {
                 arrStr[i] = this.getValue(option[i]);
            }
                listResult.Add(arrStr.Clone());

            bool stop = this.isCircleValue(option[0]);
            for (int i = 1; i < n; i++)
            {
                stop = stop && this.isCircleValue(option[i]);
            }
        //    bool stop = this.isCircleValue(option1) && this.isCircleValue(option2);
            while (!stop)
            {
                
                for (int i = 0; i < n-1; i++)
                {
                    if (this.isCircleValueList(option, n - 2 - i))
                        arrStr[i] = this.getValue(option[i]);
                }
                    arrStr[n-1] = this.getValue(option[n-1]);
                Console.Out.WriteLine(arrStr[0], arrStr[1]);
                listResult.Add(arrStr.Clone());
                stop = this.isCircleValue(option[0]);
                for (int i = 1; i < n; i++)
                {
                    stop = stop && this.isCircleValue(option[i]);
                }
            }

            return listResult;


        }
        //---------------- End Testing to generate varient algorithm --------------------
	}
}