﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Com.SmoovPOS.Common.UploadAmazonS3.Models;
using System.Drawing.Imaging;
using System.Drawing;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
namespace Com.SmoovPOS.Common.Controllers
{
    public class PhotosController
    {
        //private const string ExistingBucketName = "smoovturnkey"; //Name of the bucket
        private readonly string ExistingBucketName = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.ExistingBucketName];
        private readonly string S3Url = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.S3Url];
        public string UploadToS3Amazon(string filePath)
        {
            string url = "";
            try
            {
                string fileName = Path.GetFileName(filePath);

                var fileTransferUtility = new
                    TransferUtility(new AmazonS3Client(Amazon.RegionEndpoint.USEast1));

                var bucket = VariableConfigController.GetBucket();

                var bucketMerchant = ExistingBucketName;
                if (!string.IsNullOrEmpty(bucket))
                {
                    bucketMerchant = ExistingBucketName + "/" + bucket;
                }
                //// List all objects
                //ListObjectsRequest listRequest = new ListObjectsRequest
                //{
                //    BucketName = ExistingBucketName,
                //    Prefix = bucketMerchant + "/"
                //};

                //ListObjectsResponse listResponse;
                //long totalSize = 0;
                //do
                //{
                //    // Get a list of objects
                //    listResponse = fileTransferUtility.S3Client.ListObjects(listRequest);
                //    foreach (S3Object obj in listResponse.S3Objects)
                //    {
                //        Console.WriteLine("Object - " + obj.Key);
                //        Console.WriteLine(" Size - " + obj.Size);
                //        Console.WriteLine(" LastModified - " + obj.LastModified);
                //        Console.WriteLine(" Storage class - " + obj.StorageClass);
                //        totalSize = totalSize + obj.Size;
                //    }

                //    // Set the marker property
                //    listRequest.Marker = listResponse.NextMarker;
                //} while (listResponse.IsTruncated);

                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = bucketMerchant,
                    FilePath = filePath,
                    StorageClass = S3StorageClass.ReducedRedundancy,
                    PartSize = 6291456, // 6 MB.
                    CannedACL = S3CannedACL.PublicRead,
                    Key = fileName
                };

                fileTransferUtility.Upload(fileTransferUtilityRequest);
                url = S3Url + bucketMerchant + "/" + fileName;
            }
            catch (AmazonS3Exception s3Exception)
            {
                System.Diagnostics.Debug.WriteLine(s3Exception.Message);
                System.Diagnostics.Debug.WriteLine(s3Exception.InnerException);
            }
            return url;
        }

        //// Create a client
        //AmazonS3Client client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1);

        //// List all objects
        //ListObjectsRequest listRequest = new ListObjectsRequest
        //{
        //    BucketName = ExistingBucketName
        //};

        //ListObjectsResponse listResponse;
        //long totalSize = 0;
        //do
        //{
        //    // Get a list of objects
        //    listResponse = fileTransferUtility.S3Client.ListObjects(listRequest);
        //    foreach (S3Object obj in listResponse.S3Objects)
        //    {
        //        Console.WriteLine("Object - " + obj.Key);
        //        Console.WriteLine(" Size - " + obj.Size);
        //        Console.WriteLine(" LastModified - " + obj.LastModified);
        //        Console.WriteLine(" Storage class - " + obj.StorageClass);
        //        totalSize = totalSize + obj.Size;
        //    }

        //    // Set the marker property
        //    listRequest.Marker = listResponse.NextMarker;
        //} while (listResponse.IsTruncated);

        // fileTransferUtilityRequest.Metadata.Add("param1", "Value1");
        // fileTransferUtilityRequest.Metadata.Add("param2", "Value2");

        //         fileTransferUtility.Upload(fileTransferUtilityRequest);
        //       //url = "https://s3-ap-southeast-1.amazonaws.com/" + ExistingBucketName + "/" + fileName;
        //       url = S3Url + bucketMerchant + "/" + fileName;

        /*
        // 2. Specify object key name explicitly.
        fileTransferUtility.Upload(filePath,
                                  ExistingBucketName, KeyName);
        System.Diagnostics.Debug.WriteLine("Upload 2 completed" );

        // 3. Upload data from a type of System.IO.Stream.
        using (var fileToUpload =
            new FileStream(filePath, FileMode.Open, FileAccess.Read))
        {
            fileTransferUtility.Upload(fileToUpload,
                                       ExistingBucketName, KeyName);
        }
        System.Diagnostics.Debug.WriteLine("Upload 3 completed");

        // 4.Specify advanced settings/options.
        var fileTransferUtilityRequest = new TransferUtilityUploadRequest
        {
            BucketName = ExistingBucketName,
            FilePath = filePath,
            StorageClass = S3StorageClass.ReducedRedundancy,
            PartSize = 6291456, // 6 MB.
            Key = KeyName,
            CannedACL = S3CannedACL.PublicRead
        };
        fileTransferUtilityRequest.Metadata.Add("param1", "Value1");
        fileTransferUtilityRequest.Metadata.Add("param2", "Value2");
        fileTransferUtility.Upload(fileTransferUtilityRequest);
        System.Diagnostics.Debug.WriteLine("Upload 4 completed");
         */
        public Size ResizeAndSave(HttpPostedFileBase photo, string path, int width, int height)
        {
            using (Image original = Image.FromStream(photo.InputStream))
            {
                Size sizeImageNew = new Size(original.Width, original.Height);
                if (original.Width > width || original.Height > height)
                {
                    sizeImageNew = CalculateDimensions(new Size(original.Width, original.Height), width, height);
                }

                sizeImageNew = resizeImage(photo, path, original, sizeImageNew);
                //attachCommand.FileStream = thumbnailStream.ToArray();
                return sizeImageNew;
            }
        }
        private static Size resizeImage(HttpPostedFileBase photo, string path, Image original, Size sizeImageNew)
        {
            string extension = Path.GetExtension(photo.FileName);
            var newImage = new Bitmap(original, sizeImageNew.Width, sizeImageNew.Height);
            newImage.Save(path, ImageFormat.Jpeg);
            newImage.Dispose();
            return sizeImageNew;
        }

        /// <summary>
        /// Calculates the dimensions.
        /// </summary>
        /// <param name="currentSize">Size of the current.</param>
        /// <param name="maxWidth">The maximum width.</param>
        /// <param name="maxHeight">The maximum height.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>3/3/2015-5:48 PM</datetime>
        public static Size CalculateDimensions(Size currentSize, double maxWidth, double maxHeight)
        {
            var sourceWidth = (double)currentSize.Width;
            var sourceHeight = (double)currentSize.Height;

            var widthPercent = maxWidth / sourceWidth;
            var heightPercent = maxHeight / sourceHeight;

            var percent = heightPercent < widthPercent
                           ? heightPercent
                           : widthPercent;

            var destWidth = (int)(sourceWidth * percent);
            var destHeight = (int)(sourceHeight * percent);

            return new Size(destWidth, destHeight);
        }
    }
}