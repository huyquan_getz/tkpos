﻿using System;
using System.Web;
namespace SmoovPOS.Common
{
    public partial class ConstantSmoovs
    {
        public const string smoovpos = "smoovpos";
        public static DateTime year1970 = new DateTime(1970, 1, 1);
        public class CardOrder
        {
            public static string myOrder = "myOrder";
            public static string myCart = "myCart";
        }
        public class TableOrder
        {
            public static string tableOrder = "tableOrder";
            public static string tableCart = "tableCart";
            public static string inventory = "inventory";

            public static string TableOrderingSetting = "TableOrderingSetting";
        }
        public class EmailConfiguration
        {
            //Configuration for email
            public static string SMTP_SERVER = "SmtpServer";
            public static string SMTP_PORT = "SmtpPort";
            public static string SMTP_USERNAME = "SmtpUsername";
            public static string SMTP_PASSWORD = "SmtpPassword";
            public static string SMTP_ENABLESSL = "EnableSsl";
        }
        public class Countrys
        {
            public const string Malaysia = "Malaysia";
            public const string Singapore = "Singapore";
        }
        public class Image_Resize
        {
            public const int width_Resize = 263;
            public const int height_Resize = 170;
            public const int width_big_Resize = 460;
            public const int height_big_Resize = 400;
            public const int width_List = 130;
            public const int height_List = 100;
            public const int width_List_S = 80;
            public const int height_List_S = 60;

            public const int width_Resize_Product = 140;
            public const int height_Resize_Product = 120;

            public const int width_WebContentLogo = 250;
            public const int height_WebContentLogo = 150;
            public const int width_WebContentLogo_Big = 250;
            public const int height_WebContentLogo_Big = 150;
        }
        public class Image_Type
        {
            public const string Small = "Small";
            public const string Normal = "Normal";
            public const string Big = "Big";
            public const string WebContentLogo = "WebContentLogo";


        }
        public class API_RESPONSE_STATUS
        {
            public const string EmailNotVerified = "EmailNotVerified";
            public const string MerchantEmailExists = "MerchantEmailExists";
            public const string MerchantInvalidEmail = "MerchantInvalidEmail";
            public const string MerchantWrongCredential = "MerchantWrongCredential";
            public const string AccessTokenNotProvided = "AccessTokenNotProvided";
            public const string AccessTokenExpired = "AccessTokenExpired";
            public const string CommonNotFound = "CommonNotFound";
            public const string CommonInvalidRequest = "CommonInvalidRequest";
            public const string CommonGenericError = "CommonGenericError";
            public const string Success = "Success";
            public const string SuccessNoUpdate = "SuccessNoUpdate";
            public const string Fail = "Fail";
            public const string Timeout = "Time out.Please try again";
            public const string ErrorParams = "Error.Missing Params";
            public const string AccountSmoovPayNotExist = "Account SmoovPay Not Exist";
            public const string InfomationDeviceNotFound = "Infomation Device Not Found";
            public const string ReturnCodeAppUnavailable = "Return Code App Unavailable";
            public const string ReturnCodeDeviceUnavailable = "Return Code Device Unavailable";
            public const string ErrorLimitDevicePOS = "Error Limit Device POS";
        }
        public class API_REFUND_STATUS
        {
            public const string PendingforApproval = "PendingforApproval";
            public const string Refunded = "Refunded";
            public const string Rejected = "Rejected";
            public const string Failed = "Failed";
        }
        public class DeliverySetting
        {
            public const string Basedonorderweight = "Based on order weight";
            public const string Basedonorderprice = "Based on order price";
        }
        public class AppTypes
        {
            public const string table = "table";
            public const string pos = "pos";
        }
        public class AppSettings
        {
            public const string SiteOnline = "SiteOnline";
            public const string ActiveLinkMerchant = "ActiveLinkMerchant";
            public const string SiteOnlineDomainSmoovPOS = "SiteOnlineDomainSmoovPOS";
            public const string CouchBaseServer = "CouchBaseServer";
            public const string UserCouchBaseServer = "UserCouchBaseServer";
            public const string PasswordCouchBaseServer = "PasswordCouchBaseServer";
            public const string SiteMerchantSmoovPOS = "SiteMerchantSmoovPOS";
            public const string UrlSandBox = "UrlSandBox";
            public const string UrlSmoovPay = "UrlSmoovPay";
            public const string PriceTestSandbox = "PriceTestSandbox";
            public const string ApiAccessKey = "ApiAccessKey";
            public const string S3Url = "S3Url";
            public const string ExistingBucketName = "ExistingBucketName";
            public const string bucketSmoovPOS = "bucketSmoovPOS";
            public const string UrlFormPostSmoovPay = "UrlFormPostSmoovPay";
            public const string UrlApiSmoovPay = "UrlApiSmoovPay";
            public const string OccupiedTimeOut = "OccupiedTimeOut";
            public const string SiteApiSmoovPOS = "SiteApiSmoovPOS";
            public const string AppleNotification = "AppleNotification";
            public const string databaseSync = "databaseSync";
        }
        public class Attributes
        {
            public const string ResourceUndefined = "Undefined";
            public const string ResourceRequiredCode = "MSG3";
            public const string ResourceDuplicateMessage = "Existed object with the field";
            public const string ResourceStringLengthDefaultCode = "LengthInvalid";
            public const string LengthInvalidMinimumDefaultCode = "LengthInvalidMinimum";
            public const string LengthInvalidMinAndMaxDefaultCode = "LengthInvalidMaxAndMin";
            public const string ResourceUserLockedOut = "ResourceUserLockedOut";
            public const string ResourceChooseAnotherPassword = "ResourceChooseAnotherPassword";
            public const string ResourceChangePasswordFailed = "ResourceChangePasswordFailed";
            public const string ResourceCompareInvalid = "CompareInvalid";
            public const string ResourceRegularInvalid = "RegularInvalid";
            public const string CreateEmailTemplateFailed = "CreateEmailTemplateFailed";
            public const string InfomationMessageNotification = "InfomationMessageNotification";
        }
        public class DefaultFormats
        {
            public const string MonthText = "dd MMM yyyy";
            public const string LongDate = "d MMM yyyy HH:mm:ss";
            public const string ShortDate = "dd/MM/yyyy";
            public const string JqueryDate = "MM/dd/yyyy";
            public const string bucket = "bucket";
            public const string shadown = "shadown";
            public const string JqueryTime = "HH:mm:ss";
            public const string JqueryShortTime = "HH:mm";
            public const string FullDateName = "dddd, dd MMM yyyy";

        }
        public class RoleNames
        {
            public const string Admin = "Admin";
            public const string Merchant = "Merchant";
            public const string UserMerchant = "UserMerchant";
            public const string Member = "Member";
        }
        public class Users
        {
            public const string domain = "domain";
            public const string subdomain = "subdomain";
            public const string checksubdomain = "checksubdomain";
            public const string businessName = "businessName";
            public const string bucket = "bucket";
            public const string userId = "userId";
            public const string userName = "userName";
            public const string roles = "roles";
            public const string IsSet = "IsSet";
            public const string firstName = "firstName";
            public const string servicePackage = "servicePackage";
        }
        public class MerchantManagement
        {
            public const string fullName = "fullName";
            public const string servicePacketEndDate = "servicePacketEndDate";
            public const string Active = "Active";
            public const string Inactive = "Inactive";
            public const int expires_in = 1;
        }
        public class FunctionMethod
        {
            public const string name = "name";
            public const string status = "status";
            public const string createddate = "createddate";
            public const string noofmerchants = "noofmerchants";
            public const string MB = "MB";
            public const string GB = "GB";

            public const string SelectModule = "Select Module";
            public const string OrderList = "Order List";
            public const string Report = "Report";
            public const string Category = "Category";
            public const string Collection = "Collection";
            public const string Product = "Product";
            public const string Discount = "Discount";
            public const string Branch = "Branch";
            public const string Employee = "Employee";
            public const string Customer = "Customer";
            public const string WebContent = "WebContent";
            public const string MessageBox = "Message Box";
            public const string Marketing = "Marketing";
        }
        public class Order
        {
            public const string DeliveryTable = "DeliveryTable";
            public const string DeliveryTab = "DeliveryTab";
            public const string SelfCollectTable = "SelfCollectTable";
            public const string SelfCollectTab = "SelfCollectTab";
            public const string CancelOrderTable = "CancelOrderTable";
            public const string CancelOrderTab = "CancelOrderTab";
            public const string TableOrderingTable = "TableOrderingTable";
            public const string TableOrderingTab = "TableOrderingTab";
        }

        public class Product
        {
            public const string ProductCount = "ProductCount";

            public const int MaxVariantOption = 3;
            public const string SepVariantOption = "─────────────";
            public const string AddNewVariantOption = "Create a new option";
            public const string DesignDoc = "product";
            public const string ViewAllProductActive = "get_all_by_status_active";
        }

        public class CouchBase
        {
            public const string DesignDocOnline = "online";
            public const string GetAllProductByCategoryWithDate = "get_all_by_date_active";

            public const string DesignDocProductItem = "productItem";
            public const string ProductItemViewNameAllBestSeller = "get_all_best_seller";
            //public const string AllProductItemMaster = "get_all_productItem_master";
            //public const string ViewAllProductItemByDisplayMaster = "get_all_by_master";
            //public const string GetAllProductItem = "get_all";
            public const string GetAllProductItem = "get_all_productItems";
            public const string GetAllByProductID = "get_all_by_productID";
            public const string GetAllByStore = "get_all_by_store";
            public const string GetAllByCategoryMaster = "get_all_by_category_master";
            public const string GetAllByTitleMaster = "get_all_by_title_master";
            public const string GetAllByCategory = "get_all_by_category";

            public const string DesignDocOrder = "order";
            public const string OrderViewNameAll = "get_all_order";
            public const string OrderViewNameAllDelivery = "get_all_order_delivery";
            public const string OrderViewNameAllSelfCollect = "get_all_order_self_collect";
            public const string OrderViewNameAllCancel = "get_all_order_cancel";
            public const string OrderViewNameAllTableOrder = "get_all_table_order";
            public const string OrderViewNameAllByCustomerEmail = "get_by_email_sender";
            public const string GetAllInstruction = "get_all_instruction";
            public const string GetAllServiceShipping = "get_all_service_shipping";

            public const string DesignDocEmployee = "employee";
            public const string EmployeeViewNameAll = "get_all_employee";
            public const string CheckByEmailPassword = "check_by_email_password";
            //public const string GetNextEmployeeID = "get_next_employeeID";


            public const string SettingMerchantViewNameAll = "get_all_setting";
            public const string SettingMerchantViewGeneralSetting = "get_merchant_general_setting";

            public const string DesignDocTemplateEmail = "template_email";
            public const string TemplateEmailViewAll = "get_all_template_email";

            public const string DesignDocCategory = "category";
            public const string GetAllCategory = "get_all_category";

            public const string DesignDocDeveloperSetting = "developer_setting";
            public const string CurrencyViewAll = "get_all_currency";


            public const string DesignDocCollection = "collection";
            public const string CollectionViewAll = "get_all_collection";
            //public const string GetAllCollectionActive = "get_all_collection_active";

            public const string DesignDocDeliveryCountry = "deliverycountry";
            public const string DeliveryCountryViewAll = "get_all_delivery_country";

            public const string DesignDocDeliverySetting = "deliverysetting";
            public const string DeliverySettingViewAll = "get_all_delivery_setting";
            public const string DeliverySettingViewByCountry = "get_all_delivery_setting_by_country";


            public const string DesignDocCustomer = "customer";
            public const string CheckMailExist = "check_email_exist";
            public const string CheckUserPass = "check_username_password";
            public const string CheckWithEmailAndPinCode = "check_with_email_and_pinCode";
            public const string GetAllCustomer = "get_all_customer";
            public const string GetEmailCustomer = "get_email_customer";
            public const string UserDefault = "customer@smoovpos.com";
            public const string PassDefault = "demo@123";

            public const string DesignDocCountry = "country";
            public const string GetAllCountry = "get_all_country";


            public const string DesigndocDiscount = "discount";
            public const string ViewAllDiscount = "get_all_discount";
            public const string ViewDiscountByDisplayActive = "get_all_discount_display_active";
            public const string ViewAllProductItemByDiscount = "get_all_productItem_discount";

            public const string DesigndocInventory = "inventory";
            public const string GetAllInventories = "get_all_inventory";


            public const string DesigndocRole = "role";
            public const string GetAllRole = "get_all_role";

            public const string DesignDocProduct = "product";
            public const string CheckSKUExists = "check_sku_exists";
            public const string GetAllProduct = "get_all_product";
            public const string GetAllByStatusActive = "get_all_by_status_active";

            public const string DesignDocSettingMerchant = "setting_merchant";
            public const string GetAllTax = "get_all_tax";
            public const string GetAllCheckoutSetting = "get_all_checkoutSetting";
            public const string GetAccountSmoovPay = "get_account_smoovpay";
            public const string GetListDevice = "get_list_devices";

            public const string DesignDocWebContent = "webcontent";
            public const string GetAllWebContent = "get_all_webcontent";

            public const string DesignDocWishlist = "wishlist";
            public const string GetByEmail = "get_by_email";

            public const string DesignDocMyCart = "mycart";
            public const string ViewNameGetDetailMyCartByEmail = "get_by_email";

            public const string DesigndocQueue = "queue";
            public const string ViewNameGetAllQueue = "get_all_queue";
            public const string ViewNameGetDetailQueueByBranch = "get_all_queue_by_branch";

            public const string DesignDocStation = "station";
            //public const string ViewNameGetAllStations = "get_all_stations";
            public const string ViewNameGetAllStationsByBranchIndex = "get_all_stations_by_branch_index";

            public const string DesigndocTableOrdering = "tableOrdering";
            public const string ViewNameGetAllTableOrdering = "get_all_table_ordering_setting";
            public const string ViewNameGetAllTableOrderingByBranch = "get_all_table_ordering_setting_by_branch";

            public const string DesigndocSMSSetting = "SMSSetting";
            public const string GetAllSMSSetting = "get_all_sms_setting";

            public const string DesigndocTakeAway = "takeAway";
            public const string ViewNameGetAllTakeAway = "get_all_take_away_setting";
            public const string ViewNameGetAllTakeAwayByBranch = "get_all_take_away_setting_by_branch";
        }

        public class Distribution
        {
            public const string SaveSucess = "Distribution is saved sucessfully!";
            public const string QuantityGreater = "The distributed quantity are greater than current number in master inventory!";
        }
        public class StationTableStatus
        {
            public const string Empty = "Empty";
            public const string Occupied = "Occupied";
            public const string Waiting = "Waiting";
            public const string Confirm = "Confirm";
            public const string Serving = "Serving";
        }
        public class StationTable
        {
            public const string Host = "Host";
            public const string Client = "Guest";
            public const string Me = "Me";
            public const string SubmitTableOrder = "SubmitTableOrder";
            public const string PaymentTableOrder = "PaymentTableOrder";
            public const string ChangedHost = "ChangedHost";
            public const string RemoveItemTableOrder = "RemoveItemTableOrder";

            public const string ConfirmedTableOrder = "ConfirmedTableOrder";
            public const string RejectTableOrder = "RejectTableOrder";
            public const string ServingTableOrder = "ServingTableOrder";
            public const string CancelTableOrder = "CancelTableOrder";
            public const string DoneTableOrder = "DoneTableOrder";
            public const string AddIdToHost = "AddIdToHost";

            public const string memberId = "mobi-";
            public const string ServiceCharge = "Service Charge";
        }
        public class PaymentStatus
        {
            public const string Pending = "Pending";
            public const string Waiting = "Waiting";
            public const string Failed = "Failed";
            public const string Paid = "Paid";
            public const string Refunded = "Refunded";
        }
        public class PaymentStatusIndex
        {
            public const int Pending = 0;
            public const int Waiting = 1;
            public const int Failed = 2;
            public const int Paid = 3;
            public const int Refunded = 4;
        }

        public class TypeMessage
        {
            public const string NewOrder = "NewOrder";
            public const string UpdateOrderStatus = "UpdateOrderStatus";
            public const string UpdateTableOrderStatus = "UpdateTableOrderStatus";
        }
        public class FilterFieldList
        {
            public const string FilterFieldOrderCode = "Order Code";
            public const string FilterFieldTransactionID = "Transaction ID";
            public const string FilterFieldCustomer = "Customer";
            public const string FilterFieldStore = "Store";
        }
        public class Stores
        {
            public const string Master = "Master";
            public const string AllStores = "All Stores";
            public const string OnlineShop = "Online Shop";
            public const int OnlineStore = 1;
            public const string Branch1 = "Branch 1";

            public const string BranchCount = "BranchCount";
        }
        public class OrderStatus
        {
            public const string OnPreRequest = "On Pre-request";
            public const string NewOrder = "New Order";
            public const string ReadyForDelivery = "Ready For Delivery";
            public const string OnDelivery = "On Delivery";
            public const string Delivered = "Delivered";
            public const string ReadyForCollection = "Ready For Collection";
            public const string Collected = "Collected";
            public const string Canceled = "Canceled";
            public const string Received = "Received";
            //---- Show For Customer
            public const string AwaitingOrderConfirm = "Awaiting Order Confirmation";
            public const string OrderConfirm = "Order Confirmed";
            public const string PendingConfirm = "Pending Confirm";
            public const string AbandonedCart = "Abandoned Cart";
            public const string Delivering = "Delivering";
            public const string Refunded = "Refunded";
            //---- Show For Table Ordering
            public const string DineInWaiting = "Waiting";
            public const string DineInConfirm = "Confirmed";
            public const string DineInServing = "Processing";
            public const string DineInDone = "Finished";
            public const string DineInReject = "Rejected";


            public const string CanceledByMerchant = "Canceled (by merchant)";
            public const string CanceledByCustomer = "Canceled (by customer)";
        }
        public class Collection
        {
            public const string AZ = "A-Z";
            public const string ZA = "Z-A";
            public const string HighestLowest = "By price : Highest - Lowest";
            public const string LowestHighest = "By price : Lowest - Highest";
            public const string NewestOldest = "By date : Newest - Oldest";
            public const string OldestNewest = "By date : Oldest -Newest";

            public const string CollectionCount = "CollectionCount";
        }
        public class Category
        {
            public const string CategoryCount = "CategoryCount";
        }
        public class CancelStatus
        {
            public const string NA = "NA";
            public const string Request = "Request";
            public const string Approved = "Approved";
            public const string Rejected = "Rejected";
        }
        public class EmailKeyword
        {

            // use for REPLACE
            public const string FirstNameOfCustomer = "[FirstNameOfCustomer]";
            public const string BusinessName = "[BusinessName]";
            public const string SupportEmailOfMerchant = "[SupportEmailOfMerchant]";
            public const string OrderNumber = "[OrderNumber]";
            public const string OrderDay = "[OrderDay]";
            public const string OrderTime = "[OrderTime]";
            public const string BillTo = "[BillTo]";
            public const string OrderTypeInformation = "[OrderTypeInformation]"; // Collection to: // Ship to:

            public const string OrderStatusOfCustomer = "[OrderStatusOfCustomer]";
            public const string FirstNameLastNameBill = "[FirstNameLastNameBill]";
            public const string FirstNameLastNameReceive = "[FirstNameLastNameReceive]";
            public const string AddressBill = "[AddressBill]";
            public const string AddressReceive = "[AddressReceive]";
            public const string PhoneBill = "[PhoneBill]";
            public const string PhoneReceive = "[PhoneReceive]";
            public const string EmailBill = "[EmailBill]";
            public const string EmailReceive = "[EmailReceive]";

            public const string ProductOrderDetails = "[ProductOrderDetails]";
            public const string OnlineshopLink = "[OnlineshopLink]";

            public const string IDMember = "[IDMember]";
            public const string ActicationCode = "[ActicationCode]";
            public const string OrderCode = "[OrderCode]";
            public const string OrderStatusDisplayForCustomer = "[OrderStatusDisplayForCustomer]";
            public const string LinkResetPasswork = "[LinkResetPasswork]";
            public const string Table = "[Table]";

            // NOT use to replace
            public const string CollectionTo = "Collection Point:";
            public const string ShipTo = "Ship To:";

            // Merchant
            public const string FirstNameOfMerchant = "[FirstNameOfMerchant]";
            public const string LinkActiveMerchant = "[LinkActiveMerchant]";
            public const string ReasonOrder = "[ReasonOrder]";

            // use for CASH DRAWER POS
            public const string CashDrawerReportDay = "[CashDrawerReportDay]";
            public const string StartingCash = "[StartingCash]";
            public const string StartingCashValue = "[StartingCashValue]";
            public const string CashTendered = "[CashTendered]";
            public const string CashTenderedValue = "[CashTenderedValue]";
            public const string CashChange = "[CashChange]";
            public const string CashChangeValue = "[CashChangeValue]";
            public const string CashRefund = "[CashRefund]";
            public const string CashRefundValue = "[CashRefundValue]";
            public const string PaidIn = "[PaidIn]";
            public const string PaidInValue = "[PaidInValue]";
            public const string PaidOut = "[PaidOut]";
            public const string PaidOutValue = "[PaidOutValue]";
            public const string ExpectedInDrawer = "[ExpectedInDrawer]";
            public const string ExpectedInDrawerValue = "[ExpectedInDrawerValue]";
            public const string TotalActual = "[TotalActual]";
            public const string TotalActualValue = "[TotalActualValue]";
            public const string TimeCashDrawer = "[TimeCashDrawer]";
            public const string TableName = "[TableName]";
            public const string OrderAmendDetail = "[OrderAmendDetail]";
        }

        public class TitleEmail
        {
            public const string UpdateOrderStatus = "Update Order status - #";
            public const string ConfirmSelfCollectOrder = "Confirm Order - #";
            public const string ConfirmDeliveryOrder = "Confirm Order - #";
            public const string ForgotPassword = "Forgot password";
            public const string RegisterComplete = "Welcome to ";
            public const string VerifyAccountEmail = "Verify your email address ";
            public const string RegisterSuccessfully = "Welcome to ";
            public const string ResetPassword = "Forgot Password";
            public const string ChangePassword = "Change Password";
            public const string VerifyAccount = "Verify your account ";
            public const string ResetPassword1 = "Reset Password";
            public const string WelcomeToSmoovPos = "Welcome to SmoovPOS";
            public const string CancelOrderNo = "Cancel Order No. ";
            public const string RefundOrderNo = "Refund Order No. ";
            public const string RefundOrder = "Refund Order ";
            public const string RejectCancelOrderNo = "Rejected - Cancel Order No. ";

            public const string ReceiptOrder = "Receipt Order - #";
            public const string ReceiptResendOrder = "Resend Receipt Order - #";
            public const string ReceiptCancelResendOrder = "Resend Receipt Order - #";
            public const string ReceiptRefundResendOrder = "Resend Receipt Order - #";
            public const string CashDrawerReport = "Cash Drawer Report";
            // Table ordering
            public const string OrderConfirmation = "Order Confirmation";
            public const string OrderReject = "Order Reject";
            public const string OrderCancel = "Order Canceled";
            public const string OrderReady = "Order Ready";
            public const string OrderAmend = "Order Amend";
        }
        public const string SmoovPosTeam = "SmoovPos Team";
        public class EmailSupport
        {
            public const string EmailSupportMerchant = "smoovpos.demo@gmail.com";
        }
        public class Customer
        {

            //  public const string siteIDDefault = "smoovpos";
        }

        public const string DesignDocTemplateEmail = "template_email";
        public const string TemplateEmailViewAll = "get_all_template_email";
        public class BodyEmail
        {
            //public const string MemberVerifyAccountEmail = "Member verify account email";
            //public const string MemberRegisterComplete = "Member Register complete";
            //public const string MemberForgotPassword = "Member - forgot password";
            //public const string CustomerConfirmDeliveryOrder = "Customer - confirm delivery order";
            //public const string CustomerSelfCollectOrder = "Customer - confirm selft collect order";
            //public const string UpdateOrderStatus = "Customer - Update order status";

            public const int MemberVerifyAccountEmail = 0;
            public const int MemberRegisterComplete = 1;
            public const int MemberForgotPassword = 2;
            public const int CustomerConfirmDeliveryOrder = 3;
            public const int CustomerSelfCollectOrder = 4;
            public const int UpdateOrderStatus = 5;
            public const int MerchantVerifyAccountEmail = 6;
            public const int MerchantRegisterComplete = 7;
            public const int MerchantResetPassword = 8;
            public const int CustomerCancelOrder = 9;
            public const int CustomerRefundOrder = 10;
            public const int CustomerRejectOrder = 11;
        }

        public class Tax
        {
            public const string DesigndocTax = "setting_merchant";
            public const string ViewNameTax = "get_all_tax";

        }
        public class Discount
        {
            public const string DiscountCount = "DiscountCount";
        }
        public class Employee
        {
            public const string EmployeeCount = "EmployeeCount";
        }

        public class OS
        {
            public const string IOS = "IOS";
            public const string Android = "Android";
        }
        public class Titles
        {
            public const string SelectTitle = "Select Title";
            public const string Dr = "Dr";
            public const string Mr = "Mr";
            public const string Mrs = "Mrs";
            public const string Ms = "Ms";
        }


        public class OrderType
        {
            public const int OrderTypeDelivery = 0;
            public const int OrderTypeSelfCollect = 1;
            public const int OrderTypeCancel = 3;
            public const int OrderTypeTableOrdering = 2;
        }
        public class OrderSort
        {
            public const string orderCode = "orderCode";
            public const string completedDate = "completedDate";
            public const string CustomerDisplay = "CustomerDisplay";
            public const string grandTotal = "grandTotal";
        }

        public class CollectionSort
        {
            public const string byCollection = "collection";
            public const string byItems = "items";
        }

        public class CheckoutSetting
        {

        }
        public class ColorPaymentStatus
        {
            public const string LightOrange = "#EFAD4C";
            public const string Blue = "#438ACA";
            public const string Red = "#D9524E";
        }
        public class ColorOrderStatus
        {
            public const string Red = "#D9524E";
            public const string LightGreen = "#5DB85B";

            public const string LightGrey = "#BBBBBB";
            public const string DarkGrey = "#777777";
            public const string LightYellow = "#F8D6A8";
            public const string Orange = "#EFAD4C";
            public const string DarkOrange = "#837663";
            public const string GreenLight = "#6FAEC0";
        }

        public class ConfigMail
        {
            public const int OfficeSite = 0;
            public const int Admin = 1;
            public const int Merchant = 2;
        }

        public class Sale
        {
            public const string DailyReport = "DailyReport";
            public const string WeeklyReport = "WeeklyReport";
            public const string MonthlyReport = "MonthlyReport";
        }
        public class BillingCycle
        {
            public const string Empty = "--";
            public const string SGD = "SGD";
            public const string Yearly = "Yearly";
            public const string Quarterly = "Quarterly";
            public const string Monthly = "Monthly";
        }
        public class AddOns_Key
        {
            public const string DATABASE = "DATABASE";
            public const string POSDEVICE = "POSDEVICE";
            public const string ONLINESHOP = "ONLINESHOP";
            public const string MEMBERSHIPFEE = "MEMBERSHIPFEE";
            public const string QUEUEMANAGEMENT = "QUEUEMANAGEMENT";
            public const string TABLEORDERING = "TABLEORDERING";
            public const string MAINPOSAPP = "MAINPOSAPP";
        }
        public class AddOns_name
        {
            public const string Database = "Database on S3";
            public const string POSDevice = "POS Device";
            public const string OnlineShop = "Online Shop";
            public const string MembershipFee = "Membership Fee";
            public const string QueueManagement = "Queue Management";
            public const string TableOrdering = "Table Ordering";
            public const string MainPOSApp = "Main POS App";
        }

        public class ResponseStatusApiSmoovPay
        {
            public const string Token = "token";
            public const int Succcess = 1;
            public const int SuccessNoUpdate = 2;
            public const int CommonGenericError = -1000;
            public const int CommonInvalidRequest = -1001;
            public const int CommonNotFound = -1002;
            public const int AccessTokenExpired = -1050;
            public const int AccessTokenNotProvided = -1051;
            public const int MerchantWrongCredential = -1100;
            public const int MerchantInvalidEmail = -1101;
            public const int MerchantEmailExists = -1102;
            public const int EmailNotVerified = -1103;
            public const int Timeout = -1300;
            public const int ErrorParams = -1301;
            public const int AccountSmoovPayNotExist = -1302;
            public const int ErrorTryCatch = -1303;
            public const int Failed = 0;
        }


        public class URLSmoovPayServer
        {

            public const string ExistingMerchant = "{0}/General/ExistingMerchant";
            public const string SignupMerchant = "{0}/General/SignupMerchant";
            public const string OneTimeToken2 = "{0}/Utility/OneTimeToken2";
            public const string RequestAuthorization = "{0}/Merchant/RequestAuthorization";
            public const string RequestRefund = "{0}/Refund/RequestRefund";
        }

        public class MailTypeOrdering
        {

            public const string OrderingConfirm = "OrderingConfirm";
            public const string OrderingReject = "OrderingReject";
            public const string OrderingCancel = "OrderingCancel";
            public const string OrderingReady = "OrderingReady";
            public const string OrderingAmend = "OrderingAmend";

        }

        public class OrderStatusValueInt
        {
            public const int OnPreRequest = 0;
            public const int NewOrder = 1;
            public const int ReadyForDelivery = 2;
            public const int OnDelivery = 3;
            public const int Delivered = 4;
            public const int ReadyForCollection = 5;
            public const int Received = 6;
            public const int Canceled = 7;
            public const int Collected = 8;
            public const int Refunded = 9;
            public const int AbandonedCart = 10;
        }
    }
}