﻿using System.Collections.Generic;

namespace SmoovPOS.Common
{
    public partial class ConstantSmoovs
    {
        public class Enums
        {
            public enum FormMode
            {
                Add,
                Edit,
                View
            }
            public enum CustomerType
            {
                Member = 0,
                Guest = 1
            }
            public enum OrderType
            {
                Delivery = 0,
                SelfCollection = 1,
                TableOrdering = 2
            }

            public enum TableOrderingOption
            {
                Open = 0,
                Split = 1,
                Closed = 2
            }

            public enum TableOrderingStatus
            {
                DineInConfirm = 12,
                DineInServing = 13,
                DineInDone = 14
            }

            public static Dictionary<int, string> Criterias = new Dictionary<int, string>(){
                { 0, ConstantSmoovs.DeliverySetting.Basedonorderweight },
                { 1, ConstantSmoovs.DeliverySetting.Basedonorderprice }
            };
            public enum Unit
            {
                Kg = 0
            }
            public static Dictionary<int, string> PaymentStatus = new Dictionary<int, string>(){
                { 0, ConstantSmoovs.PaymentStatus.Pending },
                { 1, ConstantSmoovs.PaymentStatus.Waiting },
                { 2, ConstantSmoovs.PaymentStatus.Failed },
                { 3, ConstantSmoovs.PaymentStatus.Paid },
                { 4, ConstantSmoovs.PaymentStatus.Refunded }
            };
            public static Dictionary<int, string> StationTableStatus = new Dictionary<int, string>(){
                { 0, ConstantSmoovs.StationTableStatus.Waiting },
                { 1, ConstantSmoovs.StationTableStatus.Confirm },
                { 2, ConstantSmoovs.StationTableStatus.Serving },
                { 3, ConstantSmoovs.StationTableStatus.Occupied },
                { 4, ConstantSmoovs.StationTableStatus.Empty }                
            };
            public static Dictionary<int, string> OrderStatus = new Dictionary<int, string>(){
                { 0, ConstantSmoovs.OrderStatus.OnPreRequest },
                { 1, ConstantSmoovs.OrderStatus.NewOrder },
                { 2, ConstantSmoovs.OrderStatus.ReadyForDelivery },
                { 3, ConstantSmoovs.OrderStatus.OnDelivery },
                { 4, ConstantSmoovs.OrderStatus.Delivered },
                { 5, ConstantSmoovs.OrderStatus.ReadyForCollection },
                { 6, ConstantSmoovs.OrderStatus.Received },
                { 7, ConstantSmoovs.OrderStatus.Canceled },
                { 8, ConstantSmoovs.OrderStatus.Collected },
                { 9, ConstantSmoovs.OrderStatus.Refunded },
                {10, ConstantSmoovs.OrderStatus.AbandonedCart },
                {11, ConstantSmoovs.OrderStatus.DineInWaiting },
                {12, ConstantSmoovs.OrderStatus.DineInConfirm },
                {13, ConstantSmoovs.OrderStatus.DineInServing },
                {14, ConstantSmoovs.OrderStatus.DineInDone }
            };

            public static Dictionary<int, string> OrderStatusCustomer = new Dictionary<int, string>(){
                { 0, ConstantSmoovs.OrderStatus.OnPreRequest },
                { 1, ConstantSmoovs.OrderStatus.AwaitingOrderConfirm },
                { 2, ConstantSmoovs.OrderStatus.OrderConfirm },
                { 3, ConstantSmoovs.OrderStatus.Delivering },
                { 4, ConstantSmoovs.OrderStatus.Delivered },
                { 5, ConstantSmoovs.OrderStatus.ReadyForCollection },
                { 6, ConstantSmoovs.OrderStatus.Received },
                { 7, ConstantSmoovs.OrderStatus.Canceled },
                { 8, ConstantSmoovs.OrderStatus.Received },
                { 9, ConstantSmoovs.OrderStatus.Refunded },
                {10, ConstantSmoovs.OrderStatus.AbandonedCart }
            };

            public static Dictionary<int, string> API_RESPONSE_STATUS = new Dictionary<int, string>(){
                { -1103, ConstantSmoovs.API_RESPONSE_STATUS.EmailNotVerified },
                { -1102, ConstantSmoovs.API_RESPONSE_STATUS.MerchantEmailExists  },
                { -1101, ConstantSmoovs.API_RESPONSE_STATUS.MerchantInvalidEmail  },
                { -1100, ConstantSmoovs.API_RESPONSE_STATUS.MerchantWrongCredential  },
                { -1051, ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenNotProvided  },
                { -1050, ConstantSmoovs.API_RESPONSE_STATUS.AccessTokenExpired  },
                { -1002, ConstantSmoovs.API_RESPONSE_STATUS.CommonNotFound  },
                { -1001, ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest  },
                { -1000, ConstantSmoovs.API_RESPONSE_STATUS.CommonGenericError  },
                { 1, ConstantSmoovs.API_RESPONSE_STATUS.Success  },
                { 2, ConstantSmoovs.API_RESPONSE_STATUS.SuccessNoUpdate  },
                { 0, ConstantSmoovs.API_RESPONSE_STATUS.Fail  },
                { -1500, ConstantSmoovs.API_RESPONSE_STATUS.InfomationDeviceNotFound  },
                { -2200, ConstantSmoovs.API_RESPONSE_STATUS.ReturnCodeAppUnavailable  },
                { -3300, ConstantSmoovs.API_RESPONSE_STATUS.ReturnCodeDeviceUnavailable  },
                { -1501, ConstantSmoovs.API_RESPONSE_STATUS.ErrorLimitDevicePOS  }
            };
            public static Dictionary<int, string> API_REFUND_STATUS = new Dictionary<int, string>(){
                { 11, ConstantSmoovs.API_REFUND_STATUS.PendingforApproval },
                { 9, ConstantSmoovs.API_REFUND_STATUS.Refunded },
                { 6, ConstantSmoovs.API_REFUND_STATUS.Rejected },
                { 4, ConstantSmoovs.API_REFUND_STATUS.Failed }
            };

            public static Dictionary<int, string> TypeMessage = new Dictionary<int, string>(){
                { 1, ConstantSmoovs.TypeMessage.NewOrder },
                { 2, ConstantSmoovs.TypeMessage.UpdateOrderStatus },
                { 3, ConstantSmoovs.TypeMessage.UpdateTableOrderStatus }
            };

            public static Dictionary<string, string> ListTimeZone = new Dictionary<string, string>(){
                { "Dateline Standard Time", "(UTC-12:00)" },
                { "Samoa Standard Time","(UTC-11:00)"},
                { "Hawaiian Standard Time","(UTC-10:00)"},
                { "Alaskan Standard Time","(UTC-9:00)"},
                { "Pacific Standard Time","(UTC-8:00)"},
                { "Mountain Standard Time","(UTC-7:00)"},
                { "Central Standard Time","(UTC-6:00)"},
                {"Eastern Standard Time","(UTC-5:00)"},
                {"Atlantic Standard Time","(UTC-4:00)"},
                {"SA Eastern Standard Time","(UTC-3:00)"},
                {"Mid-Atlantic Standard Time","(UTC-2:00)"},
                {"Azores Standard Time","(UTC-1:00)"},
                {"GMT Standard Time","(UTC)"},
                {"Romance Standard Time","(UTC+1:00)"},
                {"Jordan Standard Time","(UTC+2:00)"},
                {"Arabic Standard Time","(UTC+3:00)"},
                {"Arabian Standard Time","(UTC+4:00)"},
                {"West Asia Standard Time","(UTC+5:00)"},
                {"Central Asia Standard Time","(UTC+6:00)"},
                {"SE Asia Standard Time","(UTC+7:00)"},
                {"Singapore Standard Time","(UTC+8:00)"},
                {"Korea Standard Time","(UTC+9:00)"},
                {"Tasmania Standard Time","(UTC+10:00)"},
                {"Central Pacific Standard Time","(UTC+11:00)"},
                {"New Zealand Standard Time","(UTC+12:00)"},
                {"Tonga Standard Time","(UTC+13:00)"}
            };
            public static Dictionary<string, int> ListSMPT = new Dictionary<string, int>()
            {
                 { "smtp.1and1.com", 25  },
                 { "mail.adelphia.net", 25 },
                 { "smtp.aol.com", 25  },
                 { "smtp.att.yahoo.com", 465  },
                 { "mail.attbi.com.net", 25 },
                 { "mail.attbi.com", 25  },
                 { "mailhost.worldnet.att.net", 465  },
                 { "gtei.bellatlantic.net", 25  },
                 { "smtp.mybluelight.com", 465  },
                 { "smtp.bsnl.in", 25 },
                 { "mail.btinternet.com", 465  },
                 { "smtp.clara.net.net", 465 },
                 { "smtp.comcast.net", 465 },
                 { "smtpauth.earthlink.net", 578 },
                 { "smtp.frontier.com", 465 },
                 { "smtp.gmail.com", 25  },
                 { "smtp.live.com", 25 },
                 { "smtp.me.com", 25 },
                 { "smtp.mail.com", 465 },                          
                 { "smtp.o2.co.uk", 465  },                
                 { "smtp.rcn.com", 25 },     
                 { "smtp.tools.sky.com", 465 },
                 { "smtp-server.rr.com", 25 },
                 { "smtp.suddenlink.net", 25 },
                 { "smtp1.sympatico.ca", 25 },
                 { "mail.talktalk.net", 25 },     
                 { "smtp.tiscali.co.uk", 25 },
                 { "smtp.windstream.net", 25 },
                 { "smtp.mail.yahoo.co.uk", 465 },
                 { "smtp.mail.yahoo.com", 465 },
                 { "outgoing.verizon.net", 587 },
                 { "smtp.virginmedia.com", 25 },
                 { "email-smtp.us-east-1.amazonaws.com", 465 }
            };

            public static Dictionary<string, int> ListPOP = new Dictionary<string, int>()
            {
                 { "pop.1and1.com", 110  },
                 { "mail.adelphia.net", 995 },
                 { "pop.aol.com", 995  },
                 { "pop.att.yahoo.com", 995  },
                 { "mail.attbi.com.net", 995 },
                 { "mail.attbi.com", 995  },
                 { "postoffice.worldnet.att.net", 995  },
                 { "pop.bellatlantic.net", 995  },
                 { "pop.mybluelight.com", 995  },
                 { "mail.bsnl.in", 995 },
                 { "mail.btinternet.com", 995  },
                 { "pop.clara.net.net", 995 },
                 { "mail.comcast.net", 995 },
                 { "pop.earthlink.net", 995 },
                 { "pop.frontier.com", 995 },
                 { "pop.gmail.com", 995  },
                 { "pop3.live.com", 995 },
                 { "mail.me.com", 110 },
                 { "pop.mail.com", 995 },                              
                 { "mail.o2.co.uk", 995  },                 
                 { "pop.rcn.com", 995 },
                 { "pop-server.rr.com", 995 },               
                 { "pop.tools.sky.com", 995 },     
                 { "pop.suddenlink.net", 110 },
                 { "pop1.sympatico.ca", 995 },
                 { "mail.talktalk.net", 995 },              
                 { "pop.tiscali.co.uk", 995 },
                 { "pop.windstream.net", 995 },
                 { "pop.mail.yahoo.co.uk", 995 },
                 { "pop.mail.yahoo.com", 995 },
                 { "incoming.verizon.net", 995 },
                 { "pop3.virginmedia.com", 995 },
                 { "email-pop.us-east-1.amazonaws.com", 995 }
            };

            public static Dictionary<string, string> RefundReason = new Dictionary<string, string>(){
                { "CANCEL_ORDER", "Cancel Order" },
                { "WRONG_ORDER","Wrong Order"},
                { "PRODUCT_FAULTY","Product Faulty"}
              };

        }

    }
}