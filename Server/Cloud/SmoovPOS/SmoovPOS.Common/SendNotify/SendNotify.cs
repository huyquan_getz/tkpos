﻿using System.IO;
using PushSharp;
using PushSharp.Apple;
using System.Web;
using System.Configuration;
using SmoovPOS.Entity.DeviceToken;
using SmoovPOS.Common.WSCheckoutSettingReference;
using System.Collections.Generic;
using System;
using SmoovPOS.Entity.Models;

namespace SmoovPOS.Common.SendNotify
{
    public class SendNotify
    {
        static string FileP12Name = System.Configuration.ConfigurationManager.AppSettings["FileP12Name"].ToString();
        static string FileP12Password = System.Configuration.ConfigurationManager.AppSettings["FileP12Password"].ToString();
        static string AppleDeviceSoundNotify = System.Configuration.ConfigurationManager.AppSettings["AppleDeviceSoundNotify"].ToString();

        private static object locker = new object();
        public static bool SendNotifyToApple(ManageDevice model, string message, int branchIndex, log4net.ILog logger)
        {
            lock (locker)
            {
                try
                {
                    var push = new PushBroker();

                    //Wire up the events for all the services that the broker registers
                    //push.OnNotificationSent += NotificationSent;
                    //push.OnChannelException += ChannelException;
                    //push.OnServiceException += ServiceException;
                    //push.OnNotificationFailed += NotificationFailed;
                    //push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
                    //push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
                    //push.OnChannelCreated += ChannelCreated;
                    //push.OnChannelDestroyed += ChannelDestroyed;
                    //-------------------------
                    // APPLE NOTIFICATIONS
                    //-------------------------
                    //Configure and start Apple APNS
                    // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to generate one for connecting to Sandbox,
                    //   and one for connecting to Production.  You must use the right one, to match the provisioning profile you build your
                    //   app with!
                    string filePath = HttpContext.Current.Server.MapPath("~/SendNotify/ConfigFile/") + FileP12Name;
                    var appleCert = File.ReadAllBytes(filePath);
                    //IMPORTANT: If you are using a Development provisioning Profile, you must use the Sandbox push notification server 
                    //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as 'false')
                    //  If you are using an AdHoc or AppStore provisioning profile, you must use the Production push notification server
                    //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 'true')
                    push.RegisterAppleService(new ApplePushChannelSettings(false, appleCert, FileP12Password)); //Extension method
                    //Fluent construction of an iOS notification
                    //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets generated within your iOS app itself when the Application Delegate
                    //  for registered for remote notifications is called, and the device token is passed back to you
                    foreach (var item in model.ArrayDevices)
                    {
                        logger.InfoFormat("Push Notification bucket: {0}; branchIndex: {1}; DeviceToken: {2}; badge {3}", model.bucket, branchIndex, item.token, item.badge);

                        push.QueueNotification(new AppleNotification()
                                               .ForDeviceToken(item.token)
                                               .WithAlert(message)
                                               .WithBadge(item.badge)
                                               .WithSound(AppleDeviceSoundNotify));
                    }
                    push.StopAllServices(waitForQueuesToFinish: true); 

                    return true;
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return false;
                }
            }
        }

    }
}