﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.SmoovPOS.Entity;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSCategoryReference;
using SmoovPOS.Utility.WSProductReference;

namespace Com.SmoovPOS.UnitTest.Controllers
{
    [TestClass]
    class ProductControllerTest
    {
        [TestMethod]
        public void Index()
        {

        }

        [TestMethod]
        public void CreateProduct()
        {
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            product._id = Guid.NewGuid().ToString();
           
            product.userID = "001";
            product.userOwner = "Merchant_001";

            product.createdDate = DateTime.Today;
            product.modifiedDate = DateTime.Today;
            product.description ="description";
            product.name = "HTC TEST";
            product.nameShort = "HTC";
            product.status = false;

            string Category = "03c59955-130a-4ac3-a54a-bd591399dafe~category_69";
            int lenghtOfName = Category.Length - 37;
            product.categoryId = Category.Substring(0, 36);
            product.categoryName = Category.Substring(37, lenghtOfName);

            product.productType = "";
            product.vendor = "";
            //============= Chu y sua lai
            //  product.image = "";

              ImageItem image = new ImageItem();
            image.imageDetault = "0";
            List<string> listImage = new List<string>();
            listImage.Add("http://icons.iconarchive.com/icons/designcontest/ecommerce-business/128/mobile-icon.png");
            listImage.Add("http://icons.iconarchive.com/icons/designcontest/ecommerce-business/128/mobile-icon.png");
            image.arrayImage = new List<string>();
            image.arrayImage = listImage;
            product.image = new ImageItem();
            product.image = image;
            //=============
            product.price = 10.00;

            product.priceCompare = 12.00;
            product.priceCost = 9.00;

            product.sku = "htc";

            product.requireShipping = false;
            product.productMultiple = false;
            product.weight = 0.00;


            product.inventoryPolicy ="";




            // Require of SMOOV
            product.title = "HTC TEST";
            product.excerpt = "";
            product.editorNote = "";
            product.sizeNFit = "";
            product.measurement = "";
            product.unit = "";
            product.limit = 0;
            product.trackStock = "2";
            product.remindStock = 0;

            product.minimumStock = "";
            product.masterInventory = 20;
            product.display = true;
            //================== DU LIEU GIA
            InventoryItem inventory = new InventoryItem();
            inventory.name = "Size";
            List<string> listStr = new List<string>();
            listStr.Add("M");
            listStr.Add("XL");
            inventory.options = listStr;
            product.arrayInventory = new List<InventoryItem>();
            product.arrayInventory.Add(inventory);

            product.arrayVarient = new List<VarientItem>();
            VarientItem varient = new VarientItem();
            varient.sku = product.sku;

            varient.title = product.name;
            varient.color = "Red";
            varient.size = "M";
            varient.quantity = 10;
            //varient.image = "";
            varient.image = new ImageItem();
            varient.image = product.image;
            varient.price = 2;
            varient.priceCost = 1;
            varient.weight = 0;
            varient.requireShipping = product.requireShipping;
            varient.inventory_policy = "0";
            varient.masterInventory = 10;
            varient.remindStock = product.remindStock;
            varient.productID = product._id;
            varient._id = "001";


            varient.createdDate = product.createdDate;
            varient.currency = "$";
            varient.modifiedDate = product.modifiedDate;
            varient.userID = product.userID;
            varient.userOwner = product.userOwner;

            product.arrayVarient.Add(varient);
            VarientItem varient2 = new VarientItem();
            varient2.sku = product.sku;

            varient2.title = product.name;
            varient2.color = "Red";
            varient2.size = "M";
            varient2.quantity = 10;
            //varient2.image = "";
            varient2.image = new ImageItem();
            varient2.image = product.image;
            varient2.price = 2;
            varient2.priceCost = 1;
            varient2.weight = 0;
            varient2.requireShipping = product.requireShipping;
            varient2.inventory_policy = "0";
            varient2.masterInventory = 10;
            varient2.remindStock = product.remindStock;
            varient2.productID = product._id;
            varient2._id = "002";
            varient2.createdDate = product.createdDate;
            varient2.currency = "$";
            varient2.modifiedDate = product.modifiedDate;
            varient2.userID = product.userID;
            varient2.userOwner = product.userOwner;
            product.arrayVarient.Add(varient2);

         


            WSProductClient client = new WSProductClient();
            int statusCode = client.WSCreateProduct(product, VariableConfigController.GetBucket());
            client.Close();

            int expected = 0;
           
            Assert.AreEqual(expected, statusCode, "The expected value did not match  the actual value");
        }
        [TestMethod]
        public void UpdateProduct()
        {
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            WSProductClient client = new WSProductClient();

            product = client.WSDetailProduct("07e0f22d-21e4-4a73-8049-919ed89832f0", VariableConfigController.GetBucket());
            product.modifiedDate = DateTime.Today;
            product.description = "asdasd";
            product.name = "TEST UPDATE";
            product.nameShort ="tu";
            product.display = true;
            string Category = "03c59955-130a-4ac3-a54a-bd591399dafe~category_69";
            int indexSymbol = -1;
            indexSymbol = Category.IndexOf('~');
            if (indexSymbol > 0)
            {
                int lenghtOfName = Category.Length - 37;
                product.categoryId = Category.Substring(0, indexSymbol);
                product.categoryName = Category.Substring(indexSymbol, Category.Length - indexSymbol - 1); // tru di vi tri dau ~
            }
            else
            {
                product.categoryId = product.categoryName = Category;
            }
            int statusCode = client.WSUpdateProduct(product, VariableConfigController.GetBucket());
            client.Close();
        //=============
            WSCategoryClient clientCategory = new WSCategoryClient();
          //  var ListCategory = clientCategory.WSGetAllCategory("category", "get_all_category",null,null,-1,false);
            var ListCategory = clientCategory.WSGetAllCategoryNoLimit("category", "get_all_category", VariableConfigController.GetBucket());
            clientCategory.Close();

               string id = "e4478033-4e95-40fa-9430-c8d81d21ed2a";

               var list = client.WSDetailProduct(id, VariableConfigController.GetBucket());
       
            client.Close();
           int expected = 0;
            Assert.AreEqual(expected, statusCode, "The expected value did not match  the actual value");
           
        }
        [TestMethod]
        public void DeleteProduct()
        {
            string id = "001";
            WSProductClient client = new WSProductClient();
            int statusCode = client.WSDeleteProduct(id, VariableConfigController.GetBucket());
            client.Close();
            int expected = 0;
            Assert.AreEqual(expected, statusCode, "The expected value did not match  the actual value");
        }

        [TestMethod]
        public void DetailsProduct()
        {
            string id = "001";
            WSProductClient client = new WSProductClient();
            Com.SmoovPOS.Entity.Product product = client.WSDetailProduct(id, VariableConfigController.GetBucket());
            client.Close();
            Assert.IsNull(product);
        }
    }
}
