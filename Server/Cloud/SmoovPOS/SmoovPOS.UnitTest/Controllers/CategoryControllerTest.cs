﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Entity;
using SmoovPOS.Service.Category;
using SmoovPOS.Common.Controllers;
using Com.SmoovPOS.Common.Controllers;
using System.Collections;

namespace SmoovPOS.UnitTest
{
    [TestClass]
    public class CategoryControllerTest
    {
        public CategoryControllerTest()
        {

        }
        [TestMethod]
        public void CreateCategory()
        {
            string siteID = "smoovpos";
            Category cate = new Category();
            cate.createdDate = DateTime.Today; 
            cate.modifiedDate = DateTime.Today; 
            cate.userID = "001";
            cate.userOwner = "Merchant_001";
            cate.status = true;
            cate.description = "description";
            cate.title = "title";
            cate.image = "JSON";
            cate.display = true;
            cate.product = "JSON";
            cate._id = Guid.NewGuid().ToString();
            WSCategory client = new WSCategory();
            int statusCode = client.WSCreateCategory(cate, siteID);
            
        }
        [TestMethod]
        public void UpdateCategory()
        {
            Category cate = new Category();
            cate.modifiedDate = DateTime.Today; 
            cate.status = true;
            cate.description = "description";
            cate.title = "title";
            cate.image = "JSON";
            cate.display = true;
            cate.product = "JSON";
            cate._id = Guid.NewGuid().ToString();
            WSCategory client = new WSCategory();        
           // int StatusCode = client.WSUpdateCategory(cate); ;

        }

        [TestMethod]
        public void Delete()
        {
           /* string id = "001";
            WSCategory client = new WSCategory();
            int StatusCode = client.WSDeleteCategory(id); ;
            */
            string siteID = "smoovpos";
            string id = "001";
            WSCategory client = new WSCategory();
            Category category = client.WSDetailCategory(id, siteID);
            category.display = false;
            int StatusCode = client.WSUpdateCategory(category, siteID);
            int expected = 0;
            Assert.AreEqual(expected, StatusCode, "The expected value did not match  the actual value");
        }

        [TestMethod]
        public void Details()
        {
            string siteID = "smoovpos";
            string id = "001";
            WSCategory client = new WSCategory();
            Category category = client.WSDetailCategory(id, siteID);

        }

        [TestMethod]

        public void updateStatus()
        {
            WSCategory client = new WSCategory();
            string siteID = "smoovpos";

            string[] arrId = new string[] { "016060dc-25dc-48ff-800f-4ae8f1c75e71", "01a94f9d-8bf9-457c-b9ba-a24868f68fa3" };
            //IEnumerable listCate = client.WSGetAllCategory("category", "get_all_category", null, null, 5, false, 1);
            foreach (String id in arrId)
            {
                Category category = client.WSDetailCategory(id, siteID);
                if (category != null)
                {
                    category.status = true;
                    int expectResult = 0;
                    int response = client.WSUpdateCategory(category, siteID);
                    Assert.AreEqual(response, expectResult, "The expected value did not match  the actual value");
                }           
            }
        }

        [TestMethod]
        public void AddMoreCategory()
        {
            WSCategory client = new WSCategory();
            for (var i = 0; i < 200; i++)
            {   
                Category cate = new Category();
                cate._id = Guid.NewGuid().ToString();
                cate.createdDate = DateTime.Today; ;
                cate.modifiedDate = DateTime.Today;
                cate.userID = "001";
                cate.userOwner = "Merchant_001";
                cate.status = true;
                cate.description = "description_" + i.ToString(); ;
                cate.title = "Category_" + i.ToString(); ;
                cate.image = "https://s3-ap-southeast-1.amazonaws.com/smoovturnkey/-1168534783_10799579_374864306010555_1177649203_n.jpg";
                cate.display = true;
                cate.arrayProduct = new List<CategoryProductItem>();
               // client.WSCreateCategory(cate);
            }
         
           

        }
        [TestMethod]
        public void TestUploadS3()
        {
            PhotosController ph = new PhotosController();
            string path = "C:/Users/Public/Pictures/Sample Pictures/Desert.jpg";
            string t = "https://s3-ap-southeast-1.amazonaws.com/smoovturnkey/Desert.jpg";
            string result = ph.UploadToS3Amazon(path);
            Assert.AreEqual(t, result, "link image not equal");

        }

    }
}
