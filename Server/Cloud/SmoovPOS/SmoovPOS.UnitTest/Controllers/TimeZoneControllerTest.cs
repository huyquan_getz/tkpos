﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Common;
using System.Linq;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;

namespace SmoovPOS.UnitTest.Controllers
{
    [TestClass]
    public class TimeZoneControllerTest
    {
        [TestMethod]
        //public void ConvertUTCToTimeZone(DateTime timeUtc, string timeZone, string siteID)
        public void ConvertUTCToTimeZone()
        {
            DateTime timeUtc = DateTime.UtcNow;
            //string timeZone = "SE Asia Standard Time";
            string timeZone = "Singapore Standard Time";
          
            bool isExitTimeZone = false;
            //  string siteID ="";
            //Check timezone is valid or not
            //if (ConstantSmoovs.Enums.ListTimeZone.ContainsKey(timeZone) && ConstantSmoovs.Enums.ListTimeZone.ContainsValue(timeZone))
            if (ConstantSmoovs.Enums.ListTimeZone.ContainsKey(timeZone) && ConstantSmoovs.Enums.ListTimeZone.ContainsValue(timeZone))
            {
               // timeZone = ConstantSmoovs.Enums.ListTimeZone.Where(i => i.Key.Contains(timeZone) || i.Value.Contains(timeZone)).FirstOrDefault().Key;
                isExitTimeZone = true;
            }
            //else
            //{
            //    //Get saved general settings from couchbase
            //    WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            //    timeZone = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).TimeZoneFullName;
            //    merchantGeneralSettingClient.Close();
            //}

            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            //DateTime time = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, timeZoneInfo);
           // return time;

            Assert.IsTrue(isExitTimeZone);
       //     Assert.IsNotNull(timeZoneInfo);
        }
    }
}
