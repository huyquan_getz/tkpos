﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Service.Inventory;

namespace SmoovPOS.UnitTest.Controllers
{
    [TestClass]
    public class InventoryControllerTest
    {
        [TestMethod]
        public void TestMethod1()
        {
        }
        [TestMethod]
        public void CreateInventory() 
        {
            string siteID = "smoovpos";
            Com.SmoovPOS.Entity.Inventory inventory = new Com.SmoovPOS.Entity.Inventory();
            inventory._id = Guid.NewGuid().ToString();
            inventory.createdDate = DateTime.Now;
            inventory.modifiedDate = DateTime.Now;            
            inventory.status = false;
            inventory.userID = "001";
            inventory.userOwner = "merchant_001";
            inventory.country = "vietnam";
            inventory.state = "da nang";
            inventory.streetAddress = "2 quang trung";
            inventory.timeZone = "GMT (UTC+08:00)";
            inventory.zipCode = "319262";
            //inventory.contact = "8411111111";                     //20150408 MaoNguyen DEL
            inventory.contact = "8411111111";                     //20150408 MaoNguyen ADD
            inventory.description = "test inventory";
            inventory.businessID = "branch 1";
            inventory.city = "Da Nang";
            WSInventory client = new WSInventory();
            int actual = client.WSCreateInventory(inventory, siteID);            
            int expect = 0;
            Assert.AreEqual(expect, actual, "The expected value did not match  the actual value");

        }
        [TestMethod]
        public void getAllStore()
        {
            string siteID = "smoovpos";
            WSInventory client = new WSInventory();
            var listStore = client.WSGetAll("inventory", "get_all_inventory", siteID);
            foreach (var inventory in listStore)
            {
                 inventory.addressCombine = inventory.streetAddress + ", " + inventory.city + " " + inventory.zipCode + ", " + inventory.state + ", " + inventory.country;
                 inventory.city = "";
                 int response = client.WSUpdateInventory(inventory, siteID);
            }
        }
        [TestMethod]
        public void updateStatus()
        {
            WSInventory client = new WSInventory();
            string siteID = "smoovpos";

            string[] arrId = new string[] { "2abd58e7-9ce9-461d-a5e8-428232226eb5" };
            foreach (String id in arrId)
            {
                Com.SmoovPOS.Entity.Inventory inventory = client.WSDetailInventory(id, siteID);
                if (inventory != null)
                {
                    inventory.status = !inventory.status;
                    int expectResult = 0;
                    int response = client.WSUpdateInventory(inventory, siteID);
                    Assert.AreEqual(response, expectResult, "The expected value did not match  the actual value");
                }
            }
        }

        [TestMethod]
        public void deleteInventory() {
            string siteID = "smoovpos";
            WSInventory client = new WSInventory();
            string id = "2ef03f29-61c1-428f";
            int result = client.WSDeleteInventory(id, siteID);
            Assert.AreEqual(result, 1, "The expected value did not match  the actual value");
        }
    }
}
