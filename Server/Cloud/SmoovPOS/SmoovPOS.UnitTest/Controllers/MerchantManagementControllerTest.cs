﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Utility.WSMerchantManagementReference;
using Com.SmoovPOS.Model;

namespace SmoovPOS.UnitTest.Controllers
{
    [TestClass]
    public class MerchantManagementControllerTest
    {
        [TestMethod]
        public void TestUpdateStatus()
        {
            bool updateStatus = true;
            string id = "57ea4653-a70c-4497-90d3-02e25d935004";
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            MerchantManagementModel model = client.WSGetMerchantManagement(new Guid(id));
            model.status = updateStatus;
            bool isUpdate = client.WSUpdateMerchantManagement(model);
            client.Close();
            Assert.IsTrue(isUpdate, "FAILS");
        }
    }
}
