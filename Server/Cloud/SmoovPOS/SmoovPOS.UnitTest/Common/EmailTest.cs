﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Mail;
using SmoovPOS.Utility.MailServer;
using SmoovPOS.Business.Business;
using SmoovPOS.Entity.Models;

namespace SmoovPOS.UnitTest.Common
{
    [TestClass]
    public class EmailTest
    {
        //[TestMethod]

        //public void SendMailOfficeSite()
        //{
        //    MailServerUtil mailServerUtil = new MailServerUtil();
        //    string displayName = "Smoovpos support";
        //    string toEmail = "chuong.nguyen@qgs.vn";
        //    string subject = "Test email from chuong.nguyen";
        //    string body = "<html> <div>Hello the world </div> </html>";
        //    bool actual = mailServerUtil.SendMailOfficeSite(displayName, toEmail, subject, body);

        //    bool expected = true;

        //    Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        //}
        [TestMethod]
        public void TestSendMail()
        {
            MailServerUtil mailServerUtil = new MailServerUtil();
            string displayName = "Smoovpos support";
            string toEmail = "chuong.nguyen@qgs.vn";
            string subject = "Test email from chuong.nguyen";
            int type = 0;
            MailObjectModel mailObject = new MailObjectModel();
            mailObject.CreateDate = "2014-12-12";
            bool actual = mailServerUtil.SendMailTest(type, displayName, toEmail, subject, mailObject);

            bool expected = true;

            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");

        }
        [TestMethod]
        public void TestLoadMail()
        {
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            MailServerModel mail = mailConfig.GetConfigMailServer(null);
            var dsfdf = mailConfig.AddOrUpdateMailServer(mail.Id, mail, null);

            mail = mailConfig.GetConfigMailServer(new Guid("CA4A3BDB-71E7-406F-A6BC-2510CAA296A8"));
            var sdfdsf = mailConfig.AddOrUpdateMailServer(mail.Id, mail, new Guid("CA4A3BDB-71E7-406F-A6BC-2510CAA296A8"));

            Assert.AreEqual(true, dsfdf, "Fail");
        }
    }
}
