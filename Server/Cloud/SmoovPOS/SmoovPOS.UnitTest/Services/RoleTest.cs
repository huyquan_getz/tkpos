﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.WSRoleReference;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class RoleTest
    {
        [TestMethod]
        public void CreateRole()
        {
            WSRoleClient client = new WSRoleClient();
            Role role = new Role();
            role._id = Guid.NewGuid().ToString();
            role.userID = "001";
            role.userOwner = "Merchant_001";
            role.status = true;
            role.keyName = "Staff";
            role.roleName = "Staff";
            role.description = "Staff";
            role.siteId = "site_001";
            role.channels = new string[] { "server" };
            client.WSCreateRole(role, "smoovpos");
            client.Close();
        }

        [TestMethod]
        public void listRole()
        {
            WSRoleClient client = new WSRoleClient();
            var listRole = client.WSGetAllRole("role", "get_all_role", "smoovpos");
            foreach (Role role in listRole)
            {
                role.roleName = role.keyName;
            }
            client.Close();
        }
        [TestMethod]
        public void detailRole()
        {
            WSRoleClient client = new WSRoleClient();
            var role = client.WSDetailRole("0a567cf8-39db-4bdf-a0a4-48772b5a0acb", "smoovpos");
            client.Close();
        }
    }
}
