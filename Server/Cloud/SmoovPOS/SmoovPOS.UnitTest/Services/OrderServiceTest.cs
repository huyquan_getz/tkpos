﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Common;
using SmoovPOS.Entity.Models.Paging;
using SmoovPOS.Utility.WSOrderReference;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class OrderServiceTest
    {
        /// <summary>
        /// Creates the order.
        /// </summary>
        /// <createdby>nhu.dang</createdby>
        /// <createddate>12/15/2014-2:38 PM</createddate>
        [TestMethod]
        public void CreateOrder()
        {
            WSOrderClient client = new WSOrderClient();
            int code = 0;

            //Delete all order first
            string bucket = "smoovapp_14219795975512226";
            client.WSDeleteAllOrder(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAll, bucket);

            for (int i = 0; i < 37; i++)
            {
                Order order = new Order();
                order._id = Guid.NewGuid().ToString();
                order.status = true;
                order.display = true;
                order.createdDate = DateTime.UtcNow;
                order.modifiedDate = DateTime.UtcNow;

                order.userID = "001" + i.ToString();
                order.userOwner = "Merchant_001";
                order.orderCode = "Code141212-" + i + 1;
                order.transactionID = "Tran141212-" + i + 1;
                order.completedDate = DateTime.UtcNow.AddDays(-1 * (i + 1));
                order.paymentStatus =(i % 4);
                order.orderStatus = (i % 8);
                order.remark = "";
                order.subTotal = 100;
                order.grandTotal = 100;
                order.orderType = 0;
                order.channels = new string[] { "Server" }; 
                order.arrayProduct = new List<ProductOrder>();
                order.arrayProduct.Add(new ProductOrder()
                {
                    _id = Guid.NewGuid().ToString(),
                    title = "abc",
                    arrayVariantOrder = new List<VarientItem>()
                });

                order.customerSender = new Customer();
                order.customerSender.memberID = "member" + i;
                order.customerSender.firstName = "name" + i;
                order.customerReceiver = new Customer();
                order.customerReceiver.memberID = "member" + i;
                order.customerReceiver.firstName = "name" + i;
                order.refundInfo = null;
                order.paymentSuccess = true;
                order.QRCode = "";
                order.orderBy = "";
                order.isCart = false;
                order.inventory = new Inventory();
                order.inventory.businessID = "store" + i;
                order.inventory.timeZone = "SE Asia Standard Time";
                order.inventory.streetAddress = "";
                order.inventory.country = "";
                order.inventory.state = "";               
                order.inventory.createdDate = order.completedDate;
                
                order.staff = new EmployeeInfor();
                order.staff._id = Guid.NewGuid().ToString();
                order.staff.fullName = "Order Test from UNIT";
            //    order.tax = new Tax();
                order.discount = 3;
                order.roundAmount = 0.00;
                order.instruction = new Instruction();
                order.serviceShipping = new DeliverySetting();
                order.isCancelRequest = ((i % 2) == 0)?true:false;
                code = client.WSCreateOrder(order, bucket);
            }

            for (int i = 0; i < 37; i++)
            {
                Order order = new Order();
                order._id = Guid.NewGuid().ToString();
                order.status = true;
                order.display = true;
                order.createdDate = DateTime.UtcNow;
                order.modifiedDate = DateTime.UtcNow;

                order.userID = "001";
                order.userOwner = "Merchant_001";
                order.orderCode = "Code141212-" + i + 1;
                order.transactionID = "Tran141212-" + i + 1;
                order.completedDate = DateTime.UtcNow.AddDays(-1 * (i + 1));
                order.paymentStatus = (i % 4);
                order.orderStatus = (i % 8);
                order.remark = "";
                order.subTotal = 100;
                order.grandTotal = 100;
                order.orderType = 1;
                order.channels = new string[] { "Server" }; ;

                order.arrayProduct = new List<ProductOrder>();
                order.arrayProduct.Add(new ProductOrder()
                {
                    _id = Guid.NewGuid().ToString(),
                    title = "abc",
                    arrayVariantOrder = new List<VarientItem>()
                });

                order.customerSender = new Customer();
                order.customerSender.memberID = "member" + i;
                order.customerSender.firstName = "name" + i;
                order.customerReceiver = new Customer();
                order.customerReceiver.memberID = "member" + i;
                order.customerReceiver.firstName = "name" + i;
                order.refundInfo = null;
                order.paymentSuccess = true;
                order.QRCode = "";
                order.orderBy = "";
                order.isCart = false;
                order.inventory = new Inventory();
                order.inventory.businessID = "store" + i;
                order.inventory.timeZone = "SE Asia Standard Time";
                order.inventory.streetAddress = "";
                order.inventory.country = "";
                order.inventory.state = "";
                order.inventory.createdDate = order.completedDate;
                order.staff = new EmployeeInfor();
                order.staff._id = Guid.NewGuid().ToString();
                order.staff.fullName = "Order Test from UNIT";
               // order.tax = new Tax();
                order.discount = 3;
                order.roundAmount = 0.00;
                order.instruction = new Instruction();
                order.serviceShipping = new DeliverySetting();
                order.isCancelRequest = ((i % 2) == 0) ? true : false;
                code = client.WSCreateOrder(order, bucket);
            }

            for (int i = 0; i < 27; i++)
            {
                Order order = new Order();
                order._id = Guid.NewGuid().ToString();
                order.status = true;
                order.display = true;
                order.createdDate = DateTime.UtcNow;
                order.modifiedDate = DateTime.UtcNow;

                order.userID = "001";
                order.userOwner = "Merchant_001";
                order.orderCode = "Code141212-" + i + 1;
                order.transactionID = "Tran141212-" + i + 1;
                order.completedDate = DateTime.UtcNow.AddDays(-1 * (i + 1));
                order.paymentStatus = (i % 4);
                order.orderStatus = 7;
                order.remark = "";
                order.subTotal = 100;
                order.grandTotal = 100;
                order.orderType = i % 2;
                order.channels = new string[] { "Server" }; ;

                order.arrayProduct = new List<ProductOrder>();
                order.arrayProduct.Add(new ProductOrder()
                {
                    _id = Guid.NewGuid().ToString(),
                    title = "abc",
                    arrayVariantOrder = new List<VarientItem>()
                });

                order.customerSender = new Customer();
                order.customerSender.memberID = "member" + i;
                order.customerSender.firstName = "name" + i;
                order.customerReceiver = new Customer();
                order.customerReceiver.memberID = "member" + i;
                order.customerReceiver.firstName = "name" + i;
                order.refundInfo = null;
                order.paymentSuccess = true;
                order.QRCode = "";
                order.orderBy = "";
                order.isCart = false;
                order.inventory = new Inventory();
                order.inventory.businessID = "store" + i;
                order.inventory.timeZone = "SE Asia Standard Time";
                order.inventory.streetAddress = "";
                order.inventory.country = "";
                order.inventory.state = "";
                order.inventory.createdDate = order.completedDate;
                order.staff = new EmployeeInfor();
                order.staff._id = Guid.NewGuid().ToString();
                order.staff.fullName = "Order Test from UNIT";
               // order.tax = new Tax();
                order.discount = 3;
                order.roundAmount = 0.00;
                order.instruction = new Instruction();
                order.serviceShipping = new DeliverySetting();
                order.isCancelRequest = ((i % 2) == 0) ? true : false;
                code = client.WSCreateOrder(order, bucket);
            }

            // Always close the client.
            client.Close();

            int expected = 0;

            Assert.AreEqual(expected, code, "The expected value did not match  the actual value");
        }

        [TestMethod]
        public void ViewOrder()
        {
            WSOrderClient client = new WSOrderClient();
            string bucket = "smoovapp_14219795975512226";
            Order[] orderList = client.WSGetOrderBySearchCondition(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAll, new SearchOrderModel(), bucket);

            Order order = client.WSGetOrder(orderList.LastOrDefault()._id, bucket);

            // Always close the client.
            client.Close();
        }
        /// <summary>
        /// Gets all order paging.
        /// </summary>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/21/2015-3:26 PM</datetime>
        [TestMethod]
        public void GetAllOrderPaging()
        {
            WSOrderClient client = new WSOrderClient();
            SearchOrderModel searchModel = new SearchOrderModel();
            searchModel.OrderType = 0;

            EntityPaging entityPaging = new EntityPaging();
            entityPaging.pageGoTo = 1;
            entityPaging.limit = 25;
            entityPaging.sort = "orderCode";
            Order[] orderList = null;//client.WSGetAllOrderOrderPaging(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAll, searchModel, entityPaging, bucket);

            client.Close();
            Assert.IsNotNull(orderList, "Failed");
        }
        [TestMethod]
        public void GetAllOrderDelivery()
        {
            WSOrderClient client = new WSOrderClient();
            SearchOrderModel searchModel = new SearchOrderModel();
            searchModel.OrderType = 0;
            string bucket = "smoovapp_14219795975512226";
            Order[] orderList = client.WSGetAllOrderDelivery(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAllDelivery, bucket);
            client.Close();
            Assert.IsNotNull(orderList, "Failed");
        }

        [TestMethod]
        public void GetAllOrderSelfCollect()
        {
            WSOrderClient client = new WSOrderClient();
            SearchOrderModel searchModel = new SearchOrderModel();
            searchModel.OrderType = 0;
            string bucket = "smoovapp_14219795975512226";
            Order[] orderList = client.WSGetAllOrderSelfCollect(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAllSelfCollect, bucket);
            client.Close();
            Assert.IsNotNull(orderList, "Failed");
        }

        [TestMethod]
        public void GetAllOrderCancel()
        {
            WSOrderClient client = new WSOrderClient();
            SearchOrderModel searchModel = new SearchOrderModel();
            searchModel.OrderType = 0;
            string bucket = "smoovapp_14219795975512226";
            Order[] orderList = client.WSGetAllOrderCancel(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAllCancel, bucket);
            client.Close();
            Assert.IsNotNull(orderList, "Failed");
        }
        [TestMethod]
        public void DeleteAllOrder()
        {
            WSOrderClient client = new WSOrderClient();

            string bucket = "smoovapp_14219795975512226";
            client.WSDeleteAllOrder(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAll, bucket);

            // Always close the client.
            client.Close();
        }
    }
}
