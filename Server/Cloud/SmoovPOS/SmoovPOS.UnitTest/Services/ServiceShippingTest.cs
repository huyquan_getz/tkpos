﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.WSServiceShippingReference;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class ServiceShippingTest
    {
        [TestMethod]
        public void CreateServiceShipping()
        {
            ServiceShipping serviceShipping = new ServiceShipping();
            WSServiceShippingClient client = new WSServiceShippingClient();
            string siteId = "";
            serviceShipping._id = Guid.NewGuid().ToString();
            serviceShipping.createdDate = DateTime.Now;
            serviceShipping.deliveryService = "Express";
            serviceShipping.shippingFee = 20;
            serviceShipping.timeEstimate = "1 - 2 Business Days";
            serviceShipping.modifiedDate = DateTime.Now;
            serviceShipping.status = true;
            int actual = client.WSCreateServiceShipping(serviceShipping, siteId);
            client.Close();
            // Use the 'client' variable to call operations on the service.
            int expected = 0;
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
            // Always close the client.
            
        }
    }
}
