﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Model;
using SmoovPOS.Business.Business;
using SmoovPOS.Utility.WSPaymentGatewayReference;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class PaymentGatewayTest
    {
        [TestMethod]

        public void WSAddOrUpdatePaymentGateway()
        {
            WSPaymentGatewayClient client = new WSPaymentGatewayClient();
            string id = "2c3d9913-e650-4b07-846a-9cedf38303a3";
            PaymentModel payment = new PaymentModel();
           // payment.Id = Guid.NewGuid().ToString();
           
            payment.Status = 0;
            payment.SmoovUrl = "https://secure.smoovpay.com/access";
            payment.SuccessPage = "https://secure.smoovpay.com/access";
            payment.CancelPage = "http://localhost/SmoovAdmin/Setting";
            payment.StrUrl = "http://localhost/SmoovAdmin/Setting";
            
            //sandbox
            payment.SandboxSmoovUrl = "https://sandbox.smoovpay.com/access";
         
            payment.CreateDate = DateTime.Now;
            payment.ModifiedDate = DateTime.Now;
           // PaymentBusiness paymentBusiness = new PaymentBusiness();
          //  bool actual = paymentBusiness.AddOrUpdatePaymentGateway(id, payment);
           bool actual = client.WsCreatOrUpdatePaymentGateway(id, payment);
           client.Close(); 
            //bool actual = false;
            bool expected = true;
            
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");

        }
         [TestMethod]
        public void GetPaymentGateway()
        {
             WSPaymentGatewayClient client = new WSPaymentGatewayClient();
             //PaymentBusiness paymentBusiness = new PaymentBusiness();
             //PaymentModel payment = paymentBusiness.GetPaymentGateway();
             //client.Close();
             
             var paymentGateway = client.GetPaymentGateway();
             client.Close();
             bool actual = true;
             //if (payment.Id != null) {
             //    actual = true;
             //}
             //bool actual = false;
             bool expected = true;
             
             Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
    }
}
