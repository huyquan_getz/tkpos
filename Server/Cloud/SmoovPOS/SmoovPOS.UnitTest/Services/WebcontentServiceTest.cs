﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Entity;
using SmoovPOS.Service.WebContents;
using System.Collections.Generic;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class WebcontentServiceTest
    {
        [TestMethod]
        public void WSCreateWebContent()
        {
            string siteID = "smoovpos";
            WebContent webContent = new WebContent();
            webContent._id = Guid.NewGuid().ToString();
            webContent.userID = "001";
            webContent.userOwner = "Merchant_001";
            webContent.status = true;
            webContent.aboutUs = "About Us";
            webContent.contactUs = "Contact Us";
            SocialNetwork socialNetwork = new SocialNetwork();

            socialNetwork.facebook = "";
            socialNetwork.googlePlus = "";
            socialNetwork.twitter = "";
            socialNetwork.youtube = "";
            webContent.socialNetworkLink = socialNetwork;
            List<SlideImage> slideImage = new List<SlideImage>();
            webContent.slideImage = slideImage;

            WSWebContent WSWebContent = new WSWebContent();
            int expected = 0;
            int actual = WSWebContent.WSCreateWebContent(webContent, siteID);
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
        [TestMethod]
        public void WSGetAllWebContent()
        {
            string siteID = "smoovpos";
            WebContent webContent = new WebContent();
            WSWebContent WSWebContent = new WSWebContent();
            string designDoc = "webcontent";
            string viewName = "get_all_webcontent";
            IEnumerable<WebContent> listproduct = WSWebContent.WSGetAllWebContent(designDoc, viewName, siteID);
            Assert.IsNotNull(listproduct);
        }

        [TestMethod]
        public void WSUpdateWebContent()
        {
            string siteID = "smoovpos";
            WebContent webContent = new WebContent();
            WSWebContent WSWebContent = new WSWebContent();
            string id = "b96c8883-be6c-4076-9bce-0bb8bb3cb54b";
            webContent = WSWebContent.WSDetailWebContent(id, siteID);
            webContent.aboutUs = "Test Unit Test About Us";
            int expected = 0;
            int actual = WSWebContent.WSUpdateWebContent(webContent, siteID);
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
    }
}
