﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Entity;
using System.Security.Cryptography;
using SmoovPOS.Utility.WSCustomerReference;
using SmoovPOS.Common;

namespace SmoovPOS.UnitTest.Services
{
    /// <summary>
    /// Summary description for CustomerTest
    /// </summary>
    [TestClass]
    public class CustomerTest
    {
        public CustomerTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        /// <summary>
        /// Sign up for customer
        /// </summary>
        /// <param>FormCollection</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>24/12/2014-10:07 PM</createddate>
        /// 
        [TestMethod]
        public void SignUp()
        {
            Customer cus = new Customer();
            cus.createdDate = DateTime.UtcNow;
            cus.modifiedDate = DateTime.UtcNow;
            cus.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            cus.firstName = "hung";
            cus.lastName = "huu";
            cus.fullName = cus.firstName + " " + cus.lastName;
            cus.email = "dhhung89";
            cus.status = false;
            cus.active = false;
            cus.display = true;
            cus._id = Guid.NewGuid().ToString();
            cus.pinCode = Guid.NewGuid().ToString();
            cus.password = "123456";
            cus.passwordConfirm = "123456";
            cus.channels = new string[] { "server" };
            var dateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds;
            cus.memberID = "cus-" + Convert.ToInt32(dateTimeInt).ToString();
            WSCustomerClient client = new WSCustomerClient();
            int statusCode = client.WSCreateCustomer(cus, "");         
        }

        /// <summary>
        /// update status for customer when customer active mail verify
        /// </summary>
        /// <param>string memberID</param>
        /// <param>string pinCode</param>
        /// <returns>status(true/false)</returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>24/12/2014-10:07 PM</createddate>
        /// 
        [TestMethod]
        public void ActiveAccount()
        {
            string memberID = "c1259424-7db3-4330-bd3e-10c01247509f";
            string pinCode = "1a37bee1-f5eb-4ef6-a432-74b0c302dd24";
            Customer cus = new Customer();
            WSCustomerClient client = new WSCustomerClient();
            cus = client.WSDetailCustomer(memberID,"smoovpos");
            if (cus != null)
            {
                if (cus.pinCode == pinCode)
                {
                    cus.active = true;
                    cus.status = true;
                    client.WSUpdateCustomer(cus,"smoovpos");
                }
                else
                {
                    Assert.AreEqual(null, null, "pinCode not found");
                }
            }
            else
            {
                Assert.AreEqual(null, null, "memberID not found");
            }
            
        }
        [TestMethod]
        public void TestEmail() {
            WSCustomerClient client = new WSCustomerClient();
            int actual = client.WSCheckEmailExist("customer", "get_all_customer","chuong.nguyen@smoovapp.com","");
            int expected = 0;
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }

        [TestMethod]
        public void ListAllCustomer()
        {
            WSCustomerClient client = new WSCustomerClient();
            string siteId = "";
            IEnumerable<Com.SmoovPOS.Entity.Customer> listAll = client.WSGetAllCustomer("customer", "get_all_customer", siteId);
            //foreach (Customer cus in listAll)
            //{
            //    cus.addressCombine = cus.address + ", " + cus.city + " " + cus.zipCode + ", " + cus.state + ", " + cus.country;
            //    cus.strTags = "";
            //    int reponseCode = client.WSUpdateCustomer(cus, "");

            //}
            Assert.IsNotNull(listAll);
        }


        /*
         *Quynh.Hoang
         * activate/ deactive status of customer
         * step1:  list all customer (avoid hard code)
         * step2:  get first customer in list
         * step3: get detail from Id
         * step4: update customer from call service
       */

        [TestMethod]
        public void updateStatusCustomer()
        {
            WSCustomerClient client = new WSCustomerClient();
            string siteId = "";
            IEnumerable<Com.SmoovPOS.Entity.Customer> listAll = client.WSGetAllCustomer("customer", "get_all_customer", siteId);
            Customer customer = new Customer();
            foreach (var customerItem in listAll)
            {

                customer = client.WSDetailCustomer(customerItem._id, "");
                break;
            }

            if (string.IsNullOrEmpty(customer._id))
            {
                customer.status = !customer.status;
                int reponseCode = client.WSUpdateCustomer(customer, "");
                Assert.AreEqual(0, reponseCode, "The expected value did not match  the actual value");
            }
            else
            {
                Assert.AreEqual(null, null, "no customer found");
            }
           
        }
        /// <summary>
        /// Delete customer
        /// </summary>
        /// <param>string customerID</param>         
        /// <createdby>vu.ho</createdby>
        /// <createddate>26/12/2014-10:07 AM</createddate>
        /// 
        [TestMethod]
        public void deleteCustomer()
        {
            WSCustomerClient client = new WSCustomerClient();
            string CustomerID = "c612f9c9-ab1c-4b26-9da7-8e55d4ad8e13";
            int reponse = client.WSDeleteCustomer(CustomerID, "");
            Assert.AreEqual(0, reponse, "Delete customer false");
        }
    }
}
