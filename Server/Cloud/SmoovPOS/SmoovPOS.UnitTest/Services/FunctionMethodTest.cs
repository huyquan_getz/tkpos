﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.UnitTest.WSFunctionMethodReference;
using SmoovPOS.Entity.Models;
using Com.SmoovPOS.Model;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class FunctionMethodTest
    {
        //[TestMethod]
        //public void TestMethod1()
        //{
        //}
        [TestMethod]
        public void LoadFunctionMethod()
        {
            Guid id = new Guid("5533B49E-F5D1-455D-B9BF-2392B5D8349D");
            WSFunctionMethodClient client = new WSFunctionMethodClient();

            var result = client.WSGetFunctionMethod(id);

            Assert.IsNotNull(result, "fails");
        }

        [TestMethod]
        public void CreateFunctionMethod()
        {
            WSFunctionMethodClient client = new WSFunctionMethodClient();
            //for (int i = 0; i < 100; i++)
            //{
            //    int j = i + 5;

            var model = new FunctionMethodModel();


            model.name = string.Format("Employee");
            model.keyMethod = "Employee".ToUpper();
            model.isDatabaseFunction = false;
            model.dataType = string.Empty;
            model.userOwner = "697a0a11-c760-4111-a320-dbed2eb0a601";

            var result = client.WSCreateFunctionMethod(model);

            Assert.IsNotNull(result, "fails");


            //}
        }

        [TestMethod]
        public void UpdateFunctionMethod()
        {
            WSFunctionMethodClient client = new WSFunctionMethodClient();
            var model = new FunctionMethodModel();

            model.name = string.Format("Online Shop");
            model.keyMethod = "OnlineShop".ToUpper();
            model.isDatabaseFunction = false;
            model.dataType = string.Empty;
            model.modifyUser = "697a0a11-c760-4111-a320-dbed2eb0a601";

            Guid id = new Guid("04F76F92-F2E7-4180-BE72-DC252786F6B9");
            model.functionMethodID = id;
            var result = client.WSUpdateFunctionMethod(model);

            Assert.AreEqual(result, true, "fails");
        }
        [TestMethod]
        public void DeleteMerchantManagement()
        {
            Guid id = new Guid("5533B49E-F5D1-455D-B9BF-2392B5D8349D");
            WSFunctionMethodClient client = new WSFunctionMethodClient();

            var result = client.WSDeleteFunctionMethod(id);

            Assert.AreEqual(result, true, "fails");
        }
        [TestMethod]
        public void ExistMerchantManagement()
        {
            WSFunctionMethodClient client = new WSFunctionMethodClient();

            var result = client.WSExistFunctionMethodByName("EmployEe", null);

            Assert.AreEqual(result, true, "fails");
        }
    }
}
