﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.UnitTest.WSInitCouchBaseReference;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class InitationTest
    {
        [TestMethod]
        public void AutoGenerateDocDefault()
        {
            string bucket = "nguyenanh_14216586087747618";
            WSInitCouchBaseClient client = new WSInitCouchBaseClient();
            client.AutoGenerateDocDefault(bucket);
            client.Close();

        }

        [TestMethod]
        public void AutoGenerateViewCouchBase()
        {
            string bucket = "hoan_test";
            WSInitCouchBaseClient client = new WSInitCouchBaseClient();
            client.AutoGenerateViewCouchBase(bucket);
            client.Close();

        }

        [TestMethod]
        public void CreateBucketCouchbaseServer()
        {
            string bucket = "hoan_test";
            WSInitCouchBaseClient client = new WSInitCouchBaseClient();
            client.CreateBucketCouchbase(bucket,3456);
            client.AutoGenerateViewCouchBase(bucket);
            //client.AutoGenerateDocDefault(bucket);
            client.Close();

        }

        [TestMethod]
        public void DeleteMerchant()
        {
            string bucket = "8cb7356b-4e76-4076-b107-7da85d306036";
            WSInitCouchBaseClient client = new WSInitCouchBaseClient();
            client.DeleteBucketCouchbase("deleleMerchant", "get_all_by_bucket", bucket);
            client.Close();

        }

    }
}
