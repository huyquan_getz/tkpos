﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Utility.WSDeliveryCountryReference;
using SmoovPOS.Utility.WSDeliverySettingReference;
using Com.SmoovPOS.Entity;
using SmoovPOS.Common;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class DeliverySettingTest
    {
        [TestMethod]
        public void TestMethod1()
        {
        }

        [TestMethod]
        public void CreateDeliveryCountry()
        {
            string bucket = "bucket_1_shadown";
            WSDeliveryCountryClient client = new WSDeliveryCountryClient();

            #region Vietnam
            DeliveryCountry countryVietname = new DeliveryCountry() { _id = Guid.NewGuid().ToString(), country = "Vietnam", countryCode = "084" };

            countryVietname.id = countryVietname._id;
            countryVietname.createdDate = DateTime.UtcNow;
            countryVietname.modifiedDate = DateTime.UtcNow;
            countryVietname.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            countryVietname.image = "~/Content/Images/Country/vn.png";

            countryVietname.arrayArea = new System.Collections.Generic.List<Area>();
            countryVietname.arrayArea.Add(new Area() { areaName = "An Giang", zipCode = "880000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ba Ria Vung Tau", zipCode = "790000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Bac Lieu", zipCode = "260000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Bac Kan", zipCode = "960000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Bac Giang", zipCode = "220000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Bac Ninh", zipCode = "790000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ben Tre", zipCode = "930000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Binh Duong", zipCode = "590000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Binh Dinh", zipCode = "820000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Binh Phuoc", zipCode = "830000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Binh Thuan", zipCode = "800000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ca Mau", zipCode = "970000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Cao Bang", zipCode = "900000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Can Tho", zipCode = "270000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Hau Giang", zipCode = "910000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Da Nang", zipCode = "550000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "DakLak", zipCode = "630000-" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Dac Nong", zipCode = "640000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Dong Nai", zipCode = "810000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Dong Thap", zipCode = "870000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Gia Lai", zipCode = "600000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ha Giang", zipCode = "310000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ha Nam", zipCode = "400000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ha Noi", zipCode = "100000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ha Tinh", zipCode = "480000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Hai Duong", zipCode = "170000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Hai Phong", zipCode = "180000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Hoa Binh", zipCode = "350000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Hung Yen", zipCode = "160000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ho Chí Minh", zipCode = "700000 hoặc " });
            //country.arrayArea.Add(new Area() { areaName = "", zipCode = "760000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Khanh Hoa", zipCode = "650000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Kien Giang", zipCode = "920000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Kon Tum", zipCode = "580000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Lai Chau", zipCode = "390000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Dien Bien", zipCode = "" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Lang Son", zipCode = "240000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Lao Cai", zipCode = "330000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Lam Dong", zipCode = "670000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Long An", zipCode = "850000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Nam Dinh", zipCode = "420000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Nghe An", zipCode = "460000 " });
            //country.arrayArea.Add(new Area() { areaName = "", zipCode = "hoặc 470000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ninh Binh", zipCode = "430000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ninh Thuan", zipCode = "660000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Phu Thọ", zipCode = "290000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Phu Yen", zipCode = "620000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Quang Binh", zipCode = "510000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Quang Nam", zipCode = "560000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Quang Ngai", zipCode = "570000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Quang Ninh", zipCode = "200000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Quang Tri", zipCode = "520000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Soc Trang", zipCode = "950000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Son La", zipCode = "360000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Tay Ninh", zipCode = "840000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Thai Binh", zipCode = "410000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Thai Nguyen", zipCode = "250000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Thanh Hoa", zipCode = "440000 " });
            //country.arrayArea.Add(new Area() { areaName = "", zipCode = "hoặc 450000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Thua Thien Hue", zipCode = "530000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Tien Giang", zipCode = "860000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Tra Vinh", zipCode = "940000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Tuyen Quang", zipCode = "300000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Vinh Long", zipCode = "890000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Vinh Phuc", zipCode = "280000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Yen Bai", zipCode = "320000" });
            countryVietname.arrayArea.Add(new Area() { areaName = "Ha Tay", zipCode = "" });

            client.WSCreateDeliveryCountry(countryVietname, bucket);
            #endregion

            #region Sing
            DeliveryCountry countrySingapore = new DeliveryCountry() { _id = Guid.NewGuid().ToString(), country = "Singapore", countryCode = "065" };

            countrySingapore.id = countrySingapore._id;
            countrySingapore.createdDate = DateTime.UtcNow;
            countrySingapore.modifiedDate = DateTime.UtcNow;
            countrySingapore.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            countrySingapore.image = "~/Content/Images/Country/sg.png";

            countrySingapore.arrayArea = new System.Collections.Generic.List<Area>();
            countrySingapore.arrayArea.Add(new Area() { areaName = "Raffles Place", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Cecil", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Marina", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "People's Park", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Anson", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Tanjong Pagar", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Queenstown", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Tiong Bahru", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Telok Blangah", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Harbourfront", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Pasir Panjang", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Hong Leong Garden", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Clementi New Town", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "High Street", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Beach Road (part)", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Middle Road", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Golden Mile", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Little India", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Orchard", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Cairnhill", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "River Valley", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Ardmore", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Bukit Timah", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Holland Road", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Tanglin", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Watten Estate", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Novena", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Thomson", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Balestier", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Toa Payoh", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Serangoon", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Macpherson", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Braddell", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Geylang", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Eunos", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Katong", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Joo Chiat", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Amber Road", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Bedok", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Upper East Coast", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Eastwood", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Kew Drive", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Loyang", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Changi", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Tampines", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Pasir Ris", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Serangoon Garden", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Hougang", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Ponggol", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Bishan", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Ang Mo Kio", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Upper Bukit Timah", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Clementi Park", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Ulu Pandan", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Jurong", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Hillview", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Dairy Farm", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Bukit Panjang", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Choa Chu Kang", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Lim Chu Kang", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Tengah", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Kranji", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Woodgrove", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Upper Thomson", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Springleaf", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Yishun", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Sembawang", zipCode = "" });
            countrySingapore.arrayArea.Add(new Area() { areaName = "Seletar", zipCode = "" });

            client.WSCreateDeliveryCountry(countrySingapore, bucket);
            #endregion

            #region Indo
            DeliveryCountry countryIndo = new DeliveryCountry() { _id = Guid.NewGuid().ToString(), country = "Indonesia", countryCode = "062" };

            countryIndo.id = countryIndo._id;
            countryIndo.createdDate = DateTime.UtcNow;
            countryIndo.modifiedDate = DateTime.UtcNow;
            countryIndo.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            countryIndo.image = "~/Content/Images/Country/id.png";

            countryIndo.arrayArea = new System.Collections.Generic.List<Area>();
            countryIndo.arrayArea.Add(new Area() { areaName = "Bali", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Bangka–Belitung Islands", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Banten", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Bengkulu", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Central Java", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Central Kalimantan", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Central Sulawesi", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "East Java", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "East Kalimantan[5]", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "East Nusa Tenggara", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Gorontalo", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Jakarta (Special Capital Region)", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Jambi", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Lampung", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Maluku", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "North Kalimantan", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "North Maluku", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "North Sulawesi", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "North Sumatra", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Riau", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Riau Islands Province", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "South Kalimantan", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "South Sulawesi", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "South Sumatra", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Southeast Sulawesi", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Special Region of Aceh", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Special Region of Papua", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Special Region of West Papua", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "Special Region of Yogyakarta", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "West Java", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "West Kalimantan", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "West Nusa Tenggara", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "West Sulawesi", zipCode = "" });
            countryIndo.arrayArea.Add(new Area() { areaName = "West Sumatra", zipCode = "" });

            client.WSCreateDeliveryCountry(countryIndo, bucket);
            #endregion

            #region Philip
            DeliveryCountry countryPhilippines = new DeliveryCountry() { _id = Guid.NewGuid().ToString(), country = "Philippines", countryCode = "063" };

            countryPhilippines.id = countryPhilippines._id;
            countryPhilippines.createdDate = DateTime.UtcNow;
            countryPhilippines.modifiedDate = DateTime.UtcNow;
            countryPhilippines.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            countryPhilippines.image = "~/Content/Images/Country/ph.png";

            countryPhilippines.arrayArea = new System.Collections.Generic.List<Area>();
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Abra", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Agusan del Norte[7]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Agusan del Sur", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Aklan", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Albay", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Antique", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Apayao", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Aurora", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Basilan[10]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Bataan", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Batanes", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Batangas", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Benguet[13]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Biliran", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Bohol", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Bukidnon", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Bulacan", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Cagayan", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Camarines Norte", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Camarines Sur[14]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Camiguin", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Capiz", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Catanduanes", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Cavite", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Cebu[16]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Compostela Valley", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Cotabato", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Davao del Norte", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Davao del Sur[18]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Davao Occidental", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Davao Oriental", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Dinagat Islands", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Eastern Samar", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Guimaras", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Ifugao", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Ilocos Norte", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Ilocos Sur", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Iloilo[19]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Isabela[20]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Kalinga", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "La Union", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Laguna", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Lanao del Norte[21]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Lanao del Sur", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Leyte[23]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Maguindanao[24]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Marinduque", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Masbate", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Misamis Occidental", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Misamis Oriental[26]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Mountain Province", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Negros Occidental[27]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Negros Oriental", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Northern Samar", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Nueva Ecija", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Nueva Vizcaya", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Occidental Mindoro", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Oriental Mindoro", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Palawan[29]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Pampanga[30]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Pangasinan[31]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Quezon[32]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Quirino", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Rizal", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Romblon", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Samar", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Sarangani", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Siquijor", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Sorsogon", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "South Cotabato[34]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Southern Leyte", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Sultan Kudarat", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Sulu", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Surigao del Norte", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Surigao del Sur", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Tarlac", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Tawi-Tawi", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Zambales[38]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Zamboanga del Norte", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Zamboanga del Sur[39]", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Zamboanga Sibugay", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "Metro Manila", zipCode = "" });
            countryPhilippines.arrayArea.Add(new Area() { areaName = "", zipCode = "" });

            client.WSCreateDeliveryCountry(countryPhilippines, bucket);
            #endregion

            #region Malay
            DeliveryCountry countryMalaysia = new DeliveryCountry() { _id = Guid.NewGuid().ToString(), country = "Malaysia ", countryCode = "060" };

            countryMalaysia.id = countryMalaysia._id;
            countryMalaysia.createdDate = DateTime.UtcNow;
            countryMalaysia.modifiedDate = DateTime.UtcNow;
            countryMalaysia.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            countryMalaysia.image = "~/Content/Images/Country/my.png";

            countryMalaysia.arrayArea = new System.Collections.Generic.List<Area>();
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Johor", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Kedah", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Kelantan", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Kuala Lumpur", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Labuan", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Melaka", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Negeri Sembilan", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Pahang", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Putrajaya", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Perlis", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Pulau Pinang", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Perak", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Sabah", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Selangor", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Sarawak", zipCode = "" });
            countryMalaysia.arrayArea.Add(new Area() { areaName = "Terengganu", zipCode = "" });

            client.WSCreateDeliveryCountry(countryMalaysia, bucket);
            #endregion
        }

        [TestMethod]
        public void CreateDeliverySetting()
        {
            string bucket = "smoovapp_14219795975512226";

            string country = "7cbd1690-5010-4483-bdbc-837a12f8a526";
            WSDeliveryCountryClient client = new WSDeliveryCountryClient();
            
            WSDeliverySettingClient clientSetting = new WSDeliverySettingClient();

            #region Sing
            var resultCountry = client.WSGetDeliveryCountry(country, bucket);

            DeliveryCountry countrySingapore = new DeliveryCountry() { _id = resultCountry._id, country = resultCountry.country, countryCode = resultCountry.countryCode, image = resultCountry.image };

            DeliverySetting settingSingapore = new DeliverySetting() { _id = Guid.NewGuid().ToString() };

            settingSingapore.id = settingSingapore._id;
            settingSingapore.country = countrySingapore;
            settingSingapore.flag = true.ToString();
            settingSingapore.createdDate = DateTime.UtcNow;
            settingSingapore.modifiedDate = DateTime.UtcNow;
            settingSingapore.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();

            settingSingapore.arrayServiceDelivery = new System.Collections.Generic.List<ServiceDelivery>();

            ServiceDelivery serviceDelivery1 = new ServiceDelivery();
            serviceDelivery1._id = Guid.NewGuid().ToString();
            serviceDelivery1.status = true;
            serviceDelivery1.serviceName = "Shipping price";
            serviceDelivery1.criteria = 0;
            serviceDelivery1.beginValue = 1;
            serviceDelivery1.endValue = 2;
            serviceDelivery1.isUnlimit = false;
            serviceDelivery1.unit = 0;
            serviceDelivery1.shippingPrice = 10;
            serviceDelivery1.beginEstimate = 10;
            serviceDelivery1.endEstimate = 20;

            serviceDelivery1.arrayAreaShipping = new System.Collections.Generic.List<AreaShipping>();
            AddAreaShipping(resultCountry, serviceDelivery1);
            settingSingapore.arrayServiceDelivery.Add(serviceDelivery1);
            
            ServiceDelivery serviceDelivery2 = new ServiceDelivery();
            serviceDelivery2._id = Guid.NewGuid().ToString();
            serviceDelivery2.status = true;
            serviceDelivery2.serviceName = "Singpos";
            serviceDelivery2.criteria = 0;
            serviceDelivery2.beginValue = 5;
            serviceDelivery2.endValue = 10;
            serviceDelivery2.isUnlimit = false;
            serviceDelivery2.unit = 0;
            serviceDelivery2.shippingPrice = 10;
            serviceDelivery2.beginEstimate = 40;
            serviceDelivery2.endEstimate = 70;

            serviceDelivery2.arrayAreaShipping = new System.Collections.Generic.List<AreaShipping>();
            AddAreaShipping(resultCountry, serviceDelivery2);
            settingSingapore.arrayServiceDelivery.Add(serviceDelivery2);
            #endregion

            clientSetting.WSCreateDeliverySetting(settingSingapore,bucket);
        }

        private static void AddAreaShipping(DeliveryCountry resultCountry, ServiceDelivery serviceDelivery)
        {
            for (int i = 0; i < resultCountry.arrayArea.Count; i++)
            {
                var area = resultCountry.arrayArea[i];

                var areaShipping = new AreaShipping();
                areaShipping.areaName = area.areaName;
                areaShipping.zipCode = area.zipCode;
                areaShipping.isSelect = true;
                areaShipping.areaPrice = ((i + 1) * 10);

                serviceDelivery.arrayAreaShipping.Add(areaShipping);
            }
        }
        [TestMethod]
        public void GetAllDeliveryCountry()
        {
            string bucket = "bucket_1_shadown";
            WSDeliveryCountryClient client = new WSDeliveryCountryClient();

            var results = client.WSGetAllDeliveryCountry("deliverycountry", "get_all_delivery_country", bucket);
        }
        [TestMethod]
        public void GetAllDeliverySetting()
        {
            string bucket = "smoovapp_14219795975512226";
            WSDeliverySettingClient client = new WSDeliverySettingClient();

            var results = client.WSGetAllDeliverySetting("deliverysetting", "get_all_delivery_setting", bucket);
        }
    }
}
