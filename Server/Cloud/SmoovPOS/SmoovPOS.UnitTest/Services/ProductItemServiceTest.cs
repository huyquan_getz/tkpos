﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Service.Product;
using Com.SmoovPOS.Entity;
using System.Collections.Generic;
namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class ProductItemServiceTest
    {
        [TestMethod]
        public void WSCreateProductItem()
        {
            string siteID = "smoovpos";
            ProductItem product = new ProductItem();
            product._id = Guid.NewGuid().ToString();
            product.userID = "001";
            product.userOwner = "Merchant_001";
            product.status = true;
            product.arrayVarient = null;

            WSProductItem WSProduct = new WSProductItem();
            int expected = 0;
            int actual = WSProduct.WSCreateProductItem(product, siteID);
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
        [TestMethod]
        public void WSUpdateProductItem() {
            ProductItem product = new ProductItem();
            string siteID = "smoovpos";
            WSProductItem WSProduct = new WSProductItem();
            int expected = -1;
            int actual = WSProduct.WSUpdateProductItem(product, siteID);
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
        [TestMethod]
        public void WSDetailProductItem() {
            string siteID = "smoovpos";
            ProductItem product = new ProductItem();
            WSProductItem WSProduct = new WSProductItem();
            string _id = "001";
            product = WSProduct.WSDetailProductItem(_id, siteID);
            Assert.IsNull(product);
        }
        [TestMethod]
        public void WSDeleteProductItem() {
            string siteID = "smoovpos";
            ProductItem product = new ProductItem();
            WSProductItem WSProduct = new WSProductItem();
            string _id = "0000000";
            int expected = 1;
            int actual = WSProduct.WSDeleteProductItem(_id, siteID);
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }

        [TestMethod]
        public void WSGetAllProductItem()
        {
            string siteID = "smoovpos";
            ProductItem product = new ProductItem();
            WSProductItem WSProduct = new WSProductItem();
            string designDoc = "productitem";
            string viewName = "get_all_product_item";
            IEnumerable<ProductItem> listproduct = WSProduct.WSGetAllProductItem(designDoc, viewName, siteID);
            Assert.IsNotNull(listproduct);
        }

    }
}
