﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Utility.IWSEmailTemplateReference;
namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class TemplateEmailTest
    {
        [TestMethod]
        public void CreateTemplate()
        {
            IWSEmailTemplateClient client = new IWSEmailTemplateClient();
            TemplateEmail templateEmail = new TemplateEmail();
            templateEmail._id = Guid.NewGuid().ToString();
            templateEmail.userID = "001";
            templateEmail.userOwner = "Merchant_001";
            templateEmail.status = true;
            templateEmail.createdDate = DateTime.UtcNow;
            templateEmail.modifiedDate = DateTime.UtcNow;
            templateEmail.display = true;
            templateEmail.DateTimeInt = "1419330559.24266";
            templateEmail.name = "Email sender";
            templateEmail.body = "\r\n<div>\r\n <div id=\"header-mail\">\r\n<label>\r\nHi [FirstNameOfCustomer],\r\n\r\n \r\n </label>\r\n<div style=\"margin-top:10px;\">\r\n Below is a summary of your order from your [BusinessName]. Please keep this mail for future reference.\r\n\r\n        </div>\r\n        <div>\r\n           \r\n            If you have any questions or need additional assistance with your account, please contact us.\r\n   </div>\r\n   </div>\r\n <div style=\"margin-top:50px;margin-left:50px;\">\r\n        <div>\r\n <label>\r\n                Your order number is [OrderNumber], placed  [OrderDay], status \r\n </label>\r\n <b>[OrderStatusOfCustomer].</b>\r\n    </div>\r\n  <div>\r\n   <div>\r\n  r\n   <table style=\"width:50%;\">\r\n <tr>\r\n  <td><p><b>Bill to:</b></p></td>\r\n <td><p><b>[OrderTypeInformation]</b></p></td>\r\n  </tr>\r\n <tr style=\"margin-top:10px;\">\r\n <td>\r\n [FirstNameLastNameBill] \r\n  </td>\r\n  <td>\r\n    [FirstNameLastNameReceive]\r\n  </td>\r\n  </tr>\r\n    <tr>\r\n   <td>\r\n  [AddressBill]\r\n    </td>\r\n    <td>\r\n   [AddressReceive]\r\n    </td>\r\n   </tr>\r\n <tr>\r\n   <td>\r\n [PhoneBill]\r\n    </td>\r\n                        <td>\r\n         [PhoneReceive]\r\n  </td>\r\n    </tr>\r\n <tr>\r\n <td>\r\n  [EmailBill]\r\n    </td>\r\n <td>\r\n  [EmailReceive]\r\n    </td>\r\n  </tr>\r\n\r\n                </table>\r\n  <!-- bill to ship to-->\r\n    <!-- Payment infor-->\r\n  \r\n <div style=\"margin-top:30px;\">   \r\n  <div style=\"width:50%\">\r\n [ProductOrderDetails]\r\n  </div>   \r\n  </div>\r\n  \r\n  </div>\r\n  \r\n  </div>\r\n    </div>\r\n   <div>\r\n   <div style=\"margin-top:10px;\">\r\n           Visit us again at: [OnlineshopLink]\r\n  </div>\r\n <div style=\"margin-top:30px;\">\r\n Sincerely,\r\n    </div>\r\n   <div>\r\n   [BusinessName]\r\n   </div>\r\n   </div>\r\n</div>\r\n";
            templateEmail.channels = new string[] { "server" };
           // client.WSCreateTemplateEmail(templateEmail, "smoovpos");
            client.Close();
        }
        [TestMethod]
        public void ListTemplateEmail()
        {
            IWSEmailTemplateClient client = new IWSEmailTemplateClient();
            var listTemplate = client.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, "smoovpos");
             client.Close();
             Assert.IsNotNull(listTemplate);
        }
         [TestMethod]
        public void detailTemplateEmail()
        {
            IWSEmailTemplateClient client = new IWSEmailTemplateClient();
            var template = client.WSGetDetailTemplateEmail("01c61f9b-89f0-4fae-836e-65b01ad6c1a1", "smoovpos");
          
            client.Close();
            Assert.IsNotNull(template);
        }
         //[TestMethod]
         //public void DeleteAllTemplate()
         //{
         //    IWSEmailTemplateClient client = new IWSEmailTemplateClient();


         //    client.WSDeleteAllTemplateEmail(Constants.CouchBase.DesignDocTemplateEmail, Constants.CouchBase.TemplateEmailViewAll);

         //    // Always close the client.
         //    client.Close();
         //}
    }
}
