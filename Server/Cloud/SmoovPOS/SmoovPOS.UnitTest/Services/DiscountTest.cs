﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Entity;
using System.Security.Cryptography;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UnitTest.WSDiscountReference;


namespace SmoovPOS.UnitTest.Services
{
    /// <summary>
    /// Summary description for CustomerTest
    /// </summary>
    [TestClass]
    public class DiscountTest
    {
        //[TestMethod]
        //public void CreateDiscount()
        //{
        //    Discount discount = new Discount();
        //    discount.active = false;
        //    discount.beginDate = DateTime.UtcNow;
        //    discount.discountLimit = 100;
        //    discount.discountValue = 50;
        //    discount.endDate = DateTime.UtcNow.AddDays(100);
        //    discount.name = "HuyQTA";
        //    discount.type = 0;
        //    discount.createdDate = DateTime.UtcNow;
        //    discount.modifiedDate = DateTime.UtcNow;
        //    discount._id = Guid.NewGuid().ToString();
        //    WSDiscountClient discountClient = new WSDiscountClient();
        //    //int actual = discountClient.WSCreateDiscount(discount, "smoovpos");
        //    //Assert.IsFalse(actual == 1);
        //}

        [TestMethod]
        public void GetAllDiscount()
        {
            try
            {
                string bucket = "bucket_2_shadown";
                string designDoc = "discount";
                string viewName = "get_all_discount";
                WSDiscountClient client = new WSDiscountClient();
                var listCollect = client.WSGetAllDiscount(designDoc, viewName, bucket);
                client.Close();
                Assert.IsNotNull(listCollect, "failed");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //[TestMethod]
        //public void UpdateDiscount()
        //{
        //    WSDiscountClient discountClient = new WSDiscountClient();
        //    Discount[] listdiscount = discountClient.WSGetAllDiscount("discount", "get_all_discount", "smoovpos");
        //    if (listdiscount.Length > 0)
        //    {
        //        Discount discount = listdiscount[0];
        //        discount.name += " - Updated";
        //        discount.type = 0;
        //        discount.createdDate = DateTime.UtcNow;
        //        discount.modifiedDate = DateTime.UtcNow;
        //        //int actual = discountClient.WSUpdateDiscount(discount, "smoovpos");
        //        //Assert.IsFalse(actual == 1);
        //    }

        //}

        //[TestMethod]
        //public void DeleteDiscount()
        //{
        //    try
        //    {
        //        WSDiscountClient discountClient = new WSDiscountClient();
        //        Discount[] listdiscount = discountClient.WSGetAllDiscount("discount", "get_all_discount", "bucket_2_shadown");
        //        if (listdiscount.Length > 0)
        //        {
        //            foreach (var item in listdiscount)
        //            {
        //                discountClient.WSDeleteDiscount(item._id, "smoovpos");
        //            }

        //            //Assert.IsFalse(actual == 1);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}
        //[TestMethod]
        //public void deleteDiscountTest()
        //{
        //    WSDiscountClient discountClient = new WSDiscountClient();
        //    string discountID = "06e2ab02-5c46-42e6-b9a1-5625b2fa9d95";
        //    discountClient.WSDeleteDiscount(discountID, "smoovpos");
        //    //Assert.AreEqual(0, reponse, "Delete customer false");
        //}
    }
}
