﻿using Com.SmoovPOS.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Entity.Models.Paging;
using SmoovPOS.Service.online;
using System.Collections.Generic;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class OnlineServiceTest
    {
        [TestMethod]
        public void WSGetAllActiveCategory()
        {   
            string siteID ="smoovpos";
            WSOnline WSProduct = new WSOnline();
            //int expected = 0;
            IEnumerable<Category> actual = WSProduct.WSGetAllActiveCategory("category", "get_all_category", siteID);
            //Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }

        [TestMethod]
        public void WSGetProductItemDetail()
        {
            string siteID = "smoovpos";
            WSOnline WSProduct = new WSOnline();
            //int expected = 0;
            ProductItem actual = WSProduct.WSGetProductItemDetail("0701b298-4d45-4498-93e2-e1375b83ea99", siteID);
            //Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }

        [TestMethod]
        public void WSGetAllActiveProductItem()
        {
            string siteID = "smoovpos";
            WSOnline WSProduct = new WSOnline();
            //int expected = 0;
            IEnumerable<ProductItem> productItemList = WSProduct.WSGetAllActiveProductItem("productItem", "get_all_active", new EntityPaging(), siteID);
            //Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }

        [TestMethod]
        public void WSGetAllActiveProductItemByCategory()
        {
            string siteID = "smoovpos";
            WSOnline WSProduct = new WSOnline();
            //int expected = 0;
            IEnumerable<ProductItem> productItemList = WSProduct.WSGetAllActiveProductItemByCategory("productItem", "get_all_by_category_active", "Category_Spd", new EntityPaging(), siteID);
            //Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
    }
}
