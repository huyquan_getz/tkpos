﻿using Com.SmoovPOS.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Common;
using SmoovPOS.Service.Inventory;
using SmoovPOS.Utility.WSEmployeeReference;
using SmoovPOS.Utility.WSRoleReference;
using System;
using System.Collections.Generic;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class EmployeeTest
    {
        [TestMethod]
        public void CreateEmployee()
        {
            WSEmployeeClient client = new WSEmployeeClient();
            // Use the 'client' variable to call operations on the service.
            string siteID = "smoovpos";
            // Always close the client.
            Employee employee = new Employee();
            employee.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            employee.firstName = "Hoang";
            employee.lastName = "Quynh";
            employee.fullName = "Van Quynh Hoang";
            employee.EmployeeId =  client.WSGetNextEmployeeId("employee", "get_next_employeeID","smoovpos");;
            employee.image = "";
            employee.password = "123456";
            employee.passwordConfirm = "123456";
            employee.phone = "0987654321";
            employee.pinCode = "12345";
            employee.status = true;
            employee.userID = "001";
            employee.userOwner = "Merchant_001";
            employee.zipCode = "";
            employee.hiredDate = new DateTime(2014, 12, 09).ToString();
            employee.gender = 1;
            employee.email = "abc@qgs.com.vn";
            employee.city = "Da Nang";
            employee.channels = new string[] { "server" };
            employee.siteId = "site_001";
            employee.birthday = new DateTime(1990, 12, 09).ToString();
            employee.address = "2 Quang Trung";
            employee._id = Guid.NewGuid().ToString();
            employee.AddressCombine = employee.address + ", " + employee.city + " " + employee.zipCode + ", " + employee.state + ", " + employee.country;
            List<Permission> arrPermission = new List<Permission>();

            WSRoleClient clientRole = new WSRoleClient();
            Role role = new Role();
            Permission permission = new Permission();
            permission.role = clientRole.WSGetAllRole("role", "get_all_role", "smoovpos").Length > 0 ? clientRole.WSGetAllRole("role", "get_all_role", "smoovpos")[0] : role;
            // store of Permission
            WSInventory clientInventory = new WSInventory();
            Inventory inventory = new Inventory();
            var listInventory = clientInventory.WSGetAll("inventory", "get_all_inventory", siteID);
            foreach(Inventory item in listInventory)
            {
                permission.store = item;
                break;
            }
                
            arrPermission.Add(permission);
            employee.arrPermission = arrPermission;
            int statusCode = client.WSCreateEmployee(employee, "smoovpos");
            Assert.AreEqual(0, statusCode, "The expected value did not match  the actual value");
            client.Close();
        }

        [TestMethod]
        public void updateEmployee()
        {
            string id = "bbce94e3-6dde-43fb-8cfb-c0790e760b64";
            WSEmployeeClient client = new WSEmployeeClient();
            var employee = client.WSDetailEmployee(id, "smoovpos");
            employee.fullName = "Van Quynh Hoang";
            employee.EmployeeId = 1;

            int response = client.WSUpdateEmployee(employee, "smoovpos");
            client.Close();
        }

        [TestMethod]
        public void detail()
        {
            string id = "bbce94e3-6dde-43fb-8cfb-c0790e760b64";
            WSEmployeeClient client = new WSEmployeeClient();
            var employee = client.WSDetailEmployee(id, "smoovpos");

            client.Close();
        }

        [TestMethod]
        public void getAll()
        {

            WSEmployeeClient client = new WSEmployeeClient();
            var listEmployees = client.WSGetAllEmployee("employee", "get_all_employee", "smoovpos");
            foreach (Employee emp in listEmployees)
            {

            }
            client.Close();
        }

        [TestMethod]
        public void deleteEmployee()
        {
            string id = "0713d42c-afa0-48dc-84e3-81a55fb58c57";
            WSEmployeeClient client = new WSEmployeeClient();
            int responseCode = client.WSDeleteEmployee(id, "smoovpos");
            client.Close();
        }
    }
}
