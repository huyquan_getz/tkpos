﻿using SmoovPOS.Common;
using SmoovPOS.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.Utility.CustomAttributes
{
    public class LocalizedRequiredAttribute : RequiredAttribute, IClientValidatable
    {
        public System.Collections.Generic.IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var requiredMessageResource = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText(ConstantSmoovs.Attributes.ResourceRequiredCode);

            if (string.IsNullOrEmpty(requiredMessageResource))
            {
                return new[] { new ModelClientValidationRule
                               {
                                   ErrorMessage = ErrorMessage, 
                                   ValidationType = "required"
                               } };
            }
            return new[] { new ModelClientValidationRule
                               {
                                   ErrorMessage = requiredMessageResource, 
                                   ValidationType = "required"
                               } };
        }
    }
}