﻿using SmoovPOS.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.Utility.CustomAttributes
{
    public class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        public LocalizedDisplayNameAttribute(string resourceId)
            : base(GetLabelFromResource(resourceId))
        { }

        private static string GetLabelFromResource(string resourceId)
        {
            var label = DependencyResolver.Current.GetService<ILanguageManager>().GetLabelText(resourceId);
            return string.IsNullOrEmpty(label) ? resourceId : label;
        }
    }
}