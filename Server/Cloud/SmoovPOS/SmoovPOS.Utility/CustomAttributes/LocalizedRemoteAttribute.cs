﻿using SmoovPOS.Common;
using SmoovPOS.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.Utility.CustomAttributes
{
    public class LocalizedRemoteAttribute : RemoteAttribute
    {
        public string ResourceCode { get; set; }

        public LocalizedRemoteAttribute(string action, string controller)
            : base(action, controller)
        {

        }
        public override string FormatErrorMessage(string name)
        {
            var messageErrorMessage = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText(ResourceCode);
            if (messageErrorMessage != ConstantSmoovs.Attributes.ResourceUndefined)
            {
                return messageErrorMessage;
            }
            return ConstantSmoovs.Attributes.ResourceDuplicateMessage;
        }
    }
}