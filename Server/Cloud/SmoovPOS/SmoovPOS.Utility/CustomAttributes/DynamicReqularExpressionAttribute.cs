﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace SmoovPOS.Utility.CustomAttributes
{
    public class DynamicReqularExpressionAttribute : ValidationAttribute
    {
        public string[] PatternProperty { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            foreach (var properties in PatternProperty)
            {
                PropertyInfo property = validationContext.ObjectType.GetProperty(properties);
                if (property == null)
                {
                    return new ValidationResult(string.Format("{0} is unknown property", PatternProperty));
                }
                var pattern = property.GetValue(validationContext.ObjectInstance, null).ToString();
                if (string.IsNullOrEmpty(pattern))
                {
                    return new ValidationResult(string.Format("{0} must be a valid string regex", PatternProperty));
                }

                var str = value as string;
                if (string.Equals(pattern, "True", StringComparison.OrdinalIgnoreCase))
                {
                    if (string.IsNullOrEmpty(str))
                    {
                        return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
                    }
                }
                var match = Regex.Match(str, pattern);
                if (!match.Success)
                {
                    return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
                }
            }
            return null;
        }
    }
}