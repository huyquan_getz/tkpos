﻿using SmoovPOS.Common;
using SmoovPOS.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.Utility.CustomAttributes
{
    public class LocalizedCompareAttribute : CompareAttribute, IClientValidatable
    {
        public LocalizedCompareAttribute(string otherProperty)
            : base(otherProperty)
        {
        }

        public string Name { get; set; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var fieldName = !string.IsNullOrEmpty(Name) ? DependencyResolver.Current.GetService<ILanguageManager>().GetLabelText(Name) : metadata.PropertyName;
            var fieldToCompareName = DependencyResolver.Current.GetService<ILanguageManager>().GetLabelText(OtherProperty);

            var resourceCode = ConstantSmoovs.Attributes.ResourceCompareInvalid;
            var messageResource = string.Empty;
            var messageErrorMessage = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText(resourceCode);
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                messageResource = ErrorMessage;
            }
            //messageResource = messageErrorMessage != ConstantSmoovs.Attributes.ResourceUndefined ?
            //    string.Format(messageErrorMessage, fieldName, fieldToCompareName) : ConstantSmoovs.Attributes.ResourceUndefined;

            return new[] { new ModelClientValidationEqualToRule(messageResource, OtherProperty) };
        }
    }
}