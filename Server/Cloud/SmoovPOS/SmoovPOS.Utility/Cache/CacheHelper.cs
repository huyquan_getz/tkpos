﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Menu;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Utility.Cache
{
    public class CacheHelper
    {
        private static object locker = new object();
        private static List<Menu> categories;
        private static List<Menu> collections;
        private static List<WebContent> webContents;
        private static List<TableOrderModel> tableOrders;
        private static List<Station> customerInfos;
        public static List<Menu> Categories
        {
            get
            {
                lock (locker)
                {
                    return categories;
                }
            }
            set
            {
                lock (locker)
                {
                    categories = value;
                }
            }
        }
        public static List<Menu> Collections
        {
            get
            {
                lock (locker)
                {
                    return collections;
                }
            }
            set
            {
                lock (locker)
                {
                    collections = value;
                }
            }
        }
        public static List<WebContent> WebContents
        {
            get
            {
                lock (locker)
                {
                    return webContents;
                }
            }
            set
            {
                lock (locker)
                {
                    webContents = value;
                }
            }
        }
        public static List<TableOrderModel> TableOrders
        {
            get
            {
                lock (locker)
                {
                    return tableOrders;
                }
            }
            set
            {
                lock (locker)
                {
                    tableOrders = value;
                }
            }
        }
        public static List<Station> CustomerInfos
        {
            get
            {
                lock (locker)
                {
                    return customerInfos;
                }
            }
            set
            {
                lock (locker)
                {
                    customerInfos = value;
                }
            }
        }
        public static void Add<T>(object o, string key) where T : class
        {
            HttpContext.Current.Cache.Insert(
                key,
                o,
                null,
                DateTime.Now.AddMinutes(100),
                System.Web.Caching.Cache.NoSlidingExpiration);
        }
        public static void Clear(string key)
        {
            HttpContext.Current.Cache.Remove(key);
        }
        public static bool Exists(string key)
        {
            return HttpContext.Current.Cache[key] != null;
        }
        public static T Get<T>(string key) where T : class
        {
            try
            {
                return (T)HttpContext.Current.Cache[key];
            }
            catch
            {
                return null;
            }
        }
    }
}