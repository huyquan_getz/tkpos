﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Utility.Models
{
    public class ProductByCategoryModel
    {

        public string Category { get; set; }

        public IEnumerable<ProductItem> ProductItemList { get; set; }

        public EntityPaging Paging { get; set; }

      
    }
}