﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmoovPOS.Utility.WSTakeAwaySettingReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="WSTakeAwaySettingReference.IWSTakeAwaySetting")]
    public interface IWSTakeAwaySetting {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSCreateTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSCreateTakeAwaySettingResponse")]
        int WSCreateTakeAwaySetting(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSCreateTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSCreateTakeAwaySettingResponse")]
        System.Threading.Tasks.Task<int> WSCreateTakeAwaySettingAsync(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSDetailTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSDetailTakeAwaySettingResponse")]
        Com.SmoovPOS.Entity.TakeAwaySetting WSDetailTakeAwaySetting(string _id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSDetailTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSDetailTakeAwaySettingResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Entity.TakeAwaySetting> WSDetailTakeAwaySettingAsync(string _id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSUpdateTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSUpdateTakeAwaySettingResponse")]
        int WSUpdateTakeAwaySetting(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSUpdateTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSUpdateTakeAwaySettingResponse")]
        System.Threading.Tasks.Task<int> WSUpdateTakeAwaySettingAsync(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSDeleteTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSDeleteTakeAwaySettingResponse")]
        int WSDeleteTakeAwaySetting(string _id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSDeleteTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSDeleteTakeAwaySettingResponse")]
        System.Threading.Tasks.Task<int> WSDeleteTakeAwaySettingAsync(string _id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSDetailTakeAwaySettingByBranchId", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSDetailTakeAwaySettingByBranchIdResponse")]
        Com.SmoovPOS.Entity.TakeAwaySetting WSDetailTakeAwaySettingByBranchId(string designDoc, string viewName, string branch_id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSDetailTakeAwaySettingByBranchId", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSDetailTakeAwaySettingByBranchIdResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Entity.TakeAwaySetting> WSDetailTakeAwaySettingByBranchIdAsync(string designDoc, string viewName, string branch_id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSGetAllTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSGetAllTakeAwaySettingResponse")]
        Com.SmoovPOS.Entity.TakeAwaySetting[] WSGetAllTakeAwaySetting(string designDoc, string viewName, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSTakeAwaySetting/WSGetAllTakeAwaySetting", ReplyAction="http://tempuri.org/IWSTakeAwaySetting/WSGetAllTakeAwaySettingResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Entity.TakeAwaySetting[]> WSGetAllTakeAwaySettingAsync(string designDoc, string viewName, string siteID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWSTakeAwaySettingChannel : SmoovPOS.Utility.WSTakeAwaySettingReference.IWSTakeAwaySetting, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WSTakeAwaySettingClient : System.ServiceModel.ClientBase<SmoovPOS.Utility.WSTakeAwaySettingReference.IWSTakeAwaySetting>, SmoovPOS.Utility.WSTakeAwaySettingReference.IWSTakeAwaySetting {
        
        public WSTakeAwaySettingClient() {
        }
        
        public WSTakeAwaySettingClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WSTakeAwaySettingClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSTakeAwaySettingClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSTakeAwaySettingClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int WSCreateTakeAwaySetting(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID) {
            return base.Channel.WSCreateTakeAwaySetting(takeAwaySetting, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSCreateTakeAwaySettingAsync(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID) {
            return base.Channel.WSCreateTakeAwaySettingAsync(takeAwaySetting, siteID);
        }
        
        public Com.SmoovPOS.Entity.TakeAwaySetting WSDetailTakeAwaySetting(string _id, string siteID) {
            return base.Channel.WSDetailTakeAwaySetting(_id, siteID);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Entity.TakeAwaySetting> WSDetailTakeAwaySettingAsync(string _id, string siteID) {
            return base.Channel.WSDetailTakeAwaySettingAsync(_id, siteID);
        }
        
        public int WSUpdateTakeAwaySetting(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID) {
            return base.Channel.WSUpdateTakeAwaySetting(takeAwaySetting, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSUpdateTakeAwaySettingAsync(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID) {
            return base.Channel.WSUpdateTakeAwaySettingAsync(takeAwaySetting, siteID);
        }
        
        public int WSDeleteTakeAwaySetting(string _id, string siteID) {
            return base.Channel.WSDeleteTakeAwaySetting(_id, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSDeleteTakeAwaySettingAsync(string _id, string siteID) {
            return base.Channel.WSDeleteTakeAwaySettingAsync(_id, siteID);
        }
        
        public Com.SmoovPOS.Entity.TakeAwaySetting WSDetailTakeAwaySettingByBranchId(string designDoc, string viewName, string branch_id, string siteID) {
            return base.Channel.WSDetailTakeAwaySettingByBranchId(designDoc, viewName, branch_id, siteID);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Entity.TakeAwaySetting> WSDetailTakeAwaySettingByBranchIdAsync(string designDoc, string viewName, string branch_id, string siteID) {
            return base.Channel.WSDetailTakeAwaySettingByBranchIdAsync(designDoc, viewName, branch_id, siteID);
        }
        
        public Com.SmoovPOS.Entity.TakeAwaySetting[] WSGetAllTakeAwaySetting(string designDoc, string viewName, string siteID) {
            return base.Channel.WSGetAllTakeAwaySetting(designDoc, viewName, siteID);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Entity.TakeAwaySetting[]> WSGetAllTakeAwaySettingAsync(string designDoc, string viewName, string siteID) {
            return base.Channel.WSGetAllTakeAwaySettingAsync(designDoc, viewName, siteID);
        }
    }
}
