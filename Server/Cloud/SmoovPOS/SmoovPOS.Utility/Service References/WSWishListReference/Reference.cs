﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34011
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmoovPOS.Utility.WSWishListReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="WSWishListReference.IWSWishList")]
    public interface IWSWishList {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSWishList/WSCreateWishList", ReplyAction="http://tempuri.org/IWSWishList/WSCreateWishListResponse")]
        int WSCreateWishList(Com.SmoovPOS.Entity.WishList wishlist, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSWishList/WSCreateWishList", ReplyAction="http://tempuri.org/IWSWishList/WSCreateWishListResponse")]
        System.Threading.Tasks.Task<int> WSCreateWishListAsync(Com.SmoovPOS.Entity.WishList wishlist, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSWishList/WSUpdateWishList", ReplyAction="http://tempuri.org/IWSWishList/WSUpdateWishListResponse")]
        int WSUpdateWishList(Com.SmoovPOS.Entity.WishList wishlist, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSWishList/WSUpdateWishList", ReplyAction="http://tempuri.org/IWSWishList/WSUpdateWishListResponse")]
        System.Threading.Tasks.Task<int> WSUpdateWishListAsync(Com.SmoovPOS.Entity.WishList wishlist, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSWishList/WSDeleteWishList", ReplyAction="http://tempuri.org/IWSWishList/WSDeleteWishListResponse")]
        int WSDeleteWishList(string _id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSWishList/WSDeleteWishList", ReplyAction="http://tempuri.org/IWSWishList/WSDeleteWishListResponse")]
        System.Threading.Tasks.Task<int> WSDeleteWishListAsync(string _id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSWishList/WSGetWishListWithEmail", ReplyAction="http://tempuri.org/IWSWishList/WSGetWishListWithEmailResponse")]
        Com.SmoovPOS.Entity.WishList WSGetWishListWithEmail(string designDoc, string viewName, string email, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSWishList/WSGetWishListWithEmail", ReplyAction="http://tempuri.org/IWSWishList/WSGetWishListWithEmailResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Entity.WishList> WSGetWishListWithEmailAsync(string designDoc, string viewName, string email, string siteID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWSWishListChannel : SmoovPOS.Utility.WSWishListReference.IWSWishList, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WSWishListClient : System.ServiceModel.ClientBase<SmoovPOS.Utility.WSWishListReference.IWSWishList>, SmoovPOS.Utility.WSWishListReference.IWSWishList {
        
        public WSWishListClient() {
        }
        
        public WSWishListClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WSWishListClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSWishListClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSWishListClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int WSCreateWishList(Com.SmoovPOS.Entity.WishList wishlist, string siteID) {
            return base.Channel.WSCreateWishList(wishlist, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSCreateWishListAsync(Com.SmoovPOS.Entity.WishList wishlist, string siteID) {
            return base.Channel.WSCreateWishListAsync(wishlist, siteID);
        }
        
        public int WSUpdateWishList(Com.SmoovPOS.Entity.WishList wishlist, string siteID) {
            return base.Channel.WSUpdateWishList(wishlist, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSUpdateWishListAsync(Com.SmoovPOS.Entity.WishList wishlist, string siteID) {
            return base.Channel.WSUpdateWishListAsync(wishlist, siteID);
        }
        
        public int WSDeleteWishList(string _id, string siteID) {
            return base.Channel.WSDeleteWishList(_id, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSDeleteWishListAsync(string _id, string siteID) {
            return base.Channel.WSDeleteWishListAsync(_id, siteID);
        }
        
        public Com.SmoovPOS.Entity.WishList WSGetWishListWithEmail(string designDoc, string viewName, string email, string siteID) {
            return base.Channel.WSGetWishListWithEmail(designDoc, viewName, email, siteID);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Entity.WishList> WSGetWishListWithEmailAsync(string designDoc, string viewName, string email, string siteID) {
            return base.Channel.WSGetWishListWithEmailAsync(designDoc, viewName, email, siteID);
        }
    }
}
