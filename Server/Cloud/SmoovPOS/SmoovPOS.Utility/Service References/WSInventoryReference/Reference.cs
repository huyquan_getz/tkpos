﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34011
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmoovPOS.Utility.WSInventoryReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="WSInventoryReference.IWSInventory")]
    public interface IWSInventory {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSCreateInventory", ReplyAction="http://tempuri.org/IWSInventory/WSCreateInventoryResponse")]
        int WSCreateInventory(Com.SmoovPOS.Entity.Inventory inventory, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSCreateInventory", ReplyAction="http://tempuri.org/IWSInventory/WSCreateInventoryResponse")]
        System.Threading.Tasks.Task<int> WSCreateInventoryAsync(Com.SmoovPOS.Entity.Inventory inventory, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSGetAll", ReplyAction="http://tempuri.org/IWSInventory/WSGetAllResponse")]
        Com.SmoovPOS.Entity.Inventory[] WSGetAll(string designDoc, string viewName, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSGetAll", ReplyAction="http://tempuri.org/IWSInventory/WSGetAllResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Inventory[]> WSGetAllAsync(string designDoc, string viewName, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSGetAllInventory", ReplyAction="http://tempuri.org/IWSInventory/WSGetAllInventoryResponse")]
        SmoovPOS.Entity.Models.SearchInventoryModel WSGetAllInventory(string designDoc, string viewName, string siteID, SmoovPOS.Entity.NewPaging paging);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSGetAllInventory", ReplyAction="http://tempuri.org/IWSInventory/WSGetAllInventoryResponse")]
        System.Threading.Tasks.Task<SmoovPOS.Entity.Models.SearchInventoryModel> WSGetAllInventoryAsync(string designDoc, string viewName, string siteID, SmoovPOS.Entity.NewPaging paging);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSGetAllCountry", ReplyAction="http://tempuri.org/IWSInventory/WSGetAllCountryResponse")]
        Com.SmoovPOS.Entity.Country[] WSGetAllCountry(string designDoc, string viewName, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSGetAllCountry", ReplyAction="http://tempuri.org/IWSInventory/WSGetAllCountryResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Country[]> WSGetAllCountryAsync(string designDoc, string viewName, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSGetAllActiveBranchesForTableOrder", ReplyAction="http://tempuri.org/IWSInventory/WSGetAllActiveBranchesForTableOrderResponse")]
        Com.SmoovPOS.Entity.Inventory[] WSGetAllActiveBranchesForTableOrder(string designDoc, string viewName, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSGetAllActiveBranchesForTableOrder", ReplyAction="http://tempuri.org/IWSInventory/WSGetAllActiveBranchesForTableOrderResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Inventory[]> WSGetAllActiveBranchesForTableOrderAsync(string designDoc, string viewName, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSCountAllInventory", ReplyAction="http://tempuri.org/IWSInventory/WSCountAllInventoryResponse")]
        int WSCountAllInventory(string designDoc, string viewName, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSCountAllInventory", ReplyAction="http://tempuri.org/IWSInventory/WSCountAllInventoryResponse")]
        System.Threading.Tasks.Task<int> WSCountAllInventoryAsync(string designDoc, string viewName, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSDetailInventory", ReplyAction="http://tempuri.org/IWSInventory/WSDetailInventoryResponse")]
        Com.SmoovPOS.Entity.Inventory WSDetailInventory(string _id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSDetailInventory", ReplyAction="http://tempuri.org/IWSInventory/WSDetailInventoryResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Inventory> WSDetailInventoryAsync(string _id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSDetailInventoryByIndex", ReplyAction="http://tempuri.org/IWSInventory/WSDetailInventoryByIndexResponse")]
        Com.SmoovPOS.Entity.Inventory WSDetailInventoryByIndex(string designDoc, string viewName, string siteID, int BranchIndex);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSDetailInventoryByIndex", ReplyAction="http://tempuri.org/IWSInventory/WSDetailInventoryByIndexResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Inventory> WSDetailInventoryByIndexAsync(string designDoc, string viewName, string siteID, int BranchIndex);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSUpdateInventory", ReplyAction="http://tempuri.org/IWSInventory/WSUpdateInventoryResponse")]
        int WSUpdateInventory(Com.SmoovPOS.Entity.Inventory inventory, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSUpdateInventory", ReplyAction="http://tempuri.org/IWSInventory/WSUpdateInventoryResponse")]
        System.Threading.Tasks.Task<int> WSUpdateInventoryAsync(Com.SmoovPOS.Entity.Inventory inventory, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSDeleteInventory", ReplyAction="http://tempuri.org/IWSInventory/WSDeleteInventoryResponse")]
        int WSDeleteInventory(string _id, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSInventory/WSDeleteInventory", ReplyAction="http://tempuri.org/IWSInventory/WSDeleteInventoryResponse")]
        System.Threading.Tasks.Task<int> WSDeleteInventoryAsync(string _id, string siteID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWSInventoryChannel : SmoovPOS.Utility.WSInventoryReference.IWSInventory, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WSInventoryClient : System.ServiceModel.ClientBase<SmoovPOS.Utility.WSInventoryReference.IWSInventory>, SmoovPOS.Utility.WSInventoryReference.IWSInventory {
        
        public WSInventoryClient() {
        }
        
        public WSInventoryClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WSInventoryClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSInventoryClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSInventoryClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int WSCreateInventory(Com.SmoovPOS.Entity.Inventory inventory, string siteID) {
            return base.Channel.WSCreateInventory(inventory, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSCreateInventoryAsync(Com.SmoovPOS.Entity.Inventory inventory, string siteID) {
            return base.Channel.WSCreateInventoryAsync(inventory, siteID);
        }
        
        public Com.SmoovPOS.Entity.Inventory[] WSGetAll(string designDoc, string viewName, string siteID) {
            return base.Channel.WSGetAll(designDoc, viewName, siteID);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Inventory[]> WSGetAllAsync(string designDoc, string viewName, string siteID) {
            return base.Channel.WSGetAllAsync(designDoc, viewName, siteID);
        }
        
        public SmoovPOS.Entity.Models.SearchInventoryModel WSGetAllInventory(string designDoc, string viewName, string siteID, SmoovPOS.Entity.NewPaging paging) {
            return base.Channel.WSGetAllInventory(designDoc, viewName, siteID, paging);
        }
        
        public System.Threading.Tasks.Task<SmoovPOS.Entity.Models.SearchInventoryModel> WSGetAllInventoryAsync(string designDoc, string viewName, string siteID, SmoovPOS.Entity.NewPaging paging) {
            return base.Channel.WSGetAllInventoryAsync(designDoc, viewName, siteID, paging);
        }
        
        public Com.SmoovPOS.Entity.Country[] WSGetAllCountry(string designDoc, string viewName, string siteID) {
            return base.Channel.WSGetAllCountry(designDoc, viewName, siteID);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Country[]> WSGetAllCountryAsync(string designDoc, string viewName, string siteID) {
            return base.Channel.WSGetAllCountryAsync(designDoc, viewName, siteID);
        }
        
        public Com.SmoovPOS.Entity.Inventory[] WSGetAllActiveBranchesForTableOrder(string designDoc, string viewName, string siteID) {
            return base.Channel.WSGetAllActiveBranchesForTableOrder(designDoc, viewName, siteID);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Inventory[]> WSGetAllActiveBranchesForTableOrderAsync(string designDoc, string viewName, string siteID) {
            return base.Channel.WSGetAllActiveBranchesForTableOrderAsync(designDoc, viewName, siteID);
        }
        
        public int WSCountAllInventory(string designDoc, string viewName, string siteID) {
            return base.Channel.WSCountAllInventory(designDoc, viewName, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSCountAllInventoryAsync(string designDoc, string viewName, string siteID) {
            return base.Channel.WSCountAllInventoryAsync(designDoc, viewName, siteID);
        }
        
        public Com.SmoovPOS.Entity.Inventory WSDetailInventory(string _id, string siteID) {
            return base.Channel.WSDetailInventory(_id, siteID);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Inventory> WSDetailInventoryAsync(string _id, string siteID) {
            return base.Channel.WSDetailInventoryAsync(_id, siteID);
        }
        
        public Com.SmoovPOS.Entity.Inventory WSDetailInventoryByIndex(string designDoc, string viewName, string siteID, int BranchIndex) {
            return base.Channel.WSDetailInventoryByIndex(designDoc, viewName, siteID, BranchIndex);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Entity.Inventory> WSDetailInventoryByIndexAsync(string designDoc, string viewName, string siteID, int BranchIndex) {
            return base.Channel.WSDetailInventoryByIndexAsync(designDoc, viewName, siteID, BranchIndex);
        }
        
        public int WSUpdateInventory(Com.SmoovPOS.Entity.Inventory inventory, string siteID) {
            return base.Channel.WSUpdateInventory(inventory, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSUpdateInventoryAsync(Com.SmoovPOS.Entity.Inventory inventory, string siteID) {
            return base.Channel.WSUpdateInventoryAsync(inventory, siteID);
        }
        
        public int WSDeleteInventory(string _id, string siteID) {
            return base.Channel.WSDeleteInventory(_id, siteID);
        }
        
        public System.Threading.Tasks.Task<int> WSDeleteInventoryAsync(string _id, string siteID) {
            return base.Channel.WSDeleteInventoryAsync(_id, siteID);
        }
    }
}
