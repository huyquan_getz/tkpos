﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace SmoovPOS.Utility.CustomHTMLHelper
{
    public class StringHelper
    {
        /// <summary>
        /// Trims the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        public static void Trim(object obj)
        {
            Trim(obj, new string[] { });
        }


        /// <summary>
        /// Trims the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="excludedList">The excluded list.</param>
        public static void Trim(object obj, string[] excludedList)
        {
            var type = obj.GetType();
            var propertyInfos = type.GetProperties().Where(
                it => it.CanRead && it.CanWrite && it.PropertyType == (typeof(string)))
                .Where(it => !excludedList.Contains(it.Name));
            foreach (var propertyInfo in propertyInfos)
            {
                var s = propertyInfo.GetValue(obj, null) as string;
                if (!string.IsNullOrEmpty(s))
                {
                    propertyInfo.SetValue(obj, s.Trim(), null);
                }
            }
        }

        public static void HtmlEncode(object obj, string[] skippedProperties)
        {
            if (obj == null) return;

            var type = obj.GetType();
            var propertyList =
                type.GetProperties().Where(it => it.CanRead && it.CanWrite && it.PropertyType == typeof(string))
                    .Where(it => !skippedProperties.Contains(it.Name)).ToList();
            foreach (var prop in propertyList)
            {
                var rawValue = (string)prop.GetValue(obj, null);
                var encodedValue = HttpUtility.HtmlEncode(rawValue);
                prop.SetValue(obj, encodedValue, null);
            }
        }

        public static string HexConverter(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

    }
}