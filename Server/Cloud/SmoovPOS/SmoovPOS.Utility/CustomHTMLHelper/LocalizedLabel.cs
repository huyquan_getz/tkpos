﻿using SmoovPOS.Entity.BaseEntity;
using System.Web.Mvc;

namespace SmoovPOS.Utility.CustomHTMLHelper
{
    public static class LocalizedLabelControl
    {
        public static MvcHtmlString LocalizedLabel(this HtmlHelper html, string label)
        {
            return MvcHtmlString.Create(DependencyResolver.Current.GetService<ILanguageManager>().GetLabelText(label));
        }

        public static MvcHtmlString LocalizedMessage(this HtmlHelper html, string message)
        {
            return MvcHtmlString.Create(DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText(message));
        }

        public static string LocalizedButton(this HtmlHelper html, string buttonLabel)
        {
            return DependencyResolver.Current.GetService<ILanguageManager>().GetButtonText(buttonLabel);
        }
    }
}