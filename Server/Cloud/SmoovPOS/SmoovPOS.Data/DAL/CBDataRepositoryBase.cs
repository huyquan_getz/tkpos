﻿using Com.SmoovPOS.Data.DAL;
using Com.SmoovPOS.Entity.CBEntity;
using Couchbase;
using Couchbase.Configuration;
using Couchbase.Management;
using Couchbase.Operations;
using Enyim.Caching.Memcached;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Com.SmoovPOS.Data.Repository
{
    /// <summary>
    /// Cache the couchbase object
    /// </summary>
    /// <createdby>nhu.dang</createdby>
    /// <createddate>12/9/2014-1:44 PM</createddate>
    public class CouchBaseCache
    {
        private static Dictionary<string, CouchbaseClient> cacheList;
        public static Dictionary<string, CouchbaseClient> CacheList
        {
            get
            {
                if (cacheList == null)
                    cacheList = new Dictionary<string, CouchbaseClient>();
                return cacheList;
            }
        }

        public static void RemoveCache(string key)
        {
            if (cacheList != null)
            {
                cacheList.Remove(key);
            }
        }
    }
    public abstract class CBCountDataRepositoryBase<T> where T : struct
    {
        protected CouchbaseClient _Client { get; set; }
        public void SetClientCouchbase(string bucket)
        {
            //var bucketASection = (CouchbaseClientSection) ConfigurationManager.GetSection("couchbase/bucket-a");

            // TODO: check app.config for replication of this in-code config
            if (!string.IsNullOrEmpty(bucket))
            {
                if (!CouchBaseCache.CacheList.ContainsKey(bucket))
                {
                    var cbConfig = new CouchbaseClientConfiguration();
                    cbConfig.Urls.Add(new Uri(ConfigureKeys.CouchBaseServer));
                    cbConfig.Bucket = bucket;
                    CouchBaseCache.CacheList.Add(bucket, new CouchbaseClient(cbConfig));
                }
                _Client = CouchBaseCache.CacheList[bucket];
                //TODO: remove couchbase cache after logout
            }

        }
        protected virtual IView<T> GetView(string _designDoc, string viewName)
        {
            var view = _Client.GetView<T>(_designDoc, viewName).Stale(StaleMode.False);
            return view;
        }
    }
    public abstract class CBDataRepositoryBase<T> where T : CBBaseEntity
    {
        // private readonly string _designDoc;
        protected CouchbaseClient _Client { get; set; }
        // protected string bucket  { get; set; }
        /// <summary>
        /// Initializes the <see cref="CBDataRepositoryBase{T}"/> class.
        /// </summary>
        public void SetClientCouchbase(string bucket)
        {
            //var bucketASection = (CouchbaseClientSection) ConfigurationManager.GetSection("couchbase/bucket-a");

            // TODO: check app.config for replication of this in-code config
            if (!string.IsNullOrEmpty(bucket))
            {
                if (!CouchBaseCache.CacheList.ContainsKey(bucket))
                {
                    var cbConfig = new CouchbaseClientConfiguration();
                    cbConfig.Urls.Add(new Uri(ConfigureKeys.CouchBaseServer));
                    cbConfig.Bucket = bucket;

                    //if (CouchBaseCache.CacheList[bucket] == null)
                    //{
                    CouchBaseCache.CacheList.Add(bucket, new CouchbaseClient(cbConfig));
                    //}

                }
                _Client = CouchBaseCache.CacheList[bucket];
                //TODO: remove couchbase cache after logout
            }

        }

        //public CBDataRepositoryBase(string bucket = "smoovpos")
        //{
        //    //var bucketASection = (CouchbaseClientSection) ConfigurationManager.GetSection("couchbase/bucket-a");

        //    // TODO: check app.config for replication of this in-code config
        //    if (!CouchBaseCache.CacheList.ContainsKey(bucket))
        //    {
        //        var cbConfig = new CouchbaseClientConfiguration();
        //        cbConfig.Urls.Add(new Uri(ConfigureKeys.CouchBaseServer));
        //        cbConfig.Bucket = bucket;
        //        CouchBaseCache.CacheList.Add(bucket, new CouchbaseClient(cbConfig));
        //    }
        //    _Client = CouchBaseCache.CacheList[bucket];
        //    //TODO: remove couchbase cache after logout

        //    //_designDoc = typeof(T).Name.ToLower().InflectTo().Pluralized;
        //}

        //build key
        protected virtual string BuildKey(T model)
        {
            // we are choosing model.Id
            // so if its Id is "Hello ABC 12" => key = "hello_abc_12"
            // each derivered repository will override this if it require the generated key base on the differ model property
            if (string.IsNullOrEmpty(model._id))
            {
                return Guid.NewGuid().ToString();
            }
            return model._id.InflectTo().Underscored;
        }

        //Id is not serialized into the JSON document on store
        private string serializeAndIgnoreId(T obj)
        {
            var json = JsonConvert.SerializeObject(obj,
                new JsonSerializerSettings()
                {
                    ContractResolver = new DocumentIdContractResolver(),
                });
            return json;
        }

        private class DocumentIdContractResolver : CamelCasePropertyNamesContractResolver
        {
            protected override List<MemberInfo> GetSerializableMembers(Type objectType)
            {
                return base.GetSerializableMembers(objectType).Where(o => o.Name != "id").ToList();
            }
        }

        // CRUD and misc
        protected virtual IView<T> GetViewBucket(string _designDoc, string viewName, string bucket)
        {
            var view = _Client.GetView<T>(_designDoc, viewName, true).Stale(StaleMode.False).Key(bucket);
            return view;
        }
        protected virtual IView<T> GetView(string _designDoc, string viewName)
        {
            var view = _Client.GetView<T>(_designDoc, viewName, true).Stale(StaleMode.False);
            return view;
        }

        protected virtual IView<T> GetViewByLimit(string _designDoc, string viewName, int limit = 0)
        {
            var view = _Client.GetView<T>(_designDoc, viewName, true).Stale(StaleMode.False);
            if (limit > 0) view.Limit(limit);
            return view;
        }

        public virtual IEnumerable<T> GetAllByView(string _designDoc, string viewName, int limit = 0)
        {
            var view = _Client.GetView<T>(_designDoc, viewName, true).Stale(StaleMode.False);
            if (limit > 0) view.Limit(limit);
            return view;
        }

        protected virtual int Create(T value, PersistTo persistTo = PersistTo.Zero)
        {
            var result = _Client.ExecuteStore(StoreMode.Add, value._id, serializeAndIgnoreId(value), persistTo);
            if (result.Exception != null) throw result.Exception;
            return result.StatusCode.Value;
        }
        protected virtual string Create_(T value, PersistTo persistTo = PersistTo.Zero)
        {
            var result = _Client.ExecuteStore(StoreMode.Add, value._id, serializeAndIgnoreId(value), persistTo);
            if (result.Exception != null) throw result.Exception;
            return value._id;
        }
        protected virtual int Update(T value, PersistTo persistTo = PersistTo.Zero)
        {
            var result = _Client.ExecuteStore(StoreMode.Replace, value._id, serializeAndIgnoreId(value), persistTo);
            if (result.Exception != null) throw result.Exception;
            return result.StatusCode.Value;
        }

        protected virtual int Save(T value, PersistTo persistTo = PersistTo.Zero)
        {
            var key = string.IsNullOrEmpty(value._id) ? BuildKey(value) : value._id;
            var result = _Client.ExecuteStore(StoreMode.Set, key, serializeAndIgnoreId(value), persistTo);
            if (result.Exception != null) throw result.Exception;
            return result.StatusCode.Value;
        }

        protected virtual T Get(string key)
        {
            var result = _Client.ExecuteGet<string>(key);
            if (result.Exception != null) throw result.Exception;

            if (result.Value == null)
            {
                return null;
            }

            var model = JsonConvert.DeserializeObject<T>(result.Value);
            model._id = key; //Id is not serialized into the JSON document on store, so need to set it before returning
            return model;
        }

        protected virtual int Delete(string key, PersistTo persistTo = PersistTo.Zero)
        {
            var result = _Client.ExecuteRemove(key, persistTo);
            if (result.Exception != null) throw result.Exception;
            return result.StatusCode.HasValue ? result.StatusCode.Value : 0;
        }

        // View manipulations
        //protected IView<IViewRow> GetViewRaw(string name)
        //{
        //    return _Client.GetView(_designDoc, name).Stale(StaleMode.False);
        //}

        /// <summary>
        /// Create bucket for couchbase
        /// </summary>
        /// <param></param>
        /// <returns>1</returns>
        protected int CreateBucketCouchBase(string bucket, int ProxyPort)
        {
            var config = new CouchbaseClientConfiguration();
            config.Urls.Add(new Uri(ConfigureKeys.CouchBaseServer));
            config.Username = ConfigureKeys.UserCouchBase;
            config.Password = ConfigureKeys.PasswordCouchBase;
            var cluster = new CouchbaseCluster(config);
            //create an unauthenticated Couchbase bucket
            try
            {
                cluster.CreateBucket(
                   new Bucket
                   {
                       Name = bucket,
                       AuthType = AuthTypes.None,
                       BucketType = BucketTypes.Membase,
                       Quota = new Quota { RAM = 1000 },
                       ProxyPort = ProxyPort,
                       ReplicaNumber = ReplicaNumbers.Zero
                   });
                return 1;
            }
            catch
            {
                return 0;
            }

        }
        protected bool CheckExistBucketCouchBase(string bucket, int ProxyPort)
        {
            var config = new CouchbaseClientConfiguration();
            config.Urls.Add(new Uri(ConfigureKeys.CouchBaseServer));
            config.Username = ConfigureKeys.UserCouchBase;
            config.Password = ConfigureKeys.PasswordCouchBase;
            var cluster = new CouchbaseCluster(config);
            //create an unauthenticated Couchbase bucket
            try
            {
                Bucket b = new Bucket();
                return cluster.TryGetBucket(bucket, out b);
            }
            catch
            {
                return false;
            }

        }
        /// <summary>
        /// Auto generate documents couchbase
        /// </summary>
        /// <param></param>
        /// <returns>1</returns>
        protected int GenerateDocument(string bucket)
        {
            string key = Guid.NewGuid().ToString();
            var view = @"{
                ""table"": ""Country"",
                ""arrayCountry"": [""Singapore"",""Philippine"",""Malaysia"",""Myanmar"",""VietNam""],
                ""currency"": """",
                ""_id"": ""replaceID"",
                ""createdDate"": ""2014-12-19T00:00:00+07:00"",
                ""modifiedDate"": ""2014-12-19T00:00:00+07:00"",
                ""channels"": [""server""],
                ""bucket"": ""replace""
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();
            view = @"{
                ""index"": 0,
                ""businessID"": ""Master"",
                ""description"": ""Default"",
                ""contact"": ""Default"",
                ""streetAddress"": """",           
                ""country"": ""Viet Nam"",
                ""state"": ""84000"",
                ""zipCode"": ""05110"",
                ""timeZone"": ""GMT (UTC)"",
                ""display"": true,
                ""status"": true,
                ""table"": ""Inventory"",
                ""_id"": ""replaceID"",
                ""createdDate"": ""2014-12-19T00:00:00+07:00"",
                ""modifiedDate"": ""2014-12-19T00:00:00+07:00"",
                ""userID"": """",
                ""userOwner"": """",
                ""channels"": [""server""],
                ""bucket"": ""replace""
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
                ""index"": 1,
                ""businessID"": ""Online store"",
                ""description"": ""Default"",
                ""contact"": ""Default"",
                ""streetAddress"": """",           
                ""country"": ""Viet Nam"",
                ""state"": ""84000"",
                ""zipCode"": ""05110"",
                ""timeZone"": ""GMT (UTC)"",
                ""display"": true,
                ""status"": true,
                ""table"": ""Inventory"",
                ""_id"": ""replaceID"",
                ""createdDate"": ""2014-12-19T00:00:00+07:00"",
                ""modifiedDate"": ""2014-12-19T00:00:00+07:00"",
                ""userID"": """",
                ""userOwner"": """",
                ""channels"": [""server""],
                ""bucket"": ""replace""
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
                ""table"": ""Instruction"",
                ""name"": ""For self collection, please visit our shop at 2 Orchard Link, Scape #02-07, Singapore 237978 between"",
                ""_id"": ""replaceID"",
                ""createdDate"": ""2014-12-19T00:00:00+07:00"",
                ""modifiedDate"": ""2014-12-19T00:00:00+07:00"",
                ""userID"": ""001"",
                ""userOwner"": ""Merchant_001"",
                ""display"": false,
                ""status"": false,
                ""channels"": [""server""],
                ""dateTimeInt"":""1418981589.72813"",
                ""bucket"": ""replace""    
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
                ""table"": ""ServiceShipping"",
                ""deliveryService"": ""Express"",
                ""shippingFee"": 20,
                ""timeEstimate"": ""1 - 2 Business Days"",
                ""_id"": ""replaceID"",
                ""createdDate"": ""2014-12-19T00:00:00+07:00"",
                ""modifiedDate"": ""2014-12-19T00:00:00+07:00"",
                ""userID"": ""001"",
                ""userOwner"": ""Merchant_001"",
                ""display"": false,
                ""status"": false,
                ""channels"": [""server""] ,
                ""dateTimeInt"":""1418981589.72813"",
                ""bucket"": ""replace""    
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();


            view = @"{
                ""table"": ""WebContent"",
                ""aboutUs"": """",
                ""contactUs"": """",
                ""shippingInformation"": """",
                ""returnAndExchange"": """",
                ""privacyPolicy"": """",
                ""termAndCondition"": """",
                ""logo"": """",
                ""slideImage"": [],
                ""socialNetworkLink"" : 
                 {
                     ""facebook"" : """",
                     ""twitter"" : """",
                     ""youtube"" : """",
                     ""googlePlus"" : """"
                    }    
                ,
                ""theme"": ""theme1"",
                ""primarySiteAlias"": """",
                ""_id"": ""replaceID"",
                ""createdDate"": ""2014-12-30T00:00:00+08:00"",
                ""modifiedDate"": ""2014-12-30T00:00:00+08:00"",
                ""userID"": """",
                ""userOwner"": """",
                ""display"": false,
                ""status"": false,
                ""channels"": [""server""] ,
                ""dateTimeInt"":""1418981589.72813"",
                ""bucket"": ""replace""    
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
                ""table"": ""MerchantGeneralSetting"",
                ""timeZoneFullName"": ""Singapore Standard Time"",
                ""timeZone"": ""GMT (UTC+8:00)"",
                ""currencyFullName"": ""Singapore Dollar"",
                ""currency"": ""SGD"",
                ""gst"": """",
                ""countOfBestSellerProduct"": """",
                ""DailyReportPeriodBeginFrom"": ""00:00"",
                ""DailyReportPeriodEndAt"": ""23:00"",
                ""isDailyReportPeriodEndAtNextDay"": false,
                ""_id"": ""replaceID"",
                ""createdDate"": ""2015-01-07T15:49:02.4604283+07:00"",
                ""modifiedDate"": ""2015-01-07T15:49:02.4604283+07:00"",
                ""userID"":""001"" ,
                ""userOwner"":""merchant_001"", 
                ""status"": true, 
                ""display"": true, 
                ""channels"": [""server"",""app""] ,
                ""dateTimeInt"":"""" ,
                ""bucket"": ""replace""
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
                ""table"": ""Currency"",
                ""listCurrency"": [{""country"": ""United States of America"",""currencyFullName"": ""United States Dollar"",""currency"": ""USD"",""icon"": """"},{""country"": ""Singapore"",""currencyFullName"": ""Singapore Dollar"",""currency"": ""SGD"",""icon"": """"},{""country"": ""Viet Nam"",""currencyFullName"": ""Viet Nam Dong"",""currency"": ""VND"",""icon"": """"}],
                ""_id"": ""replaceID"",
                ""createdDate"": ""2015-01-07T15:49:02.4604283+07:00"",
                ""modifiedDate"": ""2015-01-07T15:49:02.4604283+07:00"",
                ""userID"":"""" ,
                ""userOwner"":"""", 
                ""status"": false, 
                ""display"": false, 
                ""channels"": [""server""] ,
                ""dateTimeInt"":"""" ,
                ""bucket"": ""replace""
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
                ""table"": ""Setting"",
                ""urlHostOnline"": ""http:///store1.demo.smoovpos-dev.com"",
                ""updateProfile"": false,
                ""createBranch"": false,
                ""createCategory"":false ,
                ""addProduct"":false, 
                ""distribute"": false, 
                ""_id"": ""replaceID"", 
                ""createdDate"": ""2015-01-16T12:11:15.5841091Z"" ,
                ""modifiedDate"":""2015-01-16T12:11:15.5841091Z"" ,
                ""userID"":"""" ,
                ""userOwner"":"""" ,
                ""status"":false ,
                ""display"":false ,
                ""channels"":[""server""],
                ""dateTimeInt"":"""" ,
                ""bucket"": ""replace""
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();


            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 0,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Verify Account"",
          ""body"": ""\r\n<div>\r\n    <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfCustomer],\r\n\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Welcome to [BusinessName]. In order to use this website, you need to activate your account. Please click the following link to complete the process.\r\n  </div>\r\n  <div>\r\n    <a href='[OnlineshopLink]'>[OnlineshopLink]</a>  \r\n  </div>\r\n  <div>\r\n\r\n  <div style='margin-top:10px;'>\r\n    If you did not request to have your email verified you can safely ignore this email.\r\n  </div>\r\n  <div style='margin-top:30px;'>\r\n    Thank you,\r\n  </div>\r\n  <div>\r\n    [BusinessName]\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
         ""bucket"": ""replace""
            }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 1,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Register Complete"",
          ""body"": ""\r\n\r\n<div>\r\n    <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfCustomer],\r\n\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Thank you for signup with [BusinessName]! We're excited to have you join us and start shopping. We look forward to serve you.\r\n  </div> \r\n  <div>\r\n  <div style='margin-top:10px;'>\r\n    Visit us again at: <a href='[OnlineshopLink]'> [OnlineshopLink]</a>\r\n  </div>\r\n  <div style='margin-top:30px;'>\r\n    Sincerely,\r\n  </div>\r\n  <div>\r\n    [BusinessName]\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
          ""bucket"": ""replace""
            }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 2,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Forgot Password"",
          ""body"": ""\r\n\r\n<div>\r\n    <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfCustomer],\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  We have received your request to reset your password.\r\n  </div>\r\n  <div>\r\n  Please click on the following link to continue that process. This link can only be used once.\r\n  </div>\r\n  <div>\r\n  <a href='[LinkResetPasswork]'>[LinkResetPasswork]</a>\r\n  </div>\r\n  <div>\r\n  If you have any questions or need additional assistance with your account, please contact us.\r\n  </div>\r\n  <div>  \r\n  <div style='margin-top:20px;'>\r\n    Sincerely,\r\n  </div>\r\n  <div>\r\n    [BusinessName]\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
          ""bucket"": ""replace""
            }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 3,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Customer Delivery Order"",
          ""body"": ""\r\n<div>\r\n  <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfCustomer],\r\n\r\n  \r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  We would like to thank you for using [BusinessName]. Below is a summary of your order. Please keep this mail for future reference.\r\n\r\n  </div>\r\n  <div>\r\n   \r\n  If you have any questions or need additional assistance with your account,please contact us.\r\n  </div>\r\n  </div>\r\n  <div style='margin-top:50px;margin-left:50px;'>\r\n  <div>\r\n  <label>\r\n  Your order number is [OrderNumber], placed  [OrderDay], status \r\n  </label>\r\n  <b>[OrderStatusOfCustomer].</b>\r\n  </div>\r\n  <div>\r\n  <div>\r\n  <!-- Bill to ship to-->\r\n  <table style='width:50%;'>\r\n    <tr>\r\n  <td><p><b>Bill to:</b></p></td>\r\n  <td><p><b>[OrderTypeInformation]</b></p></td>\r\n    </tr>\r\n    <tr style='margin-top:10px;'>\r\n  <td>\r\n  [FirstNameLastNameBill] \r\n  </td>\r\n  <td>\r\n  [FirstNameLastNameReceive]\r\n  </td>\r\n    </tr>\r\n    <tr>\r\n  <td>\r\n  [AddressBill]\r\n  </td>\r\n  <td>\r\n  [AddressReceive]\r\n  </td>\r\n    </tr>\r\n    <tr>\r\n  <td>\r\n  [PhoneBill]\r\n  </td>\r\n  <td>\r\n  [PhoneReceive]\r\n  </td>\r\n    </tr>\r\n    <tr>\r\n  <td>\r\n  [EmailBill]\r\n  </td>\r\n  <td>\r\n  [EmailReceive]\r\n  </td>\r\n    </tr>\r\n\r\n  </table>\r\n  <!-- bill to ship to-->\r\n  <!-- Payment infor-->\r\n \r\n  <div style='margin-top:30px;'>    \r\n    <div style='width:50%'>\r\n  [ProductOrderDetails]\r\n    </div>  \r\n  </div>\r\n  \r\n  </div>\r\n   \r\n  </div>\r\n  </div>\r\n <div>\r\n <div style='margin-top:10px;'>\r\n   Visit us again at: [OnlineshopLink]\r\n </div>\r\n <div style='margin-top:30px;'>\r\n   Sincerely,\r\n </div>\r\n <div>\r\n   [BusinessName]\r\n </div>\r\n </div>\r\n</div>\r\n"",
          ""bucket"": ""replace""
            }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 4,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Customer Self Collect Order"",
          ""body"": ""\r\n<div>\r\n  <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfCustomer],\r\n\r\n  \r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Below is a summary of your order. Please keep this mail for future reference.\r\n\r\n  </div>\r\n  <div>\r\n   \r\n  If you have any questions or need additional assistance with your account, please contact us.\r\n  </div>\r\n  </div>\r\n  <div style='margin-top:50px;margin-left:50px;'>\r\n  <div>\r\n  <label>\r\n  Your order number is [OrderNumber], placed  [OrderDay], status \r\n  </label>\r\n  <b>[OrderStatusOfCustomer].</b>\r\n  </div>\r\n  <div>\r\n  <div>\r\n  <!-- Bill to ship to-->\r\n  <table style='width:50%;'>\r\n    <tr>\r\n  <td><p><b>Bill to:</b></p></td>\r\n  <td><p><b>[OrderTypeInformation]</b></p></td>\r\n    </tr>\r\n    <tr style='margin-top:10px;'>\r\n  <td>\r\n  [FirstNameLastNameBill] \r\n  </td>\r\n  <td>\r\n  [FirstNameLastNameReceive]\r\n  </td>\r\n    </tr>\r\n    <tr>\r\n  <td>\r\n  [AddressBill]\r\n  </td>\r\n  <td>\r\n  [AddressReceive]\r\n  </td>\r\n    </tr>\r\n    <tr>\r\n  <td>\r\n  [PhoneBill]\r\n  </td>\r\n  <td>\r\n  [PhoneReceive]\r\n  </td>\r\n    </tr>\r\n    <tr>\r\n  <td>\r\n  [EmailBill]\r\n  </td>\r\n  <td>\r\n  [EmailReceive]\r\n  </td>\r\n    </tr>\r\n\r\n  </table>\r\n  <!-- bill to ship to-->\r\n  <!-- Payment infor-->\r\n \r\n  <div style='margin-top:30px;'>    \r\n    <div style='width:50%'>\r\n  [ProductOrderDetails]\r\n    </div>  \r\n  </div>\r\n  \r\n  </div>\r\n   \r\n  </div>\r\n  </div>\r\n <div>\r\n <div style='margin-top:10px;'>\r\n   Visit us again at: [OnlineshopLink]\r\n </div>\r\n <div style='margin-top:30px;'>\r\n   Sincerely,\r\n </div>\r\n <div>\r\n   [BusinessName]\r\n </div>\r\n </div>\r\n</div>\r\n"",
            ""bucket"": ""replace""
            }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 5,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Update Status Order"",
           ""body"": ""<div>\r\n    <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfCustomer],\r\n\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Your order number [OrderNumber] has been updated, status <b>[OrderStatusDisplayForCustomer]</b>\r\n\r\n  </div>\r\n  <div>\r\n\r\n  <div style='margin-top:10px;'>\r\n    Visit us again at: [OnlineshopLink]\r\n  </div>\r\n  <div style='margin-top:30px;'>\r\n    Sincerely,\r\n  </div>\r\n  <div>\r\n    [BusinessName]\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
           ""bucket"": ""replace""}";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 6,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Verify Account"",
          ""body"": ""\r\n<div>\r\n    <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfMerchant],\r\n\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Welcome to SmoovPos. In order to login merchant site, you need to activate your account. Please click the following link to complete the process.\r\n  </div>\r\n  <div>\r\n    <a href='[LinkActiveMerchant]'>[LinkActiveMerchant]</a>  \r\n  </div>\r\n  <div>\r\n\r\n  <div style='margin-top:10px;'>\r\n    If you did not request to have your email verified you can safely ignore this email.\r\n  </div>\r\n  <div style='margin-top:30px;'>\r\n    Thank you,\r\n  </div>\r\n  <div>\r\n    SmoovPos Team\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
          ""bucket"": ""replace""
            }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 7,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Register Complete"",
          ""body"": ""\r\n\r\n<div>\r\n   <div><b>Welcome to SmoovPOS</b></div> <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfMerchant],\r\n\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Thank you for signup with SmoovPos! We're excited to have you join us and start shopping. We look forward to serve you.\r\n  </div> \r\n  <div>\r\n  <div style='margin-top:30px;'>\r\n    Thank you,\r\n  </div>\r\n  <div>\r\n    SmoovPos Team\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
            ""bucket"": ""replace""
            }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 8,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Reset Password"",
           ""body"": ""\r\n\r\n<div>\r\n    <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfMerchant],\r\n\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Your account password has been changed successfully. Please contact admin system for more information.\r\n  </div> \r\n  <div>\r\n  <div style='margin-top:30px;'>\r\n    Thank you,\r\n  </div>\r\n  <div>\r\n    SmoovPos Team\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
           ""bucket"": ""replace""
            }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 9,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Cancel Order"",
           ""body"": ""\r\n<div>\r\n    <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfCustomer],\r\n\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Your order #[OrderNumber] has been canceled. \r\n  </div> \r\n <div style='margin-top:10px;'>  Please contact us if you have any question.<div>\r\n  <div>\r\n  Visit us at:  <a href='[OnlineshopLink]'>[OnlineshopLink]</a>  \r\n  </div>\r\n  <div>\r\n\r\n   <div style='margin-top:30px;'>\r\n    Sincerely,\r\n  </div>\r\n  <div>\r\n    [BusinessName]\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
           ""bucket"": ""replace""}";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 10,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Refund Order"",
           ""body"": ""\r\n<div>\r\n    <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfCustomer],\r\n\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Your order #[OrderNumber] has been refunded. Reason: [ReasonOrder].\r\n  </div> \r\n <div style='margin-top:10px;'>  Please contact us if you have any question.<div>\r\n  <div>\r\n  Visit us at:  <a href='[OnlineshopLink]'>[OnlineshopLink]</a>  \r\n  </div>\r\n  <div>\r\n\r\n   <div style='margin-top:30px;'>\r\n    Sincerely,\r\n  </div>\r\n  <div>\r\n    [BusinessName]\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
           ""bucket"": ""replace""}";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
           ""table"": ""TemplateEmail"",
           ""index"": 11,
           ""_id"": ""replaceID"",
           ""createdDate"": ""2014-12-23T00:00:00+08:00"",
           ""modifiedDate"": ""2014-12-23T00:00:00+08:00"",
           ""userID"": ""001"",
           ""userOwner"": ""Merchant_001"",
           ""status"": true,
           ""display"": true,
           ""channels"": [
              ""server""
           ],
           ""dateTimeInt"": ""1419330559.24266"",
           ""name"": ""Cancel Order"",
          ""body"": ""\r\n<div>\r\n    <div id='header-mail'>\r\n  <label>\r\n  Hi [FirstNameOfCustomer],\r\n\r\n  </label>\r\n  <div style='margin-top:10px;'>\r\n  Your request to cancel order #[OrderNumber] has been canceled. \r\n  </div> \r\n <div style='margin-top:10px;'>  Please contact us if you have any question.<div>\r\n  <div>\r\n  Visit us at:  <a href='[OnlineshopLink]'>[OnlineshopLink]</a>  \r\n  </div>\r\n  <div>\r\n\r\n   <div style='margin-top:30px;'>\r\n    Sincerely,\r\n  </div>\r\n  <div>\r\n    [BusinessName]\r\n  </div>\r\n  </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n"",
            ""bucket"": ""replace""}";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            //------------------------- End template email -------------------------

            view = @"{
              
               ""roleName"": ""Staff"",
               ""keyName"": ""Staff"",
               ""description"": ""Staff"",
               ""table"": ""Role"",
               ""siteId"": ""site_001"",
               ""_id"": ""replaceID"",
               ""createdDate"": ""0001-01-01T00:00:00"",
               ""modifiedDate"": ""0001-01-01T00:00:00"",
               ""userID"": ""001"",
               ""userOwner"": ""Merchant_001"",
               ""status"": true,
               ""display"": false,
               ""channels"": [
                   ""server""
               ],
               ""dateTimeInt"": null,
               ""bucket"": ""replace""
   
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();

            view = @"{
              
               ""roleName"": ""Manager"",
               ""keyName"": ""Manager"",
               ""description"": ""Manager"",
               ""table"": ""Role"",
               ""siteId"": ""site_001"",
               ""_id"": ""replaceID"",
               ""createdDate"": ""0001-01-01T00:00:00"",
               ""modifiedDate"": ""0001-01-01T00:00:00"",
               ""userID"": ""001"",
               ""userOwner"": ""Merchant_001"",
               ""status"": true,
               ""display"": false,
               ""channels"": [
                   ""server""
               ],
               ""dateTimeInt"": null,
                ""bucket"": ""replace""
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);
            key = Guid.NewGuid().ToString();


            view = @"{             
              ""typeCheckout"": 0,
              ""acceptSelfCollection"": true,
              ""instructionsSelfCollection"": """",
              ""beginDateSelf"": """",
              ""endDateSelf"": """",
              ""timeSelfCollection"": false,
              ""acceptDelivery"": false,
              ""instructionsDelivery"": """",
              ""timeDelivery"": false,
              ""hourCutOff"": """",
              ""table"": ""CheckoutSetting"",
              ""isPickupPoint"": false,
              ""accountSmoovPay"": null,
              ""_id"": ""replaceID"",
              ""createdDate"": ""2015-03-20T00:00:00+07:00"",
              ""modifiedDate"": ""0001-01-01T00:00:00"",
              ""userID"": null,
              ""userOwner"": null,
              ""status"": false,
              ""display"": false,
              ""channels"": null,
              ""dateTimeInt"": ""63562422943.3101"",
              ""bucket"": ""replace""
                }";
            view = view.Replace("replaceID", key).Replace("replace", bucket);
            _Client.Store(StoreMode.Add, key, view);

            return 1;
        }

        /// <summary>
        /// Auto generate views of documents couchbase
        /// </summary>
        /// <param></param>
        /// <returns>none</returns>
        protected void GenerateViewDocument(string bucket)
        {
            var config = new CouchbaseClientConfiguration();
            config.Urls.Add(new Uri(ConfigureKeys.CouchBaseServer));
            config.Username = ConfigureKeys.UserCouchBase;
            config.Password = ConfigureKeys.PasswordCouchBase;
            var cluster = new CouchbaseCluster(config);
            var json =
                @"{
                  ""views"": {
                    ""get_all_category"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Category\"" && doc.display) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
                }";
            var result = cluster.CreateDesignDocument(bucket, "dev_category", json);
            result = cluster.CreateDesignDocument(bucket, "category", json);

            json =
                 @"{
                 ""views"": {
                    ""get_all_collection"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Collection\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_collection", json);
            result = cluster.CreateDesignDocument(bucket, "collection", json);

            json =
                @"{
                 ""views"": {
                    ""get_all_country"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Country\"") { \r\n\t\t emit(meta.id, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_country", json);
            result = cluster.CreateDesignDocument(bucket, "country", json);

            json =
                @"{
                 ""views"": {
                    ""check_email_exist"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Customer\""  && doc.email) { \r\n\t\t emit([doc.bucket,doc.email], doc); \r\n\t } \r\n }""
                    },
                    ""check_username_password"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Customer\""  && doc.email) { \r\n\t\t emit([doc.bucket, doc.email, doc.password], doc); \r\n\t } \r\n }""
                    },
                   ""check_with_email_and_pinCode"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Customer\"" && doc.email && doc.display) { \r\n\t\t emit([doc.bucket,doc.email,doc.pinCode], doc); \r\n\t } \r\n }""
                    },
                    ""get_all_customer"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Customer\"" && doc.display) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_customer", json);
            result = cluster.CreateDesignDocument(bucket, "customer", json);

            json =
                 @"{
                 ""views"": {
                    ""get_all_delivery_country"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""deliverycountry\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_deliverycountry", json);
            result = cluster.CreateDesignDocument(bucket, "deliverycountry", json);

            json =
                 @"{
                 ""views"": {
                    ""get_all_delivery_setting"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""deliverysetting\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                    ""get_all_delivery_setting_by_country"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""deliverysetting\"") { \r\n\t\t emit([doc.bucket,doc.country._id], doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_deliverysetting", json);
            result = cluster.CreateDesignDocument(bucket, "deliverysetting", json);

            json =
                @"{
                 ""views"": {
                    ""get_all_currency"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Currency\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_developer_setting", json);
            result = cluster.CreateDesignDocument(bucket, "developer_setting", json);

            json =
                @"{
                 ""views"": {
                    ""get_all_discount"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Discount\"" && doc.display) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_discount", json);
            result = cluster.CreateDesignDocument(bucket, "discount", json);

            json =
                @"{
                 ""views"": {
                    ""check_email_exist"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Employee\"" ) { \r\n\t\t emit([doc.bucket,doc.email], doc); \r\n\t } \r\n }""
                    },
                    ""get_all_employee"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Employee\""  && doc.display) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                    ""check_by_email_password"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Employee\"" ) { \r\n\t\t emit([doc.bucket,doc.email,doc.password], doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_employee", json);
            result = cluster.CreateDesignDocument(bucket, "employee", json);

            json =
                @"{
                 ""views"": {
                    ""get_all_inventory"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Inventory\"" && doc.display) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_inventory", json);
            result = cluster.CreateDesignDocument(bucket, "inventory", json);

            json =
                @"{
                 ""views"": {
                    ""get_by_email"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""MyCart\"") { \r\n\t\t emit([doc.bucket, doc.email], doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_mycart", json);
            result = cluster.CreateDesignDocument(bucket, "mycart", json);

            json =
                @"{
                 ""views"": {
                   ""get_all_by_date_active"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\""  && doc.display && doc.status && doc.index==1) { \r\n\t\t emit([doc.bucket, doc.categoryName, doc.dateTimeInt], doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_online", json);
            result = cluster.CreateDesignDocument(bucket, "online", json);

            json =
               @"{
                 ""views"": {
                    ""get_all_instruction"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Instruction\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }"" 
                    },
                    ""get_all_order_delivery"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Order\"" && doc.status && doc.orderType==0 && doc.orderStatus != 7 && doc.orderStatus != 9) { doc._id= meta.id;  if(doc.instruction==null)   { doc.instruction = null;} if(doc.pickUpPoint==null)   { doc.pickUpPoint = null;} emit([doc.bucket, doc.modifiedDate], doc); \r\n\t  }  \r\n\t}"" , ""reduce"":""_count""
                    },
                    ""get_all_order_self_collect"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Order\"" && doc.status && doc.orderType==1 && doc.orderStatus != 7 && doc.orderStatus != 9) {  doc._id= meta.id; if(doc.instruction==null)   { doc.instruction = null;} if(doc.pickUpPoint==null)   { doc.pickUpPoint = null;} emit([doc.bucket, doc.modifiedDate], doc); \r\n\t  }  \r\n\t}"" , ""reduce"":""_count""
                    },
                    ""get_all_order_cancel"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Order\"" && doc.status && (doc.orderStatus == 7 || doc.orderStatus == 9)) {  doc._id= meta.id; if(doc.instruction==null)   { doc.instruction = null;} if(doc.pickUpPoint==null)   { doc.pickUpPoint = null;} emit([doc.bucket, doc.modifiedDate], doc); \r\n\t  }  \r\n\t}"" , ""reduce"":""_count""
                    },
                    ""get_all_order"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Order\"" && doc.status) { doc._id= meta.id; if(doc.instruction==null)   { doc.instruction = null;}  \r\n\t  if(doc.pickUpPoint==null)   { doc.pickUpPoint = null;}\r\n\t emit(doc.bucket, doc);\r\n\t  }  \r\n\t}""
                    },
                    ""get_by_email_sender"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Order\"" && doc.status) { doc._id= meta.id;  if(doc.instruction==null)   { doc.instruction = null;} if(doc.pickUpPoint==null)   { doc.pickUpPoint = null;} emit([doc.bucket, doc.customerSender.email], doc); \r\n\t  }  \r\n\t}""
                    },
                    ""get_all_service_shipping"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ServiceShipping\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                    ""get_all_table_order"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Order\"" && doc.status && doc.orderType==2 && doc.orderStatus != 7 && doc.orderStatus != 9) {  doc._id= meta.id; emit([doc.bucket, doc.modifiedDate], doc); \r\n\t  }  \r\n\t}"" , ""reduce"":""_count""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_order", json);
            result = cluster.CreateDesignDocument(bucket, "order", json);

            json =
               @"{
                 ""views"": {
                    ""check_sku_exists"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Product\"" && doc.title && doc.display) \r\n\t {  \r\n\t\t for(var i=0;i<doc.arrayVarient.length;i++)  \r\n\t\t {  \r\n\t\t emit([doc.bucket,doc.arrayVarient[i].sku.split('-')[0]],doc._id)   \r\n\t\t }  \r\n\t } \r\n }""
                    }                 
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_product", json);
            result = cluster.CreateDesignDocument(bucket, "product", json);

            json =
               @"{
                 ""views"": {
                    ""get_all_productItems"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\"" && doc.display ) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                    ""get_all_by_productID"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\"" && doc.display ) { \r\n\t\t emit([doc.bucket,doc.productID], doc); \r\n\t } \r\n }""
                    },
                   ""get_all_by_category"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\"" && doc.display) { \r\n\t\t emit([doc.bucket,doc.store,doc.categoryName], doc); \r\n\t } \r\n }""
                    },
                    ""get_all_by_category_master"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\""  && doc.store==\""Master\"") { \r\n\t\t emit([doc.bucket,doc.categoryName,doc._id], doc); \r\n\t } \r\n }""
                    },
                   ""get_all_by_master"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\"" && doc.display && doc.store==\""Master\"") { \r\n\t\t emit(doc.store, doc); \r\n\t } \r\n }""
                    },
                    ""get_all_by_store"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\"" && doc.display) { \r\n\t\t emit([doc.bucket,doc.index], doc); \r\n\t } \r\n }""
                    },
                    ""get_all_by_title"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\"" && doc.display) { \r\n\t\t emit([doc.bucket,doc.store,doc.title], doc); \r\n\t } \r\n }""
                    },
                      ""get_all_by_title_master"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\"" && doc.store==\""Master\"") { \r\n\t\t emit([doc.bucket,doc.title], doc); \r\n\t } \r\n }""
                    },
                    ""get_all_productItem_discount"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\""  && doc.display && doc.discount && doc.index==\""1\"" && doc.status ) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                    ""get_all_best_seller"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ProductItem\"" && doc.display && doc.status&& doc.index==1 ) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }"" , ""reduce"":""_count""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_productItem", json);
            result = cluster.CreateDesignDocument(bucket, "productItem", json);

            json =
                 @"{
                 ""views"": {
                    ""get_all_role"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Role\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_role", json);
            result = cluster.CreateDesignDocument(bucket, "role", json);

            json =
               @"{
                 ""views"": {
                    ""get_all_setting"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Setting\""  ) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                   ""get_all_tax"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Tax\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                    ""get_merchant_general_setting"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""MerchantGeneralSetting\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                    ""get_tax_by_store"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Tax\"") \r\n\t {  \r\n\t\t for(var i = 0; i<doc.arrayTax.length;i++)  \r\n\t\t {  \r\n\t\t  if(doc.arrayTax[i].storeInformation) \r\n\t\t { \r\n\t\t\t var arrayStore = doc.arrayTax[i].storeInformation; \r\n\t\t\t\t  for(var j=0; j<arrayStore.length; j++ )  \r\n\t\t\t\t   emit( [doc.bucket,doc.arrayTax[i].storeInformation[j].index], doc);  \r\n\t\t\t }   \r\n\t\t }  \r\n\t } \r\n }""
                    },
                     ""get_all_checkoutSetting"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""CheckoutSetting\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                    ""get_account_smoovpay"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""AccountSmoovPay\"" && doc.display && doc.status) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                    ""get_list_devices"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""ListDevices\"" && doc.display && doc.status) { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_setting_merchant", json);
            result = cluster.CreateDesignDocument(bucket, "setting_merchant", json);

            json =
                @"{
                 ""views"": {
                    ""get_all_stations_by_branch_index"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""Station\"" && doc.display) { \r\n\t\t emit([doc.bucket,doc.branchIndex], doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_station", json);
            result = cluster.CreateDesignDocument(bucket, "station", json);

            json =
                 @"{
                 ""views"": {
                    ""get_all_table_ordering_setting"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""TableOrderingSetting\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    },
                   ""get_all_table_ordering_setting_by_branch"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""TableOrderingSetting\"") { \r\n\t\t emit([doc.bucket,doc.branchId], doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_tableOrdering", json);
            result = cluster.CreateDesignDocument(bucket, "tableOrdering", json);

            json =
                 @"{
                 ""views"": {
                    ""get_all_template_email"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""TemplateEmail\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_template_email", json);
            result = cluster.CreateDesignDocument(bucket, "template_email", json);

            json =
                 @"{
                 ""views"": {
                    ""get_all_webcontent"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""WebContent\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_webcontent", json);
            result = cluster.CreateDesignDocument(bucket, "webcontent", json);

            json =
                 @"{
                 ""views"": {
                    ""get_by_email"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""WishList\"" && doc.display && doc.email) { \r\n\t\t emit([doc.bucket,doc.email], doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_wishlist", json);
            result = cluster.CreateDesignDocument(bucket, "wishlist", json);

            json =
                 @"{
                 ""views"": {
                    ""get_all_sms_setting"": {
                      ""map"": ""function(doc, meta) { \r\n\t if (doc.table == \""SMSSetting\"") { \r\n\t\t emit(doc.bucket, doc); \r\n\t } \r\n }""
                    }
                  }
            }";
            result = cluster.CreateDesignDocument(bucket, "dev_SMSSetting", json);
            result = cluster.CreateDesignDocument(bucket, "SMSSetting", json);

        }
    }
}