﻿using System.Configuration;

namespace Com.SmoovPOS.Data.DAL
{
    /// <summary>
    /// The config key will been defined more clear so read config or database later code
    /// </summary>
    public static class ConfigureKeys
    {
        /// <summary>
        /// Gets connection string, with name in default is "SQLConnection"
        /// </summary>
        public static string ConnectionStringStandard
        {
            get { return ConnectionString("SQLConnection"); }
        }

        /// <summary>
        /// Gets connection string, with name in default is "CouchBaseServer"
        /// </summary>
        public static string CouchBaseServer
        {
            get { return GetKey("CouchBaseServer"); }
        }

        /// <summary>
        /// Get connection string, with name exits config 
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public static string ConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ToString();
        }

        /// <summary>
        /// Get value with key name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string GetKey(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        /// <summary>
        /// Gets connection string, with name in default is "CouchBaseServer"
        /// </summary>
        public static string UserCouchBase
        {
            get { return GetKey("UserCouchBaseServer"); }
        }

        /// <summary>
        /// Gets connection string, with name in default is "CouchBaseServer"
        /// </summary>
        public static string PasswordCouchBase
        {
            get { return GetKey("PasswordCouchBaseServer"); }
        }
    }
}