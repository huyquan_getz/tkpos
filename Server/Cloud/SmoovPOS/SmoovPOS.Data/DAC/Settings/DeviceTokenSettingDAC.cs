﻿using Com.SmoovPOS.Data.Repository;
using Couchbase;
using SmoovPOS.Entity.DeviceToken;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC.Settings
{
    public class DeviceTokenSettingDAC : CBDataRepositoryBase<ManageDevice>
    {
        string siteID = "";
        public DeviceTokenSettingDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public IView<ManageDevice> GetAll(string designDoc, string viewName)
        {
            return GetView(designDoc, viewName);
        }

        public int CreateDAC(ManageDevice device)
        {
            int StatusCode = Create(device);
            return StatusCode;
        }

        public int UpdateDAC(ManageDevice device)
        {
            int StatusCode = Update(device);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }
    }
}