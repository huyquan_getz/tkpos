﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class InstructionDAC : CBDataRepositoryBase<Instruction>
    {
         string siteID = "";
         public InstructionDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public int CreateDAC(Instruction instruction)
        {
            int StatusCode = Create(instruction);
            return StatusCode;
        }
        public int deleteDAC(string id)
        {
            int statuscode = Delete(id);
            return statuscode;
        }

        public int updateDAC(Instruction instruction)
        {
            int statuscode = Update(instruction);
            return statuscode;
        }
        public Instruction detailDAC(string id)
        {
            Instruction instruction = Get(id);
            return instruction;
        }

        public IView<Instruction> GetAll(string designDoc, string viewName)
        {
            IView<Instruction> listInstruction = GetView(designDoc, viewName);
            return listInstruction;
        }
    }
}