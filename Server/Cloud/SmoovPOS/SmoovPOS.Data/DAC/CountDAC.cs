﻿using Com.SmoovPOS.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class CountDAC : CBCountDataRepositoryBase<Int32>
    {
        string siteID = "";
        public CountDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public int GetCount(string designDoc, string viewName, string bucket)
        {
            var view = GetView(designDoc, viewName).Descending(true).StartKey(new string[] { bucket, "uefff" }).EndKey(new string[] { bucket }).Reduce(true);
            try
            {
                var results = view.ToList();
                if (results.Count > 0)
                {
                    return results[0];
                }
            }
            catch 
            {
            }

            return 0;
        }
    }

}