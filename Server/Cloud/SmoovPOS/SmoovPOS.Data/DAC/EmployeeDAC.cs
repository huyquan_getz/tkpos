﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Linq;

namespace Com.SmoovPOS.Data.DAC
{
    public class EmployeeDAC : CBDataRepositoryBase<Employee>
    {
        //string siteID = "";
        public EmployeeDAC(string bucket)
        {
            SetClientCouchbase(bucket);
            //siteID = siteIDParam;
        }
        public int CreateDAC(Employee employee)
        {
            int StatusCode = Create(employee);
            return StatusCode;
        }
        public int deleteDAC(string id)
        {
            int statuscode = Delete(id);
            return statuscode;
        }

        public int updateDAC(Employee employee)
        {
            int statuscode = Update(employee);
            return statuscode;
        }
        public Employee detailDAC(string id)
        {
            Employee employee = Get(id);
            return employee;
        }

        public IView<Employee> GetAll(string designDoc, string viewName)
        {
            IView<Employee> ListEmployee = GetView(designDoc, viewName);
            return ListEmployee;
        }

        public IView<Employee> GetAllWithBucket(string designDoc, string viewName, string bucket)
        {
            IView<Employee> ListEmployee = GetViewBucket(designDoc, viewName, bucket);
            return ListEmployee;
        }

        public int GetNextEmployeeId(string designDoc, string viewName, string bucket)
        {
            IView<Employee> EmployeeList = GetViewBucket(designDoc, viewName, bucket);
            if (EmployeeList == null || EmployeeList.Count() == 0)
                return 1000;
            return EmployeeList.Max(r => r.EmployeeId) + 1;
        }
    }
}