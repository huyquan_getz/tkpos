﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Data.DAC
{
    public class DiscountDAC : CBDataRepositoryBase<Discount>
    {
        string siteID = "";
        public DiscountDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }

        public int CreateDAC(Discount discount)
        {
            int StatusCode = Create(discount);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        public int UpdateDAC(Discount discount)
        {
            int StatusCode = Update(discount);
            return StatusCode;
        }
        public Discount DetailDAC(string id)
        {
            Discount discount = Get(id);
            return discount;
        }

        public IView<Discount> GetAll(string designDoc, string viewName)
        {
            IView<Discount> ListDiscount = GetView(designDoc, viewName);
            return ListDiscount;
        }
    }
}