﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class MerchantSettingDAC : CBDataRepositoryBase<MerchantSetting>
    {   
         string siteID = "";
         public MerchantSettingDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public IView<MerchantSetting> GetAllDAC(string designDoc, string viewName)
        {
            IView<MerchantSetting> ListMerchantSetting = GetView(designDoc, viewName);
            return ListMerchantSetting;
        }

        public int UpdateDAC(MerchantSetting setting)
        {
            int StatusCode = Update(setting);
            return StatusCode;
        }

    }
}