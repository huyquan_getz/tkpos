﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class TaxDAC : CBDataRepositoryBase<Tax>
    {
        public TaxDAC(string bucket)
        {
            SetClientCouchbase(bucket);
        }
        public int CreateDAC(Tax tax)
        {
            Tax tax_ = null;
            try
            {
                tax_ = Get(tax._id);

            }
            catch { }
            if (tax_ != null)
            {
                int statusCode = Update(tax);
                return statusCode;
            }
            else
            {
                int statusCode = Create(tax);
                return statusCode;
            }
        }
        public IView<Tax> GetTax(string designDoc, string viewName)
        {
            IView<Tax> listTax = GetView(designDoc, viewName);
            return listTax;
        }

        public Tax detailDAC(string id)
        {
            Tax tax = Get(id);
            return tax;
        }
    }
}