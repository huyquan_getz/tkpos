﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/30/2015-5:23 PM</datetime>
    public class CollectionDAC : CBDataRepositoryBase<Collection>
    {
        //string siteID = "";
        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionDAC"/> class.
        /// </summary>
        /// <param name="siteIDParam">The site identifier parameter.</param>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public CollectionDAC(string bucket)
        {
            SetClientCouchbase(bucket);
            //siteID = siteIDParam;
        }
        /// <summary>
        /// Creates the dac.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public int CreateDAC(Collection collection)
        {
            int StatusCode = Create(collection);
            return StatusCode;
        }
        /// <summary>
        /// Creates the da c_.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public string CreateDAC_(Collection collection)
        {
            string _id = Create_(collection);
            return _id;
        }
        /// <summary>
        /// Deletes the dac.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        /// <summary>
        /// Updates the dac.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public int UpdateDAC(Collection collection)
        {
            int StatusCode = Update(collection);
            return StatusCode;
        }
        /// <summary>
        /// Details the dac.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public Collection DetailDAC(string id)
        {
            Collection Collection = Get(id);
            return Collection;
        }

        /// <summary>
        /// Gets all collection dac.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public IView<Collection> GetAllCollectionDAC(string designDoc, string viewName, string bucket)
        {
            IView<Collection> ListCollection = GetViewBucket(designDoc, viewName, bucket);
            return ListCollection;
        }

        /// <summary>
        /// Deletes all collection dac.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public int DeleteAllCollectionDAC(string designDoc, string viewName, string bucket)
        {
            IEnumerable<Collection> CollectionList = GetView(designDoc, viewName).Key(bucket);
            foreach (var item in CollectionList.Where(r => r._id != null))
            {
                Delete(item._id);
            }

            return 0;
        }

    }
}