﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class TaxItemDAC : CBDataRepositoryBase<TaxItem>
    {   
        string siteID = "";
        public TaxItemDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public IView<TaxItem> GetTaxItem(string designDoc, string viewName)
        {
            IView<TaxItem> listTax = GetView(designDoc, viewName);
            return listTax;
        }
    }
}