﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class MerchantGeneralSettingDAC : CBDataRepositoryBase<MerchantGeneralSetting>
    {
        //string siteID = "";

        public MerchantGeneralSettingDAC(string bucket)
        {
            SetClientCouchbase(bucket);
            //siteID = siteIDParam;
        }

        public IView<MerchantGeneralSetting> GetAll(string designDoc, string viewName)
        {
            IView<MerchantGeneralSetting> ListMerchantSetting = GetView(designDoc, viewName);
            return ListMerchantSetting;
        }

        public int CreateDAC(MerchantGeneralSetting merchantGeneralSetting)
        {
            int StatusCode = Create(merchantGeneralSetting);
            return StatusCode;
        }

        public int UpdateDAC(MerchantGeneralSetting merchantGeneralSetting)
        {
            int StatusCode = Update(merchantGeneralSetting);
            return StatusCode;
        }
    }
}