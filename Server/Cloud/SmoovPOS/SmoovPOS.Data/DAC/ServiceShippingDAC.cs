﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class ServiceShippingDAC : CBDataRepositoryBase<ServiceShipping>
    {   
        string siteID = "";
        public ServiceShippingDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public int CreateDAC(ServiceShipping serviceShipping)
        {
            int StatusCode = Create(serviceShipping);
            return StatusCode;
        }
        public int deleteDAC(string id)
        {
            int statuscode = Delete(id);
            return statuscode;
        }

        public int updateDAC(ServiceShipping serviceShipping)
        {
            int statuscode = Update(serviceShipping);
            return statuscode;
        }
        public ServiceShipping detailDAC(string id)
        {
            ServiceShipping serviceShipping = Get(id);
            return serviceShipping;
        }

        public IView<ServiceShipping> GetAll(string designDoc, string viewName)
        {
            IView<ServiceShipping> listServiceShipping = GetView(designDoc, viewName);
            return listServiceShipping;
        }
    }
}