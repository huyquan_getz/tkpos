﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  Com.SmoovPOS.Data.DAC
{   
    public class RoleDAC : CBDataRepositoryBase<Role>
    {   
         string siteID = "";
         public RoleDAC(string siteIDParam)
         {
             SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public int CreateDAC(Role role)
        {
            int statusCode = Create(role);
            return statusCode;
        }
        public IView<Role> GetAllRoles(string designDoc, string viewName)
        {
            IView<Role> ListRole = GetView(designDoc, viewName);
            return ListRole;
        }
        public Role detailDAC(string id)
        {
            Role role = Get(id);
            return role;
        }
    }
}