﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Entity;
using System.Collections.Generic;
using System.Linq;

namespace Com.SmoovPOS.Data.DAC
{
    public class SMSSettingDAC : CBDataRepositoryBase<SMSSetting>
    {
        public SMSSettingDAC(string bucket)
        {
            SetClientCouchbase(bucket);
        }

        public int CreateDAC(SMSSetting smssetting)
        {
            return Create(smssetting);
        }

        public Couchbase.IView<SMSSetting> GetAll(string designDoc, string viewName)
        {
            return GetView(designDoc, viewName);
        }

        public SMSSetting GetDetailDAC(string id)
        {
            return Get(id);
        }

        public int UpdateDAC(SMSSetting smssetting)
        {
            int statuscode = Update(smssetting);
            return statuscode;
        }

        public int DeleteDAC(string id)
        {
            return Delete(id);
        }
    }
}