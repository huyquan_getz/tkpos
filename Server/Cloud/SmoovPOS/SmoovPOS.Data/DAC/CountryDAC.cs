﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;

namespace SmoovPOS.Data.DAC
{
    public class CountryDAC: CBDataRepositoryBase<Country>
    {   
         string siteID = "";
         public CountryDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public IView<Country> GetAllCountry(string designDoc, string viewName)
        {
            IView<Country> ListCountry = GetView(designDoc, viewName);
            return ListCountry;
        } 
    }
}