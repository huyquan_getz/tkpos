﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using System.Collections.Generic;
using System.Linq;

namespace Com.SmoovPOS.Data.DAC
{
    public class StationDAC : CBDataRepositoryBase<Station>
    {
        public StationDAC(string bucket)
        {
            SetClientCouchbase(bucket);
        }

        public int CreateDAC(Station station)
        {
            return Create(station);
        }

        public Couchbase.IView<Station> GetAll(string designDoc, string viewName)
        {
            return GetView(designDoc, viewName);
        }

        public Station GetDetailDAC(string id)
        {
            return Get(id);
        }

        public int UpdateDAC(Station station)
        {
            int statuscode = Update(station);
            return statuscode;
        }

        public int DeleteDAC(string id)
        {
            return Delete(id);
        }

        public int DeleteAll(string designDoc, string viewName)
        {
            IEnumerable<Station> list = GetView(designDoc, viewName);
            foreach (var item in list.Where(r => r._id != null))
            {
                Delete(item._id);
            }

            return 0;
        }
    }
}