﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System.Collections.Generic;

namespace Com.SmoovPOS.Data.DAC
{
    public class ProductItemDAC : CBDataRepositoryBase<ProductItem>
    {
        public ProductItemDAC(string bucket)
        {
            SetClientCouchbase(bucket);
        }
        public int CreateDAC(ProductItem product)
        {
            int StatusCode = Create(product);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        public int UpdateDAC(ProductItem product)
        {
            int StatusCode = Update(product);
            return StatusCode;
        }
        public ProductItem DetailDAC(string id)
        {
            ProductItem product = Get(id);
            return product;
        }

        public IEnumerable<ProductItem> GetAllDAC(string designDoc, string viewName)
        {
            IEnumerable<ProductItem> listProduct = GetAllByView(designDoc, viewName);
            return listProduct;
        }

        public IView<ProductItem> GetAllProducItem(string designDoc, string viewName)
        {
            IView<ProductItem> productItemList = GetView(designDoc, viewName);
            return productItemList;
        }
        public IView<ProductItem> GetProductItemDescendingByLimit(string designDoc, string viewName, string bucket, bool desc = true, int limit = 0, int skip = 0)
        {
            if (limit > 0)
            {
                return GetView(designDoc, viewName).Reduce(false).Key(bucket).Descending(desc).Limit(limit).Skip(skip);
            }
            else
            {
                return GetView(designDoc, viewName).Reduce(false).Key(bucket);
            }
        }
        public IView<ProductItem> GetAllProducItemWithBucket(string designDoc, string viewName, string bucket)
        {
            IView<ProductItem> productItemList = GetView(designDoc, viewName).Key(bucket);
            return productItemList;
        }
    }
}