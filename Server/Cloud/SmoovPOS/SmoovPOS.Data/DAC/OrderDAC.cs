﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using System.Collections.Generic;
using System.Linq;

namespace Com.SmoovPOS.Data.DAC
{
    public class OrderDAC : CBDataRepositoryBase<Order>
    {
        string siteID = "";
        public OrderDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }

        public int CreateDAC(Order order)
        {
            return Create(order);
        }

        public int updateDAC(Order order)
        {
            int statuscode = Update(order);
            return statuscode;
        }

        public int DeleteAllOrderDAC(string designDoc, string viewName, string siteID)
        {
            IEnumerable<Order> orderList = GetView(designDoc, viewName).Key(siteID);
            foreach (var item in orderList.Where(r => r._id != null))
            {
                Delete(item._id);
            }

            return 0;
        }

        public Order GetOrderDAC(string key)
        {
            return Get(key);
        }

        public IEnumerable<Order> GetAll(string designDoc, string viewName, string siteID)
        {
            return GetView(designDoc, viewName).Key(siteID);
        }

        public Couchbase.IView<Order> GetAllOrderByCustomerEmail(string designDoc, string viewName, string email, string siteID)
        {
            return GetView(designDoc, viewName).Key(new string[] { siteID, email });
        }

        /// <summary>
        /// Gets all order delivery.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:24 PM</datetime>
        public IEnumerable<Order> GetAllOrderDelivery(string designDoc, string viewName, string siteID)
        {
            return GetView(designDoc, viewName).StartKey(new string[] { siteID, "uefff" }).EndKey(new string[] { siteID });
        }
        /// <summary>
        /// Gets all order self collect.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:24 PM</datetime>
        public IEnumerable<Order> GetAllOrderSelfCollect(string designDoc, string viewName, string siteID)
        {
            return GetView(designDoc, viewName).StartKey(new string[] { siteID, "uefff" }).EndKey(new string[] { siteID });
        }
        /// <summary>
        /// Gets all order cancel.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:24 PM</datetime>
        public IEnumerable<Order> GetAllOrderCancel(string designDoc, string viewName, string siteID)
        {
            return GetView(designDoc, viewName).StartKey(new string[] { siteID, "uefff" }).EndKey(new string[] { siteID });
        }
        /// <summary>
        /// Gets the order descending by limit.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="desc">if set to <c>true</c> [desc].</param>
        /// <param name="limit">The limit.</param>
        /// <param name="skip">The skip.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:24 PM</datetime>
        public IEnumerable<Order> GetOrderDescendingByLimit(string designDoc, string viewName, string bucket, bool desc = true, int limit = 0, int skip = 0)
        {
            if (limit > 0)
            {
                return GetView(designDoc, viewName).Reduce(false).StartKey(new string[] { bucket, "uefff" }).EndKey(new string[] { bucket }).Descending(true).Limit(limit).Skip(skip);
            }
            else
            {
                return GetView(designDoc, viewName).Descending(true).Reduce(false).StartKey(new string[] { bucket, "uefff" }).EndKey(new string[] { bucket });
            }
        }
    }
}