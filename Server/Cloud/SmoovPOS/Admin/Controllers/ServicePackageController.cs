﻿using AutoMapper;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.CustomHTMLHelper;
using SmoovPOS.Utility.WSServicePacketReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmoovPOS.Common;
using Com.SmoovPOS.Model;
using System.Collections.Specialized;
using System.Text;
using Com.SmoovPOS.Common.Controllers;
using System.IO;
using System.Text.RegularExpressions;
using SmoovPOS.Utility.Controllers;
using SmoovPOS.Utility.WSWebContentReference;
using Com.SmoovPOS.Entity;
using SmoovPOS.Common.Controllers;
using System.Drawing;

namespace SmoovPOS.UI.Controllers
{
    public class ServicePackageController : Controller
    {
        // GET: ServicePacket
        [Authorize(Roles = "Admin")]
        public ActionResult Index(SearchServicePackageModel searchModel)
        {
            ViewBag.Class = "servicepackage";

            if (searchModel.pageGoTo == 0)
            {
                searchModel.pageGoTo = 1;
                searchModel.sort = string.Empty;
            }
            IWSServicePacketClient client = new IWSServicePacketClient();
            var model = client.WSGetServicePacketByFilter(searchModel);

            client.Close();
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchServicePackage(int pageGoTo, string sort, bool desc, string search)
        {
            //Thread.Sleep(20000);
            var searchModel = new SearchServicePackageModel() { pageGoTo = pageGoTo, sort = sort, desc = desc, name = search };
            IWSServicePacketClient client = new IWSServicePacketClient();
            var results = client.WSGetServicePacketByFilter(searchModel);
            results.name = search;
            client.Close();
            return PartialView("ServicePackageListing", results);
        }
        private void InitData(ServicePackageViewModel model, IWSServicePacketClient client)
        {
            var results = client.WSGetAllBillingCycle();

            var lstId = model.BillingServicePackets.Select(m => m.billingCycleID).ToList();
            results = results.Where(x => !lstId.Contains(x.billingCycleID)).ToArray();

            model.billingCycles = (from c in results
                                   select new SelectListItem
                                   {
                                       Value = c.billingCycleID.ToString(),
                                       Text = c.paymentCycleName
                                   }).ToList();

        }
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.Class = "servicepackage";
            IWSServicePacketClient client = new IWSServicePacketClient();

            var result = client.WSNewServicePacket(User.Identity.GetUserId());
            var model = Mapper.Map<ServicePackageModel, ServicePackageViewModel>(result);
            InitData(model, client);

            //foreach (var item in model.FunctionServicePackets.Where(x => x.keyMethod == ConstantSmoovs.AddOns_Key.ONLINESHOP
            //    || x.keyMethod == ConstantSmoovs.AddOns_Key.MEMBERSHIPFEE
            //    || x.keyMethod == ConstantSmoovs.AddOns_Key.QUEUEMANAGEMENT
            //    || x.keyMethod == ConstantSmoovs.AddOns_Key.TABLEORDERING).ToList())
            //{
            foreach (var item in model.FunctionServicePackets.Where(x => x.keyMethod == ConstantSmoovs.AddOns_Key.TABLEORDERING ||
                x.keyMethod == ConstantSmoovs.AddOns_Key.MAINPOSAPP ||
                x.keyMethod == ConstantSmoovs.AddOns_Key.QUEUEMANAGEMENT ||
                x.keyMethod == ConstantSmoovs.AddOns_Key.MEMBERSHIPFEE ||
                x.keyMethod == ConstantSmoovs.AddOns_Key.ONLINESHOP).ToList())
            {
                item.limit = 0;
            }

            var functionServicePackets = model.FunctionServicePackets.Where(x => !x.isAddOns).OrderBy(x => x.FunctionName).ToList();
            functionServicePackets.AddRange(model.FunctionServicePackets.Where(x => x.isAddOns).OrderBy(x => x.createdDate).ToList());

            model.FunctionServicePackets = functionServicePackets;
            model.status = true;
            //InitData(model);
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(ServicePackageViewModel viewmodel, FormCollection fc)
        {
            StringHelper.Trim(viewmodel);
            #region create Function

            //create Function
            IWSServicePacketClient client = new IWSServicePacketClient();
            //var bn = viewmodel.businessName.Replace(" ", string.Empty).ToLower();//remove space and to lower
            viewmodel.image = fc["logoImg"];
            viewmodel.createdDate = DateTime.Now;
            viewmodel.userOwner = User.Identity.GetUserId();
            // Check button active or inactive?
            bool isActive = true;
            if (!string.IsNullOrWhiteSpace(fc["optionsRadios"]))
            {
                isActive = Boolean.Parse(fc["optionsRadios"]);
            }
            viewmodel.status = isActive;

            GetLimitFunctionMethod(viewmodel, fc, 0);

            if (!string.IsNullOrWhiteSpace(fc["optionsOnlineShop"]))
            {
                isActive = Boolean.Parse(fc["optionsOnlineShop"]);
            }


            viewmodel.display = true;

            var model = Mapper.Map<ServicePackageViewModel, ServicePackageModel>(viewmodel);

            var billingServicePackets = new List<BillingServicePackageModel>() { };
            GetBillingCycleInfo(fc, billingServicePackets);
            model.BillingServicePackets = billingServicePackets;

            var value = client.WSCreateServicePacket(model);

            client.Close();
            if (value != null && value != Guid.Empty)
            {
                return RedirectToAction("Index");
            }
            #endregion

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        private static void GetLimitFunctionMethod(ServicePackageViewModel viewmodel, FormCollection fc, int defaultLimit)
        {
            foreach (var item in viewmodel.FunctionServicePackets)
            {
                if (item.keyMethod == ConstantSmoovs.AddOns_Key.MEMBERSHIPFEE)
                {
                    item.limit = defaultLimit;
                    item.isSelect = true;
                    item.isUnlimit = false;
                    if (!string.IsNullOrWhiteSpace(fc["optionsMembership"]))
                    {
                        var limit = int.Parse(fc["optionsMembership"].ToString());
                        item.limit = limit;
                    }
                }
                if (item.keyMethod == ConstantSmoovs.AddOns_Key.ONLINESHOP)
                {
                    item.limit = defaultLimit;
                    item.isSelect = true;
                    item.isUnlimit = false;
                    if (!string.IsNullOrWhiteSpace(fc["optionsOnlineShop"]))
                    {
                        var limit = int.Parse(fc["optionsOnlineShop"].ToString());
                        item.limit = limit;
                    }
                }
                if (item.keyMethod == ConstantSmoovs.AddOns_Key.QUEUEMANAGEMENT)
                {
                    item.limit = defaultLimit;
                    item.isSelect = true;
                    item.isUnlimit = false;
                    if (!string.IsNullOrWhiteSpace(fc["optionsQueueManagement"]))
                    {
                        var limit = int.Parse(fc["optionsQueueManagement"].ToString());
                        item.limit = limit;
                    }
                }
                if (item.keyMethod == ConstantSmoovs.AddOns_Key.TABLEORDERING)
                {
                    item.limit = defaultLimit;
                    item.isSelect = true;
                    item.isUnlimit = false;
                    if (!string.IsNullOrWhiteSpace(fc["optionsTableOrdering"]))
                    {
                        var limit = int.Parse(fc["optionsTableOrdering"].ToString());
                        item.limit = limit;
                    }
                }
                if (item.keyMethod == ConstantSmoovs.AddOns_Key.MAINPOSAPP)
                {
                    item.limit = defaultLimit;
                    item.isSelect = true;
                    item.isUnlimit = false;
                    if (!string.IsNullOrWhiteSpace(fc["optionsMainPOSApp"]))
                    {
                        var limit = int.Parse(fc["optionsMainPOSApp"].ToString());
                        item.limit = limit;
                    }
                }
            }
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Detail(Guid id)
        {
            ViewBag.Class = "servicepackage";
            IWSServicePacketClient client = new IWSServicePacketClient();
            ServicePackageModel servicePacket = new ServicePackageModel();
            servicePacket = client.WSGetServicePacket(id);

            var model = Mapper.Map<ServicePackageModel, ServicePackageViewModel>(servicePacket);

            var functionServicePackets = model.FunctionServicePackets.Where(x => !x.isAddOns).OrderBy(x => x.FunctionName).ToList();
            functionServicePackets.AddRange(model.FunctionServicePackets.Where(x => x.isAddOns).OrderBy(x => x.createdDate).ToList());

            model.FunctionServicePackets = functionServicePackets;

            model.BillingServicePackets = model.BillingServicePackets.OrderBy(b => b.number).ToList();
            client.Close();

            return View(model);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid id)
        {
            ViewBag.Class = "servicepackage";

            IWSServicePacketClient client = new IWSServicePacketClient();
            ServicePackageModel ServicePacket = client.WSGetServicePacket(id);
            //ServicePacket = UpdateListProductOfServicePacket(ServicePacket, client);
            //client.WSUpdateServicePacket(ServicePacket, bucket);

            var model = Mapper.Map<ServicePackageModel, ServicePackageViewModel>(ServicePacket);
            InitData(model, client);

            client.Close();

            var functionServicePackets = model.FunctionServicePackets.Where(x => !x.isAddOns).OrderBy(x => x.FunctionName).ToList();
            functionServicePackets.AddRange(model.FunctionServicePackets.Where(x => x.isAddOns).OrderBy(x => x.createdDate).ToList());

            model.FunctionServicePackets = functionServicePackets;
            //model.arrayProduct = model.arrayProduct.OrderByDescending(p => p.createdDate).ToList();

            model.BillingServicePackets = model.BillingServicePackets.OrderBy(b => b.number).ToList();
            var lst = (from a in model.BillingServicePackets
                       select new { id = a.billingCycleID.ToString() }).Select(x => x.id).ToList();

            model.billingCycles = model.billingCycles.Where(x => !lst.Contains(x.Value)).ToList();

            return View(model);
            //return View(ServicePacket);
        }
        [HttpPost]
        public ActionResult Edit(ServicePackageViewModel viewmodel, FormCollection fc)
        {
            if (viewmodel.servicePacketID == Guid.Empty)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            StringHelper.Trim(viewmodel);

            IWSServicePacketClient client = new IWSServicePacketClient();
            var result = client.WSGetServicePacket(viewmodel.servicePacketID);
            if (result != null)
            {
                viewmodel.image = fc["logoImg"];
                viewmodel.modifyDate = DateTime.Now;
                viewmodel.modifyUser = User.Identity.GetUserId();

                GetLimitFunctionMethod(viewmodel, fc, -1);
                foreach (var item in viewmodel.FunctionServicePackets.Where(f => f.limit < 0))
                {
                    if (result.FunctionServicePackets.Any(f => f.functionServiceID == item.functionServiceID))
                    {
                        var function = result.FunctionServicePackets.FirstOrDefault(f => f.functionServiceID == item.functionServiceID);
                        item.limit = function.limit;
                    }
                }

                var model = Mapper.Map<ServicePackageViewModel, ServicePackageModel>(viewmodel);

                var billingServicePackets = new List<BillingServicePackageModel>() { };
                GetBillingCycleInfo(fc, billingServicePackets);
                model.BillingServicePackets = billingServicePackets;

                var value = client.WSUpdateServicePacket(model);
                client.Close();
                if (value)
                {
                    return RedirectToAction("Detail", new { id = model.servicePacketID });
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult IsExistedName(string servicePacketName, Guid? servicePacketID)
        {
            if (string.IsNullOrEmpty(servicePacketName))
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            IWSServicePacketClient client = new IWSServicePacketClient();
            var isExists = client.WSExistServicePacketByName(servicePacketName, servicePacketID);
            client.Close();
            return new JsonResult
            {
                Data = !isExists,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult SelectBillingCycle(Guid billingCycle, string amountPerUnit, double discount, string paymentCycleName, Guid servicePacketID, FormCollection fc)
        {
            var billingServicePackets = new List<BillingServicePackageModel>() { };

            GetBillingCycleInfo(fc, billingServicePackets);
            var value = new BillingServicePackageModel();

            value.billingCycleID = billingCycle;
            value.billingServicePacketID = Guid.NewGuid();
            value.servicePacketID = servicePacketID;

            value.moneyPerUnit = decimal.Parse(amountPerUnit.Replace(",", ""));
            value.discount = discount;

            value.paymentCycleName = paymentCycleName;
            value.createdDate = DateTime.UtcNow;
            value.userOwner = User.Identity.GetUserId();
            value.display = true;

            IWSServicePacketClient client = new IWSServicePacketClient();
            var result = client.WSGetBillingCycle(value.billingCycleID);
            if (billingCycle != null)
            {
                value.number = result.number;
            }

            billingServicePackets.Add(value);

            return PartialView(billingServicePackets.OrderBy(b => b.number).ToList());
        }

        private static void GetBillingCycleInfo(FormCollection fc, List<BillingServicePackageModel> billingServicePackets)
        {
            string billingCycleIDs = fc["billingCycleIDs"];
            string billingServicePacketIDs = fc["billingServicePacketIDs"];
            string servicePacketIDs = fc["servicePacketIDs"];
            string discounts = fc["discounts"];
            string moneyPerUnits = fc["moneyPerUnits"];
            string createdDates = fc["createdDates"];
            string userOwners = fc["userOwners"];
            string paymentCycleNames = fc["paymentCycleNames"];
            string numberBillingCycles = fc["numberBillingCycle"];

            string[] ListbillingCycleIDs = !string.IsNullOrWhiteSpace(billingCycleIDs) ? billingCycleIDs.Split(',') : new string[0];
            string[] ListbillingServicePacketIDs = !string.IsNullOrWhiteSpace(billingServicePacketIDs) ? billingServicePacketIDs.Split(',') : new string[0];
            string[] ListservicePacketIDs = !string.IsNullOrWhiteSpace(servicePacketIDs) ? servicePacketIDs.Split(',') : new string[0];
            string[] Listdiscounts = !string.IsNullOrWhiteSpace(discounts) ? discounts.Split(',') : new string[0];
            string[] ListmoneyPerUnits = !string.IsNullOrWhiteSpace(moneyPerUnits) ? moneyPerUnits.Split(',') : new string[0];
            string[] ListcreatedDates = !string.IsNullOrWhiteSpace(createdDates) ? createdDates.Split(',') : new string[0];
            string[] ListuserOwners = !string.IsNullOrWhiteSpace(userOwners) ? userOwners.Split(',') : new string[0];
            string[] ListpaymentCycleNames = !string.IsNullOrWhiteSpace(paymentCycleNames) ? paymentCycleNames.Split(',') : new string[0];
            string[] ListnumberBillingCycles = !string.IsNullOrWhiteSpace(numberBillingCycles) ? numberBillingCycles.Split(',') : new string[0];

            for (int i = 0; i < ListbillingCycleIDs.Count(); i++)
            {
                var result = new BillingServicePackageModel();

                result.billingCycleID = new Guid(ListbillingCycleIDs[i]);
                result.billingServicePacketID = new Guid(ListbillingServicePacketIDs[i]);
                result.servicePacketID = new Guid(ListservicePacketIDs[i]);

                decimal amountPerU = 0;
                if (decimal.TryParse(ListmoneyPerUnits[i], out amountPerU))
                {
                    result.moneyPerUnit = amountPerU;
                }

                double dis = 0;
                if (double.TryParse(Listdiscounts[i], out dis))
                {
                    result.discount = dis;
                }

                int numberBilingCycle = 0;
                if (int.TryParse(ListnumberBillingCycles[i], out numberBilingCycle))
                {
                    result.number = numberBilingCycle;
                }

                result.paymentCycleName = ListpaymentCycleNames[i];

                DateTime dat = Convert.ToDateTime(ListcreatedDates[i]);
                result.createdDate = dat;

                result.userOwner = ListuserOwners[i];
                result.display = true;

                billingServicePackets.Add(result);
            }
        }

        [HttpPost]
        public ActionResult LoadBillingCycle(string[] selectBillingCycle)
        {
            IWSServicePacketClient client = new IWSServicePacketClient();
            var results = client.WSGetAllBillingCycle();

            client.Close();
            if (results.Count() > 0)
            {
                if (selectBillingCycle != null && selectBillingCycle.Count() > 0)
                {
                    var lst = (from a in selectBillingCycle
                               select new { id = new Guid(a) }).Select(x => x.id).ToList();

                    results = results.Where(x => !lst.Contains(x.billingCycleID)).ToArray();
                }

                if (results.Count() > 0)
                {
                    var billingCycles = (from c in results
                                         select new SelectListItem
                                         {
                                             Value = c.billingCycleID.ToString(),
                                             Text = c.paymentCycleName
                                         }).ToList();

                    billingCycles[0].Selected = true;
                    return Json(new { success = true, data = billingCycles }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public int UpdateStatus(string id, bool status)
        {
            IWSServicePacketClient client = new IWSServicePacketClient();
            Guid guid = Guid.Parse(id);
            //MerchantManagementModel model = client.WSGetMerchantManagement(guid);
            //model.status = status;
            //bool isUpdate = client.WSUpdateMerchantManagement(model);
            bool isUpdate = client.WSChangeStatus(guid, status);

            client.Close();
            return isUpdate ? 1 : 0;
            //   return RedirectToAction("Index");
        }

        public JsonResult GetMerchantUsingServicePackage(string id)
        {
            IWSServicePacketClient client = new IWSServicePacketClient();
            var list = client.WSGetMerchantUsingServicePackage(id, DateTime.UtcNow);
            client.Close();
            return Json(new { success = true, list = list }, JsonRequestBehavior.AllowGet);
        }

        public RedirectToRouteResult ViewList(string fullname)
        {
            SearchMerchantManagementModel searchModel = new SearchMerchantManagementModel();
            searchModel.pageGoTo = 0;
            searchModel.pageGoTo = 1;
            searchModel.sort = string.Empty;
            searchModel.fullName = fullname;
            return RedirectToAction("Index", "MerchantManagement", searchModel);
        }
    }
}