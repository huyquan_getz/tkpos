﻿using AutoMapper;
using SmoovPOS.Business.Business;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.MailServer;
using SmoovPOS.Utility.WSMerchantManagementReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SmoovPOS.UI.Controllers
{
    public class DefaultEmailController : Controller
    {
        // GET: DefaultEmail
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            ViewBag.Class = "defaultEmail";
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            MailServerModel mailAccount1 = mailConfig.GetConfigMailServer(null);
            MailServerModel mailAccount2 = mailConfig.GetConfigMailServer(null,1);
            var model = Mapper.Map<MailServerModel, DefaultEmailViewModel>(mailAccount1);
            model.emailAccount1 = mailAccount1.UserName;
            model.emailAccount2 = mailAccount2.UserName;
            return View(model);
           
          
        }

        /// <summary>
        /// Configurations the email.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/7/2015-1:50 PM</datetime>
        /// 
        [Authorize(Roles = "Admin")]
        public ActionResult ConfigEmail(int type)
        {
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            MailServerModel mail = mailConfig.GetConfigMailServer(null, type);
            var model = Mapper.Map<MailServerModel, DefaultEmailViewModel>(mail);
            model.type = type;
            return View(model);
        }

        public ActionResult SaveConfigPrivate(FormCollection fc)
        {
            ViewBag.Class = "defaultEmail";

            try
            {
                // TODO: Add update logic here
                MailConfigBusiness mailConfig = new MailConfigBusiness();
                MailServerModel mail = new MailServerModel();
                string id = fc["Id"];
                if (id.Equals(""))
                {
                    mail.Id = Guid.NewGuid().ToString();

                }
                mail.IncomingPop = fc["IncomingPop"];
                mail.IncomingPort = Int32.Parse(fc["IncomingPort"].ToString());
                mail.UserName = fc["UserName"];
                mail.PassWord = fc["PassWord"];
                mail.OutgoingSmtp = fc["OutgoingSmtp"];
                mail.OutgoingPort = Int32.Parse(fc["OutgoingPort"].ToString());
                mail.Credentials = false;
                bool check = false;
                try
                {
                    if (fc["EnableSsl"].Equals("on"))
                    {
                        check = true;
                    }
                }
                catch (Exception e)
                {
                    e.Message.ToString();
                }
                mail.EnableSsl = check;
                mail.CreateDate = DateTime.UtcNow;    
                mailConfig.AddOrUpdateMailServer(id, mail,null);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                // return View("Index");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }


        }

        /// <summary>
        /// Sends the test mail.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/7/2015-1:50 PM</datetime>
        [Authorize(Roles = "Admin")]
        public ActionResult SendTestMail(int type)
        {
            ViewBag.Class = "defaultEmail";

            MailConfigBusiness mailConfig = new MailConfigBusiness();
            MailServerModel mail = mailConfig.GetConfigMailServer(null, type);
            var model = Mapper.Map<MailServerModel, DefaultEmailViewModel>(mail);
            model.type = type;
            return View(model);
        }

        /// <summary>
        /// Tests the send mail.
        /// </summary>
        /// <param name="emailReceive">The email receive.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/7/2015-1:49 PM</datetime>
        public bool TestSendMail(string emailReceive)
        {
            ViewBag.Class = "defaultEmail";
            int type = ConstantSmoovs.ConfigMail.Merchant;
            string titleEmail = "Test Config Email";
            string body = "This is a test e-mail sent from SmoovPos.";
            MailServerUtil sendMail = new MailServerUtil();
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            var userid = User.Identity.GetUserId();
            bool isSend = false;
            isSend = sendMail.SendMail(type, "SmoovPOS Admin", emailReceive, titleEmail, body);
            return isSend;


        }

        /// <summary>
        /// Tests the configuration server SMTP.
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="portConfig">The port configuration.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/7/2015-2:07 PM</datetime>
        [HttpPost]
        public bool TestConfigServerSMTP(string domain, int portConfig)
        {
            MailServerUtil sendMail = new MailServerUtil();
            bool isSuccess = false;
            sendMail.TestConfigServerSMTP(domain, portConfig);
            return isSuccess;
        }

        /// <summary>
        /// Tests the connection.
        /// </summary>
        /// <param name="hostName">Name of the host.</param>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/7/2015-2:08 PM</datetime>
        [HttpPost]
        public bool TestConnection(string hostName, int port)
        {
            MailServerUtil mailUtil = new MailServerUtil();
            bool isSuccess = mailUtil.TestConnection(hostName, port);
            return isSuccess;
        }

        /// <summary>
        /// Valids the SMTP.
        /// </summary>
        /// <param name="hostName">Name of the host.</param>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/7/2015-2:08 PM</datetime>
        public bool ValidSMTP(string hostName, int port)
        {
            MailServerUtil mailUtil = new MailServerUtil();
            TimeSpan timeSpan = new TimeSpan(0, 0, 0, 10, 0);
            bool isSuccess = mailUtil.ValidSMTP(hostName, port,timeSpan);
            return isSuccess;
        }

        /// <summary>
        /// Valids the pop.
        /// </summary>
        /// <param name="hostName">Name of the host.</param>
        /// <param name="port">The port.</param>
        /// <param name="user">The user.</param>
        /// <param name="pass">The pass.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/7/2015-2:08 PM</datetime>
        public bool ValidPOP(string hostName, int port, string user, string pass)
        {

            MailServerUtil mailUtil = new MailServerUtil();
            //bool isValid = mailUtil.ValidPOP(hostName, port, user, pass);
            //return isValid;
            TimeSpan timeSpan = new TimeSpan(0, 0, 0, 10, 0);
            bool isSuccess = mailUtil.ValidPOP(hostName, port, timeSpan);
            return isSuccess;
        }
    }
}