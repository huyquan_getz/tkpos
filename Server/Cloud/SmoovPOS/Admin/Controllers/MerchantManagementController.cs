﻿using AutoMapper;
using Com.SmoovPOS.Model;
using Com.SmoovPOS.UI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.CustomHTMLHelper;
using SmoovPOS.Utility.MailServer;
using SmoovPOS.Utility.WSInitCouchBaseReference;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Utility.WSServicePacketReference;
using SmoovPOS.Utility.WSSiteReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;


namespace SmoovPOS.UI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/30/2015-5:28 PM</datetime>
    public class MerchantManagementController : BaseController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MerchantManagementController"/> class.
        /// </summary>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        public MerchantManagementController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="MerchantManagementController"/> class.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        public MerchantManagementController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        // private ApplicationUserManager _userManager;

        /// <summary>
        /// Gets the user manager.
        /// </summary>
        /// <value>
        /// The user manager.
        /// </value>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        public UserManager<ApplicationUser> UserManager { get; private set; }


        // GET: MerchantManagement
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-5:41 PM</datetime>
        [Authorize(Roles = "Admin")]
        public ActionResult Index(SearchMerchantManagementModel searchModel)
        //int limit = 25, int pageGoTo = 1, int pageCurrent = 1, string sort = "title", Boolean desc = false
        {
            //WebMail.SmtpServer = "localhost";
            //WebMail.SmtpPort = 25;   // Or the port you've been told to use
            //WebMail.EnableSsl = false;
            //WebMail.UserName = "";
            //WebMail.Password = "";
            //WebMail.From = "hoan.tran@smoovapp.com"; // random email

            //WebMail.Send(to: "hoan.tran@smoovapp.com",
            //    subject: "Test email message",
            //    body: "This is a debug email message"
            //);

            //var mail = new MailMessage("hoan.tran@smoovapp.com", "hoan.tran@smoovapp.com", "email.EmailTitle", "")
            //{
            //    BodyEncoding = Encoding.UTF8,
            //    IsBodyHtml = true
            //};
            //mail.Body = string.Format("Dear {0}<BR/>Thank you for your registration, please click on the below link to complete your registration: <a href=\"{1}\" title=\"User Email Confirm\">{1}</a>", "hoan", Url.Action("ConfirmEmail", "Account", new { Token = "Id", Email ="Email" }, Request.Url.Scheme));

            //SmtpClient smtp;
            //smtp = new SmtpClient
            //{
            //    Host = ConfigurationManager.AppSettings[ConstantSmoovs.EmailConfiguration.SMTP_SERVER]
            //};
            //if (string.IsNullOrEmpty(ConfigurationManager.AppSettings[ConstantSmoovs.EmailConfiguration.SMTP_PASSWORD]))
            //{
            //    smtp.UseDefaultCredentials = false;
            //    smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings[ConstantSmoovs.EmailConfiguration.SMTP_USERNAME],
            //                                               ConfigurationManager.AppSettings[ConstantSmoovs.EmailConfiguration.SMTP_PASSWORD]);
            //}
            //bool enableSsl;
            //bool.TryParse(ConfigurationManager.AppSettings[ConstantSmoovs.EmailConfiguration.SMTP_ENABLESSL], out enableSsl);
            //smtp.EnableSsl = enableSsl;

            ////System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.mydomain.com");
            ////smtp.Credentials = new System.Net.NetworkCredential("sender@mydomain.com", "password");
            ////smtp.EnableSsl = true;

            //smtp.Send(mail);
            //  SearchMerchantManagementModel searchModel = new SearchMerchantManagementModel();
            // First load
            ViewBag.Class = "management";
            if (searchModel.pageGoTo == 0)
            {
                searchModel.pageGoTo = 1;
                searchModel.sort = string.Empty;
            }
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var model = client.WSGetMerchantManagementByFilter(searchModel);
            client.Close();
            return View(model);
        }
        /// <summary>
        /// Actives the merchant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pinCode">The pin code.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        public ActionResult ActiveMerchant(Guid id, Guid pinCode)
        {
            if (id != null)
            {
                IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                //var merchantId = new Guid(id);
                var model = client.WSGetMerchantManagement(id);
                if (model != null)
                {
                    model.status = true;
                    model.verifyLink = string.Empty;
                    var result = client.WSUpdateMerchantManagement(model);
                    if (result)
                    {
                        return RedirectToAction("Index");
                    }
                }
                client.Close();
            }
            //  SearchMerchantManagementModel searchModel = new SearchMerchantManagementModel();
            // First load
            //if (searchModel.pageGoTo == 0)
            //{
            //    searchModel.pageGoTo = 1;
            //}
            //IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            //var model = client.WSGetMerchantManagementByFilter(searchModel);

            return View();
        }
        /// <summary>
        /// Details the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-5:42 PM</datetime>
        [Authorize(Roles = "Admin")]
        public ActionResult Detail(Guid id)
        {
            ViewBag.Class = "management";
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var model = client.WSGetMerchantManagement(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            client.Close();
            var viewmodel = Mapper.Map<MerchantManagementModel, MerchantManagementViewModel>(model);
            viewmodel.fullName = string.Format("{0} {1}", viewmodel.title, viewmodel.fullName);
            IWSServicePacketClient svclient = new IWSServicePacketClient();
            var servicepackage = svclient.WSGetServicePacket(viewmodel.servicePacketID.Value);
            svclient.Close();
            var billingServicePackets = servicepackage.BillingServicePackets.Where(b => b.billingCycleID == viewmodel.billingCycleId).FirstOrDefault();
            if (billingServicePackets != null)
            {
                viewmodel.BillingCycleName = billingServicePackets.paymentCycleName + " - " + billingServicePackets.moneyType + " " + billingServicePackets.moneyPerUnit;
            }
            return View(viewmodel);
        }


        /// <summary>
        /// Updates the status merchant.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <returns>return status when update success or not</returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/10/2015-3:35 PM</datetime>
        [HttpPost]
        public int updateStatusMerchant(string id, bool status)
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            Guid guid = Guid.Parse(id);
            //MerchantManagementModel model = client.WSGetMerchantManagement(guid);
            //model.status = status;
            //bool isUpdate = client.WSUpdateMerchantManagement(model);
            bool isUpdate = client.WSChangeStatusMerchantManagement(guid, status);

            client.Close();
            return isUpdate ? 1 : 0;
            //   return RedirectToAction("Index");
        }

        /// <summary>
        /// Initializes the data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        private void InitData(MerchantManagementViewModel model)
        {
            //WSInventoryClient clientInventory = new WSInventoryClient();
            //Country[] countrys = clientInventory.WSGetAllCountry("country", "get_all_country", VariableConfigController.GetBucket());
            //if (countrys.Count() == 0)
            //{
            //    countrys = clientInventory.WSGetAllCountry("country", "get_all_country", VariableConfigController.GetBucket());
            //}
            //model.countrys = new List<SelectListItem>() { };
            //if (countrys.Count() > 0)
            //{
            //    var arrCountry = countrys[0];
            //    foreach (string c in arrCountry.arrayCountry)
            //    {
            //        var countryItem = new SelectListItem() { Text = c, Value = c };
            //        if (!string.IsNullOrEmpty(model.country) && countryItem.Value == model.country)
            //        {
            //            countryItem.Selected = true;
            //        }
            //        model.countrys.Add(countryItem);
            //    }
            //}
            //else
            //{
            //    model.countrys = new List<SelectListItem>() { 
            //    //new SelectListItem(){Text=ConstantSmoovs.Countrys.Malaysia, Value=ConstantSmoovs.Countrys.Malaysia},
            //    new SelectListItem(){Text=ConstantSmoovs.Countrys.Singapore, Value=ConstantSmoovs.Countrys.Singapore}};
            //}
            //clientInventory.Close();
            IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
            var resultCountry = clientMerchant.WSGetAllCountry();
            clientMerchant.Close();
            model.countrys = (from c in resultCountry
                              select new SelectListItem
                              {
                                  Value = c,
                                  Text = c,
                                  Selected = (c == model.country)
                              }).ToList();

            model.titles = new List<SelectListItem>() { 
                //new SelectListItem(){Text="All Filter", Value="", Selected = true},
                new SelectListItem(){Text=ConstantSmoovs.Titles.SelectTitle, Value=string.Empty, Selected  = string.IsNullOrEmpty(model.title)},
                new SelectListItem(){Text=ConstantSmoovs.Titles.Dr, Value=ConstantSmoovs.Titles.Dr, Selected = (ConstantSmoovs.Titles.Dr==model.title)},
                new SelectListItem(){Text=ConstantSmoovs.Titles.Mr, Value=ConstantSmoovs.Titles.Mr, Selected = (ConstantSmoovs.Titles.Mr==model.title)},
                new SelectListItem(){Text=ConstantSmoovs.Titles.Mrs, Value=ConstantSmoovs.Titles.Mrs, Selected = (ConstantSmoovs.Titles.Mrs==model.title)},
                new SelectListItem(){Text=ConstantSmoovs.Titles.Ms, Value=ConstantSmoovs.Titles.Ms, Selected = (ConstantSmoovs.Titles.Ms==model.title)}
            };

            IWSServicePacketClient client = new IWSServicePacketClient();
            var resultSP = client.WSGetAllServicePacketDisplay().Where(r => r.display == true && r.status == true);
            model.servicePackets = (from s in resultSP
                                    select new SelectListItem
                                    {
                                        Value = s.servicePacketID.ToString(),
                                        Text = s.servicePacketName,
                                        Selected = (s.servicePacketID == model.servicePacketID)
                                    }).ToList();

            client.Close();

        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.Class = "management";
            var model = new MerchantManagementViewModel();
            InitData(model);
            return View(model);
        }
        /// <summary>
        /// Creates the specified viewmodel.
        /// </summary>
        /// <param name="viewmodel">The viewmodel.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        [HttpPost]
        public async Task<ActionResult> Create(MerchantManagementViewModel viewmodel)
        {
            StringHelper.Trim(viewmodel);
            #region create asp user
            ApplicationDbContext _db = new ApplicationDbContext();
            var user = new ApplicationUser() { UserName = viewmodel.email, Email = viewmodel.email };

            var result = await UserManager.CreateAsync(user, Guid.NewGuid().ToString());

            if (result.Succeeded)
            {
                //assign user to role merchant
                ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));

                _db.AddUserToRole(userManager, user.Id, ConstantSmoovs.RoleNames.Merchant);
            }
            #endregion
            #region create merchant
            if (result.Succeeded)
            {
                //create merchant
                IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
                //var bn = viewmodel.businessName.Replace(" ", string.Empty).ToLower();//remove space and to lower

                //ConstantSmoovs.DefaultFormats
                viewmodel.merchantID = new Guid(user.Id); //Guid.NewGuid();
                //var countMerchant = clientMerchant.WSGetCountAllMerchantManagement();
                //if (countMerchant <= 0)
                //{
                //    countMerchant = 0;
                //}
                //countMerchant = countMerchant + 1;
                //var bucket = string.Format("{0}_{1}", ConstantSmoovs.DefaultFormats.bucket, countMerchant);
                //var bucket_shadown = string.Format("{0}_{1}", bucket, ConstantSmoovs.DefaultFormats.shadown);
                //viewmodel.bucket = bucket_shadown; //string.Format("{0}_{1}", bn, (DateTime.UtcNow - ConstantSmoovs.year1970).Ticks);
                viewmodel.bucket = viewmodel.merchantID.ToString();

                viewmodel.createdDate = DateTime.Now;
                viewmodel.userOwner = User.Identity.GetUserId();
                if (string.IsNullOrEmpty(viewmodel.userOwner))
                {
                    viewmodel.userOwner = (new UserSession()).UserId;
                }
                viewmodel.fullName = string.Format("{0} {1}", viewmodel.firstName, viewmodel.lastName);
                viewmodel.gender = (viewmodel.title == ConstantSmoovs.Titles.Mr || viewmodel.title == ConstantSmoovs.Titles.Dr);
                viewmodel.userID = user.Id;
                viewmodel.status = false;
                viewmodel.display = true;
                //model.servicePacketEndDate

                var urlActive = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.ActiveLinkMerchant];
                viewmodel.verifyLink = string.Format(urlActive, viewmodel.merchantID, viewmodel.servicePacketID);
                viewmodel.servicePacketStartDate = DateTime.Now;
                if (viewmodel.servicePacketStartDate.HasValue)
                {
                    if (viewmodel.servicePacketID.GetValueOrDefault().ToString() == "7A5029B4-7639-47AE-BC2E-BDA4CC7189D0")
                    {
                        viewmodel.servicePacketEndDate = viewmodel.servicePacketStartDate.Value.AddDays(7);
                    }
                    else
                    {
                        viewmodel.servicePacketEndDate = viewmodel.servicePacketStartDate.Value.AddYears(1);
                    }
                }
                viewmodel.proxyPort = clientMerchant.WSGetNextProxyPort();//proxy buket
                viewmodel.proxyPort = viewmodel.proxyPort + 1;//proxy buket shadown

                var model = Mapper.Map<MerchantManagementViewModel, MerchantManagementModel>(viewmodel);

                var value = clientMerchant.WSCreateMerchantManagement(model);
                if (value != null)
                {
                    //create Site
                    WSSiteClient clientSite = new WSSiteClient();
                    var modelSite = new SiteModel();
                    modelSite.SiteID = viewmodel.bucket;
                    modelSite.Name = viewmodel.businessName;
                    //modelSite.Type = result.Type;
                    modelSite.UserRegistration = viewmodel.email;
                    //modelSite.ExpiryDate = result.ExpiryDate;

                    if (clientSite.WSCreateSiteSQL(modelSite))
                    {
                        //Create SiteAlias
                        var modelSiteAlias = new SiteAliasModel();
                        modelSiteAlias.SiteAliasID = modelSite.SiteID;
                        modelSiteAlias.SiteID = modelSite.SiteID;
                        //var siteOnline = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.SiteOnline];
                        //if (siteOnline != null)
                        //{
                        //    modelSiteAlias.HTTPAlias = string.Format(siteOnline.ToString(), bn);
                        //}

                        modelSiteAlias.IsPrimary = true;

                        if (clientSite.WSCreateSiteAliasSQL(modelSiteAlias))
                        {
                            //new Thread(() =>
                            //{
                            try
                            {

                                WSInitCouchBaseClient client = new WSInitCouchBaseClient();
                                //string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
                                //if (client.CheckExistBucketCouchbase(bucketSmoovPOS, 3456))
                                //{
                                //    client.CreateBucketCouchbase(bucketSmoovPOS, 3456);
                                //    client.AutoGenerateViewCouchBase(bucketSmoovPOS);
                                //    //client.AutoGenerateDocDefault(bucket);
                                //    Thread.Sleep(2000);
                                //    client.Close();
                                //    client = new WSInitCouchBaseClient();
                                //}

                                //        int ProxyPort = model.proxyPort - 1;
                                //        var isSuccess = client.CreateBucketCouchbase(bucket, ProxyPort);
                                //        //create fail re-create
                                //        while (isSuccess == 0)
                                //        {
                                //            ProxyPort = ProxyPort + 1;//next port
                                //            isSuccess = client.CreateBucketCouchbase(bucket, ProxyPort);
                                //        }
                                //        Thread.Sleep(2000);
                                //        isSuccess = 0;
                                //        while (isSuccess == 0)
                                //        {
                                //            ProxyPort = ProxyPort + 1;//next port
                                //            isSuccess = client.CreateBucketCouchbase(model.bucket, ProxyPort);
                                //        }
                                //        if (isSuccess > 0)
                                //        {
                                //            client.AutoGenerateViewCouchBase(model.bucket);
                                client.AutoGenerateDocDefault(model.bucket);

                                client.Close();
                                //            //Do an advanced looging here which takes a while

                                //            //change proxyport
                                //            if (ProxyPort != model.proxyPort)
                                //            {
                                //                model.proxyPort = ProxyPort;
                                //                clientMerchant.WSUpdateMerchantManagement(model);
                                //            }
                                //        }
                            }
                            catch
                            {
                            }
                            //}).Start();
                        }
                    }
                }
                //return RedirectToAction("index");
                try
                {
                    // send email for merchant == >> Call function SendEmail
                    //IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
                    //TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, VariableConfigController.GetBucket());
                    //if (listEmail.Count() == 0)
                    //{
                    //    listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, VariableConfigController.GetBucket());
                    //}
                    string bodyMail = "";

                    StringBuilder resultBuild;
                    //TemplateEmail emailItem = listEmail.FirstOrDefault(r => r.index == ConstantSmoovs.BodyEmail.MerchantVerifyAccountEmail);
                    //bodyMail = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
                    //bodyMail = RenderRazorViewToStringNoModel("~/Views/EmailTemplate/TempleSendEmailRegisterComplete.cshtml");
                    bodyMail = RenderRazorViewToStringNoModel("~/Views/EmailTemplate/VerifyEmail.cshtml");
                    resultBuild = new StringBuilder(bodyMail);
                    resultBuild.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfMerchant, viewmodel.firstName);
                    resultBuild.Replace(ConstantSmoovs.EmailKeyword.BusinessName, viewmodel.businessName);
                    resultBuild.Replace(ConstantSmoovs.EmailKeyword.LinkActiveMerchant, viewmodel.verifyLink);
                    bodyMail = resultBuild.ToString();
                    // string titleEmail = ConstantSmoovs.TitleEmail.RegisterSuccessfully + ConstantSmoovs.SmoovPosTeam;
                    bool isSend = sendEmailmerchant(ConstantSmoovs.ConfigMail.Admin, ConstantSmoovs.SmoovPosTeam, viewmodel.email, ConstantSmoovs.TitleEmail.VerifyAccountEmail, bodyMail);
                    // //-------------------
                    // clientEmail.Close();
                }
                catch
                {
                }
                clientMerchant.Close();
                //return isSend ? Json(new { success = true }) : Json(new { success = false });
                //return Json(new { success = true }, JsonRequestBehavior.AllowGet);

                return RedirectToAction("Index");
            }
            #endregion

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid id)
        {
            ViewBag.Class = "management";
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var model = client.WSGetMerchantManagement(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var viewmodel = Mapper.Map<MerchantManagementModel, MerchantManagementViewModel>(model);
            InitData(viewmodel);
            client.Close();
            return View(viewmodel);
        }

        /// <summary>
        /// Edits the specified viewmodel.
        /// </summary>
        /// <param name="viewmodel">The viewmodel.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        [HttpPost]
        public ActionResult Edit(MerchantManagementViewModel viewmodel)
        {
            if (viewmodel.merchantID == Guid.Empty)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            StringHelper.Trim(viewmodel);

            IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
            var result = clientMerchant.WSGetMerchantManagement(viewmodel.merchantID);
            if (result != null)
            {
                viewmodel.bucket = result.bucket;

                viewmodel.fullName = string.Format("{0} {1}", viewmodel.firstName, viewmodel.lastName);
                viewmodel.gender = (viewmodel.title == ConstantSmoovs.Titles.Mr || viewmodel.title == ConstantSmoovs.Titles.Dr);
                viewmodel.modifyDate = DateTime.Now;
                viewmodel.modifyUser = User.Identity.GetUserId();
                if (string.IsNullOrEmpty(viewmodel.modifyUser))
                {
                    viewmodel.modifyUser = (new UserSession()).UserId;
                }
                viewmodel.servicePacketEndDate = result.servicePacketEndDate;
                viewmodel.servicePacketStartDate = result.servicePacketStartDate;
                viewmodel.verifyLink = result.verifyLink;
                viewmodel.status = result.status;
                viewmodel.proxyPort = result.proxyPort;

                var model = Mapper.Map<MerchantManagementViewModel, MerchantManagementModel>(viewmodel);
                model.titleHomePage = result.titleHomePage;

                var value = clientMerchant.WSUpdateMerchantManagement(model);
                clientMerchant.Close();
                if (value)
                {
                    //return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    return RedirectToAction("Detail", new { id = model.merchantID });
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        [Authorize(Roles = "Admin")]
        public ActionResult ChangePassword(Guid id)
        {
            ViewBag.Class = "management";
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var result = client.WSGetMerchantManagement(id);
            if (result == null)
            {
                return RedirectToAction("Index");
            }
            var model = new ChangePasswordMerchantManagementViewModel();

            model = Mapper.Map<MerchantManagementModel, ChangePasswordMerchantManagementViewModel>(result);
            client.Close();
            return View(model);
        }
        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordMerchantManagementViewModel model)
        {
            ViewBag.Class = "management";
            bool isSend = false;
            var user = UserManager.FindById(model.userID);
            if (user != null)
            {
                UserManager.RemovePassword(model.userID);

                UserManager.AddPassword(model.userID, model.NewPassword);
            }
            ViewBag.ChangePasswordSuccess = true;

            //----------------
            if (!string.IsNullOrWhiteSpace(model.userID))
            {
                IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                var modelMerchant = client.WSGetMerchantManagementByUserId(model.userID);
                client.Close();
                try
                {
                    if (modelMerchant != null)
                    {
                       
                        string bodyMail = "";
                        StringBuilder resultBuild;
                        bodyMail = RenderRazorViewToStringNoModel("~/Views/EmailTemplate/TempleResetPassword.cshtml");
                        resultBuild = new StringBuilder(bodyMail);
                        resultBuild.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfMerchant, !string.IsNullOrWhiteSpace(modelMerchant.firstName) ? modelMerchant.firstName : "");

                        bodyMail = resultBuild.ToString();
                        isSend = sendEmailmerchant(ConstantSmoovs.ConfigMail.Admin, ConstantSmoovs.SmoovPosTeam, !string.IsNullOrWhiteSpace(modelMerchant.email) ? modelMerchant.email : "", ConstantSmoovs.TitleEmail.ResetPassword1, bodyMail);
                    }


                }
                catch
                {
                }
            }
            // send email
            return RedirectToAction("Detail", new { id = model.merchantID });
            //return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Resends the specified email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="verifyLink">The verify link.</param>
        /// <param name="bucket">The bucket.</param>
        /// <param name="firstName">The first name.</param>
        /// <returns></returns>
        /// <author>
        /// "Dao.Nguyen"
        /// </author>
        /// <datetime>1/15/2015-3:17 PM</datetime>
        [HttpPost]
        public ActionResult Resend(string email, string verifyLink, string bucket, string firstName)
        {
           
            string bodyMail = "";
            StringBuilder resultBuild;
            bodyMail = RenderRazorViewToStringNoModel("~/Views/EmailTemplate/VerifyEmail.cshtml");
            resultBuild = new StringBuilder(bodyMail);
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfMerchant, firstName);
            resultBuild.Replace(ConstantSmoovs.EmailKeyword.LinkActiveMerchant, verifyLink);
            bodyMail = resultBuild.ToString();
            bool isSend = sendEmailmerchant(ConstantSmoovs.ConfigMail.Admin, ConstantSmoovs.SmoovPosTeam, email, ConstantSmoovs.TitleEmail.VerifyAccountEmail, bodyMail);

            return isSend ? Json(new { success = true }) : Json(new { success = false });
        }
        /// <summary>
        /// Determines whether [is existed email] [the specified email].
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="merchantID">The merchant identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        public JsonResult IsExistedEmail(string email, Guid? merchantID)
        {
            if (string.IsNullOrEmpty(email))
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var isExists = client.WSExistMerchantManagementByEmail(email, merchantID);
            if (!isExists)
            {
                ApplicationDbContext _db = new ApplicationDbContext();
                var user = UserManager.FindByEmail(email);
                isExists = user != null;
            }
            client.Close();
            return new JsonResult
            {
                Data = !isExists,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        /// <summary>
        /// Determines whether [is existed business name] [the specified business name].
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="merchantID">The merchant identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        public JsonResult IsExistedBusinessName(string businessName, Guid? merchantID)
        {
            if (string.IsNullOrEmpty(businessName))
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var isExists = client.WSExistMerchantManagementByBusinessName(businessName, merchantID);
            client.Close();
            return new JsonResult
            {
                Data = !isExists,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }



        /// <summary>
        /// Sends the emailmerchant.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="merchantName">Name of the merchant.</param>
        /// <param name="email">The email.</param>
        /// <param name="titleEmail">The title email.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        public bool sendEmailmerchant(int type, string merchantName, string email, string titleEmail, string body)
        {
            MailServerUtil sendMail = new MailServerUtil();
            // bool isSend2 = sendMail.SendMail(2, merchantName, email, titleEmail, body);
            bool isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
            // bool isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(2, merchantName, "zinhumgato@gmail.com", titleEmail, body) : false;
            return isSend;
        }
        /// <summary>
        /// Searches the merchant.
        /// </summary>
        /// <param name="pageGoTo">The page go to.</param>
        /// <param name="sort">The sort.</param>
        /// <param name="desc">if set to <c>true</c> [desc].</param>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        [HttpPost]
        public ActionResult SearchMerchant(int pageGoTo, string sort, bool desc, string search)
        {
            //Thread.Sleep(20000);
            var searchModel = new SearchMerchantManagementModel() { pageGoTo = pageGoTo, sort = sort, desc = desc, fullName = search };
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var results = client.WSGetMerchantManagementByFilter(searchModel);
            results.fullName = search;
            client.Close();
            return PartialView("MerchantListing", results);
        }

        public JsonResult GetListBillingCycleByServicePackage(string id)
        {
            IWSServicePacketClient client = new IWSServicePacketClient();
            var servicepackage = client.WSGetServicePacket(Guid.Parse(id));
            client.Close();
            return Json(servicepackage.BillingServicePackets.OrderBy(b => b.moneyPerUnit), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewList(string id)
        {
            ViewBag.Class = "management";

            SearchMerchantManagementModel searchModel = new SearchMerchantManagementModel();
            searchModel.pageGoTo = 0;
            searchModel.sort = string.Empty;
            searchModel.fullName = id;

            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var model = client.WSGetMerchantManagementByFilter(searchModel);
            client.Close();
            return View("Index", model);
        }
    }
}