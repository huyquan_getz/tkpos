﻿using AutoMapper;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.CustomHTMLHelper;
using SmoovPOS.Utility.WSFunctionMethodReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmoovPOS.Common;
using Com.SmoovPOS.Model;

namespace SmoovPOS.UI.Controllers
{
    public class FunctionMethodController : Controller
    {
        // GET: FunctionMethod
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-4:21 PM</datetime>

        [Authorize(Roles = "Admin")]
        public ActionResult Index(SearchFunctionMethodModel searchModel)
        {
            ViewBag.Class = "functionMethod";

            if (searchModel.pageGoTo == 0)
            {
                searchModel.pageGoTo = 1;
                searchModel.sort = string.Empty;
            }
            WSFunctionMethodClient client = new WSFunctionMethodClient();
            var model = client.WSGetFunctionMethodByFilter(searchModel);
            client.Close();
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchFunction(int pageGoTo, string sort, bool desc, string search)
        {
            //Thread.Sleep(20000);
            var searchModel = new SearchFunctionMethodModel() { pageGoTo = pageGoTo, sort = sort, desc = desc, name = search };
            WSFunctionMethodClient client = new WSFunctionMethodClient();
            var results = client.WSGetFunctionMethodByFilter(searchModel);
            results.name = search;
            client.Close();
            return PartialView("FunctionMethodListing", results);
        }
        /// <summary>
        /// Determines whether [is existed name] [the specified name].
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="functionMethodID">The function method identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-4:21 PM</datetime>
        public JsonResult IsExistedName(string name, Guid? functionMethodID)
        {
            if (string.IsNullOrEmpty(name))
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            WSFunctionMethodClient client = new WSFunctionMethodClient();
            var isExists = client.WSExistFunctionMethodByName(name, functionMethodID);
            client.Close();
            return new JsonResult
            {
                Data = !isExists,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.Class = "functionMethod";
            var model = new FunctionMethodViewModel();
            InitData(model);
            return View(model);
        }

        private void InitData(FunctionMethodViewModel model)
        {
            model.dataTypes = new List<SelectListItem>() { 
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.MB, Value=ConstantSmoovs.FunctionMethod.MB},
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.GB, Value=ConstantSmoovs.FunctionMethod.GB},
            };


            model.keyMethods = new List<SelectListItem>() { 
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.SelectModule, Value=""},
                //new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.OrderList, Value=ConstantSmoovs.FunctionMethod.OrderList},
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.Report, Value=ConstantSmoovs.FunctionMethod.Report},
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.Category, Value=ConstantSmoovs.FunctionMethod.Category},
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.Collection, Value=ConstantSmoovs.FunctionMethod.Collection},
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.Product, Value=ConstantSmoovs.FunctionMethod.Product},
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.Discount, Value=ConstantSmoovs.FunctionMethod.Discount},
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.Branch, Value=ConstantSmoovs.FunctionMethod.Branch},
                new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.Employee, Value=ConstantSmoovs.FunctionMethod.Employee},
                //new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.Marketing, Value=ConstantSmoovs.FunctionMethod.Marketing}
                //new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.Customer, Value=ConstantSmoovs.FunctionMethod.Customer},
                //new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.WebContent, Value=ConstantSmoovs.FunctionMethod.WebContent},
                //new SelectListItem(){Text=ConstantSmoovs.FunctionMethod.MessageBox, Value=ConstantSmoovs.FunctionMethod.MessageBox},
            };
        }
        /// <summary>
        /// Creates the specified viewmodel.
        /// </summary>
        /// <param name="viewmodel">The viewmodel.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:28 PM</datetime>
        [HttpPost]
        public ActionResult Create(FunctionMethodViewModel viewmodel)
        {
            StringHelper.Trim(viewmodel);
            #region create Function

            //create Function
            WSFunctionMethodClient clientFunction = new WSFunctionMethodClient();
            //var bn = viewmodel.businessName.Replace(" ", string.Empty).ToLower();//remove space and to lower

            viewmodel.createdDate = DateTime.Now;
            viewmodel.userOwner = User.Identity.GetUserId();

            viewmodel.display = true;

            var model = Mapper.Map<FunctionMethodViewModel, FunctionMethodModel>(viewmodel);
            var value = clientFunction.WSCreateFunctionMethod(model);

            clientFunction.Close();
            if (value != null && value != Guid.Empty)
            {
                return RedirectToAction("Index");
            }
            #endregion

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Edits the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/3/2015-10:08 AM</datetime>
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid id)
        {
            ViewBag.Class = "functionMethod";
            WSFunctionMethodClient client = new WSFunctionMethodClient();
            var model = client.WSGetFunctionMethod(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var viewmodel = Mapper.Map<FunctionMethodModel, FunctionMethodViewModel>(model);
            InitData(viewmodel);
            client.Close();
            return View(viewmodel);
        }
        /// <summary>
        /// Edits the specified viewmodel.
        /// </summary>
        /// <param name="viewmodel">The viewmodel.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/3/2015-10:08 AM</datetime>
        [HttpPost]
        public ActionResult Edit(FunctionMethodViewModel viewmodel)
        {
            if (viewmodel.functionMethodID == Guid.Empty)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            StringHelper.Trim(viewmodel);

            WSFunctionMethodClient clientMerchant = new WSFunctionMethodClient();
            var result = clientMerchant.WSGetFunctionMethod(viewmodel.functionMethodID);
            if (result != null)
            {
                viewmodel.modifyDate = DateTime.Now;
                viewmodel.modifyUser = User.Identity.GetUserId();

                var model = Mapper.Map<FunctionMethodViewModel, FunctionMethodModel>(viewmodel);

                var value = clientMerchant.WSUpdateFunctionMethod(model);
                clientMerchant.Close();
                if (value)
                {
                    return RedirectToAction("Detail", new { id = model.functionMethodID });
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Detail(Guid id)
        {
            ViewBag.Class = "functionMethod";
            WSFunctionMethodClient client = new WSFunctionMethodClient();
            var model = client.WSGetFunctionMethod(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            client.Close();
            var viewmodel = Mapper.Map<FunctionMethodModel, FunctionMethodViewModel>(model);
            if (!string.IsNullOrEmpty(viewmodel.description))
            {
                viewmodel.description = viewmodel.description.Replace("\r\n", "<br />");
            }
            return View(viewmodel);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Guid id)
        {
            ViewBag.Class = "functionMethod";
            WSFunctionMethodClient client = new WSFunctionMethodClient();

            var statusCode = client.WSDeleteFunctionMethod(id);

            client.Close();
            return RedirectToAction("Index");
        }
    }
}