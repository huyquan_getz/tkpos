﻿using AutoMapper;
using Com.SmoovPOS.Model;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.AutoMapperProfiles
{
    public class ModelMappingProfile : AutoMapper.Profile
    {
        public override string ProfileName
        {
            get { return "ModelMappingProfile"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<MerchantManagementModel, ChangePasswordMerchantManagementViewModel>();
            Mapper.CreateMap<MerchantManagementModel, MerchantManagementViewModel>();
            Mapper.CreateMap<MerchantManagementViewModel, MerchantManagementModel>();
            Mapper.CreateMap<FunctionMethodViewModel, FunctionMethodModel>();
            Mapper.CreateMap<FunctionMethodModel, FunctionMethodViewModel>();
            Mapper.CreateMap<MailServerModel, DefaultEmailViewModel>();
            Mapper.CreateMap<DefaultEmailViewModel, MailServerModel>();
            Mapper.CreateMap<ServicePackageModel, ServicePackageViewModel>();
            Mapper.CreateMap<ServicePackageViewModel, ServicePackageModel>();
        }
    }
}