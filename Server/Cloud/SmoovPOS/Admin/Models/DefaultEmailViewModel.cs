﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class DefaultEmailViewModel
    {
        public string Id { get; set; }
        [LocalizedRequired]
        public string IncomingPop { get; set; }
        [LocalizedRequired]
        [RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        [LocalizedStringLength(5, MinimumLength = 1, ErrorMessage = "IncomingPort")]
        public int IncomingPort { get; set; }
        [LocalizedRequired]
        [EmailAddress(ErrorMessage = "Please enter a valid email address.")]
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        [LocalizedRequired]
        public string PassWord { get; set; }
        [LocalizedRequired]
        public string OutgoingSmtp { get; set; }
        //[Required(ErrorMessage = "Field Port is required")]
        // [Range(0, int.MaxValue, ErrorMessage = "Field Port must be a number")]
        //[StringLength(5)]
        [LocalizedRequired]
        [RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        [LocalizedStringLength(5, MinimumLength = 1, ErrorMessage = "OutgoingPort")]
        public int OutgoingPort { get; set; }
        public bool EnableSsl { get; set; }
        public bool Credentials { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid? merchantID { get; set; }
        public string emailAccount1 { get; set; }
        public string emailAccount2 { get; set; }
        public int type { get; set; }
    }
}