﻿using Com.SmoovPOS.Model;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models
{
    public class ServicePackageViewModel
    {
        public System.Guid servicePacketID { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("servicePacketName_Label")]
        [LocalizedRemote("IsExistedName", "ServicePackage", AdditionalFields = "servicePacketID", ResourceCode = "ServicePackageNameExisted")]
        public string servicePacketName { get; set; }
        [LocalizedDisplayName("description")]
        public string description { get; set; }

        public System.Nullable<System.DateTime> servicePacketStartDate { get; set; }

        public System.Nullable<System.DateTime> servicePacketExpiredDate { get; set; }

        public System.Nullable<bool> status { get; set; }

        public string bussinessPlan { get; set; }

        public string userOwner { get; set; }

        public System.DateTime createdDate { get; set; }

        public string modifyUser { get; set; }

        public System.Nullable<System.DateTime> modifyDate { get; set; }

        public bool display { get; set; }
        public bool isTrial { get; set; }
        public int trialDays { get; set; }
        public string image { get; set; }
        public bool isUnlimit { get; set; }
        public List<SelectListItem> billingCycles { get; set; }

        public List<BillingServicePackageModel> BillingServicePackets { get; set; }

        public List<FunctionServicePackageModel> FunctionServicePackets { get; set; }
        public int NoOfMerchants { get; set; }
    }
}