﻿using AutoMapper;
using SmoovPOS.UI.AutoMapperProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterProfiles()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<ModelMappingProfile>();
            });
        }
    }
}