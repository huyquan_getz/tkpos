//
//  PrinterController.m
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "PrinterController.h"
#import "PrinterManagement.h"
@implementation PrinterController
{
     PrinterManagement *printManager;
}
- (id)init
{
    self = [super init];
    if (self) {;
       printManager = [[PrinterManagement alloc] init];
    }
    return self;
}
+ (PrinterController*)sharedInstance
{
    static PrinterController *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[PrinterController alloc] init];
    });
    return _sharedInstance;
}

- (NSArray*)getAllPrintData{
    return [printManager getAllPrintData];
}
- (void) closeAllPrinter {
    [printManager closeAllPrinter];
}
// get all printer in local network
- (NSArray*)getAllPrintLocal {
    return [printManager listPrint];
}
- (void)processingPrint{
    [printManager processingPrint];
}
- (void)handlePrint{
    [printManager handlePrint];
}
- (void)updatePrintData:(PrintData*)printData atIndex:(int)index{
    [printManager updatePrintData:printData atIndex:index];
}
- (void)addPrintData:(PrintData*)printData{
    [printManager addPrintData:printData];
}
- (BOOL)checkConnectionWithIPAddress:(NSString *)ipAddress port:(NSInteger)port{
    return [printManager checkConnectionWithPrinterWithIP:ipAddress port:port];
}
- (void)onCoverOpen:(NSString *)deviceName
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"The cover of printer has been opened.Please check it." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    });
    
}
- (void)addPrintWithImage:(UIImage*)image Height:(float)height Ip:(NSString*)ip Type:(int)type Name:(NSString*)name Infor:(NSDictionary*)dictInfor{
    PrintData *printData=[[PrintData alloc]initWithImage:image Height:height Ip:ip Type:type Name:name Infor:dictInfor];
    [self updatePrintData:printData atIndex:99999];
}
#pragma mark - Cashdrawer
- (BOOL)openDrawerOpenWithIP:(NSString*)_ip {
    return  [printManager openDrawerWithIP:_ip];
}

@end
