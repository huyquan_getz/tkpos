//
//  Controller.h
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpClient.h"
#import "WebserviceJSON.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "PinCodeVC.h"
#import "EmailData.h"
#import "StationOrdering.h"
#import "SMSTemplate.h"

@interface Controller : NSObject<PinCodeDelegate>{
    BOOL isLogined;
}
+(instancetype)sharedObject;
@property (readonly) BOOL isLogined;
-(void)setLoginStatus:(BOOL)loginStatus;
-(void)showPincodeSecurity;
-(void)hidePincodeSecurity;


+(void)startSyncFirstWithStore:(NSDictionary*)store;
// show view same UIController
+(void)showLeftMenu:(id)sender;
+(void)showLeftMenuWithCompletion:(void (^)(BOOL finished))completion;
+(void)showCenterView;
+(void)showCenterViewWithCompletion:(void (^)(BOOL finished))completion;
+(void)showMerchantLoginFirst;
+(void)showCashierLoginView;
+(void)showTableOrdering;
+(void)showSettingMgt;

+(void)showStoreLoginViewWithStores:(NSArray*)listStore;
+(void)showForgotLoginView;
+(void)showManagerLoginView;
+(void)showWelcomeView;
+(void)startListenerStationOrdering;

+(void)restartSyncPull;
// function for save information data to local
+(void)saveMerchantSyncInfor:(NSDictionary*)dataSyncInfor;
+(void)saveUserLogined:(CBLDocument*)document;
+(CBLDocument*)getUserLoginedDoc;
+(CBLDocument*)getStoreInfoDoc;
+(CBLDocument*)getGeneralSetting;
+(CBLDocument*)getTaxServiceSetting;
+(BOOL)resetToken;

// check information
+(NSDictionary*)checkMerchantWithBusinessName:(NSString*)businessName username:(NSString*)username password:(NSString*)password;
+(BOOL)checkUserMerchantLocalWithUsername:(NSString*)username andPassword:(NSString*)string;

//Get Data from couchbaselite
+(CBLQuery*)queryGetAllCategoryDocuments;
+(CBLQuery*)queryAllProductByCategoryIDAndSortKeyMain;
+(CBLQuery*)queryAllProductSortKeyMain;
+(CBLQuery*)querySearchProductItemDocumentsWithKeySearch:(NSString*)keySearch;

+(CBLQuery *)queryTKGetReceiptLimitDateInMonth:(NSInteger)numberMonthBefore; // limit 3month or 90 date
+(CBLQuery*)querySearchReceiptWithTitleKey:(NSString *)titleSearch;

+(CBLQuery *)queryGetAllOrderWaiting;

+(CBLDocument*)employeeWithPinCode:(NSString*)pincode;
+(CBLQuery *)queryShiftDrawerInYear:(NSInteger)year month:(NSInteger)month;
+(CBLQuery*)queryGetAllPrinter;
+(CBLQuery*)queryGetAllPrinting;
+(CBLQuery*)queryAllTaxService;

+(CBLQuery*)queryAllStation;
+(CBLQuery*)queryAllOrdering;

+(CBLDocument*)productWithSKU:(NSString*)sku;
+(CBLDocument*)getTableOrderingSetting;
+(CBLDocument*)getListDevicesSetting;
//sent mail
+(NSInteger)sendEmailWithData:(NSDictionary*)dataSend toEmail:(NSString*)email type:(MailType)mailType;

//sent SMS
+(NSInteger) sendNotifySMS:(NSMutableDictionary*) paramDic;
+(void)sendSMS:(CBLDocument *)ordering typeSMS:(TypeMessageSMS)typeSMS message:(NSString*)message;

// update status ordering
+(BOOL)clearStationOrdering:(StationOrdering*)stationOrdering;
+(BOOL)updateOrdering:(CBLDocument*)ordering toStatus:(OrderStatus)newStatus;
+(void)changeStatusWaitingToConfirm:(NSArray *)arrayOrderings;

// setting noti send mail, sms
+(BOOL)allowSendMail;
+(BOOL)allowSendSMS;
+(BOOL)allowShowWhenHaveNewOrder;
+(void)sendUpdateStatusOrderToServerWithOrderChanged:(CBLDocument*)orderChanged;
+(void)updateClearSationToServerWithSationId:(NSString*) stationId tmpOrderId:(NSString*)tmpOrderId;
+(BOOL)requestResetApplicationNumberBagde;

// real time webservice
+(int)webserviceRealTimeWithURL:(NSString*)url parameter:(NSDictionary*)param;
//appDelegate instan
+(AppDelegate*)appDelegate;

+(NSInteger)storeIndexDefault;
+(NSString*)currencyDefault;
// update

//refund Order
+(void)sendNotifyWhenAmendOrder:(CBLDocument*)order withMessage:(NSString*)message;
+(NSInteger)refundOrderWithReferenceCode:(NSString*)referenceCode refundAmount:(double)refundAmount remarks:(NSString*)remarks;
+(void)updateRefundOrder:(CBLDocument *)orderDoc withArrayPaymentRefundJSON:(NSArray*)arrayPaymentRefundJSON reason:(NSString*)reason;

//device
+(NSDictionary*)getDeviceInfo;
+(NSString*)getDeviceToken;
+(void)saveDeviceToken:(NSString*)deviceToken;

+(BOOL)checkThisDeviceAvailable;
+(NSInteger)checkAndAddDeviceToServer;
@end
