//
//  CouchbaseViewController.h
//  POS
//
//  Created by Cong Nha Duong on 2/6/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
typedef enum _ViewType{
    ViewTypeSearchEmloyeeID=0,
//    ViewTypeAllCategory=1,
//    ViewTypeAllShiftDrawer=2,
//    ViewTypeAllProductSortByCategorySortKeyNormal=3,
//    ViewTypeAllProduct=4,
//    ViewTypeSearchProduct=5,
//    ViewTypeAllReceiptByLimitTime=6,
//    ViewTypeSearchReceiptByOrderCode=7,
//    ViewTypeAllOrderWaiting=8,
    ViewTypeAllPrinter=9,
    ViewTypeAllPrinting=10,
//    ViewTypeSearchProductBySKU=11,
    ViewTypeTaxService=12,
    ViewTypeGeneralSetting =13,
    ViewTypeAllStation=14,
    ViewTypeAllTableOrderingSetting=15,
    ViewTypeAllOrdering=16,
    ViewTypeAllListDevices=17,
    
}ViewType;

#import <Foundation/Foundation.h>

@interface CouchbaseViewController : NSObject
+(instancetype)sharedObject;
-(BOOL)reloadAllView;
-(BOOL)reloadView:(ViewType)viewType;
@end
