//
//  Controller.m
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "Controller.h"
#import "WebserviceJSON.h"
#import "LeftMenuController.h"
#import "CouchBaseBusinessModel.h"
#import "UserDefaultModel.h"
#import "UIAlertView+Blocks.h"
#import "LoginCashierVC.h"
#import "MBProgressHUD.h"
#import "MBProgressHUDListener.h"
#import "SyncingListener.h"
#import "LoginChooseStoreVC.h"
#import "LoginForgotVC.h"
#import "LoginManagerVC.h"
#import "WelcomeVC.h"
#import "LoginVC.h"
#import "UtilBusinessModel.h"
#import "TKEmployee.h"
#import "SettingVC.h"
#import "CouchbaseViewController.h"
#import "TableOrderingVC.h"
#import "TKOrder.h"
#import "SMSTemplate.h"
#import "TableOrderingListener.h"
#import "TKTableOrderingSetting.h"
#import "QueueSendMail.h"
#import "PaymentChild.h"
#import "PaymentBusinessModel.h"
#import "TKListDevicesSetting.h"
#import "QueueWebserviceRealTime.h"

@implementation Controller{
    PinCodeVC *pincodeSecurity;
}
@synthesize isLogined;
+(instancetype)sharedObject{
    __strong static Controller *_controller;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _controller=[[Controller alloc] init];
    });
    return _controller;
}
-(instancetype)init{
    if (self=[super init]) {
        isLogined=NO;
    }
    return self;
}
-(void)setLoginStatus:(BOOL)loginStatus{
    isLogined=loginStatus;
}
-(void)showPincodeSecurity{
    if (pincodeSecurity) {
        [pincodeSecurity.view removeFromSuperview];
        [pincodeSecurity removeFromParentViewController];
        pincodeSecurity=nil;
    }
    CBLDocument *userLogined=[Controller getUserLoginedDoc];
    pincodeSecurity=[[PinCodeVC alloc] initWithEmployee:userLogined checkLogin:NO];
    pincodeSecurity.delegate=self;
    UIViewController *mainview=[Controller appDelegate].slideMenuController;
    [mainview addChildViewController:pincodeSecurity];
    [mainview.view addSubview:pincodeSecurity.view];
}
-(void)pinCodeVerify:(id)sender switchUser:(BOOL)switchUser{
    [self hidePincodeSecurity];
    [Controller showCashierLoginView];
}
-(void)pinCodeVerify:(id)sender completed:(BOOL)completed{
    [self hidePincodeSecurity];
}
-(void)hidePincodeSecurity{
    if (pincodeSecurity) {
        [pincodeSecurity.view removeFromSuperview];
        [pincodeSecurity removeFromParentViewController];
        pincodeSecurity=nil;
    }
}
//===============================================
//static

+(void)showLeftMenuWithCompletion:(void (^)(BOOL))completion{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    if (!appDelegate.slideMenuController.leftDrawerViewController) {
        [appDelegate.slideMenuController setLeftDrawerViewController:[LeftMenuController sharedLeftMenu]];
    }
    [appDelegate.slideMenuController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:completion];
}

+(void)showLeftMenu:(id)sender{
    if ([sender isKindOfClass:[UIViewController class]]) {
        [[(UIViewController*)sender view] endEditing:YES];
    }
    [Controller showLeftMenuWithCompletion:nil];
}
+(void)showCenterViewWithCompletion:(void (^)(BOOL))completion{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    if (!appDelegate.slideMenuController.centerViewController) {
        UIViewController *emptyView=[[UIViewController alloc] init];
        [appDelegate.slideMenuController setCenterViewController:emptyView withCloseAnimation:YES completion:completion];
    }else{
        [appDelegate.slideMenuController closeDrawerAnimated:YES completion:completion];
    }
}
+(void)showCenterView{
    [Controller showCenterViewWithCompletion:nil];
}
+(void)showForgotLoginView{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[LoginForgotVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showMerchantLoginFirst{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[LoginVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showWelcomeView{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[WelcomeVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showManagerLoginView{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[LoginManagerVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showStoreLoginViewWithStores:(NSArray *)listStore{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    LoginChooseStoreVC *chooseStore=[[LoginChooseStoreVC alloc]initWithArrayStore:listStore];
    [appDelegate.slideMenuController setCenterViewController:chooseStore withCloseAnimation:YES completion:nil];
}
+(void)showCashierLoginView{
    [[Controller sharedObject] setLoginStatus:NO];
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[LoginCashierVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showTableOrdering{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[TableOrderingVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showSettingMgt{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[SettingVC alloc]init] withCloseAnimation:YES completion:nil];
}

//==============================================
+(void)startSyncFirstWithStore:(NSDictionary *)store{
    LanguageUtil *languageKey=[LanguageUtil sharedLanguageUtil];
    // get all channels
//    [UtilBusinessModel addChannels:store[tkKeyChannels]]; // store chanel to list chanels to sync
    NSDictionary *dic=[UserDefaultModel getMerchantSyncInfor];
    NSString *merchantID=dic[@"merchantID"];
    int branchIndex=[store[@"index"] intValue];
    NSArray *channelsBranch=@[[NSString stringWithFormat:@"%@_%d",merchantID,branchIndex],
                              [NSString stringWithFormat:@"%@_%d_table",merchantID,branchIndex]];
    [UtilBusinessModel addChannels:channelsBranch];
    SyncManager *sync = [SyncManager sharedSyncManager];
    [sync resetSyncManager];
    [sync setSyncRemoteDefault];
    
    MBProgressHUD *hub=[[MBProgressHUD alloc]initWithView:[self appDelegate].slideMenuController.view];
    [[self appDelegate].slideMenuController.view addSubview:hub];
    hub.labelText=[languageKey stringByKey:@"login.controller.hub-syncing"];
    sync.progressHUBSync=hub;
    MBProgressHUDListener *hubListener=[[MBProgressHUDListener alloc] initWithHubHidenBlock:^{
        [hub removeFromSuperview];
    }];
    SyncingListener *syncingListener=[[SyncingListener alloc] initWithBeginBlock:nil endBlock:^(NSError *pullError, NSError *pushError) {
        if (pullError) {
            [sync stopPull];
            [sync stopPush];
            [sync deleteDatabaseAndWait:1000];
            UIAlertView *alertError=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"login.controller.title-sync-error"] message:[languageKey stringByKey:@"login.controller.ms-sync-error"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles:nil];
            [alertError show];
            
        }else{
            CBLDocument *merchantData= [sync documentWithDocumentId:store[@"employeeID"]];
            [UserDefaultModel saveAccountMerchant:merchantData.documentID];
            [UserDefaultModel saveStoreSelected:store[tkKeyId]];
            [UserDefaultModel saveFirstSyncCompleted:YES];
            [Controller saveUserLogined:merchantData];
            MBProgressHUD *hubPrepare=[[MBProgressHUD alloc]initWithView:[self appDelegate].slideMenuController.view];
            [[self appDelegate].slideMenuController.view addSubview:hubPrepare];
            hubPrepare.labelText=[languageKey stringByKey:@"login.controller.hub-prepare"];
            [hubPrepare show:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
                    [[CouchbaseViewController sharedObject] reloadAllView];
                    [hubPrepare hide:YES];
                    [self updateBranchIndexToServer];
                }];
                
            });
        }
    }];
    sync.delegate=syncingListener;
    sync.progressHUBSync.delegate=hubListener;
    sync.showHUBwhileSyncing=YES;
    [sync restartPull];
    [sync restartPush];
}
+(void)startCashier{
    [[LeftMenuController sharedLeftMenu] setSelectedCellType:CellLeftMenuTableOrdering withAction:YES];
}
+(void)updateBranchIndexToServer{
    LanguageUtil *languageKey=[LanguageUtil sharedLanguageUtil];
    MBProgressHUD *hub=[[MBProgressHUD alloc]initWithView:[self appDelegate].slideMenuController.view];
    [[self appDelegate].slideMenuController.view addSubview:hub];
    hub.labelText=[languageKey stringByKey:@"login.controller.hub-updating"];
    [hub show:YES];
    dispatch_queue_t myQueue=dispatch_queue_create("queue.resetDeviceToken", NULL);
    dispatch_async(myQueue, ^{
        NSInteger result=[Controller checkAndAddDeviceToServer];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hub hide:YES];
            if (result != ReturnCodeSuccess) {
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-message"] message:[languageKey stringByKey:@"login.controller.ms-no-update-device-info"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.try-again"] otherButtonTitles:nil];
                [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex==0) {
                        [self updateBranchIndexToServer];
                    }
                }];
            }else{
                [self startCashier];
            }
        });
    });
}
+(void)startListenerStationOrdering{
    [[TableOrderingListener sharedObject] startListener];
}
+(void)saveMerchantSyncInfor:(NSDictionary *)dataSyncInfor{
    if (dataSyncInfor[tkKeyCreatedDate]==nil) {
        NSMutableDictionary *newDict=[dataSyncInfor mutableCopy];
        [newDict setObject:[CBLJSON JSONObjectWithDate:[NSDate date]] forKey:tkKeyCreatedDate];
        dataSyncInfor=newDict;
    }
    [UserDefaultModel saveMerchantSyncInfor:dataSyncInfor];
}
+(NSDictionary*)checkMerchantWithBusinessName:(NSString *)businessName username:(NSString *)username password:(NSString *)password{
    NSDictionary *dict=[WebserviceJSON authenticationWithBusinessName:businessName username:username password:password];
    if ([dict[tkKeyReturnStatus] integerValue] == ReturnCodeSuccess) {
        // add modifired show
        NSMutableDictionary *newdict=[dict mutableCopy];
        NSMutableDictionary *newData=[dict[tkKeyResult] mutableCopy];
        [newData setObject:businessName forKey:tkKeyBusinessName];
        [newData setObject:username forKey:tkKeyUserName];
        [newData setObject:password forKey:tkKeyPassword];
        [newdict setObject:newData forKey:tkKeyResult];
        return newdict;
    }
    return dict;
}
+(BOOL)resetToken{
    NSMutableDictionary *dataSync=[[UserDefaultModel getMerchantSyncInfor] mutableCopy];
    NSDictionary * response=[WebserviceJSON getTokenWithBusinessName:dataSync[tkKeyBusinessName] username:dataSync[tkKeyUserName] password:dataSync[tkKeyPassword]];
    if ([response[tkKeyReturnStatus] integerValue] ==ReturnCodeSuccess) {
        [dataSync addEntriesFromDictionary:response[tkKeyResult]];
        [self saveMerchantSyncInfor:dataSync];
        return YES;
    }
    return NO;
}
+(BOOL)checkUserMerchantLocalWithUsername:(NSString *)username andPassword:(NSString *)password{
    NSString *merchantDocumentId=[UserDefaultModel getAccountMerchant];
    CBLDocument *merchantDocument=[[SyncManager sharedSyncManager] documentWithDocumentId:merchantDocumentId];
    NSDictionary *merchant=merchantDocument.properties;
    if ([username isEqualToString:merchant[tkKeyEmail]] && [password isEqualToString:merchant[tkKeyPassword]]) {
        return YES;
    }else{
        return NO;
    }
}
+(CBLQuery *)queryGetAllCategoryDocuments{
    return [CouchBaseBusinessModel queryGetAllCategory];
}
+(CBLQuery *)queryAllProductByCategoryIDAndSortKeyMain{
    return [CouchBaseBusinessModel queryAllProductByCategoryIDAndSortKeyMain];
}
+(CBLQuery *)queryAllProductSortKeyMain{
    return [CouchBaseBusinessModel queryAllProductSortKeyMain];
}
+(CBLQuery *)querySearchProductItemDocumentsWithKeySearch:(NSString *)keySearch{
    return [CouchBaseBusinessModel querySearchProductItemWithTitleKey:keySearch];
}
+(CBLQuery *)queryTKGetReceiptLimitDateInMonth:(NSInteger)numberMonthBefore{
    return [CouchBaseBusinessModel queryTKGetReceiptLimitDateInMonth:numberMonthBefore];
}
+(CBLQuery *)querySearchReceiptWithTitleKey:(NSString *)titleSearch{
    return [CouchBaseBusinessModel querySearchReceiptWithTitleKey:titleSearch];
}

+(CBLQuery *)queryGetAllOrderWaiting{
    return [CouchBaseBusinessModel queryGetAllOrderWaiting];
}

+(CBLQuery *)queryGetAllPrinter{
    return [CouchBaseBusinessModel queryGetAllPrinter];
}
+(CBLQuery *)queryGetAllPrinting{
    return [CouchBaseBusinessModel queryGetAllPrinting];
}
+(CBLDocument*)employeeWithPinCode:(NSString*)pincode{
    CBLQuery *query =[CouchBaseBusinessModel querySearchEmployeePinCode:pincode];
    CBLQueryEnumerator *enumQuery =[query run:nil];
    CBLDocument *employee=nil;
    if(enumQuery && enumQuery.count>0){
        employee=[enumQuery nextRow].document;
    }
    return employee;
}
+(CBLQuery *)queryShiftDrawerInYear:(NSInteger)year month:(NSInteger)month{
    return [CouchBaseBusinessModel queryShiftDrawerInYear:year month:month];
}
+(CBLQuery *)queryAllTaxService{
    return [CouchBaseBusinessModel queryAllTaxServiceCharge];
}
+(CBLQuery *)queryAllStation{
    return [CouchBaseBusinessModel queryAllStation];
}
+(CBLQuery *)queryAllOrdering{
    return [CouchBaseBusinessModel queryAllOrdering];
}
+(CBLDocument *)productWithSKU:(NSString *)sku{
    return [CouchBaseBusinessModel productWithSKU:sku];
}
+(CBLDocument *)getTaxServiceSetting{
    NSString *docID=[UserDefaultModel getTaxServiceSetting];
    if (docID) {
        return [[SyncManager sharedSyncManager] documentWithDocumentId:docID];
    }else{
        CBLDocument *docTax=[CouchBaseBusinessModel taxServiceSetting];
        [UserDefaultModel saveTaxtServiceSetting:docTax.documentID];
        return docTax;
    }
}
+(void)saveUserLogined:(CBLDocument *)dataUser{
    [[Controller sharedObject] setLoginStatus:YES];
    [UserDefaultModel saveUserLogined:dataUser.documentID];
}
+(CBLDocument*)getUserLoginedDoc{
    NSString *userLognedDocumnetID=[UserDefaultModel getUserLogined];
    return [[SyncManager sharedSyncManager] documentWithDocumentId:userLognedDocumnetID];
}
+(CBLDocument *)getStoreInfoDoc{
    NSString *storeID=[UserDefaultModel getStoreSelected];
    return [[SyncManager sharedSyncManager] documentWithDocumentId:storeID];
}
+(CBLDocument *)getGeneralSetting{
    NSString *docID=[UserDefaultModel getGeneralSetting];
    if (docID) {
        return [[SyncManager sharedSyncManager] documentWithDocumentId:docID];
    }else{
        CBLDocument *generalSetting=[CouchBaseBusinessModel generalSetting];
        [UserDefaultModel saveGeneralSetting:generalSetting.documentID];
        return generalSetting;
    }
}
+(CBLDocument *)getListDevicesSetting{
    NSString *docID=[UserDefaultModel getListDevicesSetting];
    CBLDocument *doc;
    if (docID) {
        doc=[[SyncManager sharedSyncManager] documentWithDocumentId:docID];
    }
    if(doc==nil || doc.isDeleted){
        doc=[CouchBaseBusinessModel listDevicesSetting];
        if (doc) {
            [UserDefaultModel saveListDevicesSetting:doc.documentID];
        }
    }
    return doc;
}
+(NSInteger)storeIndexDefault{
    CBLDocument *store=[self getStoreInfoDoc];
    if (store) {
        return [store.properties[tkKeyIndexStore] integerValue];
    }
    return -1;
}
+(NSString *)currencyDefault{
    static NSString *currency;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CBLDocument *generalSetting=[self getGeneralSetting];
        currency=generalSetting.properties[tkKeyCurrency];
    });
    return currency;
}

+(NSInteger)sendEmailWithData:(NSDictionary *)dataSend toEmail:(NSString *)email type:(MailType)mailType{
    if (dataSend==nil || ![email isEmailFormat]) {
        return NO;
    }
    NSDictionary *response=[WebserviceJSON sendEmailWithData:dataSend toEmail:email type:mailType];
    return ([response[tkKeyReturnStatus] integerValue]);
}

+(NSInteger) sendNotifySMS:(NSMutableDictionary*) paramDic
{
    NSDictionary *response=[WebserviceJSON sendNotifySMS:paramDic];
    return [response[tkKeyReturnStatus] integerValue];
}
+(void)sendSMS:(CBLDocument *)ordering typeSMS:(TypeMessageSMS)typeSMS message:(NSString*)message
{
    NSMutableDictionary *param=[[NSMutableDictionary alloc] init];
    NSString *phoneSend=[TKOrder customerReceiverPhoneCall:ordering];
    [param setObject:@"797995c474ff2eaf46bcb49b4d830046" forKey:@"apikey"];
    NSDictionary *customerReceiver=[TKOrder customerReceiver:ordering];
    NSString *messageSMS = [SMSTemplate messageOfOrder:typeSMS firstName:customerReceiver[tkKeyFirstName] tableName:[TKOrder getStationName:ordering] message:message];
    [param setObject:messageSMS forKey:tkKeyMessage];
    [param setObject:phoneSend forKey:tkKeyRecipients];
    [param setObject:@"text" forKey:tkKeyEncoding];
    NSInteger result=[self sendNotifySMS:param];
    if (result==ReturnCodeSuccess) {
        [SNLog Log:@"send SMS success order:%@ status:%i",ordering.documentID,[TKOrder orderStatus:ordering]];
    }else{
        [SNLog Log:@"send SMS fail order:%@ status:%i",ordering.documentID,[TKOrder orderStatus:ordering]];
    }
}
+(BOOL)requestResetApplicationNumberBagde{
    NSDictionary *response=[WebserviceJSON resetApplicationNumberBadge];
    if ([response[tkKeyReturnStatus] integerValue] !=ReturnCodeSuccess && [response[tkKeyReturnStatus] integerValue] != ReturnCodeSuccessNotUpdate) {
        response =[WebserviceJSON resetApplicationNumberBadge];
    }
    return ([response[tkKeyReturnStatus] integerValue] == ReturnCodeSuccess || [response[tkKeyReturnStatus] integerValue] == ReturnCodeSuccessNotUpdate);
}
+(AppDelegate *)appDelegate{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    return appDelegate;
}

+(CBLDocument *)getTableOrderingSetting{
    NSString *docID=[UserDefaultModel getTableOrderingSetting];
    if (docID) {
        return [[SyncManager sharedSyncManager] documentWithDocumentId:docID];
    }else{
        CBLQuery *query=[CouchBaseBusinessModel queryTableOrderingSetting];
        query.prefetch=YES;
        CBLQueryEnumerator *queryEnum=[query run:nil];
        CBLDocument *docBranch=[self getStoreInfoDoc];
        CBLDocument *docTableOrderingSetting;
        for (CBLQueryRow *row in queryEnum) {
            if ([row.document.properties[@"branchId"] isEqualToString:docBranch.documentID]) {
                docTableOrderingSetting=row.document;
                break;
            }
        }
        if (docTableOrderingSetting) {
            [UserDefaultModel saveTableOrderingSetting:docTableOrderingSetting.documentID];
        }
        return docTableOrderingSetting;
    }
}
+(BOOL)clearStationOrdering:(StationOrdering*)stationOrdering{
    if (stationOrdering.order) {
        TypeStatusOrdering status=[stationOrdering getTypeStatusStationOrdering];
        if (status == TypeStatusOrderingServing) {
            [self updateOrdering:stationOrdering.order toStatus:OrderStatusDineInDone];
        }else{
            [self updateOrdering:stationOrdering.order toStatus:OrderStatusCanceled];
        }
    }else{
        NSString *tmpOrderId=[TKStation orderId:stationOrdering.station];
        [TKStation clearStation:stationOrdering.station];
        [self updateClearSationToServerWithSationId:stationOrdering.station.documentID tmpOrderId:tmpOrderId];
    }
    return YES;
}
+(BOOL)updateOrdering:(CBLDocument*)ordering toStatus:(OrderStatus)newStatus
{
    NSString *messToServer;
    OrderStatus oldStatus=[TKOrder orderStatus:ordering];
    MailType mailType=MailTypeNone;
    TypeMessageSMS messageType=TypeMessageSMSNone;
   
    CBLDocument *userLogined=[Controller getUserLoginedDoc];
    NSArray *arrayStatusHistory=[[TKOrder arrayStatusHistory:ordering] mutableCopy];
    NSMutableArray *newArrayStatusHistory=[[NSMutableArray alloc] init];
    if (arrayStatusHistory) {
        [newArrayStatusHistory addObjectsFromArray:arrayStatusHistory];
    }
    if (oldStatus != newStatus) {// check have change to new status
        [newArrayStatusHistory addObject:@{tkKeyOrderStatus:@(newStatus),tkKeyModifiedDate:[CBLJSON JSONObjectWithDate:[NSDate date]]}];
    }
    [[SyncManager sharedSyncManager] updateDocument:ordering propertyDoc:@{tkKeyOrderStatus:@(newStatus),tkKeyArrayStatusHistory:newArrayStatusHistory,tkKeyModifiedDate:[CBLJSON JSONObjectWithDate:[NSDate date]],tkKeyStaff:userLogined.properties}];
    switch (newStatus) {
        case OrderStatusDineInConfirm:{
            messToServer =@"ConfirmedTableOrder";
            mailType = MailTypeOrderingConfirm;
            messageType=  TypeMessageSMSConfirmed;
            break;
        }
        case OrderStatusDineInServing:{
            messToServer =@"ServingTableOrder";
            mailType = MailTypeOrderingReady;
            messageType = TypeMessageSMSReady;
            break;
        }
        case OrderStatusDineInDone:{
            messToServer =@"DoneTableOrder";
            break;
        }
        case OrderStatusRefund:
        case OrderStatusCanceled:{
            if(oldStatus == OrderStatusDineInWaiting)
            {
                messToServer =@"RejectTableOrder";
                mailType = MailTypeOrderingReject;
                messageType = TypeMessageSMSReject;
            }
            else{
                messToServer =@"CancelTableOrder";
                mailType = MailTypeOrderingCancel;
                messageType = TypeMessageSMSCancel;
                
            }
             break;
        }
           
        default:
            break;
    }
    if (messToServer) {
        [self sendUpdateStatusOrderToServerWithOrderChanged:ordering];
    }
    if ([self allowSendMail]) {
        NSString *emailSend=[TKOrder emailCustomerReceiver:ordering];
        if (emailSend && mailType!=MailTypeNone) {
            NSMutableDictionary *dataSendMail =[[NSMutableDictionary alloc] init];
            [dataSendMail setObject:ordering.documentID forKey:@"orderId"];
            [dataSendMail setObject:[TKOrder getStationId:ordering] forKey:@"stationId"];
            [dataSendMail setObject:[TKOrder customerReceiverFirstName:ordering] forKey:@"customerName"];
            [dataSendMail setObject:[TKOrder getStationName:ordering] forKey:@"stationName"];
            EmailData *emailData =[[EmailData alloc] initWithEmailData:dataSendMail emailReceiver:emailSend emailType:mailType];
            emailData.keyMail=[NSString stringWithFormat:@"notiCustomerOrder %@",ordering.documentID];
            [[QueueSendMail sharedQueue] addEmailToQueue:emailData];
        }
    }
    if ([self allowSendSMS]) {
        NSString *phoneSend=[TKOrder customerReceiverPhoneCall:ordering];
        if (phoneSend && messageType!=TypeMessageSMSNone) {
            dispatch_queue_t myQueue = dispatch_queue_create("queue.SendSMS", NULL);
            dispatch_async(myQueue, ^{
                [self sendSMS:ordering typeSMS:messageType message:nil];
            });
        }
    }
    return true;
}
#pragma SEND NOTIFY AMEND
+(void)sendNotifyWhenAmendOrder:(CBLDocument *)order withMessage:(NSString *)message{
    if ([self allowSendMail]) {
        NSString *emailSend=[TKOrder emailCustomerReceiver:order];
        if (emailSend) {
            NSMutableDictionary *dicSendMail =[[NSMutableDictionary alloc] init];
            [dicSendMail setObject:order.documentID forKey:@"orderId"];
            [dicSendMail setObject:[TKOrder getStationId:order] forKey:@"stationId"];
            [dicSendMail setObject:[TKOrder customerReceiverFirstName:order] forKey:@"customerName"];
            [dicSendMail setObject:[TKOrder getStationName:order] forKey:@"stationName"];
            [dicSendMail setObject:message forKey:@"message"];
            EmailData *emailData=[[EmailData alloc] initWithEmailData:dicSendMail emailReceiver:emailSend emailType:MailTypeOrderingAmend];
            emailData.keyMail=[NSString stringWithFormat:@"notiCustomerOrder %@",order.documentID];
            [[QueueSendMail sharedQueue] addEmailToQueue:emailData];
        }
    }
    if ([self allowSendSMS]) {
        NSString *phoneSend=[TKOrder customerReceiverPhoneCall:order];
        if (phoneSend) {
            dispatch_queue_t myQueue = dispatch_queue_create("queue.SendSMS", NULL);
            dispatch_async(myQueue, ^{
                [self sendSMS:order typeSMS:TypeMessageSMSAmend message:message];
            });
        }
    }
}
+(BOOL)allowSendMail{
    CBLDocument *orderingSetting=[Controller getTableOrderingSetting];

 return [TKTableOrderingSetting getAllowSendEmail:orderingSetting];
}
+(BOOL)allowSendSMS{
    CBLDocument *orderingSetting=[Controller getTableOrderingSetting];

return [TKTableOrderingSetting getAllowSendSMS:orderingSetting];
}
+(BOOL)allowShowWhenHaveNewOrder{
    CBLDocument *orderingSetting=[Controller getTableOrderingSetting];
    
    return [TKTableOrderingSetting getAllowSouldWhenHaveNewOrder:orderingSetting];
}
+(void)changeStatusWaitingToConfirm:(NSArray *)arrayOrderings
{
    for (CBLDocument *ordering in arrayOrderings) {
       [self updateOrdering:ordering toStatus:OrderStatusDineInConfirm];
    }
}

+(NSInteger)refundOrderWithReferenceCode:(NSString*)referenceCode refundAmount:(double)refundAmount remarks:(NSString*)remarks
{
    NSDictionary *dataSync=[UserDefaultModel getMerchantSyncInfor];
    NSDictionary *response=[WebserviceJSON refundOrderWithMerchantID:dataSync[tkKeyMerchantID] referenceCode:referenceCode refundAmount:refundAmount remarks:remarks];
    
    return ([response[tkKeyReturnStatus] integerValue]);
}
+(void)updateRefundOrder:(CBLDocument *)orderDoc withArrayPaymentRefundJSON:(NSArray*)arrayPaymentRefundJSON reason:(NSString *)reason //
{
    NSArray *arrayPaymentRefundJSONExisted=[TKOrder arrayPaymentRefundJSON:orderDoc];
    NSMutableArray *newArrayPaymentJSONRefund=[[NSMutableArray alloc] init];
    if (arrayPaymentRefundJSONExisted) {
        [newArrayPaymentJSONRefund addObjectsFromArray:arrayPaymentRefundJSONExisted];
    }
    [newArrayPaymentJSONRefund addObjectsFromArray:arrayPaymentRefundJSON];
    
    NSDate *currentDate=[NSDate date];
    NSMutableDictionary *refundData=[[NSMutableDictionary alloc] init];
    [refundData setObject:newArrayPaymentJSONRefund forKey:tkkeyArrayPayment];
    [refundData setObject:[CBLJSON JSONObjectWithDate:currentDate] forKey:tkKeyRefundDate];
    [refundData setObject:@(0) forKey:@"typeRefund"];
    [refundData setObject:reason forKey:tkKeyReasonRefund];
    CBLDocument *userLogined=[Controller getUserLoginedDoc];
    [refundData setObject:@([TKEmployee getEmployeeID:userLogined]) forKey:@"staffID"];
    [refundData setObject:[TKEmployee getEmployeeName:userLogined] forKey:@"staffFullName"];
    [[SyncManager sharedSyncManager] updateDocument:orderDoc propertyDoc:@{tkKeyPaymentStatus:@(PaymentStatusRefunded),tkKeyModifiedDate:[CBLJSON JSONObjectWithDate:currentDate],tkKeyRefundDate:[CBLJSON JSONObjectWithDate:currentDate],tkKeyRefundInfo:refundData}];
}
+(void)sendUpdateStatusOrderToServerWithOrderChanged:(CBLDocument *)orderChanged{
    NSDictionary *data=@{
                         @"orderID":orderChanged.documentID,
                         @"orderStatus":@([TKOrder orderStatus:orderChanged]),
                         @"paymentStatus":@([TKOrder paymentStatus:orderChanged]),
                         @"stationID":[TKOrder getStationId:orderChanged],
                         tkKeyArrayStatusHistory:[TKOrder arrayStatusHistory:orderChanged]
                         };
    NSDictionary *dataSync=[UserDefaultModel getMerchantSyncInfor];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:dataSync[tkKeyMerchantID] forKey:tkKeyMerchantID];
    [paramDic setObject:[CBLJSON stringWithJSONObject:data options:0 error:nil] forKey:tkKeyResult];
    [paramDic setObject:@(2) forKey:@"typeMessage"];
    DataWebsevice *dataWS=[[DataWebsevice alloc] initWithData:paramDic url:tkWebServiceUpdateStatusOrderToServer identifyData:orderChanged.documentID];
    [[QueueWebserviceRealTime sharedQueue] addDataToQueue:dataWS];
}
+(int)webserviceRealTimeWithURL:(NSString *)url parameter:(NSDictionary *)param{
    NSMutableDictionary *newParam=[param mutableCopy];
    NSDictionary *response=[WebserviceJSON callWebserviceAuthenWithURL:url parameters:newParam headers:nil];
    return [response[tkKeyReturnStatus] intValue];
}
//device
+(NSDictionary *)getDeviceInfo{
    UIDevice *device=[UIDevice currentDevice];
    NSString *deviceToken=[self getDeviceToken];
    if (deviceToken==nil) {
        deviceToken=@"";
    }
    return @{
             @"uuid":device.identifierForVendor.UUIDString,
             @"name":device.name,
             @"model":device.model,
             @"systemName":device.systemName,
             @"systemVersion":device.systemVersion,
             @"deviceToken":deviceToken,
             @"os":tkOSType,
             @"appType":tkAppType};
}
+(NSString*)getDeviceToken{
    return [UserDefaultModel getDeviceToken];
}
+(void)saveDeviceToken:(NSString *)deviceToken{
    if (deviceToken) {
        [UserDefaultModel saveDeviceToken:deviceToken];
    }
}
+(BOOL)checkThisDeviceAvailable{
    CBLDocument *listDevicesDoc=[self getListDevicesSetting];
    NSArray *listDevicesAvailable=[TKListDevicesSetting getListDeviceAvailable:listDevicesDoc];
    NSDictionary *thisDeviceInfo=[self getDeviceInfo];
    BOOL available=NO;
    for (NSDictionary *device in listDevicesAvailable) {
        if ([thisDeviceInfo[@"uuid"] isEqualToString:device[@"uuid"]]) {
            available=YES;
            break;
        }
    }
    return available;
}
+(NSInteger)checkAndAddDeviceToServer{
    NSDictionary *dict=[WebserviceJSON checkAndAddDevice];
    return [dict[tkKeyReturnStatus] integerValue];
}

//restart pull sync
+(void)restartSyncPull{
    if (![[SyncManager sharedSyncManager] syncing]) {
        [[SyncManager sharedSyncManager] restartPull];
    }
}

#pragma Send status of station with typeMessage 3
+(void)updateClearSationToServerWithSationId:(NSString *)stationId tmpOrderId:(NSString *)tmpOrderId{
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setObject:tmpOrderId forKey:@"orderID"];
    [data setObject:@(1)  forKey:@"orderStatus"];
    [data setObject:@(1)  forKey:@"paymentStatus"];
    [data setObject:stationId  forKey:@"stationID"];
    [data setObject:@[] forKey:tkKeyArrayStatusHistory];

    NSDictionary *dataSync=[UserDefaultModel getMerchantSyncInfor];
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    [paramDic setObject:dataSync[tkKeyMerchantID] forKey:tkKeyMerchantID];
    [paramDic setObject:[CBLJSON stringWithJSONObject:data options:0 error:nil] forKey:tkKeyResult];
    [paramDic setObject:@(3) forKey:@"typeMessage"];
    
    DataWebsevice *dataWS=[[DataWebsevice alloc] initWithData:paramDic url:tkWebServiceUpdateStatusOrderToServer identifyData:tmpOrderId];
    [[QueueWebserviceRealTime sharedQueue] addDataToQueue:dataWS];
}
@end
