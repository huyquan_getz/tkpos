//
//  WebserviceJSON.h
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EmailData.h"

@interface WebserviceJSON : NSObject
+(NSDictionary*)authenticationWithBusinessName:(NSString*)businessName username:(NSString*)username password:(NSString*)password;
+(NSDictionary*)checkAndAddDevice;
+(NSDictionary*)resetApplicationNumberBadge;
+(NSDictionary*)getTokenWithBusinessName:(NSString*)businessName username:(NSString*)username password:(NSString*)password;
+(NSDictionary*)sendEmailWithData:(NSDictionary*)dataSend toEmail:(NSString*)email type:(MailType)mailType;
+(NSString*)md5HexDigest:(NSString*)input;
+(NSDictionary*) sendNotifySMS:(NSMutableDictionary*) paramDic;
+(NSDictionary*)refundOrderWithMerchantID:(NSString*)merchantID referenceCode:(NSString*)referenceCode refundAmount:(double)refundAmount remarks:(NSString*)remarks;
+(NSDictionary*)sendUpdateOrderToServerWithData:(NSDictionary*)data typeMessage:(int)typeMessage;
+(NSDictionary*)callWebserviceAuthenWithURL:(NSString*)urlWebService parameters:(NSDictionary*)parameters headers:(NSDictionary*)headers;
@end
