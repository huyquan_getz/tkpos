//
//  PrintObjectData.h
//  POS
//
//  Created by Hoang Van Quynh on 13/01/2015.
//  Copyright (c) Năm 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrintData.h"
@interface PrintObjectData : NSCoder
@property (nonatomic, copy, readwrite) NSString *ip;
@property (nonatomic, copy, readwrite) NSString *name;
@property (nonatomic, copy, readwrite) NSString *data;
- (id)initWithIp:(NSString*)ip Name:(NSString*)name;
@end
