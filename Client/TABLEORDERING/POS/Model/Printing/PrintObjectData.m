//
//  PrintObjectData.m
//  POS
//
//  Created by Hoang Van Quynh on 13/01/2015.
//  Copyright (c)  2015 Nha Duong Cong. All rights reserved.
//

#import "PrintObjectData.h"

@implementation PrintObjectData
- (id)initWithIp:(NSString*)ip Name:(NSString*)name{
    self = [super init];
    if (self)
    {
        _ip=ip;
        _name=name;
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.ip forKey:@"ip"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.data forKey:@"data"];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        _ip = [aDecoder decodeObjectForKey:@"ip"];
        _name = [aDecoder decodeObjectForKey:@"name"];
        _data = [aDecoder decodeObjectForKey:@"data"];
        
        
    }
    return self;
}
@end
