//
//  CellProductInReceipt.m
//  POS
//
//  Created by Cong Nha Duong on 1/15/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
// Width 510
#define Height1Line 25
#define Height2Line 52
#define FrameQuantity CGRectMake(8, 0, 51, 25)
#define FrameName CGRectMake(67, 0, 285, 25)
#define FrameAmountCrude CGRectMake(365, 0, 145, 25)
#define FrameDiscount CGRectMake(67, 64, 285, 25)
#define FrameAmountDiscount CGRectMake(365, 64, 145, 25)
#import "CellProductInReceipt.h"
#import "Constant.h"

@implementation CellProductInReceipt{
    BOOL haveDiscount;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithQuantity:(NSString *)quantity_ title:(NSString *)title_ amount:(NSString *)amount{
    if(self =[super init]){
        haveDiscount=NO;
        [self setQuantity:quantity_];
        [self setTitle:title_];
        [self setAmountCrude:amount];
    }
    return self;
}
-(instancetype)initWithQuantity:(NSString *)quantity_ title:(NSString *)title_ amount:(NSString *)amount discountTitle:(NSString *)discountTitle amountDiscount:(NSString *)amountDiscount{
    if (self =[super init]) {
        haveDiscount=YES;
        [self setQuantity:quantity_];
        [self setTitle:title_];
        [self setAmountCrude:amount];
        [self setDiscount:discountTitle];
        [self setAmountDiscount:amountDiscount];
    }
    return self;
}
-(NSInteger)maxHeight{
    NSInteger height=[self maxHeightName];
    if (haveDiscount) {
        height+=10;
        height+=FrameDiscount.size.height;
    }
    return height;
}
-(NSInteger)maxHeightName{
    UIFont *font=tkFontMainWithSize(22);
    NSInteger heightName=[_lbTitle.text heightWithFont:font width:FrameName.size.width];
    NSInteger heightAmount=[_lbAmountCrude.text heightWithFont:font width:FrameAmountCrude.size.width];
    NSInteger heightQuantity=[_lbQuantity.text heightWithFont:font width:FrameQuantity.size.width];
    return MAX(heightName, MAX(heightAmount, heightQuantity))+5;
}
-(void)layoutSubviews{
    NSInteger heightName=[self maxHeightName];
    CGRect frame=FrameQuantity;
    frame.size.height=heightName;
    _lbQuantity.frame=frame;
    
    frame=FrameName;
    frame.size.height=heightName;
    _lbTitle.frame=frame;
    
    frame=FrameAmountCrude;
    frame.size.height=heightName;
    _lbAmountCrude.frame=frame;
    
    if (haveDiscount) {
        frame=FrameDiscount;
        frame.origin.y=heightName+10;
        _lbDiscount.frame=frame;
        
        frame=FrameAmountDiscount;
        frame.origin.y=heightName+10;
        _lbAmountDiscount.frame=frame;
    }
}
-(UILabel*)lablePrinting{
    UILabel *lb=[[UILabel alloc] init];
    lb.backgroundColor=[UIColor clearColor];
    lb.textColor=[UIColor blackColor];
    lb.numberOfLines=4;
    lb.minimumScaleFactor=0.7;
    lb.font=tkFontMainWithSize(22);
    lb.textAlignment=NSTextAlignmentLeft;
    return lb;
}
-(void)setQuantity:(NSString*)quantity{
    if (_lbQuantity==nil) {
        _lbQuantity=[self lablePrinting];
        [self addSubview:_lbQuantity];
    }
    _lbQuantity.text=quantity;
}
-(void)setTitle:(NSString*)title{
    if (_lbTitle==nil) {
        _lbTitle=[self lablePrinting];
        [self addSubview:_lbTitle];
    }
    _lbTitle.text=title;
}
-(void)setAmountCrude:(NSString*)amountCrude{
    if (_lbAmountCrude==nil) {
        _lbAmountCrude=[self lablePrinting];
        _lbAmountCrude.textAlignment=NSTextAlignmentRight;
        [self addSubview:_lbAmountCrude];
    }
    _lbAmountCrude.text=amountCrude;
}
-(void)setDiscount:(NSString*)discount{
    if (_lbDiscount==nil) {
        _lbDiscount=[self lablePrinting];
        [_lbDiscount setFont:tkFontMainWithSize(19)];
        [self addSubview:_lbDiscount];
    }
    _lbDiscount.text=discount;
}
-(void)setAmountDiscount:(NSString*)amountDiscount{
    if (_lbAmountDiscount==nil) {
        _lbAmountDiscount=[self lablePrinting];
        [_lbAmountDiscount setFont:tkFontMainWithSize(19)];
        _lbAmountDiscount.textAlignment=NSTextAlignmentRight;
        [self addSubview:_lbAmountDiscount];
    }
    _lbAmountDiscount.text=amountDiscount;
}

@end
