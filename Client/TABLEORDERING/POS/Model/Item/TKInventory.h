//
//  TKInventory.h
//  POS
//
//  Created by Cong Nha Duong on 2/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TKItem.h"

@interface TKInventory : TKItem
+(NSString*)getAddressCombine:(CBLDocument*)document;
+(NSString*)getContact:(CBLDocument*)document;
+(NSString*)getTimzone:(CBLDocument*)document;
@end
