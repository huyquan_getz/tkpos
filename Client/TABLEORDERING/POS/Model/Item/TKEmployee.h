//
//  TKEmployee.h
//  POS
//
//  Created by Nha Duong Cong on 12/20/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TKItem.h"

@interface TKEmployee : TKItem
+(NSArray*)getArrayPermision:(CBLDocument*)document;
+(NSInteger)getEmployeeID:(CBLDocument*)document;
+(NSString*)getEmployeeName:(CBLDocument *)document;
+(NSString*)getPincode:(CBLDocument*)document;
+(NSArray*)getRoles:(CBLDocument*)document storeIndex:(NSInteger)storeIndex;
+(BOOL)isMerchant:(CBLDocument*)document;
@end
