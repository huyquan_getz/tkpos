//
//  TKProductItem.h
//  POS
//
//  Created by Nha Duong Cong on 11/8/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TKItem.h"
#import "TKDiscount.h"

@interface TKProductItem : TKItem
+(NSArray*)getArrayAllVariant:(CBLDocument*)document;
+(NSArray*)getArrayVariantAvailable:(CBLDocument*)document;
+(NSInteger)getNumberInstance:(CBLDocument*)document;
+(NSInteger)getMiniQuantityStockWarning:(CBLDocument*)document;  // -1; no mini stock , 0: empty of a variant, orther warning stock.
+(BOOL)checkVariantAvailable:(CBLDocument*)document withSKU:(NSString*)variantSKU;
+(BOOL)checkQuantityVariantAvailable:(CBLDocument *)document withSKU:(NSString *)variantSKU quanity:(NSInteger)quantityGet;
+(TKDiscount*)checkDiscount:(CBLDocument*)document;
+(TKDiscount*)discount:(CBLDocument*)document;
@end
