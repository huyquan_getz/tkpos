//
//  TKVariant.h
//  POS
//
//  Created by Nha Duong Cong on 12/19/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKVariant : NSObject
+(double)getPrice:(NSDictionary*)variant;
+(NSInteger)getQuantity:(NSDictionary*)variant;
+(NSInteger)getRemindStock:(NSDictionary*)variant;
+(BOOL)isWarningQuantity:(NSDictionary*)variant; // check have emplty or warning mini stock
+(NSString*)getSKU:(NSDictionary*)variant;
@end
