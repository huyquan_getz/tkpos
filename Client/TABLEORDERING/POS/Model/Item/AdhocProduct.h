//
//  AdhocProduct.h
//  POS
//
//  Created by Cong Nha Duong on 2/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "TKDiscount.h"

@interface AdhocProduct : NSObject<CBLJSONEncoding>{
    NSString *title;
    NSString *variantName;
    NSString *comment;
    double price;
    NSInteger quantity;
    NSDate *createdDate;
    NSDate *modifiedDate;
    NSInteger employeeID;
    NSString *employeeName;
    BOOL applyDiscount;
    BOOL discount;
    TKDiscount *discountProductItem;
    NSString *categoryId;
}
@property(readonly) NSString *title;
@property(readonly) NSString *variantName;
@property(readonly)NSString *comment;
@property(readonly)NSString *categoryId;
@property(readonly)double price;
@property(readonly)NSInteger quantity;
@property(readonly)NSDate *createdDate;
@property(readonly)NSDate *modifiedDate;
@property(readonly)NSInteger employeeID;
@property(readonly)NSString *employeeName;
@property(readonly)BOOL applyDiscount;
@property(readonly)BOOL discount;
@property(readonly)TKDiscount *discountProductItem;
-(instancetype)initWithTitle:(NSString*)title_ description:(NSString*)description_ price:(double)price_ quantity:(NSInteger)quantity_ employeeID:(NSInteger)employeeID_ employeeName:(NSString*)employeeName_ applyDiscount:(BOOL)applyDiscount_ categoryId:(NSString*)categoryId_;
-(instancetype)initWithJSON:(id)jsonObject;
-(id)encodeAsJSON;
-(void)setDiscountProductItem:(TKDiscount *)discountItem_;
-(NSString*)nameWithVariant;
@end
