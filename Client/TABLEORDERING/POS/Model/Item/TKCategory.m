//
//  TKCategory.m
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TKCategory.h"

@implementation TKCategory
+(NSString *)getImageUrl:(CBLDocument *)doc{
    return (NSString*)[doc[tkKeyImage] convertNullToNil];
}
+(NSString *)getShortName:(CBLDocument*)doc{
    return doc.properties[tkKeyShortName];
}
+(NSInteger)getNumberProduct:(CBLDocument *)doc{
    NSArray *products=(NSArray*)[doc.properties[tkkeyArrayProduct] convertNullToNil];
    return [products count];
}
+(NSString *)categoryIdAdhocDefault{
    NSString *idCategory=[[NSUserDefaults standardUserDefaults] objectForKey:@"categoryAdhoc"];
    if (idCategory) {
        return idCategory;
    }else{
        NSMutableDictionary *dic=[[TKItem infomationBaseWithTable:tkCategoryTable] mutableCopy];
        [dic setObject:@(NO) forKey:tkKeyDisplay];
        [dic setObject:@"Ad-hoc" forKey:tkKeyTitle];
        [dic setObject:@"" forKey:tkKeyDescription];
        [dic setObject:@"" forKey:tkKeyImage];
        [dic setObject:@[] forKey:tkkeyArrayProduct];
        CBLDocument *doc=[[SyncManager sharedSyncManager] createDocumentLocalOnly:dic];
        [[NSUserDefaults standardUserDefaults] setObject:doc.documentID forKey:@"categoryAdhoc"];
        return doc.documentID;
    }
}
@end
