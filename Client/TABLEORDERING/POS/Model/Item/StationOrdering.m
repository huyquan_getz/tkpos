//
//  StationOrdering.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/17/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "StationOrdering.h"
#import "TKOrder.h"
#import "TKStation.h"
#import "Constant.h"

@implementation StationOrdering
@synthesize station,order;
-(instancetype)initWithStation:(CBLDocument *)station_ order:(CBLDocument *)order_{
    if (self=[super init]) {
        order=order_;
        station=station_;
    }
    return self;
}
-(TypeStatusOrdering)getTypeStatusStationOrdering{
    if (order==nil) {
        return [station.properties[@"statusOrdering"] intValue];
    }else{
        switch ([TKOrder orderStatus:order]) {
            case OrderStatusDineInWaiting:
                return TypeStatusOrderingWaiting;
                break;
            case OrderStatusDineInConfirm:
                return TypeStatusOrderingConfirmed;
                break;
            case OrderStatusDineInServing:
                return TypeStatusOrderingServing;
                break;
            default:
                break;
        }
    }
    return TypeStatusOrderingNone;
}
-(NSString *)stationName{
    return [TKStation getName:station];
}
-(NSString *)orderCode{
    if (order) {
        return [TKOrder orderCode:order];
    }
    return nil;
}
-(NSDictionary *)customerInfo{
    if (order) {
        return [TKOrder customerReceiver:order];
    }else{
        return [TKStation customerHost:station];
    }
}
-(NSString *)customerName{
    NSString *customerName=[[self customerInfo] objectForKey:tkKeyFullName];
    return (customerName.length>0)?customerName:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.text-walk-in"];
}
-(NSString *)customerFirstName{
    NSString *customerName=[[self customerInfo] objectForKey:tkKeyFirstName];
    return (customerName.length>0)?customerName:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.text-walk-in"];
}
-(NSString *)customerPhone{
    return [[self customerInfo] objectForKey:tkKeyPhone];
}
-(NSString *)customerPhoneDisplay{
    NSDictionary *customerInfo=[self customerInfo];
    NSString *region=[customerInfo[@"phoneRegion"] convertNullToNil];
    if (region.length>0) {
        if (![region hasPrefix:@"+"]) {
            region=[region stringByReplacingCharactersInRange:NSMakeRange(0, 0) withString:@"+"];
        }
        return [NSString stringWithFormat:@"(%@) %@",region,customerInfo[tkKeyPhone]];
    }else{
        return customerInfo[tkKeyPhone];
    }
}
-(NSString *)customerEmail{
    return [[self customerInfo] objectForKey:tkKeyEmail];
}
-(NSString *)customerImageURL{
    return [[self customerInfo] objectForKey:tkKeyImage];
}
-(NSString *)specialRequest{
    if (order) {
        return [TKOrder specialRequest:order];
    }
    return nil;
}
-(NSInteger)totalProductItem{
    if (order) {
        return [TKOrder totalProductItem:order];
    }
    return 0;
}
-(NSDate *)startWaitingDate{
    if (order) {
        return [TKOrder createdDate:order];
    }
    return nil;
}
+(NSString *)typeStringByType:(TypeStatusOrdering)type{
    switch (type) {
        case TypeStatusOrderingWaiting:
            return [[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.status-waiting"];
            break;
        case TypeStatusOrderingConfirmed:
            return [[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.status-confirmed"];
            break;
        case TypeStatusOrderingServing:
            return [[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.status-serving"];
            break;
        case TypeStatusOrderingOccupied:
            return [[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.status-occupied"];
            break;
        case TypeStatusOrderingEmpty:
            return [[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.status-empty"];
        default:
            return @"Unknown";
            break;
    }
}
-(BOOL)isEqualStationOrdering:(StationOrdering *)stationOrderingCompare{
    NSString *orderId1;
    NSString *orderId2;
    if (order) {
        orderId1=order.documentID;
    }else if([self getTypeStatusStationOrdering]==TypeStatusOrderingOccupied){
        orderId1=[TKStation orderId:station];
    }
    if (stationOrderingCompare.order) {
        orderId2=stationOrderingCompare.order.documentID;
    }else if([stationOrderingCompare getTypeStatusStationOrdering]==TypeStatusOrderingOccupied){
        orderId2=[TKStation orderId:stationOrderingCompare.station];
    }
    return (station==stationOrderingCompare.station && ([orderId1 isEqualToString:orderId2]));
}
+(NSArray *)subStationOrderingWithType:(TypeStatusOrdering)type fromStationOrderingList:(NSArray *)listStation{
    NSMutableArray *listStationOrderingByType=[[NSMutableArray alloc] init];
    for (StationOrdering *stOrder in listStation) {
        if ([stOrder getTypeStatusStationOrdering]==type) {
            [listStationOrderingByType addObject:stOrder];
        }
    }
    return listStationOrderingByType;
}
@end
