//
//  TKAmountDisplay.h
//  POS
//
//  Created by Cong Nha Duong on 2/7/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKAmountDisplay : NSObject{
    NSString *currency;
}
@property (readonly)NSString *currency;
-(instancetype)initWithCurrency:(NSString*)currency_;
-(NSString*)stringAmountHaveCurrency:(double)amount;
+(BOOL)isZero:(double)amount;
+(double)tkRoundCash:(double)amount;
+(double)tkRoundCashTotal:(double)amount;// as same round cash;
+(NSString*)stringByRoundCashAmout:(double)amout;
+(NSString *)stringAmount:(double)amount withCurrency:(NSString *)currency_;
+(double)ceil:(double)amount withIndexNumberCeil:(NSUInteger)indexNumberCeil;
@end
