//
//  UserDefaultModel.m
//  POS
//
//  Created by Nha Duong Cong on 10/14/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define keyDatabaseName @"databaseName"
#define keyServerCouchbaseURL @"serverCouchbaseURL"
#define keyAccountMerchant @"accountMerchant"
#define keyCashierId @"cashierId"
#define keyTokenAuth @"tokenAuth"
#define keyFirstSyncCompleted @"firstSyncCompleted"
#define keyLanguageType @"languageType"
#define keyMerchantSyncInfor @"syncInfor"
#define keyStoreSlected @"storeSelected"
#define keyUserLogined @"userLogined"
#define keyUserMerchant @"userMerchant"
#define keyRequestPincodeWhenEnterForeground @"requestPCEnterForeground"
#define keyTaxService @"taxService"
#define keyGeneralSetting @"generalSetting"
#define keyTableOrderingSetting @"tableOrderingSetting"
#define keyDeviceToken @"deviceToken"
#define keyListDevicesSetting @"listDevicesSetting"
#define keyNumberBadgeNotUpdate @"numberBadgeNotUpdate"
#import "UserDefaultModel.h"

static NSUserDefaults *_userDefault;
@implementation UserDefaultModel
+(void)initialize{
    _userDefault =[NSUserDefaults standardUserDefaults];
}
+(void)removeAllKeyUserDefault{
    NSDictionary *dataUserDefault=[[_userDefault dictionaryRepresentation] copy];
    for (NSString *key in dataUserDefault) {
        [_userDefault removeObjectForKey:key];
    }
}

+(void)saveServerCouchbaseURL:(NSString *)serverURL{
    [_userDefault setObject:serverURL forKey:keyServerCouchbaseURL];
    [_userDefault synchronize];
}
+(NSString *)getServerCouchbaseURL{
    return [_userDefault objectForKey:keyServerCouchbaseURL];
}
+(void)saveAccountMerchant:(NSString *)documentID{
    [_userDefault setObject:documentID forKey:keyAccountMerchant];
    [_userDefault synchronize];
}
+(NSString *)getAccountMerchant{
    return [_userDefault objectForKey:keyAccountMerchant];
}

+(void)saveUserMerchant:(NSDictionary *)dictionary{
    [_userDefault setObject:dictionary forKey:keyUserMerchant];
    [_userDefault synchronize];
}
+(NSDictionary *)getUserMerchant{
    return [_userDefault objectForKey:keyUserMerchant];
}

+(void)saveFirstSyncCompleted:(BOOL)completed{
    [_userDefault setBool:completed forKey:keyFirstSyncCompleted];
    [_userDefault synchronize];
}
+(BOOL)wasFirstSyncCompleted{
    //default have not keyFirstSyncCompleted then return NO;
    return [_userDefault boolForKey:keyFirstSyncCompleted];
}

+(void)saveCashierId:(NSString *)cashierId{
    [_userDefault setObject:cashierId forKey:keyCashierId];
    [_userDefault synchronize];
}
+(NSString *)getCashierId{
    return [_userDefault objectForKey:keyCashierId];
}

+(void)saveTokenAuthentication:(NSString *)token{
    [_userDefault setObject:token forKey:keyTokenAuth];
    [_userDefault synchronize];
}
+(NSString *)getTokenAuthentication{
    return [_userDefault objectForKey:keyTokenAuth];
}

+(void)saveLanguageType:(NSNumber*)languageType{
    [_userDefault setObject:languageType forKey:keyLanguageType];
    [_userDefault synchronize];
}
+(NSNumber*)getLanguageType{
    return [_userDefault objectForKey:keyLanguageType];
}

+(void)saveMerchantSyncInfor:(NSDictionary *)merchantDataSync{
    [_userDefault setObject:merchantDataSync forKey:keyMerchantSyncInfor];
    [_userDefault synchronize];
}
+(NSDictionary *)getMerchantSyncInfor{
    return [_userDefault objectForKey:keyMerchantSyncInfor];
}

+(void)saveStoreSelected:(NSString *)storeID{
    [_userDefault setObject:storeID forKey:keyStoreSlected];
    [_userDefault synchronize];
}
+(NSString *)getStoreSelected{
    return [_userDefault objectForKey:keyStoreSlected];
}

+(void)saveUserLogined:(NSString *)documentID{
    [_userDefault setObject:documentID forKey:keyUserLogined];
    [_userDefault synchronize];
}
+(NSString *)getUserLogined{
    return [_userDefault objectForKey:keyUserLogined];
}
+(void)saveRequestPincodeWhenEnterForeground:(BOOL)request{
    [_userDefault setBool:request forKey:keyRequestPincodeWhenEnterForeground];
    [_userDefault synchronize];
}
+(BOOL)getRequestPincodeWhenEnterForeground{
    return [_userDefault boolForKey:keyRequestPincodeWhenEnterForeground];
}
+(void)saveTaxtServiceSetting:(NSString *)documentID{
    [_userDefault setObject:documentID forKey:keyTaxService];
    [_userDefault synchronize];
}
+(NSString *)getTaxServiceSetting{
    return [_userDefault objectForKey:keyTaxService];
}

+(void)saveGeneralSetting:(NSString *)documentID{
    [_userDefault setObject:documentID forKey:keyGeneralSetting];
    [_userDefault synchronize];
}
+(NSString *)getGeneralSetting{
    return [_userDefault objectForKey:keyGeneralSetting];
}

+(void)saveEncodeArchiver:(id)object forKey:(NSString *)key{
    NSData *data=[NSKeyedArchiver archivedDataWithRootObject:object];
    [_userDefault setObject:data forKey:key];
    [_userDefault synchronize];
}
+(id)getDecodeArchiverObjectForKey:(NSString *)key{
    NSData *data=[_userDefault objectForKey:key];
    if (data==nil) {
        return nil;
    }else{
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
}
+(void) saveTableOrderingSetting:(NSString *) documentID
{
    [_userDefault setObject:documentID forKey:keyTableOrderingSetting];
    [_userDefault synchronize];
}
+(NSString *)getTableOrderingSetting
{
    return [_userDefault objectForKey:keyTableOrderingSetting];
}

+(void)saveDeviceToken:(NSString *)deviceToken{
    [_userDefault setObject:deviceToken  forKey:keyDeviceToken];
    [_userDefault synchronize];
}
+(NSString*)getDeviceToken{
    return[_userDefault objectForKey:keyDeviceToken];
}

+(void)saveListDevicesSetting:(NSString *)listDevicesID{
    [_userDefault setObject:listDevicesID  forKey:keyListDevicesSetting];
    [_userDefault synchronize];
}
+(NSString *)getListDevicesSetting{
    return [_userDefault objectForKey:keyListDevicesSetting];
}

+(void)saveApplicationNumberBadgeNotUpdate:(NSInteger)numberBadgeNotUpdate{
    [_userDefault setInteger:numberBadgeNotUpdate  forKey:keyNumberBadgeNotUpdate];
    [_userDefault synchronize];
}
+(NSInteger)getApplicationNumberBadgeNotUpdate{
    return [_userDefault integerForKey:keyNumberBadgeNotUpdate];
}

@end
