//
//  ListPrinterVC.h
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "PrinterController.h"

@interface ListPrinterVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listPrinter;
    CBLLiveQuery *liveQuery;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btAddNew;
- (IBAction)clickAddNew:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbNoPrinter;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;

@end
