//
//  ListPrinterVC.m
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define HeighCell 50
#define MaxPrinterQuery 1000
#define CellPrinterIdentify @"cellPrinter"
#import "ListPrinterVC.h"
#import "AddNewPrinterVC.h"
#import "TableCellPrinter.h"
#import "Controller.h"
#import "Printer.h"
#import "Constant.h"


@interface ListPrinterVC ()

@end

@implementation ListPrinterVC{
}
-(instancetype)init{
    if (self =[super init]) {
        listPrinter=[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _ivNaviBar.backgroundColor=[UIColor statusNaviBar];
    _tbvMain.clipsToBounds=YES;
    _tbvMain.layer.cornerRadius=tkCornerRadiusViewPopup;
    _tbvMain.layer.borderColor=tkColorFrameBorder.CGColor;
    _tbvMain.layer.borderWidth=1;
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellPrinter" bundle:nil] forCellReuseIdentifier:CellPrinterIdentify];
    // Do any additional setup after loading the view from its nib.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self reSetupQueryLiveWithQuery:[Controller queryGetAllPrinter]];
    });
}
-(void)viewDidAppear:(BOOL)animated{
    _lbTitle.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"setting.printer-list.title"];
    _lbNoPrinter.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"setting.printer-list.lb-no-printer"];
}
-(void)reSetupQueryLiveWithQuery:(CBLQuery*)query{
    if (query==nil) {
        return;
    }
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery=[query asLiveQuery];
    [liveQuery addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    liveQuery.limit = MaxPrinterQuery;
    liveQuery.descending=NO;
    [liveQuery start];
}
-(void)removeQueryLive{
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery=nil;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object == liveQuery) {
        [self displayRows:liveQuery.rows];
    }
}
-(void)displayRows:(CBLQueryEnumerator*)enumRows{
    NSMutableArray *printers=[[NSMutableArray alloc] init];
    for (CBLQueryRow *row in enumRows) {
        [printers addObject:row.document];
        [SNLog Log:@"%@",row.documentID];
    }
    [listPrinter removeAllObjects];
    [listPrinter addObjectsFromArray:printers];
    [_tbvMain reloadData];
    [self resetFrameTableView];
}
-(void)resetFrameTableView{
    CGRect frame=_tbvMain.frame;
    NSInteger heightContent=listPrinter.count * HeighCell;
    if (heightContent>658) {
        heightContent=658;
    }
    frame.size.height=heightContent;
    [_tbvMain setFrame:frame];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    _lbNoPrinter.hidden=(listPrinter.count!=0);
    return listPrinter.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellPrinter *cell=[tableView dequeueReusableCellWithIdentifier:CellPrinterIdentify];
    [cell setValueDefault];
    cell.tag=indexPath.row;
    CBLDocument *doc=listPrinter[indexPath.row];
    cell.lbTitle.text=[Printer getPrinterName:doc];
    dispatch_queue_t myQueue=dispatch_queue_create("queue.checkonoff", nil);
    dispatch_async(myQueue, ^{
        NSInteger indexCell=indexPath.row;
        BOOL on=[[PrinterController sharedInstance] checkConnectionWithIPAddress:[Printer getIpAddress:doc] port:[Printer getPort:doc]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (indexCell==cell.tag) {
                cell.activityIndicator.hidden=YES;
                [cell setOn:on];
            }
        });
    });
    
   
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AddNewPrinterVC *editPrinter=[[AddNewPrinterVC alloc] initWithPrinter:listPrinter[indexPath.row]];
    
    [self.navigationController pushViewController:editPrinter animated:YES];
}
- (IBAction)clickAddNew:(id)sender {
    AddNewPrinterVC *addNew=[[AddNewPrinterVC alloc] initWithPrinter:nil];
    [self.navigationController pushViewController:addNew animated:YES];
}
-(void)dealloc{
    [liveQuery removeObserver:self forKeyPath:@"rows"];
}
@end
