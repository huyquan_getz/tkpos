//
//  TableCellPrinting.m
//  POS
//
//  Created by Cong Nha Duong on 1/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellPrinting.h"
#import "Constant.h"

@implementation TableCellPrinting

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        _lbTitle.textColor=[UIColor whiteColor];
        _lbPrinter.textColor=[UIColor whiteColor];
        _ivNext.image=[UIImage imageNamed:@"icon_next_detail_w.png"];
    }else{
        _lbTitle.textColor=[UIColor darkGrayColor];
        _lbPrinter.textColor=[UIColor darkGrayColor];
        _ivNext.image=[UIImage imageNamed:@"icon_next_detail.png"];
    }
    // Configure the view for the selected state
}
-(void)setValueDefault{
    UIView *bgSelected=[[UIView alloc] init];
    [bgSelected setBackgroundColor:tkColorMainSelected];
    [super setSelectedBackgroundView:bgSelected];
    _vLine.backgroundColor=tkColorFrameBorder;
    _lbTitle.text=@"";
    _lbPrinter.text=@"";
}
@end
