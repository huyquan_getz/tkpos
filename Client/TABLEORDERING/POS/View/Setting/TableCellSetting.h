//
//  TableCellSetting.h
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellSetting : UITableViewCell
@property (strong,nonatomic) UIImage *imgISelected;
@property (strong,nonatomic)UIImage *imgIDeselected;

@property (weak, nonatomic) IBOutlet UIImageView *ivIndentify;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
-(void)setValueDefault;
@end
