//
//  LeftMenuSettingVC.m
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define HeightCell 50
#define CellSettingIndentify @"cellSetting"
#import "LeftMenuSettingVC.h"
#import "TableCellSetting.h"
#import "Constant.h"

@interface LeftMenuSettingVC ()

@end

@implementation LeftMenuSettingVC{
    LanguageUtil *languageKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _tbvMain.backgroundColor=tkColorMainBackground;
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellSetting" bundle:nil] forCellReuseIdentifier:CellSettingIndentify];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [_tbvMain reloadData];
    [self selectValueDefault];
}
-(void)selectValueDefault{
    [_tbvMain selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    [_delegate leftMenuSetting:self clickInStoreGeneral:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case SettingSectionTypeInStore:
            return 1;
            break;
        case SettingSectionTypeHardware:
            return 2;
            break;
        default:
            return 0;
            break;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellSetting *cell=[tableView dequeueReusableCellWithIdentifier:CellSettingIndentify];
    [cell setValueDefault];
    if (indexPath.section==SettingSectionTypeInStore) {
        switch (indexPath.row) {
            case SettingTypeInStoreGeneral:{
                cell.lbTitle.text=[languageKey stringByKey:@"setting.left-menu.cell-general"];
                cell.imgIDeselected=[UIImage imageNamed:@"icon_General.png"];
                cell.imgISelected=[UIImage imageNamed:@"icon_General_w.png"];
                cell.bottomLine.backgroundColor=[UIColor clearColor];
                break;
            }
            default:
                break;
        }
    }else if(indexPath.section == SettingSectionTypeHardware){
        switch (indexPath.row) {
            case SettingTypeHardwarePrinterList:{
                cell.lbTitle.text=[languageKey stringByKey:@"setting.left-menu.printer-list"];
                cell.imgIDeselected=[UIImage imageNamed:@"icon_PrinterList.png"];
                cell.imgISelected=[UIImage imageNamed:@"icon_PrinterList_w.png"];
                break;
            }
            case SettingTypeHardwarePrinting:{
                cell.lbTitle.text=[languageKey stringByKey:@"setting.left-menu.default-printer"];
                cell.imgIDeselected=[UIImage imageNamed:@"icon_DefaultPrinter.png"];
                cell.imgISelected=[UIImage imageNamed:@"icon_DefaultPrinter_w.png"];
                break;
            }
            default:
                break;
        }
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_delegate==nil) {
        return;
    }
    if (indexPath.section==SettingSectionTypeInStore) {
        switch (indexPath.row) {
            case SettingTypeInStoreGeneral:
                [_delegate leftMenuSetting:self clickInStoreGeneral:YES];
                break;
            default:
                break;
        }
    }else if(indexPath.section == SettingSectionTypeHardware){
        switch (indexPath.row) {
            case SettingTypeHardwarePrinterList:
                [_delegate leftMenuSetting:self clickHardwarePrinterList:YES];
                break;
            case SettingTypeHardwarePrinting:
                [_delegate leftMenuSetting:self clickHardwarePrinting:YES];
                break;
            default:
                break;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 22;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *vHeader=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 22)];
    [vHeader setBackgroundColor:tkColorMainBackground];
    CGRect frame=vHeader.frame;
    frame.origin.x=20;
    frame.size.width-=20;
    UILabel *title =[[UILabel alloc] initWithFrame:frame];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setTextColor:[UIColor blackColor]];
    [title setFont:tkFontMainWithSize(15)];
    [vHeader addSubview:title];
    switch (section) {
        case SettingSectionTypeInStore:
            title.text=[languageKey stringByKey:@"setting.left-menu.section-in-store"];
            break;
        case SettingSectionTypeHardware:
            title.text=[languageKey stringByKey:@"setting.left-menu.section-hardware"];
            break;
        default:
            return nil;
            break;
    }
    UIView *vTopL=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 1)];
    vTopL.backgroundColor=tkColorFrameBorder;
    UIView *vBottomL=[[UIView alloc] initWithFrame:CGRectMake(0, 21, _tbvMain.frame.size.width, 1)];
    vBottomL.backgroundColor=tkColorFrameBorder;
    [vHeader addSubview:vTopL];
    [vHeader addSubview:vBottomL];
    return vHeader;
}
@end
