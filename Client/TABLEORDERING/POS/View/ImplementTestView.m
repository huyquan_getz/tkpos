//
//  ImplementTestView.m
//  POS
//
//  Created by Nha Duong Cong on 10/30/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "ImplementTestView.h"
#import "SettingVC.h"
#import "Audio.h"
#import "PlayAudio.h"
#import "WaitingAmendVC.h"

#import "TableOrderingWaitingDetailVC.h"
#import "TableOrderingOccupiedDetailVC.h"
#import "TableOrderingServingDetailVC.h"
#import "Constant.h"
#import "Controller.h"
#import "CouchBaseBusinessModel.h"
#import "WebserviceJSON.h"
#import "WelcomeVC.h"
#import "QueueSendMail.h"
#import "UserDefaultModel.h"
#import "QueueWebserviceRealTime.h"
// Nha commit
@implementation ImplementTestView{
    NSTimer *timer;
}
+(ImplementTestView *)sharedTestView{
    __strong static ImplementTestView *_implementTestView;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _implementTestView=[[ImplementTestView alloc] init];
    
    });
    return _implementTestView;
}
-(instancetype)init{
    if (self =[super init]) {
        myQueue11 =[NSOperationQueue new];
        [myQueue11 setMaxConcurrentOperationCount:1];
    }
    return self;
}
-(BOOL)showTest{
    [self example];
    return NO;
    ;
}
-(UIViewController *)viewShow{
//    
//    [ WebserviceJSON sendReceipt:dicOrder toEmail:@"zinhumgato@gmail.com" type:MailTypeOrderingConfirm];
    UIViewController *view=[[WelcomeVC alloc] init];
    return view;
}


-(void)example{
//    NSDictionary *dic=[Controller checkMerchantWithBusinessName:@"NhaStore" username:@"nha@gmail.com" password:@"abc123"];
//    NSDictionary *dic1=[WebserviceJSON getTokenWithBusinessName:@"ltuyen" username:@"tuyensdc@gmail.com" password:@"123456"];
//    [[QueueSendMail sharedQueue] sendReceipt:@{@"customerName":@"conga",@"stationName":@"sdfdf"} toEmail:@"nha.duong@smoovapp.com" type:MailTypeOrderingCancel];
//  timer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(testTimer) userInfo:nil repeats:YES];
    DataWebsevice *data=[[DataWebsevice alloc] initWithData:@{} url:@"" identifyData:@"123" expiredTime:60];
    sleep(1);
    DataWebsevice *data2=[[DataWebsevice alloc] initWithData:@{} url:@"" identifyData:@"123" expiredTime:60];
    [[QueueWebserviceRealTime sharedQueue] addDataToQueue:data];
    sleep(3);
    [[QueueWebserviceRealTime sharedQueue] addDataToQueue:data2];
}
-(void)testQueue{
    for (int i=0; i<10; i++) {
        sleep(1);
        NSLog(@"%d internal %d",1,i);
    }
}

@end
