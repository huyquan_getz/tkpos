//
//  TableOrderingVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define keyUserDefaultSetStatus @"setStatus"
#import "TableOrderingVC.h"
#import "Controller.h"
#import "TableOrderingListener.h"
#import "UserDefaultModel.h"
#import "TableOrderingDetailVC.h"
#import "TableOrderingWaitingDetailVC.h"
#import "StationOrdering.h"
#import "TableOrderingServingDetailVC.h"
#import "TableOrderingConfirmedDetailVC.h"
#import "TableOrderingOccupiedDetailVC.h"
#import "QueueSendMail.h"
#import "TKOrder.h"
#import "MBProgressHUD.h"
#import "PaymentChild.h"
#import "CashierBusinessModel.h"
#import "TKEmployee.h"

@interface TableOrderingVC ()

@end

@implementation TableOrderingVC{
    LanguageUtil *languageKey;
}
-(instancetype)init{
    if (self =[super init]) {
        [[TableOrderingListener sharedObject] startListener];
        NSIndexSet *indexSet=[UserDefaultModel getDecodeArchiverObjectForKey:keyUserDefaultSetStatus];
        if (indexSet==nil) {
            indexSet=[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 3)];
            [UserDefaultModel saveEncodeArchiver:indexSet forKey:keyUserDefaultSetStatus];
        }
        indexSetStatusSelected=[indexSet mutableCopy];
        listStationOrderings=[[NSMutableArray alloc] init];
        languageKey=[LanguageUtil sharedLanguageUtil];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _ivNavi.backgroundColor=tkColorMain;
    [TableOrderingListener sharedObject].delegate=self;
    _lbNbStationWaiting.textColor=tkColorDarkRed;
    _lbNbStationConfirmed.textColor=tkColorPending;
    _lbNbStationServing.textColor=tkColorReady;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    _lbTitle.text=[languageKey stringByKey:@"ordering.title"];
    _lbTitleNbStationWaiting.text=[languageKey stringByKey:@"ordering.status-waiting"];
    _lbTitleNbStationConfirmed.text=[languageKey stringByKey:@"ordering.status-confirmed"];
    _lbTitleNbStationServing.text=[languageKey stringByKey:@"ordering.status-serving"];
    [_btChooseStatus setTitle:[languageKey stringByKey:@"ordering.status-all-status"] forState:UIControlStateNormal];
    
    [self addListTableOrderingVC];
    [self tableOrderingListener:nil listStationOrderings:[[TableOrderingListener sharedObject] shareListStationOrderings] stationOrderingNews:@[]];
}
-(void)reloadListWithNewOrderingUpdate:(NSArray*)newList{
    if (listStationOrderings != newList) {
        [listStationOrderings removeAllObjects];
        [listStationOrderings addObjectsFromArray:newList];
    }
    [self resetStationOrderingSelected];
    [self reloadData];
}
-(void)reloadData{
    NSArray *listStationOrderShow=[TableOrderingVC filterByStatusOrdering:listStationOrderings inSetStatus:indexSetStatusSelected];
    [_listTableOrderingVC resetWithListTableOrdering:listStationOrderShow];
    [self updateQuantityOnTopView];
}
-(void)resetStationOrderingSelected{
    StationOrdering *newStationOrderingSelected;
    if (stationOrderingSelected) {
        for (int row=0;row<listStationOrderings.count;row++) {
            StationOrdering *stOrder=listStationOrderings[row];
            if ([stationOrderingSelected isEqualStationOrdering:stOrder]) {
                newStationOrderingSelected=stOrder;
                break;
            }
        }
    }
    stationOrderingSelected=newStationOrderingSelected;
}
-(void)updateQuantityOnTopView{
    _lbNbStationWaiting.text=[NSString stringWithFormat:@"%lu",(unsigned long)[StationOrdering subStationOrderingWithType:TypeStatusOrderingWaiting fromStationOrderingList:listStationOrderings].count];
    _lbNbStationConfirmed.text=[NSString stringWithFormat:@"%lu",(unsigned long)[StationOrdering subStationOrderingWithType:TypeStatusOrderingConfirmed fromStationOrderingList:listStationOrderings].count];
    _lbNbStationServing.text=[NSString stringWithFormat:@"%lu",(unsigned long)[StationOrdering subStationOrderingWithType:TypeStatusOrderingServing fromStationOrderingList:listStationOrderings].count];
}
+(NSArray*)filterByStatusOrdering:(NSArray*)stationOrderings inSetStatus:(NSIndexSet*)setStatuss{
    NSMutableArray *listFilter=[[NSMutableArray alloc] init];
    for (StationOrdering *stOrder in stationOrderings) {
        if ([setStatuss containsIndex:[stOrder getTypeStatusStationOrdering]]) { // check status in indexset select
            [listFilter addObject:stOrder];
        }
    }
    return listFilter;
}
-(void)addListTableOrderingVC{
    if (_listTableOrderingVC) {
        [_listTableOrderingVC.view removeFromSuperview];
        [_listTableOrderingVC removeFromParentViewController];
    }
    NSMutableArray *listOrdering=[[NSMutableArray alloc] init];
    _vFrameCenter.hidden=YES;
    _listTableOrderingVC=[[ListTableOrderingVC alloc] init];
    [_listTableOrderingVC resetWithListTableOrdering:listOrdering];
    _listTableOrderingVC.delegate=self;
    [self addChildViewController:_listTableOrderingVC];
    _listTableOrderingVC.view.frame=_vFrameCenter.frame;
    [self.view addSubview:_listTableOrderingVC.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickLeftMenu:(id)sender {
    [Controller showLeftMenu:self];
}
-(void)tableOrderingListener:(id)sender listStationOrderings:(NSArray *)stationOrderings stationOrderingNews:(NSArray *)stationOrderingNews{
    [self reloadListWithNewOrderingUpdate:stationOrderings];
}

- (IBAction)clickChooseStatus:(UIButton*)sender {
    ChooseStatusVC *choose=[[ChooseStatusVC alloc] init];
    [choose resetIndexSetSelectedWithIndexSetStatus:indexSetStatusSelected];
    choose.delegate=self;
    _popover=[[UIPopoverController alloc] initWithContentViewController:choose];
    [_popover setPopoverContentSize:choose.view.frame.size];
    CGRect frameShow=sender.frame;
    frameShow.size.height-=15;
    [_popover presentPopoverFromRect:frameShow inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma ListTableOrderingDelegate
-(void)listTableOrdering:(id)sender clickDetail:(StationOrdering *)stationOrdering{
    [SNLog Log:@"station:%@",stationOrdering.station.documentID];
    if (stationOrdering.order) {
        [SNLog Log:@"order:%@",stationOrdering.order.documentID];
    }
  //  NSLog(@"%@",)
    stationOrderingSelected=stationOrdering;
    // add subview
    if(_tableOrderingDetailVC)
    {
        
        [_tableOrderingDetailVC.view removeFromSuperview];
        [_tableOrderingDetailVC removeFromParentViewController];
        _tableOrderingDetailVC = nil;
    }
    if (stationOrdering==nil) {
        return;
    }
    
    switch ([stationOrderingSelected getTypeStatusStationOrdering]) {
        case TypeStatusOrderingWaiting:{
            TableOrderingWaitingDetailVC  *tableOrderingWatingVC = [[TableOrderingWaitingDetailVC alloc]initWithStationOrdering:stationOrderingSelected];
            tableOrderingWatingVC.delegate=self;
            _tableOrderingDetailVC=tableOrderingWatingVC;
            break;
        }
        case TypeStatusOrderingConfirmed:{
            TableOrderingConfirmedDetailVC  *tableOrderingConfirmedDetailVC = [[TableOrderingConfirmedDetailVC alloc]initWithStationOrdering:stationOrderingSelected];
            tableOrderingConfirmedDetailVC.delegate=self;
            _tableOrderingDetailVC=tableOrderingConfirmedDetailVC;
            break;
        }

        case TypeStatusOrderingServing:{
            TableOrderingServingDetailVC  *tableOrderingServingDetailVC = [[TableOrderingServingDetailVC alloc]initWithStationOrdering:stationOrderingSelected];
            tableOrderingServingDetailVC.delegate=self;
            _tableOrderingDetailVC=tableOrderingServingDetailVC;
            break;
        }
        case TypeStatusOrderingOccupied:{
            TableOrderingOccupiedDetailVC  *tableOrderingOccupiedDetailVC = [[TableOrderingOccupiedDetailVC alloc]initWithStationOrdering:stationOrderingSelected];
            tableOrderingOccupiedDetailVC.delegate=self;
            _tableOrderingDetailVC=tableOrderingOccupiedDetailVC;
            break;
        }
        default:
            break;
    }
    if (_tableOrderingDetailVC) {
        [self addChildViewController:_tableOrderingDetailVC];
        CGRect frame=self.view.frame;
        frame.origin=CGPointZero;
        _tableOrderingDetailVC.view.frame=frame;
        [self.view addSubview:_tableOrderingDetailVC.view];
    }
    else{
        return;
    }
   
    
}

-(StationOrdering *)listTableOrdering:(id)sender getStationOrderingSelected:(BOOL)none{
    return stationOrderingSelected;
}

-(NSArray*)listProductOrderShortWithListProductSingle:(NSArray*)listProductSingle{
    NSMutableArray *arrayProductOrderShort=[[NSMutableArray alloc] init];
    NSMutableArray *listProductOrdersCopy =[listProductSingle mutableCopy];
    while (listProductOrdersCopy.count>0) {
        ProductOrderSingle *firstProductOrder=(ProductOrderSingle*)[listProductOrdersCopy firstObject];
        NSString *productIDFirst=[firstProductOrder productId];
        NSArray *listProductOrderWithProID=[self listProductOrderWithProductID:productIDFirst fromListProductOrder:listProductOrdersCopy];
        [arrayProductOrderShort addObject:[self mergeProductOrderSameProduct:listProductOrderWithProID]];
        [listProductOrdersCopy removeObjectsInArray:listProductOrderWithProID];
    }
    return arrayProductOrderShort;
}
-(NSArray *)listProductOrderWithProductID:(NSString*)productID fromListProductOrder:(NSArray*)listProductOrderOrginal{
    NSMutableArray *array =[[NSMutableArray alloc] init];
    for (ProductOrderSingle *productOrder in listProductOrderOrginal) {
        if ([[productOrder productId] isEqualToString:productID]) {
            [array addObject:productOrder];
        }
    }
    return array;
}
-(NSDictionary*)mergeProductOrderSameProduct:(NSArray*)productOrderSameProduct{
    return [CashierBusinessModel mergeProductOrderSameProduct:productOrderSameProduct];
}
-(BOOL)checkDataChange:(TableOrderingDetailVC*)tbOrderingDetail{
    BOOL haveChangeData=[tbOrderingDetail checkDataChangeFromWeb];
    if (haveChangeData) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"There is data change from another device. Please reopen to check" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex==0) {
                [self listTableOrdering:_listTableOrderingVC clickDetail:stationOrderingSelected];
            }
        }];
    }
    return haveChangeData;
}
#pragma ChooseStatusDelegate
-(void)chooseStatus:(id)sender applyWithSetStatus:(NSIndexSet *)indexSet{
    if (indexSet.count==0) {
        // not tick status
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-warning"] message:@"Please select a status to continue" delegate:nil cancelButtonTitle: [languageKey stringByKey:@"general.bt.ok"]otherButtonTitles: nil];
        [alert show];
        return;
    }
    [_popover dismissPopoverAnimated:YES];
    [indexSetStatusSelected removeAllIndexes];
    [indexSetStatusSelected addIndexes:indexSet];
    [UserDefaultModel saveEncodeArchiver:indexSetStatusSelected forKey:keyUserDefaultSetStatus];
    [self reloadData];
    
}

#pragma TableOrderingDetailDelegate
-(void)tableOrderingDetail:(id)sender clickCancel:(BOOL)none{
    if (_tableOrderingDetailVC) {
        [_tableOrderingDetailVC.view removeFromSuperview];
        [_tableOrderingDetailVC removeFromParentViewController];
        _tableOrderingDetailVC=nil;
    }
}

#pragma TableOrderingWaitingDetailDelegate
-(void)tableOrderingWaiting:(id)sender approveStationOrdering:(StationOrdering *)stationOrdering{
    if ([self checkDataChange:sender]) {
        return;
    }
    //else
    [Controller updateOrdering:stationOrdering.order toStatus:OrderStatusDineInConfirm];
    // show view
    [self listTableOrdering:_listTableOrderingVC clickDetail:stationOrdering];
}
-(void)tableOrderingWaiting:(id)sender amendStationOrdering:(StationOrdering *)stationOrdering listProductFrom:(NSArray *)oldProductSingles to:(NSArray *)newProductSingles indexSetItemAvailable:(NSIndexSet *)setItemAvailable message:(NSString *)message{
    if ([self checkDataChange:sender]) {
        return;
    }
    //else
    double oldGrandTotal=[TKOrder grandTotal:stationOrdering.order];
    NSMutableArray *newProductSinglesAvailable=[[NSMutableArray alloc] init];
    for (int i=0; i<newProductSingles.count; i++) {
        if ([setItemAvailable containsIndex:i]) {
            [newProductSinglesAvailable addObject:newProductSingles[i]];
        }
    }
    NSString *currency=[TKOrder currency:stationOrdering.order];
    if (currency==nil) {
        currency=[Controller currencyDefault];
    }
    TKDiscount *discountTotal=[TKOrder discountCartItem:stationOrdering.order];
    NSArray *listTax=[TKOrder listTaxes:stationOrdering.order];
    TKTaxServiceCharge *serviceCharge=[TKOrder serviceCharge:stationOrdering.order];
    CashierBusinessModel *cashierBusiness=[[CashierBusinessModel alloc] initWithDiscountTotal:discountTotal listTax:listTax serviceCharge:serviceCharge currency:currency];
    [cashierBusiness resetListProductOrder:newProductSinglesAvailable];
    double newGrandTotal=[cashierBusiness grandTotal];
    double refundAmount =(oldGrandTotal - newGrandTotal);
    
    NSDictionary *paymentJSONFirst = [[TKOrder arrayPaymentJSON:stationOrdering.order] firstObject];
    NSString *referenceCode=[paymentJSONFirst[@"referenceCode"] convertNullToNil];
    if (referenceCode.length==0) {
        referenceCode = [stationOrdering.order.properties[@"referenceCode"] convertNullToNil];
    }
    MBProgressHUD *hub=[[MBProgressHUD alloc]initWithView: self.view];
    [self.view addSubview:hub];
    dispatch_queue_t myQueue=dispatch_queue_create("queue.amend", NULL);
    [hub show:YES];
    dispatch_async(myQueue, ^{
       
        int result =[Controller refundOrderWithReferenceCode:referenceCode refundAmount:refundAmount remarks:message]; // have 4 return code. check again
        NSLog(@"Refund Order:%@ refCode: %@ refAmount: %f remark: %@ with result: %d",stationOrdering.order.documentID,referenceCode,refundAmount,message,result  );
        dispatch_async(dispatch_get_main_queue(), ^{
            if (result==ReturnCodeRefundRefunded) {
                NSMutableDictionary *dataUpdate=[[NSMutableDictionary alloc] init];
                NSDate *currentDate=[NSDate date];
                [dataUpdate setObject:[Controller getUserLoginedDoc].properties forKey:tkKeyStaff];
                [dataUpdate setObject:[self listProductOrderShortWithListProductSingle:newProductSinglesAvailable] forKey:tkkeyArrayProduct];
                double subTotal =[cashierBusiness subTotal];
                [dataUpdate setObject:@(subTotal) forKey:tkKeySubTotal];
                double grandTotal =[cashierBusiness grandTotal];
                [dataUpdate setObject:@(grandTotal) forKey:tkKeyGrandTotal];
                double roundAmount=[cashierBusiness roundAmount];
                [dataUpdate setObject:@(roundAmount) forKey:tkKeyRoundAmount];
                [dataUpdate setObject:@(roundAmount) forKey:@"roundAmnt"];
                if (cashierBusiness.discount) {
                    [dataUpdate setObject:[cashierBusiness.discount jsonValue] forKey:tkKeyDiscountCartItem];
                    [dataUpdate setObject:@(cashierBusiness.totalCartDiscountValue) forKey:@"discountCartValue"];
                }
                [dataUpdate setObject:[CBLJSON JSONObjectWithDate:currentDate] forKey:tkKeyModifiedDate];
                [dataUpdate setObject:@([cashierBusiness totalTax]) forKey:@"totalTax"];
                [dataUpdate setObject:@([cashierBusiness totalServiceCharges]) forKey:@"totalServiceCharge"];
                if (cashierBusiness.serviceCharge) {
                    [dataUpdate setObject:[CashierBusinessModel serviceChargeJSONWithTotalAmount:subTotal withServiceCharge:cashierBusiness.serviceCharge] forKey:@"serviceCharge"];
                }
                double subTotalHaveServiceCharge=[cashierBusiness subTotalHaveServiceCharge];
                [dataUpdate setObject:[CashierBusinessModel listTaxesJSONWithTotalAmount:subTotalHaveServiceCharge withListTaxes:cashierBusiness.listTax] forKey:@"listTaxvalue"];
                
                NSMutableDictionary *refundData=[[NSMutableDictionary alloc] init];
                PaymentChild *pm=[[PaymentChild alloc] initPaymentCreditWithCharged:refundAmount paymentType:@"SmoovPay" approvedCode:@"approvedCode"];
                NSArray *arrayPaymentRefundJSONExisted=[TKOrder arrayPaymentRefundJSON:stationOrdering.order];
                NSMutableArray *newArrayPaymentJSONRefund=[[NSMutableArray alloc] init];
                if (arrayPaymentRefundJSONExisted) {
                    [newArrayPaymentJSONRefund addObjectsFromArray:arrayPaymentRefundJSONExisted];
                }
                [newArrayPaymentJSONRefund addObject:[pm jsonData]];
                [refundData setObject:newArrayPaymentJSONRefund forKey:tkkeyArrayPayment];
                [refundData setObject:[CBLJSON JSONObjectWithDate:currentDate] forKey:tkKeyRefundDate];
                [refundData setObject:@(0) forKey:@"typeRefund"];
                [refundData setObject:message forKey:tkKeyReasonRefund];
                CBLDocument *userLogined=[Controller getUserLoginedDoc];
                [refundData setObject:@([TKEmployee getEmployeeID:userLogined]) forKey:@"staffID"];
                [refundData setObject:[TKEmployee getEmployeeName:userLogined] forKey:@"staffFullName"];
                
                [dataUpdate setObject:refundData forKey:tkKeyRefundInfo];
                [dataUpdate setObject:userLogined.properties forKey:tkKeyStaff];
                [[SyncManager sharedSyncManager] updateDocument:stationOrdering.order propertyDoc:dataUpdate];
                [Controller sendNotifyWhenAmendOrder:stationOrdering.order withMessage:message];
                [Controller updateOrdering:stationOrdering.order toStatus:OrderStatusDineInConfirm];
                [self listTableOrdering:_listTableOrderingVC clickDetail:stationOrdering];
            }else{
                [self showErrorWhenRefundWithReturnCode:result];
            }
            [hub hide:YES];
        });
    });
}
-(void)showErrorWhenRefundWithReturnCode:(NSInteger)returnCodeRefund{
    NSString *alertMessage;
    switch (returnCodeRefund) {
        case ReturnCodeRefundPendingForApprove:
        {
            alertMessage = [languageKey stringByKey:@"ordering.ms-system-pending-for-approve"];
            break;
        }
        case ReturnCodeRefundReject:
        {
            alertMessage = [languageKey stringByKey:@"ordering.ms-confirm-reject-refund"];
            break;
        }
        case ReturnCodeRefundFailed:
        {
            alertMessage = [languageKey stringByKey:@"ordering.ms-system-fail-refund"];
            break;
        }
        case ReturnCodeErrorAccountSmoovPayNotExist:
        {
            alertMessage = [languageKey stringByKey:@"ordering.ms-error-account-smoovpay-not-exit"];
            break;
        }
        case ReturnCodeErrorMissingParam:
        case ReturnCodeOrderNotExist:
        default:{
            alertMessage = [languageKey stringByKey:@"ordering.ms-system-fail-refund"];
            break;
        }
    }
    if(alertMessage){
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-warning"] message:alertMessage delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)tableOrderingWaiting:(id)sender rejectStationOrdering:(StationOrdering *)stationOrdering
{
    if ([self checkDataChange:sender]) {
        return;
    }
    if(stationOrdering.order == nil){
        return;
    }
    if (![NetworkUtil checkInternetConnection]) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-alert"] message:[languageKey stringByKey:@"general.ms.not-connect-internet"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    CBLDocument *order = stationOrdering.order;
    double grandTotal=[TKOrder grandTotal:order];
    
    MBProgressHUD *hub=[[MBProgressHUD alloc]initWithView: self.view];
    [self.view addSubview:hub];
    dispatch_queue_t myQueue=dispatch_queue_create("queue.refund", NULL);
    [hub show:YES];
    dispatch_async(myQueue, ^{
        NSDictionary *paymentJSONFirst = [[TKOrder arrayPaymentJSON:stationOrdering.order] firstObject];
        NSString *referenceCode=[paymentJSONFirst[@"referenceCode"] convertNullToNil];
        if (referenceCode.length==0) {
            referenceCode = [order.properties[@"referenceCode"] convertNullToNil];
        }
        if (referenceCode==nil) {
            referenceCode=@"";
        }
        NSString *remarks=@"Cancel Order";
        int returnCode = [Controller refundOrderWithReferenceCode:referenceCode refundAmount:grandTotal remarks:remarks];
        NSLog(@"Refund Order:%@ refCode: %@ refAmount: %f remark: %@ with result: %d",stationOrdering.order.documentID,referenceCode,grandTotal,remarks,returnCode  );
        dispatch_async(dispatch_get_main_queue(), ^{
            [hub hide:YES];
            if (returnCode==ReturnCodeRefundRefunded) {
                PaymentChild *pm=[[PaymentChild alloc] initPaymentCreditWithCharged:grandTotal paymentType:@"SmoovPay" approvedCode:referenceCode];
                [Controller updateRefundOrder:stationOrdering.order withArrayPaymentRefundJSON:@[[pm jsonData]] reason:remarks];
                [Controller updateOrdering:stationOrdering.order toStatus:OrderStatusRefund];
                [self listTableOrdering:_listTableOrderingVC clickDetail:stationOrdering];
            }else{
                [self showErrorWhenRefundWithReturnCode:returnCode];
            }
        });
    });

}
#pragma TableOrderingConfirmDetailDelegate
-(void)tableOrderingConfirm:(id)sender readyStationOrdering:(StationOrdering *)stationOrdering{
    if ([self checkDataChange:sender]) {
        return;
    }
    //else
    [Controller updateOrdering:stationOrdering.order toStatus:OrderStatusDineInServing];
    [self listTableOrdering:_listTableOrderingVC clickDetail:stationOrdering];
}
-(void)tableOrderingConfirm:(id)sender cancelAndRefundStationOrdering:(StationOrdering *)stationOrdering{
    [self tableOrderingWaiting:sender rejectStationOrdering:stationOrdering]; // in function have check data change
}

#pragma TableOrderingServingDetailDelegate
-(void)tableOrderingServing:(id)sender clearStationOrdering:(StationOrdering *)stationOrdering{
    if ([self checkDataChange:sender]) {
        return;
    }
    //else
    [Controller clearStationOrdering:stationOrdering];
    if (_tableOrderingDetailVC) {
        [_tableOrderingDetailVC.view removeFromSuperview];
        [_tableOrderingDetailVC removeFromParentViewController];
        _tableOrderingDetailVC=nil;
    }
}

#pragma TableOrderingOccupiedDetailDelegate
-(void)tableOrderingOccupied:(id)sender clearStationOrdering:(StationOrdering *)stationOrdering{
    if ([self checkDataChange:sender]) {
        return;
    }
    //else
    [Controller clearStationOrdering:stationOrdering];
    if (_tableOrderingDetailVC) {
        [_tableOrderingDetailVC.view removeFromSuperview];
        [_tableOrderingDetailVC removeFromParentViewController];
        _tableOrderingDetailVC=nil;
    }
}
@end
