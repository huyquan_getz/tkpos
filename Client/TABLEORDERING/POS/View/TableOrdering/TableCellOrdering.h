//
//  TableCellOrdering.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseStatusVC.h"

@interface TableCellOrdering : UITableViewCell{
    TypeStatusOrdering status;
}
@property (weak, nonatomic) IBOutlet UILabel *lbStation;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbOrderCode;
@property (weak, nonatomic) IBOutlet UILabel *lbItem;
@property (weak, nonatomic) IBOutlet UILabel *lbSession;
@property (weak, nonatomic) IBOutlet UILabel *lbWaiting;
@property (weak, nonatomic) IBOutlet UILabel *lbStatus;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (strong,nonatomic) NSDate *startWaitingDate;
-(void)setValueDefault;
-(void)reShowWaitingTime;
-(void)setStatus:(TypeStatusOrdering)status_;
@end
