//
//  TableCellOrdering.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellOrdering.h"
#import "Constant.h"

@implementation TableCellOrdering

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    if(selected){
        self.backgroundColor=tkColorMain;
        NSArray *changeColor=@[_lbStation,_lbName,_lbOrderCode,_lbItem,_lbSession,_lbWaiting];
        for (UILabel *lb in changeColor) {
            [lb setTextColor:[UIColor whiteColor]];
        }
        if (status ==TypeStatusOrderingEmpty) {
            _lbStatus.textColor=[UIColor whiteColor];
        }
    }else{
        self.backgroundColor=[UIColor whiteColor];
        NSArray *changeColor=@[_lbStation,_lbName,_lbOrderCode,_lbItem,_lbSession,_lbWaiting];
        for (UILabel *lb in changeColor) {
            [lb setTextColor:[UIColor darkGrayColor]];
        }
        switch (status) {
            case TypeStatusOrderingWaiting:
                _lbOrderCode.textColor=tkColorDarkRed;
                break;
            case TypeStatusOrderingEmpty:
                _lbStatus.textColor=[UIColor darkGrayColor];
                break;
            default:
                break;
        }
    }
    _vLine.hidden=selected;
    // Configure the view for the selected state
}
-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    // nothing
}
-(void)setValueDefault{
//    UIView *bgSelected=[[UIView alloc] init];
//    [bgSelected setBackgroundColor:tkColorMainSelected];
//    [super setSelectedBackgroundView:bgSelected];
    _vLine.backgroundColor=tkColorFrameBorder;
    status=TypeStatusOrderingNone;
    _lbStation.text=@"";
    _lbName.text=@"";
    _lbOrderCode.text=@"";
    _lbItem.text=@"";
    _lbSession.text=@"";
    _lbWaiting.text=@"";
    _lbStatus.text=@"";
    _lbStatus.layer.cornerRadius=tkCornerRadiusButton;
    _lbStatus.clipsToBounds=YES;
}
-(void)setStatus:(TypeStatusOrdering)status_{
    status=status_;
    UIColor *backgroundStatus=[UIColor clearColor];
    UIColor *textColorStatus=[UIColor clearColor];
    _lbStatus.textColor=[UIColor whiteColor];
    switch (status) {
        case TypeStatusOrderingWaiting:
            backgroundStatus=tkColorDarkRed;
            textColorStatus=[UIColor whiteColor];
            break;
        case TypeStatusOrderingConfirmed:
            backgroundStatus=tkColorPending;
            textColorStatus=[UIColor whiteColor];
            break;
        case TypeStatusOrderingServing:
            backgroundStatus=tkColorReady;
            textColorStatus=[UIColor whiteColor];
            break;
        case TypeStatusOrderingOccupied:
            backgroundStatus=[UIColor darkGrayColor];
            textColorStatus=[UIColor whiteColor];
            break;
        case TypeStatusOrderingEmpty:
            backgroundStatus=[UIColor clearColor];
            textColorStatus=[UIColor darkGrayColor];
            break;
        default:
            break;
    }
    _lbStatus.backgroundColor=backgroundStatus;
    _lbStatus.textColor=textColorStatus;
}
-(void)reShowWaitingTime{
    NSString *h=[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.hour-short"];
    NSString *m=[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.minute-short"];
    NSString *s=[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.second-short"];
    if (status != TypeStatusOrderingEmpty && status != TypeStatusOrderingOccupied) {
        NSDate *currentDate=[NSDate date];
        NSInteger subDate=(NSInteger)[currentDate timeIntervalSinceDate:_startWaitingDate];
        if(subDate >= 3600){
            self.lbSession.text=[NSString stringWithFormat:@"%li%@ %li%@",subDate/3600,h,(subDate%3600)/60,m];
        }else{
            self.lbSession.text=[NSString stringWithFormat:@"%li%@ %li%@",subDate/60,m,subDate%60,s];
        }
        
        // waiting time
        if (status == TypeStatusOrderingWaiting) {
            NSDate *currentDate=[NSDate date];
            NSInteger subDate=(NSInteger)[currentDate timeIntervalSinceDate:_startWaitingDate];
            if(subDate >= 3600){
                self.lbWaiting.text=[NSString stringWithFormat:@"%li%@ %li%@",subDate/3600,h,(subDate%3600)/60,m];
            }else{
                self.lbWaiting.text=[NSString stringWithFormat:@"%li%@ %li%@",subDate/60,m,subDate%60,s];
            }
        }else{
            self.lbWaiting.text=@"";
        }
    }
}
@end
