//
//  StatusOrderingCell.h
//  TABLEORDERING
//
//  Created by Nguyen Anh Dao (Zin) on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusOrderingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivChooseStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@end
