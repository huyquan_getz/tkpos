//
//  TableOrderingVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableOrderingListener.h"
#import "ListTableOrderingVC.h"
#import "ChooseStatusVC.h"
#import "TableOrderingDetailVC.h"
#import "StationOrdering.h"

@interface TableOrderingVC : UIViewController<TableOrderingListenerDelegate,UIPopoverControllerDelegate,ListTableOrderingDelegate,ChooseStatusDelegate,TableOrderingDetailDelegate,TableOrderingWaitingDelegate,TableOrderingServingDelegate,TableOrderingConfirmedDelegate,TableOrderingOccupiedDelegate>{
    NSMutableArray *listStationOrderings;
    StationOrdering *stationOrderingSelected;
    NSMutableIndexSet *indexSetStatusSelected;
}
- (IBAction)clickLeftMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *lable2;
@property (weak, nonatomic) IBOutlet UIImageView *ivNavi;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (strong,nonatomic) ListTableOrderingVC *listTableOrderingVC;
@property (strong,nonatomic) TableOrderingDetailVC *tableOrderingDetailVC;
@property (weak, nonatomic) IBOutlet UIView *vFrameCenter;
@property (strong, nonatomic)UIPopoverController *popover;
@property (weak, nonatomic) IBOutlet UILabel *lbNbStationWaiting;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleNbStationWaiting;
@property (weak, nonatomic) IBOutlet UILabel *lbNbStationConfirmed;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleNbStationConfirmed;
@property (weak, nonatomic) IBOutlet UILabel *lbNbStationServing;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleNbStationServing;
@property (weak, nonatomic) IBOutlet UIButton *btChooseStatus;
- (IBAction)clickChooseStatus:(id)sender;
@end
