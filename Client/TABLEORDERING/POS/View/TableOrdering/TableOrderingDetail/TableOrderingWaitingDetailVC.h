//
//  TableOrderingWaitingDetailVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableOrderingDetailVC.h"
#import "WaitingAmendVC.h"
#import "TableOrderingConfirmedDetailVC.h"
#import "ViewHeaderOrderingDetail.h"
#import "OrderDetailVC.h"
@interface TableOrderingWaitingDetailVC : TableOrderingDetailVC<WaitingAmendDelegate>{

}
- (IBAction)btnCancel:(id)sender;
@property (weak, nonatomic) id<TableOrderingWaitingDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleTableOrder;
@property (weak, nonatomic) IBOutlet UIView *vTableOrderDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblViewHeader;
@property (weak, nonatomic) IBOutlet UIView *vFrameDetail;
@property (strong,nonatomic) WaitingAmendVC *waitingAmendVC;
@property (strong,nonatomic) TableOrderingConfirmedDetailVC *tableOrderingConfirmedDetailVC;
@property (strong,nonatomic) OrderDetailVC *orderDetailVC;
@property (weak, nonatomic) IBOutlet UIButton *btApprove;
@property (weak, nonatomic) IBOutlet UIButton *btAmend;
@property (weak, nonatomic) IBOutlet UIButton *btReject;
- (IBAction)clickApprove:(id)sender;
- (IBAction)clickAmend:(id)sender;
- (IBAction)clickReject:(id)sender;
@end
