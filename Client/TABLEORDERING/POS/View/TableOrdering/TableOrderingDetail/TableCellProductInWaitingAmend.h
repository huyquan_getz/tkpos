//
//  TableCellProductInWaitingAmend.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol TableCellProductInWaitingAmendDelegate <NSObject>
-(BOOL)tableCellProductInWaitingAmend:(id)sender shouldChangeStatusTick:(BOOL)newStatusTick indexPath:(NSIndexPath*)indexPath;
-(void)tableCellProductInWaitingAmend:(id)sender didBeginEditing:(UITextField*)textField indexPath:(NSIndexPath *)indexPath;
-(void)tableCellProductInWaitingAmend:(id)sender didEndEditing:(UITextField*)textField indexPath:(NSIndexPath *)indexPath;
-(BOOL)tableCellProductInWaitingAmend:(id)sender shouldChangeQuantity:(UITextField *)textField to:(NSInteger)newQuantity indexPath:(NSIndexPath *)indexPath;

@end
#import <UIKit/UIKit.h>

@interface TableCellProductInWaitingAmend : UITableViewCell<UITextFieldDelegate>
@property (weak,nonatomic)id<TableCellProductInWaitingAmendDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btTick;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UITextField *tfQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lbAmount;
@property (weak, nonatomic) IBOutlet UILabel *lbRemark;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (strong,nonatomic) NSIndexPath *indexPathCell;
- (IBAction)clickTick:(id)sender;
-(void)setTick:(BOOL)tick;
-(void)setVariant:(NSString*)variant remark:(NSString*)remark;
+(CGFloat)heightCellWithVariant:(NSString*)variant remark:(NSString*)remark;
@end
