//
//  TableOrderingDetailVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "StationOrdering.h"
@protocol TableOrderingDetailDelegate <NSObject>
-(void)tableOrderingDetail:(id)sender clickCancel:(BOOL)none;
@end

#pragma Waiting
@protocol TableOrderingWaitingDelegate <TableOrderingDetailDelegate>

-(void)tableOrderingWaiting:(id)sender approveStationOrdering:(StationOrdering*)stationOrdering;
-(void)tableOrderingWaiting:(id)sender amendStationOrdering:(StationOrdering*)stationOrdering listProductFrom:(NSArray*)oldProductSingles to:(NSArray*)newProductSingles indexSetItemAvailable:(NSIndexSet*)setItemAvailable message:(NSString*)message; // amend refund order and return list PaymentRefund (PaymentChild)
-(void)tableOrderingWaiting:(id)sender rejectStationOrdering:(StationOrdering*)stationOrdering;
@end

#pragma Confirm
@protocol TableOrderingConfirmedDelegate <TableOrderingDetailDelegate>
-(void)tableOrderingConfirm:(id)sender readyStationOrdering:(StationOrdering*)stationOrdering;
-(void)tableOrderingConfirm:(id)sender cancelAndRefundStationOrdering:(StationOrdering*)stationOrdering;
@end

#pragma Serving
@protocol TableOrderingServingDelegate <TableOrderingDetailDelegate>
-(void)tableOrderingServing:(id)sender clearStationOrdering:(StationOrdering*)stationOrdering;
@end

#pragma Occupied
@protocol TableOrderingOccupiedDelegate <TableOrderingDetailDelegate>
-(void)tableOrderingOccupied:(id)sender clearStationOrdering:(StationOrdering*)stationOrdering;
@end

#pragma Empty
@protocol TableOrderingEmptyDelegate <TableOrderingDetailDelegate>

@end

#pragma
@protocol TableOrderDetailDataShow <NSObject>


@end
#import <UIKit/UIKit.h>

@interface TableOrderingDetailVC : UIViewController<TableOrderDetailDataShow>{
    StationOrdering *stationOrdering;
    __weak UIAlertView *currentAlert;
}
-(instancetype)initWithStationOrdering:(StationOrdering*)stationOrdering_;
-(BOOL)checkDataChangeFromWeb;
-(void)dismissCurrentAlertView;
@end
