//
//  TableCellProductInDetail.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellProductInDetail.h"
#import "Constant.h"

@implementation TableCellProductInDetail

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setVariant:(NSString *)variant remark:(NSString *)remark{
    CGRect frame=_lbTitle.frame;
    if (variant==nil || variant.length==0) {
        _lbDescription.text=@"";
        frame.size.height=50;
    }else{
        frame.size.height=30;
        _lbDescription.text=variant;
    }
    _lbTitle.frame=frame;
    
    if (remark && remark.length>0) {
        remark =[@"* " stringByAppendingString:remark];
        NSInteger heightRemark =[remark heightWithFont:_lbRemark.font width:_lbRemark.frame.size.width];
        if (heightRemark>30) {
            //2 line
            frame =_lbRemark.frame;
            frame.size.height=35;
            _lbRemark.frame=frame;
            
            frame=_vLine.frame;
            frame.origin.y=89;
            _vLine.frame=frame;
        }else{
            //1 line
            frame =_lbRemark.frame;
            frame.size.height=20;
            _lbRemark.frame=frame;
            
            frame=_vLine.frame;
            frame.origin.y=74;
            _vLine.frame=frame;
        }
        _lbRemark.text=remark;
    }else{
        _lbRemark.text=@"";
        frame=_vLine.frame;
        frame.origin.y=49;
        _vLine.frame=frame;
    }
    
}
+(CGFloat)heightCellWithVariant:(NSString *)variant remark:(NSString *)remark{
    if (remark && remark.length>0) {
        remark =[@"* " stringByAppendingString:remark];
        NSInteger heightRemark =[remark heightWithFont:tkFontMainWithSize(14) width:487];
        if (heightRemark>30) {
            //2 line
            return 90;
        }else{
            //1 line
            return 75;
        }
    }
    return 50;
}
@end
