//
//  ViewHeaderOrderingDetail.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/20/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "ViewHeaderOrderingDetail.h"
#import "Constant.h"

@implementation ViewHeaderOrderingDetail

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"ViewHeaderOrderingDetail" owner:self options:nil];
        self.bounds=self.view.bounds;
        [self addSubview:self.view];
        [self setFirstValue];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"ViewHeaderOrderingDetail" owner:self options:nil];
        self.bounds=self.view.bounds;
        [self addSubview:self.view];
        [self setFirstValue];
    }
    return self;
}
-(void)setFirstValue{
    _lbName.textColor=tkColorMain;
    _ivAvata.layer.borderWidth=1;
    _ivAvata.layer.borderColor=tkColorFrameBorder.CGColor;
    _ivAvata.layer.cornerRadius=_ivAvata.frame.size.width/2;
    _ivAvata.clipsToBounds=YES;
    _lbStatus.backgroundColor=[UIColor clearColor];
}
-(void)setValueDefault{
    _lbStatus.text=@"";
    _lbName.text=@"";
    _lbEmail.text=@"";
    _ivAvata.image=[UIImage imageNamed:@"no_avatar.png"];
}
-(void)setLbStatusColor:(UIColor*)color text:(NSString *)text{
    _lbStatus.text=text;
    _lbStatus.textColor=color;
    _borderStatus.borderType=BorderTypeDashed;
    _borderStatus.dashPattern=4;
    _borderStatus.spacePattern=4;
    _borderStatus.borderWidth=2;
    _borderStatus.borderColor=color;
    _borderStatus.cornerRadius=tkCornerRadiusButton;
}
-(void)setImageAvataWithURL:(NSString *)urlString{
    [_ivAvata loadImageFromUrl:urlString completed:^(NSError *error, UIImage *image) {
        if (image) {
            _ivAvata.image=image;
        }else{
            _ivAvata.image=[UIImage imageNamed:@"no_avatar.png"];
        }
    }];
}
@end
