//
//  WaitingAmendVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "StationOrdering.h"
@protocol WaitingAmendDelegate <NSObject>
-(void)waitingAmend:(id)sender clickCancel:(BOOL)none;
-(void)waitingAmend:(id)sender amendStationOrdering:(StationOrdering*)stationOrdering listProductFrom:(NSArray*)oldProductSingles to:(NSArray*)newProductSingles indexSetItemAvailable:(NSIndexSet*)setItemAvailable message:(NSString*)message;

@end
#import <UIKit/UIKit.h>
#import "TableCellProductInWaitingAmend.h"
#import "StationOrdering.h"

@interface WaitingAmendVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,TableCellProductInWaitingAmendDelegate>{
    StationOrdering *stationOrdering;
}
-(instancetype)initWithStationOrdering:(StationOrdering*)stationOrdering_;
@property (weak,nonatomic) id<WaitingAmendDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleMessage;
@property (weak, nonatomic) IBOutlet UITextView *tvMessage;
@property (weak, nonatomic) IBOutlet UIButton *btAccept;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
- (IBAction)clickAcceptAndRefund:(id)sender;
- (IBAction)clickCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *bgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbOrderCode;
@property (weak, nonatomic) IBOutlet UIView *bgOrderCode;

@end
