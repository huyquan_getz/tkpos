//
//  LoginManagerVC.h
//  POS
//
//  Created by Nguyen Anh Dao on 10/31/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginManagerVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btBack;
@property (weak, nonatomic) IBOutlet UIButton *btSignIn;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UIView *vManager;
@property (weak, nonatomic) IBOutlet UIView *vEmailPass;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageLogin;

@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbPassword;
@property (weak, nonatomic) IBOutlet UILabel *lbManagerLogin;
@property (weak, nonatomic) IBOutlet UIView *vLine;


- (IBAction)clickBack:(id)sender;
- (IBAction)clickSignIn:(id)sender;

@end
