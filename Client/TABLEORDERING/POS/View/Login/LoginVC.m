//
//  LoginVC.m
//  POS
//
//  Created by Nha Duong Cong on 10/13/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "LoginVC.h"
#import "UserDefaultModel.h"
#import "UIAlertView+Blocks.h"
#import "NSString+Util.h"
#import "Controller.h"
#import "UITextField+Util.h"
#import "NetworkUtil.h"

@interface LoginVC ()

@end

@implementation LoginVC{
    LanguageUtil *languageKey;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //hiden button back
    _btBack.hidden=YES;
    _btCreateAccount.hidden=YES;
    _btForgotPass.hidden=YES;
    self.view.frame = [AppDelegate bounds];
    _btLogin.layer.cornerRadius=tkCornerRadiusButton;
    [_btLogin setBackgroundColor:tkColorMain];
    [_btLogin.titleLabel setFont:tkFontMainTitleButton];
    [_btLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIView *leftBN=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tkSpaceLeft, 1)];
    _tfBusinessName.leftView=leftBN;
    _tfBusinessName.leftViewMode=UITextFieldViewModeAlways;
    _tfBusinessName.layer.cornerRadius=tkCornerRadiusButton;
    _tfBusinessName.layer.borderWidth=1;
    _tfBusinessName.layer.borderColor=tkColorFrameBorder.CGColor;
    
    UIView *leftUN=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tkSpaceLeft, 1)];
    _tfUsername.leftView=leftUN;
    _tfUsername.leftViewMode=UITextFieldViewModeAlways;
    _tfUsername.layer.cornerRadius=tkCornerRadiusButton;
    _tfUsername.layer.borderWidth=1;
    _tfUsername.layer.borderColor=tkColorFrameBorder.CGColor;
    
    UIView *leftPW=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tkSpaceLeft, 1)];
    _tfPassword.leftView=leftPW;
    _tfPassword.leftViewMode=UITextFieldViewModeAlways;
    _tfPassword.layer.cornerRadius=tkCornerRadiusButton;
    _tfPassword.layer.borderWidth=1;
    _tfPassword.layer.borderColor=tkColorFrameBorder.CGColor;
    
    _btLogin.backgroundColor=tkColorButtonBackground;
    
    [_lbStatusMessage setText:@""];
    _vMainLogin.backgroundColor=[UIColor clearColor];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    _tfUsername.text=@"";
    _tfPassword.text=@"";
    _tfBusinessName.text=@"";
    _tfUsername.placeholder=[languageKey stringByKey:@"general.lb.email-holder"];
    _tfPassword.placeholder=[languageKey stringByKey:@"general.lb.password-holder"];
    _tfBusinessName.placeholder=[languageKey stringByKey:@"general.bl.business-name-holder"];
    [_btLogin setTitle:[languageKey stringByKey:@"login.bt.sign-in"] forState:UIControlStateNormal];
    [_btForgotPass setTitle:[languageKey stringByKey:@"login.bt.forgot-pass"] forState:UIControlStateNormal];
    [_btCreateAccount setTitle:[languageKey stringByKey:@"login.bt.create-account"] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)hideKeyboard{
    [_tfUsername resignFirstResponder];
    [_tfPassword resignFirstResponder];
}
- (IBAction)clickLogin:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self hideKeyboard];
    [self clearStatus];
    if (![NetworkUtil checkInternetConnection]) {
        [UIAlertView showMessage:[languageKey stringByKey:@"general.ms.not-connect-internet"]];
        return;
    }
    MBProgressHUD *hub=[[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hub];
    hub.labelText=[languageKey stringByKey:@"login.hub-authen.authenticating"];
    [hub show:YES];
    dispatch_queue_t myQueue=dispatch_queue_create("queue.Authentication", NULL);
    dispatch_async(myQueue, ^{
        NSDictionary *dictMerchantData=[Controller checkMerchantWithBusinessName:_tfBusinessName.text username:_tfUsername.text password:_tfPassword.text];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hub hide:YES];
            [hub removeFromSuperview];
            //login root
            int returnCode=[dictMerchantData[tkKeyReturnStatus] intValue];
            switch (returnCode) {
                case ReturnCodeSuccess:{
                    [self loginSuccessWithDictMerchant:dictMerchantData[tkKeyResult]];
                    break;
                }
                case ReturnCodeBusinessNotExist:
                    [self showMessageError:[languageKey stringByKey:@"login.dl.ms-business-name-not-found"]];
                    break;
                case ReturnCodeUserPassInvalid:
                    [self showMessageError:[languageKey stringByKey:@"login.dl.ms-user-pass-incorrect"]];
                    break;
                case ReturnCodeUserNotPermission:
                    [self showMessageError:@"Your user haven't permission."];
                    break;
                case ReturnCodeAppUnavailable:
                    [self showMessageError:[languageKey stringByKey:@"ordering.ms-app-table-unavailable"]];
                    break;
                case ReturnCodeDeviceUnavailable:
                    [self showMessageError:[languageKey stringByKey:@"ordering.ms-device-unavailable"]];
                    break;
                case ReturnCodeLimitDevice:
                    [self showMessageError:[languageKey stringByKey:@"ordering.ms-device-limited"]];
                    break;
                default:{
                    [UIAlertView showMessage:[languageKey stringByKey:@"general.ms.not-connect-server"]];
                    break;
                }
            }
        });
    });
}
-(void)loginSuccessWithDictMerchant:(NSDictionary*)dict{
    [Controller saveMerchantSyncInfor:dict];
    //show choose store
    [Controller showStoreLoginViewWithStores:dict[@"stores"]];
}
-(BOOL)validateInput{
    _tfBusinessName.text=[_tfBusinessName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _tfUsername.text=[_tfUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ( [_tfUsername isEmptyText] ) {
        [self showMessageError:[languageKey stringByKey:@"login.dl.ms-username-empty"]];
        return NO;
    }
    if( [_tfPassword isEmptyText] ){
        [self showMessageError:[languageKey stringByKey:@"login.dl.ms-password-empty"]];
        return NO;
    }
    if ( ! [_tfUsername.text isEmailFormat] ){
        [self showMessageError:[languageKey stringByKey:@"login.dl.ms-email-invalid"]];
        return NO;
    }
    return YES;
}

- (IBAction)clickCreateAccount:(id)sender {
    NSString *urlSmoovPosSignIn=tkLinkSignUp;
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:urlSmoovPosSignIn]];
}
-(void)showMessageError:(NSString*)error{
    [_lbStatusMessage setTextColor:tkColorStatusMessageError];
    [_lbStatusMessage setText:error];
}
-(void)clearStatus{
    [_lbStatusMessage setText:@""];
}
- (IBAction)clickForgotPassword:(id)sender {
    [self hideKeyboard];
    loginForgotVC=[[LoginForgotVC alloc] init];
    loginForgotVC.delegate=self;
    [self addChildViewController:loginForgotVC];
    [self.view addSubview:loginForgotVC.view];
}
- (IBAction)clickBack:(id)sender {
    [Controller showWelcomeView];
}
#pragma TextfieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField==_tfBusinessName) {
        [_tfUsername becomeFirstResponder];
    }else if (textField==_tfUsername) {
        [_tfPassword becomeFirstResponder];
    }else if(textField==_tfPassword){
        [textField resignFirstResponder];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self clickLogin:nil];
        });
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=_vMainLogin.frame;
    frame.origin.y-=100;
    _vMainLogin.frame=frame;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=_vMainLogin.frame;
    frame.origin.y+=100;
    _vMainLogin.frame=frame;
}
#pragma LoginForgotDelegate
-(void)loginForgotVC:(LoginForgotVC*)sender send:(BOOL)send{
    [sender.view removeFromSuperview];
    [sender removeFromParentViewController];
    loginForgotVC=nil;
}

@end
