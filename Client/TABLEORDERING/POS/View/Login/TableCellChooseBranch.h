//
//  TableCellChooseBranch.h
//  POS
//
//  Created by Cong Nha Duong on 1/23/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellChooseBranch : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ivTick;
@property (weak, nonatomic) IBOutlet UIView *vLine;

@end
