//
//  LoginCashierVC.h
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PinCodeVC.h"
@interface LoginCashierVC : UIViewController<PinCodeDelegate>
- (IBAction)clickForgotID:(id)sender;
- (IBAction)clickNumber:(id)sender;
- (IBAction)clickClear:(id)sender;
- (IBAction)clickEnter:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vInput;
@property (weak, nonatomic) IBOutlet UIButton *btManager;
@property (weak, nonatomic) IBOutlet UIButton *btNumber1;
@property (weak, nonatomic) IBOutlet UIButton *btNumber2;
@property (weak, nonatomic) IBOutlet UIButton *btNumber3;
@property (weak, nonatomic) IBOutlet UIButton *btNumber4;
@property (weak, nonatomic) IBOutlet UIButton *btNumber5;
@property (weak, nonatomic) IBOutlet UIButton *btNumber6;
@property (weak, nonatomic) IBOutlet UIButton *btNumber7;
@property (weak, nonatomic) IBOutlet UIButton *btNumber8;
@property (weak, nonatomic) IBOutlet UIButton *btNumber9;
@property (weak, nonatomic) IBOutlet UIButton *btNumber0;
@property (weak, nonatomic) IBOutlet UIButton *btForgotPass;
@property (weak, nonatomic) IBOutlet UILabel *lbStatusMessage;
@property (weak, nonatomic) IBOutlet UIButton *btClear;
@property (weak, nonatomic) IBOutlet UIButton *btEnter;
@property (strong,nonatomic)PinCodeVC *pincodeVC;

- (IBAction)clickManager:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UIView *vLine21;
@property (weak, nonatomic) IBOutlet UIView *vLine22;
@property (weak, nonatomic) IBOutlet UIView *vLine23;
@property (weak, nonatomic) IBOutlet UIView *vLine31;
@property (weak, nonatomic) IBOutlet UIView *vLine32;
@property (weak, nonatomic) IBOutlet UIView *vLine33;
@property (weak, nonatomic) IBOutlet UIView *vLine34;
@property (weak, nonatomic) IBOutlet UIView *vLine35;
@property (weak, nonatomic) IBOutlet UIView *vLine36;
@property (weak, nonatomic) IBOutlet UIView *vTextField;
@property (weak, nonatomic) IBOutlet UITextField *tfID1;
@property (weak, nonatomic) IBOutlet UITextField *tfID2;
@property (weak, nonatomic) IBOutlet UITextField *tfID3;
@property (weak, nonatomic) IBOutlet UITextField *tfID4;
@end
