//
//  LoginChooseStoreVC.m
//  POS
//
//  Created by Nguyen Anh Dao on 10/30/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define CellBranchIdentify @"cellBranch"
#import "LoginChooseStoreVC.h"
#import "Controller.h"
#import "CouchBaseBusinessModel.h"
#import "UIAlertView+Blocks.h"
#import "NetworkUtil.h"
#import "Constant.h"
#import "TableCellChooseBranch.h"
@interface LoginChooseStoreVC ()
@end
@implementation LoginChooseStoreVC
{
    LanguageUtil *languageKey;
    NSInteger indexSelected;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithArrayStore:(NSArray *)array{
    if (self =[super init]) {
        listStores =[[NSMutableArray alloc] initWithArray:array];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    indexSelected=-1;
    _btStart.layer.cornerRadius =tkCornerRadiusButton;
    _btStart.backgroundColor=tkColorMain;
    [_btStart.titleLabel setFont:tkFontMainTitleButton];
    
    [_btSignOut.titleLabel setFont:tkFontMainTitleButton];
    
    _tbvStore.layer.cornerRadius=tkCornerRadiusViewPopup;
    _tbvStore.layer.borderColor=tkColorFrameBorder.CGColor;
    _tbvStore.layer.borderWidth=1;
    [_tbvStore registerNib:[UINib nibWithNibName:@"TableCellChooseBranch" bundle:nil] forCellReuseIdentifier:CellBranchIdentify];
    _btStart.backgroundColor=tkColorButtonBackground;
    [self showMessageError:@""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [_tbvStore reloadData];
    languageKey = [LanguageUtil sharedLanguageUtil];
    _lbSelectStore.text = [languageKey stringByKey:@"login.lb.select-store-default"];
    [ _btSignOut setTitle:[languageKey stringByKey:@"login.bt.log-out-sign-another-account"] forState:UIControlStateNormal];
    [ _btStart setTitle:[languageKey stringByKey:@"login.bt.start"] forState:UIControlStateNormal];
}
-(void)showMessageError:(NSString*)error{
    _lbMessageStatus.textColor=[UIColor redColor];
    _lbMessageStatus.text=error;
}
- (IBAction)clickSignOut:(id)sender {
    [Controller showMerchantLoginFirst];
}

- (IBAction)clickStart:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self showMessageError:@""];
    if (![NetworkUtil checkInternetConnection]) {
        [UIAlertView showMessage:[languageKey stringByKey:@"general.ms.not-connect-internet"]];
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [Controller startSyncFirstWithStore:listStores[indexSelected]];
    });
}

-(BOOL)validateInput{
    if (indexSelected<0) {
        [self showMessageError:[languageKey stringByKey:@"login.choose-store.ms-choose-store-default"]];
        return NO;
    }else{
        return YES;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listStores.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableCellChooseBranch *cell=[tableView dequeueReusableCellWithIdentifier:CellBranchIdentify];
    NSDictionary *store=[listStores objectAtIndex:indexPath.row] ;
    cell.lbTitle.text=[store  objectForKey:tkKeyBusinessID];
    cell.vLine.backgroundColor=tkColorFrameBorder;
    return cell;

}
#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    indexSelected=indexPath.row;
    
}
@end
