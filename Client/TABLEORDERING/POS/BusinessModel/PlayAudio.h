//
//  PlayAudio.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/11/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Audio.h"

@interface PlayAudio : NSObject<AudioDelegate>{
    NSMutableArray *listAudio;
}
+(instancetype)sharedObject;
-(void)addAudio:(Audio*)audio;
+(void)playAudioNewOrdering;
@end
