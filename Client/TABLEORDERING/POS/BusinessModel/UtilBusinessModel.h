//
//  UtilBusinessModel.h
//  POS
//
//  Created by Nha Duong Cong on 11/1/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>

@interface UtilBusinessModel : NSObject
+(NSString*)stringUrlSyncData;
+(id<CBLAuthenticator>)authenticationBasic;
+(NSArray*)channelsSync;
+(void)addChannels:(NSArray*)channelsAdd;
@end
