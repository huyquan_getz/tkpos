//
//  File.m
//  POS
//
//  Created by Cong Nha Duong on 1/14/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "File.h"

@implementation File
+(NSString *)rootFolder{
    NSString *rootFoler=[NSHomeDirectory() stringByAppendingPathComponent:@"Data"];
    if (![self folderExist:rootFoler]) {
        [self createFolder:rootFoler];
    }
    return rootFoler;
}
+(NSString*)printingFolder{
    NSString *printingFolder=[[self rootFolder] stringByAppendingPathComponent:@"Printing"];
    if (![self folderExist:printingFolder]) {
        [self createFolder:printingFolder];
    }
    return printingFolder;
}
+(NSString *)logFolder{
    NSString *printingFolder=[[self rootFolder] stringByAppendingPathComponent:@"Log"];
    if (![self folderExist:printingFolder]) {
        [self createFolder:printingFolder];
    }
    return printingFolder;
}
+(NSString*)createFolderWithName:(NSString *)childFolder toFolder:(NSString *)parentFolder{
    NSString * pathNewFolder = [parentFolder stringByAppendingPathComponent:childFolder];
    NSLog(@"F%@",pathNewFolder);
    NSFileManager * fileManager =[NSFileManager defaultManager];
    NSError * error =nil;
    
    if (![fileManager createDirectoryAtPath:pathNewFolder withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"Not Create");
        return nil;
    }
    return pathNewFolder;
}
+(NSString *)createFolder:(NSString *)fullPath{
    NSFileManager * fileManager =[NSFileManager defaultManager];
    NSError * error =nil;
    
    if (![fileManager createDirectoryAtPath:fullPath withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSLog(@"Not Create");
        return nil;
    }
    return fullPath;
}
+(BOOL)folderExist:(NSString*)pathFile{
    BOOL isDirectory;
    NSFileManager * fm = [NSFileManager defaultManager ];
    [fm fileExistsAtPath:pathFile isDirectory:&isDirectory];
    return isDirectory;
}
+(BOOL)fileExist:(NSString *)fullPath{
    NSFileManager * fm = [NSFileManager defaultManager ];
    return [fm fileExistsAtPath:fullPath];
}

+(BOOL)saveData:(NSData *)data toFolder:(NSString *)folder withName:(NSString*)name{
    return [data writeToFile:[folder stringByAppendingPathComponent:name] atomically:YES];
}
+(NSData*)getDataWithFullFile:(NSString *)fullFile{
    return [NSData dataWithContentsOfFile:fullFile];
}
+(NSData *)getdataWithFileName:(NSString *)fileName fromFolder:(NSString *)folder{
    return [NSData dataWithContentsOfFile:[folder stringByAppendingPathComponent:fileName]];
}
@end
