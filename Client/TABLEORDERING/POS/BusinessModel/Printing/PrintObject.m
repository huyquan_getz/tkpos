//
//  PrintObject.m
//  POS
//
//  Created by Hoang Van Quynh on 13/01/2015.
//  Copyright (c) Năm 2015 Nha Duong Cong. All rights reserved.
//
#import "PrintObject.h"

#import "ePOS-Print.h"
#import "PrintData.h"
#import "PrintObjectData.h"
#import "PrinterController.h"
#import "SNLog.h"
#define SEND_TIMEOUT     10 * 1000
#define IMAGE_WIDTH_MAX  512
#define kReachabilityChangedNotification @"kNetworkReachabilityChangedNotification"

@implementation PrintObject{
    EposBuilder *builder;
    EposPrint *printer;
    BOOL openPrint;
    BOOL openCash;
    BOOL showAler;
    int count;
}
@synthesize printObjectData;
@synthesize data;
-(id)initWithPrintObjectData:(PrintObjectData*)printObjectData_{
    self = [super init];
    if (self) {
        data=[[NSMutableArray alloc]init];
        printObjectData=printObjectData_;
        builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-T88V" Lang:EPOS_OC_MODEL_ANK];
        printer = [[EposPrint alloc] init];
        count = 0;
    }
    return self;
}
- (void)removeImage:(NSString *)fileName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    [fileManager removeItemAtPath:fileName error:&error];
}
- (BOOL)checkPrinterOnline {
    
    int result ;
    BOOL isOnline = NO;
    [builder clearCommandBuffer];
    [printer closePrinter];
    result= [printer openPrinter:EPOS_OC_DEVTYPE_TCP DeviceName:printObjectData.ip Enabled:EPOS_OC_TRUE Interval:EPOS_OC_PARAM_DEFAULT];
    if(result==EPOS_OC_SUCCESS){
         [printer closePrinter];
        isOnline = YES;
    }else{
        isOnline = NO;
    }
    
    return isOnline;
}
- (void)addPrintData:(PrintData*)printData{
    [data addObject:printData];
    NSLog(@"count=%lu",data.count);
    if(!openPrint){
        openPrint=YES;
        [self processingPrint];
    }
    
}

- (BOOL)printWithImage:(UIImage*)_img withIP:(NSString*)_ip width:(long)_width height:(long)_height{
    [printer setPaperEndEventCallback:@selector(onPaperEnd:) Target:self];
    [printer setCoverOpenEventCallback:@selector(onCoverOpen:) Target:self];
    int result = [builder addImage:_img X:0 Y:0 Width:_img.size.width Height:_img.size.height Color:EPOS_OC_COLOR_1 Mode:EPOS_OC_MODE_MONO Halftone:EPOS_OC_HALFTONE_DITHER Brightness:1.0];
    NSLog(@"addImage %d",result);
    if(result != EPOS_OC_SUCCESS){
        return NO;
    }
    [builder addCut:EPOS_OC_CUT_FEED];
    if(result != EPOS_OC_SUCCESS){
        return NO;
    }
    unsigned long status = 0;
    unsigned long battery = 0;
    result = [printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery];
    if (result == EPOS_OC_ERR_MEMORY) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"EPOS_OC_ERR_MEMORY" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alertView show];
    }
    if(result != EPOS_OC_SUCCESS){
        return NO;
    }
    [builder clearCommandBuffer];
    return YES;
}
- (void)onPaperEnd:(NSString *)deviceName
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Printer (%@) status indicates there is no paper.",printObjectData.name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    });
}

- (void)onCoverOpen:(NSString *)deviceName
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"The cover of printer (%@) has been opened.Please check it.",printObjectData.name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    });
    
}
- (void)onStatusChange:(NSString *)deviceName Status:(NSNumber *)status {
    [SNLog Log:[NSString stringWithFormat:@"deviceName %@ status:%@",deviceName,status]];
}
- (BOOL)openDrawer{
    [data addObject:@""];
    if(!openPrint){
        openPrint=YES;
        [self processingPrint];
    }
    return YES;
}
-(void)closePrinter {
    [printer closePrinter];
    [builder clearCommandBuffer];
}
- (void)processingPrint{
    char test[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    int r = arc4random() % 26;
    char name=test[r];
    dispatch_queue_t myQueue = dispatch_queue_create(&name, NULL);
    dispatch_async(myQueue,^(void) {
        int result ;
        result= [printer openPrinter:EPOS_OC_DEVTYPE_TCP DeviceName:printObjectData.ip Enabled:EPOS_OC_TRUE Interval:EPOS_OC_PARAM_DEFAULT];
        if(result==EPOS_OC_SUCCESS){
            [printer setStatusChangeEventCallback:@selector(onStatusChange:Status:) Target:self];
            for(int i=0;i<[data count];i++){
                PrintData *printData=data[i];
                if([printData isEqual:@""])
                {
                    [data removeObjectAtIndex:i];
                    i--;
                    [builder addPulse:EPOS_OC_DRAWER_1 Time:EPOS_OC_PULSE_100];
                    unsigned long status = 0;
                    unsigned long battery = 0;
                    int result= [printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery];
                    if(result != EPOS_OC_SUCCESS){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertView *dialog = [[UIAlertView alloc] initWithTitle:@"Setup Cash Drawer" message:@"Cannot connect to your cash drawer. Please check again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
                            [dialog show];
                        });
                    }
                    [builder clearCommandBuffer];
                    openCash=TRUE;
                }
                else
                    if(![printData.status boolValue]){
                        UIImage *imagePrint=[UIImage imageWithContentsOfFile:printData.strImage];
                        if(imagePrint==nil){
                            printData.status=@"1";
                            [[PrinterController sharedInstance]updatePrintData:printData atIndex:[printData.index intValue]];
                            [data removeObjectAtIndex:i];
                            [self removeImage:printData.strImage];
                            i--;
                            
                        }
                        else
                            if([self printWithImage:imagePrint withIP:printObjectData.ip width:510 height:[printData.height doubleValue]]){
                                printData.status=@"1";
                                [[PrinterController sharedInstance]updatePrintData:printData atIndex:[printData.index intValue]];
                                [data removeObjectAtIndex:i];
                                [self removeImage:printData.strImage];
                                i--;
                                
                            }
                            else{
                                [builder clearCommandBuffer];
                                [printer closePrinter];
                                openPrint=NO;
                                NSString *name= printObjectData.name;

                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                    
                                    if(!showAler){
                                        
                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Can not connect to printer %@ (%@). Please check the printer connection and try again.",name,printObjectData.ip] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again", nil];
                                        [alertView show];
                                        showAler=true;
                                        count++;
                                        
                                    }
                                    /*
                                     nha.duong comment
                                    if (count > 1) {
                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Please check your network connection."] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                        [alertView show];
                                        count = 0;
                                    }
                                     */
                                });
                                break;
                            }
                    }
            }
        }
        else{
            [SNLog Log:[NSString stringWithFormat:@"open printer %d",result]];
            NSString *name=printObjectData.name;
            openPrint=NO;
            [builder clearCommandBuffer];
            [printer closePrinter];
            if (result == EPOS_OC_ERR_MEMORY) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"EPOS_OC_ERR_MEMORY" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
                [alertView show];
                result = [printer openPrinter:EPOS_OC_DEVTYPE_TCP DeviceName:printObjectData.ip Enabled:EPOS_OC_TRUE Interval:EPOS_OC_PARAM_DEFAULT];
            }else
                if(result != EPOS_OC_SUCCESS ){
                    [SNLog Log:[NSString stringWithFormat:@"open printer %d",result]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if(!showAler){
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Can not connect to printer %@ (%@). Please check the printer connection and try again.",name,printObjectData.ip] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again", nil];
                            [alertView show];
                            count++;
                            showAler=true;
                            if (count > 1) {
                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Please check your network connection."] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alertView show];
                                count = 0;
                            }
                        }
                    });
                    
                }
        }
        if (data.count==0) {
            [builder clearCommandBuffer];
            [printer closePrinter];
            openPrint=NO;
            [[PrinterController sharedInstance]handlePrint];
        }
        openCash=FALSE;
    });
}

- (void)onPowerOff:(NSString *)deviceName
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"There is no response concerning printer status." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again",nil];
        [alertView show];
        showAler=true;
    });
}
- (void)onOnline:(NSString *)deviceName
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Printer status is online." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
        [alertView show];
    });
}
- (void)onCoverOk:(NSString *)deviceName
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!showAler){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Printer status indicates cover close." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again",nil];
            [alertView show];
            showAler=true;
        }
    });
}
- (void)onPaperOk:(NSString *)deviceName
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(!showAler){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Printer status indicates paper ok." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again",nil];
            [alertView show];
            showAler=true;
        }
    });
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    showAler=false;
    [builder clearCommandBuffer];
    [printer closePrinter];
    openPrint=NO;
    openCash=FALSE;
    if (buttonIndex == 1) {
        [self processingPrint];
    }else{
        // [data removeAllObjects];
        [[PrinterController sharedInstance]handlePrint];
    }
}
@end

