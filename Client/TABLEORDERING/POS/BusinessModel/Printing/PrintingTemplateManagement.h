//
//  PrintingTemplateManagement.h
//  POS
//
//  Created by Cong Nha Duong on 1/15/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface PrintingTemplateManagement : NSObject
+(instancetype)sharedObject;
-(UIImage *)printingImageWithReceipt:(CBLDocument *)receipt receiptCopy:(BOOL)receiptCopy reprint:(BOOL)reprint;
-(UIImage *)printingSingleOrderChit:(NSArray*)listProductOrder listAdhoc:(NSArray*)listAdhoc withOrderInfo:(NSDictionary*)info totalOder:(BOOL)totalOrder;
-(void)printReceipt:(CBLDocument*)receipt rePrint:(BOOL)isReprint;
@end
