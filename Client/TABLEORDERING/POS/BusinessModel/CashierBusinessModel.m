//
//  CashierBusinessModel.m
//  POS
//
//  Created by Nha Duong Cong on 10/20/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "CashierBusinessModel.h"
#import "PaymentChild.h"
#import "QueueSendMail.h"
#import "Controller.h"
#import "TKEmployee.h"
#import "PaymentBusinessModel.h"


@implementation CashierBusinessModel
@synthesize amountDisplay;
@synthesize listTax;
@synthesize discount;
@synthesize serviceCharge;
-(instancetype)initWithDiscountTotal:(TKDiscount *)discount_ listTax:(NSArray *)listTax_ serviceCharge:(TKTaxServiceCharge *)serviceCharge_ currency:(NSString *)currency_{
    if (self =[super init]) {
        listProductOrder=[[NSMutableArray alloc] init];
        listAdhocProduct=[[NSMutableArray alloc] init];
        amountDisplay=[[TKAmountDisplay alloc] initWithCurrency:currency_];
        listTax=listTax_;
        serviceCharge=serviceCharge_;
        discount=discount_;
    }
    return self;
}
-(instancetype)initServiceDefault{
    listProductOrder=[[NSMutableArray alloc] init];
    listAdhocProduct=[[NSMutableArray alloc] init];
    NSArray *taxes=[TKTaxServiceCharge listTax:[Controller getTaxServiceSetting] forStore:[Controller storeIndexDefault]];
    TKTaxServiceCharge *svCharge=[TKTaxServiceCharge serviceCharge:[Controller getTaxServiceSetting] forStore:[Controller storeIndexDefault]];
    NSString *curcy=[Controller currencyDefault];
    if (self=[self initWithDiscountTotal:nil listTax:taxes serviceCharge:svCharge currency:curcy]) {
        
    }
    return self;
}
-(void)resetListProductOrder:(NSArray *)listProductOrder_{
    [listProductOrder removeAllObjects];
    [listProductOrder addObjectsFromArray:listProductOrder_];
}
-(void)resetListAdhocProduct:(NSArray *)listAdhocProduct_{
    [listAdhocProduct removeAllObjects];
    [listAdhocProduct addObjectsFromArray:listAdhocProduct_];
}
-(void)setDiscountTotal:(TKDiscount *)discount_{
    discount=discount_;
}
-(double)subTotalWithNoCartDiscountValue{
    double totalAmountHaveDiscount=0.0;
    for (ProductOrderSingle *proOrder in listProductOrder) {
        totalAmountHaveDiscount+=[self totalAmountHaveDiscountOfProductOrder:proOrder];
    }
    for (AdhocProduct *adhoc in listAdhocProduct) {
        totalAmountHaveDiscount+=[self totalAmountHaveDiscountOfAdhocProduct:adhoc];
    }
    return totalAmountHaveDiscount;
}
-(double)subTotal{
    double totalAmountHaveDiscount=[self subTotalWithNoCartDiscountValue];
    if (discount && discount.type==DiscountTypeValue) {
        totalAmountHaveDiscount -= [CashierBusinessModel discountAmount:totalAmountHaveDiscount withDiscount:discount];
    }
    double totalCharge =totalAmountHaveDiscount;
    return totalCharge;
}
-(double)totalCartDiscountValue{
    if (discount && discount.type==DiscountTypeValue) {
        double totalAmountHaveDiscount=[self subTotalWithNoCartDiscountValue];
        return [CashierBusinessModel discountAmount:totalAmountHaveDiscount withDiscount:discount];
    }
    return 0.0;
}
-(double)subTotalHaveServiceCharge{
    double subTotal=[self subTotal];
    return subTotal + [self totalServiceCharges];
}
-(double)total{
    double totalAmountHaveDiscount=[self subTotalHaveServiceCharge];
    double totalCharge =totalAmountHaveDiscount+[self totalTax];
    return [TKAmountDisplay tkRoundCash:totalCharge];
}
-(double)grandTotal{
    double total=[self total];
    return [TKAmountDisplay tkRoundCashTotal:total];
}
-(double)roundAmount{
    double total=[self total];
    double totalRoundTotal =[TKAmountDisplay tkRoundCashTotal:total];
    return [TKAmountDisplay tkRoundCash:(totalRoundTotal - total)];
}
-(double)totalDiscount{
    double totalDiscount=0;
    for (ProductOrderSingle *pr in listProductOrder) {
        totalDiscount += [self totalDiscountOfProductOrder:pr];
    }
    for (AdhocProduct *adhoc in listAdhocProduct) {
        totalDiscount +=[self totalDiscountOfAdhocProduct:adhoc];
    }
    totalDiscount+=[self totalCartDiscountValue];
    return [TKAmountDisplay tkRoundCash:totalDiscount];
}
-(double)totalTax{
    double totalTax=0.0;
    double subTotalHaveServiceCharge=[self subTotalHaveServiceCharge];
    for (TKTaxServiceCharge *tax in listTax) {
        totalTax+=[CashierBusinessModel taxServiceAmount:subTotalHaveServiceCharge withTaxService:tax];
    }
    return [TKAmountDisplay tkRoundCash:totalTax];
}
-(double)totalServiceCharges{
    double subTotal=[self subTotal];
    if (serviceCharge) {
        return [CashierBusinessModel taxServiceAmount:subTotal withTaxService:serviceCharge];
    }else{
        return 0.0;
    }
}

-(double)totalAmountCrudeOfProductOrder:(ProductOrderSingle*)productOrder{
    return [CashierBusinessModel totalAmountCrudeOfProductOrder:productOrder];
}
-(double)totalDiscountOfProductOrder:(ProductOrderSingle *)productOrder{
    TKDiscount *discountItem=[productOrder discount];
    double totalDiscount=0;
    if (discountItem==nil && discount && discount.type==DiscountTypePercent) {
        discountItem=discount;
    }
    if (discountItem) {
        if (discountItem.type==DiscountTypePercent) {
            totalDiscount=[CashierBusinessModel discountAmount:productOrder.quantity*[productOrder price] withDiscount:discountItem];
        }else{
            totalDiscount= ([CashierBusinessModel discountAmount:[productOrder price] withDiscount:discountItem] * productOrder.quantity);
        }
        return [TKAmountDisplay tkRoundCash:totalDiscount];
    }else{
        return 0.0;
    }
}
-(double)totalAmountHaveDiscountOfProductOrder:(ProductOrderSingle*)productOrder{
    return [self totalAmountCrudeOfProductOrder:productOrder] -[self totalDiscountOfProductOrder:productOrder];
}
-(double)totalAmountCrudeOfAdhocProduct:(AdhocProduct *)adhocProduct{
    return [CashierBusinessModel totalAmountCrudeOfAdhocProduct:adhocProduct];
}
-(double)totalDiscountOfAdhocProduct:(AdhocProduct *)adhocProduct{
    TKDiscount *discountItem=adhocProduct.discountProductItem;
    double totalDiscount=0;
    if (discountItem==nil && discount && discount.type==DiscountTypePercent && adhocProduct.applyDiscount) {
        discountItem=discount;
    }
    if (discountItem) {
        if (discountItem.type==DiscountTypePercent) {
            totalDiscount=[CashierBusinessModel discountAmount:adhocProduct.quantity*adhocProduct.price withDiscount:discountItem];
        }else{
            totalDiscount= ([CashierBusinessModel discountAmount:adhocProduct.price withDiscount:discountItem] * adhocProduct.quantity);
        }
        return [TKAmountDisplay tkRoundCash:totalDiscount];
    }else{
        return 0.0;
    }
}
-(double)totalAmountHaveDiscountOfAdhocProduct:(AdhocProduct *)adhocProduct{
    return [self totalAmountCrudeOfAdhocProduct:adhocProduct] -[self totalDiscountOfAdhocProduct:adhocProduct];
}

-(CBLDocument *)createNewOrder{
    NSDate *createdDate=[NSDate date];
    NSMutableDictionary *infor =[[NSMutableDictionary alloc] initWithDictionary:[TKItem infomationBaseWithTable:tkOrderTable]];
    [infor setObject:@(OrderStatusNone) forKey:tkKeyOrderStatus];
    [infor setObject:[CashierBusinessModel orderCodeWithDate:createdDate] forKey:tkKeyOrderCode];
    return [[SyncManager sharedSyncManager] createDocumentLocalOnly:infor];
}




/// PUBLIC
+(double)totalAmountCrudeOfProductOrder:(ProductOrderSingle*)productOrder{
    NSInteger quantity =productOrder.quantity;
    double amount =[productOrder price];
    return quantity*amount;
}
+(double)totalDiscountOfProductOrder:(ProductOrderSingle *)productOrder{
    TKDiscount *discount=[productOrder discount];
    double totalDiscount=0;
    if (discount) {
        if (discount.type==DiscountTypePercent) {
            totalDiscount=[CashierBusinessModel discountAmount:productOrder.quantity*[productOrder price] withDiscount:discount];
        }else{
            totalDiscount= ([CashierBusinessModel discountAmount:[productOrder price] withDiscount:discount] * productOrder.quantity);
        }
        return [TKAmountDisplay tkRoundCash:totalDiscount];
    }else{
        return 0.0;
    }
}
+(double)totalAmountHaveDiscountOfProductOrder:(ProductOrderSingle*)productOrder{
    return [self totalAmountCrudeOfProductOrder:productOrder] -[self totalDiscountOfProductOrder:productOrder];
}
+(double)totalAmountCrudeOfAdhocProduct:(AdhocProduct *)adhocProduct{
    return adhocProduct.price * adhocProduct.quantity;
}
+(double)totalDiscountOfAdhocProduct:(AdhocProduct *)adhocProduct{
    TKDiscount *discount=adhocProduct.discountProductItem;
    double totalDiscount=0;
    if (discount) {
        if (discount.type==DiscountTypePercent) {
            totalDiscount=[CashierBusinessModel discountAmount:adhocProduct.quantity*adhocProduct.price withDiscount:discount];
        }else{
            totalDiscount= ([CashierBusinessModel discountAmount:adhocProduct.price withDiscount:discount] * adhocProduct.quantity);
        }
        return [TKAmountDisplay tkRoundCash:totalDiscount];
    }else{
        return 0.0;
    }
}
+(double)totalAmountHaveDiscountOfAdhocProduct:(AdhocProduct *)adhocProduct{
    return [self totalAmountCrudeOfAdhocProduct:adhocProduct] -[self totalDiscountOfAdhocProduct:adhocProduct];
}
+(NSString*)titleVariantOfProductOrder:(ProductOrderSingle*)productOrder{
    NSString *varianName=[productOrder variantName];
    if (varianName.length==0) {
        return [productOrder productName];
    }else{
        return [NSString stringWithFormat:@"%@ (%@)",productOrder.productName,varianName];
    }
}
+(NSString *)orderCodeWithDate:(NSDate *)date{
    return [NSString stringWithFormat:@"%li-%@",(long)[Controller storeIndexDefault],[date getStringWithFormat:@"ddMMyyyyHHmmss"]];
}
+(NSDictionary*)mergeProductOrderSameProduct:(NSArray*)productOrderSameProduct{
    if (productOrderSameProduct.count==0) {
        return nil;
    }
    NSMutableDictionary *newDict=[[NSMutableDictionary alloc] init];
    ProductOrderSingle *firstProductOrder=[productOrderSameProduct firstObject];
    [newDict setObject:@"ProductOrder" forKey:tkKeyTable];
    NSDictionary *productShort=@{tkKeyId:firstProductOrder.productProperies[tkKeyId],
                                 tkKeyTitle:firstProductOrder.productProperies[tkKeyTitle],
                                 tkKeySKU:firstProductOrder.productProperies[tkKeySKU],
                                 tkKeyArrayInventory:firstProductOrder.productProperies[tkKeyArrayInventory],
                                 tkKeyCategoryId:firstProductOrder.productProperies[tkKeyCategoryId],
                                 tkKeyDiscount:firstProductOrder.productProperies[tkKeyDiscount],
                                 tkKeyDiscountProductItem:firstProductOrder.productProperies[tkKeyDiscountProductItem]};
    [newDict addEntriesFromDictionary:productShort];
    NSMutableArray *arrayVariantOrder=[[NSMutableArray alloc] init];
    for (ProductOrderSingle *proOrder in productOrderSameProduct) {
        NSMutableDictionary *variantCopyHaveQuantity =[proOrder.variantOrder mutableCopy];
        // set quantity in variant is quantity order
        [variantCopyHaveQuantity setObject:@(proOrder.quantity) forKey:tkKeyQuantity];
        [arrayVariantOrder addObject:variantCopyHaveQuantity];
    }
    [newDict setObject:arrayVariantOrder forKey:@"arrayVariantOrder"];
    return newDict;
}
+(NSArray *)arrayProductOrderWithProductOrdersJSON:(NSArray *)listProductOrderJSON{
    // split product order whith same product
    NSMutableArray *arrayProductOrder=[[NSMutableArray alloc] init];
    for (NSDictionary *productOrderJSON in listProductOrderJSON) {
        NSMutableDictionary *productProperties =[productOrderJSON mutableCopy];
        [productProperties removeObjectForKey:@"arrayVariantOrder"];
        for (NSDictionary *variant in productOrderJSON[@"arrayVariantOrder"]) {
            NSMutableDictionary *newVariant =[variant mutableCopy];
            
            NSMutableArray *variantOptions=[[NSMutableArray alloc] init];
            for (NSDictionary *dictOption in [productProperties[tkKeyArrayInventory] convertNullToNil]) {
                NSString *optionKey = dictOption[@"name"];
                [variantOptions addObject:@{@"optionKey":optionKey,@"optionValue":newVariant[optionKey]}];
            }
            
            ProductOrderSingle *productOrder=[[ProductOrderSingle alloc] initWithProductProperties:productProperties variantOrder:newVariant variantOptions:variantOptions quantity:[variant[tkKeyQuantity] integerValue]];
            [arrayProductOrder addObject:productOrder];
        }
    }
    return arrayProductOrder;
}
+(NSArray *)arrayAdhocProductWithAdhocProductJSON:(NSArray *)listAdhocProductJSON{
    NSMutableArray *adhocProducts=[[NSMutableArray alloc] init];
    for (NSDictionary *json in listAdhocProductJSON) {
        [adhocProducts addObject:[[AdhocProduct alloc] initWithJSON:json]];
    }
    return adhocProducts;
}
+(NSArray *)arrayAdhocProductJSONWithAdhocProduct:(NSArray *)listAdhocProduct{
    NSMutableArray *adhocProductJSON=[[NSMutableArray alloc] init];
    for (AdhocProduct *adhoc in listAdhocProduct) {
        [adhocProductJSON addObject:[adhoc encodeAsJSON]];
    }
    return adhocProductJSON;
}
+(double)discountAmount:(double)amount withDiscount:(TKDiscount*)discount{
    double discountAmount=0;
    switch (discount.type) {
        case DiscountTypePercent:
            discountAmount = amount * discount.discountValue/100.0;
            break;
        case DiscountTypeValue:
            discountAmount=discount.discountValue;
            break;
        default:
            break;
    }
    if (discountAmount > amount) {
        discountAmount=amount;
    }
    return [TKAmountDisplay tkRoundCash:discountAmount];
}
+(double)taxServiceAmount:(double)amount withTaxService:(TKTaxServiceCharge *)taxService{
    double discountAmount=0;
    switch (taxService.type) {
        case TaxServiceTypePercent:
            discountAmount = amount * taxService.value/100;
            break;
        case DiscountTypeValue:
            discountAmount=taxService.value;
            break;
        default:
            break;
    }
    if (discountAmount > amount) {
        discountAmount=amount;
    }
    return [TKAmountDisplay tkRoundCash:discountAmount];
}
+(NSDictionary *)taxJSONWithTotalAmount:(double)amount withTax:(TKTaxServiceCharge *)tax{
    double taxAmount=[self taxServiceAmount:amount withTaxService:tax];
    return @{@"taxValue":@(taxAmount),@"taxName":tax.name,@"percent":@(tax.value)};
}
+(NSArray *)listTaxesJSONWithTotalAmount:(double)amount withListTaxes:(NSArray *)listTaxes{
    NSMutableArray *listTaxesJSON=[[NSMutableArray alloc] init];
    for (TKTaxServiceCharge *tax in listTaxes) {
        [listTaxesJSON addObject:[self taxJSONWithTotalAmount:amount withTax:tax]];
    }
    return listTaxesJSON;
}
+(NSDictionary *)serviceChargeJSONWithTotalAmount:(double)amount withServiceCharge:(TKTaxServiceCharge *)serviceCharge{
    double svChargeAmount=[self taxServiceAmount:amount withTaxService:serviceCharge];
    return @{@"serviceChargeValue":@(svChargeAmount),@"serviceChargeName":serviceCharge.name,@"percent":@(serviceCharge.value)};
}

@end
