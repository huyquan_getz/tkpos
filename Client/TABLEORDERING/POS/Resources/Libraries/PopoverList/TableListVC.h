//
//  TableListVC.h
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol TableListDelegate <NSObject>
@required
-(void)tableList:(id)sender selectedCellIndex:(NSInteger)cellIndex;

@end
#import <UIKit/UIKit.h>

@interface TableListVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSArray *listString;
    NSString* messageNoList;
}
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak,nonatomic)  id<TableListDelegate> delegate;
@property (assign) NSInteger heighCell;
@property (strong,nonatomic) UIColor *colorLine;
@property (strong,nonatomic) UIColor *colorText;
@property (strong,nonatomic) UIFont *fontText;
@property (assign) NSInteger heightCell;
@property (weak, nonatomic) IBOutlet UILabel *lbNoList;
-(instancetype)initWithListString:(NSArray*)listString_ messageNoList:(NSString*)messageNoList_;
@end
