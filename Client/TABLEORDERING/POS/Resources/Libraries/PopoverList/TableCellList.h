//
//  TableCellList.h
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellList : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIView *vLine;
-(void)setValueDefault;
@end
