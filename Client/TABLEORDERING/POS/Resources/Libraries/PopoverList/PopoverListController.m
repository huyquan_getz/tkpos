//
//  PopoverListController.m
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define MaxWith 400
#define MaxHeight 600
#import "PopoverListController.h"
#import "TableListVC.h"

@implementation PopoverListController
-(instancetype)initWithListString:(NSArray*)listString_ messageEmptyList:(NSString*)emptyMessage_{
    listString=[[NSArray alloc] initWithArray:listString_];
    tableList=[[TableListVC alloc] initWithListString:listString messageNoList:emptyMessage_];
    tableList.delegate=self;
    if (self =[super initWithContentViewController:tableList]) {
        heightCell=50;
    }
    return self;
}
//@override
-(void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated{
    tableList.fontText=_fontText;
    tableList.colorLine=_colorLine;
    tableList.colorText=_colorText;
    tableList.heighCell=heightCell;
    tableList.delegate=self;
    [tableList.tbvMain reloadData];
    tableList.tbvMain.contentOffset=CGPointZero;
    [self setPopoverContentSize:[self maxSizeView]];
    [super presentPopoverFromRect:rect inView:view permittedArrowDirections:arrowDirections animated:animated];
}

#pragma TableListDelegate
-(void)tableList:(id)sender selectedCellIndex:(NSInteger)cellIndex{
    if (_listDelegate) {
        [_listDelegate popoverList:self dismissWithCellIndex:cellIndex valude:listString[cellIndex]];
    }
    [self dismissPopoverAnimated:NO];
}
-(CGSize)maxSizeView{
    if (listString.count==0) {
        return CGSizeMake(300, 50);
    }else{
        NSInteger widthMax=0;
        if (_fontText==nil) {
            _fontText=[UIFont systemFontOfSize:17];
        }
        for (NSString *test in listString) {
            CGSize size = [test sizeWithAttributes:@{NSFontAttributeName: _fontText}];
            if (size.width>widthMax) {
                widthMax=size.width;
            }
        }
        NSInteger maxHeight =listString.count * heightCell;
        if (maxHeight>MaxHeight) {
            maxHeight=MaxHeight;
        }
        return CGSizeMake(widthMax+40, maxHeight+16);
    }
}
@end
