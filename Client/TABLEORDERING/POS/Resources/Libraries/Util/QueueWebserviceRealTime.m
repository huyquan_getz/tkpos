//
//  QueueWebserviceRealTime.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "QueueWebserviceRealTime.h"
#import "File.h"
#import "Controller.h"

@implementation QueueWebserviceRealTime{
    NSOperationQueue *queueSend;
    NSInteger numberOperationing;
}

+(QueueWebserviceRealTime *)sharedQueue{
    __strong static QueueWebserviceRealTime *_queueWebserviceRealTime;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _queueWebserviceRealTime =[[QueueWebserviceRealTime alloc] init];
    });
    return _queueWebserviceRealTime;
}
-(instancetype)init{
    if (self =[super init]) {
        filePathWatingRealTime=[[File rootFolder] stringByAppendingPathComponent:@"wsrealtime.bin"];
        queueSend =[NSOperationQueue new];
        [queueSend setMaxConcurrentOperationCount:1];
        numberOperationing=0;
    }
    return self;
}
-(void)addDataToQueue:(DataWebsevice *)dataWebservice{
    [self addDataWebservice:dataWebservice addToStore:YES];
}
-(void)addDataWebservice:(DataWebsevice *)dataWS addToStore:(BOOL)addToStore{
    if (addToStore) {
        [self addDataWStoStore:dataWS];
    }
    if (![[NetworkUtil sharedInternetConnection] checkInternetConnection]){
        return;
    }
    NSInvocationOperation *operation=[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(callWebservice:) object:dataWS];
    [queueSend addOperation:operation];
    numberOperationing++;
}
-(void)callWebservice:(DataWebsevice*)dataWS{
    if ([[NetworkUtil sharedInternetConnection] checkInternetConnection]) {
        // send email
        NSInteger returnCodeSend=[Controller webserviceRealTimeWithURL:dataWS.url parameter:dataWS.data];
        [dataWS increaseResendCount];
        if (returnCodeSend ==ReturnCodeRequestError && [[NetworkUtil sharedInternetConnection] checkInternetConnection]) { // resend again when internet bad
            returnCodeSend=[Controller webserviceRealTimeWithURL:dataWS.url parameter:dataWS.data];
        }
        
        if (returnCodeSend == ReturnCodeSuccess || returnCodeSend==ReturnCodeSuccessNotUpdate || dataWS.checkExpired) {
            [self removeDataWSFromStore:dataWS];
            
            if (returnCodeSend==ReturnCodeSuccess) {
                [SNLog Log:@"ws sent(key:%@ resent:%d)",dataWS.keyWS,dataWS.reSendCount];
            }else if(returnCodeSend== ReturnCodeSuccessNotUpdate){
                [SNLog Log:@"ws sent not update(key:%@ resent:%d)",dataWS.keyWS,dataWS.reSendCount];
            }else{
                [SNLog Log:@"ws exired (key:%@ resent:%d)",dataWS.keyWS,dataWS.reSendCount];
            }
        }else{
            [self addDataWStoStore:dataWS];
            [SNLog Log:@"webservice into waiting (key:%@ resent:%d)",dataWS.keyWS,dataWS.reSendCount];
        }
        
    }
    numberOperationing--;
    if (numberOperationing<=0) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSArray *array=[self getDataWSsInQueue];
            if (array.count>0) {
                [self addDataWebservice:[array firstObject] addToStore:NO];
            }
        });
    }
}
-(void)addDataWStoStore:(DataWebsevice*)dataWS{
    NSMutableArray *dataWSsInQueue=[self getDataWSsInQueue];
    BOOL existed=NO;
    for (int i=0;i<dataWSsInQueue.count;i++) {
        DataWebsevice *dataW=dataWSsInQueue[i];
        if ([dataWS sameData:dataW]) { // email existed
            existed=YES;
            if ([dataWS.createdDate compare:dataW.createdDate]!=NSOrderedAscending) { // check reversion as time 
                [dataWSsInQueue replaceObjectAtIndex:i withObject:dataWS];
            }
            break;
        }
    }
    if (!existed) {
        [dataWSsInQueue addObject:dataWS];
    }
    [self saveDataWSs:dataWSsInQueue];
}
-(void)removeDataWSFromStore:(DataWebsevice*)dataWS{
    NSMutableArray *dataWSsInQueue=[self getDataWSsInQueue];
    for (DataWebsevice *dataW in dataWSsInQueue) {
        if ([dataWS isEqualToDataWS:dataW]) {
            [dataWSsInQueue removeObject:dataW];
            [self saveDataWSs:dataWSsInQueue];
            break;
        }
    }
}
-(NSMutableArray*)getDataWSsInQueue{
    NSFileManager *fm =[NSFileManager defaultManager];
    if (![fm fileExistsAtPath:filePathWatingRealTime]) {
        return [[NSMutableArray alloc] init];
    }else{
        return [NSKeyedUnarchiver unarchiveObjectWithFile:filePathWatingRealTime];
    }
}

-(BOOL)saveDataWSs:(NSMutableArray *)dataWSs{
    return [NSKeyedArchiver archiveRootObject:dataWSs toFile:filePathWatingRealTime];
}
-(void)cancelAllOperationWebserviceRealTime{
    [queueSend cancelAllOperations];
    numberOperationing=0;
}
-(void)reCallUpdating{
    if (numberOperationing<=0) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSArray *array=[self getDataWSsInQueue];
            for (DataWebsevice *dataWS in array) {
                [self addDataWebservice:dataWS addToStore:NO];
            }
        });
    }
}
//network util
-(void)networkBackgroundNotiConnection:(BOOL)connectionOn{
    if (connectionOn) {
        [self reCallUpdating];
    }
}
@end
