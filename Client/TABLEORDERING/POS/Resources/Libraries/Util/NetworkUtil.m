//
//  NetworkUtil.m
//  POS
//
//  Created by Nha Duong Cong on 10/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "NetworkUtil.h"
#include "UIAlertView+Blocks.h"
#import "QueueSendMail.h"
@implementation NetworkUtil
+(NetworkUtil*)sharedInternetConnection{
    __strong static NetworkUtil *_networkUtil=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _networkUtil=[[NetworkUtil alloc] initInternetConnection];
        [_networkUtil start];
    });
    return _networkUtil;
}
-(id)initInternetConnection{
    if (self=[super init]) {
        reachability=[Reachability reachabilityForInternetConnection];
        listDelegates=[[NSMutableArray alloc] init];
    }
    return self;
}
-(void)start{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    [reachability startNotifier];
}
- (void) reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
	[self updateInterfaceWithReachability:curReach];
}
- (void)updateInterfaceWithReachability:(Reachability *)reachability_{
    if (reachability == reachability_) {
        [SNLog Log:@"Reachability connection %@",(([self checkInternetConnection])?@"HAVE":@"LOST")];
        [self notiToDelegate];
        if ( _allowNotiWhenChangeStatusInternet) {
            NSString *message=nil;
            if ([self checkInternetConnection]) {
                message=[[LanguageUtil sharedLanguageUtil] stringByKey:@"general.ms-connection-have"];
            }else{
                message=[[LanguageUtil sharedLanguageUtil] stringByKey:@"general.ms-connection-lost"];
            }
            [UIAlertView showMessage:message];
        }
    }
}
-(BOOL)checkInternetConnection{
    return [reachability currentReachabilityStatus] != NotReachable;
}
-(void)addDelegate:(id<NetworkDelegate>)newDelegate{
    [listDelegates removeObject:newDelegate];
    [listDelegates addObject:newDelegate];
}
-(void)removeDelegate:(id)delegateDelete{
    [listDelegates removeObject:delegateDelete];
}
-(void)notiToDelegate{
    for (id<NetworkDelegate> delegate in listDelegates) {
        int random=arc4random();
        NSString *stringQueueName= [NSString stringWithFormat:@"queue.network.%d",random];
        dispatch_queue_t myQueue =dispatch_queue_create([stringQueueName UTF8String], NULL);
        dispatch_async(myQueue, ^{
            [delegate networkBackgroundNotiConnection:[self checkInternetConnection]];
        });
    }
}
+(BOOL)checkHostConnection:(NSString *)host{
    Reachability *hostConnection=[Reachability reachabilityWithHostName:host];
    NetworkStatus status=[hostConnection currentReachabilityStatus];
    if (status==NotReachable) {
        return NO;
    }
    return YES;
}
+(BOOL)checkInternetConnection{
    Reachability *internetConnection=[Reachability reachabilityForInternetConnection];
    NetworkStatus status=[internetConnection currentReachabilityStatus];
    if (status==NotReachable) {
        return NO;
    }
    return YES;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}
@end
