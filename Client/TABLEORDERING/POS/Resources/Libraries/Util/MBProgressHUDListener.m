//
//  MBProgressHUDListener.m
//  POS
//
//  Created by Nha Duong Cong on 10/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
static NSMutableArray *_listHUDListener;

#import "MBProgressHUDListener.h"
@interface MBProgressHUDListener ()

@end

@implementation MBProgressHUDListener

+(void)initialize{
    _listHUDListener =[[NSMutableArray alloc] init];
}
+(void)removeAllHubListener{
    [_listHUDListener removeAllObjects];
}
-(id)initWithHubHidenBlock:(void (^)())hubHidenBlock_{
    if (self=[super init]) {
        _hubHidenBlock=hubHidenBlock_;
        [_listHUDListener addObject:self];
    }
    return self;
}

#pragma MBProgressHUDListener
-(void)hudWasHidden:(MBProgressHUD *)hud{
    if (_hubHidenBlock) {
        _hubHidenBlock();
    }
    [_listHUDListener removeObject:self];
}
+(NSArray *)allHubListener{
    return [_listHUDListener copy];
}
-(void)dealloc{
    [SNLog Log:11 data:@"Hub Listener Dealloc"];
}
@end
