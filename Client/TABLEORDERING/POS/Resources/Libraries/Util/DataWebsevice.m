//
//  DataWebsevice.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define keyCoderData @"data"
#define keyCoderURL @"url"
#define keyCoderResendCount @"resendCount"
#define keyCoderCreatedDate @"crDate"
#define keyCoderIdentifyData @"iden"
#define keyCoderExpiredDate @"exDate"
#define keyCoderKeyWS @"key"

#define MaxResend 10
#import "DataWebsevice.h"
#import "NSDate+Util.h"
@implementation DataWebsevice
@synthesize data;
@synthesize identifyData;
@synthesize url;
@synthesize reSendCount;
@synthesize createdDate;
-(instancetype)initWithData:(NSDictionary *)data_ url:(NSString *)url_ identifyData:(NSString *)identifyData_{
    if (self =[self initWithData:data_ url:url_ identifyData:identifyData_ expiredTime:ExpireTimeDefault]) {
        
    }
    return self;
}
-(instancetype)initWithData:(NSDictionary *)data_ url:(NSString *)url_ identifyData:(NSString *)identifyData_ expiredTime:(NSUInteger)expiredTime_{
    if (self=[super init]) {
        data=data_;
        url=url_;
        identifyData=identifyData_;
        createdDate=[NSDate date];
        expiredDate=[NSDate dateWithTimeInterval:expiredTime_ sinceDate:createdDate];
        _keyWS=[NSString stringWithFormat:@"%@_%@",identifyData,[createdDate getStringWithFormat:@"hh:mm:ss.SS"]];
    }
    return self;
}
-(BOOL)sameData:(DataWebsevice *)anotherData{
    return [identifyData isEqualToString:anotherData.identifyData];
}
-(void)increaseResendCount{
    reSendCount++;
}
-(BOOL)isEqualToDataWS:(DataWebsevice *)anotherData{
    return [identifyData isEqualToString:anotherData.identifyData] && ([createdDate compare:anotherData.createdDate]==NSOrderedSame);
}
-(BOOL)checkExpired{
    return ([expiredDate compare:[NSDate date]]==NSOrderedAscending) || (reSendCount > MaxResend);
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:data forKey:keyCoderData];
    [aCoder encodeObject:url forKey:keyCoderURL];
    [aCoder encodeInteger:reSendCount forKey:keyCoderResendCount];
    [aCoder encodeObject:createdDate forKey:keyCoderCreatedDate];
    [aCoder encodeObject:identifyData forKey:keyCoderIdentifyData];
    [aCoder encodeObject:expiredDate forKey:keyCoderExpiredDate];
    [aCoder encodeObject:_keyWS forKey:keyCoderKeyWS];
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self =[super init]) {
        data=[aDecoder decodeObjectForKey:keyCoderData];
        url=[aDecoder decodeObjectForKey:keyCoderURL];
        reSendCount=[aDecoder decodeIntegerForKey:keyCoderResendCount];
        createdDate=[aDecoder decodeObjectForKey:keyCoderCreatedDate];
        identifyData =[aDecoder decodeObjectForKey:keyCoderIdentifyData];
        expiredDate =[aDecoder decodeObjectForKey:keyCoderExpiredDate];
        _keyWS =[aDecoder decodeObjectForKey:keyCoderKeyWS];
    }
    return self;
}
@end
