//
//  NSString+Util.h
//  POS
//
//  Created by Nha Duong Cong on 10/14/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Util)
-(BOOL)isEmailFormat;
-(BOOL)isUIntegerFormat;
-(BOOL)isFloatFormat;
-(BOOL)isTKAmountFormat;
-(BOOL)isIPv4AddressFormat;
-(BOOL)isPrefixIPv4AddressFormat;
-(BOOL)isPortFormat;
-(BOOL)isURLFormat;
-(NSString*)encodeURL;
-(NSString*)decodeURL;
-(CGFloat)widthWithFont:(UIFont*)font;
+(CGFloat)heightWithFont:(UIFont*)font;
-(NSInteger)heightWithFont:(UIFont*)font width:(CGFloat)width;
@end
