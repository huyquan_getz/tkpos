//
//  EmailData.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/3/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
typedef enum _MailType{
    MailTypeNone=0,
    // message status change
    MailTypeReady=1,
    MailTypeRefund=2,
    MailTypeCancel=3,
    MailTypeRejectCancel=4,
    
    // constain send content receipt
    MailTypeReSendRefund=5,
    MailTypeReSendCancel=6,
    MailTypeCollected=7,
    MailTypeReSendCollected=8,
    
    // reportCashDrawer

    MailTypeReportCashDrawer =100,
    
    // Notify to customer change  status table  ordering
    MailTypeOrderingConfirm =101,
    MailTypeOrderingReject = 102, // reject order waiting
    MailTypeOrderingCancel =103, // cancel and refund when comfirmed
    MailTypeOrderingReady = 104, // confirmed ready to serving
    MailTypeOrderingAmend =105, // accept& refund with amend
}MailType;
typedef enum _PriorityEmailType{
    PriorityEmailTypeHigh=10,
    PriorityEmailTypeNormal=7,
    PriorityEmailTypeLow=3,
}PriorityEmailType;
#import <Foundation/Foundation.h>

@interface EmailData : NSObject<NSCoding>{
    NSDictionary *data;
    NSString * email;
    MailType emailType;
    NSInteger priority;
    NSInteger reSendCount;
    NSDate *createdDate;
}
@property (readonly)NSDictionary *data;
@property (readonly)NSString *email;
@property (readonly)MailType emailType;
@property (readonly)NSInteger priority;
@property (readonly)NSInteger reSendCount;
@property (readonly)NSDate *createdDate;
@property (strong,nonatomic) NSString *keyMail;
-(instancetype)initWithEmailData:(NSDictionary*)data_ emailReceiver:(NSString*)email_  emailType:(MailType) emailType_;
-(instancetype)initWithEmailData:(NSDictionary*)data_ emailReceiver:(NSString*)email_  emailType:(MailType) emailType_ emailPriority:(PriorityEmailType)priority_;
-(void)increaseResendCount;
-(void)decreasePriority;
-(BOOL)sameData:(EmailData*)emailData;
@end
