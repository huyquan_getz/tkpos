//
//  EmailData.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/3/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define MaxResendCount 3
#define DataKeyCoder @"data"
#define EmailKeyCoder @"email"
#define EmailTypeKeyCoder @"emailType"
#define ResendCountKeyCoder @"resendCount"
#define PriorityKeyCoder @"priority"
#define CreatedDateKeyCoder @"createdDate"
#define KeyIDEmailKeyCoder @"keyIDEmail"
#import "EmailData.h"

@implementation EmailData
@synthesize data;
@synthesize email;
@synthesize emailType;
@synthesize reSendCount;
@synthesize priority;
@synthesize createdDate;
-(instancetype)initWithEmailData:(NSDictionary *)data_ emailReceiver:(NSString *)email_ emailType:(MailType)emailType_ emailPriority:(PriorityEmailType)priority_{
    if (self =[super init]) {
        data=data_;
        email=email_;
        emailType=emailType_;
        priority=priority_;
        reSendCount=0;
        createdDate=[NSDate date];
        _keyMail=@"";
    }
    return self;
}
-(instancetype)initWithEmailData:(NSDictionary *)data_ emailReceiver:(NSString *)email_ emailType:(MailType)emailType_{
    if (self=[self initWithEmailData:data_ emailReceiver:email_ emailType:emailType_ emailPriority:PriorityEmailTypeNormal]) {
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:data forKey:DataKeyCoder];
    [aCoder encodeObject:email forKey:EmailKeyCoder];
    [aCoder encodeInteger:emailType forKey:EmailTypeKeyCoder];
    [aCoder encodeInteger:reSendCount forKey:ResendCountKeyCoder];
    [aCoder encodeInteger:priority forKey:PriorityKeyCoder];
    [aCoder encodeObject:createdDate forKey:CreatedDateKeyCoder];
    [aCoder encodeObject:_keyMail forKey:KeyIDEmailKeyCoder];
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self =[super init]) {
        data=[aDecoder decodeObjectForKey:DataKeyCoder];
        email=[aDecoder decodeObjectForKey:EmailKeyCoder];
        emailType=(int)[aDecoder decodeIntegerForKey:EmailTypeKeyCoder];
        reSendCount=[aDecoder decodeIntegerForKey:ResendCountKeyCoder];
        priority =[aDecoder decodeIntegerForKey:PriorityKeyCoder];
        createdDate =[aDecoder decodeObjectForKey:CreatedDateKeyCoder];
        _keyMail =[aDecoder decodeObjectForKey:KeyIDEmailKeyCoder];
    }
    return self;
}
-(void)increaseResendCount{
    reSendCount++;
}
-(void)decreasePriority{
    priority--;
}
-(BOOL)sameData:(EmailData *)emailData{
    return [createdDate compare:emailData.createdDate]==NSOrderedSame;
}
@end
