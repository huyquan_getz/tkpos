//
//  DataWebsevice.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define ExpireTimeDefault 600
#import <Foundation/Foundation.h>

@interface DataWebsevice : NSObject<NSCoding>{
    NSDictionary *data;
    NSString * url;
    int reSendCount;
    NSDate *createdDate;
    NSString *identifyData;
    NSDate *expiredDate;
}
@property (readonly)NSDictionary *data;
@property (readonly)NSString *url;
@property (readonly)NSString *identifyData;
@property (readonly)int reSendCount;
@property (readonly)NSDate *createdDate;
@property (strong,nonatomic) NSString *keyWS;

-(instancetype)initWithData:(NSDictionary*)data_ url:(NSString*)url_ identifyData:(NSString*)identifyData_;
-(instancetype)initWithData:(NSDictionary*)data_ url:(NSString*)url_ identifyData:(NSString*)identifyData_ expiredTime:(NSUInteger)expiredTime_;
-(BOOL)sameData:(DataWebsevice*)anotherData;
-(BOOL)isEqualToDataWS:(DataWebsevice*)anotherData;
-(BOOL)checkExpired;
-(void)increaseResendCount;
@end
