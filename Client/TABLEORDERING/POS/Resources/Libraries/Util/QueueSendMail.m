//
//  QueueSendMail.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/3/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define MaxResendCount 3
#import "File.h"
#import "QueueSendMail.h"
#import "Controller.h"
@implementation QueueSendMail{
    NSOperationQueue *queueSend;
    BOOL requestSendAgain;
    NSInteger numberOperationing;
}
+(QueueSendMail*)sharedQueue{
    __strong static QueueSendMail *_QueueSendMail=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _QueueSendMail=[[QueueSendMail alloc] init];
    });
    return _QueueSendMail;
}
-(instancetype)init{
    if (self =[super init]) {
        filePathSaveReceiptWatingSend=[[File rootFolder] stringByAppendingPathComponent:@"emails.bin"];
        queueSend =[NSOperationQueue new];
        [queueSend setMaxConcurrentOperationCount:1];
        numberOperationing=0;
        requestSendAgain=NO;
    }
    return self;
}
-(void)addEmailToQueue:(EmailData *)emailData{
    [self addEmailToQueue:emailData addToStore:YES];
}
-(void)addEmailToQueue:(EmailData *)emailData addToStore:(BOOL)addToStore{
    if (addToStore) {
        [self addEmailtoStore:emailData];
    }
    NSInvocationOperation *operation=[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(sendEmail:) object:emailData];
    [queueSend addOperation:operation];
    numberOperationing++;
}
-(void)sendEmail:(EmailData*)emailData{
    if ([[NetworkUtil sharedInternetConnection] checkInternetConnection]) {
        // send email
        NSInteger returnCodeSend=[self sendEmailWithData:emailData.data toEmail:emailData.email type:emailData.emailType];
        [emailData increaseResendCount];
        [emailData decreasePriority];
        if (returnCodeSend ==ReturnCodeRequestError && [[NetworkUtil sharedInternetConnection] checkInternetConnection]) { // resend again when internet bad
            returnCodeSend=[self sendEmailWithData:emailData.data toEmail:emailData.email type:emailData.emailType];
        }
        
        if (returnCodeSend!= ReturnCodeSuccess && returnCodeSend!=ReturnCodeSuccessNotUpdate && emailData.reSendCount < MaxResendCount) {
            [self addEmailtoStore:emailData];
            [SNLog Log:@"email into queue(type:%d key:%@)",emailData.emailType,emailData.keyMail];
        }else{
            [self removeEmailFromStore:emailData];
            if (returnCodeSend==ReturnCodeSuccess) {
                [SNLog Log:@"email sent(type:%d key:%@)",emailData.emailType,emailData.keyMail];
            }else if(returnCodeSend== ReturnCodeSuccessNotUpdate){
                [SNLog Log:@"email sent not update(type:%d key:%@)",emailData.emailType,emailData.keyMail];
            }else{
                [SNLog Log:@"email not sent(type:%d key:%@)",emailData.emailType,emailData.keyMail];
            }
        }

    }
    numberOperationing--;
    if (numberOperationing<=0) {
        if (requestSendAgain) {
            requestSendAgain=NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self reSendEmailInQueue];
            });
        }
    }
}
-(NSInteger)sendEmailWithData:(NSDictionary *)data toEmail:(NSString *)email type:(MailType)mailType{
    return [Controller sendEmailWithData:data toEmail:email type:mailType];
}
-(void)addEmailtoStore:(EmailData*)emailData{
    NSMutableArray *emailsInQueue=[[self getEmailsInQueue] mutableCopy];
    BOOL existed=NO;
    for (NSInteger i=0;i<emailsInQueue.count;i++) {
        EmailData *dataE=emailsInQueue[i];
        if ([emailData sameData:dataE]) { // email existed
            existed=YES;
            if (emailData.reSendCount <= dataE.reSendCount) { // not change
                return;
            }else{
                [emailsInQueue replaceObjectAtIndex:i withObject:emailData];
            }
            break;
        }
    }
    if (!existed) {
        [emailsInQueue addObject:emailData];
    }
    [emailsInQueue sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        EmailData *em1=obj1;
        EmailData *em2=obj2;
        if (em1.priority==em2.priority) {
            return NSOrderedSame;
        }else if (em1.priority>em2.priority){
            return NSOrderedAscending;
        }else{
            return NSOrderedDescending;
        }
    }];
    [self saveEmailDatas:emailsInQueue];
}
-(void)removeEmailFromStore:(EmailData*)emailData{
    NSMutableArray *emailsInQueue=[[self getEmailsInQueue] mutableCopy];
    for (EmailData *dataE in emailsInQueue) {
        if ([dataE sameData:emailData]) {
            [emailsInQueue removeObject:dataE];
            [self saveEmailDatas:emailsInQueue];
            break;
        }
    }
}
-(NSArray*)getEmailsInQueue{
    NSFileManager *fm =[NSFileManager defaultManager];
    if (![fm fileExistsAtPath:filePathSaveReceiptWatingSend]) {
        return @[];
    }else{
        return [NSKeyedUnarchiver unarchiveObjectWithFile:filePathSaveReceiptWatingSend];
    }
}

-(BOOL)saveEmailDatas:(NSArray *)emailDatas{
    return [NSKeyedArchiver archiveRootObject:emailDatas toFile:filePathSaveReceiptWatingSend];
}
-(void)reSendEmails{
    if (numberOperationing<=0) {
        [self reSendEmailInQueue];
    }else{
        requestSendAgain=YES;
    }
}
-(void)reSendEmailInQueue{
    NSArray *emailInQueue=[self getEmailsInQueue];
    for (EmailData *emailData in emailInQueue) {
        [self addEmailToQueue:emailData addToStore:NO];
    }

}
-(void)cancelAllOperationSendMail{
    [queueSend cancelAllOperations];
    numberOperationing=0;
}
#pragma  Network Delegate
-(void)networkBackgroundNotiConnection:(BOOL)connectionOn{
    //background noti;
    if (connectionOn) {
        [self reSendEmails];
    }
}
@end
