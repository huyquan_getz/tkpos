//
//  LanguageUtil.m
//  POS
//
//  Created by Nha Duong Cong on 10/27/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "LanguageUtil.h"
#import "UserDefaultModel.h"

@implementation LanguageUtil
@synthesize dictLanguageKey;
+(LanguageUtil *)sharedLanguageUtil{
    __strong static LanguageUtil *_languageUtil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _languageUtil=[[LanguageUtil alloc] initWithLanguageType:[LanguageUtil getLanguageFromUserDefault]];
    });
    return _languageUtil;
}
-(id)initWithLanguageType:(LanguageType)languageType_{
    if (self=[super init]) {
        languageType =languageType_;
        dictLanguageKey=[LanguageUtil dataLanguageWithType:languageType];
    }
    return self;
}
+(LanguageType)getLanguageFromUserDefault{
    NSNumber *languageNb = [UserDefaultModel getLanguageType];
    if (languageNb==nil) {
        LanguageType languageType=LanguageTypeEnglish;
        [UserDefaultModel saveLanguageType:[NSNumber numberWithInt:languageType]];
        return languageType;
    }else{
        return (LanguageType)[languageNb intValue];
    }
}
-(BOOL)setLanguageWithType:(LanguageType)languageType_{
    if (languageType == languageType_) {
        return YES;
    }else{
        NSMutableDictionary *dict=[LanguageUtil dataLanguageWithType:languageType_];
        if (dict) {
            languageType=languageType_;
            [dictLanguageKey removeAllObjects];
            [dictLanguageKey addEntriesFromDictionary:dict];
            return YES;
        }else{
            return NO;
        }
    }
}
+(NSMutableDictionary*)dataLanguageWithType:(LanguageType)type{
    NSString *languageFileName=nil;
    switch (type) {
        case LanguageTypeEnglish:
            languageFileName=@"EnglishKey";
            break;
        case LanguageTypeVietnamese:
            languageFileName=@"VietnameseKey";
            break;
        default:
            break;
    }
    if (languageFileName) {
        NSString *pathLanguageKeyFile=[[NSBundle mainBundle]pathForResource:languageFileName ofType:@"plist"];
        NSMutableDictionary *data=[[NSMutableDictionary alloc] initWithContentsOfFile:pathLanguageKeyFile];
        return data;
    }else{
        return nil;
    }
}
-(NSString *)stringByKey:(NSString *)key{
    return dictLanguageKey[key];
}

@end
