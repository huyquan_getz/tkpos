//
//  Constant.h
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
// type document on database
#define tkStoreTable @"Store"
#define tkCategoryTable @"Category"
#define tkProductTable @"Product"
#define tkProductItemTable @"ProductItem"
#define tkOrderTable @"Order"
#define tkTransationTable @"Transation"
#define tkMerchantSettingTable @"UserSetting"
#define tkUserCashierTable @"Employee"
#define tkUserMerchantTable @"MerchantMember"
#define tkMerchantGeneralSetting @"MerchantGeneralSetting"
#define tkTaxTable @"Tax"
#define tkStationTable @"Station"
#define tkTableOrderingSettingTable @"TableOrderingSetting"
#define tkListDevicesSettingTable @"ListDevices"
        //local
#define tkShiftDrawerTable @"ShiftDrawer"
#define tkPrinterTable @"PrinterLocal"
#define tkPrintingTable @"PrintingLocal"
//define Queue
#define tkKeyPrinterIp @"printerIp"

//reasonOrder
#define tkReasonOrderCancel @"CANCEL_ORDER"
#define tkReasonOrderWrong @"WRONG_ORDER"
#define tkReasonOrderProductFaulty @"PRODUCT_FAULTY"

//key document
#define tkKeyProduct @"product"
#define tkKeyProductId @"productId"
#define tkKeyProducts @"products"
#define tkkeyArrayProduct @"arrayProduct"
#define tkkeyArrayPayment @"arrayPayment"
#define tkKeyCategory @"category"
#define tkKeyCategoryId @"categoryId"
#define tkKeyQuantity @"quantity"
#define tkKeyQuantities @"quantities"
#define tkKeyAmount @"amount"
#define tkKeyAmounts @"amounts"
#define tkKeyPrice @"price"
#define tkKeyTitle @"title"
#define tkKeyName @"name"
#define tkKeyShortName @"nameShort"
#define tkKeyFullName @"fullName"
#define tkKeyFirstName @"firstName"
#define tkKeyProperty @"property"
#define tkKeyLocalOnly @"localOnly"
#define tkKeyImage @"image"
#define tkKeyImages @"arrayImage"
#define tkKeyDescription @"description"
#define tkKeyCurrency @"currency"
#define tkKeyCashierId @"cashierId"
#define tkKeyTable @"table"
#define tkKeyOrderStatus @"orderStatus"
#define tkKeyPaymentStatus @"paymentStatus"
#define tkKeyOrderType @"orderType"
#define tkKeyStore @"store"
#define tkKeyIndexStore @"index"
#define tkKeyBranchIndex @"branchIndex"
#define tkKeyStoreId @"businessID"
#define tkKeyStores @"stores"
#define tkKeyId @"_id"
#define tkKeyRev @"_rev"
#define tkKeySKU @"sku"
#define tkKeyPinCode @"pinCode"
#define tkKeyRefundInfo @"refundInfo"
#define tkKeyStatus @"status"
#define tkKeyDisplay @"display"
#define tkKeyArrayVariant @"arrayVarient"
#define tkKeyArrayInventory @"arrayInventory"
#define tkKeyCreatedDate @"createdDate"
#define tkKeyModifiedDate @"modifiedDate"
#define tkKeyCompletedDate @"completedDate"
#define tkKeyPaidDate @"paidDate"
#define tkKeyRequestDate @"requestDate"
#define tkKeyExpiredTokenDate @"expiredTokenDate"
#define tkKeyClosedDate @"closedDate"
#define tkKeyRefundDate @"refundDate"
#define tkKeyUserId @"userID"
#define tkKeyUserOwner @"userOwner"
#define tkKeyPincode @"pinCode"
#define tkKeyEmployeeId @"employeeId"
#define tkKeyEmployeeName @"employeeName"
#define tkKeyArrayPermission @"arrPermission"
#define tkKeyRole @"role"
#define tkKeyInventory @"inventory"
#define tkKeyIsCancelRequest @"isCancelRequest"
#define tkKeyBusinessID @"businessID"
#define tkKeyChannels @"channels"
#define tkKeyOrderCode @"orderCode"
#define tkKeyReasonRefund @"reasonRefund"
#define tkKeyCustomerReceiver @"customerReceiver"
#define tkKeyCustomerSender @"customerSender"
#define tkKeyStaff @"staff"
#define tkKeyGrandTotal @"grandTotal"
#define tkKeySubTotal @"subTotal"
#define tkKeyChangeDue @"changeDue"
#define tkKeyRoundAmount @"roundAmount"
#define tkKeyDiscount @"discount"
#define tkKeyApplyDiscount @"applyDiscount"
#define tkKeyDiscountID @"discountID"
#define tkKeyDiscountProductItem @"discountProductItem"
#define tkKeyDiscountCartItem @"discountCartItem"
#define tkKeyDiscountCartValue @"discountCartValue"
#define tkKeyDiscountCart @"discountCart"
#define tkKeyIsPOS @"isPOS"
#define tkKeyArrayTax @"arrayTax"
#define tkKeyStoreInformation @"storeInformation"
#define tkKeyValueRate @"valueRate"
#define tkKeyIsPercent @"isPercent"
#define tkKeyArrServiceCharge @"arrServiceCharge"
#define tkKeyArrayAdhocProduct @"arrayAdhocProduct"
#define tkKeySpecialRequest @"specialRequest"
#define tkKeyRemark @"remark"
#define tkKeyArrayStatusHistory @"arrayStatusHistory"
// Tabler Ordering Setting
#define tkKeyAllowAutoAcceptOrder @"allowAutoAcceptOrder"
#define tkKeyAllowPushNotification @"allowPushNotification"
#define tkKeyAllowSendEmail @"allowSendEmail"
#define tkKeyAllowSendSMS @"allowSendSMS"
#define tkKeyAllowSound @"allowSound"
#define tkKeyApi @"apikey"
#define tkKeyTableName @"tableName"
#define tkKeyMessage @"message"
#define tkKeyRecipients @"recipients"
#define tkKeyEncoding @"encoding"
#define tkKeyPhone @"phone"
//sort
#define tkSortKeyNormal tkKeyModifiedDate

//product
#define tkKeyImageDefault @"imageDefault"
#define tkKeyArrayImage @"arrayImage"

//key util
#define tkKeyEmail @"email"
#define tkKeyPassword @"password"
#define tkKeyAccessToken @"access_token"
#define tkKeyAuthorization @"Authorization"
#define tkKeyBusinessName @"businessName"
#define tkKeyUserName @"userName"
#define tkKeyMerchantID @"merchantID"
//notification
#define tkNotiChangedDatabase @"ChangedDatabase"

//data default
#define tkUserAccessSyncDefault @"smoov"
#define tkPassAccessSyncDefault @"smoov@123"
//integer
#define tkNanoSecond 1000000000
#define ESP 0.0000001
#define tkDelaySearchText 0.5
//color
#define tkColorButtonBackground [UIColor colorWithRed:6/255.0 green:67/255.0 blue:128/255.0 alpha:1]
#define tkColorMainBackground [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1]
#define tkColorMain [UIColor colorWithRed:6/255.0 green:122/255.0 blue:189/255.0 alpha:1]   //067ADA
#define tkColorFrameBorder [UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1]
#define tkColorStatusMessageError [UIColor redColor]
#define tkColorDarkRed [UIColor colorWithRed:195/255.0 green:4/255.0 blue:4/255.0 alpha:1]
#define tkColorStatusMessageSuccess [UIColor blueColor]
#define tkColorMainSelected [UIColor colorWithRed:6/255.0 green:122/255.0 blue:189/255.0 alpha:1]
#define tkColorMainAccept [UIColor colorWithRed:6/255.0 green:122/255.0 blue:189/255.0 alpha:1]
#define tkColorPending [UIColor colorWithRed:255/255.0 green:118/255.0 blue:43/255.0 alpha:1]
#define tkColorReady [UIColor colorWithRed:73/255.0 green:189/255.0 blue:34/255.0 alpha:1]
#define tkColorMainActive [UIColor colorWithRed:0 green:143/255.0 blue:231/255.0 alpha:1]
//font size
#define tkFontMainName @"Helvetica Neue"
#define tkFontMainWithSize(x) [UIFont fontWithName:@"Helvetica Neue" size:x]
#define tkBoldFontMainWithSize(x) [UIFont fontWithName:@"HelveticaNeue-Bold" size:x]

#define tkFontMainText tkFontMainWithSize(17)
#define tkBoldFontMainText tkBoldFontMainWithSize(17)

#define tkFontMainTitleButton tkFontMainWithSize(17)
#define tkBoldFontMainTitleButton tkBoldFontMainWithSize(17)

// date
#define tkNoLimitDate [NSDate dateWithYear:9999 month:9 day:9 timeZone:[NSTimeZone defaultTimeZone]]

//activity
#define tkActivityIndicatorViewDefault [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]

// view size
#define tkSpaceLeft 20
#define tkHeightStatusBar 20
#define tkHeightNavigationBar 44
#define tkHeightStatusNaviBar 66
#define tkEgdeInsetTopContentNavi 10 
#define tkCornerRadiusButton 3
#define tkCornerRadiusViewPopup 4
#define tkWidthSpaceLeftTextfield 15

//value static
#define tkMaxLittleStockProduct 3

//app
#define tkAppType @"table"
#define tkOSType @"iOS"

//link
/*
#define tkLinkSignUp @"https://accounts.google.com/ServiceLogin?hl=en&continue=https://www.google.com.vn/"
#define tkWebServiceAuthenUserPass @"http://service.demo.smoovpos-dev.com/AuthenticationPOS/WSCheckAuthenticationPOS"
#define tkWebServiceAuthenToken @"http://service.demo.smoovpos-dev.com/AuthenticationPOS/WSGetAuthenticationPOS"
#define tkWebServiceSendMailReceipt @"http://service.demo.smoovpos-dev.com/SendMailPOS/WSSendMailReceipt"
#define tkWebServiceSendMailChangeStatus @"http://service.demo.smoovpos-dev.com/SendMailPOS/WSSendMailOrder"
#define tkWebServiceSendMailCashDrawerReport @"http://service.demo.smoovpos-dev.com/SendMailPOS/WSSendMailCashDrawer"
#define tkWebServiceSendMailChangeStatusOrdering @"http://service.demo.smoovpos-dev.com/SendMailPOS/WSSendMailNotifyTableOrdering"
#define tkWebServiceCheckAndAddDevice @"http://service.demo.smoovpos-dev.com/AuthenticationPOS/WSAddDeviceToken"
#define tkWebServiceResetBadgeDevice @"http://service.demo.smoovpos-dev.com/TableOrder/WSResetBadge"
#define tkWebServiceSendSMS @"http://admin.smoovim.com/api/sms"
#define tkWebServiceSmoovPayRefundOrder @"http://service.demo.smoovpos-dev.com/SmoovPay/WSRefundOrder"
#define tkWebServiceUpdateStatusOrderToServer @"http://service.demo.smoovpos-dev.com/TableOrder/WSUpdateOrder"
*/

// uat

#define tkLinkSignUp @"https://accounts.google.com/ServiceLogin?hl=en&continue=https://www.google.com.vn/"
#define tkWebServiceAuthenUserPass @"http://uat-api.smoovpos-dev.com/AuthenticationPOS/WSCheckAuthenticationPOS"
#define tkWebServiceAuthenToken @"http://uat-api.smoovpos-dev.com/AuthenticationPOS/WSGetAuthenticationPOS"
#define tkWebServiceSendMailReceipt @"http://uat-api.smoovpos-dev.com/SendMailPOS/WSSendMailReceipt"
#define tkWebServiceSendMailChangeStatus @"http://uat-api.smoovpos-dev.com/SendMailPOS/WSSendMailOrder"
#define tkWebServiceSendMailCashDrawerReport @"http://uat-api.smoovpos-dev.com/SendMailPOS/WSSendMailCashDrawer"
#define tkWebServiceSendMailChangeStatusOrdering @"http://uat-api.smoovpos-dev.com/SendMailPOS/WSSendMailNotifyTableOrdering"
#define tkWebServiceCheckAndAddDevice @"http://uat-api.smoovpos-dev.com/AuthenticationPOS/WSAddDeviceToken"
#define tkWebServiceResetBadgeDevice @"http://uat-api.smoovpos-dev.com/TableOrder/WSResetBadge"
#define tkWebServiceSendSMS @"http://admin.smoovim.com/api/sms"
#define tkWebServiceSmoovPayRefundOrder @"http://uat-api.smoovpos-dev.com/SmoovPay/WSRefundOrder"
#define tkWebServiceUpdateStatusOrderToServer @"http://uat-api.smoovpos-dev.com/TableOrder/WSUpdateOrder"

//#import
#import <Foundation/Foundation.h>
#import "NSString+Util.h"
#import "UIAlertView+Blocks.h"
#import "UITextField+Util.h"
#import "NetworkUtil.h"
#import "LanguageUtil.h"
#import "UIImage+Util.h"
#import "UIColor+Util.h"
#import "CBLJSON+Util.h"
#import "NSObject+Util.h"
#import "NSString+SBJSON.h"
#import "DLILOperation+Resize.h"
#import "NSDate+Util.h"
#import "NSDate+Util.h"
#import "UILabel+FormatText.h"
#import "UIImage+MDQRCode.h"
#import "SyncManager.h"
#import "SNLog.h"
#import "LanguageUtil.h"
#import "UIView+Shadow.h"
#import "SMPageControl.h"


//type enum

typedef enum _GenderType{
    GenderTypeMale=0,
    GenderTypeFemale=1,
    GenderTypeNone=2
}GenderType;
@interface Constant : NSObject

@end
