//
//  NSObject+Util.m
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "NSObject+Util.h"

@implementation NSObject (Util)
-(id )convertNullToNil{
    if ([self isEqual:[NSNull null]]) {
        return nil;
    }
    return self;
}
-(id)convertNilToNull{
    if (self==nil) {
        return [NSNull null];
    }
    return nil;
}
@end
