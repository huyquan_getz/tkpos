//
//  BRListPicker.m
//  POS
//
//  Created by Cong Nha Duong on 3/5/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "BRListPicker.h"

@interface BRListPicker ()

@end

@implementation BRListPicker
-(instancetype)initWithListObject:(NSArray *)objects setDisplay:(NSString *(^)(id))displayObject_{
    if (self =[super init]) {
        listObject=[objects copy];
        displayObject=displayObject_;
        _indexDefault=0;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _vMain.layer.cornerRadius=4;
    _vMain.layer.borderColor=[UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1].CGColor;
    _vMain.layer.borderWidth=1;
    [_vMain.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vMain.layer setShadowOpacity:0.7];
    [_vMain.layer setShadowRadius:4];
    [_vMain.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [_vCover addGestureRecognizer:tap];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [_pkView selectRow:_indexDefault inComponent:0 animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tapCoverView:(id)sender{
    [self hidePicker];
    if (_delegate && [_delegate respondsToSelector:@selector(brListPicker:cancelSelect:)]) {
        [_delegate brListPicker:self cancelSelect:YES];
    }
}
- (IBAction)clickDone:(id)sender {
    [self hidePicker];
    if (_delegate && [_delegate respondsToSelector:@selector(brListPicker:selectIndex:valueSelected:)]) {
        NSInteger indexSelected=[_pkView selectedRowInComponent:0];
        [_delegate brListPicker:self selectIndex:indexSelected valueSelected:listObject[indexSelected]];
    }
}
-(void)showPickerOnVC:(UIViewController *)parentVC{
    [parentVC addChildViewController:self];
    self.view.frame=parentVC.view.frame;
    [parentVC.view addSubview:self.view];
}
-(void)hidePicker{
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

#pragma PickerViewDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listObject.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return displayObject(listObject[row]);
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
