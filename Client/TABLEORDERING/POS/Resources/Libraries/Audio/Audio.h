//
//  Audio.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/11/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol AudioDelegate <NSObject>

-(void)audio:(id)sender finishWithError:(NSError*)error;
-(void)audio:(id)sender beginInterruption:(BOOL)none;

@end
#import <AVFoundation/AVFoundation.h>

@interface Audio : NSObject<AVAudioPlayerDelegate>{
    AVAudioPlayer *player;
    BOOL isPlaying;
}
-(instancetype)initWithFileName:(NSString*)fileName ofType:(NSString*)type;
@property (weak,nonatomic) id<AudioDelegate> delegate;
-(void)playingWithTimeInterval:(double)second;
-(void)playingWithLoop:(BOOL)loop;
-(void)stop;
@end
