//
//  Audio.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/11/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "Audio.h"

@implementation Audio
-(instancetype)initWithFileName:(NSString *)fileName ofType:(NSString *)type{
    if (self =[super init]) {
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSString *filePath = [mainBundle pathForResource:fileName ofType:type];
        NSData *fileData = [NSData dataWithContentsOfFile:filePath];
        NSError *error = nil;
        player = [[AVAudioPlayer alloc] initWithData:fileData
                                                         error:&error];
        player.delegate=self;
        [player prepareToPlay];
        isPlaying=NO;
    }
    return self;
}
-(void)playingWithLoop:(BOOL)loop{
    if (!isPlaying) {
        if (loop) {
            player.numberOfLoops=INT32_MAX;
        }
        [player play];
        isPlaying=YES;
    }
}
-(void)playingWithTimeInterval:(double)second{
    if (!isPlaying) {
        player.numberOfLoops=INT32_MAX;
        [player play];
        isPlaying=YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(second * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self stop];
        });
    }
}
-(void)stop{
    [player stop];
    if (_delegate) {
        [_delegate audio:self finishWithError:nil];
    }
}
-(void)dealloc{
    [player stop];
}
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if (_delegate) {
        [_delegate audio:self finishWithError:nil];
    }
}
-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    if (_delegate) {
        [_delegate audio:self beginInterruption:YES];
    }
}
-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error{
    if (_delegate) {
        [_delegate audio:self finishWithError:error];
    }
}
@end
