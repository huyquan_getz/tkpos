//
//  ControllerTest.m
//  POS
//
//  Created by Nha Duong Cong on 10/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UserDefaultModel.h"
#import "Controller.h"
#import "UtilBusinessModel.h"

@interface ControllerTest : XCTestCase

@end

@implementation ControllerTest{
    SyncManager *sync;
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
-(BOOL)compareDict:(NSDictionary*)dic andDict:(NSDictionary*)dic2{
    if (dic.count == dic2.count) {
        for (NSString *key in dic.allKeys) {
            if (![dic[key] isEqualToString:dic2[key]]) {
                return NO;
            }
        }
    }else{
        return NO;
    }
    return YES;
}
-(BOOL)compareArrayString:(NSArray*)arr1 andArrayString:(NSArray*)arr2{
    if (arr1.count==arr2.count) {
        for (int i=0;i<arr1.count;i++) {
            if (![arr1[i] isEqualToString:arr2[i]]) {
                return NO;
            }
        }
        return YES;
    }else{
        return NO;
    }
}

@end
