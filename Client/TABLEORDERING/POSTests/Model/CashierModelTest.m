//
//  CashierModelTest.m
//  POS
//
//  Created by Nha Duong Cong on 10/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define eps 0.00001
#import <XCTest/XCTest.h>
#import "CashierBusinessModel.h"

@interface CashierModelTest : XCTestCase

@end

@implementation CashierModelTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testCeil
{
    double amount =123.45;
    double ceil0=[TKAmountDisplay ceil:amount withIndexNumberCeil:0];
    double ceil1=[TKAmountDisplay ceil:amount withIndexNumberCeil:1];
    XCTAssert(ceil0==124.00,"ceill fail");
    XCTAssert(ceil1==130.00,"ceill fail");
}
-(void)testRound{
    double r1=123.345;
    double r2=123.3451;
    double r3=123.346;
    double r4=123.35;
    double r5=123.351;
    double r6=123.35491;
    XCTAssert([TKAmountDisplay tkRoundCash:r1] == 123.35, "round amount error");
    XCTAssert([TKAmountDisplay tkRoundCash:r2] == 123.35, "round amount error");
    XCTAssert([TKAmountDisplay tkRoundCash:r3] == 123.35, "round amount error");
    XCTAssert([TKAmountDisplay tkRoundCash:r4] == 123.35, "round amount error");
    XCTAssert([TKAmountDisplay tkRoundCash:r5] == 123.35, "round amount error");
    XCTAssert([TKAmountDisplay tkRoundCash:r6] == 123.35, "round amount error");
}
-(void)testStringByRound{
    XCTAssert([[TKAmountDisplay stringByRoundCashAmout:112.3342] isEqualToString:@"112.33"],"string by round fail");
    XCTAssert([[TKAmountDisplay stringByRoundCashAmout:112.33] isEqualToString:@"112.33"],"string by round fail");
    XCTAssert([[TKAmountDisplay stringByRoundCashAmout:112.335] isEqualToString:@"112.34"],"string by round fail");
    XCTAssert([[TKAmountDisplay stringByRoundCashAmout:112] isEqualToString:@"112.00"],"string by round fail");
}
-(void)testStringAmountWithCurrency{
    XCTAssert([[TKAmountDisplay stringAmount:123.234 withCurrency:@"$"] isEqualToString:@"$ 123.23"], "String amount with currency fail");
    
}
-(void)testVariant{
//    NSDictionary *pro=@{@"title":@"congacon"};
//    NSDictionary *varient=@{@"color":@"blue",@"size":@"M",@"price":@"123.2"};
//    NSArray *varientOption=@[@{@"optionKey":@"color",@"optionValue":@"blue"},@{@"optionKey":@"size",@"optionValue":@"M"}];
//    NSDictionary *proOrder=[CashierBusinessModel productOrderWithProduct:pro quantity:100 varient:varient variantOption:varientOption];
//    CashierBusinessModel *cashier=[[CashierBusinessModel alloc] initWithCurrency:@"$"];
//    NSString *titleVariant=[cashier titleVariantOfProductOrder:proOrder];
//    XCTAssert([titleVariant isEqualToString:@"congacon (blue-M)"], "title variant fail");
//    XCTAssert([cashier quantityOfProductOrder:proOrder]== 100,"quanity product order fail");
//    XCTAssert([cashier totalAmountCrudeOfProductOrder:proOrder] == 12320 ,"crude total amount fail");
    
}
-(void)testRoundAmountTotal{
    double r1=123.34;
    double r2=123.35;
    double r3=123.36;
    double r4=123.37;
    double r5=123.33;
    XCTAssert([TKAmountDisplay tkRoundCashTotal:r1] == 123.35, "round amount total error");
    XCTAssert([TKAmountDisplay tkRoundCashTotal:r2] == 123.35, "round amount total error");
    XCTAssert([TKAmountDisplay tkRoundCashTotal:r3] == 123.35, "round amount total error");
    XCTAssert([TKAmountDisplay tkRoundCashTotal:r4] == 123.35, "round amount total error");
    XCTAssert([TKAmountDisplay tkRoundCashTotal:r5] == 123.35, "round amount total error");
}
@end
