//
//  LoginTest.m
//  POS
//
//  Created by Nha Duong Cong on 10/14/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>
#import "LoginVC.h"
#import "LoginCashierVC.h"
#import "LoginChooseStoreVC.h"
#import "UserDefaultModel.h"
#import "CouchBaseBusinessModel.h"

@interface LoginTest : XCTestCase
{
    LoginVC *loginVC;
}
@end

@implementation LoginTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    loginVC=nil;
    [super tearDown];
}

-(NSArray *)dataInputPass{
    NSMutableArray *tcase=[[NSMutableArray alloc] init];
    
    [tcase addObject:@{@"username": @"conga@qgs.com",@"password":@"huhi"}];
    [tcase addObject:@{@"username": @"conga@qgs.com",@"password":@"huhi,;;"}];
    
    return tcase;
}
-(NSArray*)dataInputFail{
    NSMutableArray *tcase=[[NSMutableArray alloc] init];
    [tcase addObject:@{}];//nil 2value
    [tcase addObject:@{@"username": @"congacon"}];
    [tcase addObject:@{@"username": @"congacon@gmail.com"}];
    [tcase addObject:@{@"password": @"congacon@gmail.com"}];
    [tcase addObject:@{@"username": @"",@"password":@""}];
    [tcase addObject:@{@"username": @"conga",@"password":@""}];
    [tcase addObject:@{@"username": @"conga",@"password":@"con"}];
    [tcase addObject:@{@"username": @"conga@qgs.com",@"password":@""}];
    [tcase addObject:@{@"username": @"conga@qgs.com",@"password":@""}];

    
    return tcase;
}
//test input login include Username, Password

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//        for (int i=0 ;i<1000;i++) {
//            for (int j=0; j<1000; j++) {
//                int a=0;
//                a++;
//                a++;
//            }
//        }
//    }];
//}


@end
