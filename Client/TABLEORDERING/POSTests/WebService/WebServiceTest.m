//
//  WebServiceTest.m
//  POS
//
//  Created by Nha Duong Cong on 10/21/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HttpClient.h"

@interface WebServiceTest : XCTestCase

@end

@implementation WebServiceTest{
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testHTTPClient{
    NSDictionary *dataResponse = [[HttpClient sharedHttpClient] postRequestWithURL:@"http://congnhahoddoco.com" parameters:@{@"key": @"value"} headers:nil timeout:10];
    XCTAssert(([dataResponse[tkKeyReturnStatus] intValue]==ReturnCodeRequestError) || ([dataResponse[tkKeyReturnStatus] intValue]==ReturnCodeDataResponseEmpty), "url invalid but returncode not ReturnCodeRequestError");
    
//    NSDictionary *dict=[[HttpClient sharedHttpClient] postRequestWithURL:@"http://192.168.2.128/tkpos.ui.web/token" parameters:@{@"username": @"admin@smoovpos.com", @"password":@"Qgs@123!", @"grant_type": @"password"} headers:nil];
//    XCTAssert(([dict[tkKeyReturnStatus] intValue]==ReturnCodeSuccess) || ([dict[tkKeyReturnStatus] intValue]==ReturnCodeResponseInvalid) , "post request not success");
}
-(void)testHeaderAuthen{
    NSDictionary *dic=[[HttpClient sharedHttpClient] headerAuthenticationBasicWithUsername:@"abc@domain.com" password:@"ace3.3/4/$5"];
    if (![dic[@"Authorization"] isEqualToString:@"Basic YWJjQGRvbWFpbi5jb206YWNlMy4zLzQvJDU="]) {
        XCTAssert(NO, "hedeader Authen base64 fail");
    }
}
@end
