//
//  main.m
//  POS
//
//  Created by Nha Duong Cong on 10/13/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
