
//
//  SyncManager.m
//  SyncDemo
//
//  Created by Nguyen Anh Dao on 10/9/14.
//  Copyright (c) 2014 Nguyen Anh Dao. All rights reserved.
//
#define LEVEL_SNLOG_SYNC 200
#define DatabaseNameDefault @"bucket_sync"
#define FilterPushDefault @"filter-push-default"
#import "SyncManager.h"
#import "UserDefaultModel.h"
#import "SNLog.h"
#import "UtilBusinessModel.h"
#import "UIAlertView+Blocks.h"
#import "Constant.h"


@implementation SyncManager
@synthesize  status=_status;
@synthesize syncURL;
@synthesize pull;
@synthesize push;
+(SyncManager*)sharedSyncManager{
    __strong static SyncManager *_syncManager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _syncManager=[[SyncManager alloc] init];
        if ([_syncManager createTheManager] && [_syncManager createTheDatabase]) {
            [_syncManager setSyncRemoteDefault];
        }else{
            _syncManager=nil;
        };
    });
    if (_syncManager==nil) {
        NSLog(@"Nil SyncManager");
    }
    return _syncManager;
}

//creates the Replicartions


// creates the manager object
- (BOOL) createTheManager {
    
    // create a shared instance of CBLManager
    _manager = [CBLManager sharedInstance];
    if (!_manager) {
        NSLog (@"Cannot create shared instance of CBLManager");
        return NO;
    }

    NSLog (@"Manager created");
    
    return YES;
    
}

// creates the database
- (BOOL) createTheDatabase {
    NSError *error;
    // create a name for the database and make sure the name is legal
    if (![CBLManager isValidDatabaseName: DatabaseNameDefault]) {
        [SNLog Log:201 :@"Bad database name"];
        return NO;
    }
    // create a new database
    _database = [_manager databaseNamed: DatabaseNameDefault error: &error];
    if (!_database) {
        [SNLog Log:201 :@"Cannot create database. Error message: %@", error.localizedDescription ];
        return NO;
    }
    
    // log the database location
    NSString *databaseLocation = [[[[NSBundle mainBundle] bundlePath] stringByDeletingLastPathComponent] stringByAppendingString: @"/Library/Application Support/CouchbaseLite"];
    NSLog(@"Database %@ created at %@", DatabaseNameDefault, [NSString stringWithFormat:@"%@/%@%@", databaseLocation, DatabaseNameDefault, @".cblite"]);
    NSLog(@"Database %@ created",DatabaseNameDefault);
    
    return YES;
}
-(void)setSyncRemoteDefault{
    /*
     @"serverSync":@"http://192.168.2.201",
     @"databaseSync":@"sync_gateway",
     @"userAccessSync":@"congnha",
     @"passAccessSync":@"password",
     @"channels":@[@"default"],
     */
    NSString *urlString=[UtilBusinessModel stringUrlSyncData];
    id<CBLAuthenticator> auth=[UtilBusinessModel authenticationBasic];
    NSArray *channels=[UtilBusinessModel channelsSync];
    NSURL *urlRemote=[NSURL URLWithString:urlString];
    if (urlRemote && channels) {
        [self setSyncURL:urlRemote auth:auth channels:channels];
    }
}
- (void) setSyncURL: (NSURL*)serverDbURL auth:(id<CBLAuthenticator>)auth channels:(NSArray*) channelNames {// only set the one time
    if (syncURL) {
        return;
    }
    syncURL=serverDbURL;
    pull = [_database createPullReplication:syncURL];
    push = [_database createPushReplication:syncURL];
    pull.continuous = push.continuous = YES;
    pull.authenticator=push.authenticator=auth;
    pull.channels =channelNames;
//    _pull.channels = @[@"*"];
    
    //set filter push
    [self setFilterLocalOnly:push];
    
    // Observe replication progress changes, in both directions:
    NSNotificationCenter* nctr = [NSNotificationCenter defaultCenter];
    [nctr addObserver: self selector: @selector(replicationPullProgress:)
                 name: kCBLReplicationChangeNotification object: pull];
    [nctr addObserver: self selector: @selector(replicationPushProgress:)
                 name: kCBLReplicationChangeNotification object: push];
}
-(void)setFilterLocalOnly:(CBLReplication*)pushReplication_{
    if (pushReplication_==nil || pushReplication_.pull) {
        return;
    }
    // Stop Push Document if Document have key localOnly = true
    [_database setFilterNamed:FilterPushDefault asBlock:^BOOL(CBLSavedRevision *revision, NSDictionary *params) {
        BOOL localOnly=[revision.properties[tkKeyLocalOnly] boolValue];
        return ! localOnly;
    }];
    pushReplication_.filter=FilterPushDefault;
}
#pragma Remote PUSH
/*
 Use to filter when Push Data for function Sync
 Dao.Nguyen
 */
 
-(void)setRemotePushFilter:(NSString*)filterName param:(NSDictionary*)DicParam
{
    // Define a filter that matches only docs with a given "owner" property.
    // The value to match is given as a parameter named "name":
    [_database setFilterNamed:filterName asBlock: FILTERBLOCK({
        BOOL equal=YES;
        for (NSString *key in params) {
            if (![revision[key] isEqualToString:params[key]]) {
                equal=NO;
                break;
            }
        }
        return  equal;
    })];
    //
    // Set up a filtered push replication using the above filter block,
    // that will push only docs whose "owner" property equals "Waldo":
    //Example:
    /*
     push.filter = @"byOwner";
     push.filterParams = @{@"name": @"Waldo"};
     */
    push.filter = filterName;
    push.filterParams = DicParam;
  //  return  true;
}
-(void)startPull{
    if (!pull.running) {
        [pull start];
    }
}
-(void)stopPull{
    [pull stop];
}
-(void)startPush{
    if (!push.running) {
        [push start];
    }
}
-(void)stopPush{
    [push stop];
}
-(void)restartPull{
    if (pull.running) {
        [pull restart];
    }else{
        [pull start];
    }
}
-(void)restartPush{
    if (push.running) {
        [push restart];
    }else{
        [push start];
    }
}
-(void)resetSyncManager{
    [self stopPull];
    [self stopPush];
    [self deleteDatabaseAndWait:100];
    _database=nil;
    pull=nil;
    pull=nil;
    syncURL=nil;
    _syncing=NO;
    _showHUBwhileSyncing=NO;
    _delegate=nil;
    _progressHUBSync=nil;
    _completed=0;
    _total=0;
    _active=NO;
    _status=kCBLReplicationOffline;
    [self createTheDatabase];
}
-(void)deleteDatabaseAndWait:(long)usecond{
    if (_database) {
        NSError *error;
        [_database deleteDatabase:&error];
        usleep(usecond);// wait for databse deleted backgound
        if (error) {
            [SNLog Log:1000 :@"Delete Database Error"];
        }else{
            _database=nil;
        }
    }
}
-(void)replicationPullProgress:(NSNotificationCenter*)n{
    [self replicationProgress:n pull:YES];
}
-(void)replicationPushProgress:(NSNotificationCenter*)n{
    [self replicationProgress:n pull:NO];
}

//show startus progress sync and noti beginSync and endSync
- (void) replicationProgress: (NSNotificationCenter*)n pull:(BOOL)isPull{
    if (isPull) {
        NSLog(@"PULL %d ",pull.status);
    }else{
        NSLog(@"PUSH %d ",push.status);
    }
    if (pull.status == kCBLReplicationActive || push.status == kCBLReplicationActive) {
        // Sync is active -- aggregate the progress of both replications and compute a fraction:
        unsigned completed = pull.completedChangesCount + push.completedChangesCount;
        unsigned total = pull.changesCount+ push.changesCount;
        if (_active != YES|| completed != _completed || total != _total || _status != kCBLReplicationActive
            || _error) {
            _active = YES;
            _completed = completed;
            _total = total;
            _progress = (completed / (float)MAX(total, 1u));
            _status = kCBLReplicationActive;
        [SNLog Log:LEVEL_SNLOG_SYNC :@"SYNC progress: %u/%u", completed, total];
        }
        //check start syncing
        if (_syncing==NO) {
            _syncing=YES;
            [self beginSyncing];
        }
    }else{
        _active=false;
        _syncing=NO;
        _status=MAX(pull.status, push.status);
        [SNLog Log:LEVEL_SNLOG_SYNC :@"SYNC IDLE"];
        //noti
        if ((isPull && pull.status==kCBLReplicationIdle) || (!isPull && push.status==kCBLReplicationIdle)) {
            [SNLog Log:LEVEL_SNLOG_SYNC :@"Noti"];
            [[NSNotificationCenter defaultCenter] postNotificationName:tkNotiChangedDatabase object:nil userInfo:nil];
            //end syncing with success;
            [self endSyncing];
        }
        else if ((isPull && pull.status==kCBLReplicationStopped) || (!isPull && push.status==kCBLReplicationStopped)) {
            //end syncing with error;
            [self endSyncing];
        }
    }
    // Check for any change in error status and display new errors:
    NSError* error = pull.lastError ? pull.lastError : push.lastError;
    if (error != _error) {
        _error = error;
        if (error) {
            [SNLog Log:LEVEL_SNLOG_SYNC :@"Error Sync: %@",error];
        }
    }
}
-(void)beginSyncing{
    if (_delegate && [_delegate respondsToSelector:@selector(syncManagerBeginSyncing:)]) {
        [_delegate syncManagerBeginSyncing:self];
    }
    if (_showHUBwhileSyncing && _progressHUBSync !=nil) {
        [_progressHUBSync show:YES];
    }
}
-(void)endSyncing{
    if (_delegate && [_delegate respondsToSelector:@selector(syncManagerEndSyncing:pullError:pushError:)]) {
        [_delegate syncManagerEndSyncing:self pullError:pull.lastError pushError:push.lastError];
    }
    _delegate=nil;
    if (_progressHUBSync) {
        [_progressHUBSync hide:YES];
        [_progressHUBSync removeFromSuperview];
        _progressHUBSync=nil;
    }
    _showHUBwhileSyncing=NO;
}
-(CBLView *)viewWithName:(NSString *)viewName{
    return [_database viewNamed:viewName];
}
#pragma Creat View for Query
/*
 - titleView: name of View
 - titleKey: Key of type
 - titleCompare: key' name to compare
 - titleName: key to search
 
 */
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView titleKey:(NSString *)titleKey keyMain:(NSString *)keyMain{
  
//   [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        id date = [doc[titleKey] convertNullToNil];
        if (date)
        {
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue])
               emit(doc[keyMain], doc);
        }
    }) reduceBlock: nil version: @"1.0"];

    return [_database viewNamed:titleView];
}
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView titleKey:(NSString *)titleKey titleCompare:(NSString *)titleCompare keyMain:(NSString *)keyMain{
//    [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        id data = [doc[titleKey] convertNullToNil];
  
        if (data && [data isEqualToString:titleCompare])
        {
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue])
                emit(doc[keyMain], doc);
        }
    }) reduceBlock: nil version: @"1.0"];
    return [_database viewNamed:titleView];
}
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView compareMap:(NSDictionary *)compareMap keyMain:(NSString *)keyMain{
    
//    [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        bool valid=YES;
        for (NSString *key in compareMap.allKeys) {
            id data = [doc[key] convertNullToNil];
            if (![data isKindOfClass:[NSString class]] || ![data isEqualToString:[compareMap objectForKey:key]]) {
                valid=NO;
                break;
            }
        }
        if (valid)
        {
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue])
                emit(doc[keyMain], doc);
        }
    }) reduceBlock: nil version: @"1.0"];
    return [_database viewNamed:titleView];
}

- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView compareMapString:(NSDictionary *)compareMapString compareMapNumber:(NSDictionary *)compareMapNumber keyMain:(NSString *)keyMain{
    
    //    [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        bool valid=YES;
        for (NSString *key in compareMapString.allKeys) {
            id data = [doc[key] convertNullToNil];
            if (data==nil || ![data isEqualToString:[compareMapString objectForKey:key]]) {
                valid=NO;
                break;
            }
        }
        if (valid) {
            for (NSString *key in compareMapNumber.allKeys) {
                id data = [doc[key] convertNullToNil];
                if (data==nil || [data compare:[compareMapNumber objectForKey:key] ] != NSOrderedSame) {
                    valid=NO;
                    break;
                }
            }
        }
        if (valid)
        {
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue])
                emit(doc[keyMain], doc);
        }
    }) reduceBlock: nil version: @"1.0"];
    return [_database viewNamed:titleView];
}

- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView titleKey:(NSString *)titleKey titleCompare:(NSString *)titleCompare keySearch:(NSString*)keySearch{
    
    //    [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        // id data = doc[titleKey];
        id name = [doc[keySearch] convertNullToNil];
        if ([doc[titleKey] isEqualToString:titleCompare] ) {
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue])
                emit(CBLTextKey(name), doc);
        }
        
    }) reduceBlock: nil version: @"1.0"];
    return [_database viewNamed:titleView];
}
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView compareMap:(NSDictionary *)compareMap keySearch:(NSString *)keySearch{
    
    //    [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        // id data = doc[titleKey];
        id name = [doc[keySearch] convertNullToNil];
        
        bool valid=YES;
        for (NSString *key in compareMap.allKeys) {
            id data = [doc[key] convertNullToNil];
            if (![data isKindOfClass:[NSString class]] || ![data isEqualToString:[compareMap objectForKey:key]]) {
                valid=NO;
                break;
            }
        }
        if (valid) {
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue])
                emit(CBLTextKey(name), doc);
        }
        
    }) reduceBlock: nil version: @"1.0"];
    return [_database viewNamed:titleView];
}
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView compareMapString:(NSDictionary *)compareMapString compareMapNumber:(NSDictionary *)compareMapNumber keySearch:(NSString *)keySearch{
    
    //    [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        // id data = doc[titleKey];
        id name = [doc[keySearch] convertNullToNil];
        bool valid=YES;
        for (NSString *key in compareMapString.allKeys) {
            id data = [doc[key] convertNullToNil];
            if (![data isKindOfClass:[NSString class]] || ![data isEqualToString:[compareMapString objectForKey:key]]) {
                valid=NO;
                break;
            }
        }
        if (valid) {
            for (NSString *key in compareMapNumber.allKeys) {
                id data = [doc[key] convertNullToNil];
                if (data==nil || [data compare:[compareMapNumber objectForKey:key]]!=NSOrderedSame) {
                    valid=NO;
                    break;
                }
            }
        }
        if (valid) {
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue])
                emit(CBLTextKey(name), doc);
        }
        
    }) reduceBlock: nil version: @"1.0"];
    return [_database viewNamed:titleView];
}
- (CBLView *)viewListDocumentWithTitleView:(NSString *)titleView blockCondition:(BOOL (^)(NSDictionary *))condition keyMain:(NSString *)keyMain{
    
    //    [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        if (condition(doc))
        {
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue])
                emit(doc[keyMain], doc);
        }
    }) reduceBlock: nil version: @"1.0"];
    return [_database viewNamed:titleView];
}
-(CBLView *)viewListDocumentWithTitleView:(NSString *)titleView blockCondition:(BOOL (^)(NSDictionary *))condition keySearch:(NSString *)keySearch{
    
    //    [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        if (condition(doc))
        {   id name = [doc[keySearch] convertNullToNil];
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue])
                emit(CBLTextKey(name), doc);
        }
    }) reduceBlock: nil version: @"1.0"];
    return [_database viewNamed:titleView];
}
-(CBLView *)viewMapReduceWithTitleView:(NSString *)titleView blockCondition:(BOOL (^)(NSDictionary *))condition keyMain1:(NSString *)keyMain1 keyMain2:(NSString *)keyMain2{
    
    //    [self deleteViewWithViewName:titleView];
    [[_database viewNamed: titleView] setMapBlock: MAPBLOCK({
        if (condition(doc))
        {
            if([doc[tkKeyStatus] boolValue]&&[doc[tkKeyDisplay] boolValue]){
                if (keyMain2) {
                    emit(@[doc[keyMain1],doc[keyMain2]], doc);
                }else{
                    emit(doc[keyMain1], doc);
                }
            }
        }
    }) reduceBlock:^id(NSArray *keys, NSArray *values, BOOL rereduce) {
        if (rereduce) {
            return [CBLView totalValues:values];
        }else{
            return @(values.count);
        }
    } version:@"1.0"];
    return [_database viewNamed:titleView];
}

-(void)deleteViewWithViewName:(NSString *)viewName{
    if ([_database existingViewNamed:viewName]) {
        [[_database viewNamed:viewName] deleteView];
    }
}

#pragma Creat Query and return Array Result
-(NSArray *)queryListWithViewName:(NSString *)viewName{
    NSError *error;
    CBLQuery* query = [[_database viewNamed:viewName] createQuery] ;
    query.descending = YES;
    CBLQueryEnumerator *rowEnum = [query run: &error];
    if (error) {
        return nil;
    }else{
        NSMutableArray *array=[[NSMutableArray alloc] init];
        for (CBLQueryRow* row in rowEnum)
        {
            [array addObject:row.document];
        }
        return array;
    }
}
#pragma Background thread to call database
-(void)backgroundTellQueryWithViewName:(NSString *)viewName response:(void (^)(NSArray *))response{
    [_manager backgroundTellDatabaseNamed:DatabaseNameDefault to:^(CBLDatabase *db) {
        NSArray *list=[self queryListWithViewName:viewName];
        response(list);
    }];
}
-(void)backgroundTellWithBlock:(void (^)(CBLDatabase *))block{
    [_manager backgroundTellDatabaseNamed:DatabaseNameDefault to:block];
}
-(CBLDocument*)createDocument:(NSDictionary*) dicDocument
{
    CBLDocument* doc = [_database createDocument];
    if (dicDocument == nil) {
        return doc;
    }
    NSError* error;
    if (![doc putProperties: dicDocument error: &error]) {
        [self showError:error];
        [doc deleteDocument:&error];
        return nil;
    }
    return doc;
}
-(CBLDocument *)documentWithDocumentId:(NSString *)documentId{
    return [_database documentWithID:documentId];
}

-(CBLDocument *)createDocumentLocalOnly:(NSDictionary *)docData{
    NSMutableDictionary *data=[docData mutableCopy];
    [data setObject:@(true) forKey:tkKeyLocalOnly];
    return [self createDocument:data];
}
-(CBLDocument *)convertDocument:(CBLDocument *)document toLocalOnly:(BOOL)localOnly{
    return [self updateDocument:document propertyDoc:@{tkKeyLocalOnly:@(localOnly)}];
}
-(BOOL)deleteDocument:(CBLDocument *) doc
{
    NSError* error;
    if (![doc deleteDocument: &error])
        return false;
    
    return true;
}
-(BOOL)deleteDocumentWithDocumentID:(NSString *)docID{
    return [self deleteDocument:[_database documentWithID:docID]];
}
-(CBLDocument*) updateDocument:(CBLDocument *) doc propertyDoc:(NSDictionary*) dic
{
   
    //======= CU
    NSError* error;
    NSMutableDictionary *data =[doc.properties mutableCopy];
    [data addEntriesFromDictionary:dic];
    if (![doc putProperties: data error: &error])
        
    {
         NSLog(@"UPDATE DOCUMENT FAILED");
        return nil;
    }
     NSLog(@"UPDATE DOCUMENT SUCCESS ");
    return doc;
}
-(CBLDocument*)updateDocumentWithDocumentID:(NSString *)docID propertyDoc:(NSDictionary *)dic{
    return [self updateDocument:[_database documentWithID:docID] propertyDoc:dic];
}
-(BOOL) writeAttachImage:(CBLDocument *)doc withImage:(UIImage*)image
{
    NSData* imageData = UIImageJPEGRepresentation(image, 0.75);
    return [self writeAttachment:doc withData:imageData name:tkKeyImage contentType:@"image/jpeg"];
}
-(UIImage *) readingAttachImage:(CBLDocument *)doc
{
    CBLAttachment* att = [self readingAttachment:doc withName:tkKeyImage];
    UIImage* photo = nil;
    if (att != nil) {
        NSData* imageData = att.content;
        photo = [[UIImage alloc] initWithData: imageData];
        
    }
    return photo;
}
-(BOOL)removeAttachImage:(CBLDocument *)doc{
    return [self removeAttachmentWithDocument:doc withName:tkKeyImage];
}
-(BOOL)writeAttachment:(CBLDocument *)doc withData:(NSData *)data name:(NSString *)attName contentType:(NSString *)contentType{
    @try {
        CBLUnsavedRevision* newRev = [doc.currentRevision createRevision];
        [newRev setAttachmentNamed:attName withContentType:contentType content:data];
        NSError *error;
        assert([newRev save: &error]);
        if (error) {
            return NO;
        }else{
            return YES;
        }
    }
    @catch (NSException *exception) {
        return NO;
    }
}
-(CBLAttachment *)readingAttachment:(CBLDocument *)doc withName:(NSString *)attName{
    CBLRevision* rev = doc.currentRevision;
    CBLAttachment* att = [rev attachmentNamed: attName];
    return att;
}
-(NSArray *)allAttachmentsWithDocument:(CBLDocument *)doc{
    return [[doc currentRevision] attachments];
}
-(BOOL)removeAttachmentWithDocument:(CBLDocument *)doc withName:(NSString *)attName{
    // Remove an attachment from a document:
    CBLUnsavedRevision* newRev = [doc.currentRevision createRevision];
    [newRev removeAttachmentNamed: attName];
    // (You could also update newRev.properties while you're here)
    NSError* error;
    assert([newRev save: &error]);
    return error == nil;
}
-(BOOL)removeAllAttachmentsWithDocument:(CBLDocument *)doc{
    // remove all attachent of document.
    // if have any attachment remove error then stop progress and return NO else return YES
    NSArray *listAttachments=[self allAttachmentsWithDocument:doc];
    for (CBLAttachment *att in listAttachments) {
        if (![self removeAttachmentWithDocument:doc withName:att.name]) {
            return NO;
        }
    }
    return YES;
}
//demo
-(void)demo{
/*
    NSString *titleV=@"testSomeProperty";
    [[_database viewNamed: titleV] setMapBlock: MAPBLOCK({
        // id data = doc[titleKey];
        if (doc[@"table"]) {
            //             if(![doc[@"_deleted"] boolValue])
            emit(CBLTextKey(doc[@"table"]),doc[@"table"]);
        }
        
    }) reduceBlock: nil version: @"1.0"];
    CBLQuery* query = [_database createAllDocumentsQuery];//[[_database viewNamed:titleV] createQuery] ;
    query.descending = YES;
    query.limit=100;
    query.skip=0;
    NSMutableArray *total=[[NSMutableArray alloc] init];
    BOOL done=NO;
    while (!done) {
        query.skip=total.count;
        NSError *error;
        CBLQueryEnumerator *rowEnum=[query run:&error];
        if (!error) {
            NSMutableArray *array=[[NSMutableArray alloc] init];
            for (CBLQueryRow* row in rowEnum)
            {
                [array addObject:row.documentID];
            }
            if (array.count<100) {
                done=YES;
            }
 
        }
    }
 */

    
    
    [_manager backgroundTellDatabaseNamed:DatabaseNameDefault to:^(CBLDatabase *database) {
        
        [[_database viewNamed:@"demoView"]setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
            emit(doc[@"name"],doc[@"_id"]);
        } reduceBlock:^id(NSArray *keys, NSArray *values, BOOL rereduce) {
            if (rereduce) {
                return [CBLView totalValues:values];
            }else{
                NSLog(@"RED %i",values.count);
                return @(values.count);
            }
        } version:@"2"];
        CBLView *viewDemo=[_database viewNamed:@"demoView"];
        CBLQuery *query= [viewDemo createQuery];
        query.limit=2;
        
        CBLQueryEnumerator *en=[query run:nil];
        NSLog(@"%i",en.count);
        
        NSMutableArray *array=[[NSMutableArray alloc] init];
                NSMutableArray *array2=[[NSMutableArray alloc] init];
        for (CBLQueryRow *row in en) {

            NSLog(@"%@ %@",row.document,row.documentID);
            if (row.document) {
                        [array addObject:row.document];
            }


        }
        
        for (CBLQueryRow *row in en) {
            if (row.document) {
                [array2 addObject:row.document];
            }
            
        }
        NSLog(@"%i%i%i",array.count,array2.count,en.count);
    }];
}

-(void)showError:(NSError*)error{
    NSLog(@"%@",error.localizedDescription);
}
-(void)showMessage:(NSString*)string{
    NSLog(@"%@",string);
}


#pragma Search Product With Key
-(NSArray*) searchProductWithView:(CBLView*) viewSearch titleKey:(NSString*)keyName
{
    NSMutableArray *arrSearch = [[NSMutableArray alloc] init];
    if (_database) {
        if(viewSearch)
        {
            NSError *error;
            CBLQuery* queryFirstName = [viewSearch createQuery];
            queryFirstName.fullTextQuery = [NSString stringWithFormat:@"%@*",keyName];
            queryFirstName.fullTextSnippets = YES;   // enables snippets; see next example
            queryFirstName.fullTextRanking = YES;
            queryFirstName.limit = 10;
//            queryFirstName.skip = 0;
            CBLQueryEnumerator *rowQuery = [queryFirstName run: &error];
            for (CBLQueryRow* row in rowQuery)
            {
                CBLDocument *doc = row.document;
                NSLog(@" PRODUCT Search is: %@", doc.properties);
                [arrSearch addObject:doc];
            }
            return arrSearch;
            
        }
        return nil;
    }
    else{
        return nil;
    }
    return arrSearch;
    
}
-(void)test{
    for (int i=0; i<000; i++) {
        int rand=arc4random()%100;
        int rand2=arc4random()%100;
        NSString *st=[NSString stringWithFormat:@"%d",rand];
        NSString *st2=[NSString stringWithFormat:@"%d",rand2];
        NSDictionary *dic=@{@"key":st,@"key2":st2,@"value":@(i),@"table":@"TEST"};
        [self createDocument:dic];
    }
    [self deleteViewWithViewName:@"testView"];
    [[_database viewNamed:@"testView"] setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
        NSString *table=[doc[@"table"] convertNullToNil];
        if ([table isEqualToString:@"TEST"]) {
            NSString *keySearch1=[NSString stringWithFormat:@"%d",[doc[@"key"] integerValue]];
            //NSString *keySearch2=[NSString stringWithFormat:@"%d",[doc[@"key2"] integerValue]];
            emit(CBLTextKey(keySearch1),doc[@"value"]);
        }
    } reduceBlock:nil version:@"1.0"];
    CBLQuery *queryAll=[_database createAllDocumentsQuery];
    CBLQueryEnumerator *en=[queryAll run:nil];
    NSLog(@"%d",en.count);
    CBLView *view=[_database viewNamed:@"testView"];
    CBLQuery *query=[view createQuery];
    query.fullTextQuery=@"*";
    query.fullTextSnippets=NO;
    query.fullTextRanking = YES;
    query.descending=NO;
    query.prefetch=YES;
    CBLQueryEnumerator *enn=[query run:nil];
    NSLog(@"%d",enn.count);
    for (CBLQueryRow *row  in enn) {
        NSLog(@"%@ %@",row.documentProperties[@"key"],row.documentProperties[@"key2"]);
    }
}
@end
