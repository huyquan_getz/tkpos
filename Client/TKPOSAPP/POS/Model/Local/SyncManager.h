//
//  SyncManager.h
//
//  Created by Nguyen Anh Dao on 10/9/14.
//  Copyright (c) 2014 Nguyen Anh Dao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "MBProgressHUD.h"


@class SyncManager;
@protocol SyncManagerDelegate <NSObject>
@optional
-(void)syncManagerBeginSyncing:(id)syncManager;
-(void)syncManagerEndSyncing:(id)syncManager pullError:(NSError*)pullE pushError:(NSError*)pushE;
@end
@interface SyncManager : NSObject{
    CBLReplication *pull;
    CBLReplication *push;
    NSURL* syncURL;
}
+(SyncManager*)sharedSyncManager;
@property (readonly) CBLDatabase *database;
@property (readonly) CBLManager *manager;
//
@property (weak,nonatomic) id delegate;
@property (strong,nonatomic) MBProgressHUD *progressHUBSync;
@property (assign) BOOL showHUBwhileSyncing; //folow show hub while syncing, set to NO when complete syncing
// These are not KVO-observable; observe SyncManagerStateChangedNotification instead
@property (nonatomic, readonly) unsigned completed, total;
@property (nonatomic, readonly) float progress;
@property (nonatomic, readonly) bool active;
@property (nonatomic, readonly) CBLReplicationStatus status;
@property (nonatomic, readonly) NSError* error;
@property (assign) BOOL syncing;

@property (readonly) CBLReplication *pull;
@property (readonly) CBLReplication *push;
@property (readonly) NSURL* syncURL;

- (void)setSyncRemoteDefault;
- (BOOL) createTheDatabase;

/*View was existed forever in database avoid you delete them
 If view have Parameter available , You need deleted view before you create view and delete when query done for than better
 */
- (CBLView*)viewWithName:(NSString*)viewName;
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView titleKey:(NSString *)titleKey keyMain:(NSString*)keyMain;
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView titleKey:(NSString *)titleKey titleCompare:(NSString *)titleCompare keyMain:(NSString*)keyMain;
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView compareMapString:(NSDictionary *)compareMapString compareMapNumber:(NSDictionary *)compareMapNumber keyMain:(NSString *)keyMain;
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView compareMap:(NSDictionary *)compareMap keyMain:(NSString*)titleKey;
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView titleKey:(NSString *)titleKey titleCompare:(NSString *)titleCompare keySearch:(NSString*)keySearch;
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView compareMap:(NSDictionary *)compareMap keySearch:(NSString*)keySearch;
- (CBLView*)viewListDocumentWithTitleView:(NSString *)titleView compareMapString:(NSDictionary *)compareMapString compareMapNumber:(NSDictionary *)compareMapNumber keySearch:(NSString*)keySearch;
- (CBLView*)viewListDocumentWithTitleView:(NSString*)titleView blockCondition:(BOOL (^)(NSDictionary *doc))condition keyMain:(NSString*)keyMain;
- (CBLView*)viewListDocumentWithTitleView:(NSString*)titleView blockCondition:(BOOL (^)(NSDictionary *doc))condition keySearch:(NSString*)keySearch;

- (CBLView*)viewMapReduceWithTitleView:(NSString*)titleView blockCondition:(BOOL (^)(NSDictionary *doc))condition keyMain1:(NSString*)keyMain1 keyMain2:(NSString*)keyMain2;

-(CBLDocument*)updateDocument:(CBLDocument *) doc propertyDoc:(NSDictionary*) dic;
-(CBLDocument*)updateDocumentWithDocumentID:(NSString *) docID propertyDoc:(NSDictionary*) dic;
-(BOOL)deleteDocument:(CBLDocument *) doc;
-(BOOL)deleteDocumentWithDocumentID:(NSString *) docID;
-(BOOL) writeAttachImage:(CBLDocument *)doc withImage:(UIImage*)image;// default name is "image"
-(UIImage *) readingAttachImage:(CBLDocument *)doc; //default name is "image"
-(BOOL) removeAttachImage:(CBLDocument*)doc;//default name is "image"

-(BOOL) writeAttachment:(CBLDocument *)doc withData:(NSData*)data  name:(NSString*)attName contentType:(NSString*)contentType;
-(CBLAttachment *) readingAttachment:(CBLDocument *)doc withName:(NSString*)attName;
-(NSArray*)allAttachmentsWithDocument:(CBLDocument*)doc;
- (BOOL) removeAttachmentWithDocument:(CBLDocument*)doc withName:(NSString*)attName;
-(BOOL)removeAllAttachmentsWithDocument:(CBLDocument*)doc;
-(CBLDocument*)createDocument:(NSDictionary*) docData;
-(CBLDocument*)documentWithDocumentId:(NSString*)documentId;
-(NSArray *)queryListWithViewName:(NSString *)viewName;
-(void) deleteViewWithViewName: (NSString *) titleView;

//call request with view in background when database free
-(void)backgroundTellQueryWithViewName:(NSString*)viewName response:(void (^)(NSArray *list))response;
//call block in background when database free
-(void)backgroundTellWithBlock:(void(^)(CBLDatabase *database))block;
//create document and property;
-(CBLDocument*)createDocumentLocalOnly:(NSDictionary*) docData;
-(CBLDocument*)convertDocument:(CBLDocument*)document toLocalOnly:(BOOL)localOnly;
-(void)startPull;
-(void)stopPull;
-(void)startPush;
-(void)stopPush;
-(void)restartPull;
-(void)restartPush;
-(void)resetSyncManager;// resetDatabase, pull, push;
-(void)deleteDatabaseAndWait:(long)usecond;
-(void)demo;

#pragma Filter PUSH
-(void)setRemotePushFilter:(NSString*)filterName param:(NSDictionary*)DicParam;
-(NSArray*) searchProductWithView:(CBLView*) viewSearch titleKey:(NSString*)keyName;
@end