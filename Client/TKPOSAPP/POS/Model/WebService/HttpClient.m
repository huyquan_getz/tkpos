//
//  HttpClient.m
//  POS
//
//  Created by Nha Duong Cong on 10/21/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define HTTP_LEVEL_SNLOG 100
#define TIME_DEFAULT_FIRST 30

#import "HttpClient.h"
#import "ASIFormDataRequest.h"
#import "ASIHTTPRequest.h"
#import "UserDefaultModel.h"
#import "NSString+SBJSON.h"
#import "NSObject+SBJson.h"
#import "SNLog.h"

@implementation HttpClient
+(HttpClient *)sharedHttpClient{
    __strong static HttpClient *_httpClient;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _httpClient =[[HttpClient alloc] init];
    });
    return _httpClient;
}
-(id)init{
    if(self =[super init]){
        _timeoutDefault=TIME_DEFAULT_FIRST;
    }
    return self;
}
-(NSDictionary *)postRequestWithURL:(NSString *)url parameters:(NSDictionary *)parameters headers:(NSDictionary *)headers{
    return [self postRequestWithURL:url parameters:parameters headers:headers timeout:_timeoutDefault];
}
-(NSDictionary *)postRequestWithURL:(NSString *)url parameters:(NSDictionary *)parameters headers:(NSDictionary *)headers timeout:(int)timeout{
    return [self requestWithURL:url method:@"POST" parameters:parameters headers:headers timeout:timeout];
}
-(NSDictionary *)requestWithURL:(NSString *)url method:(NSString *)method parameters:(NSDictionary *)parameters headers:(NSDictionary *)headers timeout:(int)timeout{
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@" "];
    NSString *url_ = [[url componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString:@"%20"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url_]];
    [request setTimeOutSeconds:timeout];
    [request setRequestMethod:method];
    //add headers
    if (headers) {
        for (NSString *keyHeader in headers.allKeys) {
            id valueHeader=[headers objectForKey:keyHeader];
            [request addRequestHeader:keyHeader value:valueHeader];
        }
    }
    //add parameters
    if (parameters) {
        for (NSString *keyParameter in parameters.allKeys) {
            id value = [parameters objectForKey:keyParameter];
            [request setPostValue:value forKey:keyParameter];
        }
    }
    
    [request startSynchronous];
    NSError *error = [request error];
    
    if (!error) {
        NSString *responseString=[request responseString];
        if (responseString==nil) {
            [SNLog Log:HTTP_LEVEL_SNLOG :@"Response String of Request Nil"];
            return [self dataResponseEmpty];
        }
        id responseJSONObject=nil;
        @try {
            responseJSONObject = [responseString JSONValue];
        }
        @catch (NSException *exception) {
            [SNLog Log:HTTP_LEVEL_SNLOG :@"Parse Response Exception: %@",exception];
            [SNLog Log:HTTP_LEVEL_SNLOG :@"String Parse Error :\"%@\"",responseString];
        }
        
        if (responseJSONObject == nil) {
            [SNLog Log:HTTP_LEVEL_SNLOG :@"JSON Object Error" ];
            return [self dataResponseParseStringErrorWithString:responseString];
        }
        if ([self checkJSONObjectExceptionWithObject:responseJSONObject]) {
            [SNLog Log:HTTP_LEVEL_SNLOG :@"Response with Exception:%@",responseJSONObject[tkKeyException]];
            return [self dataResponseExceptionWithJSONObject:responseJSONObject];
        }
        //check valid of response
        if (![self checkMyJSONObjectWithObject:responseJSONObject]) {
            [SNLog Log:HTTP_LEVEL_SNLOG :@"JSON Object Is Invalid"];
            [SNLog Log:HTTP_LEVEL_SNLOG :@"JSON DATA: %@",responseJSONObject];
            return [self dataResponseInvalidWithJSONObject:responseJSONObject];
        }
        
        return responseJSONObject;
        
    }else{
        //error request
        [SNLog Log:HTTP_LEVEL_SNLOG :@"Request Error: %@",error];
        return [self dataResponseRequestError];
    }
}
-(BOOL)checkJSONObjectExceptionWithObject:(id)object{
    return [object isKindOfClass:[NSDictionary class]] && object[tkKeyException];
}
-(BOOL)checkMyJSONObjectWithObject:(id)object{
    //chect object is valid: is NSDictionary, have "returnCode" and "data" keys
    return [object isKindOfClass:[NSDictionary class]] && object[tkKeyReturnStatus];
}
-(NSDictionary*)dataResponseEmpty{
    return @{tkKeyReturnStatus: [NSNumber numberWithInt:ReturnCodeDataResponseEmpty]};
}
-(NSDictionary*)dataResponseParseStringErrorWithString:(NSString*)responseString{
    return @{tkKeyReturnStatus: [NSNumber numberWithInt:ReturnCodeParseStringError],tkKeyResponseString:responseString};
}
-(NSDictionary*)dataResponseRequestError{
    return @{tkKeyReturnStatus: [NSNumber numberWithInt:ReturnCodeRequestError]};
}
-(NSDictionary*)dataResponseInvalidWithJSONObject:(id)jsonObject{
    return @{tkKeyReturnStatus: [NSNumber numberWithInt:ReturnCodeResponseInvalid],tkKeyResult:jsonObject};
}
-(NSDictionary*)dataResponseExceptionWithJSONObject:(id)jsonObject{
    return @{tkKeyReturnStatus: [NSNumber numberWithInt:ReturnCodeExceiption],tkKeyResult:jsonObject};
}
-(NSDictionary *)headerAuthenticationBasicWithUsername:(NSString *)username password:(NSString *)password{
    NSString *tokenBase64=[ASIHTTPRequest base64forData:[[NSString stringWithFormat:@"%@:%@",username,password] dataUsingEncoding:NSUTF8StringEncoding]];
    return   @{@"Authorization" :[NSString stringWithFormat:@"Basic %@",tokenBase64]};
}
@end
