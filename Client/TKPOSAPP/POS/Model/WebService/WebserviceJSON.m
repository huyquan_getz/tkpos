//
//  WebserviceJSON.m
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "WebserviceJSON.h"
#import "HttpClient.h"
#import "Constant.h"
#import "UserDefaultModel.h"
#import "TKOrder.h"
#import "Controller.h"

@implementation WebserviceJSON
+(NSDictionary *)authenticationWithBusinessName:(NSString *)businessName username:(NSString *)username password:(NSString *)password{
    NSDate *dateRequest=[NSDate date];
    NSMutableDictionary *dict=[[[HttpClient sharedHttpClient] postRequestWithURL:tkWebServiceAuthenUserPass parameters:@{@"businessName": businessName, @"userName":username, @"password": password} headers:nil] mutableCopy];
    [dict setObject:[CBLJSON JSONObjectWithDate:dateRequest] forKey:tkKeyRequestDate];
    if ([dict[tkKeyReturnStatus] integerValue]==ReturnCodeSuccess) {
        return [self expectedResponseAuthen:dict];
    }else{
        return dict;
    }
}
+(NSDictionary *)getTokenWithBusinessName:(NSString *)businessName username:(NSString *)username password:(NSString *)password{
    NSDate *dateRequest=[NSDate date];
    NSMutableDictionary *dict=[[[HttpClient sharedHttpClient] postRequestWithURL:tkWebServiceAuthenUserPass parameters:@{@"businessName": businessName, @"userName":username, @"password": password} headers:nil] mutableCopy];
    [dict setObject:[CBLJSON JSONObjectWithDate:dateRequest] forKey:tkKeyRequestDate];
    if ([dict[tkKeyReturnStatus] integerValue]==ReturnCodeSuccess) {
        NSDictionary *data=dict[tkKeyResult];
        long expiredTimeMili=[data[@"expires_in"] longValue];
        NSDate *dateExpired=[NSDate dateWithTimeInterval:expiredTimeMili/1000 sinceDate:dateRequest];
        NSDictionary *tokenInfo=@{tkKeyAccessToken:data[tkKeyAccessToken],
                                  @"expires_in":data[@"expires_in"],
                                  tkKeyExpiredTokenDate:[CBLJSON JSONObjectWithDate:dateExpired]};
        [dict setObject:tokenInfo forKey:tkKeyResult];
    }
    return dict;
}
+(NSDictionary*)expectedResponseAuthen:(NSDictionary*)response{
    NSMutableDictionary *newResponse=[response mutableCopy];
    NSMutableDictionary *newData=[response[tkKeyResult] mutableCopy];
    
    NSDate *dateRequest=[CBLJSON dateWithJSONObject:response[tkKeyRequestDate]];
    NSDictionary *dictUpdate=@{@"userAccessSync":@"smoov",@"passAccessSync":@"smoov@123"};
    //check server sync
    NSString *serverSync=newData[@"serverSync"];
    if ([serverSync hasPrefix:@"http://"] || [serverSync hasPrefix:@"https://"] ) {
        //
    }else{
        serverSync =[@"http://" stringByAppendingString:serverSync];
    }
    [newData setObject:serverSync forKey:@"serverSync"];
    
    NSArray *channels=@[newData[@"merchantID"],
                        [NSString stringWithFormat:@"%@_pos",newData[@"merchantID"]]];
    [newData setObject:channels forKey:@"channels"];
    //check port sync
    if ([newData[@"portSync"] convertNullToNil]==nil) {
        [newData setObject:@"4984" forKey:@"portSync"];
    }
    [newData addEntriesFromDictionary:dictUpdate];
    long expiredTimeMili=[newData[@"expires_in"] longValue];
    NSDate *dateExpired=[NSDate dateWithTimeInterval:expiredTimeMili/1000 sinceDate:dateRequest];
    [newData setObject:[CBLJSON JSONObjectWithDate:dateExpired] forKey:tkKeyExpiredTokenDate];
    
    [newResponse setObject:newData forKey:tkKeyResult];
    return newResponse;
}
+(NSDictionary *)sendReceipt:(NSDictionary *)receipt toEmail:(NSString *)email type:(MailType)mailType{
    return [self sendReceipt:receipt toEmail:email type:mailType countLoop:0];
}
+(NSDictionary *)sendReceipt:(NSDictionary *)receipt toEmail:(NSString *)email type:(MailType)mailType  countLoop:(NSInteger)countLoop{
    NSMutableDictionary *param=[[NSMutableDictionary alloc] init];
    NSString *urlSendMail;
    
    NSDictionary *dataSync=[UserDefaultModel getMerchantSyncInfor];
    [param setObject:[self stringEmailTypeWithEmailType:mailType] forKey:@"typeEmail"];
    [param setObject:email forKey:@"emailCustomer"];
    [param setObject:dataSync[tkKeyBusinessName] forKey:@"businessName"];
    switch (mailType) {
        case MailTypeCollected:
        case MailTypeReSendCollected:
        case MailTypeReSendCancel:
        case MailTypeReSendRefund:{
            
            urlSendMail=tkWebServiceSendMailReceipt;
            [param setObject:[CBLJSON stringWithJSONObject:receipt options:0 error:nil] forKey:@"stringOrder"];
            [param setObject:dataSync[tkKeyBusinessName] forKey:@"businessName"];
            [param setObject:email forKey:@"emailCustomer"];
            [param setObject:receipt[tkKeyOrderStatus] forKey:@"status"];
            
            break;
        }
        case MailTypeReady:
        case MailTypeRefund:
        case MailTypeCancel:
        case MailTypeRejectCancel:{
            urlSendMail=tkWebServiceSendMailChangeStatus;
            NSMutableDictionary *data=[[NSMutableDictionary alloc] init];
            [data setObject:receipt[tkKeyOrderCode] forKey:@"OrderNumber"];
            [data setObject:dataSync[tkKeyBusinessName] forKey:@"BusinessName"];
            if (mailType == MailTypeRefund) {
                [data setObject:[receipt[tkKeyRefundInfo] objectForKey:tkKeyReasonRefund] forKey:@"Reason"];
            }
            [data setObject:email forKey:@"EmailCustomer"];
            [data setObject:[receipt[tkKeyCustomerReceiver] objectForKey:tkKeyFirstName] forKey:@"FirstName"];
            NSString *stringData=[CBLJSON stringWithJSONObject:data options:0 error:nil];
            [param setObject:stringData forKey:tkKeyResult];
            break;
        }
            
        case MailTypeReportCashDrawer:{
            urlSendMail=tkWebServiceSendMailCashDrawerReport;
            NSString *stringData=[CBLJSON stringWithJSONObject:receipt options:0 error:nil];
            [param setObject:stringData forKey:tkKeyResult];
            break;
        }
        default:
            break;
    }
    if (urlSendMail) {
        NSDate *expiredTokenDate=[CBLJSON dateWithJSONObject:dataSync[tkKeyExpiredTokenDate]];
        if (expiredTokenDate && [expiredTokenDate compare:[NSDate date]]==NSOrderedAscending) {
            [Controller resetToken];
        }
        [param setObject:dataSync[tkKeyAccessToken] forKey:tkKeyAuthorization];
        NSDictionary *dict=[[HttpClient sharedHttpClient] postRequestWithURL:urlSendMail parameters:param headers:nil];
        if ((([dict[tkKeyReturnStatus] integerValue]== ReturnCodeAccessTokenExpired)||([dict[tkKeyReturnStatus] integerValue]== ReturnCodeAccessTokenNotProvided)) && countLoop  < 1) {
            [Controller resetToken];
            return [self sendReceipt:receipt toEmail:email type:mailType countLoop:countLoop+1];
        }else{
            return dict;
        }
    }
    return @{tkKeyReturnStatus:@(ReturnCodeRequestError)};
}
+(NSString*)stringEmailTypeWithEmailType:(MailType)typeEmail{
    switch (typeEmail) {
        case MailTypeRejectCancel:
            return @"Reject";
            break;
        case MailTypeCancel:
            return @"Cancel";
            break;
        case MailTypeRefund:
            return @"Refund";
            break;
        case MailTypeReady:
            return @"Ready";
            break;
        case MailTypeCollected:
            return @"Receipt";
            break;
        case MailTypeReSendCollected:
            return @"ReceiptResend";
            break;
        case MailTypeReSendCancel:
            return @"ReceiptCancelResend";
            break;
        case MailTypeReSendRefund:
            return @"ReceiptRefundResend";
            break;
        case MailTypeReportCashDrawer:
            return @"CashDrawerReport";
            break;
        default:
            break;
    }
    return @"";
}
@end
