//
//  PrintData.m
//  POS
//
//  Created by Hoang Van Quynh on 13/01/2015.
//  Copyright (c) Năm 2015 Nha Duong Cong. All rights reserved.
//


#import "PrintData.h"

@implementation PrintData
- (id)initWithImage:(UIImage*)image Height:(float)height Ip:(NSString*)ip Type:(int)type Name:(NSString*)name Infor:(NSDictionary*)dictInfor{
    self = [super init];
    if (self)
    {
        NSString *urlImageLocal = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%i%f",arc4random() % 100,CACurrentMediaTime()]];
        NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(image)];
        [data1 writeToFile:urlImageLocal atomically:YES];
        _strImage=urlImageLocal;
        _height=[NSString stringWithFormat:@"%f", height];
        _ip=ip;
        _infor=@"";
        _type=[NSString stringWithFormat:@"%i",type];
        _name=name;
        _status=@"0";
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.strImage forKey:@"strImage"];
    [aCoder encodeObject:self.width forKey:@"width"];
    [aCoder encodeObject:self.height forKey:@"height"];
    [aCoder encodeObject:self.ip forKey:@"ip"];
    [aCoder encodeObject:self.infor forKey:@"infor"];
    [aCoder encodeObject:self.type forKey:@"type"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.status forKey:@"status"];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        _strImage = [aDecoder decodeObjectForKey:@"strImage"];
        _width = [aDecoder decodeObjectForKey:@"width"];
        _height = [aDecoder decodeObjectForKey:@"height"];
        _ip = [aDecoder decodeObjectForKey:@"ip"];
        _infor = [aDecoder decodeObjectForKey:@"infor"];
        _type = [aDecoder decodeObjectForKey:@"type"];
        _name = [aDecoder decodeObjectForKey:@"name"];
        _status = [aDecoder decodeObjectForKey:@"status"];
        
    }
    return self;
}
@end