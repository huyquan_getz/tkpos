//
//  ShiftDrawer.m
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define ClosingCashNotFound -1
#import "ShiftDrawer.h"
#import "ActitivityDrawer.h"

@implementation ShiftDrawer
@synthesize modifiedDate,closed,closedDate,closingCash,startedDate,startingCash,document,currency;
-(instancetype)initWithDocument:(CBLDocument *)shiftDrawer{
    if (self =[super init]) {
        document=shiftDrawer;
        self =[self initWithJSON:shiftDrawer.properties];
    }
    return self;
}
-(NSArray *)arrayActivity{
    return [arrayActivity copy];
}
-(instancetype)initWithStartingCash:(double)startingCash_ currency:(NSString *)currency_{
    if (self =[super init]) {
        startingCash=startingCash_;
        closingCash=ClosingCashNotFound;
        startedDate=[NSDate date];
        modifiedDate=[startedDate copy];
        closedDate=tkNoLimitDate;
        arrayActivity =[[NSMutableArray alloc] init];
        closedDate=NO;
        currency=currency_;
        document=[[SyncManager sharedSyncManager] createDocumentLocalOnly:[self encodeAsJSON]];
        [SNLog Log:@"dr %@",document.properties];
    }
    return self;
}
-(void)saveToDocument{
    [[SyncManager sharedSyncManager] updateDocument:document propertyDoc:[self encodeAsJSON]];
}
-(id)encodeAsJSON{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setObject:[CBLJSON JSONObjectWithDate:startedDate] forKey:@"startedDate"];
    if (closedDate) {
        [dict setObject:[CBLJSON JSONObjectWithDate:closedDate] forKey:tkKeyClosedDate];
    }else{
        NSDate *noCloseDate=tkNoLimitDate;
        [dict setObject:[CBLJSON JSONObjectWithDate:noCloseDate] forKey:tkKeyClosedDate];
    }
    [dict setObject:@(startingCash) forKey:@"startingCash"];
    [dict setObject:@(closingCash) forKey:@"closingCash"];
    NSMutableArray *jsonArrayActivity=[[NSMutableArray alloc] init];
    for (ActitivityDrawer *activity in arrayActivity) {
        [jsonArrayActivity addObject:[activity encodeAsJSON]];
    }
    [dict setObject:currency forKey:@"currency"];
    [dict setObject:jsonArrayActivity forKey:@"arrayActivity"];
    [dict setObject:@(closed) forKey:@"closed"];
    [dict setObject:[CBLJSON JSONObjectWithDate:modifiedDate] forKey:tkKeyModifiedDate];
    [dict setObject:tkShiftDrawerTable forKey:tkKeyTable];
    
    [dict setObject:@(YES) forKey:tkKeyStatus];
    [dict setObject:@(YES) forKey:tkKeyDisplay];
    return dict;
}
-(instancetype)initWithJSON:(id)jsonObject{
    if (self =[super init]) {
        NSDictionary *json=(NSDictionary*)jsonObject;
        arrayActivity=[[NSMutableArray alloc] init];
        NSArray *jsonArrayActivity=json[@"arrayActivity"];;
        for (NSDictionary *jsonActivity in jsonArrayActivity) {
            [arrayActivity addObject:[[ActitivityDrawer alloc] initWithJSON:jsonActivity]];
        }
        startedDate=[CBLJSON dateWithJSONObject:json[@"startedDate"]];
        if ([json[tkKeyModifiedDate] convertNullToNil]) {
            closedDate=[CBLJSON dateWithJSONObject:json[tkKeyClosedDate]];
        }
        startingCash=[json[@"startingCash"] doubleValue];
        closingCash=[json[@"closingCash"] doubleValue];
        closed =[json[@"closed"] boolValue];
        currency=json[@"currency"];
        modifiedDate=[CBLJSON dateWithJSONObject:json[tkKeyModifiedDate]];
    }
    return self;
}
-(void)addActitvity:(ActitivityDrawer *)activityDrawer{
    [arrayActivity insertObject:activityDrawer atIndex:0];
    modifiedDate=[NSDate date];
    [self saveToDocument];
}
-(void)closeDrawerWithClosingCash:(double)closingCash_{
    closingCash=closingCash_;
    closed=YES;
    closedDate=[NSDate date];
    modifiedDate=closedDate;
    [self saveToDocument];
}
-(void)closeDrawerNotClosingCashWithDate:(NSDate*)closedDate_{
    closedDate=closedDate_;
    if (closedDate==nil) {
        closedDate =modifiedDate;
    }else{
        modifiedDate=closedDate;
    }
    closed=YES;
    [self saveToDocument];
}
-(NSString *)documentId{
    return document.documentID;
}

-(double)totalCashInWithType:(CashInType)cashInType_{
    double total=0;
    for (ActitivityDrawer *activity in arrayActivity) {
        if (activity.cashInType == cashInType_) {
            total+=activity.cashInValue;
        }
    }
    return total;
}
-(double)totalCashOutWithType:(CashOutType)cashOutType_{
    double total=0;
    for (ActitivityDrawer *activity in arrayActivity) {
        if (activity.cashOutType==cashOutType_) {
            total+=activity.cashOutValue;
        }
    }
    return total;
}
-(double)totalCashIn{
    double total=0;
    for (ActitivityDrawer *activity in arrayActivity) {
        if (activity.cashInType != CashInTypeNone) {
            total+=activity.cashInValue;
        }
    }
    return total;
}
-(double)totalCashOut{
    double total=0;
    for (ActitivityDrawer *activity in arrayActivity) {
        if (activity.cashOutType != CashOutTypeNone) {
            total+=activity.cashOutValue;
        }
    }
    return total;
}
-(double)expectedTotal{
    return startingCash + [self totalCashIn] -[self totalCashOut];
}
-(NSDictionary *)dictionaryReportInfo{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setObject:@(startingCash) forKey:@"startingCash"];
    
    [dict setObject:@([self totalCashInWithType:CashInTypeTender]) forKey:@"cashTendered"];
    [dict setObject:@([self totalCashInWithType:CashInTypeDeposit]) forKey:@"cashPaidIn"];
    
    [dict setObject:@([self totalCashOutWithType:CashOutTypeChangeDue]) forKey:@"cashChange"];
    [dict setObject:@([self totalCashOutWithType:CashOutTypeRefund]) forKey:@"cashRefund"];
    [dict setObject:@([self totalCashOutWithType:CashOutTypePayOut]) forKey:@"cashPaidOut"];
    
    [dict setObject:@([self expectedTotal]) forKey:@"cashExpected"];
    [dict setObject:@(closingCash) forKey:@"cashTotalActual"];
    return dict;
}
@end
