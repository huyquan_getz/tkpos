//
//  TKItem.m
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@class TKProductItem;
#import "TKItem.h"
#import "Constant.h"
#import "Controller.h"

@implementation TKItem
+(NSString *)getName:(CBLDocument *)document{
    return (NSString*)[document.properties[tkKeyTitle] convertNullToNil];
}
+(NSString *)getDescription:(CBLDocument *)document{
    return (NSString*)[document.properties[tkKeyDescription] convertNullToNil];
}
+(NSDictionary *)infomationBaseWithTable:(NSString *)table{
    NSString *date=[CBLJSON JSONObjectWithDate:[NSDate date]];
    NSString *indexStore=[NSString stringWithFormat:@"%ld",(long)[Controller storeIndexDefault]];
    return @{tkKeyTable:table,
             tkKeyCreatedDate:date,
             tkKeyModifiedDate:date,
             tkKeyUserId:@"001",
             tkKeyUserOwner:@"Merchant_001",
             tkKeyStatus:@(YES),
             tkKeyDisplay:@(YES),
             tkKeyIndexStore:indexStore,
             tkKeyChannels:@[indexStore],
             tkKeyIsPOS:@(ItemFromTypePOS)
             };
}
+(BOOL)getStatus:(CBLDocument *)document{
    return [document.properties[tkKeyStatus] boolValue];
}
+(BOOL)getDisplay:(CBLDocument *)document{
    return [document.properties[tkKeyDisplay] boolValue];
}
@end
