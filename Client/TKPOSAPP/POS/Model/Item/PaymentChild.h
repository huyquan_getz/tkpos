//
//  PaymentChild.h
//  POS
//
//  Created by Nha Duong Cong on 12/4/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define StringPaymentTypeCash @"Cash"
#import <Foundation/Foundation.h>

@interface PaymentChild : NSObject{
    NSString *paymentType;
    double charged;
    NSDate *createdDate;
    NSString *approvedCode;
    double changeDue;
}
@property (readonly) NSString *paymentType;
@property (readonly) double charged;
@property (readonly) NSString *approvedCode;
@property (readonly) NSDate *createdDate;
@property (readonly) double changeDue;
-(instancetype)initPaymentCashWithCharged:(double)charge_;
-(instancetype)initPaymentCashWithCharged:(double)charge_ changeDue:(double)changeDue_;
-(instancetype)initPaymentCreditWithCharged:(double)charge_ paymentType:(NSString*)paymentType approvedCode:(NSString*)approvedCode_;

-(instancetype)initFromJsonData:(NSDictionary*)jsonData;
-(double)customerSent;
-(NSString*)displayName;
-(NSDictionary*)jsonData;
+(NSArray*)arrayJSONPaymentChildWithListPaymentChild:(NSArray*)arrayPaymentChild;
@end
