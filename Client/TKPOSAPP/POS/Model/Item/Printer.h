//
//  Printer.h
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface Printer : NSObject
+(CBLDocument*)createPrinterWithName:(NSString*)printerName ip:(NSString*)ipAddress port:(NSInteger)port;
+(CBLDocument*)updatePrinter:(CBLDocument*)printerDocument withName:(NSString*)printerName ip:(NSString*)ipAddress port:(NSInteger)port;
+(void)removePrinter:(CBLDocument*)printer;
+(NSString*)getPrinterName:(CBLDocument*)document;
+(NSString*)getIpAddress:(CBLDocument*)document;
+(NSInteger)getPort:(CBLDocument*)document;
@end
