//
//  TKTaxServiceCharge.h
//  POS
//
//  Created by Cong Nha Duong on 2/6/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
typedef enum _TaxServiceType{
    TaxServiceTypePercent=0,
    TaxServiceTypeValue=1,
}TaxServiceType;
#import "TKItem.h"

@interface TKTaxServiceCharge : TKItem{
    TaxServiceType type;
    double value;
    NSString *name;
}
@property (readonly) TaxServiceType type;
@property (readonly) double value;
@property (readonly) NSString *name;
-(instancetype)initTaxFromJSON:(NSDictionary*)taxJSON;
-(instancetype)initServiceChargeFromeJSON:(NSDictionary*)serviceChargeJSON;
-(instancetype)initWithName:(NSString*)name_ type:(TaxServiceType)type_ value:(double)value_;
+(NSArray*)listTax:(CBLDocument*)document forStore:(NSInteger)indexStore;
+(TKTaxServiceCharge*)serviceCharge:(CBLDocument*)document forStore:(NSInteger)indexStore;

@end
