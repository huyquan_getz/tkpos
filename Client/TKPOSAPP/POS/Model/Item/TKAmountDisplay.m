//
//  TKAmountDisplay.m
//  POS
//
//  Created by Cong Nha Duong on 2/7/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "TKAmountDisplay.h"
#import "Constant.h"

@implementation TKAmountDisplay
@synthesize currency;
-(instancetype)initWithCurrency:(NSString *)currency_{
    if (self=[super init]) {
        currency=currency_;
    }
    return self;
}
-(NSString *)stringAmountHaveCurrency:(double)amount{
    return [TKAmountDisplay stringAmount:amount withCurrency:currency];
}

+(NSString *)stringByRoundCashAmout:(double)amout{
    if (amout> -ESP && amout<ESP) {
        amout=0;
    }
    return [NSString stringWithFormat:@"%0.2f",[self tkRoundCash:amout]];
}
+(BOOL)isZero:(double)amount{
    if (amount> -ESP && amount<ESP) {
        return YES;
    }
    return NO;
}
+(double)tkRoundCash:(double)amount{
    return (round((amount+ESP)*100))/100.0;
}
+(double)tkRoundCashTotal:(double)amount{
    return (round((amount+ESP)*10*2))/(10.0*2);
}

+(NSString *)stringAmount:(double)amount withCurrency:(NSString *)currency_{
    return [NSString stringWithFormat:@"%@ %@",currency_,[self stringByRoundCashAmout:amount]];
}
+(double)ceil:(double)amount withIndexNumberCeil:(NSUInteger)indexNumberCeil{
    // ex :     43210.89   indexNumberCeil=0 -> 43211.00   indexNumberCeil=1; 43220.00
    double pow=powf(10,indexNumberCeil);
    return pow * ceil(amount/pow);
}
@end
