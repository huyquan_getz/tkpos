//
//  TKTaxServiceCharge.m
//  POS
//
//  Created by Cong Nha Duong on 2/6/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TKTaxServiceCharge.h"
#import "Constant.h"
@implementation TKTaxServiceCharge
@synthesize name;
@synthesize value;
@synthesize type;
-(instancetype)initWithName:(NSString *)name_ type:(TaxServiceType)type_ value:(double)value_{
    if (self=[super init]) {
        name=name_;
        value=value_;
        type =type_;
    }
    return self;
}
-(instancetype)initTaxFromJSON:(NSDictionary *)taxJSON{
    if (self = [super init]) {
        name =taxJSON[tkKeyName];
        value=[taxJSON[tkKeyValueRate] doubleValue];
        type=TaxServiceTypePercent;
    }
    return self;
}
-(instancetype)initServiceChargeFromeJSON:(NSDictionary *)serviceChargeJSON{
    if (self =[super init]) {
        name=@"Service Charge";
        value=[serviceChargeJSON[@"serviceChargeRate"] doubleValue];
        type=TaxServiceTypePercent;
    }
    return self;
}

//PUBLIC
+(NSArray *)listTax:(CBLDocument *)document forStore:(NSInteger)indexStore{
    NSMutableArray *listTaxInStore=[[NSMutableArray alloc] init];
    NSArray *arrayTax=[document.properties[tkKeyArrayTax] convertNullToNil];
    for (NSDictionary *tax in arrayTax) {
        BOOL check=NO;
        double valueRate=0;
        for (NSDictionary *store in  [tax[tkKeyStoreInformation] convertNullToNil]) {
            if ([store[tkKeyIndexStore] integerValue] == indexStore) {
                check=YES;
                valueRate=[store[tkKeyValueRate] doubleValue];
                break;
            }
        }
        if (check) {
            NSMutableDictionary *dict=[tax mutableCopy];
            [dict removeObjectForKey:tkKeyStoreInformation];
            [dict setObject:@(indexStore) forKey:tkKeyIndexStore];
            [dict setObject:@(valueRate) forKey:tkKeyValueRate];
            [listTaxInStore addObject:[[TKTaxServiceCharge alloc] initTaxFromJSON:dict]];
        }
    }
    return listTaxInStore;
}
+(TKTaxServiceCharge *)serviceCharge:(CBLDocument *)document forStore:(NSInteger)indexStore{
    NSArray *arrayServiceCharge=[document.properties[tkKeyArrServiceCharge] convertNullToNil];
    for (NSDictionary *serviceCharge in arrayServiceCharge) {
        if ([serviceCharge[tkKeyIndexStore] integerValue]==indexStore) {
            NSMutableDictionary *svCharge=[serviceCharge mutableCopy];
            [svCharge removeObjectForKey:tkKeyStore];
            return [[TKTaxServiceCharge alloc] initServiceChargeFromeJSON:svCharge];
        }
    }
    return nil;
}

@end
