//
//  CashDrawer.m
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define CurrentDrawerID @"CurrentDrawerID"
#define keyDrawerName @"drawerName"
#define keyIpAddress @"ipAddress"
#define keyPort @"port"
#import "CashDrawer.h"

@implementation CashDrawer
+(instancetype)sharedCashDrawer{
    __strong static CashDrawer *_cashDrawer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _cashDrawer=[[CashDrawer alloc] init];
    });
    return _cashDrawer;
}
-(instancetype)init{
    if (self =[super init]) {
        NSString *drawerID=[[NSUserDefaults standardUserDefaults] objectForKey:CurrentDrawerID];
        if (drawerID) {
            currentCashDrawer =[[SyncManager sharedSyncManager] documentWithDocumentId:drawerID];
        }else{
            currentCashDrawer=[self createDrawerWithName:@"" ip:@"" port:-1];
            [[NSUserDefaults standardUserDefaults] setObject:currentCashDrawer.documentID forKey:CurrentDrawerID];
        }
    }
    return self;
}
-(void)createCashDrawer{
    
}
-(CBLDocument *)createDrawerWithName:(NSString *)drawerName ip:(NSString *)ipAddress port:(NSInteger)port{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    NSDate *currentDate=[NSDate date];
    [dict setObject:@"CashDrawerLocal" forKey:tkKeyTable];
    [dict setObject:[CBLJSON JSONObjectWithDate:currentDate] forKey:tkKeyCreatedDate];
    [dict setObject:[CBLJSON JSONObjectWithDate:currentDate] forKey:tkKeyModifiedDate];
    [dict setObject:drawerName forKey:keyDrawerName];
    [dict setObject:ipAddress forKey:keyIpAddress];
    [dict setObject:@(port) forKey:keyPort];
    [dict setObject:@(YES) forKey:tkKeyStatus];// not need
    [dict setObject:@(YES) forKey:tkKeyDisplay];//not need
    return [[SyncManager sharedSyncManager]createDocumentLocalOnly:dict];
}
-(void)updateCashDrawerWithName:(NSString *)cashDrawerName ip:(NSString *)ipAddress port:(NSInteger)port{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setObject:[CBLJSON JSONObjectWithDate:[NSDate date]] forKey:tkKeyModifiedDate];
    [dict setObject:cashDrawerName forKey:keyDrawerName];
    [dict setObject:ipAddress forKey:keyIpAddress];
    [dict setObject:@(port) forKey:keyPort];
    [[SyncManager sharedSyncManager] updateDocument:currentCashDrawer propertyDoc:dict];
}
-(NSString *)getCashDrawerName{
    return currentCashDrawer.properties[keyDrawerName];
}
-(NSString *)getIpAddress{
    return currentCashDrawer.properties[keyIpAddress];
}
-(NSInteger)getPort{
    return [currentCashDrawer.properties[keyPort] integerValue];
}
@end

