//
//  TKOrder.m
//  POS
//
//  Created by Nha Duong Cong on 11/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TKOrder.h"
#import "PaymentChild.h"
#import "Constant.h"

@implementation TKOrder
+(NSString *)displayName:(CBLDocument *)document{
    return [self orderCode:document];
}
+(OrderStatus)orderStatus:(CBLDocument *)document{
    return (OrderStatus)[document.properties[tkKeyOrderStatus] integerValue];
}
+(double)grandTotal:(CBLDocument *)document{
    return [document.properties[tkKeyGrandTotal] doubleValue];
}
+(double)subTotal:(CBLDocument *)document{
    return [document.properties[tkKeySubTotal] doubleValue];
}
+(double)changeDue:(CBLDocument *)document{
    return [document.properties[tkKeyChangeDue] doubleValue];
}
+(double)roundAmount:(CBLDocument *)document{
    return [document.properties[tkKeyRoundAmount] doubleValue];
}
+(double)discountTotalAmount:(CBLDocument *)document{
    return [document.properties[tkKeyDiscount] doubleValue];
}
+(TKDiscount *)discountCartItem:(CBLDocument *)document{
    NSDictionary *json=[document.properties[tkKeyDiscountCartItem] convertNullToNil];
    TKDiscount *discount;
    if (json) {
        discount=[[TKDiscount alloc] initFromJsonData:json];
    }
    return discount;
}
+(BOOL)discountCart:(CBLDocument *)document{
    if (document.properties[tkKeyDiscountCart]) {
        return [document.properties[tkKeyDiscountCart] boolValue];
    }
    return NO;
}
+(double)discountCartValue:(CBLDocument *)document{
    return [document.properties[tkKeyDiscountCartValue] doubleValue];
}
+(NSDate *)completedDate:(CBLDocument *)document{
    NSString *completed=document.properties[tkKeyCompletedDate];
    return [CBLJSON dateWithJSONObject:completed];
}
+(NSDate *)createdDate:(CBLDocument *)document{
    NSString *createdDate=document.properties[tkKeyCreatedDate];
    return [CBLJSON dateWithJSONObject:createdDate];
}
+(NSString *)orderCode:(CBLDocument *)document{
    return [document.properties[tkKeyOrderCode] convertNullToNil];
}
+(NSString *)cashierName:(CBLDocument *)document{
    if (![document[tkKeyStaff] convertNullToNil]) {
        return nil;
    }else{
        return [document[tkKeyStaff] objectForKey:tkKeyFullName];
    }
}
+(NSArray *)arrayProductJSON:(CBLDocument *)document{
    return document.properties[tkkeyArrayProduct];
}
+(NSArray *)arrayPaymentJSON:(CBLDocument *)document{
   
    return document.properties[tkkeyArrayPayment];
}
+(NSArray *)arrayAdhocProductJSON:(CBLDocument *)document{
    return [document.properties[tkKeyArrayAdhocProduct] convertNullToNil];
}
+(NSDictionary *)refundInfo:(CBLDocument *)document{
    return [document.properties[tkKeyRefundInfo] convertNullToNil];
}
+(NSString *)currency:(CBLDocument *)document{
    return [document.properties[tkKeyCurrency] convertNullToNil];
}
+(NSArray *)arrayPaymentRefundJSON:(CBLDocument *)document{
    return [self refundInfo:document][tkkeyArrayPayment];
}
+(NSDate *)refundDate:(CBLDocument *)document{
    return [CBLJSON dateWithJSONObject:[self refundInfo:document][tkKeyRefundDate]];
}
+(NSString*)refundEmployeeName:(CBLDocument*)document{
    return [self refundInfo:document][@"staffFullName"];
}
+(NSString *)refundDisplayReason:(CBLDocument *)document{
    NSString *reason=[self refundInfo:document][tkKeyReasonRefund];
    return [self displayReasonRefundWithReason:reason];
}
+(NSString*)displayReasonRefundWithReason:(NSString*)reasonOrginal{
    NSString *reasonDisplay;
    if ([reasonOrginal isEqualToString:tkReasonOrderCancel]) {
        reasonDisplay = [[LanguageUtil sharedLanguageUtil] stringByKey:@"receipt.refund.text-cancel-order"];
    }else if ([reasonOrginal isEqualToString:tkReasonOrderWrong]){
        reasonDisplay=[[LanguageUtil sharedLanguageUtil] stringByKey:@"receipt.refund.text-wrong-order"];
    }else if ([reasonOrginal isEqualToString:tkReasonOrderProductFaulty]){
        reasonDisplay=[[LanguageUtil sharedLanguageUtil] stringByKey:@"receipt.refund.text-product-faulty"];
    }else{
        reasonDisplay=reasonOrginal;
    }
    return reasonDisplay;
}
+(BOOL)havePayementWithCash:(CBLDocument *)document{
    NSArray *listPaymentJSON=[self arrayPaymentJSON:document];
    BOOL havePaymentCash=NO;
    for (NSDictionary *json in listPaymentJSON){
        PaymentChild *pm=[[PaymentChild alloc] initFromJsonData:json];
        if ([pm.paymentType isEqualToString:StringPaymentTypeCash]) {
            havePaymentCash=YES;
            break;
        }
    }
    return havePaymentCash;
}
+(BOOL)isCancelRequest:(CBLDocument *)document{
    return [document.properties[tkKeyIsCancelRequest] boolValue];
}
+(NSDictionary *)customerReceiver:(CBLDocument *)document{
    return [document.properties[tkKeyCustomerReceiver] convertNullToNil];
}
+(NSString *)emailCustomerReceiver:(CBLDocument *)document{
    return [[[TKOrder customerReceiver:document] objectForKey:tkKeyEmail] convertNullToNil];
}
+(NSString *)customerReceiverName:(CBLDocument *)document{
    return [[[TKOrder customerReceiver:document] objectForKey:tkKeyFullName] convertNullToNil];
}

+(NSDictionary *)customerSender:(CBLDocument *)document{
    return [document.properties[tkKeyCustomerSender] convertNullToNil];
}
+(NSString *)emailCustomerSender:(CBLDocument *)document{
    return [[[TKOrder customerSender:document] objectForKey:tkKeyEmail] convertNullToNil];
}
+(NSDictionary *)employee:(CBLDocument *)document{
    return [document.properties[tkKeyStaff] convertNullToNil];
}
+(NSString *)employeeName:(CBLDocument *)document{
    NSDictionary *employee=[self employee:document];
    return employee[tkKeyFullName];
}
+(NSArray *)listTaxDisplayes:(CBLDocument *)document{
    return [document.properties[@"listTaxvalue"] convertNullToNil];
}
+(NSArray *)listTaxes:(CBLDocument *)document{
    NSArray *taxDisplayes=[self listTaxDisplayes:document];
    NSMutableArray *taxObjects=[[NSMutableArray alloc] init];
    for (NSDictionary *taxDs in taxDisplayes) {
        [taxObjects addObject:[[TKTaxServiceCharge alloc] initWithName:taxDs[@"taxName"] type:TaxServiceTypePercent value:[taxDs[@"percent"] doubleValue]]];
    }
    return taxObjects;
}
+(NSDictionary *)serviceChargeDisplay:(CBLDocument *)document{
    return [document.properties[@"serviceCharge"] convertNullToNil];
}
+(TKTaxServiceCharge *)serviceCharge:(CBLDocument *)document{
    NSDictionary *svChargeDisplay=[self serviceChargeDisplay:document];
    if (svChargeDisplay) {
        return [[TKTaxServiceCharge alloc] initWithName:svChargeDisplay[@"serviceChargeName"] type:TaxServiceTypePercent value:[svChargeDisplay[@"percent"] doubleValue]];
    }else{
        return nil;
    }
}
@end
