//
//  TKItem.h
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
typedef enum _ItemFromType{
    ItemFromTypeOnline=0,
    ItemFromTypePOS=1
}ItemFromType;

#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "Constant.h"

@interface TKItem : CBLModel
+(NSString*)getName:(CBLDocument*)document;
+(NSString*)getDescription:(CBLDocument*)document;
+(NSDictionary*)infomationBaseWithTable:(NSString*)table;
+(BOOL)getStatus:(CBLDocument*)document;
+(BOOL)getDisplay:(CBLDocument*)document;
@end
