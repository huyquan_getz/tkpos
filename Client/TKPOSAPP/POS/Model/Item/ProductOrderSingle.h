//
//  ProductOrderSingle.h
//  POS
//
//  Created by Nha Duong Cong on 12/11/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKDiscount.h"

@interface ProductOrderSingle : NSObject
@property (strong,nonatomic) NSMutableDictionary *productProperies;
@property (strong,nonatomic) NSDictionary *variantOrder;
@property (strong,nonatomic) NSArray *variantOptions;
@property (assign) NSInteger quantity;
-(instancetype)initWithProductProperties:(NSMutableDictionary*)productPr variantOrder:(NSDictionary*)variantOrder_ variantOptions:(NSArray*)variantOptions_ quantity:(NSInteger)quantity_;
-(NSString*)categoryId;
-(NSString*)productId;
-(NSString*)variantSKU;
-(NSString*)productSKU;
-(NSString*)productName;
-(NSString*)variantName;
-(double)price;
-(TKDiscount*)checkDiscount;
-(TKDiscount*)discount;
-(void)setDiscount:(TKDiscount*)discount;
-(instancetype)copy;
@end
