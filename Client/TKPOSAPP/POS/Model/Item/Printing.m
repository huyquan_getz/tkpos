//
//  Printing.m
//  POS
//
//  Created by Cong Nha Duong on 1/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define keyArrayCategory @"arrayCategory"
#define keyPrinter @"printer"
#define keyPrintingType @"printingType"
#define keyCombine @"combine"
#import "Printing.h"
#import "Printer.h"
#import "CashierBusinessModel.h"
#import "Controller.h"
#import "PrintingTemplateManagement.h"
#import "PrinterController.h"
#import "ProductOrderSingle.h"

@implementation Printing
+(NSArray *)getListPrintingTypeDefault{
    // PrintingTypeReceipt, PrintingTypeReceiptCopy, PrintingTypeSingleChit;
    NSMutableArray *array=[[NSMutableArray alloc] init];
    for (int i=0; i<4; i++) {
        [array addObject:[self getPrintingDisplayWithPrintingType:(PrintingType)i]];
    }
    return array;
}
+(NSString *)getPrintingDisplayWithPrintingType:(PrintingType)type{
    switch (type) {
        case PrintingTypeReceipt:
            return @"Receipt";
            break;
        case PrintingTypeReceiptCopy:
            return @"Receipt Copy";
            break;
        case PrintingTypeSingleOrderChit:
            return @"Single Order Chit";
            break;
        case PrintingTypeTotalOrderChit:;
            return @"Total Order Chit";
        default:
            return nil;
            break;
    }
}

+(PrintingType)getPrintingType:(CBLDocument *)document{
    return (PrintingType)[document.properties[keyPrintingType] integerValue];
}
+(NSString *)getPrintingDisplay:(CBLDocument *)document{
    return [self getPrintingDisplayWithPrintingType:[self getPrintingType:document]];
}
+(CBLDocument *)getPrinter:(CBLDocument *)document{
    NSString *printerID=document.properties[keyPrinter];
    return [[SyncManager sharedSyncManager] documentWithDocumentId:printerID];
}
+(NSString *)getPrinterNameAssign:(CBLDocument *)document{
    CBLDocument *printer=[self getPrinter:document];
    return [Printer getPrinterName:printer];
}
+(NSString *)getPrinterIpAddress:(CBLDocument *)document{
    CBLDocument *printer=[self getPrinter:document];
    return [Printer getIpAddress:printer];
}
+(NSArray *)getArrayCategoryId:(CBLDocument *)document{
    return document.properties[keyArrayCategory];
}
+(BOOL)getCombine:(CBLDocument *)document{
    return [document.properties[keyCombine] boolValue];
}
+(CBLDocument *)createPrintingWithType:(PrintingType)type printer:(CBLDocument *)printer listCategory:(NSArray *)listCategory combine:(BOOL)combine{
    NSMutableArray *arrayCategoryID=[[NSMutableArray alloc] init];
    for (CBLDocument *category in listCategory) {
        [arrayCategoryID addObject:category.documentID];
    }
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    NSDate *currentDate=[NSDate date];
    [dict setObject:tkPrintingTable forKey:tkKeyTable];
    [dict setObject:[CBLJSON JSONObjectWithDate:currentDate] forKey:tkKeyCreatedDate];
    [dict setObject:[CBLJSON JSONObjectWithDate:currentDate] forKey:tkKeyModifiedDate];
    [dict setObject:arrayCategoryID forKey:keyArrayCategory];
    [dict setObject:printer.documentID forKey:keyPrinter];
    [dict setObject:@(type) forKey:keyPrintingType];
    [dict setObject:@(combine) forKey:keyCombine];
    [dict setObject:@(YES) forKey:tkKeyStatus];
    [dict setObject:@(YES) forKey:tkKeyDisplay];
    return [[SyncManager sharedSyncManager]createDocumentLocalOnly:dict];
}
+(CBLDocument *)updatePrinting:(CBLDocument *)printing withType:(PrintingType)type printer:(CBLDocument *)printer listCategory:(NSArray *)listCategory combine:(BOOL)combine{
    NSMutableArray *arrayCategoryID=[[NSMutableArray alloc] init];
    for (CBLDocument *category in listCategory) {
        [arrayCategoryID addObject:category.documentID];
    }
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setObject:tkPrintingTable forKey:tkKeyTable];
    [dict setObject:[CBLJSON JSONObjectWithDate:[NSDate date]] forKey:tkKeyModifiedDate];
    [dict setObject:arrayCategoryID forKey:keyArrayCategory];
    [dict setObject:printer.documentID forKey:keyPrinter];
    [dict setObject:@(type) forKey:keyPrintingType];
    [dict setObject:@(combine) forKey:keyCombine];
    return [[SyncManager sharedSyncManager] updateDocument:printing propertyDoc:dict];
}
+(void)removePrinting:(CBLDocument *)document{
    [[SyncManager sharedSyncManager] deleteDocument:document];
}


+(void)printingAfterPayment:(CBLDocument*)orderDocumentPaymented {
    //currentOrderDocumentPaymented
    [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
        CBLQuery *queryPrinter=[Controller queryGetAllPrinting];
        CBLQueryEnumerator *enumRowPrinter=[queryPrinter run:nil];
        dispatch_queue_t queue1 = dispatch_queue_create("tkPrintingReceiptQueue", NULL);
        dispatch_queue_t queue2 = dispatch_queue_create("tkPrintingSigleQueue", NULL);
        dispatch_group_t group = dispatch_group_create();
        NSMutableArray *listProductbyPrinter = [[NSMutableArray alloc] init];
        
        NSDictionary *dictOrderInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                       [TKOrder orderCode:orderDocumentPaymented],tkKeyOrderCode,
                                       [TKOrder employeeName:orderDocumentPaymented],@"employeeName",
                                       [TKOrder createdDate:orderDocumentPaymented],@"createdDate"
                                       ,nil];
        for (CBLQueryRow *row in enumRowPrinter) {
            int type = [Printing getPrintingType:row.document ];
            NSString *printerIpAdress =[Printing getPrinterIpAddress:row.document ];
             NSString *printerName =[Printing getPrinterNameAssign:row.document ];
            
            if (type == PrintingTypeReceipt || type == PrintingTypeReceiptCopy) {
                dispatch_group_async(group, queue1, ^{
                    UIImage *image = [[PrintingTemplateManagement sharedObject] printingImageWithReceipt:orderDocumentPaymented receiptCopy:(type==PrintingTypeReceiptCopy) reprint:NO];
                    [[PrinterController sharedInstance] addPrintWithImage:image Height:image.size.height Ip:printerIpAdress Type:0 Name:printerName Infor:nil];
                });
                
            }else if(type == PrintingTypeTotalOrderChit){
                    dispatch_group_async(group, queue1, ^{
                        NSArray *arrProductItem = [CashierBusinessModel arrayProductOrderWithProductOrdersJSON:[TKOrder arrayProductJSON:orderDocumentPaymented]];
                        NSArray *arrayAdhocItem=[CashierBusinessModel arrayAdhocProductWithAdhocProductJSON:[TKOrder arrayAdhocProductJSON:orderDocumentPaymented]];
                        UIImage *image = [[PrintingTemplateManagement sharedObject] printingSingleOrderChit:arrProductItem listAdhoc:arrayAdhocItem withOrderInfo:dictOrderInfo totalOder:YES];
                        [[PrinterController sharedInstance] addPrintWithImage:image Height:image.size.height Ip:printerIpAdress Type:0 Name:printerName Infor:nil];
                });
            }else {
                 NSArray *arrCategoryId = [Printing getArrayCategoryId:row.document];
                 NSArray *arrProductItem = [CashierBusinessModel arrayProductOrderWithProductOrdersJSON:[TKOrder arrayProductJSON:orderDocumentPaymented]];
                 NSArray *arrayAdhocItem=[CashierBusinessModel arrayAdhocProductWithAdhocProductJSON:[TKOrder arrayAdhocProductJSON:orderDocumentPaymented]];
               
                if ([Printing getCombine:row.document]) { // Printer is combine single chit
                        NSMutableArray *arrayProductPr = [[NSMutableArray alloc] init];
                        for (ProductOrderSingle *object in arrProductItem) {
                            NSPredicate *predicte = [NSPredicate predicateWithFormat:
                                                     @"SELF contains[c] %@", object.categoryId];
                            NSArray *filteredArray = [arrCategoryId filteredArrayUsingPredicate:predicte];

                            if (filteredArray.count > 0) {
                                [arrayProductPr addObject:object];
                            }
                        }
                        NSMutableArray *arrayAdhocPr=[[NSMutableArray alloc] init];
                        for (AdhocProduct *object in arrayAdhocItem) {
                            NSPredicate *predicte = [NSPredicate predicateWithFormat:
                                                 @"SELF contains[c] %@", object.categoryId];
                            NSArray *filteredArray = [arrCategoryId filteredArrayUsingPredicate:predicte];
                        
                            if (filteredArray.count > 0) {
                                [arrayAdhocPr addObject:object];
                            }
                        }
                    if (arrayProductPr.count>0 || arrayAdhocPr.count>0) {
                         [listProductbyPrinter addObject:[[NSMutableDictionary alloc] initWithObjectsAndKeys: printerIpAdress,tkKeyPrinterIp,printerName,@"printerName",arrayProductPr,tkkeyArrayProduct,arrayAdhocPr,tkKeyArrayAdhocProduct, nil]];
                    }
                }else { //printer is not combine single chit
                 
                    
                    dispatch_group_async(group, queue2, ^{
                        for (ProductOrderSingle *object in arrProductItem) {
                            NSPredicate *predicte = [NSPredicate predicateWithFormat:
                                                     @"SELF contains[c] %@", object.categoryId];
                            NSArray *filteredArray = [arrCategoryId filteredArrayUsingPredicate:predicte];
                            if (filteredArray.count > 0) {
                                
                                UIImage *image = [[PrintingTemplateManagement sharedObject] printingSingleOrderChit:@[object] listAdhoc:nil withOrderInfo:dictOrderInfo totalOder:NO];
                                [[PrinterController sharedInstance] addPrintWithImage:image Height:image.size.height Ip:printerIpAdress Type:0 Name:printerName Infor:nil];
                            }
                        }
                        //for adhoc
                        for (AdhocProduct *object in arrayAdhocItem) {
                            NSPredicate *predicte = [NSPredicate predicateWithFormat:
                                                     @"SELF contains[c] %@", object.categoryId];
                            NSArray *filteredArray = [arrCategoryId filteredArrayUsingPredicate:predicte];
                            if (filteredArray.count > 0) {
                                
                                UIImage *image = [[PrintingTemplateManagement sharedObject] printingSingleOrderChit:nil listAdhoc:@[object] withOrderInfo:dictOrderInfo totalOder:NO];
                                [[PrinterController sharedInstance] addPrintWithImage:image Height:image.size.height Ip:printerIpAdress Type:0 Name:printerName Infor:nil];
                            }
                        }
                    });
                
                }
            }
        }
        
        if (listProductbyPrinter.count > 0) {
            dispatch_group_async(group, queue2, ^{
            for (int i=0; i < listProductbyPrinter.count; i++) {
                    UIImage *image = [[PrintingTemplateManagement sharedObject] printingSingleOrderChit:[listProductbyPrinter[i] objectForKey:tkkeyArrayProduct] listAdhoc:[listProductbyPrinter[i] objectForKey:tkKeyArrayAdhocProduct] withOrderInfo:dictOrderInfo totalOder:NO];
                     [[PrinterController sharedInstance] addPrintWithImage:image Height:image.size.height Ip:[listProductbyPrinter[i] objectForKey:tkKeyPrinterIp] Type:0 Name:[listProductbyPrinter[i] objectForKey:@"printerName"] Infor:nil];
                }
            });
        }
//        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
//           
//        });
    }];
}
@end
