//
//  TKProduct.h
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TKItem.h"

@interface TKProduct : TKItem
+(NSString*)getImageUrlDefault:(CBLDocument*)document;
+(NSInteger)getNumberVariants:(CBLDocument *)document;
+(NSString*)getShortName:(CBLDocument*)document;
+(NSArray*)getArrayOptions:(CBLDocument*)document;
@end
