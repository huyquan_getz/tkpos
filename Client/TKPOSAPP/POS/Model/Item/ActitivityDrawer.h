//
//  ActitivityDrawer.h
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
typedef enum _CashInType{
    CashInTypeNone=-1,
    CashInTypeDeposit=0,
    CashInTypeTender=1
}CashInType;

typedef enum _CashOutType{
    CashOutTypeNone=-1,
    CashOutTypePayOut=0,
    CashOutTypeChangeDue=1,
    CashOutTypeRefund =2
}CashOutType;
#import <Foundation/Foundation.h>
#import "Constant.h"

@interface ActitivityDrawer : NSObject<CBLJSONEncoding>
@property (assign,nonatomic) NSInteger employeeId;
@property (strong,nonatomic) NSString *employeeName;
@property (strong,nonatomic) NSDate *createdDate;
@property (assign,nonatomic) CashInType cashInType;
@property (assign,nonatomic) double cashInValue;
@property (assign,nonatomic) CashOutType cashOutType;
@property (assign,nonatomic) double cashOutValue;

@property (strong,nonatomic) NSString *comment;
@property (readonly) NSString *type;
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString*)employeeName_ createdDate:(NSDate*)createdDate_ cashInValue:(double)cashInValue_ cashInType:(CashInType)cashInType_ cashOutValue:(double)cashOutValue_ cashOutType:(CashOutType)cashOutType_ comment:(NSString*)comment_ type:(NSString*)type_;
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString *)employeeName_ deposit:(double)depositAmount comment:(NSString*)comment;
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString *)employeeName_ payOut:(double)payOutAmount comment:(NSString *)comment;
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString *)employeeName_ paymentCash:(double)paymentAmount changeDue:(double)changeDue comment:(NSString *)comment;
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString *)employeeName_ refund:(double)refundAmount comment:(NSString *)comment;
-(id)encodeAsJSON;
-(instancetype)initWithJSON:(id)jsonObject;
@end
