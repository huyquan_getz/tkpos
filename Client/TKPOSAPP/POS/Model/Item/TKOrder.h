//
//  TKOrder.h
//  POS
//
//  Created by Nha Duong Cong on 11/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
typedef enum _OrderType{
    OrderTypeDelivery = 0,
    OrderTypeSelfCollect = 1
}OrderType;

typedef enum _PaymentStatus{
    PaymentStatusPending =0,
    PaymentStatusWaiting =1,
    PaymentStatusFailDispute =2,
    PaymentStatusPaid =3,
    PaymentStatusRefunded =4
}PaymentStatus;

typedef enum _OrderStatus{
    OrderStatusNone = -1, // order just created
    OrderStatusPendingWaiting =0,
    OrderStatusOrdered = 1,// new order for Delivery and self-collective
    OrderStatusReadyDelivery =2,
    OrderStatusOnDelivery =3,
    OrderStatusDelivered =4,
    OrderStatusReadyCollection =5,
    OrderStatusReceived =6,
    OrderStatusCanceled =7,
    OrderStatusCollected =8,
    OrderStatusRefund=9
}OrderStatus;

#import "TKItem.h"
#import "TKTaxServiceCharge.h"
#import "TKDiscount.h"

@interface TKOrder : TKItem
+(NSString*)orderCode:(CBLDocument*)document;
+(OrderStatus)orderStatus:(CBLDocument*)document;
+(NSString*)displayName:(CBLDocument*)document;
+(double)grandTotal:(CBLDocument*)document;
+(double)subTotal:(CBLDocument*)document;
+(double)changeDue:(CBLDocument*)document;
+(double)roundAmount:(CBLDocument*)document;
+(BOOL)discountCart:(CBLDocument*)document;
+(double)discountCartValue:(CBLDocument*)document;

+(TKDiscount*)discountCartItem:(CBLDocument*)document; // for order not finish
+(double)discountTotalAmount:(CBLDocument*)document;
+(NSDate*)completedDate:(CBLDocument*)document;
+(NSDate*)createdDate:(CBLDocument*)document;
+(NSDate*)refundDate:(CBLDocument*)document;
+(NSArray*)arrayProductJSON:(CBLDocument*)document;
+(NSArray*)arrayPaymentJSON:(CBLDocument*)document;
+(NSArray*)arrayAdhocProductJSON:(CBLDocument*)document;
+(NSDictionary*)refundInfo:(CBLDocument*)document;
+(NSArray*)arrayPaymentRefundJSON:(CBLDocument*)document;
+(NSString*)refundEmployeeName:(CBLDocument*)document;
+(NSString*)refundDisplayReason:(CBLDocument*)document;
+(NSDictionary*)customerReceiver:(CBLDocument*)document;
+(NSDictionary*)customerSender:(CBLDocument*)document;
+(NSString*)emailCustomerReceiver:(CBLDocument*)document;
+(NSString*)emailCustomerSender:(CBLDocument*)document;
+(NSString*)customerReceiverName:(CBLDocument *)document;
+(NSDictionary*)employee:(CBLDocument*)document;
+(NSString*)employeeName:(CBLDocument*)document;
+(BOOL)havePayementWithCash:(CBLDocument*)document;
+(BOOL)isCancelRequest:(CBLDocument*)document;
+(NSString*)currency:(CBLDocument*)document;
+(NSArray*)listTaxDisplayes:(CBLDocument*)document; // list NSDictionary
+(NSArray*)listTaxes:(CBLDocument*)document; // list TKTaxService
+(NSDictionary*)serviceChargeDisplay:(CBLDocument*)document;
+(TKTaxServiceCharge*)serviceCharge:(CBLDocument*)document;
+(NSString*)displayReasonRefundWithReason:(NSString*)reasonOrginal;
@end
