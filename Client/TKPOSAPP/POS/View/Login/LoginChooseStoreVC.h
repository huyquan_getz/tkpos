//
//  LoginChooseStoreVC.h
//  POS
//
//  Created by Nguyen Anh Dao on 10/30/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginChooseStoreVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listStores;
}

@property (weak, nonatomic) IBOutlet UIButton *btStart;
@property (weak, nonatomic) IBOutlet UITableView *tbvStore;
@property (weak, nonatomic) IBOutlet UIButton *btSignOut;
- (IBAction)clickSignOut:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbSelectStore;
- (IBAction)clickStart:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
-(id)initWithArrayStore:(NSArray*)array;
@end
