//
//  LoginManagerVC.m
//  POS
//
//  Created by Nguyen Anh Dao on 10/31/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "LoginManagerVC.h"
#import "Controller.h"
#import "UITextField+Util.h"
#import "UIAlertView+Blocks.h"
#import "NSString+Util.h"
#import "UserDefaultModel.h"
@interface LoginManagerVC ()
{
    LanguageUtil *languageKey;
}
@end

@implementation LoginManagerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _btSignIn.layer.cornerRadius=tkCornerRadiusButton;
    _vEmailPass.layer.cornerRadius=tkCornerRadiusButton;
    [_vManager setShadowWithCornerRadius:tkCornerRadiusViewPopup];
    _vEmailPass.layer.borderWidth=1;
    _vEmailPass.layer.borderColor =tkColorFrameBorder.CGColor;
    _vLine.backgroundColor=tkColorFrameBorder;
}
-(void)viewWillAppear:(BOOL)animated
{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [self showMessageError:@""];
    _lbEmail.text =[languageKey stringByKey:@"general.lb.email"];
    _lbPassword.text =[languageKey stringByKey:@"general.lb.password"];
    _tfEmail.text=@"";
    _tfEmail.placeholder=[languageKey stringByKey:@"general.lb.email-holder"];
    _tfPassword.text=@"";
    _tfPassword.placeholder=[languageKey stringByKey:@"general.lb.password-holder"];
    _lbManagerLogin.text =[languageKey stringByKey:@"login.lb.manager-login"];
   
    [_btSignIn setTitle:[languageKey stringByKey:@"login.bt.sign-in"] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBack:(id)sender {
    [Controller showCashierLoginView];
}

- (IBAction)clickSignIn:(id)sender {
    [self showMessageError:@""];
    if (![self validateInput]) {
        return;
    }
    
    if ([Controller checkUserMerchantLocalWithUsername:_tfEmail.text andPassword:_tfPassword.text]) {
        [self loginSuccess];
    }else{
        [self showMessageError:[languageKey stringByKey:@"login.dl.ms-user-pass-incorrect"]];
    }
}
-(void)loginSuccess{
    [UserDefaultModel saveUserLogined:[UserDefaultModel getAccountMerchant]];
    [Controller showHomeViewCashier];
}
-(BOOL)validateInput{
    if ( [_tfEmail isEmptyText] ) {
        [self showMessageError:[languageKey stringByKey:@"login.dl.ms-username-empty"]];
        return NO;
    }
    if( [_tfPassword isEmptyText] ){
        [self showMessageError:[languageKey stringByKey:@"login.dl.ms-password-empty"]];
        return NO;
    }
    if ( ! [_tfEmail.text isEmailFormat] ){
        [self showMessageError:[languageKey stringByKey:@"login.dl.ms-email-invalid"]];
        return NO;
    }
    return YES;
}
-(void)showMessageError:(NSString*)msgError{
    [_lbMessageLogin setText:msgError];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    CGRect frame=_vManager.frame;
    frame.origin.y -= 130 ;
    [_vManager setFrame:frame];
    [self.view setNeedsDisplay];
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField {
    //keyboard will hide
    CGRect frame=_vManager.frame;
    frame.origin.y += 130;
    [_vManager setFrame:frame];
    [self.view setNeedsDisplay];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField==_tfEmail){
        [_tfPassword becomeFirstResponder];
    }else if (textField==_tfPassword) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self clickSignIn:nil];
        });
    }
    return [textField resignFirstResponder];
}
@end
