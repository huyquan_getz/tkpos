//
//  LoginForgotVC.m
//  POS
//
//  Created by Nguyen Anh Dao on 10/31/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "LoginForgotVC.h"
#import "Controller.h"
#import "TapListener.h"
#import "UITextField+Util.h"
#import "NSString+Util.h"
@interface LoginForgotVC ()
{
    LanguageUtil *languageKey;
}
@end

@implementation LoginForgotVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _vEmailFrame.layer.cornerRadius=tkCornerRadiusViewPopup;
    [_vMainForgot setShadowWithCornerRadius:tkCornerRadiusViewPopup];
    _vEmailFrame.layer.borderColor = tkColorFrameBorder.CGColor;
    _vEmailFrame.layer.borderWidth=1;
    _btClose.layer.cornerRadius=tkCornerRadiusButton;
    _btSubmit.layer.cornerRadius=tkCornerRadiusButton;
    [_vCover setViewCoverDefault];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [_vCover addGestureRecognizer:tap];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    languageKey =[LanguageUtil sharedLanguageUtil];
    [self showMessageError:@""];
    _tfEmail.text=@"";
    _tfEmail.placeholder=[languageKey stringByKey:@"general.lb.email-holder"];
    _lbEmail.text = [languageKey stringByKey:@"general.lb.email"];
    _lbForgot.text =[languageKey stringByKey:@"login.bt.forgot-pass"];
    [_btClose setTitle:[languageKey stringByKey:@"general.bt.close"] forState:UIControlStateNormal];
     [_btSubmit setTitle:[languageKey stringByKey:@"general.bt.submit"] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showMessageError:(NSString*)error{
    [_lbStatusMessage setTextColor:tkColorStatusMessageError];
    [_lbStatusMessage setText:error];
}
-(void)clearStatus{
    [_lbStatusMessage setText:@""];
}
-(void)tapCoverView:(id)sender{
    [self clickClose:nil];
}
- (IBAction)clickClose:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(loginForgotVC:send:)]) {
        [_delegate loginForgotVC:self send:NO];
    }
}

- (IBAction)clickSubmit:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self clearStatus];
    BOOL sendCompleted=YES;
    if (_delegate && [_delegate respondsToSelector:@selector(loginForgotVC:send:)]) {
        [_delegate loginForgotVC:self send:sendCompleted];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self clickSubmit:nil];
    });
    return [textField resignFirstResponder];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    CGRect frame=_vMainForgot.frame;
    frame.origin.y -= 80 ;
    [_vMainForgot setFrame:frame];

}
-(void)textFieldDidEndEditing:(UITextField *)textField {
    //keyboard will hide
    CGRect frame=_vMainForgot.frame;
    frame.origin.y += 80;
    [_vMainForgot setFrame:frame];
}

-(BOOL)validateInput{
    if ( [_tfEmail isEmptyText] ) {
        [self showMessageError:[languageKey stringByKey:@"login.dl.ms-username-empty"]];
        return NO;
    }
    if ( ! [_tfEmail.text isEmailFormat] ){
        [self showMessageError:[languageKey stringByKey:@"login.dl.ms-email-invalid"]];
        return NO;
    }
    return YES;
}
@end
