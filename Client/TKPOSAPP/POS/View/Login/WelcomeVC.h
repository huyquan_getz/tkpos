//
//  WelcomeVC.h
//  POS
//
//  Created by Nha Duong Cong on 10/30/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btSignUp;
- (IBAction)clickSignUp:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btSignIn;
- (IBAction)clickSignIn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *ivTopWelcome;

@end
