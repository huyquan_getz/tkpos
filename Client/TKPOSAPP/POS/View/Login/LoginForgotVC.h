//
//  LoginForgotVC.h
//  POS
//
//  Created by Nguyen Anh Dao on 10/31/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol LoginForgotDelegate <NSObject>

@optional
-(void)loginForgotVC:(id)loginForgotVC send:(BOOL)send;

@end

#import <UIKit/UIKit.h>

@interface LoginForgotVC : UIViewController<UITextFieldDelegate>
@property (weak,nonatomic)id<LoginForgotDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *vCover;

@property (weak, nonatomic) IBOutlet UIButton *btClose;
@property (weak, nonatomic) IBOutlet UIButton *btSubmit;
@property (weak, nonatomic) IBOutlet UIView *vMainForgot;
@property (weak, nonatomic) IBOutlet UIView *vEmailFrame;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbForgot;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbStatusMessage;


- (IBAction)clickClose:(id)sender;
- (IBAction)clickSubmit:(id)sender;

@end
