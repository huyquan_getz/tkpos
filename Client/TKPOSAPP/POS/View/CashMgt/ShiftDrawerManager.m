//
//  ShiftDrawerManager.m
//  POS
//
//  Created by Nha Duong Cong on 12/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define CurrentShiftID @"currentShiftID"
#import "ShiftDrawerManager.h"
#import "CashierBusinessModel.h"
#import "PrinterController.h"
#import "CashDrawer.h"
#import "PrintingTemplateManagement.h"

@implementation ShiftDrawerManager
+(instancetype)sharedDrawerManager{
    __strong static ShiftDrawerManager *_shiftDrawerManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shiftDrawerManager=[[ShiftDrawerManager alloc] init];
    });
    return _shiftDrawerManager;
}
-(instancetype)init{
    if (self=[super init]) {
        NSString *shiftID=[[NSUserDefaults standardUserDefaults] objectForKey:CurrentShiftID];
        if (shiftID) {
            currentShift =[[SyncManager sharedSyncManager] documentWithDocumentId:shiftID];
        }
    }
    return self;
}
- (void)openCurrentCashDrawer{
    [[PrinterController sharedInstance] openDrawerOpenWithIP:[[CashDrawer sharedCashDrawer] getIpAddress]];
}

-(CBLDocument *)currentShiftDrawer{
    return currentShift;
}
-(CBLDocument *)startNewShiftDrawer:(double)cashInDrawer currency:(NSString*)currency{
    if (currentShift==nil) {
        currentShift=[[ShiftDrawer alloc] initWithStartingCash:cashInDrawer currency:currency].document;
        [[NSUserDefaults standardUserDefaults] setObject:currentShift.documentID forKey:CurrentShiftID];
        [self openCurrentCashDrawer];
        return currentShift;
    }else{
        return nil;// existed currentShift
    }
}
-(CBLDocument *)closeCurrentShiftDrawer:(double)currentCashInDrawer notCheckDrawer:(BOOL)notCheckDrawer{
    if (currentShift) {
        ShiftDrawer *shiftDrawer=[[ShiftDrawer alloc] initWithDocument:currentShift]; // create tmp
        if (notCheckDrawer) {
            [shiftDrawer closeDrawerNotClosingCashWithDate:[NSDate date]];
        }else{
            [shiftDrawer closeDrawerWithClosingCash:currentCashInDrawer];
            [self openCurrentCashDrawer];
            [[PrintingTemplateManagement sharedObject] printShiftDrawerReport:shiftDrawer.document];
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:CurrentShiftID];
    }
    CBLDocument *save=currentShift;
    currentShift=nil;
    return save;
}
-(BOOL)addToCurrentShiftWithActivity:(ActitivityDrawer *)activity{
    if (currentShift) {
        ShiftDrawer *shiftDrawer=[[ShiftDrawer alloc] initWithDocument:currentShift]; // create tmp
        [shiftDrawer addActitvity:activity];
         [self openCurrentCashDrawer];
        return YES;
    }else{
        return  NO;
    }
}
@end
