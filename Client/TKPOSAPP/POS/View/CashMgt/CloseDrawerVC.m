//
//  CloseDrawerVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/26/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "CloseDrawerVC.h"
#import "Constant.h"
#import "CashierBusinessModel.h"

@interface CloseDrawerVC ()

@end

@implementation CloseDrawerVC{
}
-(instancetype)initWithExpectedCash:(double)expectedCash_ currency:(NSString *)currency_{
    if (self =[super init]) {
        currency=currency_;
        expectedCash=expectedCash_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _tfActual.text=@"";
    _tfActual.placeholder=[TKAmountDisplay stringAmount:0 withCurrency:currency];
    [_vMain setShadowDefault];
    _vInput.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vInput.layer.borderColor=tkColorFrameBorder.CGColor;
    _vInput.layer.borderWidth=1;
    
    _btClose.layer.cornerRadius=tkCornerRadiusButton;
    _btClose.backgroundColor=[UIColor redColor];
    self.view.backgroundColor=[UIColor clearColor];
    _vMain.backgroundColor=[UIColor whiteColor];
    _vInput.backgroundColor=[UIColor whiteColor];
    _vLine.backgroundColor=tkColorFrameBorder;
    UIView *vCorver=[[UIView alloc] initWithFrame:self.view.bounds];
    [vCorver setViewCoverDefault];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [vCorver addGestureRecognizer:tap];
    [self.view insertSubview:vCorver belowSubview:_vMain];
    
    [self showMessageError:@""];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    _lbExpectedAmount.text=[TKAmountDisplay stringAmount:expectedCash withCurrency:currency];
    _lbAmountDifferent.text=[TKAmountDisplay stringAmount:0 withCurrency:currency];
    LanguageUtil *languageKey=[LanguageUtil sharedLanguageUtil];
    _lbTitle.text=[languageKey stringByKey:@"cash-mgt.close-drawer.title"];
    _lbTitleExpected.text=[languageKey stringByKey:@"cash-mgt.close-drawer.exprected-in-drawer"];
    _lbTitleActual.text=[languageKey stringByKey:@"cash-mgt.close-drawer.actual"];
    _lbTitleDifferent.text=[languageKey stringByKey:@"cash-mgt.close-drawer.different"];
    [_btClose setTitle:[languageKey stringByKey:@"general.bt.close"] forState:UIControlStateNormal];
}
-(void)viewDidAppear:(BOOL)animated{
    [_tfActual becomeFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)showMessageError:(NSString*)message{
    _lbMessageStatus.text=message;
}
-(void)clearMessageStatus{
    _lbMessageStatus.text=@"";
}
-(void)tapCoverView:(id)sender{
    [self clickCancel:nil];
}
- (IBAction)clickCancel:(id)sender {
    if (_delegate) {
        [_delegate closeDrawer:self cancel:YES];
    }
}
- (IBAction)clickClose:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self clearMessageStatus];
    if (_delegate) {
        double closingCash=[_tfActual.text doubleValue];
        [_delegate closeDrawer:self closingCash:closingCash];
    }
}
-(BOOL)validateInput{
    if ([_tfActual isEmptyText]) {
        [self showMessageError:[[LanguageUtil sharedLanguageUtil] stringByKey:@"cash-mgt.pay-in-out.empty-amount"]];
        return NO;
    }
    return YES;
}
-(void)updateDifferentAmountWithActual:(double)actualAmount{
    double different =[TKAmountDisplay tkRoundCash:(actualAmount - expectedCash)];
    if (different<0) {
        _lbAmountDifferent.text=[NSString stringWithFormat:@"- %@",[TKAmountDisplay stringAmount:-different withCurrency:currency]];
    }else{
        _lbAmountDifferent.text=[TKAmountDisplay stringAmount:different withCurrency:currency];
    }

}
#pragma TextfieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    if (textField==_tfActual) {
        NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
        if (![newString isTKAmountFormat]) {
            return NO;
        }
        double closingCashNew=[newString doubleValue];
        [self updateDifferentAmountWithActual:closingCashNew];
    }
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y-= 160;
    [_vMain setFrame:frame];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y+= 160;
    [_vMain setFrame:frame];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}
@end
