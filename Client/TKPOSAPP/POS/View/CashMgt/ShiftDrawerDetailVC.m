//
//  ShiftDrawerDetailVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define SpaceCellY 10
#define HeightCell 35
#define SpaceCellX 80
#import "ShiftDrawerDetailVC.h"
#import "Constant.h"
#import "CellViewAmount.h"
#import "CashierBusinessModel.h"

@interface ShiftDrawerDetailVC ()

@end

@implementation ShiftDrawerDetailVC{
    LanguageUtil *languageKey;
    
    NSMutableArray *arrayCellAmount;
    NSMutableArray *arrayLine;
    UIButton *btDeposit;
    UIButton *btPayOut;
}
- (IBAction)clickStartClose:(UIButton*)sender {
    // button StartClose
    // tag =100 : start button
    // tag= -100  :close button
    if (sender.tag==100) {
        if (_delegate) {
            [_delegate shiftDrawerDetail:self startNewShift:YES];
        }
    }else if(sender.tag==-100){
        if (_delegate) {
            [_delegate shiftDrawerDetail:self closeCurrentShift:YES];
        }
    }
}
-(instancetype)initEmptyWithCurrency:(NSString *)currency_ enableStartClose:(BOOL)startClose{
    if (self =[super init]) {
        currency=currency_;
        enablePaidInOut=NO;
        enableStartClose=startClose;
    }
    return self;
}
-(instancetype)initWithShiftDrawer:(CBLDocument *)shiftDrawerDoc_ enableStartClose:(BOOL)startClose{
    if (self =[super init]) {
        enableStartClose=startClose;
        shiftDrawer =[[ShiftDrawer alloc] initWithDocument:shiftDrawerDoc_];
        currency = shiftDrawer.currency;
        enablePaidInOut=!shiftDrawer.closed;
        arrayCellAmount=[[NSMutableArray alloc] init];
        arrayLine=[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    languageKey=[LanguageUtil sharedLanguageUtil];
    arrayCellAmount=[[NSMutableArray alloc] init];
    arrayLine=[[NSMutableArray alloc] init];
    
    _btStartCloseDrawer.layer.cornerRadius=tkCornerRadiusButton;
    _btStartCloseDrawer.backgroundColor=tkColorMainAccept;
    btDeposit=[[UIButton alloc] init];
    btDeposit.layer.cornerRadius=tkCornerRadiusButton;
    [btDeposit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btDeposit setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [btDeposit setBackgroundColor:tkColorMainAccept];
    [btDeposit.titleLabel setFont:tkFontMainTitleButton];
    [btDeposit addTarget:self action:@selector(clickDeposit:) forControlEvents:UIControlEventTouchUpInside];
    
    btPayOut=[[UIButton alloc] init];
    btPayOut.layer.cornerRadius=tkCornerRadiusButton;
    [btPayOut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btPayOut setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [btPayOut setBackgroundColor:tkColorMainAccept];
    [btPayOut.titleLabel setFont:tkFontMainTitleButton];
    [btPayOut addTarget:self action:@selector(clickPayOut:) forControlEvents:UIControlEventTouchUpInside];
    languageKey=[LanguageUtil sharedLanguageUtil];
    [btDeposit setTitle:[languageKey stringByKey:@"cash-mgt.dt.bt-deposit"] forState:UIControlStateNormal];
    [btPayOut setTitle:[languageKey stringByKey:@"cash-mgt.dt.bt-pay-out"] forState:UIControlStateNormal];
    // show button start close
    if (enableStartClose) {
        _btStartCloseDrawer.hidden=NO;
        if (shiftDrawer==nil || shiftDrawer.closed) {
            _btStartCloseDrawer.tag=100;
            [_btStartCloseDrawer setTitle:[languageKey stringByKey:@"cash-mgt.dt.bt-start-drawer"] forState:UIControlStateNormal];
        }else{
            _btStartCloseDrawer.tag=-100;
            [_btStartCloseDrawer setTitle:[languageKey stringByKey:@"cash-mgt.dt.bt-close-drawer"] forState:UIControlStateNormal];
        }
    }else{
        _btStartCloseDrawer.hidden=YES;
    }
    
    // show lable status
    // title starting, closed
    if (shiftDrawer) {
        _lbTimeStartClose.hidden=NO;
        NSString *timeStartClose;
        if (shiftDrawer.closed) {
            timeStartClose=[NSString stringWithFormat:@"%@: %@",[languageKey stringByKey:@"cash-mgt.dt.closed-date"],[shiftDrawer.closedDate getStringWithFormat:@"dd MMM yyyy - HH:mm:ss"]];
        }else{
            timeStartClose=[NSString stringWithFormat:@"%@: %@",[languageKey stringByKey:@"cash-mgt.dt.starting-date"],[shiftDrawer.startedDate getStringWithFormat:@"dd MMM yyyy - HH:mm:ss"]];
        }
        _lbTimeStartClose.text=timeStartClose;
    }else{
        _lbTimeStartClose.hidden=YES;
    }
    // add information amount
    [self addAmount];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)addAmount{
    CGPoint startPoint=CGPointMake(SpaceCellX ,140);
    if (!enableStartClose) {
        startPoint.y-= 70;
    }
    CGRect frame=CGRectMake(startPoint.x, startPoint.y, 300, HeightCell);
    [_lbCash setFrame:frame];
    startPoint.y += _lbCash.frame.size.height;
    startPoint.y += SpaceCellY;

    NSArray *listAmount=@[@{@"title":[languageKey stringByKey:@"cash-mgt.dt.starting-cash"],@"value":[TKAmountDisplay stringAmount:shiftDrawer.startingCash withCurrency:currency]},
                          @{@"title":[languageKey stringByKey:@"cash-mgt.dt.cash-tendered"],@"value":[TKAmountDisplay stringAmount:[shiftDrawer totalCashInWithType:CashInTypeTender] withCurrency:currency]},
                          @{@"title":[languageKey stringByKey:@"cash-mgt.dt.cash-change"],@"value":[TKAmountDisplay stringAmount:[shiftDrawer totalCashOutWithType:CashOutTypeChangeDue] withCurrency:currency]},
                          @{@"title":[languageKey stringByKey:@"cash-mgt.dt.cash-refund"],@"value":[TKAmountDisplay stringAmount:[shiftDrawer totalCashOutWithType:CashOutTypeRefund] withCurrency:currency]},
                          @{@"title":[languageKey stringByKey:@"cash-mgt.dt.paid-in"],@"value":[TKAmountDisplay stringAmount:[shiftDrawer totalCashInWithType:CashInTypeDeposit] withCurrency:currency]},
                          @{@"title":[languageKey stringByKey:@"cash-mgt.dt.paid-out"],@"value":[TKAmountDisplay stringAmount:[shiftDrawer totalCashOutWithType:CashOutTypePayOut] withCurrency:currency]},
                          @{@"title":[languageKey stringByKey:@"cash-mgt.dt.text-expected-in"],@"value":[TKAmountDisplay stringAmount:[shiftDrawer expectedTotal] withCurrency:currency]}
                          ];
    [arrayLine removeAllObjects];
    [arrayCellAmount removeAllObjects];
    for (NSDictionary *data in listAmount) {
        [arrayLine addObject:[self addLineCellToPoint:&startPoint]];
        [arrayCellAmount addObject:[self addAmountCellWithTitle:data[@"title"] value:data[@"value"] bold:NO toPoint:&startPoint]];
    }
    NSString *totalActual=@"";
    if (shiftDrawer.closingCash>=0) {
        totalActual=[TKAmountDisplay stringAmount:shiftDrawer.closingCash withCurrency:currency];
        [self addAmountCellWithTitle:[languageKey stringByKey:@"cash-mgt.dt.total-actual"] value:totalActual bold:YES toPoint:&startPoint];
    }
    
    if (enablePaidInOut) {
        // Paid In  index 4 ; add button deposit
        UIView *cellPaidIn =arrayCellAmount[4];
        CGRect frameBt=CGRectMake(150, 0, 3*HeightCell, HeightCell);
        [btDeposit setFrame:frameBt];
        [cellPaidIn addSubview:btDeposit];
        // Paid Out index 5
        UIView *cellPaidOut =arrayCellAmount[5];
        [btPayOut setFrame:frameBt];
        [cellPaidOut addSubview:btPayOut];
    }
}
-(void)reloadView{
    for (UIView *vv in arrayCellAmount) {
        [vv removeFromSuperview];
    }
    [arrayCellAmount removeAllObjects];
    for (UIView *vv in arrayLine) {
        [vv removeFromSuperview];
    }
    [arrayLine removeAllObjects];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIView*)addLineCellToPoint:(CGPoint*)pointStart{
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake((*pointStart).x, (*pointStart).y, self.view.frame.size.width -(*pointStart).x - SpaceCellX, 1)];
    [line setBackgroundColor:tkColorFrameBorder];
    [self.view addSubview:line];
    (*pointStart).y+=line.frame.size.height;
    (*pointStart).y+=SpaceCellY;
    return line;
}
-(CellViewAmount*)addAmountCellWithTitle:(NSString*)title value:(NSString*)value bold:(BOOL)bold toPoint:(CGPoint*)pointStart{
    CGRect frame=CGRectZero;
    frame.origin=(*pointStart);
    frame.size.width=self.view.frame.size.width-frame.origin.x-SpaceCellX;
    frame.size.height=HeightCell;
    CellViewAmount *cell=[[CellViewAmount alloc] initWithTitle:title value:value bold:bold];
    [cell setFrame:frame];
    [self.view addSubview:cell];
    (*pointStart).y+=cell.frame.size.height;
    (*pointStart).y+=SpaceCellY;
    return cell;
}
-(IBAction)clickDeposit:(id)sender{
    if (_delegate) {
        [_delegate shiftDrawerDetail:self deposit:YES];
    }
}
-(IBAction)clickPayOut:(id)sender{
    if (_delegate) {
        [_delegate shiftDrawerDetail:self payOut:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
