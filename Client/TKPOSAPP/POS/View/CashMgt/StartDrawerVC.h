//
//  StartDrawerVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/26/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol StartDrawerDelegate <NSObject>
@required
-(void)startDrawer:(id)sender startingCash:(double)startingCash;
-(void)startDrawer:(id)sender cancel:(BOOL)cancel;

@end

#import <UIKit/UIKit.h>

@interface StartDrawerVC : UIViewController{
    NSString *currency;
}
-(instancetype)initWithCurrency:(NSString*)currency_;
@property (weak,nonatomic) id<StartDrawerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIView *vInputTextfield;
@property (weak, nonatomic) IBOutlet UIButton *btStart;
- (IBAction)clickStart:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleStartCash;
- (IBAction)clickCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;

@end
