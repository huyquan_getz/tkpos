//
//  ShiftDrawerManager.h
//  POS
//
//  Created by Nha Duong Cong on 12/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShiftDrawer.h"

@interface ShiftDrawerManager : NSObject{
    CBLDocument *currentShift;
}
+(instancetype) sharedDrawerManager;
-(CBLDocument*)currentShiftDrawer;
-(CBLDocument *)startNewShiftDrawer:(double)cashInDrawer currency:(NSString*)currency;
-(CBLDocument*)closeCurrentShiftDrawer:(double)currentCashInDrawer notCheckDrawer:(BOOL)notCheckDrawer;
-(BOOL)addToCurrentShiftWithActivity:(ActitivityDrawer*)activity;
- (void) openCurrentCashDrawer;
@end
