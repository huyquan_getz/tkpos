//
//  CloseDrawerVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/26/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol CloseDrawerDelegate <NSObject>
@required
-(void)closeDrawer:(id)sender closingCash:(double)closingCash;
-(void)closeDrawer:(id)sender cancel:(BOOL)none;

@end
#import <UIKit/UIKit.h>

@interface CloseDrawerVC : UIViewController<UITextFieldDelegate>{
    NSString *currency;
    double expectedCash;
}
@property (weak, nonatomic) id<CloseDrawerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIView *vInput;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
- (IBAction)clickCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (weak, nonatomic) IBOutlet UIButton *btClose;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleDifferent;
@property (weak, nonatomic) IBOutlet UILabel *lbAmountDifferent;
- (IBAction)clickClose:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleActual;
@property (weak, nonatomic) IBOutlet UITextField *tfActual;
@property (weak, nonatomic) IBOutlet UILabel *lbExpectedAmount;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleExpected;
@property (weak, nonatomic) IBOutlet UIView *vLine;
-(instancetype)initWithExpectedCash:(double)expectedCas_ currency:(NSString*)currecy_;

@end
