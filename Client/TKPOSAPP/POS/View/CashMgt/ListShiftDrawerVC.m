//
//  ListShiftDrawerVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define MaxShiftDrawerQuery 1000
#define CellShiftIndentify @"cellShift"
#import "ListShiftDrawerVC.h"
#import "TableCellShiftDrawer.h"
#import "Controller.h"
#import "Constant.h"
#import "ShiftDrawerManager.h"

@interface ListShiftDrawerVC ()

@end

@implementation ListShiftDrawerVC{
    LanguageUtil *languageKey;
}
-(instancetype)initWithYear:(NSInteger)year_ month:(NSInteger)month_{
    if (self =[super init]) {
        year=year_;
        month=month_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    listShifDrawer =[[NSMutableArray alloc] init];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellShiftDrawer" bundle:nil] forCellReuseIdentifier:CellShiftIndentify];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [self reloadShiftDrawerWithYear:year month:month];
}
-(void)reloadShiftDrawerWithYear:(NSInteger)year_ month:(NSInteger)month_{
    year=year_;
    month=month_;
    NSDate *monthYear=[NSDate dateWithYear:year month:month day:1 timeZone:[NSTimeZone localTimeZone]];
    [_btCalendar setTitle:[monthYear getStringWithFormat:@"MMM yyyy"] forState:UIControlStateNormal];
    [self reSetupQueryLive];
}
-(void)reSetupQueryLive{
    CBLQuery *query =[Controller queryShiftDrawerInYear:year month:month];
    if (query==nil) {
        return;
    }
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery =[query asLiveQuery];
    [liveQuery addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    liveQuery.limit=MaxShiftDrawerQuery;
    liveQuery.descending=YES;
    [liveQuery start];
}
-(void)removeQueryLive{
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery=nil;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object == liveQuery) {
        [self displayRows:liveQuery.rows];
    }
}
-(void)displayRows:(CBLQueryEnumerator*)enumRows{
    NSMutableArray *array =[[NSMutableArray alloc] init];
    for (CBLQueryRow *row in enumRows) {
        [array addObject:row.document];
        NSLog(@"%@",row.documentID);
    }
    [listShifDrawer removeAllObjects];
    [listShifDrawer addObjectsFromArray:array];
    [_tbvMain reloadData];
    // reselected cell
    CBLDocument *shiftDrawerSeleted =[_delegate listShiftDrawer:self requireDrawerSelected:YES];
    NSIndexPath *indextPathSelected;
    if (shiftDrawerSeleted) {
        NSInteger indexExisted=[listShifDrawer indexOfObject:shiftDrawerSeleted];
        if (indexExisted !=NSNotFound) {
            indextPathSelected=[NSIndexPath indexPathForRow:indexExisted inSection:0];
        }
    }
    if(indextPathSelected==nil && listShifDrawer.count>0){// have receipt
        indextPathSelected =[NSIndexPath indexPathForRow:0 inSection:0];
    }
    if (indextPathSelected) {
        [_tbvMain selectRowAtIndexPath:indextPathSelected animated:YES scrollPosition:UITableViewScrollPositionNone];
        [_delegate listShiftDrawer:self clickDetail:listShifDrawer[indextPathSelected.row]];
    }else{
        [_delegate listShiftDrawer:self clickDetail:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listShifDrawer.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellShiftDrawer *cell=[tableView dequeueReusableCellWithIdentifier:CellShiftIndentify];
    [cell setValueDefault];
    CBLDocument *shiftDrawerDoc=listShifDrawer[indexPath.row];
    ShiftDrawer *shiftDrawer =[[ShiftDrawer alloc] initWithDocument:shiftDrawerDoc];
    if ([ShiftDrawerManager sharedDrawerManager].currentShiftDrawer == shiftDrawerDoc) {
        cell.lbTitle.text=@"Current Shift";
//        cell.lbTitleTime.text=[NSString stringWithFormat:@"starting %@",[shiftDrawer.startedDate getStringWithFormat:@"HH:mm:ss"]];
    }else{
        cell.lbTitle.text=[shiftDrawer.modifiedDate getStringWithFormat:@"dd MMM yyyy"];
        cell.lbTitleTime.text=[NSString stringWithFormat:@"%@: %@",[languageKey stringByKey:@"cash-mgt.list.text-closed-time"],[shiftDrawer.modifiedDate getStringWithFormat:@"HH:mm:ss"]];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_delegate) {
        CBLDocument *shiftDrawer=listShifDrawer[indexPath.row];
        [_delegate listShiftDrawer:self clickDetail:shiftDrawer];
    }
}
- (IBAction)clickCalendar:(id)sender {
    if(_delegate){
        [_delegate listShiftDrawer:self clickCalender:YES];
    }
}
-(void)dealloc{
    [liveQuery removeObserver:self forKeyPath:@"rows"];
}
@end
