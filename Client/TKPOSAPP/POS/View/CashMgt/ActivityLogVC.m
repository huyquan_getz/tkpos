//
//  ActivityLogVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define CellActivityIndentify @"cellActivity"
#import "ActivityLogVC.h"
#import "TableCellActivityLog.h"
#import "CashierBusinessModel.h"

@interface ActivityLogVC ()

@end

@implementation ActivityLogVC{
}
-(instancetype)init{
    if (self =[super init]) {
        listActivity=[[NSMutableArray alloc] init];
    }
    return self;
}
-(id)initWithShiftDrawer:(CBLDocument *)shiftDrawerDoc_{
    if (self =[super init]) {
        shiftDrawer=[[ShiftDrawer alloc] initWithDocument:shiftDrawerDoc_];
        currency=shiftDrawer.currency;
        listActivity =[[NSMutableArray alloc] initWithArray:shiftDrawer.arrayActivity];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellActivityLog" bundle:nil] forCellReuseIdentifier:CellActivityIndentify];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    LanguageUtil *languageKey=[LanguageUtil sharedLanguageUtil];
    _lbDateTime.text=[languageKey stringByKey:@"cash-mgt.activity.date-time"];
    _lbStaff.text=[languageKey stringByKey:@"cash-mgt.activity.staff"];
    _lbCashIn.text=[languageKey stringByKey:@"cash-mgt.activity.cash-in"];
    _lbCashOut.text=[languageKey stringByKey:@"cash-mgt.activity.cash-out"];
    _lbDescription.text=[languageKey stringByKey:@"cash-mgt.activity.description"];
    _lbType.text=[languageKey stringByKey:@"cash-mgt.activity.type"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listActivity.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellActivityLog *cell=[tableView dequeueReusableCellWithIdentifier:CellActivityIndentify];
    [cell setValueDefault];
    ActitivityDrawer *activity =(ActitivityDrawer*)listActivity[indexPath.row];
    cell.lbDateTime.text=[activity.createdDate getStringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    cell.lbStaff.text=activity.employeeName;
    if (activity.cashInType!=CashInTypeNone) {
        cell.lbCashIn.text=[TKAmountDisplay stringAmount:activity.cashInValue withCurrency:currency];
    }
    if (activity.cashOutType!=CashOutTypeNone) {
        cell.lbCashOut.text=[TKAmountDisplay stringAmount:activity.cashOutValue withCurrency:currency];
    }
    cell.lbDescription.text=activity.comment;
    cell.lbType.text=activity.type;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return _vTableHeader;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
