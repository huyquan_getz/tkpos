//
//  TableCellPrinting.h
//  POS
//
//  Created by Cong Nha Duong on 1/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellPrinting : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbPrinter;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (weak, nonatomic) IBOutlet UIImageView *ivNext;
-(void)setValueDefault;
@end
