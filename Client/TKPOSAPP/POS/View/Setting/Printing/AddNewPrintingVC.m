//
//  AddNewPrintingVC.m
//  POS
//
//  Created by Cong Nha Duong on 1/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define MaxHeightTableEdit 450
#define MaxHeightTableAdd 500
#define HeighCell 50
#define CellChooseCategoryIdentify @"cellChooseCategory"
#import "AddNewPrintingVC.h"
#import "Constant.h"
#import "TableCellChooseCategory.h"
#import "Controller.h"
#import "Printer.h"
#import "TKCategory.h"
#import "TKItem.h"

@interface AddNewPrintingVC ()

@end

@implementation AddNewPrintingVC{
    NSInteger maxHeightTableCategory;
    LanguageUtil *languageKey;
    BOOL enableSelectCategory;
}
-(instancetype)initWithPrinting:(CBLDocument *)printing{
    if (self =[super init]) {
        printingTypeSelect=PrintingTypeNone;
        listPrinter =[[NSMutableArray alloc] init];
        listCategory=[[NSMutableArray alloc] init];
        listCategorySelected=[[NSMutableArray alloc] init];
        printingDoc=printing;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
     _ivNavBar.backgroundColor=[UIColor statusNaviBar];
    _vRequirePrinter.hidden=YES;
    _vRequireReceiptType.hidden=YES;
    _tbvCategory.clipsToBounds=YES;
    enableSelectCategory=YES;
    _tbvCategory.layer.cornerRadius=tkCornerRadiusViewPopup;
    _tbvCategory.layer.borderColor=tkColorFrameBorder.CGColor;
    _tbvCategory.layer.borderWidth=1;
    _vLine.backgroundColor=tkColorFrameBorder;
    _vInput.backgroundColor=[UIColor clearColor];
    _vInput.layer.borderWidth=1;
    _vInput.layer.borderColor=tkColorFrameBorder.CGColor;
    _vInput.layer.cornerRadius=tkCornerRadiusViewPopup;
    _btRemove.layer.cornerRadius=tkCornerRadiusButton;
    [_btChoosePrinter setTitle:nil forState:UIControlStateNormal];
    [_btChooseType setTitle:nil forState:UIControlStateNormal];
    [_tbvCategory registerNib:[UINib nibWithNibName:@"TableCellChooseCategory" bundle:nil] forCellReuseIdentifier:CellChooseCategoryIdentify];
    CGRect frame=_vCombine.frame;
    frame.origin.x=_vInput.frame.origin.x;
    frame.origin.y=_vInput.frame.origin.y+_vInput.frame.size.height+15;
    _vCombine.frame=frame;
    [self.view addSubview:_vCombine];
    _vCombine.hidden=YES;
    _vCombine.layer.borderColor=tkColorFrameBorder.CGColor;
    _vCombine.layer.borderWidth=1;
    _vCombine.layer.cornerRadius=tkCornerRadiusButton;
    [_swCombine setOnTintColor:tkColorMainSelected];

    if (printingDoc) {
        maxHeightTableCategory=MaxHeightTableEdit;
        _btRemove.hidden=NO;
    }else{
        maxHeightTableCategory=MaxHeightTableAdd;
        _btRemove.hidden=YES;
    }
    if (printingDoc) {
        printerSelected=[Printing getPrinter:printingDoc];
        if ([printerSelected isDeleted]) {
            printerSelected=nil;
        }
        printingTypeSelect=[Printing getPrintingType:printingDoc];
        [listCategorySelected removeAllObjects];
        for (NSString *categoryID in [Printing getArrayCategoryId:printingDoc]) {
            [listCategorySelected addObject:[[SyncManager sharedSyncManager] documentWithDocumentId:categoryID]];
        }
        [_btChooseType setTitle:[Printing getPrintingDisplayWithPrintingType:printingTypeSelect] forState:UIControlStateNormal];
        [_btChoosePrinter setTitle:[Printer getPrinterName:printerSelected] forState:UIControlStateNormal];
        [_swCombine setOn:[Printing getCombine:printingDoc]];
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [super viewWillAppear:animated];
    if (printingDoc) {
        _lbTitle.text=[languageKey stringByKey:@"setting.printing.edit.title"];
    }else{
        _lbTitle.text=[languageKey stringByKey:@"setting.printing.add.title"];;
    }
    [self resetFrameTableView];
}
-(void)viewDidAppear:(BOOL)animated{
    [self loadDataFirst];
}
-(void)resetFrameTableView{
    CGRect frame=_tbvCategory.frame;
    NSInteger heightContent=listCategory.count * HeighCell;
    _tbvCategory.scrollEnabled=NO;
    if (heightContent>maxHeightTableCategory) {
        heightContent=maxHeightTableCategory;
        _tbvCategory.scrollEnabled=YES;
    }
    frame.size.height=heightContent;
    [_tbvCategory setFrame:frame];
}
-(void)loadDataFirst{
    self.view.userInteractionEnabled=NO;
    [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
        CBLQuery *queryPrinter=[Controller queryGetAllPrinter];
        CBLQueryEnumerator *enumRowPrinter=[queryPrinter run:nil];
        NSMutableArray *newPrinter=[[NSMutableArray alloc] init];
        for (CBLQueryRow *row in enumRowPrinter) {
            [newPrinter addObject:row.document];
        }
        [listPrinter removeAllObjects];
        [listPrinter addObjectsFromArray:newPrinter];

        CBLQuery *query=[Controller queryGetAllCategoryDocuments];
        query.descending=YES;
        CBLQueryEnumerator *enumRow=[query run:nil];
        NSMutableArray *newCategory=[[NSMutableArray alloc] init];
        [newCategory addObject:[[SyncManager sharedSyncManager] documentWithDocumentId:[TKCategory categoryIdAdhocDefault]]];
        for (CBLQueryRow *row in enumRow) {
            [newCategory addObject:row.document];
        }
        [listCategory removeAllObjects];
        [listCategory addObjectsFromArray:newCategory];
        if (printingDoc==nil) {
            [listCategorySelected removeAllObjects];
            [listCategorySelected addObjectsFromArray:listCategory];
        }
        [_tbvCategory reloadData];
        [self resetFrameTableView];
        [self resetSelectedCategoryEnable];
        [self resetFrameCheckCombine];
        
        self.view.userInteractionEnabled=YES;
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickSave:(id)sender {
    if (![self validateInput]) {
        return;
    }
    BOOL combine=_swCombine.isOn;
    if(printingDoc){
        [Printing updatePrinting:printingDoc withType:printingTypeSelect printer:printerSelected listCategory:listCategorySelected combine:combine];
    }else{
        [Printing createPrintingWithType:printingTypeSelect printer:printerSelected listCategory:listCategorySelected combine:combine];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickChooseType:(UIButton*)sender {
    _vRequireReceiptType.hidden=YES;
    if (_popoverListType) {
        [_popoverListType dismissPopoverAnimated:YES];
    }
    _popoverListType=[[PopoverListController alloc] initWithListString:[Printing getListPrintingTypeDefault]messageEmptyList:nil];
    _popoverListType.delegate=self;
    _popoverListType.listDelegate=self;
    CGRect frame=_vInput.frame;
    frame.size=CGSizeZero;
    frame.origin.x+=sender.frame.origin.x+sender.frame.size.width;
    frame.origin.y+=sender.frame.origin.y+sender.frame.size.height/2+10;
    [_popoverListType presentPopoverFromRect:frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
- (IBAction)clickChoosePrinter:(UIButton*)sender {
    _vRequirePrinter.hidden=YES;
    if (_popoverListPrinter) {
        [_popoverListPrinter dismissPopoverAnimated:YES];
    }
    NSMutableArray *listPrinterName=[[NSMutableArray alloc] init];
    for (CBLDocument *printer in listPrinter) {
        [listPrinterName addObject:[Printer getPrinterName:printer]];
    }
    _popoverListPrinter=[[PopoverListController alloc] initWithListString:listPrinterName messageEmptyList:[languageKey stringByKey:@"setting.printer-list.lb-no-printer"]];
    _popoverListPrinter.delegate=self;
    _popoverListPrinter.listDelegate=self;
    CGRect frame=_vInput.frame;
    frame.size=CGSizeZero;
    frame.origin.x+=sender.frame.origin.x+sender.frame.size.width;
    frame.origin.y+=sender.frame.origin.y+sender.frame.size.height/2+10;
    [_popoverListPrinter presentPopoverFromRect:frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
-(void)popoverList:(id)sender dismissWithCellIndex:(NSInteger)cellIndex valude:(NSString *)value{
    if (sender==_popoverListType) {
        printingTypeSelect=(PrintingType)cellIndex;
        [_btChooseType setTitle:value forState:UIControlStateNormal];
        [self resetSelectedCategoryEnable];
        [self resetFrameCheckCombine];
    }else if (sender == _popoverListPrinter){
        printerSelected=listPrinter[cellIndex];
        [_btChoosePrinter setTitle:value forState:UIControlStateNormal];
    }
    [_popoverListPrinter dismissPopoverAnimated:NO];
}
-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    if (popoverController == _popoverListType) {
        _popoverListType=nil;
    }else if (popoverController==_popoverListPrinter) {
        _popoverListPrinter=nil;
    }
}

#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listCategory.count + 1;// first object is choose all category
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellChooseCategory *cell=[tableView dequeueReusableCellWithIdentifier:CellChooseCategoryIdentify];
    [cell setValueDefault];
    CBLDocument *category=[self categoryWithIndexPath:indexPath];// cell0 all category
    if (indexPath.row==0) {
        cell.lbTitle.text=[languageKey stringByKey:@"setting.printing.all-category"];
        [cell setTick:(listCategorySelected.count==listCategory.count)];
        cell.lbTitle.textColor=[UIColor blackColor];
    }else{
        cell.lbTitle.text=[TKCategory getName:category];
        [cell setTick:[listCategorySelected containsObject:category]];
        cell.lbTitle.textColor=[UIColor grayColor];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellChooseCategory *cell=(TableCellChooseCategory*)[tableView cellForRowAtIndexPath:indexPath];
    CBLDocument *category=[self categoryWithIndexPath:indexPath];// cell0 all category
    BOOL tick=![cell currentTick];
    [cell setTick:tick];
    if (indexPath.row==0) {
        // tick all category;
        [listCategorySelected removeAllObjects];
        if (tick) {
            [listCategorySelected addObjectsFromArray:listCategory];
        }
        for (TableCellChooseCategory *currentCell in [tableView visibleCells]) {
            [currentCell setTick:tick];
        }
    }else{
        if (tick) {
            [listCategorySelected addObject:category];
        }else{
            [listCategorySelected removeObject:category];
        }
        // check selected all category
        TableCellChooseCategory *cell0=(TableCellChooseCategory*)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell0 setTick:(listCategorySelected.count==listCategory.count)];
        
    }
}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return enableSelectCategory;
}

-(CBLDocument *)categoryWithIndexPath:(NSIndexPath*)indexPath{
    if (indexPath.row==0) {
        return nil ;
    }else{
        return listCategory[indexPath.row-1];
    }
}
-(BOOL)validateInput{
    BOOL validate=YES;
    if (printingTypeSelect==PrintingTypeNone) {
        _vRequireReceiptType.hidden=NO;
        validate=NO;
    }
    if (printerSelected==nil) {
        _vRequirePrinter.hidden=NO;
        validate=NO;
    }
    return validate;
    
}
-(void)resetSelectedCategoryEnable{
    BOOL enableSelect=NO;
    if (printingTypeSelect==PrintingTypeNone || printingTypeSelect== PrintingTypeSingleOrderChit) {
        enableSelect=YES;
    }
    enableSelectCategory=enableSelect;
    if (!enableSelectCategory) {// selected all;
        [listCategorySelected removeAllObjects];
        [listCategorySelected addObjectsFromArray:listCategory];
        for (TableCellChooseCategory *currentCell in [_tbvCategory visibleCells]) {
            [currentCell setTick:YES];
        }
    }
}
-(void)resetFrameCheckCombine{
    NSInteger positionLbSelect=208;
    NSInteger positionTbv=237;
    NSInteger heightTbv=maxHeightTableCategory;
    if (printingTypeSelect==PrintingTypeSingleOrderChit) {
        positionLbSelect+=65;
        positionTbv+=65;
        heightTbv-=50;
        _vCombine.hidden=NO;
    }else{
        _vCombine.hidden=YES;
    }
    CGRect frame=_lbTitleSelect.frame;
    frame.origin.y=positionLbSelect;
    _lbTitleSelect.frame=frame;
    
    frame=_tbvCategory.frame;
    frame.origin.y=positionTbv;
    frame.size.height=heightTbv;
    _tbvCategory.frame=frame;
}
- (IBAction)clickRemove:(id)sender {
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"setting.printing.ms-confirm-remove"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            [Printing removePrinting:printingDoc];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}
- (IBAction)clickSwitchCombine:(id)sender {
}
@end
