//
//  CashDrawerConfigVC.m
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define MaxLengthDrawerName 50
#import "CashDrawerConfigVC.h"
#import "Constant.h"
#import "CashDrawer.h"
#import "PrinterController.h"
@interface CashDrawerConfigVC ()

@end

@implementation CashDrawerConfigVC{
    LanguageUtil *languageKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _ivNaviBar.backgroundColor=[UIColor statusNaviBar];
    _vLine1.backgroundColor=tkColorFrameBorder;
    _vLine2.backgroundColor=tkColorFrameBorder;
    _vInput.backgroundColor=[UIColor clearColor];
    _vInput.layer.borderWidth=1;
    _vInput.layer.borderColor=tkColorFrameBorder.CGColor;
    _vInput.layer.cornerRadius=tkCornerRadiusViewPopup;
    _btTestConnect.layer.cornerRadius=tkCornerRadiusButton;
    _btTestConnect.backgroundColor=tkColorMainAccept;

    [self showMessageError:@""];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    _lbTitle.text=[languageKey stringByKey:@"setting.drawer.title"];
    _lbTitleDrawerName.text=[languageKey stringByKey:@"setting.drawer.lb-drawer-name"];
    _lbTitleIPAddress.text=[languageKey stringByKey:@"setting.drawer.lb-ip-address"];
    _lbTitlePort.text=[languageKey stringByKey:@"setting.drawer.lb-port"];
    _tfDrawerName.placeholder=[languageKey stringByKey:@"setting.drawer.holder-drawer-name"];
    _tfIPAddress.placeholder=[languageKey stringByKey:@"setting.drawer.holder-ip-address"];
    _tfPort.placeholder=[languageKey stringByKey:@"setting.drawer.holder-port"];
    _tfDrawerName.text=[[CashDrawer sharedCashDrawer] getCashDrawerName
                        ];
    _tfIPAddress.text=[[CashDrawer sharedCashDrawer] getIpAddress];
    NSInteger port=[[CashDrawer sharedCashDrawer] getPort];
    if (port>=0) {
        _tfPort.text=[NSString stringWithFormat:@"%li",(long)port];
    }else{
        _tfPort.text=@"";
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showMessageError:(NSString*)error{
    [_lbMessageStatus setTextColor:tkColorStatusMessageError];
    _lbMessageStatus.text=error;
}
-(void)showMessageSuccess:(NSString*)message{
    [_lbMessageStatus setTextColor:tkColorStatusMessageSuccess];
    _lbMessageStatus.text=message;
}
-(void)clearMessageError{
    _lbMessageStatus.text=@"";
}
- (IBAction)clickSave:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self clearMessageError];
    [self.view endEditing:YES];
    
    if (![self checkChangeInput]) {// no change
        return;
    }
    NSInteger port=-1;
    if (![_tfPort isEmptyText]) {
        port=[_tfPort.text integerValue];
    }
    [[CashDrawer sharedCashDrawer] updateCashDrawerWithName:_tfDrawerName.text ip:_tfIPAddress.text port:port];
}
-(BOOL)checkChangeInput{
    
    if (![[[CashDrawer sharedCashDrawer] getCashDrawerName] isEqualToString:_tfDrawerName.text]) {
        return YES;
    }
    if (![[[CashDrawer sharedCashDrawer] getIpAddress] isEqualToString:_tfIPAddress.text]) {
        return YES;
    }
    NSInteger port=-1;
    if (![_tfPort isEmptyText]) {
        port=[_tfPort.text integerValue];
    }
    if ([[CashDrawer sharedCashDrawer] getPort] != port) {
        return YES;
    }
    return NO;
}
#pragma TextfieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == _tfDrawerName) {
        if (newString.length>MaxLengthDrawerName) {
            return NO;
        }
        return YES;
    }else if(textField == _tfIPAddress){
        return [newString isPrefixIPv4AddressFormat];
    }else if(textField == _tfPort){
        return [newString isPortFormat];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (textField == _tfDrawerName) {
        [_tfIPAddress becomeFirstResponder];
    }else if (textField== _tfIPAddress){
        [_tfPort becomeFirstResponder];
    }else if (textField==_tfPort){
        [self clickSave:nil];
    }
    return YES;
}
-(BOOL)validateInput{
    if ([_tfIPAddress isEmptyText]) {
        [self showMessageError:[languageKey stringByKey:@"setting.drawer.ms-empty-ip-address"]];
        return NO;
    }
    if ([_tfPort isEmptyText]) {
        [self showMessageError:[languageKey stringByKey:@"setting.drawer.ms-empty-port"]];
        return NO;
    }
    if (![_tfIPAddress.text isIPv4AddressFormat]) {
        [self showMessageError:[languageKey stringByKey:@"setting.drawer.ms-ip-invalid"]];
        return NO;
    }
    if (![_tfPort.text isPortFormat]) {
        [self showMessageError:[languageKey stringByKey:@"setting.drawer.ms-port-invalid"]];
        return NO;
    }
    return YES;
}


- (IBAction)clickTestCashDrawer:(id)sender{
    if ([self validateInput]) {
        [self clearMessageError];
        [[PrinterController sharedInstance] openDrawerOpenWithIP:_tfIPAddress.text];
    }
}
@end
