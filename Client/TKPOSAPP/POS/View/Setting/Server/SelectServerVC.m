//
//  SelectServerVC.m
//  POS
//
//  Created by Cong Nha Duong on 3/3/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define CellServerIdentify @"cellServerName"
#import "SelectServerVC.h"
#import "TableCellServer.h"
#import "Constant.h"

@interface SelectServerVC ()

@end

@implementation SelectServerVC
-(instancetype)init{
    if (self =[super init]) {
        listServerName=[[NSMutableArray alloc] init];
        [listServerName addObject:@"http://admin.demo.smoovpos-dev.com"];
        [listServerName addObject:@"http://192.168.1.69"];
        [listServerName addObject:@"http://192.168.1.60"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellServer" bundle:nil] forCellReuseIdentifier:CellServerIdentify];
    _btDone.layer.cornerRadius=tkCornerRadiusButton;
    _tbvMain.layer.borderWidth=1;
    _tbvMain.layer.borderColor=tkColorFrameBorder.CGColor;
    _tbvMain.layer.cornerRadius=tkCornerRadiusViewPopup;
    [self showMessageError:@""];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listServerName.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellServer *cell=(TableCellServer*)[tableView dequeueReusableCellWithIdentifier:CellServerIdentify];
    cell.lbTitle.text=listServerName[indexPath.row];
    cell.ivTick.hidden=(indexPathSelected==nil || indexPath.row!=indexPathSelected.row);
    return cell ;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPathSelected && indexPathSelected.row==indexPath.row) {
        return;
    }
    NSIndexPath *oldIndexPath=indexPathSelected;
    indexPathSelected=indexPath;
    [tableView reloadRowsAtIndexPaths:@[indexPathSelected] withRowAnimation:UITableViewRowAnimationNone];
    if (oldIndexPath) {
        [tableView reloadRowsAtIndexPaths:@[oldIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    _tfServerName.text=listServerName[indexPathSelected.row];
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    [listServerName removeObjectAtIndex:indexPath.row];
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
}
-(void)selectCellWithText:(NSString*)text{
    NSInteger indexText=-1;
    for (NSInteger i=0;i<listServerName.count ;i++) {
        if ([text isEqualToString:listServerName[i]]) {
            indexText=i;
            break;
        }
    }
    NSIndexPath *oldIndexPathSelected=indexPathSelected;
    if (indexText>=0) {
        indexPathSelected=[NSIndexPath indexPathForRow:indexText inSection:0];
    }else{
        indexPathSelected=nil;
    }
    
    if (oldIndexPathSelected) {
        [_tbvMain reloadRowsAtIndexPaths:@[oldIndexPathSelected] withRowAnimation:UITableViewRowAnimationNone];
    }
    if (indexPathSelected) {
        [_tbvMain reloadRowsAtIndexPaths:@[indexPathSelected] withRowAnimation:UITableViewRowAnimationNone];
    }
}
#pragma Textfield
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    [self selectCellWithText:newString];
    return YES;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField{
    [self selectCellWithText:@""];
    return YES;
}
-(void)showMessageError:(NSString*)error{
    _lbMessage.text=error;
    _lbMessage.textColor=[UIColor redColor];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickDone:(id)sender {
    if (![self validateInput]) {
        return;
    }
    //else
    if (!indexPathSelected) {// not existed in list// add to list
        [listServerName addObject:_tfServerName.text];
        indexPathSelected=[NSIndexPath indexPathForRow:listServerName.count-1 inSection:0];
        [_tbvMain reloadData];
    }

}
-(BOOL)validateInput{
    [self showMessageError:@""];
    if ([_tfServerName isEmptyText]) {
        [self showMessageError:@"Please enter a server name to continue"];
        return NO;
    }
//    else if (![_tfServerName.text isURLFormat]){
//        [self showMessageError:@"Please enter a valid server name to continue"];
//        return NO;
//    }
    return YES;
}
@end
