//
//  TableCellServer.h
//  POS
//
//  Created by Cong Nha Duong on 3/3/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellServer : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ivTick;

@end
