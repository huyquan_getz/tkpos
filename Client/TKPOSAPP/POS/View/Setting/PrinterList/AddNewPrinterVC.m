//
//  AddNewPrinterVC.m
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define MaxLengthPrinterName 50
#import "AddNewPrinterVC.h"
#import "Constant.h"
#import "Printer.h"
@interface AddNewPrinterVC ()

@end

@implementation AddNewPrinterVC{
    LanguageUtil *languageKey;
}
-(instancetype)initWithPrinter:(CBLDocument *)printer{
    if (self =[super init]) {
        printerDoc=printer;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _ivNavBar.backgroundColor=[UIColor statusNaviBar];
    languageKey=[LanguageUtil sharedLanguageUtil];
    _vLine1.backgroundColor=tkColorFrameBorder;
    _vLine2.backgroundColor=tkColorFrameBorder;
    _vInput.backgroundColor=[UIColor clearColor];
    _vInput.layer.borderWidth=1;
    _vInput.layer.borderColor=tkColorFrameBorder.CGColor;
    _vInput.layer.cornerRadius=tkCornerRadiusViewPopup;
    _btTestConnect.layer.cornerRadius=tkCornerRadiusButton;
    _btTestConnect.backgroundColor=tkColorMainAccept;
    _activityITestConnection.hidden=YES;
    _btRemove.layer.cornerRadius=tkCornerRadiusButton;
    [self showMessageError:@""];
    if (printerDoc) {
        _btRemove.hidden=NO;
        _tfPrinterName.text=[Printer getPrinterName:printerDoc];
        _tfIpAddress.text=[Printer getIpAddress:printerDoc];
        _tfPort.text=[NSString stringWithFormat:@"%li",(long)[Printer getPort:printerDoc]];
    }else{
        _btRemove.hidden=YES;
    }
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    _lbTitle.text=[languageKey stringByKey:@"setting.printer-list.new.title"];
    _lbTitlePrinterName.text=[languageKey stringByKey:@"setting.printer-list.lb-printer-name"];
    _lbTitleIpAddress.text=[languageKey stringByKey:@"setting.printer-list.lb-ip-address"];
    _lbTitlePort.text=[languageKey stringByKey:@"setting.printer-list.lb-port"];
    _tfPrinterName.placeholder=[languageKey stringByKey:@"setting.printer-list.holder-printer-name"];
    _tfIpAddress.placeholder=[languageKey stringByKey:@"setting.printer-list.holder-ip-address"];
    _tfPort.placeholder=[languageKey stringByKey:@"setting.printer-list.holder-port"];
    [_btTestConnect setTitle:[languageKey stringByKey:@"setting.printer-list.bt-test-connect"] forState:UIControlStateNormal];
    [_btRemove setTitle:[languageKey stringByKey:@"setting.printer-list.bt-remove-printer"] forState:UIControlStateNormal];
    if (printerDoc) {
        _activityITestConnection.hidden=NO;
        [_activityITestConnection startAnimating];
        dispatch_queue_t myQueue=dispatch_queue_create("queue.checkonoff", nil);
        dispatch_async(myQueue, ^{
            BOOL on=[[PrinterController sharedInstance]checkConnectionWithIPAddress:[Printer getIpAddress:printerDoc] port:[Printer getPort:printerDoc]];
            dispatch_async(dispatch_get_main_queue(), ^{
                _activityITestConnection.hidden=YES;
                if (on) {
                    [self showMessageSuccess:[languageKey stringByKey:@"setting.printer-list.status-online"]];
                }else{
                    [self showMessageError:[languageKey stringByKey:@"setting.printer-list.status-offline"]];
                }
            });
        });
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)showMessageError:(NSString*)error{
    [_lbMessageStatus setTextColor:tkColorStatusMessageError];
    _lbMessageStatus.text=error;
}
-(void)showMessageSuccess:(NSString*)message{
    [_lbMessageStatus setTextColor:tkColorStatusMessageSuccess];
    _lbMessageStatus.text=message;
}
-(void)clearMessageError{
    _lbMessageStatus.text=@"";
}
- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)clickTestConnection:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self clearMessageError];
    [self.view endEditing:YES];
    [[PrinterController sharedInstance] processingPrint];
    _activityITestConnection.hidden=NO;
    [_activityITestConnection startAnimating];
    self.view.userInteractionEnabled=NO;
    dispatch_queue_t myQueue=dispatch_queue_create("queue.checkonoff", nil);
    dispatch_async(myQueue, ^{
        BOOL on=[[PrinterController sharedInstance] checkConnectionWithIPAddress:_tfIpAddress.text port:[_tfPort.text integerValue]];
        dispatch_async(dispatch_get_main_queue(), ^{
            _activityITestConnection.hidden=YES;
            self.view.userInteractionEnabled=YES;
            if (on) {
                [self showMessageSuccess:[languageKey stringByKey:@"setting.printer-list.status-online"]];
            }else{
                [self showMessageError:[languageKey stringByKey:@"setting.printer-list.status-offline"]];
            }
        });
    });
}
- (IBAction)clickSave:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self clearMessageError];
    [self.view endEditing:YES];
    if ([_tfPrinterName isEmptyText]) {
        _tfPrinterName.text=_tfIpAddress.text;
    }
    if (printerDoc) {
        [Printer updatePrinter:printerDoc withName:_tfPrinterName.text ip:_tfIpAddress.text port:[_tfPort.text integerValue]];
    }else{
        [Printer createPrinterWithName:_tfPrinterName.text ip:_tfIpAddress.text port:[_tfPort.text integerValue]];   
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma TextfieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == _tfPrinterName) {
        if (newString.length>MaxLengthPrinterName) {
            return NO;
        }
        return YES;
    }else if(textField == _tfIpAddress){
        return [newString isPrefixIPv4AddressFormat];
    }else if(textField == _tfPort){
        return [newString isPortFormat];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (textField == _tfPrinterName) {
        [_tfIpAddress becomeFirstResponder];
    }else if (textField== _tfIpAddress){
        [_tfPort becomeFirstResponder];
    }else if (textField==_tfPort){
        [self clickTestConnection:nil];
    }
    return YES;
}
-(BOOL)validateInput{
    if ([_tfIpAddress isEmptyText]) {
        [self showMessageError:[languageKey stringByKey:@"setting.printer-list.ms-empty-ip-address"]];
        return NO;
    }
    if ([_tfPort isEmptyText]) {
        [self showMessageError:[languageKey stringByKey:@"setting.printer-list.ms-empty-port"]];
        return NO;
    }
    if (![_tfIpAddress.text isIPv4AddressFormat]) {
        [self showMessageError:[languageKey stringByKey:@"setting.printer-list.ms-ip-invalid"]];
        return NO;
    }
    if (![_tfPort.text isPortFormat]) {
        [self showMessageError:[languageKey stringByKey:@"setting.printer-list.ms-port-invalid"]];
        return NO;
    }
    return YES;
}

- (IBAction)clickRemove:(id)sender {
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"setting.printer-list.ms-confirm"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            [Printer removePrinter:printerDoc];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}
@end
