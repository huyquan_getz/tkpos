//
//  ViewCustomerVC.h
//  POS
//
//  Created by Cong Nha Duong on 3/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol ViewCustomerDelegate <NSObject>

-(void)viewCustomer:(id)sender clickEditCustomer:(NSDictionary*)customer;
@end

#import <UIKit/UIKit.h>

@interface ViewCustomerVC : UIViewController{
    NSDictionary *customer;
}
-(instancetype)initWithCustomer:(NSDictionary*)customer_;
@property (weak,nonatomic) id<ViewCustomerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
- (IBAction)clickEdit:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *bg1;
@property (weak, nonatomic) IBOutlet UILabel *bg2;
@property (weak, nonatomic) IBOutlet UILabel *bg3;
@property (weak, nonatomic) IBOutlet UILabel *bg4;
@property (weak, nonatomic) IBOutlet UIScrollView *scrMain;
@property (weak, nonatomic) IBOutlet UIImageView *ivAvata;
@property (weak, nonatomic) IBOutlet UITextField *tfMemberID;
@property (weak, nonatomic) IBOutlet UITextField *tfName;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPhoneNb;
@property (weak, nonatomic) IBOutlet UITextField *tfGender;
@property (weak, nonatomic) IBOutlet UITextField *tfBirthday;
@property (weak, nonatomic) IBOutlet UITextField *tfAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfZipcode;
@property (weak, nonatomic) IBOutlet UITextField *tfState;
@property (weak, nonatomic) IBOutlet UITextField *tfCountry;
@property (weak, nonatomic) IBOutlet UITextField *tfRemart;
@property (weak, nonatomic) IBOutlet UITextField *tfLastLogin;
@property (weak, nonatomic) IBOutlet UITextField *tfLastOrder;
@property (weak, nonatomic) IBOutlet UITextField *tfTotalOrder;
@property (weak, nonatomic) IBOutlet UITextField *tfTotalSpend;
@property (weak, nonatomic) IBOutlet UITextField *tfMemberSince;
@property (weak, nonatomic) IBOutlet UITextField *tfStatusAcceptNewsletter;
@property (weak, nonatomic) IBOutlet UITextField *tfStatusAcceptSMS;
@property (weak, nonatomic) IBOutlet UITextField *tfStatusActive;
@property (weak, nonatomic) IBOutlet UIButton *btEdit;

@end
