//
//  ListCustomerVC.m
//  POS
//
//  Created by Cong Nha Duong on 3/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define CellCustomerIdentify @"cellCustomer"
#import "ListCustomerVC.h"
#import "Constant.h"
#import "TableCellCustomer.h"

@interface ListCustomerVC ()

@end

@implementation ListCustomerVC{
    NSInteger countSearchChange;
}
-(instancetype)init{
    if (self =[super init]) {
        listCustomerOrginal=[[NSMutableArray alloc] init];
        listCustomerShow=[[NSMutableArray alloc] init];
        countSearchChange=0;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_searchBar setBarTintColor:[UIColor whiteColor]];
    _searchBar.layer.borderWidth=1;
    _searchBar.layer.borderColor=[UIColor whiteColor].CGColor;
    [_btCancelSearch setTitleColor:tkColorMain forState:UIControlStateNormal];
    _btCancelSearch.hidden=YES;
    _vLine.backgroundColor=tkColorFrameBorder;
    UITextField *textField=[self textFieldInView:_searchBar];
    if (textField) {
        [textField setBackgroundColor:[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1]];
    }
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellCustomer" bundle:nil] forCellReuseIdentifier:CellCustomerIdentify];
    [self reloadAllCustomer];
    // Do any additional setup after loading the view from its nib.
}
-(void)reloadAllCustomer{
    dispatch_queue_t myQueue=dispatch_queue_create("customer.loadall", nil);
    dispatch_async(myQueue, ^{
        listCustomerOrginal=[[self allCustomer] mutableCopy];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSArray *listCustomerInSection=[self listCustomerByFirstCharacterFrom:listCustomerOrginal];
            [self reShowListCustomer:listCustomerInSection];
        });
    });
}
-(void)reShowListCustomer:(NSArray*)customersInSection{
    [listCustomerShow removeAllObjects];
    [listCustomerShow addObjectsFromArray:customersInSection];
    [_tbvMain reloadData];
    [self resetSelectedCustomer];
}
-(UITextField*)textFieldInView:(UIView*)vvv{
    if ([vvv isKindOfClass:[UITextField class]]) {
        return (UITextField*)vvv;
    }else{
        for (UIView *vv in vvv.subviews) {
            UITextField *textField=[self textFieldInView:vv];
            if (textField) {
                return textField;
            }
        }
    }
    return nil;
}
-(NSArray*)allCustomer{
    CBLDocument *doc=[[SyncManager sharedSyncManager] documentWithDocumentId:@"test"];
    return doc.properties[@"list"];
}
-(NSArray*)listCustomerByFirstCharacterFrom:(NSArray*)list{
    if (list.count==0) {
        return @[];
    }
    NSArray *arrayShort=[list sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
        return [[obj1[@"firstName"] uppercaseString] compare:[obj2[@"firstName"] uppercaseString]];
    }];
    NSMutableArray *listByFirstCharacter=[[NSMutableArray alloc] init];
    NSInteger startIdex=0;
    NSInteger length=1;
    char charFirst=toupper([[arrayShort[startIdex] objectForKey:@"firstName"] characterAtIndex:0]);
    for (NSInteger i = 1; i<arrayShort.count; i++) {
        char charCurrent=toupper([[arrayShort[i] objectForKey:@"firstName"] characterAtIndex:0]);
        if (charFirst==charCurrent) {
            length ++;
        }else{
            [listByFirstCharacter addObject:[arrayShort subarrayWithRange:NSMakeRange(startIdex, length)]];
            startIdex=i;
            length=1;
            charFirst=toupper([[arrayShort[startIdex] objectForKey:@"firstName"] characterAtIndex:0]);
        }
    }
    [listByFirstCharacter addObject:[arrayShort subarrayWithRange:NSMakeRange(startIdex, length)]];// add last Date;
    return listByFirstCharacter;
}
-(void)resetSelectedCustomer{
    // reselected cell
    NSDictionary *customerShowing =[_delegate listCustomer:self requireCustomerSelected:YES];
    NSString *customerShowingID=customerShowing[@"id"];
    NSIndexPath *indextPathSelected;
    if (customerShowing) {
        for (int section=0;section<listCustomerShow.count;section++) {
            NSArray *customerInSection =listCustomerShow[section];
            NSInteger indexExisted=NSNotFound;
            for (int row=0; row<customerInSection.count; row++) {
                NSDictionary *ccCustomer=customerInSection[row];
                if ([customerShowingID isEqualToString:ccCustomer[@"id"]]) {
                    indexExisted=row;
                    break;
                }
            }
            if (indexExisted!=NSNotFound) {
                indextPathSelected=[NSIndexPath indexPathForRow:indexExisted inSection:section];
                break;
            }
        }
    }
    if(indextPathSelected==nil && listCustomerShow.count>0){// have receipt
        indextPathSelected =[NSIndexPath indexPathForRow:0 inSection:0];
    }
    if (indextPathSelected) {
        [_tbvMain selectRowAtIndexPath:indextPathSelected animated:YES scrollPosition:UITableViewScrollPositionNone];
        [_delegate listCustomer:self clickDetailWithCustomer:[listCustomerShow[indextPathSelected.section] objectAtIndex:indextPathSelected.row]];
    }else{
        [_delegate listCustomer:self clickDetailWithCustomer:nil];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    _lbNoCustomer.hidden=listCustomerShow.count!=0;
    return listCustomerShow.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arrayInSection=listCustomerShow[section];
    return arrayInSection.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *vH=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 25)];
    vH.backgroundColor=tkColorMainBackground;
    UIView *lineT=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 1)];
    UIView *lineB=[[UIView alloc] initWithFrame:CGRectMake(0, 24, _tbvMain.frame.size.width, 1)];
    lineT.backgroundColor=tkColorFrameBorder;
    lineB.backgroundColor=tkColorFrameBorder;
    [vH addSubview:lineT];
    [vH addSubview:lineB];
    if (!isSearching) {
        UILabel *lb=[[UILabel alloc] initWithFrame:CGRectMake(20, 0, _tbvMain.frame.size.width-20, 25)];
        lb.backgroundColor=[UIColor clearColor];
        lb.textColor=[UIColor blackColor];
        lb.font=tkFontMainWithSize(18);
        NSArray *listCustomerByFirstCharacter=listCustomerShow[section];
        NSDictionary *customerInfoFirst=listCustomerByFirstCharacter[0];
        lb.text=[[customerInfoFirst[@"firstName"] substringToIndex:1] uppercaseString];
        [vH addSubview:lb];
    }
    return vH;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellCustomer *cell=[tableView dequeueReusableCellWithIdentifier:CellCustomerIdentify];
    [cell setValueDefault];
    NSArray *listCustomerInSection=listCustomerShow[indexPath.section];
    NSDictionary *customerInfo=listCustomerInSection[indexPath.row];
    [cell.ivMain cancelLoading];
    cell.lbTitle.text=customerInfo[@"fullName"];
    cell.lbDescription.text=[customerInfo[@"phone"] convertNullToNil];
    NSString *urlImage=[customerInfo[@"image"] convertNullToNil];
    if (urlImage) {
        [cell.ivMain loadImageFromUrl:urlImage completed:^(NSError *error, UIImage *image) {
            if (image) {
                cell.ivMain.image=image;
            }else{
                cell.ivMain.image =[UIImage imageNamed:@"icon_short_name_1.png"];
                cell.lbShortName.text=[self shortNameWithText:customerInfo[@"firstName"]];
            }
        }];
    }else{
        cell.ivMain.image =[UIImage imageNamed:@"icon_short_name_1.png"];
        cell.lbShortName.text=[self shortNameWithText:customerInfo[@"firstName"]];
    }
    cell.vLine.hidden=(indexPath.row==listCustomerInSection.count-1 && indexPath.section != listCustomerShow.count-1);
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *customerInfo=[listCustomerShow[indexPath.section] objectAtIndex:indexPath.row];
    if (_delegate) {
        [_delegate listCustomer:self clickDetailWithCustomer:customerInfo];
    }
}
-(NSString*)shortNameWithText:(NSString*)text{
    if (text.length>3) {
        text=[text substringToIndex:3];
    }
    return [text uppercaseString];
}
- (IBAction)clickCancelSearch:(id)sender {
    [_searchBar resignFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame=_searchBar.frame;
        frame.size.width+=62;
        _searchBar.frame=frame;
    }];
    _btCancelSearch.hidden=YES;
    isSearching=NO;
    if (_delegate) {
        [_delegate listCustomer:self resetCustomerSelected:YES];
    }
    [self reShowListCustomer:[self listCustomerByFirstCharacterFrom:listCustomerOrginal]];
    
}
#pragma TextFieldDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    if(!isSearching){
        [UIView animateWithDuration:0.1 animations:^{
            CGRect frame=searchBar.frame;
            frame.size.width-=62;
            searchBar.frame=frame;
        }];
        _btCancelSearch.hidden=NO;
        isSearching=YES;
        oldKeySearch=nil;
        if (_delegate) {
            [_delegate listCustomer:self resetCustomerSelected:YES];
        }
    }
    return YES;
}
// search
-(void)searchCustomerWithTitleKey:(NSString*)keySearch{
    if ([oldKeySearch isEqualToString:keySearch] || [keySearch isEqualToString:@""]) {
        return;
    }
    oldKeySearch=[keySearch lowercaseString];
    {
        //searching
        if (_delegate) {
            [_delegate listCustomer:self resetCustomerSelected:YES];
        }
        NSPredicate *find=[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            NSString *fullName=[[evaluatedObject objectForKey:@"fullName"] lowercaseString];
            return [fullName containsString:oldKeySearch];
        }];
        NSArray *result=[listCustomerOrginal filteredArrayUsingPredicate:find];
        if (result.count>0) {
            [self reShowListCustomer:@[result]];
        }else{
            [self reShowListCustomer:@[]];
        }
    }
}
-(void)addSearchTextChange:(NSString*)text countChange:(NSUInteger)myCountChangeId{
    if (myCountChangeId != countSearchChange) {
        return;// have change affter
    }
    [self searchCustomerWithTitleKey:text];
}

#pragma SearchBarDelegate
-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        return YES;
    }
    if (countSearchChange == NSUIntegerMax) {
        countSearchChange=0;
    }else{
        countSearchChange++;
    }
    NSString *newString=[searchBar.text stringByReplacingCharactersInRange:range withString:text];
    newString =[newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSUInteger myCountChange=countSearchChange;//copy
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(tkDelaySearchText * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self addSearchTextChange:newString countChange:myCountChange];
    });
    return YES;
}
@end
