//
//  ListReceiptVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/9/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define DelaySearchText 0.5
#define MaxReceiptQuery 1000
#define CellReceiptIndentify @"cellReceipt"
#import "ListReceiptVC.h"
#import "TKOrder.h"
#import "Controller.h"
#import "CashierBusinessModel.h"

@interface ListReceiptVC ()

@end

@implementation ListReceiptVC{
    NSUInteger countSearchChange;
    BOOL isShowSearch;
}
-(instancetype)init{
    if (self =[super init]) {
        currency=[Controller currencyDefault];
        listReceiptDay=[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _tbvMain.backgroundColor=tkColorMainBackground;
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellReceipt" bundle:nil] forCellReuseIdentifier:CellReceiptIndentify];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    _lbNoReceipt.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"receipt.lb.no-receipt"];
}
-(void)viewDidAppear:(BOOL)animated{
}
-(void)setListReceipt:(NSArray *)listReceipt fromSearch:(BOOL)search{
    isShowSearch=search;
    if (search) {
        if (listReceipt.count==0) {
            [self resetWithListReceipt:@[]];
        }else{
            [self resetWithListReceipt:@[listReceipt]];
        }
    }else{
        [self resetWithListReceipt:[self listReceiptSectionWithAll:listReceipt]];
    }
}
-(void)resetWithListReceipt:(NSArray*)listReceiptDay_{
    if (listReceiptDay!=listReceiptDay_) {
        [listReceiptDay removeAllObjects];
        [listReceiptDay addObjectsFromArray:listReceiptDay_];
    }
    [_tbvMain reloadData];
    
    // reselected cell
    CBLDocument *receiptShowing =[_delegate listReceiptRequireReceiptSelected:self];
    NSIndexPath *indextPathSelected;
    if (receiptShowing) {
        for (int section=0;section<listReceiptDay.count;section++) {
            NSArray *receiptInSection =listReceiptDay[section];
            NSInteger indexExisted=[receiptInSection indexOfObject:receiptShowing];
            if (indexExisted!=NSNotFound) {
                indextPathSelected=[NSIndexPath indexPathForRow:indexExisted inSection:section];
                break;
            }
        }
    }
    if(indextPathSelected==nil && listReceiptDay.count>0){// have receipt
        indextPathSelected =[NSIndexPath indexPathForRow:0 inSection:0];
    }
    if (indextPathSelected) {
        [_tbvMain selectRowAtIndexPath:indextPathSelected animated:YES scrollPosition:UITableViewScrollPositionNone];
        [_delegate listReceipt:self clickDetail:[listReceiptDay[indextPathSelected.section] objectAtIndex:indextPathSelected.row]];
    }else{
        [_delegate listReceipt:self clickDetail:nil];
    }
}
-(NSArray*)listReceiptSectionWithAll:(NSArray*)listReceipts{
    if (listReceipts.count==0) {
        return @[];
    }
    NSMutableArray *sectionDates=[[NSMutableArray alloc]init];
    NSInteger startIdex=0;
    NSInteger length=1;
    NSDate *dateStart=[TKOrder completedDate:listReceipts[startIdex]];
    for (NSInteger i = 1; i<listReceipts.count; i++) {
        NSDate *dateCurrent =[TKOrder completedDate:listReceipts[i]];
        if ([dateCurrent sameDateWithDate:dateStart]) {
            length ++;
        }else{
            [sectionDates addObject:[listReceipts subarrayWithRange:NSMakeRange(startIdex, length)]];
            startIdex=i;
            length=1;
            dateStart =[TKOrder completedDate:listReceipts[startIdex]];
        }
    }
    [sectionDates addObject:[listReceipts subarrayWithRange:NSMakeRange(startIdex, length)]];// add last Date;
    return sectionDates;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    _lbNoReceipt.hidden=(listReceiptDay.count>0);
    return listReceiptDay.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *listReceiptInDay=listReceiptDay[section];
    return listReceiptInDay.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellReceipt *cell=[tableView dequeueReusableCellWithIdentifier:CellReceiptIndentify];
    [cell setValueDefault];
    CBLDocument *doc =[listReceiptDay[indexPath.section] objectAtIndex:indexPath.row];
    cell.lbTitle.text=[TKOrder displayName:doc];
    NSDate *date=[TKOrder completedDate:doc];
    cell.lbTime.text = [date getStringWithFormat:@"hh:mm aa"];
    cell.lbAmount.text=[TKAmountDisplay stringAmount:[TKOrder grandTotal:doc] withCurrency:currency];
    if ([TKOrder orderStatus:doc]==OrderStatusRefund) {
        cell.imgStatusDeselected=[UIImage imageNamed:@"icon_status_refund_b.png"];
        cell.imgStatusSelected=[UIImage imageNamed:@"icon_status_refund_w.png"];
    }else if ([TKOrder orderStatus:doc]==OrderStatusCanceled) {
        cell.imgStatusDeselected=[UIImage imageNamed:@"icon_status_cancel_r.png"];
        cell.imgStatusSelected=[UIImage imageNamed:@"icon_status_cancel_w.png"];
    }
    if (indexPath.row==([listReceiptDay[indexPath.section] count]-1)) {
        cell.vLine.hidden=YES;
    }else{
        cell.vLine.hidden=NO;
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *vH=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 25)];
    vH.backgroundColor=tkColorMainBackground;
    UIView *lineT=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 1)];
    UIView *lineB=[[UIView alloc] initWithFrame:CGRectMake(0, 24, _tbvMain.frame.size.width, 1)];
    lineT.backgroundColor=tkColorFrameBorder;
    lineB.backgroundColor=tkColorFrameBorder;
    [vH addSubview:lineT];
    [vH addSubview:lineB];
    if (!isShowSearch) {
        UILabel *lb=[[UILabel alloc] initWithFrame:CGRectMake(20, 0, _tbvMain.frame.size.width-20, 25)];
        lb.backgroundColor=[UIColor clearColor];
        lb.textColor=[UIColor blackColor];
        lb.font=tkBoldFontMainText;
        NSArray *listReceiptInDay=listReceiptDay[section];
        NSDate *date=[TKOrder completedDate:[listReceiptInDay firstObject]];
        lb.text=[date getStringWithFormat:@"EEE, dd MMM yyyy"];
        [vH addSubview:lb];
    }
    return vH;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *listReceiptInDay=listReceiptDay[indexPath.section];
    CBLDocument *receiptSelected =listReceiptInDay[indexPath.row];
    [SNLog Log:100 :@"%@",receiptSelected.documentID];
    if (_delegate) {
        [_delegate listReceipt:self clickDetail:receiptSelected];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
