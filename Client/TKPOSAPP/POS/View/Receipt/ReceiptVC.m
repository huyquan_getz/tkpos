//
//  ReceiptVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/9/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define DelaySearchText 0.5
#define MaxReceiptQuery 1000
#import "ReceiptVC.h"
#import "RefundVC.h"
#import "CashierBusinessModel.h"
#import "QueueSendMail.h"
#import "TKOrder.h"
#import "ShiftDrawerManager.h"
#import "PrintingTemplateManagement.h"

@interface ReceiptVC ()

@end

@implementation ReceiptVC{
    LanguageUtil *languageKey;
    NSInteger countSearchChange;
}
-(instancetype)init{
    if (self=[super init]) {
        countSearchChange=0;
        listReceipt=[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_searchBar setBarTintColor:[UIColor statusNaviBar]];
    _searchBar.layer.borderWidth=1;
    _searchBar.layer.borderColor=[UIColor statusNaviBar].CGColor;
    CGRect frame=_vSearch.frame;
    frame.origin.x=66;
    frame.origin.y=0;
    [_vSearch setFrame:frame];
    [_vSearch setBackgroundColor:[UIColor statusNaviBar]];
    _vSearch.hidden=YES;
    [_btCancelSearch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:_vSearch];
    UITapGestureRecognizer *tapBehideSearchBar=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBehideSearch:)];
    tapBehideSearchBar.numberOfTapsRequired=1;
    tapBehideSearchBar.numberOfTouchesRequired=1;
    [_vSearch addGestureRecognizer:tapBehideSearchBar];
    [_vLine setBackgroundColor:tkColorFrameBorder];
    [_vFrameReceiptDetail setBackgroundColor:[UIColor clearColor]];
    [_ivNaviBar setBackgroundColor:[UIColor statusNaviBar]];
    [self addListReceipt];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    _lbTitleListReceipt.text=[languageKey stringByKey:@"receipt.lb.all-receipt"];
    _lbTitleInformation.text=[languageKey stringByKey:@"receipt.lb.information"];
    _lbNoPlaceOrder.text=[languageKey stringByKey:@"receipt.ms-please-place-an-receipt"];
}
-(void)viewDidAppear:(BOOL)animated{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self setupQueryLimitTime];
    });
}
-(void)setupQueryLimitTime{
    //limit 3 month
    [self reSetupQueryLiveWithQuery:[Controller queryTKGetReceiptLimitDateInMonth:3]];
}
-(void)reSetupQueryLiveWithQuery:(CBLQuery*)query{
    if (query==nil) {
        return;
    }
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery=[query asLiveQuery];
    [liveQuery addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    liveQuery.limit = MaxReceiptQuery;
    liveQuery.descending=YES;
    [liveQuery start];
}
-(void)removeQueryLive{
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery=nil;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object == liveQuery) {
        [self displayRows:liveQuery.rows fromSearch:NO];
    }
}
-(void)displayRows:(CBLQueryEnumerator*)enumRows fromSearch:(BOOL)search{
    NSMutableArray *receipts=[[NSMutableArray alloc] init];
    for (CBLQueryRow *row in enumRows) {
        [receipts addObject:row.document];
    }
    [listReceipt removeAllObjects];
    [listReceipt addObjectsFromArray:receipts];
    if (_listReceiptVC==nil) {
        [self addListReceipt];
    }
    [_listReceiptVC setListReceipt:listReceipt fromSearch:search];
}
-(void)addListReceipt{
    _listReceiptVC = [[ListReceiptVC alloc] init];
    _listReceiptVC.delegate=self;
    [self addChildViewController:_listReceiptVC];
    [_listReceiptVC.view setFrame:_vFrameListReceipt.frame];
    _vFrameListReceipt.hidden=YES;
    [self.view insertSubview:_listReceiptVC.view aboveSubview:_vFrameListReceipt];
}
-(void)reAddReceiptDetailWithOrder:(CBLDocument*)receipt{
    if (_receiptDetailVC) {
        [_receiptDetailVC.view removeFromSuperview];
        [_receiptDetailVC removeFromParentViewController];
        _receiptDetailVC=nil;
    }
    receiptSelected=receipt;
    if (receiptSelected==nil) {
        return;
    }else{
        _receiptDetailVC = [[ReceiptDetailVC alloc] initWithReceipt:receiptSelected];
        [self addChildViewController:_receiptDetailVC];
        [_receiptDetailVC.view setFrame:_vFrameReceiptDetail.frame];
        [self.view insertSubview:_receiptDetailVC.view aboveSubview:_vFrameReceiptDetail];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickMenu:(id)sender {
    [Controller showLeftMenu:self];
}
- (IBAction)clickSendMail:(id)sender {
    if (receiptSelected==nil) {
        return;
    }
    if (_sendEmailVC) {
        [_sendEmailVC.view removeFromSuperview];
        [_sendEmailVC removeFromParentViewController];
        _sendEmailVC=nil;
    }
    _sendEmailVC=[[SendEmailReceipt2VC alloc] initWithReceipt:receiptSelected];
    _sendEmailVC.delegate=self;
    [self addChildViewController:_sendEmailVC];
    [self.view addSubview:_sendEmailVC.view];
    
}
#pragma SendMainReceipt2Delegate
-(void)sendMainReceipt2:(id)sender completed:(BOOL)completed inQueue:(BOOL)inQueue{
    if (_sendEmailVC) {
        [_sendEmailVC.view removeFromSuperview];
        [_sendEmailVC removeFromParentViewController];
        _sendEmailVC=nil;
    }
}

#pragma  ListReceiptDelegate
-(void)listReceipt:(id)sender clickDetail:(CBLDocument *)receipt{
    [self reAddReceiptDetailWithOrder:receipt];
    if (receipt==nil) {
        _btPrint.enabled=NO;
        _btRefund.enabled=NO;
        _btSendMail.enabled=NO;
    }else if ([TKOrder orderStatus:receipt]==OrderStatusRefund) {
        _btPrint.enabled=YES;
        _btRefund.enabled=NO;
        _btSendMail.enabled=YES;
    }else{
        _btPrint.enabled=YES;
        _btRefund.enabled=YES;
        _btSendMail.enabled=YES;
    }
}
-(CBLDocument *)listReceiptRequireReceiptSelected:(id)sender{
    return receiptSelected;
}
- (IBAction)clickPrint:(id)sender{
   [[PrintingTemplateManagement sharedObject] printReceipt:receiptSelected rePrint:YES];
}

-(IBAction)clickRefund:(id)sender{
    if (_refundVC) {
        [_refundVC.view removeFromSuperview];
        [_refundVC removeFromParentViewController];
        _refundVC=nil;
    }
    if (receiptSelected) {
        _refundVC=[[RefundVC alloc] initWithReceipt:receiptSelected];
        _refundVC.delegate=self;
        [self addChildViewController:_refundVC];
        [self.view addSubview:_refundVC.view];
    }
}

#pragma RefundDelegate
-(void)refund:(id)sender cancel:(BOOL)none{
    if (_refundVC) {
        [_refundVC.view removeFromSuperview];
        [_refundVC removeFromParentViewController];
        _refundVC=nil;
    }
}
-(void)refund:(id)sender refundReceipt:(CBLDocument *)receiptRefund arrayPayment:(NSArray *)arrayPayment reason:(NSString *)reason{
    CBLDocument *docRefund=[CashierBusinessModel refundReceipt:receiptRefund paymentRefund:arrayPayment reason:reason];
    [CashierBusinessModel sendEmailWithOrder:docRefund emailIfNeed:nil type:MailTypeRefund];
    [[PrintingTemplateManagement sharedObject] printReceipt:docRefund rePrint:NO];
    if (_refundVC) {
        [_refundVC.view removeFromSuperview];
        [_refundVC removeFromParentViewController];
        _refundVC=nil;
    }
}

// search
-(void)searchReceiptWithTitleKey:(NSString*)keySearch{
    isShowSearching=YES;
    if ([oldSearchKey isEqualToString:keySearch] || [keySearch isEqualToString:@""]) {
        return;
    }
    oldSearchKey=keySearch;
    [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
        CBLQuery *query=[Controller querySearchReceiptWithTitleKey:keySearch];
        query.descending=YES;
        NSError *error;
        CBLQueryEnumerator *enumerow=[query run:&error];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self displayRows:enumerow fromSearch:YES];
        });
    }];
}
-(void)addSearchTextChange:(NSString*)text countChange:(NSUInteger)myCountChangeId{
    if (myCountChangeId != countSearchChange) {
        return;// have change affter
    }
    [self searchReceiptWithTitleKey:text];
}

#pragma SearchBarDelegate
-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        return YES;
    }
    if (countSearchChange == NSUIntegerMax) {
        countSearchChange=0;
    }else{
        countSearchChange++;
    }
    NSString *newString=[searchBar.text stringByReplacingCharactersInRange:range withString:text];
    newString =[newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSUInteger myCountChange=countSearchChange;//copy
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(DelaySearchText * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self addSearchTextChange:newString countChange:myCountChange];
    });
    return YES;
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    _btCancelSearch.enabled=YES;
    [self removeQueryLive];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searchReceiptWithTitleKey:searchBar.text];
}
- (IBAction)clickCancelSearch:(id)sender {
    _btCancelSearch.enabled=NO;
    [_searchBar resignFirstResponder];
    _searchBar.text=@"";
    receiptSelected=nil;
    isShowSearching=NO;
    _vSearch.hidden=YES;
    [self setupQueryLimitTime];
}

- (IBAction)clickSearch:(id)sender {
    receiptSelected=nil;
    isShowSearching=YES;
    _vSearch.hidden=NO;
    [_searchBar becomeFirstResponder];
}
-(void)clickBehideSearch:(id)sender{
    [_searchBar becomeFirstResponder];
}
-(void)dealloc{
    [liveQuery removeObserver:self forKeyPath:@"rows"];
}


@end
