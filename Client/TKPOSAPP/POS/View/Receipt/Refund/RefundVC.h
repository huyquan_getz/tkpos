//
//  RefundVC.h
//  POS
//
//  Created by Cong Nha Duong on 1/21/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "Constant.h"
@protocol RefundDelegate <NSObject>

@required
-(void)refund:(id)sender cancel:(BOOL)none;
-(void)refund:(id)sender refundReceipt:(CBLDocument*)receipt arrayPayment:(NSArray*)arrayPayment reason:(NSString*)reason;

@end
#import <UIKit/UIKit.h>


@interface RefundVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    CBLDocument *receipt;
}
@property (weak,nonatomic) id<RefundDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *tfOther;
@property (weak, nonatomic) IBOutlet UIImageView *ivTickOther;
@property (strong, nonatomic) IBOutlet UITableViewCell *tbCellOther;
@property (weak, nonatomic) IBOutlet UITableView *tbvReason;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIView *vBgTitle;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (weak, nonatomic) IBOutlet UITableView *tbvPaymentInfo;
- (IBAction)clickCancel:(id)sender;
- (IBAction)clickDone:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btDone;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleReason;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleAmount;
-(instancetype)initWithReceipt:(CBLDocument*)receipt_;
@end
