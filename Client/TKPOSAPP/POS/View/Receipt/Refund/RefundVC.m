//
//  RefundVC.m
//  POS
//
//  Created by Cong Nha Duong on 1/21/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define CellIndentifyReason @"cellReasonRefund"
#define CellIndentifyPaymentInfo @"cellPaymentInfor"
#define MaxLegthReasonOther 50
#import "RefundVC.h"
#import "TableCellReasonRefund.h"
#import "TableCellAmountOfRefund.h"
#import "Constant.h"
#import "TKOrder.h"
#import "PaymentChild.h"
#import "CashierBusinessModel.h"
#import "PaymentBusinessModel.h"
#import "Controller.h"

@interface RefundVC ()

@end

@implementation RefundVC{
    NSInteger indexReasonRefundSelect;
    NSArray *listPayment;
    TKAmountDisplay *amountDisplay;
}


-(instancetype)initWithReceipt:(CBLDocument *)receipt_{
    if (self = [super init]) {
        receipt=receipt_;
        NSString *currency=[TKOrder currency:receipt];
        if (currency==nil) {
            currency=[Controller currencyDefault];
        }
        amountDisplay=[[TKAmountDisplay alloc] initWithCurrency:currency];
        NSArray *arrayPaymentJSON=[TKOrder arrayPaymentJSON:receipt];
        NSMutableArray *arrayPaymentChild=[[NSMutableArray alloc] init];
        for (NSDictionary * json in arrayPaymentJSON) {
            [arrayPaymentChild addObject:[[PaymentChild alloc] initFromJsonData:json]];
        }
        listPayment=[self arrayPaymentWithListPaymentChild:arrayPaymentChild];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    indexReasonRefundSelect=-1;
    _tfOther.enabled=NO;
    _ivTickOther.hidden=YES;
    UIView *bgSelectedCellOther=[[UIView alloc] init];
    bgSelectedCellOther.backgroundColor=tkColorFrameBorder;
    _tbCellOther.selectedBackgroundView=bgSelectedCellOther;
    [_tbvReason registerNib:[UINib nibWithNibName:@"TableCellReasonRefund" bundle:nil] forCellReuseIdentifier:CellIndentifyReason];
    [_tbvPaymentInfo registerNib:[UINib nibWithNibName:@"TableCellAmountOfRefund" bundle:nil] forCellReuseIdentifier:CellIndentifyPaymentInfo];
    _tbvReason.layer.borderColor=tkColorFrameBorder.CGColor;
    _tbvReason.layer.borderWidth=1;
    _tbvReason.layer.cornerRadius=tkCornerRadiusViewPopup;
    _tbvPaymentInfo.layer.borderColor=tkColorFrameBorder.CGColor;
    _tbvPaymentInfo.layer.borderWidth=1;
    _tbvPaymentInfo.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vLine1.backgroundColor=tkColorFrameBorder;
    
    [_btDone setTitleColor:tkColorMain forState:UIControlStateNormal];
    [_btDone setTitleColor:tkColorMainActive forState:UIControlStateHighlighted];
    self.view.backgroundColor=[UIColor clearColor];
    UIView *vCorver=[[UIView alloc] initWithFrame:self.view.bounds];
    [vCorver setViewCoverDefault];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [vCorver addGestureRecognizer:tap];
    [self.view insertSubview:vCorver belowSubview:_vMain];
    
    [_vMain setShadowDefault];
    _vMain.clipsToBounds=YES;
    _vBgTitle.layer.cornerRadius=tkCornerRadiusViewPopup;
    [self showMessageError:@""];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [self resetFrame];
}

-(void)resetFrame{
    NSInteger maxHeight=150;
    NSInteger height=listPayment.count * 50;
    if (height>maxHeight) {
        height=maxHeight;
        _tbvPaymentInfo.scrollEnabled=YES;
    }else{
        _tbvPaymentInfo.scrollEnabled=NO;
    }
    CGRect frame=_tbvPaymentInfo.frame;
    frame.size.height=height;
    _tbvPaymentInfo.frame=frame;
    
    frame=_vMain.frame;
    frame.size.height-=(maxHeight-height);
    _vMain.frame=frame;
}
-(NSArray*)arrayPaymentWithListPaymentChild:(NSArray*)listPaymentChild{
    NSArray *listTypeDifferent =[PaymentBusinessModel listPaymentTypeWithListPaymentChild:listPaymentChild];
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    for (NSString *paymentType in listTypeDifferent) {
        NSArray *array =[PaymentBusinessModel subListPaymentChildWithListPaymentChild:listPaymentChild paymentType:paymentType];
        PaymentChild *firstObject=[array firstObject];
        NSString *title =[firstObject displayName];
        double totalCharge=[PaymentBusinessModel totalChargeWithListPaymentChild:array];
        NSString *value =[amountDisplay stringAmountHaveCurrency:totalCharge];
        UIImage *imageIdentify=nil;
        if ([firstObject.paymentType isEqualToString:StringPaymentTypeCash]) {
            imageIdentify=[UIImage imageNamed:@"icon_cash.png"];
        }else{
            imageIdentify=[UIImage imageNamed:@"icon_visa.png"];
        }
        if (imageIdentify) {
            [arrayPr addObject:@{@"title":title,@"value":value,@"image":imageIdentify}];
        }else{
            [arrayPr addObject:@{@"title":title,@"value":value}];
        }
        
    }
    return arrayPr;
}

-(void)showMessageError:(NSString*)error{
    [_lbMessageStatus setTextColor:[UIColor redColor]];
    _lbMessageStatus.text=error;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView==_tbvReason) {
        return 4;
    }else{
        return listPayment.count;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==_tbvReason) {
        TableCellReasonRefund *cell=[tableView dequeueReusableCellWithIdentifier:CellIndentifyReason];
        [cell setValueDefault];
        switch (indexPath.row) {
            case 0:
                cell.lbTitle.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"receipt.refund.text-cancel-order"];
                break;
            case 1:
                cell.lbTitle.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"receipt.refund.text-wrong-order"];
                break;
            case 2:
                cell.lbTitle.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"receipt.refund.text-product-faulty"];
                break;
            case 3:{
                _tfOther.placeholder=[[[LanguageUtil sharedLanguageUtil]stringByKey:@"receipt.refund.holder-reason-other"] stringByReplacingOccurrencesOfString:@"MAX_INPUT" withString:[NSString stringWithFormat:@"%d",MaxLegthReasonOther]];
                return _tbCellOther;
                break;
            }
            default:
                break;
        }
        return cell;
    }else{
        //table payment child
        TableCellAmountOfRefund *cell=[tableView dequeueReusableCellWithIdentifier:CellIndentifyPaymentInfo];
        NSDictionary *pm=listPayment[indexPath.row];
        cell.lbTitle.text=pm[@"title"];
        cell.lbValue.text=pm[@"value"];
        cell.ivIndentify.image=pm[@"image"];
        cell.vLine.backgroundColor=tkColorFrameBorder;
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    indexReasonRefundSelect=indexPath.row;
    if (indexReasonRefundSelect==3){
        _tfOther.enabled=YES;
        [_tfOther becomeFirstResponder];
        _ivTickOther.hidden=NO;
    }else{
        [_tfOther resignFirstResponder];
        _ivTickOther.hidden=YES;
        _tfOther.enabled=NO;
    }
}
#pragma Textfied Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (newString.length>MaxLegthReasonOther) {
        return NO;
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y-=100;
    _vMain.frame=frame;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y+=100;
    _vMain.frame=frame;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}
-(void)tapCoverView:(id)sender{
    [self clickCancel:nil];
}
- (IBAction)clickCancel:(id)sender {
    if (_delegate) {
        [_delegate refund:self cancel:YES];
    }
}

- (IBAction)clickDone:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self showMessageError:@""];
    [self.view endEditing:YES];
    if (_delegate) {
        [_delegate refund:self refundReceipt:receipt arrayPayment:[TKOrder arrayPaymentJSON:receipt] reason:[self reasonStringWithIndexReason:indexReasonRefundSelect]];
    }
}
-(NSString*)reasonStringWithIndexReason:(NSInteger)indexReason{
    switch (indexReason) {
        case 0:
            return tkReasonOrderCancel;
            break;
        case 1:
            return tkReasonOrderWrong;
            break;
        case 2:
            return tkReasonOrderProductFaulty;
            break;
        case 3:
            return _tfOther.text;
            break;
        default:
            break;
    }
    return nil;
}
-(BOOL)validateInput{
    if (indexReasonRefundSelect<0) {
        [self showMessageError:[[LanguageUtil sharedLanguageUtil] stringByKey:@"receipt.refund.ms-no-select"]];
        return NO;
    }
    if (indexReasonRefundSelect==3 && [_tfOther isEmptyText]) {
        [self showMessageError:[[LanguageUtil sharedLanguageUtil] stringByKey:@"receipt.refund.ms-empty-reason"]];
        return NO;
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
