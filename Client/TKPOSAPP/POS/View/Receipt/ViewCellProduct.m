//
//  ViewCellProduct.m
//  POS
//
//  Created by Cong Nha Duong on 3/2/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "ViewCellProduct.h"

@implementation ViewCellProduct
-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"ViewCellProduct" owner:self options:nil];
        self.bounds=self.view.bounds;
        [self addSubview:self.view];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"ViewCellProduct" owner:self options:nil];
        self.bounds=self.view.bounds;
        [self addSubview:self.view];
    }
    return self;
}
-(void)setShowVariant:(BOOL)variant showDiscount:(BOOL)discount{
    NSInteger move=0;
    if (variant==NO) {
        CGRect frame=_vProduct.frame;
        frame.size.height-=25;
        _vProduct.frame=frame;
        
        frame=_lbDiscount.frame;
        frame.origin.y-=25;
        _lbDiscount.frame=frame;
        
        frame=_lbAmountDiscount.frame;
        frame.origin.y-=25;
        _lbAmountDiscount.frame=frame;
        
        move+=25;
    }
    if (discount==NO) {
        move+=25;
    }
    if (move>0) {
        CGRect frame=self.view.frame;
        frame.size.height-=move;
        self.view.frame=frame;
        self.frame=frame;
    }
    [self setNeedsLayout];
}
@end
