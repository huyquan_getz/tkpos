//
//  SendEmailReceipt2VC.h
//  POS
//
//  Created by Nha Duong Cong on 12/12/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

@protocol SendMailReceipt2Delegate <NSObject>
@required
-(void)sendMainReceipt2:(id)sender completed:(BOOL)completed inQueue:(BOOL)inQueue;

@end
#import <UIKit/UIKit.h>
#import "Constant.h"

@interface SendEmailReceipt2VC : UIViewController<UITextFieldDelegate>{
    CBLDocument *receiptDoc;
}
-(instancetype)initWithReceipt:(CBLDocument*)receipt_;
@property (weak,nonatomic) id<SendMailReceipt2Delegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
- (IBAction)clickCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UIButton *btSend;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (weak, nonatomic) IBOutlet UIView *vCover;
- (IBAction)clickSend:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIView *vTitle;
@property (weak, nonatomic) IBOutlet UIView *vLine;

@end
