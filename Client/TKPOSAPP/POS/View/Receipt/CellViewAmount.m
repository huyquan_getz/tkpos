//
//  CellViewAmount.m
//  POS
//
//  Created by Nha Duong Cong on 12/11/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "CellViewAmount.h"
#import "Constant.h"

@implementation CellViewAmount

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithTitle:(NSString *)title_ value:(NSString *)value_ bold:(BOOL)bold_{
    if (self =[super init]) {
        _ratio=2;
        [self setTitle:title_];
        [self setValue:value_];
        [self setBold:bold_];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self=[super initWithCoder:aDecoder]) {
        
    }
    return self;
}
-(void)layoutSubviews{
    [super layoutSubviews];
    CGRect frame=self.frame;
    frame.size.width*=(_ratio/(_ratio+1));
    frame.origin=CGPointZero;
    [_lbTitle setFrame:frame];
    frame.origin.x=frame.size.width;
    frame.size.width=self.frame.size.width/(_ratio+1);
    [_lbValue setFrame:frame];
    if (_font) {
        _lbTitle.font=_font;
        _lbValue.font=_font;
    }else{
        if (bold) {
            [_lbTitle setFont:tkBoldFontMainWithSize(17)];
            [_lbValue setFont:tkBoldFontMainWithSize(17)];
        }else{
            [_lbTitle setFont:tkFontMainWithSize(17)];
            [_lbValue setFont:tkFontMainWithSize(17)];
        }
    }
}
-(void)setTitle:(NSString *)title_{
    title = title_;
    if (_lbTitle==nil) {
        _lbTitle =[[UILabel alloc]init];
        [_lbTitle setTextColor:[UIColor blackColor]];
        [_lbTitle setBackgroundColor:[UIColor clearColor]];
        [_lbTitle setTextAlignment:NSTextAlignmentLeft];
        _lbTitle.numberOfLines=2;
        [self addSubview:_lbTitle];
    }
    _lbTitle.text=title;
    [self setNeedsLayout];
}
-(void)setValue:(NSString *)value_{
    value=value_;
    if (_lbValue==nil) {
        _lbValue=[[UILabel alloc] init];
        [_lbValue setTextColor:[UIColor blackColor]];
        [_lbValue setBackgroundColor:[UIColor clearColor]];
        [_lbValue setTextAlignment:NSTextAlignmentRight];
        _lbValue.numberOfLines=2;
        [self addSubview:_lbValue];
    }
    _lbValue.text=value;
    [self setNeedsLayout];
}
-(void)setBold:(BOOL)bold_{
    bold=bold_;
    [self setNeedsLayout];
}
-(NSInteger)numberLinesWidthFont:(UIFont *)font{
    [self layoutSubviews];
    NSInteger nbLineName=(NSInteger)ceilf([_lbTitle.text widthWithFont:font]/_lbTitle.frame.size.width);
    NSInteger nbLineValue=(NSInteger)ceilf([_lbValue.text widthWithFont:font]/_lbValue.frame.size.width);
    return MAX(nbLineName,nbLineValue);
}
@end
