//
//  ListProductVC.m
//  POS
//
//  Created by Nha Duong Cong on 11/11/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define CellProductIdentify @"cellProduct"
#import "ListProductVC.h"
#import "TKProduct.h"
#import "TKProductItem.h"
#import "TableCellProduct.h"

@interface ListProductVC ()

@end

@implementation ListProductVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithListProducts:(NSArray *)listPro indexItemShow:(NSInteger)indexItemShow{
    if (self=[super init]) {
        listProducts=[[NSMutableArray alloc] initWithArray:listPro];
        indexCellTop=indexItemShow;
    }
    return self;
}
-(id)initWithListProducts:(NSArray *)listPro{
    return [self initWithListProducts:listPro indexItemShow:0];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [_tbMain registerNib:[UINib nibWithNibName:@"TableCellProduct" bundle:nil] forCellReuseIdentifier:CellProductIdentify];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAdhocCell:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [_vAdhoc addGestureRecognizer:tap];
    _vLineAdhoc.backgroundColor=tkColorFrameBorder;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [_lbNoneItem setText:[[LanguageUtil sharedLanguageUtil] stringByKey:@"cashier.lb-no-product-item"]];
    [_tbMain reloadData];
    [self resetSelectedItem];
    NSIndexPath *scrollIndexPath=[NSIndexPath indexPathForRow:indexCellTop inSection:0];
    [self scrollToTopIndexPath:scrollIndexPath animation:NO];
}
-(void)resetSelectedItem{
    // reset Selected Cell
    CBLDocument *productSelected =[_delegate productShower:self requireProductSelected:YES];
    NSInteger indexSelected=-1;
    if (productSelected) {
        indexSelected=[listProducts indexOfObject:productSelected];
        if (indexSelected !=NSNotFound) {
            NSIndexPath *indexPathSl=[NSIndexPath indexPathForRow:indexSelected inSection:0];
            [_tbMain selectRowAtIndexPath:indexPathSl animated:YES scrollPosition:UITableViewScrollPositionNone];
        }
    }
}
-(void)scrollToTopIndexPath:(NSIndexPath*)moveIndextPath animation:(BOOL)animation{
    if (moveIndextPath.row >=listProducts.count) {
        return;
    }
    [_tbMain scrollToRowAtIndexPath:moveIndextPath atScrollPosition:UITableViewScrollPositionTop animated:animation];
}
#pragma TableViewDeletegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 70;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return _vAdhoc;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    _lbNoneItem.hidden=(listProducts.count != 0);
    return listProducts.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellProduct *cell =[tableView dequeueReusableCellWithIdentifier:CellProductIdentify];
    CBLDocument *doc = [listProducts objectAtIndex:indexPath.row];
    [cell setValueDefault];
    cell.lbTitle.text =[TKProduct getName:doc];
    NSInteger quantityWarining=[TKProductItem getMiniQuantityStockWarning:doc];
    if (quantityWarining>=0) {
        if (quantityWarining==0) {
            [cell setEmptyStock];
        }else{
            [cell setRemainFew:quantityWarining];
        }
    }else{
        //<0 quantity not check;
    }
    [cell.ivMain cancelLoading];
    NSString *urlImage=[TKProduct getImageUrlDefault:doc];
    if (urlImage) {
        [cell.ivMain loadImageFromUrl:urlImage completed:^(NSError *error, UIImage *image) {
            if (image) {
                cell.ivMain.image=image;
            }else{
                cell.ivMain.image =[UIImage imageDefaultWithSortName:[TKProduct getShortName:doc]];
            }
        }];
    }else{
        cell.ivMain.image =[UIImage imageDefaultWithSortName:[TKProduct getShortName:doc]];
    }

    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self selectAdHocCell:NO];
    if (_delegate && [_delegate respondsToSelector:@selector(productShower:showProductDetail:)]) {
        CBLDocument *product=[listProducts objectAtIndex:indexPath.row];
        [_delegate productShower:self showProductDetail:product];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)reloadListWithProducts:(NSArray *)products resetPosition:(BOOL)resetPosition{
    if (products != listProducts) {
        [listProducts removeAllObjects];
        [listProducts addObjectsFromArray:products];
    }
    [_tbMain reloadData];
    if (resetPosition) {
        [_tbMain setContentOffset:CGPointZero];
    }else{
        [self resetSelectedItem];
    }
}
#pragma ShowProduct
-(void)showListProducts:(NSArray *)products resetPosition:(BOOL)resetPosition{
    [self reloadListWithProducts:products resetPosition:resetPosition];
}
-(void)reloadItemWithProductSelected:(CBLDocument *)documentSelected{
    [_tbMain reloadData];
}
-(NSInteger)currentIndexItemShow{
    NSArray *array=[_tbMain indexPathsForVisibleRows];
    NSInteger topIndexItem=0;
    if (array.count>0) {
        NSIndexPath *indexP=[array firstObject];
        topIndexItem=indexP.row;
    }
    return topIndexItem+1;
}
-(void)resetCurrentIndexItemShow{
    indexCellTop=0;
    [_tbMain setContentOffset:CGPointZero];
}
-(void)setProductShowerDelegate:(id<ProductShowerDelegate>)delegate_{
    _delegate=delegate_;
}

-(void)setCurrentIndexItemShow:(NSInteger)currentIndex{
    indexCellTop=currentIndex;
    if ([self isViewLoaded]) {
        NSIndexPath *moveIndexPath=[NSIndexPath indexPathForRow:indexCellTop inSection:0];
        [self scrollToTopIndexPath:moveIndexPath animation:YES];
    }
}
-(void)tapAdhocCell:(UITapGestureRecognizer*)tap{
    if (_tbMain.indexPathForSelectedRow) {
        [_tbMain deselectRowAtIndexPath:_tbMain.indexPathForSelectedRow animated:NO];
    }
    [self selectAdHocCell:YES];
    if (_delegate) {
        [_delegate productShower:self showAdhocProduct:YES];
    }
}
-(void)selectAdHocCell:(BOOL)select{
    if (select) {
        _vAdhoc.backgroundColor=tkColorMain;
    }else{
        _vAdhoc.backgroundColor=[UIColor whiteColor];
    }
}
@end
