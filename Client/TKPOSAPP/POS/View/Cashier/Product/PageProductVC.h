//
//  PageProductVC.h
//  POS
//
//  Created by Nha Duong Cong on 10/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "SMPageControl.h"
#import "ContentPageProductVC.h"
#import "ProductShower.h"
#import "ProductShowerDelegate.h"
#import "ListPageVC.h"

@interface PageProductVC : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate,ContentPageProductDelegate,ProductShower,ListPageDelegate,UIPopoverControllerDelegate>{
    NSInteger currentPageIndex;
    NSInteger totalPage;
    NSMutableArray *listProducts;
    NSInteger numberItemInPage;
    UIPopoverController *popoverListPage;
}

@property (weak,nonatomic)id<ProductShowerDelegate> delegate;
@property (readonly) NSInteger currentPageIndex;
@property (readonly) NSInteger totalPage;
@property (readonly) NSMutableArray *listProducts;
@property (readonly) NSInteger numberItemInPage;
@property (weak, nonatomic) IBOutlet SMPageControl *pageControl;
@property (strong,nonatomic) UIPageViewController *pageVC;
-(id)initWithListProducts:(NSArray*)listPro;
-(id)initWithListProducts:(NSArray *)listPro indexItemShow:(NSInteger)indexItemShow;
-(void)reloadInPageWithListProducts:(NSArray*)listProductsNew;// hold current pageIndex
-(void)reloadWithListProducts:(NSArray*)listProductsNew showPageIndex:(NSInteger)pageIndex;
-(void)moveToPage:(NSInteger)pageIndex animation:(BOOL)animation;
//test
-(NSInteger)numberItemInPageWithPageIndex:(NSInteger)pageIndex;
-(NSArray*)listProductsWithPageIndex:(NSInteger)pageIndex;
-(NSInteger)indexOfContentVC:(UIViewController*)contentVC;
-(UIViewController*)contentVCWithIndex:(NSInteger)contentIndex;


// action with ListPage
@property (strong, nonatomic) IBOutlet UIView *listPageControl;
@property (weak, nonatomic) IBOutlet UIButton *listPageControl_btRight;
@property (weak, nonatomic) IBOutlet UIButton *listPageControl_btLeft;
@property (weak, nonatomic) IBOutlet UIButton *listPageControl_btCenter;
- (IBAction)listPageControl_clickLeft:(id)sender;
- (IBAction)listPageControl_clickRight:(id)sender;
- (IBAction)listPageControl_clickCenter:(id)sender;
- (void)displayListPageControl;
@end
