//
//  CollectionCellProduct.h
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLImageView.h"

@interface CollectionCellProduct : UICollectionViewCell
@property (weak, nonatomic) IBOutlet DLImageView *ivMain;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btStatus;
@property (weak, nonatomic) IBOutlet UIView *vCover;
-(void)setValueDefault;
-(void)setEmptyStock;
-(void)setRemainFew:(NSInteger)nbProducts;
@end
