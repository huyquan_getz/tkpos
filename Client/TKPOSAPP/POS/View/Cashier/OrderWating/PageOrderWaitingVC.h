//
//  PageOrderWaitingVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "Constant.h"
@protocol PageOrderWaitingDelegate <NSObject>
@required
-(void)pageOrderWaiting:(id)sender clickDetailOrder:(CBLDocument*)order;
-(void)pageOrderWaiting:(id)sender reloadCurrentOrder:(CBLDocument*)order;
-(CBLDocument*)pageOrderWaitingRequireCurrentOrder:(id)sender;

@end

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "ContentPageOrderVC.h"

@interface PageOrderWaitingVC : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate, ContentPageOrderDelegate>{
    NSMutableArray *listOrderWaiting;
    NSInteger currentPageIndex;
    NSInteger totalPage;
    NSInteger numberItemInPage;
    CBLLiveQuery *liveQuery;
}
@property (weak,nonatomic) id<PageOrderWaitingDelegate> delegate;
@property (weak, nonatomic) IBOutlet SMPageControl *pageControl;
@property (strong,nonatomic) UIPageViewController *pageVC;
-(void)reloadData;
@end
