//
//  OrderSelfCollectVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol OrderSelfCollectDelegate <NSObject>
@required
-(void)orderSelfCollectClose:(id)sender;
-(void)orderSelfCollect:(id)sender updateQuantityProductID:(NSString*)productID variantSKU:(NSString*)variantSKU add:(NSInteger)addQ sub:(NSInteger)subQ;
@end

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "TKOrder.h"
#import "CashierBusinessModel.h"

@interface OrderSelfCollectVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listProductOrders;
    NSMutableArray *listService;
    NSDate *createdDate;
    NSInteger discountValue;
    CBLDocument *orderDoc;
}
@property (weak, nonatomic) IBOutlet UILabel *lbMessage;
@property (weak, nonatomic) IBOutlet UIButton *btCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lbCustomerName;
@property (weak,nonatomic) id<OrderSelfCollectDelegate> delegate;
-(instancetype)initWithOrder:(CBLDocument*)orderDocument;
@property (strong, nonatomic) IBOutlet UIButton *btCancelNewOrder;
@property (weak, nonatomic) IBOutlet UIButton *btChangeStatus;
- (IBAction)clickClose:(id)sender;
- (IBAction)clickChangeStatus:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btClose;
- (IBAction)clickCancelNewOrder:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTimeNewOrder;
@property (weak, nonatomic) IBOutlet UILabel *lbOrderCode;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UIView *vLine2;
@property (weak, nonatomic) IBOutlet UIView *vLine3;
-(CBLDocument*)orderDocument;
-(void)reloadOrder;
@end
