//
//  OrderSelfCollectVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
/*
 HeightTable 525/480
 PostionYLine 637/592
 */
#define KeyFirstLoadViewCheckSKU @"FirstLoadViewCheckSKU"
#define CellOrderIdentify @"cellOrder"
#define CellServiceIdentify @"cellService"
#import "OrderSelfCollectVC.h"
#import "QueueSendMail.h"
#import "TableCellProductOrder.h"
#import "DLImageLoader.h"
#import "TKProductItem.h"
#import "Constant.h"
#import "Controller.h"
#import "TableCellServiceOrder.h"

@interface OrderSelfCollectVC ()

@end

@implementation OrderSelfCollectVC{
    LanguageUtil *languageKey;
    NSMutableArray *arrayProductOrderInStoreOfOrder; // element @{@"productId":@"",
                                                     //           @"variantSKU":@"",
                                                     //           @"quantity":@()}
    TKAmountDisplay *amountDisplay;
}

-(instancetype)initWithOrder:(CBLDocument *)orderDocument{
    if (self =[super init]) {
        orderDoc=orderDocument;
        NSString *currency=[TKOrder currency:orderDoc];
        if (currency==nil) {
            currency=[Controller currencyDefault];
        }
        amountDisplay=[[TKAmountDisplay alloc] initWithCurrency:currency];
        NSArray *productOrderJSON=[TKOrder arrayProductJSON:orderDoc];
        listService=[[NSMutableArray alloc]initWithArray:[self listServicesWithOrder:orderDocument]];
        listProductOrders =[[NSMutableArray alloc] initWithArray:[CashierBusinessModel arrayProductOrderWithProductOrdersJSON:productOrderJSON]];
        createdDate =[TKOrder createdDate:orderDoc];
        arrayProductOrderInStoreOfOrder=[[NSMutableArray alloc] init];
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellProductOrder" bundle:nil] forCellReuseIdentifier:CellOrderIdentify];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellServiceOrder" bundle:nil] forCellReuseIdentifier:CellServiceIdentify];
    _vLine1.backgroundColor=tkColorFrameBorder;
    _vLine2.backgroundColor=tkColorFrameBorder;
    _vLine3.backgroundColor=tkColorFrameBorder;
    _tbvMain.backgroundColor=tkColorMainBackground;
    _btCustomer.imageView.contentMode=UIViewContentModeScaleAspectFill;
    _btChangeStatus.layer.cornerRadius=tkCornerRadiusButton;
    _btClose.layer.cornerRadius=tkCornerRadiusButton;
    _tbvMain.tableFooterView=_btCancelNewOrder;
    [_btCancelNewOrder setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    _btCancelNewOrder.layer.borderWidth=1;
    _btCancelNewOrder.layer.borderColor=tkColorFrameBorder.CGColor;
    
    // Do any additional setup after loading the view from its nib.
}
-(NSArray*)listServicesWithOrder:(CBLDocument*)orderDocument{
    if (languageKey==nil) {
        languageKey=[LanguageUtil sharedLanguageUtil];
    }
    NSMutableArray *newServices=[[NSMutableArray alloc] init];
    [newServices addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-discount"],
                             @"value":[amountDisplay stringAmountHaveCurrency:[TKOrder discountTotalAmount:orderDocument]]}];
    [newServices addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-subtotal"],
                             @"value":[amountDisplay stringAmountHaveCurrency:[TKOrder subTotal:orderDocument]]}];
    NSDictionary *serviceCharegeDisplay=[TKOrder serviceChargeDisplay:orderDoc];
    if (serviceCharegeDisplay) {
        double serviceCharge=[serviceCharegeDisplay[@"serviceChargeValue"] doubleValue];
        [newServices addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-service-charges"],
                                 @"value":[amountDisplay stringAmountHaveCurrency:serviceCharge]}];
    }
    for (NSDictionary *taxDisplay in [TKOrder listTaxDisplayes:orderDoc]) {
        double tax=[taxDisplay[@"taxValue"] doubleValue];
        [newServices addObject:@{@"title":taxDisplay[@"taxName"],
                                 @"value":[amountDisplay stringAmountHaveCurrency:tax]}];
    }
    double roundAmout=[TKOrder roundAmount:orderDoc];
    if(![TKAmountDisplay isZero:roundAmout]){
        NSString *value;
        if (roundAmout>0) {
            value=[amountDisplay stringAmountHaveCurrency:roundAmout];
        }else{
            value=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:-roundAmout]];
        }
        [newServices addObject:@{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.round-amt"],@"value":value}];
    }
    [newServices addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-total"],
                             @"value":[amountDisplay stringAmountHaveCurrency:[TKOrder grandTotal:orderDocument]]}];
    return newServices;
}
-(void)reloadOrder{
    NSArray *productOrderJSON=[TKOrder arrayProductJSON:orderDoc];
    listProductOrders =[[NSMutableArray alloc] initWithArray:[CashierBusinessModel arrayProductOrderWithProductOrdersJSON:productOrderJSON]];
    [_tbvMain reloadData];
    OrderStatus orderStatus =[TKOrder orderStatus:orderDoc];
    [self setActionWithOrderStatus:orderStatus cancelRequire:[TKOrder isCancelRequest:orderDoc]];
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [_btCancelNewOrder setTitle:[languageKey stringByKey:@"cashier.order-self-collect.bt.cancel-sale"] forState:UIControlStateNormal];
    
    _lbTimeNewOrder.text =[createdDate getStringWithFormat:@"HH:mm dd/MM/yyyy"];
    _lbCustomerName.text=[TKOrder customerReceiverName:orderDoc];
    _lbOrderCode.text =[TKOrder orderCode:orderDoc];
    NSString *imageUrl =[[[TKOrder customerReceiver:orderDoc] objectForKey:@"image"] convertNullToNil];
    if (imageUrl) {
        [[DLImageLoader sharedInstance] loadImageFromUrl:imageUrl completed:^(NSError *error, UIImage *image) {
            if (image) {
                [_btCustomer setImage:image forState:UIControlStateNormal];
            }
        }];
    }
    [self setActionWithOrderStatus:[TKOrder orderStatus:orderDoc] cancelRequire:[TKOrder isCancelRequest:orderDoc]];
}
-(void)viewDidAppear:(BOOL)animated{
    BOOL loadFistView =[[NSUserDefaults standardUserDefaults] boolForKey:KeyFirstLoadViewCheckSKU];
    if(!loadFistView){
        self.view.userInteractionEnabled=YES;
        [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
            [Controller productWithSKU:@""];
            self.view.userInteractionEnabled=YES;
            [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:KeyFirstLoadViewCheckSKU];
        }];
    }
}
-(void)setActionWithOrderStatus:(OrderStatus)orderStatus cancelRequire:(BOOL)cancelRequire{
    if (cancelRequire) {
        [_btClose setTitle:[languageKey stringByKey:@"general.bt.reject"] forState:UIControlStateNormal];
        _btClose.layer.borderColor=[UIColor redColor].CGColor;
        _btClose.layer.borderWidth=1;
        _btClose.backgroundColor=[UIColor clearColor];
        [_btClose setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        _btChangeStatus.layer.borderColor=[UIColor redColor].CGColor;
        _btChangeStatus.backgroundColor=[UIColor redColor];
        [_btChangeStatus setTitle:[languageKey stringByKey:@"general.bt.approve"] forState:UIControlStateNormal];
        [_btChangeStatus setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _lbMessage.text=[languageKey stringByKey:@"cashier.order-self-collect.ms-request-cancel"];
        _tbvMain.tableFooterView=nil;
        return;
    }
    //else
    _lbMessage.hidden=YES;
    CGRect frame=_tbvMain.frame;
    frame.size.height=525;
    _tbvMain.frame=frame;
    
    frame=_vLine3.frame;
    frame.origin.y=637;
    _vLine3.frame=frame;
    
    switch (orderStatus) {
        case OrderStatusOrdered:{
            [_btClose setTitle:[languageKey stringByKey:@"general.bt.close"] forState:UIControlStateNormal];
            _btClose.layer.borderColor=tkColorPending.CGColor;
            _btClose.layer.borderWidth=1;
            _btClose.backgroundColor=[UIColor clearColor];
            [_btClose setTitleColor:tkColorPending forState:UIControlStateNormal];
            _btChangeStatus.layer.borderColor=tkColorPending.CGColor;
            _btChangeStatus.backgroundColor=tkColorPending;
            [_btChangeStatus setTitle:[languageKey stringByKey:@"general.bt.ready"] forState:UIControlStateNormal];
            [_btChangeStatus setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        }
        case OrderStatusReadyCollection:{
            [_btClose setTitle:[languageKey stringByKey:@"general.bt.close"] forState:UIControlStateNormal];
            _tbvMain.tableFooterView=nil;
            [_tbvMain setNeedsDisplay];
            _btClose.layer.borderColor=tkColorReady.CGColor;
            _btClose.layer.borderWidth=1;
            _btClose.backgroundColor=[UIColor clearColor];
            [_btClose setTitleColor:tkColorReady forState:UIControlStateNormal];
            _btChangeStatus.layer.borderColor=tkColorReady.CGColor;
            _btChangeStatus.backgroundColor=tkColorReady;
            [_btChangeStatus setTitle:[languageKey stringByKey:@"general.bt.complete"] forState:UIControlStateNormal];
            [_btChangeStatus setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        }
        default:
            break;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return (listProductOrders.count==0)?0:3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return listProductOrders.count;
    }
    if (section==1) {
        return listService.count;
    }
    if (section==2) {
        return 0;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        ProductOrderSingle *pro=listProductOrders[indexPath.row];
        if ([pro discount]) {
            return 90;
        }else{
            return 70;
        }
    }else{
        return 50;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *vH=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 10)];
    UIView *lineT=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 1)];
    UIView *lineB=[[UIView alloc] initWithFrame:CGRectMake(0, 9, _tbvMain.frame.size.width, 1)];
    vH.backgroundColor=tkColorMainBackground;
    lineT.backgroundColor=tkColorFrameBorder;
    lineB.backgroundColor=tkColorFrameBorder;
    [vH addSubview:lineT];
    if (section<2) {
        [vH addSubview:lineB];
    }
    return vH;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        TableCellProductOrder *cell =(TableCellProductOrder*)[tableView dequeueReusableCellWithIdentifier:CellOrderIdentify];
        [cell resetValueDefault];
        ProductOrderSingle *productOrder =listProductOrders[indexPath.row];
        cell.lbTitle.text=productOrder.productName;
        cell.lbDescription.text=productOrder.variantName;
        cell.lbQuantity.text =[NSString stringWithFormat:@"%@%li",[languageKey stringByKey:@"cashier.order.text-quantity-order-x"],(long)productOrder.quantity];
        double totalCured=[CashierBusinessModel totalAmountCrudeOfProductOrder:productOrder];
        cell.lbAmountTotal.text=[amountDisplay stringAmountHaveCurrency:totalCured];
        TKDiscount *discount=[productOrder discount];
        if (discount) {
            double discountAmount=[CashierBusinessModel totalDiscountOfProductOrder:productOrder];
            cell.lbAmoutDiscount.text=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:discountAmount]];
            NSMutableString *discountShow=[[NSMutableString alloc]initWithString:discount.name];//[languageKey stringByKey:@"cashier.order.text-discount"]];
            if (discount.type==DiscountTypePercent) {
                [discountShow appendFormat:@" (%.2f%%)",discount.discountValue];
            }
            cell.lbDiscount.text=discountShow;
            [cell setLineBottomTo:89];
        }else{
            [cell setLineBottomTo:69];
        }
        cell.lineBottom.hidden=(indexPath.row==listProductOrders.count-1);
        return cell;
    }else{
        //cell for discount, tax, service charges
        TableCellServiceOrder *cell=[tableView dequeueReusableCellWithIdentifier:CellServiceIdentify];
        [cell resetValueDefault];
        NSDictionary *data=listService[indexPath.row];
        cell.lbTitle.text=data[@"title"];
        cell.lbValue.text=data[@"value"];
        cell.vLine.hidden=(indexPath.row==listService.count-1);
        return cell;
    }
}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//-(void)acceptAction:(id)sender{
//    // confirm c     ancel order
//    if (acceiptCompleteOrder) {
//        orderDoc =[CashierBusinessModel updateStatusForOrder:orderDoc toOrderStatus:OrderStatusCollected];
//        [CashierBusinessModel sendEmailWithOrder:orderDoc emailIfNeed:nil];
//    }else{
//        orderDoc =[CashierBusinessModel updateStatusForOrder:orderDoc toOrderStatus:OrderStatusCanceled];
//        [CashierBusinessModel sendEmailWithOrder:orderDoc emailIfNeed:nil];
//    }
//    // confirm complete order
//    if (_delegate) {
//        [_delegate orderSelfCollectClose:self];
//    }
//}
- (IBAction)clickCancelNewOrder:(id)sender {
    // confirm cancel order
    UIAlertView *alertConfirm=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"cashier.order-self-collect.ms-confirm-cancel"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    [alertConfirm showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            [self cancelOrder];
        }
    }];
}
- (IBAction)clickClose:(id)sender {
    if ([TKOrder isCancelRequest:orderDoc]) {
        //reject
        UIAlertView *alertConfirm=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"cashier.order-self-collect.ms-confirm-reject-cancel"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
        [alertConfirm showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex==1) {
                [self rejectCancelOrder];
            }
        }];
        return;
    }
    //else
    if (_delegate) {
        [_delegate orderSelfCollectClose:self];
    }
}
- (IBAction)clickChangeStatus:(id)sender {
    if ([TKOrder isCancelRequest:orderDoc]) {
        UIAlertView *alertConfirm=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"cashier.order-self-collect.ms-confirm-approve-cancel"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
        [alertConfirm showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex==1) {
                [self approveCancelOrder];
            }
        }];
        return;
    }
    //else
    
    OrderStatus currentStatus=[TKOrder orderStatus:orderDoc];
    switch (currentStatus) {
        case OrderStatusOrdered:{
            self.view.userInteractionEnabled=NO;
            UIActivityIndicatorView *activity=tkActivityIndicatorViewDefault;
            activity.center=_btChangeStatus.center;
            [_btChangeStatus.superview addSubview:activity];
            [activity startAnimating];
            [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
                NSInteger orderAvailable=[self checkOrderAvailable];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [activity stopAnimating];
                    [activity removeFromSuperview];
                    self.view.userInteractionEnabled=YES;
                    if (orderAvailable >0) {
                        [self readyOrder];
                    }else{
                        NSString *alert=nil;
                        if (orderAvailable <0) {
                            alert =[languageKey stringByKey:@"cashier.order-self-collect.ms-product-not-available"];
                        }else{
                            alert =[languageKey stringByKey:@"cashier.order-self-collect.ms-not-enough-item"];
                        }
                        UIAlertView *alertV=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-warning"] message:alert delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
                        [alertV showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                            if (buttonIndex==1) {
                                [self readyOrder];
                            }
                        }];
                    }
                });
            }];
            break;
        }
        case OrderStatusReadyCollection:{
            // confrim complete order
            UIAlertView *alertConfirm=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"cashier.order-self-collect.ms-confirm-complete"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
            [alertConfirm showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex==1) {
                    [self completeOrder];
                }
            }];
            break;
        }
        default:
            break;
    }
}
-(void)approveCancelOrder{// approve cancel order from customer
    orderDoc =[CashierBusinessModel updateStatusForOrder:orderDoc toOrderStatus:OrderStatusCanceled];
    [CashierBusinessModel sendEmailWithOrder:orderDoc emailIfNeed:nil type:MailTypeCancel];
}
-(void)rejectCancelOrder{//reject cancel order from customer
    orderDoc =[CashierBusinessModel rejectCancelRequestOrder:orderDoc];
    [CashierBusinessModel sendEmailWithOrder:orderDoc emailIfNeed:nil type:MailTypeRejectCancel];
}
-(void)cancelOrder{
    orderDoc =[CashierBusinessModel updateStatusForOrder:orderDoc toOrderStatus:OrderStatusCanceled];
    [CashierBusinessModel sendEmailWithOrder:orderDoc emailIfNeed:nil type:MailTypeCancel];
}
-(void)completeOrder{
    orderDoc =[CashierBusinessModel updateStatusForOrder:orderDoc toOrderStatus:OrderStatusCollected];
}
-(void)readyOrder{
    [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
        orderDoc = [CashierBusinessModel updateStatusForOrder:orderDoc toOrderStatus:OrderStatusReadyCollection];
        [CashierBusinessModel sendEmailWithOrder:orderDoc emailIfNeed:nil type:MailTypeReady];
        OrderStatus currentStatus =[TKOrder orderStatus:orderDoc];
        [self setActionWithOrderStatus:currentStatus cancelRequire:[TKOrder isCancelRequest:orderDoc]];
    }];
    if (arrayProductOrderInStoreOfOrder) {
        for (NSDictionary *productOrderInStore in arrayProductOrderInStoreOfOrder) {
            [_delegate orderSelfCollect:self updateQuantityProductID:productOrderInStore[@"productId"] variantSKU:productOrderInStore[@"variantSKU"] add:0 sub:[productOrderInStore[@"quantity"] integerValue]];
        }
    }
}
-(NSInteger)checkOrderAvailable{
    NSArray *arrayProduct =[CashierBusinessModel arrayProductOrderWithProductOrdersJSON:[TKOrder arrayProductJSON:orderDoc]];
    BOOL notEnoughQuantity=NO;
    BOOL notAvailableProduct=NO;
    //update arrayProductInStoreOfOrder
    [arrayProductOrderInStoreOfOrder removeAllObjects];
    for (ProductOrderSingle *productOrder in arrayProduct) {
        CBLDocument *productInStore =[Controller productWithSKU:productOrder.productSKU];
        //check product available
        if (productInStore==nil || ![TKProductItem getStatus:productInStore] || ![TKProductItem getDisplay:productInStore] ) {
            notAvailableProduct=YES;
            break;
        }
        //check variant available
        
        if (![TKProductItem checkVariantAvailable:productInStore withSKU:productOrder.variantSKU]) {
            notAvailableProduct=YES;
            break;
        }
        
        //update arrayProductInStore
        [arrayProductOrderInStoreOfOrder addObject:@{@"productId":productInStore.documentID,
                                                     @"variantSKU":productOrder.variantSKU,
                                                     @"quantity":@(productOrder.quantity)}];
        //check variant quantity available
        
        if (![TKProductItem checkQuantityVariantAvailable:productInStore withSKU:productOrder.variantSKU quanity:productOrder.quantity]) {
            notEnoughQuantity=YES;
            continue;
        }
    }
    if (notAvailableProduct) {
        return -1; // product not available
    }
    if (notEnoughQuantity) {
        return 0; // quantity not available
    }
    return 1;// available all
}
-(CBLDocument *)orderDocument{
    return orderDoc;
}
@end
