//
//  SearchCategory.h
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "Constant.h"

@protocol SearchCategoryDelegate <NSObject>
-(void)searchCategoryVC:(id)searchVC data:(CBLDocument*)data;
@required
-(CBLDocument*)categorySelected;
@required

@end
#import <UIKit/UIKit.h>
#import <CouchbaseLite/CouchbaseLite.h>


@interface SearchCategory : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    CBLLiveQuery *liveQuery;
    NSMutableArray *listCategory;
    NSMutableArray *listNumberProductInCategory;
    NSInteger totalNumberProduct;
}
@property (weak,nonatomic) id<SearchCategoryDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbNoCategory;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (strong, nonatomic) IBOutlet UIView *vCellAllProduct;
@property (weak, nonatomic) IBOutlet UIImageView *ivTickAllProduct;
@property (weak, nonatomic) IBOutlet UIButton *lbAllProduct;
- (IBAction)clickAllProduct:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vLineAllProduct;


@end
