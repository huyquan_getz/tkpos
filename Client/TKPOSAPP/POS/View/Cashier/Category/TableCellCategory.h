//
//  TableCellCategory.h
//  POS
//
//  Created by Nha Duong Cong on 11/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLImageView.h"

@interface TableCellCategory : UITableViewCell
@property (weak, nonatomic) IBOutlet DLImageView *ivMain;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIView *vLine;
-(void)setValueDefault;
@end
