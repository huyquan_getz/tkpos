//
//  DiscountVC.m
//  POS
//
//  Created by Cong Nha Duong on 2/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "DiscountVC.h"
#import "Constant.h"
#import "TKAmountDisplay.h"

@interface DiscountVC ()

@end

@implementation DiscountVC{
    NSArray *arrayBTPercentTages;
}
-(instancetype)initWithDiscount:(TKDiscount *)discount{
    if (self=[super init]) {
        oldDiscount=discount;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor clearColor];
    UIView *vCorver=[[UIView alloc] initWithFrame:self.view.bounds];
    [vCorver setViewCoverDefault];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [vCorver addGestureRecognizer:tap];
    [self.view insertSubview:vCorver belowSubview:_vMain];
    [_vMain setShadowDefault];
    _vTitle.layer.cornerRadius=tkCornerRadiusButton;
    arrayBTPercentTages=@[_btPercent5,_btPercent10,_btPercent20,_btPercent30,_btPercent50];
    NSArray *lines=@[_vLine1,_vLine2,_vLine3,_vLine4,_vLine5,_vLine6,_vLine10,_vLine11,_vLine12];
    for (UIView *line in lines) {
        line.backgroundColor=tkColorFrameBorder;
    }
    _btPercent5.tag=5;
    _btPercent10.tag=10;
    _btPercent20.tag=20;
    _btPercent30.tag=30;
    _btPercent50.tag=50;
    UIImage *bgColorN=[UIImage imageWithColor:tkColorMainBackground];
    UIImage *bgColorS=[UIImage imageWithColor:tkColorMain];
    for (UIButton *bt in arrayBTPercentTages) {
        [bt setBackgroundImage:bgColorN forState:UIControlStateNormal];
        [bt setBackgroundImage:bgColorS forState:UIControlStateSelected];
        [bt setBackgroundImage:bgColorS forState:UIControlStateHighlighted];
        [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [bt setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [bt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        bt.layer.borderWidth=1;
        bt.layer.borderColor=tkColorFrameBorder.CGColor;
    }
    NSArray *btNumbers=@[_btNumber0,_btNumber1,_btNumber2,_btNumber3,_btNumber4,_btNumber5,_btNumber6,_btNumber7,_btNumber8,_btNumber9];
    for (NSInteger i=0; i<btNumbers.count; i++) {
        UIButton *bt=(UIButton*)btNumbers[i];
        [bt setTitle:[NSString stringWithFormat:@"%d",i] forState:UIControlStateNormal];
        bt.tag=i;
    }
    [_btAdd setTitleColor:tkColorMain forState:UIControlStateNormal];
    
    UIView *vR=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 1)];
    _tfValue.rightView=vR;
    _tfValue.rightViewMode=UITextFieldViewModeAlways;
    _tfValue.backgroundColor=tkColorMainBackground;
    _tfValue.layer.borderColor=tkColorFrameBorder.CGColor;
    _tfValue.layer.borderWidth=1;
    _tfValue.layer.cornerRadius=tkCornerRadiusButton;
    _btClear.layer.cornerRadius=tkCornerRadiusButton;
    _btClear.backgroundColor=tkColorFrameBorder;
    _tfValue.enabled=NO;
    _tfValue.placeholder=[TKAmountDisplay stringByRoundCashAmout:0];
    _tfValue.text=@"";
    _vTitle.backgroundColor=tkColorMainBackground;
    [_segment setTintColor:tkColorMain];
    _vMain.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vMain.clipsToBounds=YES;
    [self showMessageError:@""];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [self setCurrentValue];
}
-(void)setCurrentValue{
    if (oldDiscount) {
        _segment.selectedSegmentIndex=oldDiscount.type;
        [self clickSegment:nil];
        _tfValue.text=[TKAmountDisplay stringByRoundCashAmout:oldDiscount.discountValue];
    }else{
        _segment.selectedSegmentIndex=0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
     
-(void)showMessageError:(NSString*)error{
    _lbMessageStatus.textColor=[UIColor redColor];
    _lbMessageStatus.text=error;
}
-(void)tapCoverView:(id)sender{
    [self clickClose:nil];
}
- (IBAction)clickClose:(id)sender {
    if (_delegate) {
        [_delegate discount:self clickClose:YES];
    }
}
- (IBAction)clickAdd:(id)sender {
    if (![self validateInput] && oldDiscount==nil) {
        return;
    }
    if (_delegate) {
        [_delegate discount:self updateDiscount:oldDiscount withNew:[self currentDiscountValue]];
    }
}
-(TKDiscount*)currentDiscountValue{
    if ([self validateInput]) {
        NSDate *currentDate=[NSDate date];
        NSString *discountID=[currentDate getStringWithFormat:@"yyyyMMddHHmmssSSS"];
        return [[TKDiscount alloc] initWithID:discountID name:[[LanguageUtil sharedLanguageUtil] stringByKey:@"cashier.order.text-cart-discount"] type:(DiscountType)_segment.selectedSegmentIndex discountValue:[_tfValue.text doubleValue] noLimitTime:YES beginDate:[NSDate date] endDate:tkNoLimitDate];
    }
    return nil;
}
-(BOOL)validateInput{
    if ([_tfValue isEmptyText]) {
        [self showMessageError:@"Please enter discount value to continue"];
        return NO;
    }
    [self showMessageError:@""];
    return YES;
}
- (IBAction)clickClear:(id)sender {
    _tfValue.text=@"";
    [self clickPercent:nil];
}
-(void)showPercentValue:(double)percent{
    [self setTextForValue:[TKAmountDisplay stringByRoundCashAmout:percent]];
}
-(BOOL)setTextForValue:(NSString*)text{
    if (![text isTKAmountFormat]) {
        return NO;
    }
    _tfValue.text=text;
    return YES;
}
-(void)showPercentage:(BOOL)show{
    for (UIButton *bt in arrayBTPercentTages) {
        bt.enabled=show;
    }
}
- (IBAction)clickPercent:(UIButton*)sender {
    sender.selected=YES;
    for (UIButton *bt in arrayBTPercentTages) {
        if (bt!=sender) {
            bt.selected=NO;
        }
    }
    NSInteger percent=sender.tag;
    if (sender) {
        [self showPercentValue:percent];
    }
    
}
- (IBAction)clickSegment:(id)sender {
    [self clickClear:nil];
    if (_segment.selectedSegmentIndex ==0) {
        [self showPercentage:YES];
    }else{
        [self clickPercent:nil];
        [self showPercentage:NO];
    }
}

- (IBAction)clickNumber:(UIButton*)sender{
    NSInteger nb=sender.tag;
    NSString *newString=[NSString stringWithFormat:@"%@%d",_tfValue.text,nb];
    [self setTextForValue:newString];
}

- (IBAction)clickDot:(id)sender {
    NSString *newString=[_tfValue.text stringByAppendingString:@"."];
    [self setTextForValue:newString];
}

- (IBAction)clickZeroZero:(id)sender {
    NSString *newString=[_tfValue.text stringByAppendingString:@"00"];
    [self setTextForValue:newString];
}
@end
