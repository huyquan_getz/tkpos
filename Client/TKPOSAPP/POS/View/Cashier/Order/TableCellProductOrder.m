//
//  TableCellProductOrder.m
//  POS
//
//  Created by Nha Duong Cong on 11/25/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TableCellProductOrder.h"
#import "Constant.h"

@implementation TableCellProductOrder

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        _lbTitle.textColor=[UIColor whiteColor];
        _lbDescription.textColor=[UIColor whiteColor];
        _lbQuantity.textColor=[UIColor whiteColor];
        _lbAmountTotal.textColor=[UIColor whiteColor];
        _lbDiscount.textColor=[UIColor whiteColor];
        _lbAmoutDiscount.textColor=[UIColor whiteColor];
    }else{
        _lbTitle.textColor=[UIColor blackColor];
        _lbDescription.textColor=[UIColor darkGrayColor];
        _lbQuantity.textColor=[UIColor blackColor];
        _lbAmountTotal.textColor=[UIColor blackColor];
        _lbDiscount.textColor=[UIColor darkGrayColor];
        _lbAmoutDiscount.textColor=[UIColor darkGrayColor];
    }
    // Configure the view for the selected state
}
-(void)resetValueDefault{
    UIView *backgroundView =[[UIView alloc] init];
    [backgroundView setBackgroundColor:tkColorMainSelected];
    [super setSelectedBackgroundView:backgroundView];
    _lbTitle.text=@"";
    _lbDescription.text=@"";
    _lbQuantity.text=@"";
    _lbDiscount.text=@"";
    _lbAmoutDiscount.text=@"";
    self.backgroundColor=[UIColor clearColor];
    _lineBottom.backgroundColor=tkColorFrameBorder;
}
-(void)setLineBottomTo:(NSInteger)positionY{
    CGRect frame=_lineBottom.frame;
    frame.origin.y=positionY;
    _lineBottom.frame=frame;
}
@end
