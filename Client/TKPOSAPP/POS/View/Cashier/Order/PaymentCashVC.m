//
//  PaymentCashVC.m
//  POS
//
//  Created by Nha Duong Cong on 11/28/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "PaymentCashVC.h"
#import "CashierBusinessModel.h"
#import "PaymentBusinessModel.h"


@interface PaymentCashVC ()

@end

@implementation PaymentCashVC{
    LanguageUtil *languageKey;
}
-(instancetype)initWithAmountPay:(double)amPay currency:(NSString *)currency_{
    if (self= [super init]) {
        amountPay=amPay;
        currency=currency_;
        indexInNumberRound1=0;
        indexInNumberRound2=indexInNumberRound1+1;
        indexInNumberRound3=indexInNumberRound2+1;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _tfAmountGet.layer.borderColor=tkColorFrameBorder.CGColor;
    _tfAmountGet.layer.borderWidth=1;
    _tfAmountGet.layer.cornerRadius=tkCornerRadiusButton;
    _tfAmountGet.rightView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 192, 0)];
    _tfAmountGet.rightViewMode=UITextFieldViewModeAlways;
    
    _btTender.layer.cornerRadius=tkCornerRadiusButton;
    _btTender.clipsToBounds=YES;
    _btTender.backgroundColor=tkColorMainAccept;
    
    _btRoundCharge1.layer.cornerRadius=tkCornerRadiusButton;
    _btRoundCharge2.layer.cornerRadius=tkCornerRadiusButton;
    _btRoundCharge3.layer.cornerRadius=tkCornerRadiusButton;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [_btTender setTitle:[languageKey stringByKey:@"cashier.payment.bt.cash-tender"] forState:UIControlStateNormal];
    [_btRoundCharge1 setTitle:[TKAmountDisplay stringAmount:[TKAmountDisplay ceil:amountPay withIndexNumberCeil:indexInNumberRound1] withCurrency:currency] forState:UIControlStateNormal];
    [_btRoundCharge2 setTitle:[TKAmountDisplay stringAmount:[TKAmountDisplay ceil:amountPay withIndexNumberCeil:indexInNumberRound2] withCurrency:currency] forState:UIControlStateNormal];
    [_btRoundCharge3 setTitle:[TKAmountDisplay stringAmount:[TKAmountDisplay ceil:amountPay withIndexNumberCeil:indexInNumberRound3] withCurrency:currency] forState:UIControlStateNormal];
    if ([_btRoundCharge2.titleLabel.text isEqualToString:_btRoundCharge1.titleLabel.text]) {
        _btRoundCharge2.enabled=NO;
        _btRoundCharge2.alpha=0.6;
    }
    if ([_btRoundCharge3.titleLabel.text isEqualToString:_btRoundCharge2.titleLabel.text]) {
        _btRoundCharge3.enabled=NO;
        _btRoundCharge3.alpha=0.6;
    }
    _tfAmountGet.text=[TKAmountDisplay stringAmount:amountPay withCurrency:currency];
}
-(void)resetRoundAmount{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)clickTender:(id)sender {
    [self.view endEditing:YES];
    double chargeGet=[[_tfAmountGet.text substringFromIndex:currency.length] doubleValue];
    if (_delegate) {
        PaymentChild *paymentChild;
        if (chargeGet>amountPay) {
            paymentChild=[[PaymentChild alloc] initPaymentCashWithCharged:amountPay changeDue:(chargeGet - amountPay)];
        }else{
            paymentChild=[[PaymentChild alloc] initPaymentCashWithCharged:chargeGet];
        }
        [_delegate paymenter:self pay:paymentChild];
    }
}
-(IBAction)clickRoundCharge1:(UIButton*)sender{
    _tfAmountGet.text =sender.titleLabel.text;
    [self clickTender:nil];
}
-(IBAction)clickRoundCharge2:(UIButton*)sender{
    _tfAmountGet.text =sender.titleLabel.text;
    [self clickTender:nil];
}
-(IBAction)clickRoundCharge3:(UIButton*)sender{
    _tfAmountGet.text =sender.titleLabel.text;
    [self clickTender:nil];
}

#pragma TextfieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (newString.length <currency.length+1) { // currency + space
        return NO;
    }
    if (![[newString substringFromIndex:currency.length+1] isTKAmountFormat]) {
        return NO;
    }
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=self.view.superview.frame;
    frame.origin.y-= 200;
    [self.view.superview setFrame:frame];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=self.view.superview.frame;
    frame.origin.y+= 200;
    [self.view.superview setFrame:frame];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}

@end
