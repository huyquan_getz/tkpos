//
//  TableCellProductOrder.h
//  POS
//
//  Created by Nha Duong Cong on 11/25/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TableCellProductOrder : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lbAmountTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbAmoutDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lbDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lineBottom;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
-(void)setLineBottomTo:(NSInteger)positionY;
-(void)resetValueDefault;

@end
