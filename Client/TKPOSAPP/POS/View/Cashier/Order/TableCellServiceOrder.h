//
//  TableCellServiceOrder.h
//  POS
//
//  Created by Cong Nha Duong on 1/27/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellServiceOrder : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbValue;
@property (weak, nonatomic) IBOutlet UIView *vLine;
-(void)resetValueDefault;
@end
