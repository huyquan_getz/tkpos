//
//  PaymentCashVC.h
//  POS
//
//  Created by Nha Duong Cong on 11/28/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Paymenter.h"

@interface PaymentCashVC : UIViewController<PaymenterDataSource,UITextFieldDelegate>{
    double amountPay;
    NSString *currency;
    NSUInteger indexInNumberRound1;
    NSUInteger indexInNumberRound2;
    NSUInteger indexInNumberRound3;
    double roundAmount1;
    double roundAmount2;
    double roundAmount3;
}
-(instancetype)initWithAmountPay:(double)amPay currency:(NSString*)currency_;
- (IBAction)clickTender:(id)sender;
@property (weak,nonatomic) id<PaymenterDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btTender;
@property (weak, nonatomic) IBOutlet UITextField *tfAmountGet;
@property (weak, nonatomic) IBOutlet UIButton *btRoundCharge1;
@property (weak, nonatomic) IBOutlet UIButton *btRoundCharge2;
@property (weak, nonatomic) IBOutlet UIButton *btRoundCharge3;
- (IBAction)clickRoundCharge1:(id)sender;
- (IBAction)clickRoundCharge2:(id)sender;
- (IBAction)clickRoundCharge3:(id)sender;
@end
