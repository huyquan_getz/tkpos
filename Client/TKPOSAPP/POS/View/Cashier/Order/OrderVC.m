//
//  OrderVC.m
//  POS
//
//  Created by Nha Duong Cong on 11/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define KeyLoadFirstView @"orderVCLoadFirstView"
#define CellOrderIdentify @"cellOrder"
#define CellServiceIdentify @"cellService"
#import "OrderVC.h"
#import "CashierBusinessModel.h"
#import "PaymentBusinessModel.h"
#import "TKOrder.h"
#import "CouchBaseBusinessModel.h"
#import "Controller.h"
#import "File.h"
#import "PrinterController.h"
#import "TableCellServiceOrder.h"
#import "AdhocProduct.h"

@interface OrderVC ()

@end

@implementation OrderVC{
    LanguageUtil *languageKey;
    NSMutableArray *listCompleteInfo;
}
-(instancetype)initWithDefault{
    if (self =[super init]) {
        listProductOrders =[[NSMutableArray alloc] init];
        listAdhocProduct=[[NSMutableArray alloc] init];
        listCompleteInfo=[[NSMutableArray alloc] init];
        createdDate =[NSDate date];
        cashierBusiness =[[CashierBusinessModel alloc] initServiceDefault];
        orderDoc =[cashierBusiness createNewOrder];
        createdDate =[TKOrder createdDate:orderDoc];
        [SNLog Log:@"Cr %@",orderDoc.documentID];
    }
    return self;
}
-(instancetype)initWithOrder:(CBLDocument *)orderDocument{
    if (self =[super init]) {
        orderDoc=orderDocument;
        listCompleteInfo=[[NSMutableArray alloc] init];
        NSArray *productOrderJSON=[TKOrder arrayProductJSON:orderDoc];
        listProductOrders =[[NSMutableArray alloc] initWithArray:[CashierBusinessModel arrayProductOrderWithProductOrdersJSON:productOrderJSON]];
        listAdhocProduct=[[NSMutableArray alloc] initWithArray:[CashierBusinessModel arrayAdhocProductWithAdhocProductJSON:[TKOrder arrayAdhocProductJSON:orderDoc]]];
        createdDate =[TKOrder createdDate:orderDoc];
        NSString *currency=[TKOrder currency:orderDoc];
        if (currency==nil) {
            currency=[Controller currencyDefault];
        }
        TKDiscount *discountTotal=[TKOrder discountCartItem:orderDoc];
        NSArray *listTax=[TKOrder listTaxes:orderDoc];
        TKTaxServiceCharge *serviceCharge=[TKOrder serviceCharge:orderDoc];
        cashierBusiness=[[CashierBusinessModel alloc] initWithDiscountTotal:discountTotal listTax:listTax serviceCharge:serviceCharge currency:currency];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellProductOrder" bundle:nil] forCellReuseIdentifier:CellOrderIdentify];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellServiceOrder" bundle:nil] forCellReuseIdentifier:CellServiceIdentify];
    _vLine1.backgroundColor=tkColorFrameBorder;
    _vLine2.backgroundColor=tkColorFrameBorder;
    _btCustomer.imageView.contentMode=UIViewContentModeScaleAspectFill;
    _btPayment.layer.cornerRadius=tkCornerRadiusButton;
    _tbvMain.tableFooterView=_btCancelNewOrder;
    _tbvMain.backgroundColor=tkColorMainBackground;
    [_btCancelNewOrder setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    _btCancelNewOrder.layer.borderWidth=1;
    _btCancelNewOrder.layer.borderColor=tkColorFrameBorder.CGColor;

    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [_btCancelNewOrder setTitle:[languageKey stringByKey:@"cashier.order.bt.cancel-order"] forState:UIControlStateNormal];
    _lbTimeNewOrder.text =[createdDate getStringWithFormat:@"HH:mm"];
    _lbOrderCode.text =[TKOrder orderCode:orderDoc];
    [self resetData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateService{
    if (languageKey==nil) {
        languageKey=[LanguageUtil sharedLanguageUtil];
    }
    NSMutableArray *newServices=[[NSMutableArray alloc] init];
    double discount=[cashierBusiness totalDiscount];
    double subtotal=[cashierBusiness subTotal];
    double subtotalHaveServiceCharge=[cashierBusiness subTotalHaveServiceCharge];
    [newServices addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-total-discount"],
                            @"value":[cashierBusiness.amountDisplay stringAmountHaveCurrency:discount]}];
    [newServices addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-subtotal"],
                             @"value":[cashierBusiness.amountDisplay stringAmountHaveCurrency:subtotal]}];
    if (cashierBusiness.serviceCharge) {
        double totalServiceCharge=[cashierBusiness totalServiceCharges];
        [newServices addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-service-charges"],
                                 @"value":[cashierBusiness.amountDisplay stringAmountHaveCurrency:totalServiceCharge]}];
    }
    for (TKTaxServiceCharge *tax in cashierBusiness.listTax) {
        double totalTax=[CashierBusinessModel taxServiceAmount:subtotalHaveServiceCharge withTaxService:tax];
        [newServices addObject:@{@"title":tax.name,
                                 @"value":[cashierBusiness.amountDisplay stringAmountHaveCurrency:totalTax]}];
    }
    double roundAmout=[cashierBusiness roundAmount];
    if(![TKAmountDisplay isZero:roundAmout]){
        NSString *value;
        if (roundAmout>0) {
            value=[cashierBusiness.amountDisplay stringAmountHaveCurrency:roundAmout];
        }else{
            value=[NSString stringWithFormat:@"-%@",[cashierBusiness.amountDisplay stringAmountHaveCurrency:-roundAmout]];
        }
        [newServices addObject:@{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.round-amt"],@"value":value}];
    }
    [listCompleteInfo removeAllObjects];
    [listCompleteInfo addObjectsFromArray:newServices];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return ([self isEmptyOrder])?0:5;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return listProductOrders.count;
    }
    if (section==1) {
        return listAdhocProduct.count;
    }
    if (section==2) {
        if (cashierBusiness.discount && cashierBusiness.discount.type==DiscountTypeValue) {
            return 1;
        }else{
            return 0;
        }
    }
    if (section==3) {
        return listCompleteInfo.count;
    }
    if (section==4) {
        return 0;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        ProductOrderSingle *pro=listProductOrders[indexPath.row];
        if ([pro discount] || (cashierBusiness.discount && cashierBusiness.discount.type==DiscountTypePercent)) {
            return 90;
        }else{
            return 70;
        }
    }else if (indexPath.section==1){
        AdhocProduct *adhoc=listAdhocProduct[indexPath.row];
        if ([adhoc discountProductItem] || (cashierBusiness.discount && cashierBusiness.discount.type==DiscountTypePercent && adhoc.applyDiscount)) {
            return 90;
        }else{
            return 70;
        }
    }else{
        return 50;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==1||section==2) {
        return 0;
    }
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==1) {
        return nil;
    }
    UIView *vH=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 10)];
    UIView *lineT=[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbvMain.frame.size.width, 1)];
    UIView *lineB=[[UIView alloc] initWithFrame:CGRectMake(0, 9, _tbvMain.frame.size.width, 1)];
    vH.backgroundColor=tkColorMainBackground;
    lineT.backgroundColor=tkColorFrameBorder;
    lineB.backgroundColor=tkColorFrameBorder;
    [vH addSubview:lineT];
    if (section<4) {
        [vH addSubview:lineB];
    }
    return vH;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section==0) {
        TableCellProductOrder *cell =(TableCellProductOrder*)[tableView dequeueReusableCellWithIdentifier:CellOrderIdentify];
        [cell resetValueDefault];
        ProductOrderSingle *productOrder =listProductOrders[indexPath.row];
        cell.lbTitle.text=productOrder.productName;
        cell.lbDescription.text=productOrder.variantName;
        cell.lbQuantity.text =[NSString stringWithFormat:@"%@%li",[languageKey stringByKey:@"cashier.order.text-quantity-order-x"],(long)productOrder.quantity];
        double totalCured=[cashierBusiness totalAmountCrudeOfProductOrder:productOrder];
        cell.lbAmountTotal.text=[cashierBusiness.amountDisplay stringAmountHaveCurrency:totalCured];
        TKDiscount *discount=[productOrder discount];
        if (discount==nil && cashierBusiness.discount &&cashierBusiness.discount.type==DiscountTypePercent) {
            discount=cashierBusiness.discount;// not have discount item but have discount total
        }
        if (discount) {
            double discountAmount=[cashierBusiness totalDiscountOfProductOrder:productOrder];
            cell.lbAmoutDiscount.text=[NSString stringWithFormat:@"-%@",[cashierBusiness.amountDisplay stringAmountHaveCurrency:discountAmount]];
            NSMutableString *discountShow=[[NSMutableString alloc]initWithString:discount.name];//[languageKey stringByKey:@"cashier.order.text-discount"]];
            if (discount.type==DiscountTypePercent) {
                [discountShow appendFormat:@" (%.2f%%)",discount.discountValue];
            }
            cell.lbDiscount.text=discountShow;
            [cell setLineBottomTo:89];
        }else{
            [cell setLineBottomTo:69];
        }
        cell.lineBottom.hidden=(indexPath.row==listProductOrders.count-1 && listAdhocProduct.count==0 && (cashierBusiness.discount==nil || cashierBusiness.discount.type!=DiscountTypeValue));
        return cell;
    }else if(indexPath.section==1){
        //ad hoc
        TableCellProductOrder *cellAdhoc =(TableCellProductOrder*)[tableView dequeueReusableCellWithIdentifier:CellOrderIdentify];
        [cellAdhoc resetValueDefault];
        AdhocProduct *adhoc=listAdhocProduct[indexPath.row];
        cellAdhoc.lbTitle.text=adhoc.title;
        cellAdhoc.lbQuantity.text =[NSString stringWithFormat:@"%@%li",[languageKey stringByKey:@"cashier.order.text-quantity-order-x"],(long)adhoc.quantity];
        cellAdhoc.lbDescription.text=adhoc.variantName;
        TKDiscount *discount=adhoc.discountProductItem;
        double totalCured=[cashierBusiness totalAmountCrudeOfAdhocProduct:adhoc];
        cellAdhoc.lbAmountTotal.text=[cashierBusiness.amountDisplay stringAmountHaveCurrency:totalCured];
        if (discount==nil && cashierBusiness.discount && cashierBusiness.discount.type==DiscountTypePercent && adhoc.applyDiscount) {
            discount=cashierBusiness.discount;// not have discount item but have discount total
        }
        if (discount) {
            double discountAmount=[cashierBusiness totalDiscountOfAdhocProduct:adhoc];
            cellAdhoc.lbAmoutDiscount.text=[NSString stringWithFormat:@"-%@",[cashierBusiness.amountDisplay stringAmountHaveCurrency:discountAmount]];
            NSMutableString *discountShow=[[NSMutableString alloc]initWithString:discount.name];//[languageKey stringByKey:@"cashier.order.text-discount"]];
            if (discount.type==DiscountTypePercent) {
                [discountShow appendFormat:@" (%.2f%%)",discount.discountValue];
            }
            cellAdhoc.lbDiscount.text=discountShow;
            [cellAdhoc setLineBottomTo:89];
        }else{
            [cellAdhoc setLineBottomTo:69];
        }
        cellAdhoc.lineBottom.hidden=(indexPath.row==listAdhocProduct.count-1 && (cashierBusiness.discount==nil || cashierBusiness.discount.type!=DiscountTypeValue));
        return cellAdhoc;
    }else if(indexPath.section==2){
        //cell for discount total with value
        TableCellServiceOrder *cell=[tableView dequeueReusableCellWithIdentifier:CellServiceIdentify];
        [cell resetValueDefault];
        cell.lbTitle.text=cashierBusiness.discount.name;
        cell.lbValue.text=[NSString stringWithFormat:@"-%@",[cashierBusiness.amountDisplay stringAmountHaveCurrency:[cashierBusiness totalCartDiscountValue]]];
        cell.lbValue.font=tkFontMainWithSize(15);
        cell.vLine.hidden=YES;
        return cell;
    }else{
        //cell for discount, tax, service charges
        TableCellServiceOrder *cell=[tableView dequeueReusableCellWithIdentifier:CellServiceIdentify];
        [cell resetValueDefault];
        NSDictionary *data=listCompleteInfo[indexPath.row];
        cell.lbTitle.text=data[@"title"];
        cell.lbValue.text=data[@"value"];
        cell.vLine.hidden=(indexPath.row==listCompleteInfo.count-1);
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        //product order
        ProductOrderSingle *productOrder=listProductOrders[indexPath.row];
        if (_delegate) {
            [_delegate order:self clickDetail:productOrder];
        }
    }else if (indexPath.section==1){
        AdhocProduct *adhoc=listAdhocProduct[indexPath.row];
        if (_delegate) {
            [_delegate order:self clickDetailAdhoc:adhoc];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)isEmptyOrder{
    return listProductOrders.count==0 && listAdhocProduct.count==0;
}
-(void)deleteCurrentOrder{
    [[SyncManager sharedSyncManager] deleteDocument:orderDoc];
}
- (IBAction)clickCancelNewOrder:(id)sender {
    if ([self isEmptyOrder]) {
        [self deleteCurrentOrder];
        if (_delegate) {
            [_delegate orderClose:self andReAddNew:NO];
        }
    }else{
        // confirm cancel order
        UIAlertView *alertConfirm=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"cashier.order.mgs-confirm-cancel-order"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
        [alertConfirm showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex==1) {
                [self cancelOrder];
            }
        }];
    }
}
-(void)cancelOrder{
    [self deleteCurrentOrder];
    if (_delegate) {
        [_delegate orderCancel:self listProductOrder:listProductOrders];
    }
}
-(void)resetData{
    [cashierBusiness resetListProductOrder:listProductOrders];
    [cashierBusiness resetListAdhocProduct:listAdhocProduct];
    [self updateService];
    [self resetTotalCharge];
    [self resetChargeEnable];
    [_tbvMain reloadData];
}
-(void)resetTotalCharge{
    double totalCharge=[cashierBusiness grandTotal];
    [_btPayment setTitle:[NSString stringWithFormat:@"%@ %@",[languageKey stringByKey:@"cashier.order.text-charge"],[cashierBusiness.amountDisplay stringAmountHaveCurrency:totalCharge]] forState:UIControlStateNormal];
}
-(void)resetChargeEnable{
    if ([self isEmptyOrder]) {
        _btPayment.backgroundColor=[UIColor clearColor];
        _btPayment.enabled=NO;
    }else{
        _btPayment.enabled=YES;
        _btPayment.backgroundColor=tkColorMainAccept;
    }
}
-(void)saveToOrder{
    NSMutableDictionary *dataOrder=[[NSMutableDictionary alloc] init];
    [dataOrder setObject:@(PaymentStatusPending) forKey:tkKeyPaymentStatus];
    [dataOrder setObject:@(OrderTypeSelfCollect) forKey:tkKeyOrderType];
    [dataOrder setObject:[Controller getUserLoginedDoc].properties forKey:tkKeyStaff];
    [dataOrder setObject:[self customerDictionary] forKey:tkKeyCustomerSender];
    [dataOrder setObject:[self customerDictionary] forKey:tkKeyCustomerReceiver];
    [dataOrder setObject:[Controller getStoreInfoDoc].properties forKey:tkKeyInventory];
    [dataOrder setObject:[self listProductOrderShort] forKey:tkkeyArrayProduct];
    [dataOrder setObject:[CashierBusinessModel arrayAdhocProductJSONWithAdhocProduct:listAdhocProduct] forKey:tkKeyArrayAdhocProduct];
    double subTotal =[cashierBusiness subTotal];
    [dataOrder setObject:@(subTotal) forKey:tkKeySubTotal];
    double grandTotal =[cashierBusiness grandTotal];
    [dataOrder setObject:@(grandTotal) forKey:tkKeyGrandTotal];
    double roundAmount=[cashierBusiness roundAmount];
    [dataOrder setObject:@(roundAmount) forKey:tkKeyRoundAmount];
    [dataOrder setObject:@(roundAmount) forKey:@"roundAmnt"];
    [dataOrder setObject:@([cashierBusiness totalDiscount]) forKey:tkKeyDiscount];
    if (cashierBusiness.discount) {
        [dataOrder setObject:[cashierBusiness.discount jsonValue] forKey:tkKeyDiscountCartItem];
        [dataOrder setObject:@(cashierBusiness.totalCartDiscountValue) forKey:@"discountCartValue"];
    }else{
        [dataOrder setObject:[NSNull null] forKey:tkKeyDiscountCartItem];
        [dataOrder setObject:@(0.0) forKey:@"discountCartValue"];
    }
    [dataOrder setObject:@(cashierBusiness.discount && cashierBusiness.discount.type==DiscountTypeValue) forKey:tkKeyDiscountCart];
    [dataOrder setObject:@(OrderStatusPendingWaiting) forKey:tkKeyOrderStatus];
    [dataOrder setObject:[CBLJSON JSONObjectWithDate:[NSDate date]] forKey:tkKeyModifiedDate];
    [dataOrder setObject:[NSNull null] forKey:@"dateTimeInt"];
    [dataOrder setObject:[NSNull null] forKey:@"tax"];
    [dataOrder setObject:@([cashierBusiness totalTax]) forKey:@"totalTax"];
    [dataOrder setObject:@([cashierBusiness totalServiceCharges]) forKey:@"totalServiceCharge"];
    [dataOrder setObject:[NSNull null] forKey:@"instruction"];
    [dataOrder setObject:[NSNull null] forKey:@"serviceShipping"];
    [dataOrder setObject:[NSNull null] forKey:@"refund"];
    if (cashierBusiness.serviceCharge) {
        [dataOrder setObject:[CashierBusinessModel serviceChargeJSONWithTotalAmount:subTotal withServiceCharge:cashierBusiness.serviceCharge] forKey:@"serviceCharge"];
    }else{
        [dataOrder setObject:[NSNull null] forKey:@"serviceCharge"];
    }
    double subTotalHaveServiceCharge=[cashierBusiness subTotalHaveServiceCharge];
    [dataOrder setObject:[CashierBusinessModel listTaxesJSONWithTotalAmount:subTotalHaveServiceCharge withListTaxes:cashierBusiness.listTax] forKey:@"listTaxvalue"];
    [dataOrder setObject:[TKOrder orderCode:orderDoc] forKey:@"qrCode"];
    [dataOrder setObject:@(NO) forKey:@"isCancelRequest"];
    [dataOrder setObject:[NSNull null] forKey:@"transactionID"];
    [dataOrder setObject:@(NO) forKey:@"isCart"];
    [dataOrder setObject:cashierBusiness.amountDisplay.currency forKey:tkKeyCurrency];
    [dataOrder setObject:@"" forKey:@"remark"];
    [dataOrder setObject:[NSNull null] forKey:@"orderBy"];
    [dataOrder setObject:@(NO) forKey:@"paymentSuccess"];
    [dataOrder setObject:@"" forKey:@"customerDisplay"];
    orderDoc = [CashierBusinessModel updateInforOrder:orderDoc withJSON:dataOrder];
    [SNLog Log:@"%@",dataOrder];
}
-(void)applyCartDiscountToProduct{
    //when click chargeOrder
    for (ProductOrderSingle *pr in listProductOrders) {
        TKDiscount *discountItem=[pr discount];
        if (discountItem==nil && cashierBusiness.discount && cashierBusiness.discount.type==DiscountTypePercent) {
            [pr setDiscount:cashierBusiness.discount];
        }
    }
    
    for (AdhocProduct *adhoc in listAdhocProduct) {
        TKDiscount *discountItem=adhoc.discountProductItem;
        if (discountItem==nil && cashierBusiness.discount && cashierBusiness.discount.type==DiscountTypePercent && adhoc.applyDiscount) {
            [adhoc setDiscountProductItem:cashierBusiness.discount];
        }
    }
    [self resetData];
    [self saveToOrder];
}
-(void)removeCartDiscountFromProduct{
    //when cancel charge order;
    for (ProductOrderSingle *pr in listProductOrders) {
        TKDiscount *discountItem=[pr discount];
        if (discountItem && cashierBusiness.discount && [discountItem.discountID isEqualToString:cashierBusiness.discount.discountID]) {
            [pr setDiscount:nil];
        }
    }
    
    for (AdhocProduct *adhoc in listAdhocProduct) {
        TKDiscount *discountItem=adhoc.discountProductItem;
        if (discountItem && cashierBusiness.discount && [discountItem.discountID isEqualToString:cashierBusiness.discount.discountID]) {
            [adhoc setDiscountProductItem:nil];
        }
    }
    [self resetData];
    [self saveToOrder];
}
-(NSArray*)listProductOrderShort{
    NSMutableArray *arrayProductOrderShort=[[NSMutableArray alloc] init];
    NSMutableArray *listProductOrdersCopy =[listProductOrders mutableCopy];
    while (listProductOrdersCopy.count>0) {
        ProductOrderSingle *firstProductOrder=(ProductOrderSingle*)[listProductOrdersCopy firstObject];
        NSString *productIDFirst=[firstProductOrder productId];
        NSArray *listProductOrderWithProID=[self listProductOrderWithProductID:productIDFirst fromListProductOrder:listProductOrdersCopy];
        [arrayProductOrderShort addObject:[self mergeProductOrderSameProduct:listProductOrderWithProID]];
        [listProductOrdersCopy removeObjectsInArray:listProductOrderWithProID];
    }
    return arrayProductOrderShort;
}
-(NSArray *)listProductOrderWithProductID:(NSString*)productID fromListProductOrder:(NSArray*)listProductOrderOrginal{
    NSMutableArray *array =[[NSMutableArray alloc] init];
    for (ProductOrderSingle *productOrder in listProductOrderOrginal) {
        if ([[productOrder productId] isEqualToString:productID]) {
            [array addObject:productOrder];
        }
    }
    return array;
}
-(NSDictionary*)mergeProductOrderSameProduct:(NSArray*)productOrderSameProduct{
    return [CashierBusinessModel mergeProductOrderSameProduct:productOrderSameProduct];
}
-(void)addNewProductOrder:(ProductOrderSingle *)productOrder{
    NSInteger indexExisted=[self indexOfProductOrder:productOrder];
    if (indexExisted<0) {
        [listProductOrders addObject:productOrder];
    }else{
        // addition quantity to newProductOrder from oldProductOrder
        ProductOrderSingle *existedProductOrder =[listProductOrders objectAtIndex:indexExisted];
        productOrder.quantity += existedProductOrder.quantity;
        [listProductOrders replaceObjectAtIndex:indexExisted withObject:productOrder];
    }
}
-(void)removeProductOrder:(ProductOrderSingle*)productOrder{
    [listProductOrders removeObject:productOrder];
}
-(void)editProductOrder:(ProductOrderSingle *)oldProductOrder withProductOrder:(ProductOrderSingle *)newProductOrder{
    if (oldProductOrder==nil && newProductOrder) {
        [self addNewProductOrder:newProductOrder];
    }else if (oldProductOrder && newProductOrder==nil){
        [self removeProductOrder:oldProductOrder];
    }else if(oldProductOrder && newProductOrder){
        [self replaceProductOrder:oldProductOrder withProductOrder:newProductOrder];
    }
    [self resetData];
    [self saveToOrder];
}
-(void)replaceProductOrder:(ProductOrderSingle*)oldProductOrder withProductOrder:(ProductOrderSingle*)newProductOrder{
    NSInteger indexOldPr=[listProductOrders indexOfObject:oldProductOrder];
    if (indexOldPr < 0) {
        [self addNewProductOrder:newProductOrder];
    }else{
        NSInteger indexExistedNewPr=[self indexOfProductOrder:newProductOrder];
        if (indexExistedNewPr<0 || indexExistedNewPr==indexOldPr) {
            [listProductOrders replaceObjectAtIndex:indexOldPr withObject:newProductOrder];
        }else{
            // addition quantity to newProductOrder from oldProductOrder
            ProductOrderSingle *existedProductOrder =[listProductOrders objectAtIndex:indexExistedNewPr];
            newProductOrder.quantity += existedProductOrder.quantity;
            [listProductOrders replaceObjectAtIndex:indexExistedNewPr withObject:newProductOrder]; // indexOrderShowing in list hold when replace
            [listProductOrders removeObjectAtIndex:indexOldPr];
        }
    }
    [_tbvMain reloadData];
}
-(void)editAdhocProduct:(AdhocProduct *)oldAdhocProduct withAdhocProduct:(AdhocProduct *)newAdhocProduct{
    if (oldAdhocProduct==nil && newAdhocProduct) {
        [listAdhocProduct addObject:newAdhocProduct];
    }else if (oldAdhocProduct && newAdhocProduct==nil){
        [listAdhocProduct removeObject:oldAdhocProduct];
    }else if(oldAdhocProduct && newAdhocProduct){
        [listAdhocProduct replaceObjectAtIndex:[listAdhocProduct indexOfObject:oldAdhocProduct] withObject:newAdhocProduct];
    }
    [self resetData];
    [self saveToOrder];
}
-(NSInteger)indexOfProductOrder:(ProductOrderSingle*)productOrderN{
    for (int i=0;i<listProductOrders.count; i++) {
        if ([self compareProductOrder:productOrderN andProductOrder:listProductOrders[i]]) {
            return i;
        }
    }
    return -1;
}
-(BOOL)compareProductOrder:(ProductOrderSingle*)prOrder1 andProductOrder:(ProductOrderSingle*)prOrder2{
    NSDictionary *proProperty1=prOrder1.productProperies;
    NSDictionary *proProperty2=prOrder2.productProperies;
    NSArray *option1=prOrder1.variantOptions;
    NSArray *option2=prOrder2.variantOptions;
    if ([proProperty1[tkKeyId] isEqualToString: proProperty2[tkKeyId]] && [self compareVariantOption:option1 andVariantOption:option2]) {
        return YES;
    }
    return NO;
}
-(BOOL)compareVariantOption:(NSArray*)variantOption1 andVariantOption:(NSArray*)variantOption2{
    NSMutableArray *vOpitons1=[variantOption1 mutableCopy];
    [vOpitons1 removeObject:[NSNull null]];
    NSMutableArray *vOpitons2=[variantOption2 mutableCopy];
    [vOpitons2 removeObject:[NSNull null]];
    
    if (vOpitons1.count == vOpitons2.count) {
        BOOL isEqual=YES;
        for (int i=0;i<vOpitons1.count;i++) {
            NSDictionary *op1=vOpitons1[i];
            NSDictionary *op2=vOpitons2[i];
            if (![op1[@"optionKey"] isEqualToString:op2[@"optionKey"]] || ![op1[@"optionValue"] isEqualToString:op2[@"optionValue"]]) {
                isEqual=NO;
                break;
            }
        }
        return isEqual;
    }
    return NO;
}
- (IBAction)clickPayment:(id)sender {
    [self applyCartDiscountToProduct];
    if(_delegate){
        double totalCharge =[cashierBusiness grandTotal];
        totalCharge =[TKAmountDisplay tkRoundCashTotal:totalCharge];
        [_delegate order:self clickPayment:totalCharge];
    }
}
-(void)cancelPayment{
    [self removeCartDiscountFromProduct];
}
-(NSUInteger)currentQuanityWithProductOrder:(ProductOrderSingle *)productOrderCheck withEditOldProductOrder:(ProductOrderSingle *)oldProductOrder{
    NSInteger indexCurrentWithOrderCheck=[self indexOfProductOrder:productOrderCheck];
    if (oldProductOrder && [listProductOrders indexOfObject:oldProductOrder] == indexCurrentWithOrderCheck) {
        //variant current check is editing.
        return 0;
    }
    if (indexCurrentWithOrderCheck>=0 && indexCurrentWithOrderCheck != NSNotFound) {
        return [(ProductOrderSingle*)listProductOrders[indexCurrentWithOrderCheck] quantity];
    }else{
        return 0;
    }
}

-(NSUInteger)currentQuantityOrderWithProductId:(NSString *)productId{
    NSUInteger currentQuantity=0;
    for (ProductOrderSingle *productOrder in listProductOrders) {
        if ([self checkProductOrder:productOrder withProductId:productId]) {
            currentQuantity+=productOrder.quantity;
        }
    }
    return currentQuantity;
}

-(BOOL)checkProductOrder:(ProductOrderSingle*)productOrder withProductId:(NSString*)productId{
    NSDictionary *proProperty=productOrder.productProperies;
    return [proProperty[tkKeyId] isEqualToString:productId];

}

- (CBLDocument *)orderDocument{
    return orderDoc;
}
- (TKDiscount *)currentDiscountCart{
    return cashierBusiness.discount;
}
- (void)editDiscountCart:(TKDiscount *)oldDiscount withDiscountCart:(TKDiscount *)newDiscount{
    [cashierBusiness setDiscountTotal:newDiscount];
    [self resetData];
    [self saveToOrder];
}
-(void)willRemoveSelf{
    if ([self isEmptyOrder]) {
        [self deleteCurrentOrder];
    }
}

-(NSDictionary*)customerDictionary{
    return @{ @"firstName": @"Cong",
              @"lastName": @"Nha",
              @"fullName": @"Cong Nha",
              @"gender": @(1),
              @"birthday": @"1990-12-09T00:00:00",
              @"phone": @"0987654321",
              @"hiredDate": @"2014-12-09T00:00:00",
              @"address": @"2 Quang Trung",
              @"city": @"Da Nang",
              @"zipCode": @"",
              @"email": @"nha.duong@smoovapp.com",
              @"pinCode": @"12345",
              @"password": @"123456",
              @"passwordConfirm": @"123456",
              @"table": @"Customer",
              @"memberID": @"",
              @"acceptsNewsletter":@(NO),
              @"acceptsSms": @(NO),
              @"tags":[NSNull null],
              @"lastLogin": @"2015-01-28T08:43:33.19571Z",
              @"lastOrder": @"2015-01-28T08:43:33.2007106Z",
              @"totalOrder": @(1),
              @"totalSpend": @(1.1),
              @"image": @""};
}
-(void)dealloc{

}
@end
