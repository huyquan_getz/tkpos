//
//  CashierVC.m
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define MaxProductQuery 1000
#define DelaySearchText 0.5 //delay real 0.4s
#import "CashierVC.h"
#import "Controller.h"
#import "SearchCategory.h"
#import "CollectionCellProduct.h"
#import "UIAlertView+Blocks.h"
#import "UserDefaultModel.h"
#import "CashierBusinessModel.h"
#import "UIColor+Util.h"
#import "UIImage+Util.h"
#import "ListProductVC.h"
#import "ProductDetailVC.h"
#import "OrderVC.h"
#import "PaymentVC.h"
#import "WebserviceJSON.h"
#import "TKProductItem.h"
#import "TKProduct.h"
#import "Printing.h"

@interface CashierVC ()
{
    LanguageUtil *languageKey;
    NSUInteger countSearchChange;
}
@end

@implementation CashierVC{
    UIView *viewCover;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}
-(id)init{
    if (self =[super init]) {
        listProducts=[[NSMutableArray alloc] init];
        searching=NO;
        countSearchChange=0;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setButtonTypeWithCurrentTypeShow:_typeProductShower];
    _vFrameShowProduct.hidden=YES;
    [_searchBar setBarTintColor:[UIColor statusNaviBar]];
    _searchBar.layer.borderWidth=1;
    _searchBar.layer.borderColor=[UIColor statusNaviBar].CGColor;
    CGRect frame=_vBehideSearchBar.frame;
    frame.origin.x=66;
    frame.origin.y=0;
    [_vBehideSearchBar setFrame:frame];
    [_vBehideSearchBar setBackgroundColor:[UIColor statusNaviBar]];
    _vBehideSearchBar.hidden=YES;
    [_btCancelSearchCustom setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:_vBehideSearchBar];
    UITapGestureRecognizer *tapBehideSearchBar=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBehideSearch)];
    tapBehideSearchBar.numberOfTapsRequired=1;
    tapBehideSearchBar.numberOfTouchesRequired=1;
    [_vBehideSearchBar addGestureRecognizer:tapBehideSearchBar];
    _vLine.backgroundColor=tkColorFrameBorder;
    
    [_ivNaviBar setBackgroundColor:[UIColor statusNaviBar]];
    
    listTagButton=@[_btTagProduct,_btTagOrders];
    for (UIButton *btTag in listTagButton) {
        [btTag setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btTag setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [btTag setBackgroundColor:[UIColor clearColor]];
        UIImage *image=[UIImage imageCornerTopWithColor:[UIColor whiteColor] frame:btTag.frame corner:tkCornerRadiusButton];
        [btTag setBackgroundImage:image forState:UIControlStateSelected];
        [btTag setTitleColor:tkColorMain forState:UIControlStateSelected];
    }
    _btTagProduct.selected=YES;
    showingMain=ShowingMainProduct;
    [self reAddProductShower:[self createProductsShowerWithType:_typeProductShower products:nil indexItemShow:0]];
    [self reloadRequestProductWithCategory:nil alwayReload:YES];
    _btAddDiscount.enabled=NO;
    // Do any additional setup after loading the view from its nib.
}
-(UIViewController<ProductShower>*)createProductsShowerWithType:(TypeProductShower)typeShower products:(NSArray*)products indexItemShow:(NSInteger)currentIndexShow{
    switch (typeShower) {
        case ProductShowerList:{
            return [[ListProductVC alloc] initWithListProducts:products indexItemShow:currentIndexShow];
        }
        case ProductShowerGrid:
        default:{
            return [[PageProductVC alloc] initWithListProducts:products indexItemShow:currentIndexShow];
            break;
            
            break;
        }
    }
}
-(void)reAddProductShower:(UIViewController<ProductShower>*)newShower{
    if(_productsShower){
        [_productsShower.view removeFromSuperview];
        [_productsShower removeFromParentViewController];
    }
    _productsShower=newShower;
    [_productsShower setProductShowerDelegate:self];
    [_productsShower.view setFrame:_vFrameShowProduct.frame];
    [self addChildViewController:_productsShower];
    [self.view addSubview:_productsShower.view];
}
-(void)reSetupQueryLiveWithQuery:(CBLQuery*)query{
    if (query==nil) {
        return;
    }
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery =[query asLiveQuery];
    [liveQuery addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    liveQuery.limit=MaxProductQuery;
    [liveQuery start];
}
-(void)removeQueryLive{
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery=nil;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object == liveQuery) {
        [self displayRows:liveQuery.rows resetPostion:NO];
    }
}
-(void)displayRows:(CBLQueryEnumerator*)enumRows resetPostion:(BOOL)resetPostion{
    NSMutableArray *products=[[NSMutableArray alloc] init];
    for (CBLQueryRow *row in enumRows) {
        [products addObject:row.document];
    }
    [self resetProductsShowerWithProducts:products resetPostion:resetPostion];
}
-(void)viewWillAppear:(BOOL)animated
{
    languageKey =[ LanguageUtil sharedLanguageUtil];
    [_btTagProduct setTitle:[languageKey stringByKey:@"cashier.bt.tag-product"] forState:UIControlStateNormal];
    [_btTagOrders setTitle:[languageKey stringByKey:@"cashier.bt.tag-order"] forState:UIControlStateNormal];
    [_btCancelSearchCustom setTitle:[languageKey stringByKey:@"general.bt.cancel"] forState:UIControlStateNormal];
    [_searchBar setPlaceholder:[languageKey stringByKey:@"cashier.search-holder"]];
    _lbRequireNewOrder.text=[languageKey stringByKey:@"cashier.lb.require-new-order"];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)reShowMain:(ShowingMain) showingMainNew{
    if (showingMainNew == showingMain) {
        return;
    }
    
    //remove
    switch (showingMain) {
        case ShowingMainProduct:{
            [_productsShower.view removeFromSuperview];
            [_productsShower removeFromParentViewController];
            [listProducts removeAllObjects];
            _productsShower=nil;
            break;
        }
        case ShowingMainOrder:{
            [_pageOrderWaitingVC.view removeFromSuperview];
            [_pageOrderWaitingVC removeFromParentViewController];
            _pageOrderWaitingVC=nil;
            break;
        }
        default:
            break;
    }
    
    //add
    showingMain=showingMainNew;
    switch (showingMain) {
        case ShowingMainProduct:{
            _productsShower =[self createProductsShowerWithType:_typeProductShower products:nil indexItemShow:0];
            [_productsShower setProductShowerDelegate:self];
            [self addChildViewController:_productsShower];
            [_productsShower.view setFrame:_vFrameShowProduct.frame];
            [self.view addSubview:_productsShower.view];
            [self reloadRequestProductWithCategory:categorySelected alwayReload:YES];
            
            _btSearch.enabled=YES;
            _btChangeTypeShowProduct.enabled=YES;
            break;
        }
        case ShowingMainOrder:{
            _pageOrderWaitingVC =[[PageOrderWaitingVC alloc] init];
            _pageOrderWaitingVC.delegate=self;
            [self addChildViewController:_pageOrderWaitingVC];
            _pageOrderWaitingVC.view.frame=_vFrameShowProduct.frame;
            [self.view addSubview:_pageOrderWaitingVC.view];
            
            _btSearch.enabled=NO;
            _btChangeTypeShowProduct.enabled=NO;
            break;
        }
        default:
            break;
    }
}
-(void)reShowRightNew:(ShowingRight)showingRightNew withOrders:(CBLDocument*)order{
    switch (showingRight) {
        case ShowingRightOrderOpen:{
            [_orderVC willRemoveSelf];
            [_orderVC.view removeFromSuperview];
            [_orderVC removeFromParentViewController];
            _orderVC=nil;
            break;
        }
        case ShowingRightOrderOrded:{
            [_orderSelfCollectVC.view removeFromSuperview];
            [_orderSelfCollectVC removeFromParentViewController];
            _orderSelfCollectVC=nil;
            break;
        }
        default:
            break;
    }
    
    //add
    showingRight=showingRightNew;
    _btAddDiscount.enabled=(showingRight == ShowingRightOrderOpen);
    switch (showingRight) {
        case ShowingRightOrderOpen:{
            if (order) {
                _orderVC =[[OrderVC alloc] initWithOrder:order];
            }else{
                _orderVC =[[OrderVC alloc] initWithDefault];
            }
            _orderVC.delegate=self;
            [self addChildViewController:_orderVC];
            _orderVC.view.frame=_vFrameOrderList.frame;
            [self.view addSubview:_orderVC.view];
            break;
        }
        case ShowingRightOrderOrded:{
            if (order) {
                _orderSelfCollectVC =[[OrderSelfCollectVC alloc] initWithOrder:order];
                _orderSelfCollectVC.delegate=self;
                [self addChildViewController:_orderSelfCollectVC];
                _orderSelfCollectVC.view.frame=_vFrameOrderList.frame;
                [self.view addSubview:_orderSelfCollectVC.view];
            }
            break;
        }
        default:
            break;
    }
}
-(void)addPageOrderWating{
    if (_pageOrderWaitingVC) {
        [_pageOrderWaitingVC.view removeFromSuperview];
        [_pageOrderWaitingVC removeFromParentViewController];
        _pageOrderWaitingVC=nil;
    }
    _pageOrderWaitingVC =[[PageOrderWaitingVC alloc] init];
    _pageOrderWaitingVC.view.frame=_vFrameShowProduct.frame;
    _pageOrderWaitingVC.delegate=self;
    [self addChildViewController:_pageOrderWaitingVC];
    [self.view addSubview:_pageOrderWaitingVC.view];
}
- (IBAction)clickMenu:(id)sender {
    [Controller showLeftMenu:self];
}
- (IBAction)clickProduct:(UIButton*)sender {
    [self selectTagButton:sender];
    if (showingMain==ShowingMainProduct) { // click product again
        SearchCategory *search=[[SearchCategory alloc] init];
        search.delegate=self;
        _popover=[[UIPopoverController alloc] initWithContentViewController:search];
        _popover.delegate=self;
        [_popover setPopoverContentSize:search.view.frame.size];
        [_popover presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }else{
        [self reShowMain:ShowingMainProduct];
    }
}

- (IBAction)clickOrders:(id)sender {
    [self selectTagButton:sender];
    if (showingMain!=ShowingMainOrder) {
        [self reShowMain:ShowingMainOrder];
    }
    
}
-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    _popover=nil;
}
#pragma SearchCategoryDelegate
-(void)searchCategoryVC:(id)searchVC data:(CBLDocument *)data{
    [_popover dismissPopoverAnimated:YES];
    [self reloadRequestProductWithCategory:data alwayReload:NO];
}
-(CBLDocument *)categorySelected{
    return categorySelected;
}

-(void)reloadRequestProductWithCategory:(CBLDocument*)category alwayReload:(BOOL)alwayReload{
    if (![categorySelected.documentID isEqualToString:category.documentID] || alwayReload) {
        categorySelected=category;
        [_productsShower resetCurrentIndexItemShow];
        if (category==nil) {
            [self reSetupQueryLiveWithQuery:[Controller queryAllProductSortKeyMain]];
        }else{
            CBLQuery *query=[Controller queryAllProductByCategoryIDAndSortKeyMain];
            query.startKey=@[category.documentID,@{}];
            query.endKey=@[category.documentID];
            [self reSetupQueryLiveWithQuery:query];
        }
    }
}
-(void)resetProductsShowerWithProducts:(NSArray*)products resetPostion:(BOOL)resetPostion{
    if (products != listProducts) {
        [listProducts removeAllObjects];
        [listProducts addObjectsFromArray:products];
    }
    [_productsShower showListProducts:listProducts resetPosition:resetPostion];
}
-(void)selectTagButton:(UIButton*)buttonSelect{
    for (UIButton *tagButton in listTagButton) {
        tagButton.selected=NO;
    }
    buttonSelect.selected=YES;
}

-(void)searchProductWithTitleKey:(NSString*)keySearch{
    if ([oldSearchKey isEqualToString:keySearch] || [keySearch isEqualToString:@""]) {
        return;
    }
    oldSearchKey=keySearch;
    [_productsShower resetCurrentIndexItemShow];
    [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
        CBLQuery *query=[Controller querySearchProductItemDocumentsWithKeySearch:keySearch];
        query.descending=YES;
        NSError *error;
        CBLQueryEnumerator *enumerow=[query run:&error];
        [self displayRows:enumerow resetPostion:YES];
    }];
}

#pragma ShowProducts
-(void)productShower:(id)sender showProductDetail:(CBLDocument *)product{
    [_searchBar resignFirstResponder];
    productSelected=product;
    _productDetailVC =[[ProductDetailVC alloc] initWithProduct:product onlyShow:(_orderVC==nil)];
    _productDetailVC.delegate =self;
    [self addChildViewController:_productDetailVC];
    _productDetailVC.view.frame=self.view.frame;
    [self.view addSubview:_productDetailVC.view];
}
-(void)productShower:(id)sender showAdhocProduct:(BOOL)none{
    [_searchBar resignFirstResponder];
    productSelected=nil;
    if (_adhocProductVC) {
        [_adhocProductVC.view removeFromSuperview];
        [_adhocProductVC removeFromParentViewController];
        _adhocProductVC=nil;
    }
    _adhocProductVC=[[AdHocProductVC alloc] initWithAdHoc:nil onlyShow:(_orderVC==nil)];
    _adhocProductVC.delegate=self;
    [self addChildViewController:_adhocProductVC];
    _adhocProductVC.view.frame=self.view.frame;
    [self.view addSubview:_adhocProductVC.view];
}
-(CBLDocument*)productShower:(id)sender requireProductSelected:(BOOL)none{
    return productSelected;
}
#pragma ProductDetailDelegate
-(void)productDetail:(id)sender editProductOrder:(ProductOrderSingle *)oldProductOrder withProductOrder:(ProductOrderSingle *)newProductOrder productOriginal:(CBLDocument *)product{
    [self hideProductDetail];
    if (_orderVC) {
        NSInteger addQuantity=0;
        NSInteger subQuantity=0;
        if (newProductOrder) {
            subQuantity = newProductOrder.quantity;
        }
        if (oldProductOrder) {
            addQuantity =oldProductOrder.quantity;
        }
        // add to order
        [_orderVC editProductOrder:oldProductOrder withProductOrder:newProductOrder];
        
        // update quantity tity;
        NSString *skuVariantEdit =(newProductOrder!=nil)?newProductOrder.variantSKU:oldProductOrder.variantSKU;
        [self updateQuantityProductID:product.documentID variantSKU:skuVariantEdit add:addQuantity sub:subQuantity];
    }
}
-(void)productDetailCancel:(id)sender{
    [self hideProductDetail];
}
-(void)hideProductDetail{
    if (_productDetailVC) {
        [_productDetailVC.view removeFromSuperview];
        [_productDetailVC removeFromParentViewController];
        _productDetailVC=nil;
    }
}

#pragma PageOrderWaitingDelegate
-(void)pageOrderWaiting:(id)sender clickDetailOrder:(CBLDocument *)order{
    if (order==nil) {
        [self reShowRightNew:ShowingRightNone withOrders:nil];
    }else{
        OrderStatus orderStatus =[TKOrder orderStatus:order];
        if (orderStatus == OrderStatusPendingWaiting) {
            [self reShowRightNew:ShowingRightOrderOpen withOrders:order];
        }else{
            [self reShowRightNew:ShowingRightOrderOrded withOrders:order];
        }
    }
}
-(CBLDocument *)pageOrderWaitingRequireCurrentOrder:(id)sender{
    switch (showingRight) {
        case ShowingRightOrderOpen:{
            if (_orderVC) {
                return [_orderVC orderDocument];
            }else{
                return nil;
            }
            break;
        }
        case ShowingRightOrderOrded:{
            if (_orderSelfCollectVC) {
                return [_orderSelfCollectVC orderDocument];
            }else{
                return nil;
            }
        }
        default:
            break;
    }
    return nil;
}
-(void)pageOrderWaiting:(id)sender reloadCurrentOrder:(CBLDocument *)order{
    if (_orderSelfCollectVC) {
        [_orderSelfCollectVC reloadOrder];
    }
}
#pragma Search Bar Delegate
-(void)clickBehideSearch{ // extend area click textfield text affter hide keyboard
    [self showSearchBar];
}

//start search
- (IBAction)clickSearch:(id)sender {
    searching=YES;
    productSelected=nil;
    [_searchBar setText:@""];
    [self showSearchBar];
    
    //remove Query live
    [self removeQueryLive];
}
//end search
- (IBAction)clickCancelSearch:(id)sender {
    [self hideSearchBar];
    searching=NO;
    oldSearchKey=nil;
    
    // reload product when key
    [self reloadRequestProductWithCategory:categorySelected alwayReload:YES];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if (searchBar.text == nil || searchBar.text.length==0) {
        return;
    }
    [self searchProductWithTitleKey:[searchBar.text copy]];
}
-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        return YES;
    }
    if (countSearchChange == NSUIntegerMax) {
        countSearchChange=0;
    }else{
        countSearchChange++;
    }
    NSString *newString=[searchBar.text stringByReplacingCharactersInRange:range withString:text];
    newString =[newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSUInteger myCountChange=countSearchChange;//copy
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(DelaySearchText * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self addSearchTextChange:newString countChange:myCountChange];
    });
    return YES;
}
-(void)addSearchTextChange:(NSString*)text countChange:(NSUInteger)myCountChangeId{
    if (myCountChangeId != countSearchChange) {
        return;// have change affter
    }
    [self searchProductWithTitleKey:text];
}

-(void)showSearchBar{
    _btSearch.hidden=YES;
    _vBehideSearchBar.hidden=NO;
    [_searchBar becomeFirstResponder];
}
-(void)hideSearchBar{
    [_searchBar resignFirstResponder];
    _vBehideSearchBar.hidden=YES;
    _btSearch.hidden=NO;
    
}
-(void)updateQuantityProductID:(NSString*)productID variantSKU:(NSString*)variantSKU add:(NSInteger)addQ sub:(NSInteger)subQ{
    [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
        SyncManager *sync=[SyncManager sharedSyncManager];
        CBLDocument *product=[sync documentWithDocumentId:productID];
        // update quantity tity;
        NSMutableArray *arrayVariant =[[TKProductItem getArrayAllVariant:product] mutableCopy];
        for (NSInteger i=0; i<arrayVariant.count; i++) {
            NSDictionary *variantCurrent =arrayVariant[i];
            if ([variantCurrent[tkKeySKU] isEqualToString:variantSKU]) {
                NSInteger currentQuantity =[variantCurrent[tkKeyQuantity] integerValue];
                if (currentQuantity>=0) { // have check quantity
                    currentQuantity = (currentQuantity + addQ - subQ);
                    if (currentQuantity<0) { // error (rarely)
                        currentQuantity=0;
                    }
                    NSMutableDictionary *newVariant=[variantCurrent mutableCopy];
                    [newVariant setObject:@(currentQuantity) forKey:tkKeyQuantity];
                    [arrayVariant replaceObjectAtIndex:i withObject:newVariant];
                    [sync updateDocument:product propertyDoc:@{tkKeyArrayVariant:arrayVariant}];
                    break;
                }
            }
        }
    }];
}
#pragma OrderDelegate
-(void)orderClose:(id)sender andReAddNew:(BOOL)reAdd{
    if(reAdd){
        [self reShowRightNew:ShowingRightOrderOpen withOrders:nil];
    }else{
        [self reShowRightNew:ShowingRightNone withOrders:nil];
    }
}
-(void)orderCancel:(id)sender listProductOrder:(NSArray *)listProductOrder{
    for (ProductOrderSingle *proOrder in listProductOrder) {
        [self updateQuantityProductID:proOrder.productId variantSKU:proOrder.variantSKU add:proOrder.quantity sub:0];
    }
    [self orderClose:nil andReAddNew:NO];
    
}
-(void)order:(id)sender clickDetail:(ProductOrderSingle *)productOrder{
    [_searchBar resignFirstResponder];
    _productDetailVC =[[ProductDetailVC alloc] initWithProductOrder:productOrder fromOrder:YES];
    _productDetailVC.delegate =self;
    [self addChildViewController:_productDetailVC];
    _productDetailVC.view.frame=self.view.frame;
    [self.view addSubview:_productDetailVC.view];
}
-(void)order:(id)sender clickDetailAdhoc:(AdhocProduct *)adhocProduct{
    [self.view endEditing:YES];
    _adhocProductVC=[[AdHocProductVC alloc] initWithAdHoc:adhocProduct onlyShow:NO];
    _adhocProductVC.delegate=self;
    [self addChildViewController:_adhocProductVC];
    _adhocProductVC.view.frame=self.view.frame;
    [self.view addSubview:_adhocProductVC.view];
}
-(void)order:(id)sender clickPayment:(double)totalCharge{
    if (_paymentVC) {
        [_paymentVC.view removeFromSuperview];
        [_paymentVC removeFromParentViewController];
        _paymentVC= nil;
    }
    _paymentVC=[[PaymentVC alloc] initWithTotalCharge:totalCharge currency:[Controller currencyDefault]];
    _paymentVC.delegate=self;
    [self addChildViewController:_paymentVC];
    [self.view addSubview:_paymentVC.view];
}
#pragma PaymentDelegate
-(void)payment:(id)sender payment:(NSArray *)paymenterCharges completed:(BOOL)completed{
    // remove PaymentVC
    if (completed) {
        //add payment to order document
        if (_sendEmailRecieptVC) {
            [_sendEmailRecieptVC.view removeFromSuperview];
            [_sendEmailRecieptVC removeFromParentViewController];
            _sendEmailRecieptVC=nil;
        }
        PaymentChild *lastPayment=[paymenterCharges lastObject];
        double changeDueTotal=[PaymentBusinessModel totalChangeDueWithListPaymentChild:paymenterCharges];
        currentOrderDocumentPaymented =[CashierBusinessModel updatePaymentInforForOrder:_orderVC.orderDocument withListPaymentJSON:[PaymentChild arrayJSONPaymentChildWithListPaymentChild:_paymentVC.listPaymentChild] changeDue:changeDueTotal paymentStatus:PaymentStatusPaid];
        _sendEmailRecieptVC=[[SendEmailReceiptVC alloc] initWithLastCharge:lastPayment.customerSent outChange:lastPayment.changeDue currency:[Controller currencyDefault] order:currentOrderDocumentPaymented];
        _sendEmailRecieptVC.delegate=self;
        
        //hide PaymentVC . use reshow if sendmail back
        if (_paymentVC) {
            [_paymentVC.view removeFromSuperview];
            [_paymentVC removeFromParentViewController];
            _paymentVC=nil;
        }
        // add PaymentVC
        [self addChildViewController:_sendEmailRecieptVC];
        [self.view addSubview:_sendEmailRecieptVC.view];
    }
    
}
-(void)paymentCancel:(id)sender{
    if (_paymentVC) {
        [_paymentVC.view removeFromSuperview];
        [_paymentVC removeFromParentViewController];
        _paymentVC=nil;
    }
    [_orderVC cancelPayment];
}


#pragma SendEmailReceipt
-(void)sendEmailFinishPayment:(id)sender{
    //update to Shift Drawer
    [Controller updateDrawerCashWithOrder:currentOrderDocumentPaymented];
    [Printing printingAfterPayment:currentOrderDocumentPaymented];
    [SNLog Log:@"order paymented id: %@",currentOrderDocumentPaymented.properties[tkKeyId]];
    if (_orderVC) {
        [_orderVC.view removeFromSuperview];
        [_orderVC removeFromParentViewController];
        _orderVC=nil;
    }
    if (_sendEmailRecieptVC) {
        [_sendEmailRecieptVC.view removeFromSuperview];
        [_sendEmailRecieptVC removeFromParentViewController];
        _sendEmailRecieptVC=nil;
    }
    //remove current payment
    _paymentVC=nil;
}
-(void)sendEmailCancelPayment:(id)sender{
    [CashierBusinessModel removePaymentInfoForOrder:currentOrderDocumentPaymented toPaymentStatus:PaymentStatusPending];
    [SNLog Log:@"order paymented cancel id: %@",currentOrderDocumentPaymented.documentID];
    if (_sendEmailRecieptVC) {
        [_sendEmailRecieptVC.view removeFromSuperview];
        [_sendEmailRecieptVC removeFromParentViewController];
        _sendEmailRecieptVC=nil;
    }
    [_orderVC cancelPayment];
}
#pragma OrderSelfCollectDelegate
-(void)orderSelfCollectClose:(id)sender{
    [self reShowRightNew:ShowingRightNone withOrders:nil];
    if (_pageOrderWaitingVC) {
        [_pageOrderWaitingVC reloadData];
    }
}
-(void)orderSelfCollect:(id)sender updateQuantityProductID:(NSString *)productID variantSKU:(NSString *)variantSKU add:(NSInteger)addQ sub:(NSInteger)subQ{
    [self updateQuantityProductID:productID variantSKU:variantSKU add:addQ sub:subQ];
}
#pragma AdHocDelegate
-(void)adhocProduct:(id)sender close:(BOOL)none{
    [_adhocProductVC.view removeFromSuperview];
    [_adhocProductVC removeFromParentViewController];
    _adhocProductVC=nil;
}
-(void)adhocProduct:(id)sender updateAdhoc:(AdhocProduct *)oldAdhoc withNew:(AdhocProduct *)newAdhoc{
    [_adhocProductVC.view removeFromSuperview];
    [_adhocProductVC removeFromParentViewController];
    _adhocProductVC=nil;
    if (_orderVC) {
        [_orderVC editAdhocProduct:oldAdhoc withAdhocProduct:newAdhoc];
    }
    
}

- (IBAction)clickNewOrder:(id)sender {
    [self reShowRightNew:ShowingRightOrderOpen withOrders:nil];
    if (_pageOrderWaitingVC) {
        [_pageOrderWaitingVC reloadData];
    }
}
- (IBAction)clickChangeTypeShowProduct:(id)sender {
    if (_typeProductShower== ProductShowerList) {
        _typeProductShower=ProductShowerGrid;
    }else{
        _typeProductShower=ProductShowerList;
    }
    NSInteger currentIndex=0;
    if (_productsShower) {
        currentIndex = [_productsShower currentIndexItemShow];
    }
    [self reAddProductShower:[self createProductsShowerWithType:_typeProductShower products:listProducts indexItemShow:currentIndex]];
    [self setButtonTypeWithCurrentTypeShow:_typeProductShower];
}
-(void)setButtonTypeWithCurrentTypeShow:(TypeProductShower)typeShower{
    switch (typeShower) {
        case ProductShowerList:{
            [_btChangeTypeShowProduct setImage:[UIImage imageNamed:@"icon_thumbnail_w.png"] forState:UIControlStateNormal];
            break;
        }
        case ProductShowerGrid:
            [_btChangeTypeShowProduct setImage:[UIImage imageNamed:@"icon_list_w.png"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}
-(void)dealloc{
    [liveQuery removeObserver:self forKeyPath:@"rows"];
}


- (IBAction)clickAddDiscount:(id)sender {
    if (_discountCartVC) {
        [_discountCartVC.view removeFromSuperview];
        [_discountCartVC removeFromParentViewController];
    }
    TKDiscount *currentDiscount;
    if (_orderVC) {
        currentDiscount=[_orderVC currentDiscountCart];
    }
    _discountCartVC=[[DiscountVC alloc] initWithDiscount:currentDiscount];
    _discountCartVC.delegate=self;
    _discountCartVC.view.frame=self.view.frame;
    [self addChildViewController:_discountCartVC];
    [self.view addSubview:_discountCartVC.view];
}
#pragma DiscountDelegate
-(void)discount:(id)sender clickClose:(BOOL)none{
    [_discountCartVC.view removeFromSuperview];
    [_discountCartVC removeFromParentViewController];
}
-(void)discount:(id)sender updateDiscount:(TKDiscount *)oldDiscount withNew:(TKDiscount *)newDiscount{
    [_discountCartVC.view removeFromSuperview];
    [_discountCartVC removeFromParentViewController];
    if (_orderVC) {
        [_orderVC editDiscountCart:oldDiscount withDiscountCart:newDiscount];
    }
}
@end
