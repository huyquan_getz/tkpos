//
//  ImplementTestView.h
//  POS
//
//  Created by Nha Duong Cong on 10/30/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImplementTestView : NSObject
+(ImplementTestView*)sharedTestView;
-(UIViewController*)viewShow;
-(BOOL)showTest;
@end
