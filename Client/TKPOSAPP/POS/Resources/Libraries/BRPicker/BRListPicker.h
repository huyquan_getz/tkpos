//
//  BRListPicker.h
//  POS
//
//  Created by Cong Nha Duong on 3/5/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol BRListPicker <NSObject>
@optional
-(void)brListPicker:(id)sender cancelSelect:(BOOL)none;
-(void)brListPicker:(id)sender selectIndex:(NSInteger)indexSelected valueSelected:(id)object;

@end

#import <UIKit/UIKit.h>

@interface BRListPicker : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>{
    NSString* (^displayObject)(id object);
    NSArray *listObject;
}
@property (assign) NSInteger indexDefault;
-(instancetype)initWithListObject:(NSArray*)objects setDisplay:(NSString* (^)(id object))displayObject_;
@property (weak,nonatomic) id<BRListPicker> delegate;
-(void)showPickerOnVC:(UIViewController *)parentVC;
-(void)hidePicker;
@property (weak, nonatomic) IBOutlet UIView *vCover;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIPickerView *pkView;
@property (weak, nonatomic) IBOutlet UIButton *btDone;
- (IBAction)clickDone:(id)sender;

@end
