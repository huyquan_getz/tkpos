//
//  BRDatePicker.h
//  POS
//
//  Created by Cong Nha Duong on 3/5/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol BRDatePickerDelegate <NSObject>
@optional
-(void)brDatePicker:(id)sender clickClear:(BOOL)none;
-(void)brDatePicker:(id)sender setDate:(NSDate*)dateSet;
-(void)brDatePicker:(id)sender cancelSetDate:(BOOL)none;

@end
#import <UIKit/UIKit.h>

@interface BRDatePicker : UIViewController{
}
@property (strong,nonatomic)NSDate *dateDefault;
@property (weak, nonatomic) id<BRDatePickerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *pkDate;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIButton *btSetDate;
@property (weak, nonatomic) IBOutlet UIButton *btClear;
@property (weak, nonatomic) IBOutlet UIView *vCorver;
- (IBAction)clickClear:(id)sender;
- (IBAction)clickSetDate:(id)sender;
-(void)showPickerOnVC:(UIViewController*)parentVC;
-(void)hidePicker;
@end
