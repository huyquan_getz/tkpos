//
//  EmailReceipt.m
//  POS
//
//  Created by Nha Duong Cong on 12/4/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define MaxResendCount 3
#define ReceiptKeyCoder @"receipt"
#define EmailKeyCoder @"email"
#define EmailTypeKeyCoder @"emailType"
#define ResendCountKeyCode @"resendCount"
#import "EmailReceipt.h"
#import "Constant.h"

@implementation EmailReceipt
@synthesize receipt;
@synthesize email;
@synthesize emailType;
@synthesize resendCount;
-(instancetype)initWithReceipt:(NSDictionary *)receipt_ email:(NSString *)email_ type:(MailType)emailType_{
    if (self = [super init]) {
        receipt=[receipt_ copy];
        email=[email_ copy];
        emailType=emailType_;
        resendCount=0;
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:receipt forKey:ReceiptKeyCoder];
    [aCoder encodeObject:email forKey:EmailKeyCoder];
    [aCoder encodeInteger:emailType forKey:EmailTypeKeyCoder];
    [aCoder encodeInteger:resendCount forKey:ResendCountKeyCode];
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self =[super init]) {
        receipt=[aDecoder decodeObjectForKey:ReceiptKeyCoder];
        email=[aDecoder decodeObjectForKey:EmailKeyCoder];
        emailType=[aDecoder decodeIntegerForKey:EmailTypeKeyCoder];
        resendCount=[aDecoder decodeIntegerForKey:ResendCountKeyCode];
    }
    return self;
}
-(void)increaseResendCount{
    resendCount++;
}
-(BOOL)exceedingResendcount{
    return resendCount>MaxResendCount;
}
-(NSString *)idReceipt{
    return receipt[tkKeyId];
}
-(NSString *)reversionReceipt{
    return receipt[tkKeyRev];
}
@end
