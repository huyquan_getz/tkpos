//
//  EmailReceipt.h
//  POS
//
//  Created by Nha Duong Cong on 12/4/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
typedef enum _MailType{
    MailTypeNone=0,
    // message status change
    MailTypeReady=1,
    MailTypeRefund=2,
    MailTypeCancel=3,
    MailTypeRejectCancel=4,
    
// constain send content receipt
    MailTypeReSendRefund=5,
    MailTypeReSendCancel=6,
    MailTypeCollected=7,
    MailTypeReSendCollected=8,
    
// reportCashDrawer
    MailTypeReportCashDrawer =100
}MailType;
#import <Foundation/Foundation.h>

@interface EmailReceipt : NSObject<NSCoding>{
    NSDictionary *receipt;
    NSString * email;
    MailType emailType;
    NSInteger resendCount;
}
-(instancetype)initWithReceipt:(NSDictionary*)receipt_ email:(NSString*)email_ type:(MailType)emailType_;
@property(readonly)NSDictionary *receipt;
@property(readonly)NSString *email;
@property (readonly)MailType emailType;
@property (readonly)NSInteger resendCount;
-(NSString*)idReceipt;
-(NSString*)reversionReceipt;
-(void)increaseResendCount;
-(BOOL)exceedingResendcount;
@end
