//
//  LanguageUtil.h
//  POS
//
//  Created by Nha Duong Cong on 10/27/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
typedef enum _LanguageType{
    LanguageTypeEnglish,
    LanguageTypeVietnamese
}LanguageType;

#import <Foundation/Foundation.h>

@interface LanguageUtil : NSObject{
    LanguageType languageType;
    NSMutableDictionary *dictLanguageKey;
}
@property (strong,nonatomic) NSMutableDictionary *dictLanguageKey;
+(LanguageUtil*)sharedLanguageUtil;
-(BOOL)setLanguageWithType:(LanguageType)languageType;
+(NSMutableDictionary*)dataLanguageWithType:(LanguageType)type;
-(NSString*)stringByKey:(NSString*)key;
@end
