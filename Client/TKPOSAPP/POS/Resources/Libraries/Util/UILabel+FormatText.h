//
//  UILabel+FormatText.h
//  POS
//
//  Created by Cong Nha Duong on 3/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (FormatText)
- (void)setTextColor:(UIColor *)textColor range:(NSRange)range;
- (void)setFont:(UIFont *)font range:(NSRange)range;

- (void)setTextColor:(UIColor *)textColor occurenceOfString:(NSString*)separator;
- (void)setTextColor:(UIColor *)textColor beforeOccurenceOfString:(NSString*)separator;
- (void)setTextColor:(UIColor *)textColor afterOccurenceOfString:(NSString*)separator;
- (void)setFont:(UIFont *)font occurenceOfString:(NSString*)separator;
- (void)setFont:(UIFont *)font beforeOccurenceOfString:(NSString*)separator;
- (void)setFont:(UIFont *)font afterOccurenceOfString:(NSString*)separator;

@end
