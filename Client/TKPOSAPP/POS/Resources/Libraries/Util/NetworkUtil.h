//
//  NetworkUtil.h
//  POS
//
//  Created by Nha Duong Cong on 10/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol NetworkDelegate <NSObject>
@required
-(void)networkBackgroundNotiConnection:(BOOL)connectionOn;

@end
#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "Constant.h"

@interface NetworkUtil : NSObject{
    Reachability *reachability;
    NSMutableArray *listDelegates;
}
@property (assign)BOOL allowNotiWhenChangeStatusInternet;
+(NetworkUtil*)sharedInternetConnection;
-(BOOL)checkInternetConnection;
-(void)addDelegate:(id<NetworkDelegate>) newDelegate;
-(void)removeDelegate:(id)delegateDelete;
+(BOOL)checkHostConnection:(NSString *)host;
+(BOOL)checkInternetConnection;
@end
