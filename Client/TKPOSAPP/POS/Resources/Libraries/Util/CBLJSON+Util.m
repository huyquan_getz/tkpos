//
//  CBLJSON+Util.m
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "CBLJSON+Util.h"

@implementation CBLJSON (Util)
+(BOOL)jsonNull:(id)object{
    return (object==nil || [object isEqual:[NSNull null]]);
}
@end
