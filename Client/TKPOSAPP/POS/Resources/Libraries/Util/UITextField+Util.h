//
//  UITextField+Util.h
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Util)
-(BOOL)isEmptyText;
-(void)clearText;
@end
