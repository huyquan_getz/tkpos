//
//  SyncingListener.m
//  POS
//
//  Created by Nha Duong Cong on 10/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "SyncingListener.h"

static NSMutableArray *_listSyncingListener;

@implementation SyncingListener
+(void)initialize{
    _listSyncingListener=[[NSMutableArray alloc] init];
}
-(id)initWithBeginBlock:(void (^)())beginBlock_ endBlock:(void (^)(NSError *, NSError *))endBlock_{
    if (self =[super init]) {
        _beginBlock=beginBlock_;
        _endBlock=endBlock_;
        [_listSyncingListener addObject:self];
    }
    return self;
}
+(void)removeAllSyncingListerner{
    [_listSyncingListener removeAllObjects];
}
#pragma SyncManagerDelegate
-(void)syncManagerBeginSyncing:(id)syncManager{
    if (_beginBlock) {
        _beginBlock();
    }
}
-(void)syncManagerEndSyncing:(id)syncManager pullError:(NSError *)pullE pushError:(NSError *)pushE{
    if (_endBlock) {
        _endBlock(pullE,pullE);
    }
    [_listSyncingListener removeObject:self];
}
+(NSArray*)allSyncingListener{
    return [_listSyncingListener copy];
}
-(void)dealloc{
    [SNLog Log:10 :@"syncingListener Dealloc"];
}
@end
