//
//  UITextField+Util.m
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "UITextField+Util.h"

@implementation UITextField (Util)
-(BOOL)isEmptyText{
    return (self.text==nil || self.text.length==0);
}
-(void)clearText{
    self.text=@"";
}
@end
