//
//  Controller.m
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "Controller.h"
#import "WebserviceJSON.h"
#import "LeftMenuController.h"
#import "CouchBaseBusinessModel.h"
#import "UserDefaultModel.h"
#import "UIAlertView+Blocks.h"
#import "CashierVC.h"
#import "LoginCashierVC.h"
#import "MBProgressHUD.h"
#import "MBProgressHUDListener.h"
#import "SyncingListener.h"
#import "LoginChooseStoreVC.h"
#import "LoginForgotVC.h"
#import "LoginManagerVC.h"
#import "WelcomeVC.h"
#import "LoginVC.h"
#import "UtilBusinessModel.h"
#import "ReceiptVC.h"
#import "CashMgtVC.h"
#import "TKEmployee.h"
#import "ShiftDrawerManager.h"
#import "ShiftDrawerManager.h"
#import "SettingVC.h"
#import "CouchbaseViewController.h"
#import "CustomerVC.h"

@implementation Controller{
    PinCodeVC *pincodeSecurity;
}
@synthesize isLogined;
+(instancetype)sharedObject{
    __strong static Controller *_controller;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _controller=[[Controller alloc] init];
    });
    return _controller;
}
-(instancetype)init{
    if (self=[super init]) {
        isLogined=NO;
    }
    return self;
}
-(void)setLoginStatus:(BOOL)loginStatus{
    isLogined=loginStatus;
}
-(void)showPincodeSecurity{
    if (pincodeSecurity) {
        [pincodeSecurity.view removeFromSuperview];
        [pincodeSecurity removeFromParentViewController];
        pincodeSecurity=nil;
    }
    CBLDocument *userLogined=[Controller getUserLoginedDoc];
    pincodeSecurity=[[PinCodeVC alloc] initWithEmployee:userLogined checkLogin:NO];
    pincodeSecurity.delegate=self;
    UIViewController *mainview=[Controller appDelegate].slideMenuController;
    [mainview addChildViewController:pincodeSecurity];
    [mainview.view addSubview:pincodeSecurity.view];
}
-(void)pinCodeVerify:(id)sender switchUser:(BOOL)switchUser{
    [self hidePincodeSecurity];
    [Controller showCashierLoginView];
}
-(void)pinCodeVerify:(id)sender completed:(BOOL)completed{
    [self hidePincodeSecurity];
}
-(void)hidePincodeSecurity{
    if (pincodeSecurity) {
        [pincodeSecurity.view removeFromSuperview];
        [pincodeSecurity removeFromParentViewController];
        pincodeSecurity=nil;
    }
}
//===============================================
//static

+(void)showLeftMenuWithCompletion:(void (^)(BOOL))completion{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    if (!appDelegate.slideMenuController.leftDrawerViewController) {
        [appDelegate.slideMenuController setLeftDrawerViewController:[LeftMenuController sharedLeftMenu]];
    }
    [appDelegate.slideMenuController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:completion];
}

+(void)showLeftMenu:(id)sender{
    if ([sender isKindOfClass:[UIViewController class]]) {
        [[(UIViewController*)sender view] endEditing:YES];
    }
    [Controller showLeftMenuWithCompletion:nil];
}
+(void)showCenterViewWithCompletion:(void (^)(BOOL))completion{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    if (!appDelegate.slideMenuController.centerViewController) {
        UIViewController *emptyView=[[UIViewController alloc] init];
        [appDelegate.slideMenuController setCenterViewController:emptyView withCloseAnimation:YES completion:completion];
    }else{
        [appDelegate.slideMenuController closeDrawerAnimated:YES completion:completion];
    }
}
+(void)showCenterView{
    [Controller showCenterViewWithCompletion:nil];
}
+(void)showForgotLoginView{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[LoginForgotVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showMerchantLoginFirst{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[LoginVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showWelcomeView{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[WelcomeVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showManagerLoginView{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[LoginManagerVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showStoreLoginViewWithStores:(NSArray *)listStore{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    LoginChooseStoreVC *chooseStore=[[LoginChooseStoreVC alloc]initWithArrayStore:listStore];
    [appDelegate.slideMenuController setCenterViewController:chooseStore withCloseAnimation:YES completion:nil];
}
+(void)showCashierLoginView{
    [[Controller sharedObject] setLoginStatus:NO];
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[LoginCashierVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showHomeViewCashier{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[CashierVC alloc]init] withCloseAnimation:YES completion:nil];
    if ([ShiftDrawerManager sharedDrawerManager].currentShiftDrawer == nil) {
        LanguageUtil *languageKey=[LanguageUtil sharedLanguageUtil];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-message"] message:[languageKey stringByKey:@"controller.ms-dont-start-drawer"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles:nil];
        [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [[LeftMenuController sharedLeftMenu] setSelectedCellType:CellLeftMenuCashManagement withAction:YES];
        }];
    }
}
+(void)showHomeViewReceipts{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[ReceiptVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showHomeViewCashMgt{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[CashMgtVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showSettingMgt{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[SettingVC alloc]init] withCloseAnimation:YES completion:nil];
}
+(void)showCustomerMgt{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.slideMenuController setCenterViewController:[[CustomerVC alloc]init] withCloseAnimation:YES completion:nil];
}

//==============================================
+(void)startSyncFirstWithStore:(NSDictionary *)store{
    LanguageUtil *languageKey=[LanguageUtil sharedLanguageUtil];
    // get all channels
    NSDictionary *dic=[UserDefaultModel getMerchantSyncInfor];
    NSString *merchantID=dic[@"merchantID"];
    NSInteger branchIndex=[store[@"index"] integerValue];
    NSArray *channelsBranch=@[[NSString stringWithFormat:@"%@_%i",merchantID,branchIndex],
                              [NSString stringWithFormat:@"%@_%i_pos",merchantID,branchIndex]];
    [UtilBusinessModel addChannels:channelsBranch];
    // store chanel to list chanels to sync
    SyncManager *sync = [SyncManager sharedSyncManager];
    [sync resetSyncManager];
    [sync setSyncRemoteDefault];
    
    MBProgressHUD *hub=[[MBProgressHUD alloc]initWithView:[self appDelegate].slideMenuController.view];
    [[self appDelegate].slideMenuController.view addSubview:hub];
    hub.labelText=[languageKey stringByKey:@"login.controller.hub-syncing"];
    sync.progressHUBSync=hub;
    MBProgressHUDListener *hubListener=[[MBProgressHUDListener alloc] initWithHubHidenBlock:^{
        [hub removeFromSuperview];
    }];
    SyncingListener *syncingListener=[[SyncingListener alloc] initWithBeginBlock:nil endBlock:^(NSError *pullError, NSError *pushError) {
        if (pullError) {
            [sync stopPull];
            [sync stopPush];
            [sync deleteDatabaseAndWait:1000];
            UIAlertView *alertError=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"login.controller.title-sync-error"] message:[languageKey stringByKey:@"login.controller.ms-sync-error"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles:nil];
            [alertError show];
            
        }else{
            CBLDocument *merchantData= [sync documentWithDocumentId:store[@"employeeID"]];
            [UserDefaultModel saveAccountMerchant:merchantData.documentID];
            [UserDefaultModel saveStoreSelected:store[tkKeyId]];
            [UserDefaultModel saveFirstSyncCompleted:YES];
            [Controller saveUserLogined:merchantData];
            MBProgressHUD *hubPrepare=[[MBProgressHUD alloc]initWithView:[self appDelegate].slideMenuController.view];
            [[self appDelegate].slideMenuController.view addSubview:hubPrepare];
            hubPrepare.labelText=@"Preparing...";
            [hubPrepare show:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
                    [[CouchbaseViewController sharedObject] reloadAllView];
                    [hubPrepare hide:YES];
                    [[LeftMenuController sharedLeftMenu] setSelectedCellType:CellLeftMenuCashier withAction:YES];
                }];
                
            });
        }
    }];
    sync.delegate=syncingListener;
    sync.progressHUBSync.delegate=hubListener;
    sync.showHUBwhileSyncing=YES;
    [sync restartPull];
    [sync restartPush];
}
+(void)saveMerchantSyncInfor:(NSDictionary *)dataSyncInfor{
    if (dataSyncInfor[tkKeyCreatedDate]==nil) {
        NSMutableDictionary *newDict=[dataSyncInfor mutableCopy];
        [newDict setObject:[CBLJSON JSONObjectWithDate:[NSDate date]] forKey:tkKeyCreatedDate];
        dataSyncInfor=newDict;
    }
    [UserDefaultModel saveMerchantSyncInfor:dataSyncInfor];
}
+(NSDictionary*)checkMerchantWithBusinessName:(NSString *)businessName username:(NSString *)username password:(NSString *)password{
    NSDictionary *dict=[WebserviceJSON authenticationWithBusinessName:businessName username:username password:password];
    if ([dict[tkKeyReturnStatus] integerValue] == ReturnCodeSuccess) {
        // add modifired show
        NSMutableDictionary *newdict=[dict mutableCopy];
        NSMutableDictionary *newData=[dict[tkKeyResult] mutableCopy];
        [newData setObject:businessName forKey:tkKeyBusinessName];
        [newData setObject:username forKey:tkKeyUserName];
        [newData setObject:password forKey:tkKeyPassword];
        [newdict setObject:newData forKey:tkKeyResult];
        return newdict;
    }
    return dict;
}
+(BOOL)resetToken{
    NSMutableDictionary *dataSync=[[UserDefaultModel getMerchantSyncInfor] mutableCopy];
    NSDictionary * response=[WebserviceJSON getTokenWithBusinessName:dataSync[tkKeyBusinessName] username:dataSync[tkKeyUserName] password:dataSync[tkKeyPassword]];
    if ([response[tkKeyReturnStatus] integerValue] ==ReturnCodeSuccess) {
        [dataSync addEntriesFromDictionary:response[tkKeyResult]];
        [self saveMerchantSyncInfor:dataSync];
        return YES;
    }
    return NO;
}
+(BOOL)checkUserMerchantLocalWithUsername:(NSString *)username andPassword:(NSString *)password{
    NSString *merchantDocumentId=[UserDefaultModel getAccountMerchant];
    CBLDocument *merchantDocument=[[SyncManager sharedSyncManager] documentWithDocumentId:merchantDocumentId];
    NSDictionary *merchant=merchantDocument.properties;
    if ([username isEqualToString:merchant[tkKeyEmail]] && [password isEqualToString:merchant[tkKeyPassword]]) {
        return YES;
    }else{
        return NO;
    }
}
+(void)updateDrawerCashWithOrder:(CBLDocument*)order{
    NSMutableArray *listPayment=[[NSMutableArray alloc] init];
    for (NSDictionary *paymentJSON in [TKOrder arrayPaymentJSON:order]) {
        [listPayment addObject:[[PaymentChild alloc] initFromJsonData:paymentJSON]];
    }
    double totalCash=[PaymentBusinessModel totalChargeWithListPaymentChild:[PaymentBusinessModel subListPaymentChildWithListPaymentChild:listPayment isPayCash:YES]];
    double changeDue=[TKAmountDisplay tkRoundCash:[TKOrder changeDue:order]];
    if (totalCash > 0 || changeDue >0) {
        double totalCashTender=(totalCash + changeDue);
        CBLDocument *userLogined=[Controller getUserLoginedDoc];
        ActitivityDrawer *activity =[[ActitivityDrawer alloc] initWithEmployeeId:[TKEmployee getEmployeeID:userLogined] employeeName:[TKEmployee getEmployeeName:userLogined] paymentCash:totalCashTender changeDue:changeDue comment:@"Payment"];
        [[ShiftDrawerManager sharedDrawerManager] addToCurrentShiftWithActivity:activity];
    }
}
+(void)updateDrawerCashWithPaymentRefund:(double )refundAmount reason:(NSString*)comment{
    CBLDocument *userLogined=[Controller getUserLoginedDoc];
    ActitivityDrawer *activity =[[ActitivityDrawer alloc] initWithEmployeeId:[TKEmployee getEmployeeID:userLogined] employeeName:[TKEmployee getEmployeeName:userLogined] refund:refundAmount comment:comment];
        
    [[ShiftDrawerManager sharedDrawerManager] addToCurrentShiftWithActivity:activity];
}
+(CBLQuery *)queryGetAllCategoryDocuments{
    return [CouchBaseBusinessModel queryGetAllCategory];
}
+(CBLQuery *)queryAllProductByCategoryIDAndSortKeyMain{
    return [CouchBaseBusinessModel queryAllProductByCategoryIDAndSortKeyMain];
}
+(CBLQuery *)queryAllProductSortKeyMain{
    return [CouchBaseBusinessModel queryAllProductSortKeyMain];
}
+(CBLQuery *)querySearchProductItemDocumentsWithKeySearch:(NSString *)keySearch{
    return [CouchBaseBusinessModel querySearchProductItemWithTitleKey:keySearch];
}
+(CBLQuery *)queryTKGetReceiptLimitDateInMonth:(NSInteger)numberMonthBefore{
    return [CouchBaseBusinessModel queryTKGetReceiptLimitDateInMonth:numberMonthBefore];
}
+(CBLQuery *)querySearchReceiptWithTitleKey:(NSString *)titleSearch{
    return [CouchBaseBusinessModel querySearchReceiptWithTitleKey:titleSearch];
}

+(CBLQuery *)queryGetAllOrderWaiting{
    return [CouchBaseBusinessModel queryGetAllOrderWaiting];
}

+(CBLQuery *)queryGetAllPrinter{
    return [CouchBaseBusinessModel queryGetAllPrinter];
}
+(CBLQuery *)queryGetAllPrinting{
    return [CouchBaseBusinessModel queryGetAllPrinting];
}
+(CBLDocument*)employeeWithPinCode:(NSString*)pincode{
    CBLQuery *query =[CouchBaseBusinessModel querySearchEmployeePinCode:pincode];
    CBLQueryEnumerator *enumQuery =[query run:nil];
    CBLDocument *employee=nil;
    if(enumQuery && enumQuery.count>0){
        employee=[enumQuery nextRow].document;
    }
    return employee;
}
+(CBLQuery *)queryShiftDrawerInYear:(NSInteger)year month:(NSInteger)month{
    return [CouchBaseBusinessModel queryShiftDrawerInYear:year month:month];
}
+(CBLQuery *)queryAllTaxService{
    return [CouchBaseBusinessModel queryAllTaxServiceCharge];
}
+(CBLDocument *)productWithSKU:(NSString *)sku{
    return [CouchBaseBusinessModel productWithSKU:sku];
}
+(CBLDocument *)getTaxServiceSetting{
    NSString *docID=[UserDefaultModel getTaxServiceSetting];
    if (docID) {
        return [[SyncManager sharedSyncManager] documentWithDocumentId:docID];
    }else{
        CBLDocument *docTax=[CouchBaseBusinessModel taxServiceSetting];
        [UserDefaultModel saveTaxtServiceSetting:docTax.documentID];
        return docTax;
    }
}
+(void)saveUserLogined:(CBLDocument *)dataUser{
    [[Controller sharedObject] setLoginStatus:YES];
    [UserDefaultModel saveUserLogined:dataUser.documentID];
}
+(CBLDocument*)getUserLoginedDoc{
    NSString *userLognedDocumnetID=[UserDefaultModel getUserLogined];
    return [[SyncManager sharedSyncManager] documentWithDocumentId:userLognedDocumnetID];
}
+(CBLDocument *)getStoreInfoDoc{
    NSString *storeID=[UserDefaultModel getStoreSelected];
    return [[SyncManager sharedSyncManager] documentWithDocumentId:storeID];
}
+(CBLDocument *)getGeneralSetting{
    NSString *docID=[UserDefaultModel getGeneralSetting];
    if (docID) {
        return [[SyncManager sharedSyncManager] documentWithDocumentId:docID];
    }else{
        CBLDocument *docTax=[CouchBaseBusinessModel generalSetting];
        [UserDefaultModel saveGeneralSetting:docTax.documentID];
        return docTax;
    }
}
+(NSInteger)storeIndexDefault{
    static NSInteger storeIndex;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CBLDocument *store=[self getStoreInfoDoc];
        storeIndex=[store.properties[tkKeyIndexStore] integerValue];
    });
    return storeIndex;
}
+(NSString *)currencyDefault{
    static NSString *currency;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CBLDocument *generalSetting=[self getGeneralSetting];
        currency=generalSetting.properties[tkKeyCurrency];
    });
    return currency;
}
+(BOOL)sendReceipt:(NSDictionary *)receipt toEmail:(NSString *)email type:(MailType)mailType{
    if (receipt==nil || ![email isEmailFormat]) {
        return NO;
    }
    NSDictionary *response=[WebserviceJSON sendReceipt:receipt toEmail:email type:mailType];
    return ([response[tkKeyReturnStatus] integerValue]==ReturnCodeSuccess);
}
+(AppDelegate *)appDelegate{
    AppDelegate *appDelegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    return appDelegate;
}
@end
