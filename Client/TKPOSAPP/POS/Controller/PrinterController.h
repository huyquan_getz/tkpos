//
//  PrinterController.h
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrintData.h"
@interface PrinterController : NSObject
+ (PrinterController*)sharedInstance;
-(BOOL)checkConnectionWithIPAddress:(NSString*)ipAddress port:(NSInteger)port;
- (NSArray*)getAllPrintData;
- (NSArray*)getAllPrintLocal;
- (void)processingPrint;
- (void)handlePrint;
- (void)updatePrintData:(PrintData*)printData atIndex:(int)index;
- (void)addPrintData:(PrintData*)printData;
- (BOOL)openDrawerOpenWithIP:(NSString*)_ip;
- (void) closeAllPrinter;
- (void)addPrintWithImage:(UIImage*)image Height:(float)height Ip:(NSString*)ip Type:(int)type Name:(NSString*)name Infor:(NSDictionary*)dictInfor;
@end
