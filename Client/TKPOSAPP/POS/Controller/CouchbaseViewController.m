//
//  CouchbaseViewController.m
//  POS
//
//  Created by Cong Nha Duong on 2/6/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
//

#import "CouchbaseViewController.h"
#import "Constant.h"
#import "CouchBaseBusinessModel.h"

@implementation CouchbaseViewController{
    NSMutableSet *setViewLoaded;
}
+(instancetype)sharedObject{
    __strong static CouchbaseViewController *_couchbaseViewController;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _couchbaseViewController=[[CouchbaseViewController alloc] init];
    });
    return _couchbaseViewController;
}
-(instancetype)init{
    if (self =[super init]) {
    }
    return self;
}
-(BOOL)reloadView:(ViewType)viewType{
    return [CouchBaseBusinessModel reloadView:viewType];
}
-(BOOL)reloadAllView{
    BOOL done=YES;
    for (int i=0;i<=13;i++) {
        done = done && [self reloadView:i];
    }
    return done;
}
@end
