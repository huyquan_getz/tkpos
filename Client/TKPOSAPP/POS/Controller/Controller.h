//
//  Controller.h
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpClient.h"
#import "WebserviceJSON.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "PinCodeVC.h"
#import "EmailReceipt.h"

@interface Controller : NSObject<PinCodeDelegate>{
    BOOL isLogined;
}
+(instancetype)sharedObject;
@property (readonly) BOOL isLogined;
-(void)setLoginStatus:(BOOL)loginStatus;
-(void)showPincodeSecurity;
-(void)hidePincodeSecurity;


+(void)startSyncFirstWithStore:(NSDictionary*)store;
// show view same UIController
+(void)showLeftMenu:(id)sender;
+(void)showLeftMenuWithCompletion:(void (^)(BOOL finished))completion;
+(void)showCenterView;
+(void)showCenterViewWithCompletion:(void (^)(BOOL finished))completion;
+(void)showMerchantLoginFirst;
+(void)showCashierLoginView;
+(void)showHomeViewCashier;
+(void)showHomeViewReceipts;
+(void)showHomeViewCashMgt;
+(void)showSettingMgt;
+(void)showCustomerMgt;

+(void)showStoreLoginViewWithStores:(NSArray*)listStore;
+(void)showForgotLoginView;
+(void)showManagerLoginView;
+(void)showWelcomeView;

// function for save information data to local
+(void)saveMerchantSyncInfor:(NSDictionary*)dataSyncInfor;
+(void)saveUserLogined:(CBLDocument*)document;
+(CBLDocument*)getUserLoginedDoc;
+(CBLDocument*)getStoreInfoDoc;
+(CBLDocument*)getGeneralSetting;
+(CBLDocument*)getTaxServiceSetting;
+(BOOL)resetToken;

// check information
+(NSDictionary*)checkMerchantWithBusinessName:(NSString*)businessName username:(NSString*)username password:(NSString*)password;
+(BOOL)checkUserMerchantLocalWithUsername:(NSString*)username andPassword:(NSString*)string;

//Get Data from couchbaselite
+(CBLQuery*)queryGetAllCategoryDocuments;
+(CBLQuery*)queryAllProductByCategoryIDAndSortKeyMain;
+(CBLQuery*)queryAllProductSortKeyMain;
+(CBLQuery*)querySearchProductItemDocumentsWithKeySearch:(NSString*)keySearch;

+(CBLQuery *)queryTKGetReceiptLimitDateInMonth:(NSInteger)numberMonthBefore; // limit 3month or 90 date
+(CBLQuery*)querySearchReceiptWithTitleKey:(NSString *)titleSearch;

+(CBLQuery *)queryGetAllOrderWaiting;

+(CBLDocument*)employeeWithPinCode:(NSString*)pincode;
+(CBLQuery *)queryShiftDrawerInYear:(NSInteger)year month:(NSInteger)month;
+(CBLQuery*)queryGetAllPrinter;
+(CBLQuery*)queryGetAllPrinting;
+(CBLQuery*)queryAllTaxService;

+(CBLDocument*)productWithSKU:(NSString*)sku;
//sent mail
+(BOOL)sendReceipt:(NSDictionary*)receipt toEmail:(NSString*)email type:(MailType)mailType;
//appDelegate instance
+(AppDelegate*)appDelegate;

+(NSInteger)storeIndexDefault;
+(NSString*)currencyDefault;
// update

+(void)updateDrawerCashWithOrder:(CBLDocument*)order;
+(void)updateDrawerCashWithPaymentRefund:(double )paymentRefund reason:(NSString*)comment;
@end
