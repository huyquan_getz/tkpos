//
//  PrinterManagement.h
//  POS
//
//  Created by Cong Nha Duong on 1/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrintData.h"
#import "PrintObjectData.h"

@interface PrinterManagement : NSObject <UIAlertViewDelegate> {
    BOOL isShowAlert;
    int count;
    NSTimer *timer_;
}
- (BOOL) checkConnectionWithPrinterWithIP:(NSString *)ipAddress port:(NSInteger)port;
- (NSArray*)getAllPrintData;
- (void)processingPrint;
- (NSArray *)listPrint;
- (void)handlePrint;
- (void)addPrintData:(PrintData*)printData;
- (void)updatePrintData:(PrintData*)printData atIndex:(int)index;
- (BOOL)openDrawerWithIP:(NSString*)ip;
- (void) closeAllPrinter;
@end
