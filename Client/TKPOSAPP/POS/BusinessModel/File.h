//
//  File.h
//  POS
//
//  Created by Cong Nha Duong on 1/14/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface File : NSObject
+(NSString *)rootFolder;
+(NSString*)printingFolder;
+(NSString*)createFolderWithName:(NSString *)childFolder toFolder:(NSString *)parentFolder;
+(NSString *)createFolder:(NSString *)fullPath;
+(BOOL)folderExist:(NSString*)pathFile;
+(BOOL)fileExist:(NSString *)fullPath;

+(BOOL)saveData:(NSData *)data toFolder:(NSString *)folder withName:(NSString*)name;
+(NSData*)getDataWithFullFile:(NSString*)fullFile;
+(NSData*)getdataWithFileName:(NSString*)fileName fromFolder:(NSString*)folder;
@end
