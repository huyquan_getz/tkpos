//
//  AppDelegate.m
//  POS
//
//  Created by Nha Duong Cong on 10/13/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "AppDelegate.h"
#import "WelcomeVC.h"
#import "LoginCashierVC.h"
#import "UserDefaultModel.h"
#import "LanguageUtil.h"
#import "NetworkUtil.h"
#import "QueueSendMail.h"
#import "ImplementTestView.h"
#import "TKProduct.h"
#import "SyncManager.h"
#import "PrinterController.h"
#import "Controller.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //set appDelegate
    [LanguageUtil sharedLanguageUtil];
    [NetworkUtil sharedInternetConnection].allowNotiWhenChangeStatusInternet=NO;
    [UserDefaultModel saveRequestPincodeWhenEnterForeground:NO];
    [[NetworkUtil sharedInternetConnection]  addDelegate:[QueueSendMail sharedQueue]];
    if (![UserDefaultModel wasFirstSyncCompleted]) {
        //create slideMenu for app
        _slideMenuController=[[SlideMenuController alloc] initWithCenterViewController:[[WelcomeVC alloc] init] leftDrawerViewController:nil];
    }else{
        //create slideMenu for app
        _slideMenuController=[[SlideMenuController alloc] initWithCenterViewController:[[LoginCashierVC alloc] init] leftDrawerViewController:nil];
        [[SyncManager sharedSyncManager] startPush];
        [[SyncManager sharedSyncManager] startPull];
    }
    // check show test view
#if (TARGET_IPHONE_SIMULATOR)
    if ([ImplementTestView sharedTestView].showTest) {
        _slideMenuController=[[SlideMenuController alloc] initWithCenterViewController:[ImplementTestView sharedTestView].viewShow leftDrawerViewController:nil];
    }
#endif
    [_slideMenuController setShowsShadow:YES];
    [_slideMenuController setRestorationIdentifier:@"MMDrawer"];
    [_slideMenuController setMaximumLeftDrawerWidth:350];
    [_slideMenuController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [_slideMenuController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[PrinterController sharedInstance] processingPrint];
    });
    self.window.rootViewController=_slideMenuController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    //[[PrinterController sharedInstance] closeAllPrinter];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([[Controller sharedObject] isLogined] && [UserDefaultModel getRequestPincodeWhenEnterForeground]) {
            [[Controller sharedObject] showPincodeSecurity];
        }
    });
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
   // [[PrinterController sharedInstance] closeAllPrinter];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//

+(CGSize)size{
    if ([self isPortrait]) {
        // portrait
        return [[UIScreen mainScreen] bounds].size;
    } else {
        // landscape
        CGSize size =[[UIScreen mainScreen] bounds].size;
        return CGSizeMake(size.height, size.width);
    }
    
    
}
+(CGRect)bounds{
    if ([self isPortrait]) {
        // portrait
        return [[UIScreen mainScreen] bounds];
    } else {
        // landscape
        CGRect bounds =[[UIScreen mainScreen] bounds];
        return CGRectMake(0, 0, bounds.size.height, bounds.size.width);
    }
}
+(UIInterfaceOrientation)orientation{
    return [[UIApplication sharedApplication] statusBarOrientation];
}
+(BOOL)isPortrait{
    return UIInterfaceOrientationIsPortrait([self orientation]);
}

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)windowd{
    return UIInterfaceOrientationMaskAll;
}


@end
