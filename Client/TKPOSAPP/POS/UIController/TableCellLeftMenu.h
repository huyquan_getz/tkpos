//
//  TableCellLeftMenu.h
//  POS
//
//  Created by Cong Nha Duong on 1/20/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellLeftMenu : UITableViewCell{

}
@property (strong,nonatomic) UIImage *imgISelected;
@property (strong,nonatomic)UIImage *imgIDeselected;
@property (weak, nonatomic) IBOutlet UIImageView *ivIndentify;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIView *vLine;
-(void)setValueDefault;
@end
