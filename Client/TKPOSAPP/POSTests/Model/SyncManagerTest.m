//
//  SyncManagerTest.m
//  POS
//
//  Created by Nguyen Anh Dao on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

/*
 all test with Couchbase lite
 
 */
#import <XCTest/XCTest.h>
#import "Constant.h"
#import "SyncManager.h"
#import "UserDefaultModel.h"
@interface SyncManagerTest : XCTestCase

@end

@implementation SyncManagerTest{
    SyncManager *sync;
}

- (void)setUp
{
    sync=[SyncManager sharedSyncManager];
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
-(BOOL)compareDict:(NSDictionary*)dic andDict:(NSDictionary*)dic2{
    if (dic.count == dic2.count) {
        for (NSString *key in dic.allKeys) {
            if (![dic[key] isEqualToString:dic2[key]]) {
                return NO;
            }
        }
    }else{
        return NO;
    }
    return YES;
}
-(BOOL)compareDoc:(CBLDocument*)doc content:(NSDictionary*)dic{
    NSMutableDictionary *docdic=[doc.properties mutableCopy];
    [docdic removeObjectForKey:@"_id"];
    [docdic removeObjectForKey:@"_rev"];
    return [self compareDict:docdic andDict:dic];
}
-(BOOL)compareData:(NSData*)data1 andData:(NSData*)data2{
    return data1.length == data2.length;
}
-(void)testsharedSyncManager
{
    XCTAssert(sync, @"can't creat instance of SyncManager");
}
-(void) testSyncInit
{
    // check init value of Function Sync Not NULL
    XCTAssertNotNil(sync, "sync nill");
    XCTAssert(sync.manager !=nil, "manager nill");
    XCTAssertNotNil(sync.database, "database nill");
    NSDictionary* dict=@{
                       @"expires_in": @(1209599),
                       @"serverSync":@"http://192.168.2.201",
                       @"databaseSync":@"sync_gateway",
                       @"userAccessSync":@"congnha",
                       @"passAccessSync":@"congnha",
                       tkKeyChannels:@[@"*"],
                       @"stores":@[@{@"branch_name":@"Store 1",@"channel":@"store1"},
                                   @{@"branch_name":@"Store 2",@"channel":@"store2"}]
                       };
    [UserDefaultModel saveMerchantSyncInfor:@{}];
    XCTAssertNil(sync.syncURL, "URL nill");
    XCTAssertNil(sync.pull, "Pull Replication nill");
    XCTAssertNil(sync.push, "Push Replication nill");
       
    [UserDefaultModel saveMerchantSyncInfor:dict];
    [sync setSyncRemoteDefault];
    XCTAssertNotNil(sync.syncURL, "URL nill");
    XCTAssertNotNil(sync.pull, "Pull Replication nill");
    XCTAssertNotNil(sync.push, "Push Replication nill");
    XCTAssert(sync.pull.remoteURL!=nil, "URL of Pull Replication nill");
    XCTAssert(sync.push.remoteURL!=nil, "URL of Push Replication nill");
    XCTAssert(sync.pull.continuous, " type of  Pull Replication is not continues");
    XCTAssert(sync.push.continuous, "type of Push Replication is not continues");
    [UserDefaultModel saveMerchantSyncInfor:@{}];
}
-(void)testCreateTheManager
{
    BOOL isCreat= [sync createTheDatabase];
    XCTAssert(isCreat, @"Can not create Manager of CouchBase");
    
}

-(void) testCreatViewWithTitleView
{
    
    CBLView *view = [sync  viewListDocumentWithTitleView:@"viewTest" titleKey:@"testCase" keyMain:tkSortKeyNormal];
    XCTAssert(view, "View Is NULL");
    
}
-(void)testDeleteViewWithTitleView
{
    //case 1'
     [sync viewListDocumentWithTitleView:@"viewTestDelete" titleKey:@"testCase" keyMain:tkSortKeyNormal];//
    [sync deleteViewWithViewName:@"viewTestDelete"];
    XCTAssert(![sync.database existingViewNamed:@"viewTestDelete"], "Not Exit View");
    
    [sync deleteViewWithViewName:@"viewTestDelete"];
    XCTAssert(![sync.database existingViewNamed:@"viewTestDelete"], "Not Exit View");
    
}
-(void)testQueryList
{
    [sync viewListDocumentWithTitleView:@"viewTest" titleKey:@"testCase" keyMain:tkSortKeyNormal];
    
    NSArray *arr= [sync queryListWithViewName:@"viewTest" ];
    XCTAssert(arr, " Query is NULL");
}
-(void)testQueryListHasCompare
{
    [sync viewListDocumentWithTitleView:@"viewTest" titleKey:tkKeyTable titleCompare:@"Category" keyMain:tkSortKeyNormal];
    NSArray *arr= [sync queryListWithViewName:@"viewTest" ];
    XCTAssert(arr, " Query is NULL");
}
-(void)testQueryListHasCompareWithMap
{
    
    [sync viewListDocumentWithTitleView:@"viewTest" titleKey:tkKeyTable titleCompare:@"Category" keyMain:tkSortKeyNormal];
    NSArray *arrs= [sync queryListWithViewName:@"viewTest" ];
    for (CBLDocument *doc in arrs) {
        [sync viewListDocumentWithTitleView:@"viewTest" compareMap:@{@"categoryId": doc.properties[@"_id"], tkKeyTable:@"Product"} keyMain:@"Category"];
        NSArray *arr= [sync queryListWithViewName:@"viewTest" ];
        XCTAssert(arr, " Query is NULL ");
    }
    //  XCTAssert(arr, " Query not nil");
}
-(void) testCRUDDocument
{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]initWithDictionary:@{tkKeyTable:@"TestUnitCase",@"testCaseName":@"232"}];
    CBLDocument *doc =[sync createDocument:dic];
    XCTAssert(doc, " Creat document failed");
    XCTAssert([self compareDoc:doc content:dic], "reget document not true data");
    
    NSDictionary *newdata=@{@"newValue": @"keyUpdate",
                            @"changeValue":tkKeyTable};
    [sync updateDocument:doc propertyDoc:newdata];
    [dic addEntriesFromDictionary:newdata];//merge data
    XCTAssert([self compareDoc:doc content:dic], "not update data success");
    
    NSString *docId=doc.documentID;
    [sync deleteDocument:doc];
    XCTAssert([sync documentWithDocumentId:docId].isDeleted, "delete document fail");
    
    CBLDocument *newdoc=[sync createDocument:@{@"key":@"value"}];
    NSString *newDocId=newdoc.documentID;
    [sync deleteDocumentWithDocumentID:newDocId];
    XCTAssert([sync documentWithDocumentId:newDocId].isDeleted, "delete document fail");
    [sync.database deleteLocalDocumentWithID:doc.documentID error:nil];
    [sync.database deleteLocalDocumentWithID:newdoc.documentID error:nil];
   
}
-(void)testStopSyncReplication
{
    [sync stopPull];
    [sync stopPush];
    CBLReplicationStatus stop = sync.pull.status;
    XCTAssert(stop==kCBLReplicationStopped, "Stop PUll Sync failed");
    stop = sync.push.status;
    XCTAssert(stop==kCBLReplicationStopped, "Stop PUSH Sync failed");
}
-(void)testRemotePushFilter:(NSString*)filterName param:(NSDictionary*)dicParam
{
    [sync setRemotePushFilter:@"filterTest" param:@{@"key":tkKeyTable,@"value":@"TestUnitCase"}];
    
}
-(void)testBackgroundTell{
    //to database busy;
//    [sync viewListDocumentWithTitleView:@"viewTest" titleKey:tkKeyTable titleCompare:tkProductTable];
//    dispatch_queue_t myQueue=dispatch_queue_create("queue.test", NULL);
//    dispatch_async(myQueue, ^{
//        for (int i=0;i<100 ;i++) {
//            usleep(100);
//            NSDictionary * dataPr2=@{tkKeyTable:tkProductTable,
//                                    @"categoryId":@"232swweer",
//                                     @"name":@"12334test",
//                                     @"quantity":@"100",
//                                     @"amount":@"1111",
//                                     @"currency":@"$",
//                                     @"creatDate": [CBLJSON JSONObjectWithDate: [NSDate date]]};
//            CBLDocument *product2=[sync createDocument:dataPr2];
//            [product2 deleteDocument:nil];
//        }
//    });
    @try {
        [sync backgroundTellWithBlock:^(CBLDatabase *database) {
            NSArray *list=[sync queryListWithViewName:@"viewTest"];
        }];
    }
    @catch (NSException *exception) {
        XCTAssert(NO, "exception when tell back ground");
    }
    
    @try {
        [sync backgroundTellQueryWithViewName:@"viewTest" response:^(NSArray *list) {
            [SNLog Log:10 :@"listCount %d",list.count];
        }];
    }
    @catch (NSException *exception) {
        XCTAssert(NO, "exception when tell query ");
    }
    
    
}
/*
-(void)testWorkingAttachment{
    NSData *data=[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"noImage" ofType:@"jpeg"]];
    CBLDocument *doc=[sync createDocument:nil];
    BOOL writeTrue = [sync writeAttachment:doc withData:data name:@"noimage" contentType:@"image/jpeg"];
    XCTAssert(writeTrue, "not write attachment");
    CBLAttachment *attData=[sync readingAttachment:doc withName:@"EnglishKey"];
    XCTAssert(attData && attData.content, "not get attachment");
    XCTAssert([self compareData:data andData:attData.content], "reget attachment not true");
    
    [sync removeAttachmentWithDocument:doc withName:@"noImage"];
    CBLAttachment *attEmpty=[sync readingAttachment:doc withName:@"noImage"];
    XCTAssert(attEmpty==nil, "not deleteAttacment");
    
    //test attachment image
    UIImage *image=[UIImage imageNamed:@"noImage.jpeg"];
    XCTAssert([sync writeAttachImage:doc withImage:image],"not write image to doc");
    UIImage *imageReget=[sync readingAttachImage:doc];
    NSData *data1 = UIImagePNGRepresentation(image);
    NSData *data2 = UIImagePNGRepresentation(imageReget);
    XCTAssert([self compareData:data1 andData:data2], "reget image to not true");
    
}
 */

-(void)testResetSyncManager{
    NSDictionary * dataPr=@{tkKeyTable:tkProductTable,
                            @"categoryId":@"232swweer",
                            @"name":@"12334test",
                            @"quantity":@"9",
                            @"amount":@"123",
                            @"currency":@"$",
                            @"creatDate": [CBLJSON JSONObjectWithDate: [NSDate date]]};
    [sync createDocumentLocalOnly:dataPr];
    NSLog(@"count %d",sync.database.documentCount);
    [sync resetSyncManager];
    XCTAssert(sync.database.documentCount==0, "reset sync but have document in database");
}
@end
