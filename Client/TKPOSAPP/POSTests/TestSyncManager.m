//
//  TestSyncManager.m
//  POS
//
//  Created by Nguyen Anh Dao on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SyncManager.h"
@interface TestSyncManager : XCTestCase

@end

@implementation TestSyncManager

- (void)setUp
{
    //[SyncManager sharedSyncManager];
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
//    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}
-(void)testsharedSyncManager
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    XCTAssert(sync, @"can't creat instance of SyncManager");
}
-(void) testSyncInit
{
    // check init value of Function Sync Not NULL
    SyncManager *sync=[SyncManager sharedSyncManager];
    XCTAssertNotNil(sync, "sync nill");
    XCTAssert(sync.manager !=nil, "manager nill");
    XCTAssertNotNil(sync.database, "database nill");
    XCTAssertNotNil(sync.syncURL, "URL nill");
    XCTAssertNotNil(sync.pull, "Pull Replication nill");
    XCTAssertNotNil(sync.push, "Push Replication nill");
    XCTAssert(sync.pull.remoteURL!=nil, "URL of Pull Replication nill");
    XCTAssert(sync.push.remoteURL!=nil, "URL of Push Replication nill");
    XCTAssert(sync.pull.continuous, " type of  Pull Replication is not continues");
    XCTAssert(sync.push.continuous, "type of Push Replication is not continues");
    
    
    
}
-(void)testCreateTheManager
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    BOOL isCreat= [sync createTheDatabase];
    XCTAssert(isCreat, @"Can not create Manager of CouchBase");
    
}

-(void) testCreatViewWithTitleView
{
    
    SyncManager *sync=[SyncManager sharedSyncManager];
    CBLView *view = [sync creatViewWithTitleView:@"viewTest" titleKey:@"testCase"];
    XCTAssert(view, "View Is NULL");
    
}
-(void)testDeleteViewWithTitleView
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    //case 1'
     [sync creatViewWithTitleView:@"viewTestDelete" titleKey:@"testCase"];//
    [sync deleteViewWithViewName:@"viewTestDelete"];
    XCTAssert(![sync.database existingViewNamed:@"viewTestDelete"], "Not Exit View");
    
    [sync deleteViewWithViewName:@"viewTestDelete"];
    XCTAssert(![sync.database existingViewNamed:@"viewTestDelete"], "Not Exit View");
    
}
-(void)testQueryList
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    [sync creatViewWithTitleView:@"viewTest" titleKey:@"testCase"];
    
    NSArray *arr= [sync queryListWithViewName:@"viewTest" ];
    XCTAssert(arr, " Query is NULL");
}
-(void)testQueryListHasCompare
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    [sync viewListDocumentWithTitleView:@"viewTest" titleKey:@"type" titleCompare:@"Category"];
    NSArray *arr= [sync queryListWithViewName:@"viewTest" ];
    XCTAssert(arr, " Query is NULL");
}
-(void)testQueryListHasCompareWithMap
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    
    [sync viewListDocumentWithTitleView:@"viewTest" titleKey:@"type" titleCompare:@"Category"];
    NSArray *arrs= [sync queryListWithViewName:@"viewTest" ];
    for (CBLDocument *doc in arrs) {
        [sync viewListDocumentWithTitleView:@"viewTest" compareMap:@{@"categoryId": doc.properties[@"_id"], @"type":@"Product"} withKeyMain:@"Category"];
        NSArray *arr= [sync queryListWithViewName:@"viewTest" ];
        XCTAssert(arr, " Query is NULL ");
    }
    //  XCTAssert(arr, " Query not nil");
}
-(void) testCreatDocument
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    CBLDocument *doc =[sync creatDocument:@{@"type":@"TestUnitCase",@"testCaseName":@"232"}];
    XCTAssert(doc, " Creat docment failed");
   
}
-(void) testUpdateDocument
{
    SyncManager *sync=[SyncManager sharedSyncManager];
   [sync viewListDocumentWithTitleView:@"viewTest" titleKey:@"type" titleCompare:@"TestUnitCase"];
    NSArray *arrs= [sync queryListWithViewName:@"viewTest"];
    for (CBLDocument *doc in arrs) {
        CBLDocument *docUpdate =[sync updateDocument:doc propertyDoc:@{@"testCase":@"123"}];
        XCTAssert(docUpdate, " Update Document failed");

    }
   
}
-(void) testDeleteDocument
{
    SyncManager *sync=[SyncManager sharedSyncManager];
   [sync viewListDocumentWithTitleView:@"viewTest" titleKey:@"type" titleCompare:@"TestUnitCase"];
    NSArray *arrs= [sync queryListWithViewName:@"viewTest"];
    for (CBLDocument *doc in arrs) {
        BOOL isDelete =[sync deleteDocument:doc];
        XCTAssert(isDelete, " Delete Document failed");
    }
    
}
-(void) testDeleteDocumentWithDocumentID
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    [sync viewListDocumentWithTitleView:@"viewTest" titleKey:@"type" titleCompare:@"TestUnitCase"];
    NSArray *arrs= [sync queryListWithViewName:@"viewTest" ];
    for (CBLDocument *doc in arrs) {
        BOOL isDelete =[sync deleteDocumentWithDocumentID:doc.documentID];
        XCTAssert(isDelete, " Delete Document failed");
    }
}
-(void)testReadingAttachImage
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    //case 1'
    [sync creatViewWithTitleView:@"viewTestDelete" titleKey:@"_attachments"];
    NSArray *arrs= [sync queryListWithViewName:@"viewTest" ];
    for (CBLDocument *doc in arrs) {
        UIImage *image =[sync readingAttachImage:doc];
        XCTAssert(image, @"Can not read Image");
    }
    
}
-(void) testWriteAttachImage
{
    SyncManager *sync=[SyncManager sharedSyncManager];
  //  [sync creatDocument:@{@"type":@"TestUnitCase",@"testCaseName":@"2333332"}];
    [self testCreatDocument];
   // XCTAssert(docTest, " Creat docment failed");
    UIImage *image =[UIImage imageNamed:@"btnEnd.png"];
    [sync viewListDocumentWithTitleView:@"viewTest" titleKey:@"type" titleCompare:@"TestUnitCase"];
    NSArray *arr= [sync queryListWithViewName:@"viewTest" ];
   
    for (CBLDocument *doc in arr) {

        BOOL isWrite = [sync writeAttachImage:doc withImage:image];
        XCTAssert(isWrite, @"Can not write Image");
    }
}
-(void)testQueryNotUseLiveQuery
{
    SyncManager *sync=[SyncManager sharedSyncManager];
    [sync viewListDocumentWithTitleView:@"viewTest" titleKey:@"type" titleCompare:@"Category"];
    
    NSArray *arr= [sync queryListNotUseLiveQuery:@"viewTest" ];
    XCTAssert(arr, " Query is NULL");
}
@end
